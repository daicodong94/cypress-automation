import Login from "../../../support/pages/TMS/components/login/Login"
import Logout from "../../../support/pages/TMS/components/logout/Logout"
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"


describe("Implement Function", () => {
    var data, objLogin, objHome, objLoginPRD
    var objPickup = new Pickup()
    var objDeliveryGood = new DeliveryGood()

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })
    
    // it("TC_78 - Check flow to access DeliveryGoods - Access Page screen", () => {
    //     objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //     cy.wait(2000)
    //     // objDeliveryGood.navigateDeliveryGoodPageSuccess()
    //     objDeliveryGood.header.btnAtHeader("Bulk delivery").should("be.visible")
    // })

    // it("TC_79, TC_88 - Search Shipping Order by conditions", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.reload()
    //         cy.wait(30000)
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         objDeliveryGood.pickup.ckbChooseOrder("3").should("be.visible")
    //     })
    // })

    // it("TC_80 - Check function More...", () => {
    //     objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //     objDeliveryGood.common.hideAndUnHideSearch()
    // })

    // it("TC_81, TC_82, TC_83, TC_84 - Check function Set Column - Disable header", () => {
    //     objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //     objDeliveryGood.common.resetDefaultColumn("yes")
    //     cy.wait(5000)
    //     objDeliveryGood.common.setColumn("line name", "waybill number")
    //     objDeliveryGood.common.enableOrDisableColumn("line name", "no")
    //     objDeliveryGood.common.resetDefaultColumn("yes")     
    // })

    // it("TC_85 - Check pagination", () => {
    //     objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //     cy.wait(1000)
    //     objDeliveryGood.pickup.goToPageNumber(1)
    //     cy.wait(1000)
    //     objDeliveryGood.pickup.goToPageNumber(2)
    //     cy.wait(1000)
    //     objDeliveryGood.common.selectPageSize("100/page")
    //     cy.wait(1000)
    //     objDeliveryGood.common.selectPageSize("50/page")     
    //     cy.wait(1000)
    //     objDeliveryGood.common.selectPageSize("20/page")        
    // })

    // it("TC_86 - Check function Bulk Delivery at DeliverGoods screen", () => {
    //     cy.randomNumber(20).then((num) =>{
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //     })
    //     cy.randomNumber(20).then((num) =>{
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         var msg = "background processing, you will be notifed you through the system information after the processing is completed. during this period, please do not update pick up/delivery status for orders!"
    //         objDeliveryGood.bulkDeliveryOrderConfirm("yes", msg)
    //         cy.reload()
    //         cy.wait(10000)
    //         objDeliveryGood.leftMenu.navigateToSubPage("track", "Dispatch Order Tracking ")
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         cy.wait(1000)
    //         objDeliveryGood.dispatchOrdertrackingStatus("finished").should("be.visible")
    //     })
    // })

    // it("TC_89 - Check function Delivery completed at DeliverGoods screen", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         objDeliveryGood.deliveryCompleted()
    //         objDeliveryGood.leftMenu.navigateToSubPage("track", "Dispatch Order Tracking ")
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         cy.wait(1000)
    //         objDeliveryGood.dispatchOrdertrackingStatus("finished").should("be.visible")
    //         cy.reload()
    //         cy.wait(10000)
    //         objDeliveryGood.leftMenu.navigateToSubPage("receipt ", "driver receipt ")
    //         objDeliveryGood.common.searchByClickSuggestion("dispatch number", num)
    //         cy.wait(1000)
    //         objDeliveryGood.statusOrderOfDriverReceipt().should("be.visible")
    //     })
    // })

    // it("TC_90 - Check function Keep at DeliverGoods screen", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         objDeliveryGood.deliverySave()
    //     })
    // })

    // it("TC_91 - Check function Reject at DeliverGoods screen", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         objDeliveryGood.deliveryReject()
    //         objDeliveryGood.leftMenu.navigateToPage("abnormal ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("order number", num)
    //         cy.wait(2000)
    //         objDeliveryGood.pickup.abnormalException("reject").should("be.visible")
    //     })
    // })

    // it("TC_92 - Check function Map tracking at DeliverGoods screen\n" + 
    // "TC_93: Check function Transport process at a glance at DeliverGoods screen", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objDeliveryGood.createOrdertoDeliveryGood(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         cy.wait(1000)
    //         objDeliveryGood.mapTrackingAndTransportProcess("map tracking", "map tracking")
    //         cy.reload()
    //         // TC_93 Check function Transport process at a glance at DeliverGoods screen
    //         objDeliveryGood.leftMenu.navigateToSubPage("implement", "deliver goods ")
    //         cy.wait(2000)
    //         objDeliveryGood.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         cy.wait(1000)
    //         objDeliveryGood.mapTrackingAndTransportProcess("transport process at a glance", "shipping information at a glance")
    //     })
    // })
})