import Common from "../../../TMS/components/common/Common"
import Header from "../../../TMS/components/fragments/Header"
import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import LeftMenu from "../../../TMS/components/fragments/LeftMenu"

class Address {

    //CONSTRUCTOR
    constructor() {
        this.common = new Common()
        this.commonMD = new CommonMasterData()
        this.header = new Header()
        this.headerMD = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    //XPATHS
    btnCreateCountry() {
        return cy.xpath("//span[text()='Country']/following-sibling::div/button")
    }
    
    blockProvince() {
        return cy.xpath("//span[text()='Province']/parent::div/parent::div")
    }

    btnExpandItem(value) {
        return cy.xpath("//span[@class='el-checkbox__label' and text() = '" + value + "']/parent::label/parent::div/following-sibling::div[contains(@class,'utility-btns')]/button[contains(@class,'el-button')]")
    }

    btnCreate(type) {
        return cy.xpath("//span[text()='" + type + "']/following-sibling::div/button")
    }

    popupCreate(value) {
        //Province | District
        return cy.xpath("//div[@aria-label='Create " + value + "']")
    }

    btnClosePopupCreate(value) {
        //Province | District
        return cy.xpath("//span[text()='Create " + value + "']/following-sibling::button")
    }

    txtName(value) {
        //Province | District
        return cy.xpath("//*[@placeholder='Enter " + value + " Name']")
    }

    btnCancel() {
        return cy.xpath("(//span[@class='dialog-footer']//button)[3]")
    }

    popupConfirmQuitCreate(value) {
        //Province | District
        return cy.xpath("//div[@aria-label='Quit Create " + value + "']/div")
    }

    btnConfirmQuit() {
        return cy.xpath("(//div[@class='el-message-box__btns']//button)[2]")
    }

    btnCancelQuit() {
        return cy.xpath("//div[@class='el-message-box__btns']//button[1]")
    }

    btnSave() {
        return cy.xpath("//span[contains(text(),'Save')]/parent::button")
    }

    createAndSave(value, type) {
        this.btnCreate(type).click()
        this.txtName(type).type(value)
        this.btnSave().click()
    }

    msgSuccess() {
        return cy.xpath("//div[@class='el-message el-message--success is-closable']/p")
    }

    lblItem(value) {
        return cy.xpath("//span[@class='el-checkbox__label' and text() = '" + value + "']")
    }

    btnRemove(type) {
        return cy.xpath("//span[text()='"+ type + "']/following-sibling::div/button[contains(@class,'remove-address-btn')]")
    }

    btnConfirmRemove(type) {
        return cy.xpath("//div[@aria-label='Remove " + type + "']//button[2]")
    }

    removeItem(type, value) {
        this.lblItem(value).scrollIntoView().click()
        this.btnRemove(type).click()
        this.btnConfirmRemove(type).click()
    }

    lblWarningMsg() {
        return cy.xpath("//div[@class='text-error-5 text-sm error-message has-error']")
    }

    msgError() {
        return cy.xpath("//div[@class='el-message el-message--error is-closable messageCustomClass']/p")
    }
    
    getItemMenuId(value) {
        return cy.xpath("//span[@class='el-checkbox__label' and text() = '" + value + "']/parent::label/parent::div/following-sibling::div[contains(@class,'utility-btns')]/div/span")
    }

    btnSelectItemAction(value) {
        return cy.xpath("//span[@aria-controls='"+ value +"']/button")
    }

    btnEditItem(value) {
        return cy.xpath("//ul[@id='" + value + "']/li/button")
    }

    popupEdit(value) {
        //Province | District
        return cy.xpath("//div[@aria-label='Edit " + value + "']")
    }

    btnClosePopupEdit(value) {
        //Province | District
        return cy.xpath("//span[text()='Edit " + value + "']/following-sibling::button")
    }

    popupConfirmQuitEdit(value) {
        //Province | District
        return cy.xpath("//div[@aria-label='Quit Edit " + value + "']/div")
    }

    editAndSave(type, currentValue, newValue) {
        this.getItemMenuId(currentValue).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            this.btnSelectItemAction(id).click()
            this.btnEditItem(id).click()
        })
        this.txtName(type).clear().type(newValue)
        this.btnSave().click()
        this.btnConfirmQuit().click()
    }

    popupRemove(value) {
        //Province | District
        return cy.xpath("//div[@aria-label='Remove " + value + "']")
    }

    btnCancelConfirmRemove(type) {
        return cy.xpath("//div[@aria-label='Remove " + type + "']//button[@class='el-button el-button--default el-button--mini']")
    }

    popupUnableToRemove(type) {
        return cy.xpath("//div[@aria-label='Unable to remove some " + type + "s']")
    }

    btnDone() {
        return cy.xpath("//span[text()[normalize-space()='Done']]")
    }

    btnDownloadIssue() {
        return cy.xpath("//button[contains(@class, 'download-issue-btn')]")
    }

}

export default Address;