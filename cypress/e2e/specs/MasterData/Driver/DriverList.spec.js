import  LoginSupplier from "../../../../support/pages/TMS/components/login/LoginSupplier";

describe("Master data - Driver - Edit", () => {

    var data, objLogin, objHome, objDriverList
    var filePath = "implement/images.png"
    var date = new Date()

    beforeEach("TMS-Our-Itemtype", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            // // const url = new Utility().getBaseUrl()
            cy.visit("https://tms-stg.allnowgroup.com/")
            // if (url == "http://tmsui.allnow.tms.staging.internal/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objDriverList = objHome.leftMenu.navigateToSubPage("Master Data", "Driver")
        //     }   else if (url == "https://tms.allnowgroup.com") {
        //         cy.fixture("data_test.json").then((loginData) => {
        //             data = loginData
        //             cy.visit(data.url)
        //             objLogin = new Login()
        //             objHome = objLogin.login(data.username, data.password)
        //         })
        //     }
        })
    })

    it("Check display when hover on search box on Driverscreen", () => {
        objDriverList.ipSearchDriver().should('be.visible')
    })

    it("Check search with keywords by [Company Registered Name, Driver ID, Driver Name, Driver License Number]", () => {
        objDriverList.ipSearchDriver().click()
        objDriverList.ipSearchDriver().type("ANL-D002607")
        objDriverList.ipSearchDriver().type('{enter}')
        objDriverList.columnDriverID().first().should('have.text', 'ANL-D002607')
    })

    it("Check Search box when don't input any keywords", () => {
        objDriverList.ipSearchDriver().click()
        objDriverList.ipSearchDriver().type(" ")
        objDriverList.btnSearchDriver().click()
    })

    it("Check search with keywords by Driver ID field on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with keywords by Driver name field on Driver screen", () => {
        objDriverList.columnFirstName().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnFirstName().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with keywords by Driver License Number field on Driver screen", () => {
        objDriverList.columnDriverLicenseNumber().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverLicenseNumber().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with exact keywords on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with partial keywords on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{backspace}')
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('contain', $el.text().trim().substr(0, parseInt($el.text().length) - 1))
        })
    })

    it("Check search with special keywords on Driver screen", () => {
        objDriverList.ipSearchDriver().click()
        objDriverList.ipSearchDriver().type("!@$%!@%@")
        objDriverList.btnSearchDriver().click()
        objDriverList.lblNoData().should('be.visible')
    })

    it("Check input keyword does not exist in the system on Driver screen", () => {
        cy.randomString(5).then((text)=>{
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type(text)
            objDriverList.btnSearchDriver().click()
            objDriverList.lblNoData().should('be.visible')
        })
    })

    it("Check delete inputted keywords on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{backspace}')
            objDriverList.btnSearchDriver().click()
            objDriverList.ipSearchDriver().click()
            objDriverList.btnDeleteSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().should('have.value', '')
        })
    })

    it("Check press Enter from keyboard to search on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().type('{enter}')
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check click on Filter button", () => {
        objDriverList.btnFilter().click()
        objDriverList.filDriverLicenseType().should('be.visible')
        objDriverList.filDriverStatus().should('be.visible')
    })

    it("Check combo box Driver License Type", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverLicenseType().click()
    })

    it("Check search by Driver License Type field on Filter", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverLicenseType().click()
        objDriverList.lblDriverLicenseType("ท.2").click()
        objDriverList.columnDriverLicenseType().first().should('contain', 'ท.2')
        objDriverList.columnDriverLicenseType().last().should('contain', 'ท.2')
    })

    it("Check combo box Driver Status", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
    })

    it("Check search by Driver Status field on Filter", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Inactive").click()
        objDriverList.columnDriverStatus().first().should('contain', 'inactive')
        objDriverList.columnDriverStatus().last().should('contain', 'inactive')
    })

    it("Check click Reset filter button", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverLicenseType().click()
        objDriverList.lblDriverLicenseType("ท.2").click()
        objDriverList.btnResetFilter().click()
    })

    it("Check click filter button again after filter is used ", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverLicenseType().click()
        objDriverList.lblDriverLicenseType("ท.2").click()
        objDriverList.btnFilter().click()
        objDriverList.animationFilter().last().should('be.visible')
    })
    
    it("Check amount of customer displaying in the one page ", () => {
        objDriverList.cboPageNumber().first().click({force : true})
        
        objDriverList.lblPageNumber("30/page").last().click({force : true})
        objDriverList.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '30/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverList.cboPageNumber().first().click({force : true})
        objDriverList.lblPageNumber("50/page").last().click({force : true})
        objDriverList.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '50/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverList.cboPageNumber().first().click({force : true})
        objDriverList.lblPageNumber("75/page").last().click({force : true})
        objDriverList.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '75/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverList.cboPageNumber().first().click({force : true})
        objDriverList.lblPageNumber("100/page").last().click({force : true})
        objDriverList.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '100/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
    })

    it("Check click on any number page on Driver screen", () => {
        objDriverList.columnDriverID().first().then(($e1)=>{
            objDriverList.numberPagePag("2").first().click()
            objDriverList.columnDriverID().first().then(($e2)=>{
                if($e1.text().trim() != $e2.text().trim()){
                    cy.log("Pagination success to page 2")
                }
                else{
                    cy.log($e1.text().trim())
                    cy.log($e2.text().trim())
                }
            })
        })
    })

    it("Check click on > icon on Driver screen", () => {
        objDriverList.numberPagePag("1").first().should('have.attr', 'class', 'number active')
        objDriverList.btnNextPage().click({force : true})
        objDriverList.numberPagePag("2").first().should('have.attr', 'class', 'number active')
    })

    it("Check click on < icon on Driver screen", () => {
        objDriverList.numberPagePag("2").first().click()
        objDriverList.btnPrevPage().click()
        objDriverList.numberPagePag("1").first().should('have.attr', 'class', 'number active')
    })

    it("Check input number of page into Go to field when system has many page on Driver screen", () => {
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("2", {force : true})
        objDriverList.ipPageNumber().type('{enter}', {force : true})
        objDriverList.numberPagePag("2").first().should('have.attr', 'class', 'number active')
    })

    it("Check when input data is negative number on Driver screen" +
    "TC_40 - Check when input data is letter in Go to textbox on Driver screen" + 
    "TC_41 - Check when input data is special characters in Go to textbox on Driver screen", () => {
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("ส่วนบุคคล", {force : true})
        objDriverList.ipPageNumber().type('{enter}', {force : true})
        objDriverList.numberPagePag("1").first().should('have.attr', 'class', 'number active')
        objDriverList.ipPageNumber().should('have.value','1')
    })

    it("Check when input is letters on Go to textbox", () => {      
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("ab")
        objDriverList.ipPageNumber().should('have.value', '')
    })

    it("Check when input is special characters on Go to textbox", () => {       
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("@$!@$%@!%")
        objDriverList.ipPageNumber().should('have.value', '')
    })

    it("Check set status popup is displayed", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.lblSetStatus().should('be.visible')
    })

    it("Check tick on Select date/time radio button on set status popup", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.defaultOtherCheckBoxButtonSetStatus("Active").first().should('have.attr', 'class', 'el-radio__input is-checked')
        objDriverList.defaultOtherCheckBoxButtonSetStatus("No end date").first().should('have.attr', 'class', 'el-radio__input is-checked')
        objDriverList.txtEffectiveDate().last().click()
    })

    it("Check when select date/time in the past", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.txtEffectiveDate().last().click()
        objDriverList.lblYearDOB().click()
        objDriverList.btnPrevYear().click()
        objDriverList.lblY().should('not.to.selected')
    })

    it("Check when select date/time in the future", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.txtEffectiveDate().last().click()
        objDriverList.lblYearDOB().click()
        objDriverList.btnNextYear().click()      
        objDriverList.lblYear("2030").click()      
        objDriverList.lblMonth("Oct").click()     
        objDriverList.lblDay("29").click()     
        objDriverList.lblHour("13").scrollIntoView().click()     
        objDriverList.lblMinute("00").scrollIntoView().click()       
        objDriverList.btnOKSetED().click()      
        objDriverList.txtEffectiveDate().last().should('contain', '2030/10/29   13:00')
        objDriverList.btnCancelSetStatus().click()
        objDriverList.btnConfirm().click()
    })

    it("Check when select active date later end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        var date = new Date()
        var month = date.getMonth()
        if(month == 11){
            var nextMonth = "01"
        }
        else{
            var nextMonth = parseInt(month, 10) + 2
        }
        const year = date.getFullYear()
        objDriverList.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()   
        objDriverList.dtpSelectDateTime().click()
        objDriverList.lblDayNextMonth("1").first().click()  
        objDriverList.lblHourED("09").scrollIntoView().click()     
        objDriverList.lblMinute("00").scrollIntoView().click()       
        objDriverList.btnOKSetED().click()
        if(nextMonth < 10){ 
            objDriverList.txtSelectDateTime().should('contain', year + '/' + '0' + nextMonth + '/' + '01' + '   ' + '09:00')
        }
        else{
            objDriverList.txtSelectDateTime().should('contain', year + '/' + nextMonth + '/' + '01' + '   ' + '09:00')
        }
        objDriverList.btnCancelSetStatus().click()
        objDriverList.btnConfirm().click()
    })

    it("Check when select active date earlier than end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.txtEffectiveDate().last().then(($el)=>{
            const effectiveDate = $el.text().trim().substr(8,2)
            const endDate = parseInt(effectiveDate, 10) - 1
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()   
            objDriverList.dtpSelectDateTime().click()
            objDriverList.lblDayDisable(endDate).should('not.to.selected')
            objDriverList.dtpSelectDateTime().click()
        })
        objDriverList.btnCancelSetStatus().click()
        objDriverList.btnConfirm().click()
    })

    it("Check when select active date matches end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.animationButtonEdit("active").first().click()
        objDriverList.txtEffectiveDate().last().then(($el)=>{
            var effectiveDate = $el.text().trim().substr(8,2)
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()   
            objDriverList.dtpSelectDateTime().click()
            var date = new Date()
            if(effectiveDate = date.getDate()){
                objDriverList.lblToDay(effectiveDate).click()
            }
            else{
                objDriverList.lblDay(effectiveDate).first().click()
            }
            const hour = $el.text().trim().substr(13,2)
            objDriverList.lblHour(hour).scrollIntoView().should('not.to.selected')
        })
        objDriverList.dtpSelectDateTime().dblclick()
        objDriverList.btnCancelSetStatus().click()
        objDriverList.btnConfirm().click()
    })

    it("Check change status from Pending to Inactive", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("active").first().click()
            objDriverList.txtEffectiveDate().last().click()
            objDriverList.lblYearDOB().click()
            objDriverList.btnNextYear().click()      
            objDriverList.lblYear("2030").click()      
            objDriverList.lblMonth("Oct").click()     
            objDriverList.lblDay("29").click()     
            objDriverList.lblHour("13").scrollIntoView().click()     
            objDriverList.lblMinute("00").scrollIntoView().click()       
            objDriverList.btnOKSetED().click()
            objDriverList.btnSetSetStatus().click()
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.animationButtonEdit("pending").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'inactive')
        })
    })

    it("Check change status from Pending to Active", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("active").first().click()
            objDriverList.txtEffectiveDate().last().click()
            objDriverList.lblYearDOB().click()
            objDriverList.btnNextYear().click()      
            objDriverList.lblYear("2030").click()      
            objDriverList.lblMonth("Oct").click()     
            objDriverList.lblDay("29").click()     
            objDriverList.lblHour("13").scrollIntoView().click()     
            objDriverList.lblMinute("00").scrollIntoView().click()       
            objDriverList.btnOKSetED().click()
            objDriverList.btnSetSetStatus().click()
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.animationButtonEdit("pending").first().click()
            objDriverList.txtEffectiveDate().last().click()     
            const date = new Date()
            objDriverList.lblYearDOB().click()
            objDriverList.btnPrevYear().click()
            objDriverList.lblYearToday(date.getFullYear()).click()
            objDriverList.lblMonth("Jan").click()
            cy.selectDay("today")
            objDriverList.lblHour(date.getHours()).scrollIntoView().click()
            objDriverList.lblMinute(date.getMinutes()).scrollIntoView().click()
            objDriverList.btnOKSetED().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'active')
        })
    })

    it("Check display on hover mouse to Driver inactive status without subcontractors", () => {
        objDriverList.ipSearchDriver().click()
        objDriverList.ipSearchDriver().type("ANL-D002607")
        objDriverList.ipSearchDriver().type('{enter}')
        objDriverList.lblStatus("inactive").trigger('mouseon')
        objDriverList.lblStatus("inactive").trigger('focus')
        objDriverList.errDriverNoSub().should('be.visible')
        objDriverList.errDriverNoSub().should('contain' , 'Driver do not have Subcontractor')
    })

    it("Check status changes from Inactive to Active when record has passed end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Inactive").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.animationButtonEdit("inactive").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Active").first().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.btnResetFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.cboFilDriverStatus().click()
            objDriverList.lblDriverLicenseType("Active").click()
            cy.wait(5000)
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{enter}')
            cy.wait(5000)
            objDriverList.lblStatus("active").should('be.visible')
        })
    })

    it("Check status changes from Inactive to Pending when record has passed end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Inactive").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.animationButtonEdit("inactive").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Active").first().click()
            objDriverList.txtEffectiveDate().last().click()      
            cy.selectYear(2030, 0)
            objDriverList.lblDay("29").click()     
            objDriverList.lblHour("13").scrollIntoView().click()     
            objDriverList.lblMinute("00").scrollIntoView().click()       
            objDriverList.btnOKSetED().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.btnResetFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.cboFilDriverStatus().click()
            objDriverList.lblDriverLicenseType("Pending").click()
            cy.wait(5000)
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{enter}')
            cy.wait(5000)
            objDriverList.lblStatus("pending").should('be.visible')
        })
    })

    it("Check change status from Inactive to Active", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("active").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverList.btnSetSetStatus().click()
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'inactive')
        })
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("inactive").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Active").first().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'active')
        })
    })

    it("Check change status from Inactive to Pending", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("active").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverList.btnSetSetStatus().click()
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'inactive')
        })
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el)=>{
            objDriverList.animationButtonEdit("inactive").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Active").first().click()
            objDriverList.txtEffectiveDate().last().click()
            objDriverList.lblYearDOB().click()
            objDriverList.btnNextYear().click()      
            objDriverList.lblYear("2030").click()      
            objDriverList.lblMonth("Oct").click()     
            objDriverList.lblDay("29").click()     
            objDriverList.lblHour("13").scrollIntoView().click()     
            objDriverList.lblMinute("00").scrollIntoView().click()       
            objDriverList.btnOKSetED().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.btnResetFilter().click()
            objDriverList.ipSearchDriver().clear()
            objDriverList.ipSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click({force : true})
            objDriverList.columnDriverStatus().first().should('have.text', 'pending')
        })
    })

    it("Check display of Import dropdown list", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").should('be.visible')
        objDriverList.lblImport("Import Photo").should('be.visible')
    })

    it("Check Download template button", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        objDriverList.btnMainFunction("Download Template").click()
        cy.verifyDownload('Template.xlsx')
    })

    it("Check upload a files that exceed 5 MB", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "masterdataSite/5GBTemplate.xls"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.common.msgError().should('have.text', 'File size is exceed the allowable limit of 5 MB')
        objDriverList.btnCancel().first().click()
    })

    it("Check upload files with file is not a support file type(except .xls, .xlsx file)", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "masterdataSite/wrongTemplate.docx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.common.msgError().should('be.visible')
        objDriverList.common.msgError().should('have.text', 'The selected file is not a support file type, please check your file.')
        objDriverList.btnCancel().first().click()
    })

    it("Check Cancel button when upload file successfully", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "driver/SampleImpDriver.xlsx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.btnCancel().first().click()
        objDriverList.lblMessageTitle().should('have.text', 'Quit importing')
        objDriverList.lblMessageContent().should('have.text', 'Are you sure you want to quit importing?  ')
        objDriverList.btnCancel().last().should('be.enabled')
        objDriverList.btnConfirm().should('be.enabled')
        objDriverList.btnCancel().last().click()
        objDriverList.btnCancel().first().click()
        objDriverList.btnConfirm().click()
    })

    it("Check click the Confirm button on Quit importing Driver pop-up", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "driver/SampleImpDriver.xlsx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.btnCancel().first().click()
        objDriverList.btnConfirm().click()
        objDriverList.btnMainFunction("Import").should('be.visible')
    })

    it("Check click the Cancel button on Quit importing Driver pop-up", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "driver/SampleImpDriver.xlsx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.btnCancel().first().click()
        objDriverList.btnCancel().last().click()
        objDriverList.btnMainFunction("Download Template").should('be.visible')
        objDriverList.btnCancel().first().click()
        objDriverList.btnConfirm().click()
    })

    it("Check delete icon on the Import Driver screen", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "driver/SampleImpDriver.xlsx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.btnDeteleFileImportDriver().click()
        objDriverList.btnImportDriver().should('be.disabled')
        objDriverList.btnCancel().first().click()
    })

    it("Check upload files with file has an invalid format by the template", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        const filePath = "masterdataSite/Template_weight.xlsx"
        objDriverList.impDriver().attachFile(filePath)
        objDriverList.btnImportDriver().click()
        objDriverList.common.msgError().should('have.text', 'The selected file is in the invalid format, please check your file.')
        objDriverList.btnCancel().first().click()
        objDriverList.btnConfirm().click()
    })

    it("Check View History log buttonn", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.lblHistory().last().should('be.visible')
    })

    it("Check Search box when don't input any keywords", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.btnSearchDriver().click()
    })

    it("Check display when hover on search box on View History log screen ", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipSearchDriver().should('be.visible')
    })

    it("Check search with keywords by Driver ID field on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with keywords by Driver name field on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        cy.wait(5000)
        objDriverList.columnFirstNameHis().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnFirstNameHis().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with keywords by Driver License Number field on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        cy.wait(5000)
        objDriverList.columnDriverLicenseNumberHisLog().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverLicenseNumberHisLog().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with exact keywords on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check search with partial keywords on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{backspace}')
            objDriverList.btnSearchDriver().click()
            objDriverList.columnDriverID().first().should('contain', $el.text().trim().substr(0, parseInt($el.text().length) - 1))
        })
    })

    it("Check search with special keywords on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipSearchDriver().click()
        objDriverList.ipSearchDriver().type("!@$%!@%@")
        objDriverList.btnSearchDriver().click()
        objDriverList.lblNoData().should('be.visible')
    })

    it("Check input keyword does not exist in the system on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        cy.wait(5000)
        cy.randomString(5).then((text)=>{
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type(text)
            objDriverList.btnSearchDriver().click()
            objDriverList.lblNoData().should('be.visible')
        })
    })

    it("Check delete inputted keywords on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{backspace}')
            objDriverList.btnSearchDriver().click()
            objDriverList.ipSearchDriver().click()
            objDriverList.btnDeleteSearchDriver().click({force : true})
            objDriverList.ipSearchDriver().should('have.value', '')
        })
    })

    it("Check press Enter from keyboard to search on Driver screen", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.btnSearchDriver().type('{enter}')
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check click on any number page", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.columnDriverID().first().then(($e1)=>{
            objDriverList.numberPagePag("2").first().click({force : true})
            objDriverList.numberPagePag("2").first().click({force : true})
            cy.wait(5000)
            objDriverList.columnDriverID().first().then(($e2)=>{
                if($e1.text().trim() != $e2.text().trim()){
                    cy.log("Pagination success to page 2")
                }
                else{
                    cy.log($e1.text().trim())
                    cy.log($e2.text().trim())
                }
            })
        })
    })

    it("Check click on > icon", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.numberPagePag("1").first().should('have.attr', 'class', 'number active')
        objDriverList.btnNextPage().click({force : true})
        objDriverList.btnNextPage().click({force : true})
        objDriverList.numberPagePag("2").first().should('have.attr', 'class', 'number active')
    })

    it("Check click on < icon", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.numberPagePag("2").first().click({force : true})
        objDriverList.numberPagePag("2").first().click({force : true})
        objDriverList.btnPrevPage().click()
        objDriverList.numberPagePag("1").first().should('have.attr', 'class', 'number active')
    })

    it("Check input number of page into Go to field when system has many page", () => {
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("2", {force : true})
        objDriverList.ipPageNumber().type('{enter}', {force : true})
        objDriverList.numberPagePag("2").first().should('have.attr', 'class', 'number active')
    })

    it("Check when input is letters on Go to textbox", () => {      
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("ab")
        objDriverList.ipPageNumber().should('have.value', '1')
    })

    it("Check when input is special characters on Go to textbox", () => {       
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipPageNumber().clear({force : true})
        objDriverList.ipPageNumber().type("@$!@$%@!%")
        objDriverList.ipPageNumber().should('have.value', '1')
    })

    it.skip("Check display of Driver when status driver is inactive", () => {       
        objDriverList.btnViewHistoryLog().click()
        objDriverList.ipSearchDriver().type("ANL-D002607")
        objDriverList.ipSearchDriver().type('{enter}')
        objDriverList.lblNoData().should('be.visible')
    })

    it("Check display of Driver when status driver is Aactive", () => {       
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el) => {
            cy.wait(2000)
            objDriverList.btnViewHistoryLog().click()
            cy.wait(2000)
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{enter}')
            objDriverList.columnDriverID().first().should('have.text', $el.text().trim())
        })
    })

    it("Check navigation when click on Create", () => {
        objDriverList.btnMainFunction("Create").click()
        objDriverList.lblCreateDriver().should('be.visible')
    })

    it("Check status changes from Active to Inactive when record has passed end date", () => {
        objDriverList.btnFilter().click()
        objDriverList.cboFilDriverStatus().click()
        objDriverList.lblDriverLicenseType("Active").click()
        cy.wait(5000)
        objDriverList.columnDriverID().first().then(($el) => {
            objDriverList.animationButtonEdit("active").first().click()
            objDriverList.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverList.btnSetSetStatus().click()
            cy.wait(5000)
            objDriverList.btnResetFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.btnFilter().click()
            objDriverList.cboFilDriverStatus().click()
            objDriverList.lblDriverLicenseType("Inactive").click()
            cy.wait(5000)
            objDriverList.ipSearchDriver().click()
            objDriverList.ipSearchDriver().type($el.text().trim())
            objDriverList.ipSearchDriver().type('{enter}')
            cy.wait(5000)
            objDriverList.lblStatus("inactive").should('be.visible')
        })
    })

    it("Check navigation when click on Import Driver", () => {
        objDriverList.btnMainFunction("Import").click()
        objDriverList.lblImport("Import Driver").click()
        objDriverList.lblImportDriver().should('be.visible')
    })
})