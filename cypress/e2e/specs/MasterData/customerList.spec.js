import CustomerList from "../../../support/pages/MasterData/components/customer/CustomerList"
import CustomerDetail from "../../../support/pages/MasterData/components/customer/CustomerDetail"
import CustomerImportFile from "../../../support/pages/MasterData/components/customer/CustomerImportFile"




describe("Customer List", () => {
    var data
    var customerList = new CustomerList
    var customerDetail = new CustomerDetail
    var customerImportFile = new CustomerImportFile

    beforeEach(function () {
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            customerList.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Customer")
        })
    })

    it(' TC_05: Check element of list screen \n' +
        'TC_07: Check search on "list" screen  \n  ' +
        "TC_08+ 09: Check results search when the system has data match with keywords including 'test'  \n  " +
        'TC_10: Check results search when the system has data match with keywords including "12"  \n ' +
        'TC_11: Check results when inputting keywords not include data of Customer ID, Customer Tax ID, Company registered name, Province  \n ' +
        "TC_12: Check results when the system hasn't data match with keywords 'abcdfsg'  \n  " +
        "TC_13:Check search box when don't input any keywords  \n  " +
        'TC_14:Check press enter from keyboard to search  \n  ' +
        'TC_15: Check search when user is on page different page 1', () => {
            customerList.isDefaultDataDisplayed()
            customerList.searchByValue("Bangkok")
            customerList.searchByValue("test")
            //   customerList.searchByValue("C000000000000179")
            customerList.searchByNotMatchValue("Bangkokkk")
            customerList.searchByNotMatchValue("abcdfsg")
            customerList.searchDefault()
            customerList.searchByValueEnter("Bangkok")
            customerList.searchDefault()
            customerList.navigateToPage(3)
            customerList.searchByValue("Bangkok")

        })


    it("TC_016: Check click on 'Filter' button \n  " +
        'TC_17: Check select data of all fields on Filter \n ' +
        "TC_18:Check click on 'filter' button on second time \n " +
        "TC_19: Check click 'Reset filter' button", () => {
            customerList.clickToButtonFilter()
            customerList.txtFilter("Customer Type").should("be.visible")
            customerList.txtFilter("Payment Type").should("be.visible")
            customerList.txtFilter("Status").should("be.visible")
            customerList.selectFilterDropdownList("Customer Type", "Individual")
            customerList.btnResetFilter().should("be.visible")
            customerList.selectFilterDropdownList("Payment Type", "Cash")
            customerList.clickToButtonFilter()
            customerList.txtFilter("Customer Type").should("not.exist")
            customerList.txtFilter("Payment Type").should("not.exist")
            customerList.txtFilter("Status").should("not.exist")
            customerList.iconPoint().should("be.visible")
            customerList.clickToButtonFilter()
            customerList.selectFilterDropdownList("Customer Type", "Individual")
            customerList.clickButtonRestFilter()
            customerList.btnResetFilter().should("not.exist")

        })
    it('TC_22: Check click on any number page \n  ' +
        "TC_23 :Check click on '>' icon  \n " +
        "TC_24: Check click on '<' icon  \n " +
        'TC_25: Check amount of customer displaying in the one page \n  ' +
        "TC_26: Check input number of page into 'Go to' field when system has many page \n " +
        "TC_27: Check input number of page into 'Go to' field when system has only 1 page ", () => {
            customerList.common.btnJumpPagePrev().should("be.disabled")
            customerList.navigateToPage("3")
            customerList.common.pageActive().should("have.text", "3")
            customerList.clickToJumpPage("next")
            customerList.common.pageActive().should("have.text", "4")
            cy.wait(1000)
            customerList.clickToJumpPage("previous")
            customerList.common.pageActive().should("have.text", "3")
            customerList.navigateToPage("1")
            customerList.selectPagination("75")
            customerList.selectPagination("50")
            customerList.selectPagination("30")
            customerList.inputPageNumberGoTo("3")
            customerList.searchByValue("CMC")


        })


    it("TC-32: Check 'set status ' popup is displayed /   \n " +
        "TC_40: Check change status from Active to inactive /  \n  " +
        "TC_42: Check change status from Inactive to Active /  \n " +
        "TC-48: Check click on the 'Cancel' button when changed some fields / \n " +
        "TC-49: Check click on the 'Cancel' button when don't have any change", () => {
            customerList.txtSearch().type("ANL-C000000000003588")
            customerList.btnSearch().click()
            cy.wait(2000)
            customerList.sliceBar("left").scrollTo('right', { easing: 'linear' })
            customerList.iconPen().first().click()
            customerList.selectStatusRadioButton("Inactive")
            customerList.cancelOrSetStatus("Set")
            customerList.msgUpdatedSuccess().should("be.visible")
            customerList.txtSearch().type("ANL-C000000000003588")
            customerList.btnSearch().click()
            customerList.columnStatus().should('have.text', 'Inactive')
            customerList.iconPen().first().click()
            customerList.selectStatusRadioButton("Active")
            customerList.cancelOrSetStatus("Set")
            customerList.msgUpdatedSuccess().should("be.visible")
            customerList.txtSearch().type("ANL-C000000000003588")
            customerList.btnSearch().click()
            customerList.columnStatus().should('have.text', 'Active')
            customerList.iconPen().first().click()
            customerList.selectStatusRadioButton("Inactive")
            customerList.cancelOrSetStatus("Cancel")
            customerList.clickQuitSetting("Confirm")
            customerList.iconPen().first().click()
            customerList.cancelOrSetStatus("Cancel")
            customerList.btnConfirmQuit().should("not.be.visible")

        })

    it("TC_50: Check click on 'View history log' button \n " +
        "TC-57: Check results when inputting keywords not include data of Customer ID, Customer Tax ID, Company registered name, Province on 'View history log' screen \n  " +
        "TC-58: Check results when the system hasn't data match with keywords on 'View history log' screen \n  " +
        "TC-60 : Check 'Search' box when don't input any keywords on the 'View history log' screen  \n " +
        "TC-61: Check press 'Enter' from keyboard to search on the 'View history log' screen \n  " +
        "TC-66: Check click on page 3 if the system has more than 3 page on the 'View history log' screen \n  " +
        "TC-63: Check search when user is on page different page 1 on the 'View history log' screen  \n  " +
        "Tc-68: Check click on ' > ' icon on the 'View history log' screen \n" +
        "TC-69 : Check click on ' < ' icon on the 'View history log' screen \n" +
        "TC-70: Check amount of customer displaying in the one page on the 'View history log' screen \n " +
        "TC-71: Check input number of page into 'Go to' field when system has many page on the 'View history log' screen", () => {
            customerList.btnViewHistoryLog().click()
            cy.wait(3000)
            customerList.txtHistory().should("be.visible")
            customerList.searchByValue("ALL NOW LOG111")
            customerList.searchByNotMatchValue("Creategggh")
            customerList.searchByNotMatchValue("Bangkokkk")
            customerList.searchDefault()
            customerList.searchByValueEnter("Bangkok")
            customerList.searchDefault()
            customerList.navigateToPage("3")
            customerList.common.pageActive().should("have.text", "3")
            customerList.searchByValueEnter("ALL NOW LOG111")

            customerList.searchDefault()
            customerList.common.btnJumpPagePrev().should("be.disabled")
            customerList.clickToJumpPage("next")
            customerList.common.pageActive().should("have.text", "2")
            cy.wait(1000)
            customerList.clickToJumpPage("previous")
            customerList.common.pageActive().should("have.text", "1")
            customerList.selectPagination("75")
            customerList.selectPagination("50")
            customerList.selectPagination("30")
            customerList.inputPageNumberGoTo("2")
        })

    it(" TC-79 :Check click on 'View history log' button (on the 'Detail customer' screen) \n" +
        "TC-86: Check results when inputting keywords not include data of Customer ID, Customer Tax ID, Company registered name, Province (on the 'Detail customer' screen) \n " +
        "TC-87: Check results when the system hasn't data match with keywords (on the 'Detail customer' screen) \n" +
        "TC-89: Check 'Search' box when don't input any keywords (on the 'Detail customer' screen)", () => {
            customerDetail = customerList.dblClickItemInListCustomer()
            customerDetail.btnEditOrViewHisoryLog("1").should("be.visible")
            customerDetail.btnEditOrViewHisoryLog("2").should("be.visible")
            customerDetail.clickToBtnEditOrViewHistoryLog("View History Log")
            cy.wait(2000)
            customerDetail.customerList.searchByNotMatchValue("Createhj")
            customerDetail.customerList.searchByNotMatchValue("abdcefg")
            customerDetail.customerList.searchDefault()

        })


    it("TC-106: Check UI of 'Import' screen \n " +
        "TC-110: Check 'Cancel' button in 'Import' screen \n" +
        "TC-112: Check click the 'Cancel' button on 'Confirm' popup of 'Import' screen \n"+
        "TC-111: Check click the 'Confirm' button on 'Confirm' popup of 'Import' screen \n" +
        "TC-121 : Import only file that has incorrect data : wrong format data of required fields \n" +
        "TC-137: Import file over 5MB" , () => {
            customerImportFile = customerList.clickBtnFile("Import")
            customerImportFile.txtImportFile().should("be.visible")
            customerImportFile.common.attachImportFile("masterdataSite/Template.xlsx")
            cy.wait(2000)
            customerImportFile.txtImportFile("Template.xlsx").should("be.visible")
            customerImportFile.clickBtnCancleOrImport("Cancel")
            customerImportFile.btnConfirmQuit().should("be.visible")
            customerImportFile.btnCancelQuit().click()
            customerImportFile.txtImportFile().should("be.visible")
            cy.wait(2000)
            customerImportFile.btnCancel().click()
            customerImportFile.btnConfirmQuit().should("be.visible")
            customerImportFile.btnConfirmQuit().click()


            customerImportFile = customerList.clickBtnFile("Import")
            customerImportFile.txtImportFile().should("be.visible")
            customerImportFile.common.attachImportFile("masterdataSite/Template.xlsx")
            customerImportFile.txtImportFile("Template.xlsx").should("be.visible")
            customerImportFile.clickBtnCancleOrImport("Import")
            customerImportFile.msgUnccess("The selected file is in the invalid format, please check your file.").should("be.visible")
            customerImportFile.btnCancel().click()
            customerImportFile.btnConfirmQuit().should("be.visible")
            customerImportFile.btnConfirmQuit().click()
            customerImportFile.customerList.clickBtnFile("Import")
            customerImportFile.common.attachImportFile("masterdataSite/5GBTemplate.xls")
            customerImportFile.msgUnccess("File size is exceed the allowable limit of 5 MB").should("be.visible")
            customerImportFile.btnCancel().click()
            customerImportFile.customerList.clickBtnFile("Import")
            customerImportFile.common.attachImportFile("masterdataSite/wrongTemplate.docx")
            customerImportFile.msgUnccess("The selected file is not a support file type, please check your file.").should("be.visible")

        })

     it("TC-139: Check 'Export file' button when the system exists data \n", ()=>{
        cy.wait(2000)
        customerList.header.btnExport().click()
        cy.wait(2000)
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
        // cy.randomNumber(10).then((number) => {
        //     objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "Mr", "Trinh", "Dai", "daicodong", "SonTay", "Bangkok", "Dusit", "Wachira Phayaban", "10300")
        //     objSite.common.searchByCondition(number)
        //     cy.wait(1000)
        //     objSite.common.searchResultWithOutValue().should("be.visible")
        //     objSite.common.openDetailsView()
        //     cy.wait(2000)
        //     objSite.sortByCreateDate("Created Date")
        //     // sort by status
        //     cy.reload()
        //     objSite.sortByStatus("Status")
        //     // set Status Site
        //     objSite.common.searchByCondition(number)
        //     cy.wait(1000)
        //     objSite.common.searchResultWithOutValue().should("be.visible")
        //     // objSite.scrollbar("left").scrollTo('right', {duration: 3000 })
        //     objSite.iconDelete().click()
        //     cy.wait(2000)
        //     objSite.selectAndSetStatus("Active", "Site’s status has been updated", number)
        //     // Check change the status from having end date to no end date 
        //     objSite.iconDelete().click()
        //     cy.wait(2000)
        //     objSite.selectAndSetStatus("Inactive", "Site’s status has been updated", number)
        // })
    })

  
})
