import CustomerList from "../../../support/pages/MasterData/components/customer/CustomerList.js"
import CustomerAdd from "../../../support/pages/MasterData/components/customer/CustomerAdd.js"


describe("Customer Add", () => {
    var data, thirteenNumbers = "1236523247495", specialCharacters = "@!$@#$@", twelveNumbers = "265458874562", nineNumbers = "586426549",
        filePath = "customer/Template.xlsx", fileOver5MB = "masterdataSite/5GBTemplate.xls",
        fileWrongTemplate = "masterdataSite/wrongTemplate.docx", fileInvalidContractCode = "customer/TemplateInvalidContractCode.xlsx",
        fileInvalidFormat = "masterdataSite/Template_weight.xlsx", fileEmpty = "customer/Empty.xlsx", fileIncorrectData = "customer/Incorrect.xlsx",
        email = "atutotest@gmail.com", address = "2677 ถ. พัฒนาการ แขวง พัฒนาการ แขวงสวนหลวง กรุงเทพมหานคร 10250"
    var customerList = new CustomerList()
    var customerAdd = new CustomerAdd()

    beforeEach(function () {
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            customerList.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Customer")
        })
    })

    it("TC-08: Check display all select fields \n" +
        "TC-11: Check input number into 'Company Tax ID' field \n" +
        'TC_15	Check input 13 characters into  "Company Tax ID" field \n' +
        'TC_18	Check input type in a language Thai or English into "Company Registered Name" field	Verify \n' +
        'TC_20	Check input 150 characters TH into "Company Registered Name" field \n' +
        'TC_21	Check input 150 characters EN into "Company Registered Name" field \n' +
        'TC_25	Check input 150 character into "Company Name (TH)" field \n' +
        'TC_30	Check input EN into "Company Name(EN)" field \n' +
        'TC_31	Check input 150 character into "Company Name(EN)" field \n' +
        'TC_34	Check select one value from dropdown list for "Customer type" field \n' +
        'TC_36	Check select one value from dropdown list for  "Payment type" field \n' +
        'TC_39	Check select value from "Customer Grouping L1" field \n' +
        'TC_42	Check select value from "Customer Grouping L2" field \n' +
        'TC_44	Check select one value from dropdown list for "Credit Term" field \n' +
        'TC_48	Check input number into "Company phone no."field \n' +
        'TC_51	Check input 8 character into "Company phone no." \n' +
        'TC_53	Check input 7 character into "Company phone no." \n' +
        'TC_55	Check input 50 characters into "Customer SAP code" field \n' +
        'TC_58	Check input only text into "Customer SAP code" field \n' +
        'TC_57	Check input valid value into "Customer SAP code" field include: text, number, special characters \n' +
        'TC_59	Check input only number into "Customer SAP code" field  \n' +
        'TC_60	Check input only special characters into "Customer SAP code" field \n' +
        'TC_63	Check UI of "Rate card" box \n' +
        'TC_64	Check UI of "Import" button when no company name (TH) is filled \n' +
        'TC_68	Check click "Cancel" button in "Import" default screen \n' +
        'TC_73	Check click on "trash" icon \n' +
        'TC_74	Check click on "Download template" button \n' +
        'TC_80	Check "Reupload file" button in "Unable to import rate card" screen \n' +
        'TC_81	Import file over 5MB \n' +
        'TC_130	Check select one value from dropdown list of  "Bank name" field', () => {
            cy.fixture("vehicle.json").then(function (listData) {
                data = listData

                customerAdd = customerList.clickCreateButton()
                //TC-08: Check display all select fields
                customerAdd.isAllInformationDisplayed()
                //TC_64	Check UI of "Import" button when no company name (TH) is filled
                customerAdd.verifyUIRateCardBeforeInputThaiName()


                //TC_11	Check input number into "Company Tax ID" field
                customerAdd.inputTextbox("Enter Company Tax ID", "2566412333787")
                //TC_15	Check input 13 characters into  "Company Tax ID" field	Verify  "Company Tax ID" field
                customerAdd.inputTextbox("Enter Company Tax ID", thirteenNumbers)
                //TC_18	Check input type in a language Thai or English into "Company Registered Name" field
                customerAdd.inputTextArea("Enter Company Registered Name", "บริษัท น้ำสะอาด")
                //TC_20	Check input 150 characters TH into "Company Registered Name" field
                customerAdd.inputTextArea("Enter Company Registered Name", listData.charactersTH150)
                //TC_21	Check input 150 characters EN into "Company Registered Name" field
                customerAdd.inputTextArea("Enter Company Registered Name", listData.charactersEN150)
                //TC_25	Check input 150 character into "Company Name (TH)" field
                customerAdd.inputTextArea("Enter Thai Company Name", listData.charactersTH150)
                //TC_30	Check input EN into "Company Name(EN)" field 
                customerAdd.inputTextArea("Enter Thai Company Name", "Automation Company")
                //TC_31	Check input 150 character into "Company Name(EN)" field
                customerAdd.inputTextArea("Enter English Company Name", listData.charactersEN150)
                //TC_34	Check select one value from dropdown list for "Customer type" field
                customerAdd.common.selectDataByClick("Select Customer type", "บุคคลธรรมดา")
                //TC_36	Check select one value from dropdown list for  "Payment type" field
                customerAdd.common.selectDataByClick("Select Payment type", "โอนเงินระหว่างบัญชี")
                //TC_39	Check select value from "Customer Grouping L1" field
                customerAdd.common.selectDataByClick("Select Customer Grouping L1", "CPG")
                //TC_42	Check select value from "Customer Grouping L2" field
                customerAdd.common.selectDataByClick("Select Customer Grouping L2", "CPG-Marko")
                //TC_44	Check select one value from dropdown list for "Credit Term" field
                customerAdd.common.selectDataByClick("Select Credit Term", "60 วัน")
                //TC_130	Check select one value from dropdown list of  "Bank name" field
                customerAdd.common.selectDataByClick("Select Bank Name", "ธนาคารซูมิโตโม มิตซุย ทรัสต์ (ไทย)")
                //TC_48	Check input number into "Company phone no."field
                //TC_51	Check input 8 character into "Company phone no."
                customerAdd.inputTextbox("Enter Mobile Phone no.", 55886614)
                //TC_53	Check input 7 character into "Company phone no."
                customerAdd.inputTextbox("Enter Mobile Phone no.", 1234567)
                //TC_55	Check input 50 characters into "Customer SAP code" field
                //TC_58	Check input only text into "Customer SAP code" field 
                customerAdd.inputTextArea("Enter Customer SAP Code", listData.fiftyCharacters)
                //TC_57	Check input valid value into "Customer SAP code" field include: text, number, special characters
                customerAdd.inputTextArea("Enter Customer SAP Code", "@!@ASsvf234")
                //TC_59	Check input only number into "Customer SAP code" field 
                customerAdd.inputTextArea("Enter Customer SAP Code", thirteenNumbers)
                //TC_60	Check input only special characters into "Customer SAP code" field 
                customerAdd.inputTextArea("Enter Customer SAP Code", specialCharacters)
                //TC_63	Check UI of "Rate card" box
                customerAdd.verifyUIRateCardAfterInputThaiName()
                //TC_68	Check click "Cancel" button in "Import" default screen
                customerAdd.btnImport().click({ force: true })
                customerAdd.common.btnCancel().click({ force: true })
                //TC_73	Check click on "trash" icon
                customerAdd.btnImport().click({ force: true })
                customerAdd.common.attachImportFile(filePath)
                customerAdd.common.icTrash().should("be.visible")
                cy.wait(1000)
                customerAdd.common.removeImportFile()
                customerAdd.common.btnImportPopupAttachFileDisabled().should("have.attr", "disabled", "disabled")
                //TC_74	Check click on "Download template" button
                customerAdd.common.verifyDownload("Download Template")
                //TC_80	Check "Reupload file" button in "Unable to import rate card" screen
                customerAdd.btnImport().click({ force: true })
                customerAdd.common.attachImportFile(fileIncorrectData)
                cy.wait(1000)
                customerAdd.common.btnImportPopupAttachFileEnable().click({ force: true })
                customerAdd.common.btnImportPopupAttachFileEnable().click({ force: true })
                customerAdd.common.attachImportFile(filePath)
                customerAdd.common.icTrash().should("be.visible")
                //TC_81	Import file over 5MB
                customerAdd.common.attachImportFile(fileOver5MB)
                customerAdd.common.msgAlertErrorShouldHaveText("File size is exceed the allowable limit of 5 MB")
            })
        })

    it('TC_133	Check input number into "Account Number"field \n' +
        'TC_136	Check input 12 characters into  "Account Number " field \n' +
        'TC_140	Check input text into "Bank Account  Name"field \n' +
        'TC_141	Check input number into "Bank Account  Name"field \n' +
        'TC_142	Check input special characters into  "Bank Account  Name" field \n' +
        'TC_143	Check input 150 characters into  "Bank Account  Name " field \n' +
        'TC_148	Check input 30 characters into  "First name " field \n' +
        'TC_152	Check input 30 characters into  "Last name  " field \n' +
        'TC_155	Add correct email into "Key contact email" \n' +
        'TC_157	Add  email without "@" for "Key contact email" field \n' +
        'TC_158	Add  email without "." for "Key contact email" field \n' +
        'TC_159	Add  email without domain for "Key contact email" field \n' +
        'TC_161	Check select one value from dropdown list country code number  for  "Moblie  phone  no." field \n' +
        'TC_162	Check input 9 characters into "Moblie  phone  no." \n' +
        'TC_168	Check select one value from dropdown list code number for  "Landline phone no." field \n' +
        'TC_169	Check input 8 character into "Landline phone no." \n' +
        'TC_174	Check input 8 character into "Extension"', () => {
            cy.fixture("vehicle.json").then(function (listData) {
                data = listData

                customerAdd = customerList.clickCreateButton()
                //TC_133	Check input number into "Account Number"field
                customerAdd.inputTextbox("Enter Account Number", 55886614)
                //TC_136	Check input 12 characters into  "Account Number " field
                customerAdd.inputTextbox("Enter Account Number", twelveNumbers)
                //TC_140	Check input text into "Bank Account  Name"field
                customerAdd.inputTextArea("Enter Bank Account Name", "Bank Account name")
                //TC_141	Check input number into "Bank Account  Name"field
                customerAdd.inputTextArea("Enter Bank Account Name", twelveNumbers)
                //TC_142	Check input special characters into  "Bank Account  Name" field
                customerAdd.inputTextArea("Enter Bank Account Name", specialCharacters)
                //TC_143	Check input 150 characters into  "Bank Account  Name " field
                customerAdd.inputTextArea("Enter Bank Account Name", listData.fiftyCharacters)
                //TC_148	Check input 30 characters into  "First name " field
                customerAdd.inputTextbox("Enter Key Contact Person’s first name", listData.thirtyCharacters)
                //TC_152	Check input 30 characters into  "Last name  " field
                customerAdd.inputTextbox("Enter Key Contact Person’s last name", listData.thirtyCharacters)
                //TC_155	Add correct email into "Key contact email"
                customerAdd.inputTextbox("Enter Key Contact Person’s email", email)
                //TC_157	Add  email without "@" for "Key contact email" field
                customerAdd.inputTextbox("Enter Key Contact Person’s email", "emailgmail.com")
                customerAdd.common.msgErrorShouldBe("Invalid Email")
                //TC_158	Add  email without "." for "Key contact email" field
                customerAdd.inputTextbox("Enter Key Contact Person’s email", "email@gmailcom")
                customerAdd.common.msgErrorShouldBe("Invalid Email")
                //TC_159	Add  email without domain for "Key contact email" field
                customerAdd.inputTextbox("Enter Key Contact Person’s email", "email@gmail.")
                customerAdd.common.msgErrorShouldBe("Invalid Email")
                //TC_161	Check select one value from dropdown list country code number  for  "Moblie  phone  no." field
                customerAdd.selectMobilePhonePrefix("Mobile Phone No.", 66)
                //TC_162	Check input 9 characters into "Moblie  phone  no."
                customerAdd.inputTextbox("Enter Mobile Phone no.", nineNumbers)
                //TC_168	Check select one value from dropdown list code number for  "Landline phone no." field
                customerAdd.selectMobilePhonePrefix("Landline phone no.", 66)
                //TC_169	Check input 8 character into "Landline phone no."
                customerAdd.inputTextbox("Enter Landline phone no.", nineNumbers)
                //TC_174	Check input 8 character into "Extension"
                customerAdd.inputTextbox("Enter Landline phone no.Extension", nineNumbers)
            })
        })

    it('TC_177	Check Add key contact \n' +
        'TC_178	Check Add over 3 key contact person \n' +
        'TC_179	Check "Delete" key contact person \n' +
        'TC_180	Check input text into "Address "field \n' +
        'TC_181	Check input number into "Address "field \n' +
        'TC_182	Check input special characters into  "Address " field \n' +
        'TC_183	Check input 150 characters into  "Address " field \n' +
        'TC_189	Check select one value from dropdown list code number for "Province" field \n' +
        'TC_190	Search province match with data in system \n' +
        'TC_191	Search province does not match with data in system \n' +
        'TC_194	Check select one value from dropdown list code number for  "District" field \n' +
        'TC_197	Check select one value from dropdown list code number for "Sub-District" field \n' +
        'TC_199	Check select value from dropdown list "Postal code" \n' +
        'TC_201	Check select one value from dropdown list code number  "Postal code" field \n' +
        'TC_203	Check Latitude and Longitude field after click "Confirm location" button \n' +
        'TC_208	Check click on the "Confirm location" button', () => {
            cy.fixture("vehicle.json").then(function (listData) {
                data = listData

                customerAdd = customerList.clickCreateButton()
                //TC_177	Check Add key contact
                customerAdd.verifyClickAddKeyContact()
                //TC_178	Check Add over 3 key contact person
                customerAdd.verifyClickAddKeyContact()
                customerAdd.verifyClickAddKeyContact()
                customerAdd.btnAddKeyContactDisable().should("have.attr", "disabled", "disabled")
                //TC_179	Check "Delete" key contact person
                customerAdd.removeKeyContact()
                customerAdd.removeKeyContact()
                //TC_180	Check input text into "Address "field
                customerAdd.inputTextArea("Enter Address", address)
                //TC_181	Check input number into "Address "field
                customerAdd.inputTextArea("Enter Address", thirteenNumbers)
                //TC_182	Check input special characters into  "Address " field
                customerAdd.inputTextArea("Enter Address", specialCharacters)
                //TC_183	Check input 150 characters into  "Address " field
                customerAdd.inputTextArea("Enter Address", listData.fiftyCharacters)
                //TC_189	Check select one value from dropdown list code number for "Province" field
                customerAdd.common.ipSelectBox("Select Province").scrollIntoView().clear({ force: true })
                customerAdd.common.ipSelectBox("Select Province").click({ force: true })
                customerAdd.common.selectRandomValue()
                customerAdd.btnConfirmLocationDisable("Confirm Location").should("have.attr", "disabled", "disabled")
                //TC_190	Search province match with data in system
                customerAdd.common.ipSelectBox("Select Province").clear({ force: true })
                customerAdd.common.ipSelectBox("Select Province").click({ force: true })
                var searchKeyProvince = "กาญจนบุรี"
                customerAdd.common.ipSelectBox("Select Province").type(searchKeyProvince)
                customerAdd.verifySearchSuggestion()
                //TC_191	Search province does not match with data in system
                customerAdd.common.ipSelectBox("Select Province").clear({ force: true })
                customerAdd.common.ipSelectBox("Select Province").click({ force: true })
                customerAdd.common.ipSelectBox("Select Province").type(specialCharacters)
                customerAdd.liSearchSuggestion().should("not.exist")
                //TC_194	Check select one value from dropdown list code number for  "District" field
                customerAdd.common.ipSelectBox("Select Province").clear({ force: true })
                cy.wait(1000)
                customerAdd.common.ipSelectBox("Select Province").click({ force: true })
                customerAdd.common.ipSelectBox("Select Province").type(searchKeyProvince)
                customerAdd.verifySearchSuggestion()
                customerAdd.common.ipSelectBox("Select Sub-District").should("have.attr", "disabled", "disabled")
                customerAdd.common.ipSelectBox("Select District").click({ force: true })
                customerAdd.common.selectRandomValue()
                customerAdd.common.ipSelectBox("Select Sub-District").should("be.visible")
                customerAdd.btnConfirmLocationDisable("Confirm Location").should("have.attr", "disabled", "disabled")

                //TC_197	Check select one value from dropdown list code number for "Sub-District" field
                customerAdd.common.ipSelectBox("Select Postal Code").should("have.attr", "disabled", "disabled")
                customerAdd.common.ipSelectBox("Select Sub-District").click({ force: true })
                customerAdd.common.selectRandomValue()
                customerAdd.common.ipSelectBox("Select Postal Code").should("be.visible")
                customerAdd.btnConfirmLocationDisable("Confirm Location").should("have.attr", "disabled", "disabled")
                //TC_199	Check select value from dropdown list "Postal code"
                //TC_201	Check select one value from dropdown list code number  "Postal code" field
                customerAdd.common.ipSelectBox("Select Postal Code").click({ force: true })
                customerAdd.common.selectRandomValue()
                customerAdd.btnConfirm("Confirm Location").should("be.visible")
                //TC_203	Check Latitude and Longitude field after click "Confirm location" button
                //TC_208	Check click on the "Confirm location" button
                customerAdd.btnConfirm("Confirm Location").click()
                customerAdd.countTextInputted()
            })
        })

    it('TC_235	Create customer successfully', () => {
        customerAdd = customerList.clickCreateButton()
        //TC_235	Create customer successfully
        customerAdd.createCustomer("empty", "empty", "Save", "empty", "empty")
    })

    it('TC_236	Create customer unsuccessfully \n' +
        'TC_237	Check "Confirm" button on "Confirm" popup \n' +
        'TC_238	Check "Cancel" button on "Confirm" popup', () => {
            customerAdd = customerList.clickCreateButton()
            //TC_236	Create customer unsuccessfully
            customerAdd.createCustomer("empty", "empty", "Cancel", "Cancel", "no")
            customerAdd.common.btnConfirmHintPopupVer2("Cancel").click()
            //TC_238	Check "Cancel" button on "Confirm" popup
            cy.wait(1000)
            customerAdd.createCustomer("empty", "empty", "Cancel", "Cancel", "yes")
            //TC_237	Check "Confirm" button on "Confirm" popup
            cy.wait(1000)
            customerAdd.createCustomer("empty", "empty", "Cancel", "Confirm", "yes")
        })

    it('TC_82	Import file wrong format (except .xls, .xlsx file) \n' +
        'TC_84	Import file when input special character, text, number of "Contract code" in the import file \n' +
        'TC_91	Import file that different template rate card file \n' +
        'TC_101	Import file that have empty data \n' +
        'TC_112	Search by Service type on "Select Rate Card to active" screen \n' +
        'TC_113	Search by Vehicle type on "Select Rate Card to active" screen \n' +
        'TC_114	Search by Rate card type on "Select Rate Card to active" screen \n' +
        'TC_115	Search by unexisting Contract code on "Select rate card to active" screen', () => {
            var serviceType = "Backhaul", rateCardType = "Baht/Trip", vehicleType = "4 ล้อตู้แห้งอะลูมิเนียม"
            customerAdd = customerList.clickCreateButton()
            customerAdd.inputTextArea("Enter Thai Company Name", "Automation Company")
            //TC_82	Import file wrong format (except .xls, .xlsx file)
            customerAdd.btnImport().click({ force: true })
            customerAdd.common.attachImportFile(fileWrongTemplate)
            customerAdd.common.msgAlertErrorShouldHaveText("The selected file is not a support file type, please check your file.")
            // customerAdd.common.removeImportFile()
            //TC_84	Import file when input special character, text, number of "Contract code" in the import file
            cy.wait(1000)
            customerAdd.common.attachImportFile(fileInvalidContractCode)
            customerAdd.common.icTrash().should("be.visible")
            //TC_91	Import file that different template rate card file
            cy.wait(1000)
            customerAdd.common.removeImportFile()
            customerAdd.common.attachImportFile(fileInvalidFormat)
            customerAdd.common.btnImportPopupAttachFileEnable().click({ force: true })
            customerAdd.common.msgAlertErrorShouldHaveText("The selected file is in the invalid format, please check your file.")
            //TC_101	Import file that have empty data
            cy.wait(2000)
            customerAdd.common.removeImportFile()
            cy.wait(1000)
            customerAdd.common.attachImportFile(fileEmpty)
            cy.wait(1000)
            customerAdd.common.btnImportPopupAttachFileEnable().click({ force: true })
            customerAdd.common.msgAlertErrorShouldHaveText("The selected file is empty, please check your file.")
            customerAdd.common.icCloseDialog().click({ force: true })
            customerAdd.common.btnConfirmHintPopupVer2("Confirm").click({ force: true })
            //TC_112	Search by Service type on "Select Rate Card to active" screen
            customerAdd.importFileSuccess(filePath)
            customerAdd.common.searchByCondition(serviceType)
            customerAdd.common.searchResultWithOutValue().should("be.visible")
            //TC_113	Search by Vehicle type on "Select Rate Card to active" screen
            customerAdd.common.searchByCondition(rateCardType)
            customerAdd.common.searchResultWithOutValue().should("be.visible")
            //TC_114	Search by Rate card type on "Select Rate Card to active" screen
            customerAdd.common.searchByCondition(vehicleType)
            customerAdd.common.searchResultWithOutValue().should("be.visible")
            //TC_115	Search by unexisting Contract code on "Select rate card to active" screen
            customerAdd.common.searchByCondition(specialCharacters)
            customerAdd.common.searchResultWithOutValue().should("not.exist")
        })

    it('TC_211	Check redirect to status pop up \n' +
        'TC_213	Check tick on Inactive tickbox for "Status" \n' +
        'TC_214	Check click on "Active date/time" field \n' +
        'TC_221	Check tick on Select date / time tickbox for "End date / time " \n' +
        'TC_222	Check click on "Select date / time " field \n' +
        'TC_233	Check button [Cancel] \n' +
        'TC_234	Check button [X]', () => {
            customerAdd = customerList.clickCreateButton()
            //TC_211	Check redirect to status pop up 
            customerAdd.common.openSetStatusPopUp()
            customerAdd.addVehicle.verifyDefaultStatusPopUp()
            //TC_213	Check tick on Inactive tickbox for "Status"
            customerAdd.addVehicle.checkRadioStatusInActive()
            //TC_214	Check click on "Active date/time" field
            customerAdd.addVehicle.checkRadioStatusActive()
            //TC_221	Check tick on Select date / time tickbox for "End date / time "
            customerAdd.addVehicle.checkRadioEndDateTime()
            //TC_222	Check click on "Select date / time " field
            customerAdd.addVehicle.verifySelectDateTimePickerDisplay()
            //TC_233	Check button [Cancel]
            cy.wait(1000)
            customerAdd.addVehicle.verifyCancelSetStatus()
            customerAdd.common.btnConfirmHintPopupVer2("Confirm").click()
            customerAdd.common.icCloseDialog().should("not.exist")
            //TC_234	Check button [X]
            cy.wait(1000)
            customerAdd.common.openSetStatusPopUp({ force: true })
            customerAdd.common.closeSetStatusPopUp()
        })

    it('TC_17	Get blank into "Company Tax ID" field \n' +
        'TC_23	Get blank into "Company Registered Name" field \n' +
        'TC_27	Get blank into "Company Name(TH)" field \n' +
        'TC_35	Get blank into "Customer type" field \n' +
        'TC_37	Get blank into "Payment type" field \n' +
        'TC_38	Verify list data of "Customer Grouping L1" field \n' +
        'TC_40	Leave blank "Customer Grouping L1" field \n' +
        'TC_41	Verify list data of "Customer Grouping L2" field \n' +
        'TC_43	Leave blank "Customer Grouping L2" field \n' +
        'TC_45	Get blank into "Credit Term" field \n' +
        'TC_54	Get blank into "Company phone no." field \n' +
        'TC_62	Leave blank "Customer SAP code" field \n' +
        'TC_129	Check display data dropdown list "Bank name" \n' +
        'TC_131	Get blank into "Bank name" field \n' +
        'TC_138	Get blank into "Account Number " field \n' +
        'TC_145	Get blank into "Bank Account  Name " field \n' +
        'TC_151	Get blank into "Last name" field \n' +
        'TC_156	Get blank into "Key contact email" field \n' +
        'TC_167	Get blank into "Moblie  phone  no." field \n' +
        'TC_173	Get blank into "Landline phone no." field \n' +
        'TC_187	Get blank into "Address" field \n' +
        'TC_192	Get blank into "Province" field \n' +
        'TC_195	Get blank into "District" field \n' +
        'TC_198	Get blank into "Sub-District" field \n' +
        'TC_202	Get blank into "Postal code" field \n' +
        'TC_207	Verify the "Confirm location" button when inputting only Latitude and Longitude on "Location" block', () => {
            cy.fixture("customer.json").then(function (listData) {
                data = listData

                customerAdd = customerList.clickCreateButton()
                //TC_17	Get blank into "Company Tax ID" field
                //TC_35	Get blank into "Customer type" field
                //TC_37	Get blank into "Payment type" field
                //TC_40	Leave blank "Customer Grouping L1" field
                //TC_43	Leave blank "Customer Grouping L2" field
                //TC_45	Get blank into "Credit Term" field
                //TC_54	Get blank into "Company phone no." field
                //TC_62	Leave blank "Customer SAP code" field
                //TC_131	Get blank into "Bank name" field
                //TC_138	Get blank into "Account Number " field
                //TC_145	Get blank into "Bank Account  Name " field
                //TC_151	Get blank into "Last name" field
                //TC_156	Get blank into "Key contact email" field
                //TC_167	Get blank into "Moblie  phone  no." field
                //TC_173	Get blank into "Landline phone no." field
                //TC_187	Get blank into "Address" field
                //TC_192	Get blank into "Province" field
                //TC_195	Get blank into "District" field
                //TC_198	Get blank into "Sub-District" field
                //TC_202	Get blank into "Postal code" field

                //TC_23	Get blank into "Company Registered Name" field
                customerAdd.inputTextArea("Enter Company Registered Name", "test")
                customerAdd.txaInformation("Enter Company Registered Name").first().clear()
                customerAdd.common.msgErrorShouldBe("The Company Registered Name is required")
                //TC_27	Get blank into "Company Name(TH)" field
                customerAdd.inputTextArea("Enter Thai Company Name", "test")
                customerAdd.txaInformation("Enter Thai Company Name").first().clear()
                customerAdd.common.msgErrorShouldBe("The Company Name (TH) is required")
                //TC_38	Verify list data of "Customer Grouping L1" field
                customerAdd.common.checkDataServiceGroup("Select Customer Grouping L1", listData.listCustomerGroup1)
                //TC_41	Verify list data of "Customer Grouping L2" field
                //failed bug
                // customerAdd.common.checkDataServiceGroup("Select Customer Grouping L2", listData.listCustomerGroup2)
                //TC_129	Check display data dropdown list "Bank name"
                //failed bug
                // customerAdd.common.checkDataServiceGroup("Select Bank Name", listData.listBankName)
                //TC_207	Verify the "Confirm location" button when inputting only Latitude and Longitude on "Location" block
                customerAdd.inputTextbox("Enter Latitude (Ex.41.40338)", nineNumbers)
                customerAdd.inputTextbox("Enter Longitude (Ex.41.40338)", twelveNumbers)
                customerAdd.btnConfirmLocationDisable("Confirm Location").should("have.attr", "disabled", "disabled")


                customerAdd.btnConfirm("Save").should("have.attr", "disabled", "disabled")
            })

        })
})
