import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import { Utility } from "../../../support/utility"

describe("Checking Our - Line management", () => {

    var objLinemanagement, data, objLogin, objHome

    beforeEach("TMS-Line management", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(()=>{
        cy.wait(10000)
        objHome.logout()
    })

    it("TC_285 - Check add to add line", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.deleteLine("line name", line)
        })
    })

    it("TC_286 - Check import successfully file to system of line management's", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        var filePath = "shippingorder/LineManagementImp.xlsx"
        objLinemanagement.importLine(filePath)
        objLinemanagement.verifyNewLine("Alpha")
        objLinemanagement.deleteLine("line name", "Alpha")
    })

    it("TC_287 - Check import fail file to system of line management's", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        var filePath = "data_test.json"
        objLinemanagement.importFailFile(filePath)
    })

    it("TC_288 - Check delete success of line management's", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.deleteLine("line name", line)
        })
    })

    it("TC_289 - Check disable success of line management's", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.disabledLine("line name", line)
        })
    })

    it("TC_290 - Check enable success of line management's", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.enabledLine("line name", line)
        })
    })

    it("TC_293 - Check all required field when creating the new Line", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.addNewLine(line, "13:00")
            objLinemanagement.common.txtError().should('have.text', 'The line name already exists')
            objLinemanagement.btnCloseAddNewPopup().click()
            objLinemanagement.deleteLine("line name", line)
        })
    })

    it("TC_294 - Check add route", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
        objLinemanagement.addNewLine(line, "13:00")
        objLinemanagement.addRoute("line name", line)
        objLinemanagement.verifyNewRoute()
        objLinemanagement.deleteLine("line name", line)
        })
    })

    it("TC_295 - Check delete route", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
        objLinemanagement.addNewLine(line, "13:00")
        objLinemanagement.addRoute("line name", line)
        objLinemanagement.deleteRoute()
        objLinemanagement.deleteLine("line name", line)
        })
    })

    it("TC_296 - Check edit information line management", () => {
        objLinemanagement = objHome.leftMenu.navigateToSubPage("Master Data","Line Management")
        cy.wait(2000)
        cy.randomString(10).then((line) => {
            objLinemanagement.addNewLine(line, "13:00")
            cy.randomString(10).then((line2)=>{
                objLinemanagement.editLine("line name", line, line2, "14:00")
                objLinemanagement.verifyEditLine("line name", line2, line2)
                objLinemanagement.deleteLine("line name", line2)
            })
        })
    })
})