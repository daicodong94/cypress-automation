import ShippingOrder from "../shippingorder/ShippingOrder";
import DeliveryPlan from "../plan/DeliveryPlan";
import Schedule from "../schedule/Schedule";
import ShippingOrderTracking from "../track/ShippingOrderTracking"
import DispatchOrderTracking from "../track/DispatchOrderTracking"
import Pickup from "../Implement/PickUp.js";
import DeliveryGood from "../Implement/DeliveryGood.js";
import Function from "../setup/Function.js";
import Abnormal from "../abnormal/Abnormal.js";
import OnTheWay from "../Implement/OnTheWay.js";
import ItemType from "../our/ItemType.js";
import LineManagement from "../our/LineManagement.js";
import UserOrganization from "../our/UserOrganization.js";
import ShippersReceipt from "../receipt/ShippersReceipt.js";
import DriverReceipt from "../receipt/DriverReceipt.js";
import ServiceProduct from "../our/ServiceProduct.js";
import AccountInformation from "../setup/AccountInformation.js";
import Permission from "../setup/Permission.js";
import ExceptionType from "../setup/ExceptionType.js";
import ServiceItem from "../our/ServiceItem.js";
import Driver from "../../../MasterData/components/driver/Driver.js";
import CustomerList from "../../../MasterData/components/customer/CustomerList";
import VehicleList from "../../../MasterData/components/vehicle/VehicleList";
import SubcontractorList from "../../../MasterData/components/subcontractor/SubcontractorList";
import SiteList from "../../../MasterData/components/site/SiteList";
import ActivityTracking from "../activitytracking/ActivityTracking";
import Address from "../../../MasterData/components/Address/Address";
import RouteCode from "../../../MasterData/components/RouteCode/RouteCode";


class LeftMenu {

    lnkMenu(menu) {
        return cy.xpath("//div[@class='el-submenu__title']//span[contains(text(),'" + menu + "')]")
    }

    lnkMenuWithoutSubMenu(menu) {
        return cy.xpath("//div/li[@class='el-menu-item']//span[contains(text(),'" + menu + "')]")
    }

    lnkMenuTrack() {
        return cy.xpath("//ul[@id='J-left-menu']//li//a//span[text()='Track ']")
    }

    lnkSubMenuPageActive(subMenu) {
        return cy.xpath("//ul[@class='el-menu el-menu--inline']//li//span[contains(text(),'" + subMenu + "')]")
    }

    lnkSubMenu(subMenu) {
        return cy.xpath("//li[@role='menuitem']//span[contains(text(),'" + subMenu + "')]")
    }

    lnkSubMenuDriver() {
        return cy.xpath("(//li//span[contains(text(),'Driver')])[2]")
    }

    navigateToPage(menu) {
        this.lnkMenuWithoutSubMenu(menu).should("be.visible").click({ force: true })
        cy.wait(1000)
        // this.lnkMenuWithoutSubMenu(menu).should("be.visible").click({ force: true })
        // cy.wait(1000)
        if (menu == "Shipping order") {
            return new ShippingOrder()
        }
        if (menu == "Schedule") {
            return new Schedule()
        }
        if (menu == "Abnormal") {
            return new Abnormal()
        }
        if (menu = "Activity Tracking") {
            return new ActivityTracking()
        }
        cy.wait(2000)
    }

    navigateToSubPage(menu, subMenu) {
        this.lnkMenu(menu, { timeout: 10000 }).should("be.visible").click({ force: true })
        this.lnkSubMenu(subMenu, { timeout: 10000 }).click({ multiple: true, force: true })
        // }
        cy.wait(1000)
        if (subMenu == "Delivery Plan") {
            return new DeliveryPlan()
        }
        if (subMenu == "Shipping Order Tracking") {
            return new ShippingOrderTracking()
        }
        if (subMenu == "Dispatch Order Tracking") {
            return new DispatchOrderTracking()
        }
        if (subMenu == "Shippers Receipt") {
            return new ShippersReceipt()
        }
        if (subMenu == "Driver Receipt") {
            return new DriverReceipt()
        }
        if (subMenu == "Site") {
            return new SiteList()
        }
        if (subMenu == "Function") {
            return new Function()
        }
        if (subMenu == "Pick Up") {
            return new Pickup()
        }
        if (subMenu == "On the way") {
            return new OnTheWay()
        }
        if (subMenu == "Deliver Goods") {
            return new DeliveryGood()
        }
        if (subMenu == "Item Type") {
            return new ItemType()
        }
        if (subMenu == "Line Management") {
            return new LineManagement()
        }
        if (subMenu == "Account Information") {
            return new AccountInformation()
        }
        if (subMenu == "Permission") {
            return new Permission()
        }
        if (subMenu == "Exception Type") {
            return new ExceptionType()
        }
        if (subMenu == "User Organization") {
            return new UserOrganization()
        }
        if (subMenu == "Service Product") {
            return new ServiceProduct()
        }
        if (subMenu == "Service Item ") {
            return new ServiceItem()
        }
        if (subMenu == "Driver") {
            return new Driver()
        }
        if (subMenu == "Customer") {
            return new CustomerList()
        }
        if (subMenu == "Vehicle") {
            return new VehicleList()
        }
        if (subMenu == "Site") {
            return new SiteList()
        }
        if (subMenu == "Subcontractor") {
            return new SubcontractorList()
        }
        if (subMenu == "Address") {
            return new Address()
        }
        if (subMenu == "Route Code") {
            return new RouteCode()
        }
        cy.wait(2000)
    }

    navigateToSubPageDriver2() {
        this.lnkSubMenuDriver({ timeout: 10000 }).should("be.visible").click({ force: true })
        cy.wait(1000)
        return new Driver()
    }

    navigateToSubPageActive(subMenu) {
        this.lnkSubMenuPageActive(subMenu).should("be.visible").click({ force: true })
    }

    btnCloseMenu() {
        return cy.xpath("//i[@class='el-icon-circle-close']")
    }

    closeTab() {
        this.btnCloseMenu().last().scrollIntoView().click({ force: true })
    }
}


export default LeftMenu;