import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SubcontractorList from "./SubcontractorList";

class SubcontractorAdd {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
    }

    createSubcontractor(thaiSub, compName, landline, extension, action, confirm, click, verify) {
        cy.randomNumber(13).then((tax) => {
            this.common.inputDataToField("Enter Company Tax ID", tax)
        })

        if (compName == "generate") {
            cy.randomStringAndNumber(7).then((random) => {
                this.common.inputDataToTextarea("Enter Company Registered Name", random)
            })
        } else {
            this.common.inputDataToTextarea("Enter Company Registered Name", compName)
        }

        if (thaiSub == "generate") {
            cy.randomString(7).then((random) => {
                this.common.inputDataToTextarea("Enter Thai Company Name", random)
            })
        } else {
            this.common.inputDataToTextarea("Enter Thai Company Name", thaiSub)
        }
        cy.randomString(7).then((EngComp) => {
            this.common.inputDataToTextarea("Enter English Company Name", EngComp)
        })

        this.common.ipSelectBox("Select Payment type").click({ force: true })
        this.common.selectRandomValue()
        this.common.ipSelectBox("Select Credit Term").click({ force: true })
        this.common.selectRandomValue()

        cy.randomNumber(8).then((phone) => {
            this.common.ipSelectBox("Enter Phone no.", phone)
        })
        cy.randomString(8).then((customer) => {
            this.common.inputDataToTextarea("Enter Vendor SAP Code", customer)
        })

        this.common.ipSelectBox("Select Bank Name").click({ force: true })
        // cy.wait(2000)
        var bankName = "ธนาคารกรุงเทพ"
        this.common.ipSelectBox("Select Bank Name").type(bankName)
        cy.wait(1000)
        this.common.verifySearchSuggestionAndSelect(bankName)
        cy.wait(1000)
        // this.common.selectRandomValue()
        this.common.inputDataToTextarea("Enter Bank Account Name", "Auto Test")
        cy.randomNumber(12).then((accNumber) => {
            this.common.inputDataToField("Enter Account Number", accNumber)
        })

        this.common.ipSelectBox("Select Title").click({ force: true })
        this.common.selectRandomValue()
        cy.randomString(8).then((firstName) => {
            this.common.inputDataToField("Enter Key Contact Person’s first name", firstName)
        })
        cy.randomString(8).then((lastName) => {
            this.common.inputDataToField("Enter Key Contact Person’s last name", lastName)
        })
        this.common.inputDataToField("Enter Key Contact Person’s email", "AutoTest@gmail.com")
        cy.randomNumber(9).then((mobilePhone) => {
            this.common.inputDataToField("Enter Mobile Phone no.", mobilePhone)
        })
        if (landline != "empty") {
            cy.randomNumber(8).then((landline) => {
                this.common.inputDataToField("Enter Landline phone no.", landline)
            })
        }
        if (extension != "empty") {
            cy.randomNumber(8).then((extension) => {
                this.common.inputDataToField("Extension", extension)
            })
        }

        this.common.inputDataToTextarea("Enter Address", "Auto Address")
        this.common.ipSelectBox("Select Province").click({ force: true })
        // cy.wait(2000)
        this.common.ddlValue().should("be.visible", { timeout: 10000 })
        var searchKeyProvince = "กรุงเทพมหานคร"
        this.common.ipSelectBox("Select Province").type(searchKeyProvince)
        cy.wait(500)
        this.common.verifySearchSuggestionAndSelect(searchKeyProvince)
        cy.wait(500)
        var searchKeyDistrict = "หนองจอก"
        this.common.ipSelectBox("Select District").click({ force: true })
        this.common.ddlValue().should("be.visible", { timeout: 10000 })
        this.common.liDropdownValue(searchKeyDistrict).click({ force: true })
        cy.wait(500)
        this.common.ipSelectBox("Select Sub-District").click({ force: true })
        this.common.selectRandomValue()
        cy.wait(500)
        this.common.ipSelectBox("Select Postal Code").click({ force: true })
        this.common.selectRandomValue()
        cy.wait(500)
        this.common.btnConfirm("Confirm Location").click({ force: true })
        cy.wait(500)
        this.common.ipSelectBox("Select Pickup Area Coverage").click({ force: true })
        // cy.wait(3000)
        this.common.selectRandomValue()
        cy.wait(300)
        this.common.selectRandomValue2()
        cy.wait(300)
        this.common.ipSelectBox("Select Delivery Area Coverage").click({ force: true })
        // cy.wait(3000)
        this.common.selectRandomValue()
        cy.wait(300)
        this.common.selectRandomValue2()

        this.common.confirmCreateData(action, confirm, click, verify)

        return new SubcontractorList()
    }
}

export default SubcontractorAdd;