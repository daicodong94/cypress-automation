
class Header {

    btnAtHeader(btnName) {
        return cy.xpath("//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')]")
    }

    tabActive(tabName) {
        return cy.xpath("//li[@class='common-menu-item active']//a[text()='" + tabName + "']")
    }

    icCloseInActiveMenu() {
        return cy.xpath("//li[@class='common-menu-item']//a[@class='close']")
    }

    closeInActiveMenu() {
        this.icCloseInActiveMenu().click({ force: true })
    }

    icCloseActiveMenu() {
        return cy.xpath("//li[@class='common-menu-item active']//a[@class='close']")
    }

    closeActiveMenu() {
        this.icCloseActiveMenu().click({ force: true })
    }

    //Page size
    ddlPageSize() {
        return cy.xpath("//span[@class='el-pagination__sizes']")
    }

    spanPageSize(size) {
        return cy.xpath("//span[text()='" + size + "']")
    }

    spanPageSizeActive(size) {
        return cy.xpath("//li[@class='el-select-dropdown__item selected hover']//span[text()='" + size + "']")
    }

    //Page number
    ipPageNumber() {
        return cy.xpath("//input[@type='number']")
    }

    //Search
    ipSearchBox(searchField) {
        return cy.xpath("//input[@placeholder='" + searchField + "']")
    }

    icDeleteSearchFilterDropdown() {
        return cy.xpath("//i[@class='el-select__caret el-input__icon el-icon-circle-close']")
    }

    icDeleteSearchFilterInput() {
        return cy.xpath("//i[@class='el-input__icon el-icon-circle-close el-input__clear']")
    }

    ipEnterDataToFilter() {
        return cy.xpath("//div[@class='search-candidate w450']//input")
    }

    spanSearchFilterResult() {
        // return cy.xpath("//div[@class='search-candidate w450']//span[@class='partner-shortname']//span[@class='search-highlight']")
        return cy.xpath("//div[@class='search-candidate w450']//span[@class='search-highlight']")
    }

    spanDropdownValue(value) {
        return cy.xpath("(//span[contains(text(),'" + value + "')])[last()]")
    }

    spanDropdownValueDriver(value) {
        return cy.xpath("(//span[contains(text(),'" + value + "')])[last()]")
    }

    aDropdownValue(value) {
        return cy.xpath("(//span//a[@title='" + value + "'])[last()]")
    }

    searchResult(value) {
        return cy.xpath("//table//tr[@class='el-table__row']//td//div[text()='" + value + "']")
    }

    spanSearchSuggestionDeliveryPlanPage(searchKey) {
        return cy.xpath("(//div[@class='select-style']//a//span[@class='search-highlight' and text()='" + searchKey + "'])[last()]")
    }

    //Set column
    divColumnToAdjust(value) {
        return cy.xpath("//div[@class='dragg-box']//div[text()='" + value + "']")
    }

    btnSetColumnAction(button) {
        return cy.xpath("//div[@class='custom-table-title']//ancestor::div[@role='dialog']//div[@class='dialog-footer']/button/span[text()='" + button + "']")
    }

    btnConfirmActionSetColumn(button) {
        return cy.xpath("//p[contains(text(),'reset')]//following-sibling::div/button/span[contains(text(),'" + button + "')]")
    }

    divColumnName(column) {
        return cy.xpath("//thead[@class='has-gutter']//tr//th//div[text()='" + column + "']")
    }

    totalValue() {
        return cy.xpath('//span[@class="el-pagination__total"]')
    }

    btnPrev() {
        return cy.xpath('//button[@class="btn-prev"]')
    }

    btnNext() {
        return cy.xpath('//button[@class="btn-next"]')
    }

}

export default Header;