import Address from "../../../support/pages/MasterData/components/Address/Address"

describe("Master Data : Address", () => {

    var data
    var objAddress = new Address()

    beforeEach(function () {
        cy.fixture("data_test_new_enhancement.json").then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            objAddress.commonMD.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Address")
        })
    })

    it(" TC_5:Check display of popup Edit country name \n" +
        "TC_8:Check display of Country list with no procvince created", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        //TC_5
        objAddress.btnCreateCountry().should("be.disabled")
        //TC_8
        objAddress.btnExpandItem(countryName).click()
        objAddress.blockProvince().should("be.visible")
        objAddress.btnCreate(province).should("be.visible")
    })

    it("TC_10,TC_11,TC_12,TC_13:Check [X] button at popup Create province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "BKK Automate"

        objAddress.btnExpandItem(countryName).click()
        //TC_10
        objAddress.btnCreate(province).click()
        objAddress.popupCreate(province).should("be.visible")
        objAddress.btnClosePopupCreate(province).click()
        //TC_11
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnClosePopupCreate(province).click()
        objAddress.popupConfirmQuitCreate(province).should("be.visible")
        objAddress.btnConfirmQuit().click()
        //TC_12
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnClosePopupCreate(province).click()
        objAddress.btnCancelQuit().click()
        objAddress.popupCreate(province).should("be.visible")
        
        cy.reload()
        objAddress.btnExpandItem(countryName).click()
        //TC_13
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnClosePopupCreate(province).click()
        objAddress.btnConfirmQuit().click()
        objAddress.popupCreate(province).should('not.exist')

    })

    it("TC_14,TC_15,TC_16,TC_17:Check [Cancel] button at popup Create province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "BKK Automate"

        objAddress.btnExpandItem(countryName).click()
        //TC_14
        objAddress.btnCreate(province).click()
        objAddress.btnCancel().click()
        objAddress.popupCreate(province).should('not.exist')
        //TC_15
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnCancel().click()
        objAddress.popupConfirmQuitCreate(province).should("be.visible")
        objAddress.btnConfirmQuit().click()
        //TC_16
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnCancel().click()
        objAddress.btnCancelQuit().click()
        objAddress.popupCreate(province).should("be.visible")

        cy.reload()
        objAddress.btnExpandItem(countryName).click()
        //TC_17
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        objAddress.btnCancel().click()
        objAddress.btnConfirmQuit().click()
        objAddress.popupCreate(province).should('not.exist')

    })

    it("TC_18:Check create new province successfully", () => {
        
        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "จังหวัดทดสอบ"

        objAddress.btnExpandItem(countryName).click()
        objAddress.createAndSave(provinceName,province)
        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Create Province successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Create Province successfully")
        objAddress.removeItem(province, provinceName)

    })

    it("TC_19:Check unique of province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnCreate(province).click()
        objAddress.txtName(province).type(provinceName)
        // objAddress.lblWarningMsg().as('warningMsg').should("be.visible")
        // cy.get("@warningMsg").contains("This Province Name is already existed")
        objAddress.commonMD.msgErrorShouldBe("This Province Name is already existed")
    })

    it("TC_22, TC_23:Check [X] button at popup Edit Province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"
        var editProvinceName = "จังหวัดออโต้"

        //TC_22
        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.btnClosePopupEdit(province).click()
        objAddress.popupEdit(province).should("not.exist")

        //TC_23
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnClosePopupEdit(province).click()
        objAddress.popupConfirmQuitEdit(province).should("be.visible")
        objAddress.btnConfirmQuit().click()
    })

    it("TC_24:Check [Cancel] button at  confirm popup Edit Province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"
        var editProvinceName = "จังหวัดออโต้"

        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnClosePopupEdit(province).click()
        objAddress.btnCancelQuit().click()
        objAddress.popupEdit(province).should("be.visible")

    })

    it("TC_25:Check [Confirm] button at confirm popup Edit Province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"
        var editProvinceName = "จังหวัดออโต้"

        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnClosePopupEdit(province).click()
        objAddress.btnConfirmQuit().click()
        objAddress.popupEdit(province).should("not.exist")

    })

    it("TC_26,TC_27:Check [Cancel] button at popup Edit Province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"
        var editProvinceName = "จังหวัดออโต้"

        //TC_26
        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.btnCancel().click()
        objAddress.popupEdit(province).should("not.exist")

        //TC_27
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnCancel().click()
        objAddress.popupConfirmQuitEdit(province).should("be.visible")
        objAddress.btnConfirmQuit().click()

    })

    it("TC_28,TC_29:Check [Cancel] button at confirm popup Edit Province", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ลำพูน"
        var editProvinceName = "จังหวัดออโต้"

        //TC_28
        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnCancel().click()
        objAddress.btnCancelQuit().click()
        objAddress.popupEdit(province).should("be.visible")

        cy.reload()
        objAddress.btnExpandItem(countryName).click()

        //TC_29
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).type(editProvinceName)
        objAddress.btnCancel().click()
        objAddress.btnConfirmQuit().click()
        objAddress.popupEdit(province).should("not.exist")
    })

    it("TC_31:Check [Cancel] button of infom popup is work normally", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "นครทดสอบ"
        var editProvinceName = "จังหวัดออโต้"

        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).clear().type(editProvinceName)
        objAddress.btnSave().click()
        objAddress.btnCancelQuit().click()
        objAddress.lblItem(provinceName).should("be.visible")
    
    })

    it("TC_32:Check [Confirm] button of infom popup is work normally", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "จังหวัดทดสอบ"
        var newProvinceName = "จังหวัดทดสอบราชธานี"

        objAddress.btnExpandItem(countryName).click()
        objAddress.createAndSave(provinceName,province)
        cy.wait(2000)
        objAddress.editAndSave(province, provinceName, newProvinceName)

        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Update Province successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Update Province successfully")

        objAddress.lblItem(newProvinceName).should("be.visible")

        objAddress.removeItem(province, newProvinceName)

    })

    it("TC_37:Check unique of province ", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "นครทดสอบ"
        var editProvinceName = "กระบี่"

        objAddress.btnExpandItem(countryName).click()
        objAddress.getItemMenuId(provinceName).invoke('attr', 'aria-controls').as('menuId')
        cy.get('@menuId').then((id) => {
            cy.log(id)
            objAddress.btnSelectItemAction(id).click()
            objAddress.btnEditItem(id).click()
        })
        objAddress.txtName(province).clear().type(editProvinceName)
        // objAddress.lblWarningMsg().as('warningMsg').should("be.visible")
        // cy.get("@warningMsg").contains("This Province Name is already existed")
        objAddress.commonMD.msgErrorShouldBe("his Province Name is already existed")
    })

    it("TC_39:Check display of Remove button \n" +
            "TC_40:Check display of Confirm remove popup \n" +
            "TC_41:Check [Cancel] button of Confirm remove popup ", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "ออโตเมทบุรี"
        
        objAddress.btnExpandItem(countryName).click()

        //TC_39
        objAddress.lblItem(provinceName).click()
        objAddress.btnRemove(province).should("be.visible")

        //TC_40
        objAddress.btnRemove(province).click()
        objAddress.popupRemove(province).should("be.visible")
        objAddress.btnCancelConfirmRemove(province).should("be.visible")
        objAddress.btnConfirmRemove(province).should("be.visible")

        //TC_41
        objAddress.btnCancelConfirmRemove(province).click()
        objAddress.popupRemove(province).should("not.be.visible")
        objAddress.lblItem(provinceName).should("be.visible")

    })

    it("TC_42:Check remove a province successfully", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "จังหวัดทดสอบบุรี"

        objAddress.btnExpandItem(countryName).click()
        objAddress.createAndSave(provinceName,province)
        cy.wait(2000)

        objAddress.lblItem(provinceName).should("be.exist")

        objAddress.removeItem(province, provinceName)
        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Remove Province successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Remove Province successfully")
    })

    it("TC_43:Check remove multi provinces successfully", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceNameA = "จังหวัดทดสอบบุรี"
        var provinceNameB = "จังหวัดสมุทรทดสอบ"

        objAddress.btnExpandItem(countryName).click()
        objAddress.createAndSave(provinceNameA,province)
        cy.wait(2000)
        objAddress.createAndSave(provinceNameB,province)
        cy.wait(2000)

        objAddress.lblItem(provinceNameA).scrollIntoView().click()
        objAddress.lblItem(provinceNameB).scrollIntoView().click()
        objAddress.btnRemove(province).click()
        objAddress.btnConfirmRemove(province).click()

        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Remove Provinces successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Remove Provinces successfully")

    })

    it("TC_44:Check display of inform popup with province is used in master data \n" +
            "TC_46:Check Done button works normally", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "กระบี่"

        objAddress.btnExpandItem(countryName).click()
        objAddress.lblItem(provinceName).scrollIntoView().click()
        objAddress.btnRemove(province).click()
        objAddress.btnConfirmRemove(province).click()
        
        //TC_44
        objAddress.popupUnableToRemove(province).should("be.visible")
        objAddress.btnDone().should("be.visible")

        //TC_46
        objAddress.btnDone().click()
        objAddress.popupUnableToRemove(province).should("not.be.visible")
    })

    it("TC_45:Check download list of master data successfully ", () => {

        var countryName = "ประเทศไทย"
        var province = "Province"
        var provinceName = "กระบี่"
        var issueFile = "Issue_List.zip"

        objAddress.btnExpandItem(countryName).click()
        objAddress.lblItem(provinceName).scrollIntoView().click()
        objAddress.btnRemove(province).click()
        objAddress.btnConfirmRemove(province).click()
        
        objAddress.btnDownloadIssue().should("be.visible").click()
        cy.verifyDownload(issueFile)
    })

    it("TC_51,TC_52:Check [X] button at popup Create District \n" +
        "TC_53,TC_54:Check [Cancel] button at  confirm popup Create District", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "นครทดสอบ"
        var district = "District"
        var districtName = "เมืองนครทดสอบ"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()

        //TC_51
        objAddress.btnCreate(district).click()
        objAddress.btnClosePopupCreate(district).click()
        objAddress.popupConfirmQuitCreate(district).should("not.exist")

        //TC_52
        objAddress.btnCreate(district).click()
        objAddress.txtName(district).type(districtName)
        objAddress.btnClosePopupCreate(district).click()
        objAddress.popupConfirmQuitCreate(district).should("be.visible")

        cy.reload()
        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()

        //TC_53
        objAddress.btnCreate(district).click()
        objAddress.txtName(district).type(districtName)
        objAddress.btnClosePopupCreate(district).click()
        objAddress.btnCancelQuit().click()
        objAddress.popupCreate(district).should("be.visible")

        //TC_54
        objAddress.btnClosePopupCreate(district).click()
        objAddress.btnConfirmQuit().click()
        objAddress.popupCreate(district).should("not.exist")

    })

    it("TC_59:Check create new District successfully", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "ออโตเมทบุรี"
        var district = "District"
        var districtName = "ทุ่งหนึ่งห้อง"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.createAndSave(districtName, district)
        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Create District successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Create District successfully")
        objAddress.removeItem(district, districtName)

    })

    it("TC_74:Check Edit new District successfully", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "ออโตเมทบุรี"
        var district = "District"
        var districtName = "ทุ่งหนึ่งห้อง"
        var newDistrictName = "บางเสาชิงช้า"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.createAndSave(districtName, district)
        cy.wait(2000)
        objAddress.editAndSave(district, districtName, newDistrictName)

        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Update District successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Update District successfully")

        objAddress.lblItem(newDistrictName).should("be.visible")

        objAddress.removeItem(district, newDistrictName)

    })

    it("TC_85:Check remove a District successfully", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "ออโตเมทบุรี"
        var district = "District"
        var districtName = "ทุ่งหนึ่งห้อง"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.createAndSave(districtName, district)
        cy.wait(5000)
        objAddress.removeItem(district, districtName)
        objAddress.commonMD.msgSuccessShouldHaveText("Remove District successfully")

    })

    it("TC_87:Check display of inform popup with District is used in master data", () => {

        var countryName = "ประเทศไทย"
        var district = "District"
        var provinceName = "นครราชสีมา"
        var districtName = "ปากช่อง"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.lblItem(districtName).scrollIntoView().click()
        objAddress.btnRemove(district).click()
        objAddress.btnConfirmRemove(district).click()
        
        objAddress.popupUnableToRemove(district).should("be.visible")
        objAddress.btnDownloadIssue().should("be.visible")
        objAddress.btnDone().should("be.visible")

    })

    it("TC_102ซCheck create new Subdistrict successfully", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "นครทดสอบ"
        var districtName = "เขตนครทดสอบ"
        var subDistrict = "Sub-District"
        var subDistrictName = "นครทดสอบออโตเมท"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.btnExpandItem(districtName).click()
        objAddress.createAndSave(subDistrictName, subDistrict)
        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Create Sub-District successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Create Sub-District successfully")
        objAddress.removeItem(subDistrict, subDistrictName)

    })

    it("TC_74:Check Edit new District successfully", () => {

        var countryName = "ประเทศไทย"
        var provinceName = "นครทดสอบ"
        var districtName = "เขตนครทดสอบ"
        var subDistrict = "Sub-District"
        var subDistrictName = "นครทดสอบออโตเมท"
        var newSubDistrictName = "คลองทดสอบ"

        objAddress.btnExpandItem(countryName).click()
        objAddress.btnExpandItem(provinceName).click()
        objAddress.btnExpandItem(districtName).click()
        objAddress.createAndSave(subDistrictName, subDistrict)
        cy.wait(5000)
        objAddress.editAndSave(subDistrict, subDistrictName, newSubDistrictName)

        // objAddress.msgSuccess().as("msgAlert").should("be.visible")
        // cy.get("@msgAlert").contains("Update SubDistrict successfully")
        objAddress.commonMD.msgSuccessShouldHaveText("Update SubDistrict successfully")

        objAddress.lblItem(newSubDistrictName).should("be.visible")

        objAddress.removeItem(subDistrict, newSubDistrictName)

    })


})