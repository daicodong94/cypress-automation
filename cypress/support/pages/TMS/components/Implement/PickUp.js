import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import DeliveryPlan from "../plan/DeliveryPlan";
import Schedule from "../schedule/Schedule";
import CreateShippingOrder from "../shippingorder/CreateShippingOrder";
import ShippingOrder from "../shippingorder/ShippingOrder";


class Pickup {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.schedule = new Schedule()
        this.createshippingorder = new CreateShippingOrder()
        this.shippingorder = new ShippingOrder()
        this.deliveryplan = new DeliveryPlan()

    }

    dispatchPickupStatus(status) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//div[text()= "' + status + '"]')
    }

    spanSearchResult(name) {
        return cy.xpath('//div[@class="search-candidate-list select-scroll"]//span[text()="' + name + '"]')
    }

    btnBatchPickup() {
        return cy.xpath('//button//span[text()="batch pickup"]')
    }

    ckbChooseOrder(index) {
        return cy.xpath('(//span[@class="el-checkbox__inner"])[' + index + ']')
    }

    msgBatchPickupsuccess() {
        return cy.xpath('//p[@class="el-message__content"]')
    }

    pickupArrivalTime() {
        return cy.xpath('//input[@placeholder="pickup arrival time"]')
    }

    pickupStartTime() {
        return cy.xpath('//input[@placeholder="pickup start time"]')
    }

    pickupCompletionTime() {
        return cy.xpath('//input[@placeholder="pickup completion time"]')
    }

    btnFunctionPickup(text) {
        return cy.xpath('//button[@type="button"]//span[text()="' + text + '"]')
    }

    btnconfirm() {
        return cy.xpath('//button[@class="el-button el-button--default el-button--small el-button--primary "]//span[contains(text(),"confirm")]')
    }

    abnormalException(exception) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//div[text()= "' + exception + '"]')
    }

    tooltipRequireCompletionTime(msg) {
        return cy.xpath('//div[text()="' + msg + '"]')
    }

    linkattachment() {
        return cy.xpath('//span[contains(text(),"+ Add attachments")]')
    }

    msgattachsuccess() {
        return cy.xpath('//p[text()="images.jpguploaded successfully"]')
    }

    clickbtnFunctionPickup(text) {
        this.btnFunctionPickup(text).click({ force: true })
    }

    confirmPickup(confirm) {
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").should("be.visible", { timeout: 10000 }).click({ force: true })
            // this.msgBatchPickupsuccess().contains(msg)
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopUp("cancel").should("be.visible", { timeout: 10000 }).click({ force: true })
            // this.msgBatchPickupsuccess().contains("cancelled")
        }
    }

    selectCarrier(carrier, name) {
        this.header.ipSearchBox("Please enter a subcontractor").click({ force: true })
        cy.wait(1000)
        this.schedule.ipCarrier().type(carrier)
        cy.wait(2000)
        this.spanSearchResult(name).click({ force: true })
    }

    createOrderAndSchedule(trackingNum, carrier, name) {
        // this.leftMenu.navigateToPage("Shipping order")
        // this.shippingorder.navigateToCreateShippingOrderPage()
        this.createshippingorder.createNewShippingOrder("Direct Schedule", trackingNum, "Automation Company", "Service for Shipper", "Frozen", "SiteTestAuto", "AutoTestSite99", 20)
        cy.wait(3000)
        // this.leftMenu.navigateToPage("Schedule ")
        // this.common.searchByClickSuggestion("dispatch number", trackingNum)
        // this.common.openDetailView()
        this.selectCarrier(carrier, name)
        this.schedule.lnkAddVehicle().click({ force: true })
        this.schedule.selectDriver("AutoDriver")
        cy.wait(5000)
        this.selectConsignment("Service for Carrier")
        this.schedule.btnAction("confirm and dispatch").click({ force: true })
        cy.wait(1000)
        this.common.msgSuccess().should("be.visible")
    }

    choose2OrderandclickbtnBatchPickup() {
        this.ckbChooseOrder("3").click({ force: true })
        this.ckbChooseOrder("4").click({ force: true })
        this.btnBatchPickup().click({ force: true })
    }

    select2OrderandBatchPickup(confirm, msg) {
        this.choose2OrderandclickbtnBatchPickup()
        cy.wait(2000)
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").click({ force: true })
            this.msgBatchPickupsuccess().contains(msg)
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopUp("cancel").click({ force: true })
            this.msgBatchPickupsuccess().contains("cancelled")
        }
    }

    chooseStartTime() {
        this.pickupStartTime().click({ force: true })
        cy.wait(2000)
        this.createshippingorder.btnNow().click({ force: true })
    }

    chooseCompletionTime(days) {
        this.pickupCompletionTime().click({ force: true })
        cy.wait(1000)
        var date = new Date()
        cy.addDays(date, days).then((futuredate) => {
            this.createshippingorder.ipSelectDate().type(futuredate)
            this.createshippingorder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click({ force: true })
    }

    chooseArrivalTime(days) {
        this.pickupArrivalTime().click({ force: true })
        cy.wait(1000)
        var date = new Date()
        cy.addDays(date, days).then((futuredate) => {
            this.createshippingorder.ipSelectDate().type(futuredate)
            this.createshippingorder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click({ force: true })
    }

    chooseArrivalTimeLessDay(days) {
        this.pickupArrivalTime().click({ force: true })
        cy.wait(1000)
        var date = new Date()
        cy.lessDays(date, days).then((passDate) => {
            this.createshippingorder.ipSelectDate().type(passDate)
            this.createshippingorder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click({ force: true })
    }

    chooseStartTimeday(days) {
        this.pickupStartTime().click({ force: true })
        cy.wait(1000)
        var date = new Date()
        cy.addDays(date, days).then((futuredate) => {
            this.createshippingorder.ipSelectDate().type(futuredate)
            this.createshippingorder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click({ force: true })
    }

    chooseArrivalTimeNow() {
        this.pickupArrivalTime().click({ force: true })
        cy.wait(1000)
        this.createshippingorder.btnNow().click({ force: true })
    }

    attachPicturePickup(filePath) {
        this.attachPicturePickup().attachFile(filePath)
        cy.wait(2000)
        this.msgattachsuccess().should("be.visible")
    }

    spanDropdownValue(value) {
        return cy.xpath("(//a[text()='" + value + "'])[last()]")
    }

    selectConsignment(consignment) {
        this.header.ipSearchBox("Please select surcharge / discount").click({ force: true })
        cy.wait(1000)
        this.spanDropdownValue(consignment).click({ force: true })
    }

    goToPageNumber(page) {
        this.header.ipPageNumber().clear()
        this.header.ipPageNumber().type(page)
        this.header.ipPageNumber().type('{enter}')
        cy.wait(2000)
    }

    wayBillNumber() {
        return cy.xpath('//div[@class="common-right-title"]')
    }

    tabShipment(tabname) {
        return cy.xpath('//div[@id="' + tabname + '"]')
    }

    txtSearch(index) {
        return cy.xpath('(//input[@placeholder="Search...."])[' + index + ']')
    }

    iconSearch(index) {
        return cy.xpath('(//button[@id="btn-search"])[' + index + ']')
    }

    btnUpdateShipment() {
        return cy.xpath('//div[contains(text(),"Action")]')
    }

    updateShipment(status) {
        return cy.xpath('(//ul[@class="el-dropdown-menu el-popper el-dropdown-menu--mini"]//li[contains(text(),"' + status + '")])[last()]')
    }

    msgSuccess(msg) {
        return cy.xpath("//p[contains(text(),'" + msg + "')]")
    }

    shipmentNo(number) {
        return cy.xpath('(//span[contains(text(),"' + number + '")]//ancestor::tr)[1]')
    }

    shipmentStatus(status) {
        return cy.xpath('(//div[contains(text(),"' + status + '")])[1]')
    }

    upDateShipmentStatus(action, shipmentStatus) {
        this.btnUpdateShipment().click({ force: true })
        cy.wait(2000)
        this.updateShipment(action).scrollIntoView().click({ force: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
        // cy.wait(2000)
        this.msgSuccess("Driver have been updated").should("be.visible")
        cy.wait(1000)
        this.shipmentStatus(shipmentStatus).should("be.visible")
    }
}
export default Pickup;