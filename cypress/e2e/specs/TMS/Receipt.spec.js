import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import Login from "../../../support/pages/TMS/components/login/Login"
import DriverReceipt from "../../../support/pages/TMS/components/receipt/DriverReceipt"
import ShippersReceipt from "../../../support/pages/TMS/components/receipt/ShippersReceipt"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"

describe("Receipt", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(), objShippersReceipt = new ShippersReceipt(),
        objDriverReceipt = new DriverReceipt(), objShippingOrder = new ShippingOrder(),
        objCreateShippingOrder = new CreateShippingOrder(), objPickup = new Pickup(),
        objSchedule = new Schedule(), objDeliverGood = new DeliveryGood(), objActivityTracking = new ActivityTracking()
    var trackNumber, carrier = "SYS_DEF_INDIVIDUAL", customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
    siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it("TC: Check function Set Column - Disable header at Shipper's receipt screen \n" +
        "TC: Check function Set Column - Enable header at Shipper's receipt screen \n" +
        "TC: Check function Set Column - Drag and drop to reorder at Shipper's receipt screen \n" +
        "TC: Check function Set Column - Reset at Shipper's receipt screen \n" +
        "TC: Check paging of Shipper's receipt screen", () => {
            objShippersReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Shippers Receipt")
            objShippersReceipt.common.resetDefaultColumn("yes")
            objShippersReceipt.common.enableOrDisableColumn("shipper", "no")
            // objReceipt.common.resetDefaultColumn("yes")
            objShippersReceipt.common.enableOrDisableColumn("shipper", "yes")
            objShippersReceipt.common.setColumn("shipping order number", "shipper")
            objShippersReceipt.common.resetDefaultColumn("yes")
            cy.wait(1000)
            objShippersReceipt.common.selectPageSize("50/page")
            objShippersReceipt.common.selectPageSize("200/page")
        })

    it("TC: Check function Set Column - Enable header at Shipper's receipt screen \n" +
        "TC: Check function Set Column - Drag and drop to reorder at Driver receipt screen \n" +
        "TC: Check function Set Column - Reset at Driver receipt screen", () => {
            objDriverReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
            objDriverReceipt.common.resetDefaultColumn("no")
            objDriverReceipt.common.enableOrDisableColumn("dispatch number", "no")
            objDriverReceipt.common.enableOrDisableColumn("dispatch number", "yes")
            objDriverReceipt.common.resetDefaultColumn("yes")
        })

    it("TC: Check search Driver Receipt by track \n" +
        "TC: Check search Driver Receipt by dispatch ID\n" +
        "TC: Check search with combine condition Driver Receipt\n" +
        "TC: Check paging of Driver receipt screen\n" +
        "TC: Check information of Receipt detail\n" +
        "TC: Check information of  waybill on receipt detail\n" +
        "TC: Check Confirm POD with have no attachment\n" +
        "TC: Check Confirm receipt after uploading valid attachment\n" +
        "TC: Check function download package Driver receipt\n" +
        "TC: Check search Shipper's Receipt by shipper", () => {
            cy.randomString(7).then((id) => {
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrder("direct schedule", id, customer, product, temperature, siteA, siteC, 5);
                trackNumber = id
                objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("dispatch number", id)
                objSchedule.common.searchResultWithOutValue().should("be.visible")
                cy.wait(1000)
                objSchedule.common.openDetailView()
                cy.wait(1000)
                objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
                objSchedule.common.msgSuccess().should("be.visible")
                objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
                objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("dispatch number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

                //open detail view and get Waybill number: this.waybill
                objPickup.common.openDetailView()
                objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill2 = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill2)
                    cy.reload()
                    ////must go to activity tracking -> update status 2 times
                    objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    cy.wait(1000)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    //back to Pick up to pickup completed
                    objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    cy.wait(1000)
                    objPickup.common.searchByClickSuggestion("waybill number", id)
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                    objPickup.common.openDetailView()
                    cy.wait(1000)
                    objPickup.chooseCompletionTime(0)
                    cy.wait(1000)

                    objPickup.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    objPickup.confirmPickup("yes")
                    objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                    cy.wait(1000)
                    objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
                    cy.wait(1000)
                    objDeliverGood.common.openDetailView()
                    cy.wait(1000)
                    objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
                    cy.wait(1000)
                    objDeliverGood.pickup.clickbtnFunctionPickup("Delivery completed")
                    objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                    objDeliverGood.common.btnConfirmAgainHintPopup("confirm").click()
                    objDeliverGood.common.msgSuccess().should("be.visible")
                    cy.reload()
                    objDriverReceipt = objDeliverGood.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                    // Check paging of Driver receipt screen
                    objDriverReceipt.common.selectPageSize("50/page")
                    objDriverReceipt.common.selectPageSize("200/page")
                    cy.wait(1000)
                    //Check search Driver Receipt by track
                    objDriverReceipt.common.searchByClickSuggestion("waybill number", id)
                    objDriverReceipt.common.openDetailView()
                    objDriverReceipt.navigateToMissionInforTab()
                    objDriverReceipt.common.tdTrackNumberDetail(id).should("be.visible")
                    //Check search Driver Receipt by dispatch ID
                    cy.reload()
                    objDriverReceipt.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                    objDriverReceipt.common.searchByClickSuggestion("dispatch number", id)
                    objDriverReceipt.common.openDetailView()
                    objDriverReceipt.navigateToMissionInforTab()
                    objDriverReceipt.common.tdTrackNumberDetail(id).should("be.visible")
                    //Check search with combine condition Driver Receipt
                    cy.reload()
                    objDriverReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                    objDriverReceipt.common.searchByClickSuggestion("waybill number", id)
                    objDriverReceipt.common.searchByClickSuggestion("dispatch number", id)
                    objDriverReceipt.common.openDetailView()
                    //Check information of  waybill on receipt detail
                    objDriverReceipt.navigateToMissionInforTab()
                    objDriverReceipt.common.tdTrackNumberDetail(id).should("be.visible")
                    //Check information of Receipt detail
                    ////////Check Confirm POD with have no attachment
                    objDriverReceipt.navigateToPODInforTab()
                    cy.wait(1000)
                    objDriverReceipt.linkConfirmPOD().first().click()
                    objDriverReceipt.common.btnConfirmHintPopUp("confirm").click()
                    objDriverReceipt.common.msgError().should("be.visible")
                    ///////
                    //Check Confirm receipt after uploading valid attachment
                    var filePath = "implement\\images.png"
                    objDriverReceipt.navigateToPODInforTab()
                    cy.wait(1000)
                    objDriverReceipt.common.addFileAttachments(filePath)
                    ////////Check function download package Driver receipt
                    objDriverReceipt.header.btnAtHeader("Download package").first().click()
                    cy.task("countFiles", "cypress/downloads").then((count) => {
                        if (count > 0) {
                            return true
                        } else if (count == 0) {
                            return false
                        }
                    })
                    // ///////
                    //bug
                    objShippersReceipt = objDriverReceipt.confirmPODElectronicProofOfDelivery()
                    //Check search Shipper's Receipt by shipper
                    cy.reload()
                    objShippersReceipt.leftMenu.navigateToSubPage("Receipt", "Shippers Receipt")
                    objShippersReceipt.common.searchByInputDataToFilter("shipper", customer)
                    objShippersReceipt.common.searchResultWithOutValue().should("be.visible")
                })
            })
        })

    it("TC: Check search Shipper's Receipt by task number\n" +
        "TC: Check search Shipper's Receipt by shipping order number\n" +
        "TC: Check search with combine condition Shipper's receipt\n" +
        "TC: Check information of  Mission Information\n" +
        "TC: Check information of Shipper's detail\n" +
        "TC: Check Shipper's if not select another file\n" +
        "TC: Check Shipper's after uploading attachment\n" +
        "TC: Check function download package Shipper's receipt", () => {
            trackNumber = "736983476104642182412"
            //Check search Shipper's Receipt by task number
            objShippersReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Shippers Receipt")
            objShippersReceipt.common.searchByClickSuggestion("task number", trackNumber)
            cy.wait(1000)
            objShippersReceipt.common.openDetailView()
            objShippersReceipt.driverReceipt.navigateToMissionInforTab()
            objShippersReceipt.common.tdTrackNumberDetail(trackNumber).should("be.visible")
            //Check search Shipper's Receipt by shipping order number
            cy.reload()
            objShippersReceipt.leftMenu.navigateToSubPage("Receipt", "Shippers Receipt")
            objShippersReceipt.common.searchByClickSuggestion("shipping order number", trackNumber)
            cy.wait(1000)
            objShippersReceipt.common.openDetailView()
            objShippersReceipt.driverReceipt.navigateToMissionInforTab()
            objShippersReceipt.common.tdTrackNumberDetail(trackNumber).should("be.visible")
            //Check search with combine condition Shipper's receipt
            //bug
            cy.reload()
            objShippersReceipt.leftMenu.navigateToSubPage("Receipt", "Shippers Receipt")
            objShippersReceipt.common.searchByInputDataToFilter("shipper", customer)
            objShippersReceipt.common.searchByClickSuggestion("task number", trackNumber)
            objShippersReceipt.common.searchByClickSuggestion("shipping order number", trackNumber)
            /////Check information of  Mission Information
            cy.wait(1000)
            objShippersReceipt.common.openDetailView()
            objShippersReceipt.driverReceipt.navigateToMissionInforTab()
            objShippersReceipt.common.tdTrackNumberDetail(trackNumber).should("be.visible")
            /////
            //Check information of Shipper's detail
            objShippersReceipt.driverReceipt.navigateToPODInforTab()
            //Check Shipper's if not select another file
            objShippersReceipt.header.btnAtHeader("proof of delivery").click()
            objShippersReceipt.common.btnConfirmHintPopUp("confirm").click()
            objShippersReceipt.common.textField("Email Subject").should("be.exist")
            objShippersReceipt.header.btnAtHeader("return").click()
            //Check Shipper's after uploading attachment
            cy.wait(1000)
            objShippersReceipt.common.openDetailView()
            objShippersReceipt.driverReceipt.navigateToPODInforTab()
            var filePath2 = "implement\\images.exe"
            objShippersReceipt.driverReceipt.navigateToPODInforTab()
            cy.wait(1000)
            objShippersReceipt.common.addFileAttachments(filePath2)
            objShippersReceipt.common.msgSuccess().should("be.visible")
            objShippersReceipt.header.btnAtHeader("proof of delivery").click()
            objShippersReceipt.common.btnConfirmHintPopUp("confirm").click()
            objShippersReceipt.common.textField("Email Subject").should("be.exist")
            objShippersReceipt.header.btnAtHeader("return").click()
            //Check function download package Shipper's receipt
            cy.wait(1000)
            objShippersReceipt.common.openDetailView()
            objShippersReceipt.driverReceipt.navigateToPODInforTab()
            objShippersReceipt.header.btnAtHeader("Download package").first().click()
            cy.task("countFiles", "cypress/downloads").then((count) => {
                if (count > 0) {
                    return true
                } else if (count == 0) {
                    return false
                }
            })
        })
})