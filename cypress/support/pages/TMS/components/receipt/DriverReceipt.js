import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import ShippersReceipt from "./ShippersReceipt";

class DriverReceipt {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    tabMissionInformation() {
        return cy.xpath("//div[@id='tab-1']")
    }

    navigateToMissionInforTab() {
        this.tabMissionInformation().click({ force: true })
    }

    tabPODInformation() {
        return cy.xpath("//div[@id='tab-0']")
    }

    navigateToPODInforTab() {
        this.tabPODInformation().click()
    }

    divPlateNumber() {
        return cy.xpath("(//div[@class='el-table__header-wrapper']//div[(text()='plate number')]//ancestor::div[@id='pane-0']//tr[@class='el-table__row'])[last()]//td[last()-1]")
    }

    lblCarrier() {
        return cy.xpath("(//td[text()='Subcontractor']//following::td)[1]")
    }

    linkConfirmPOD() {
        return cy.xpath("//span[contains(text(),'Confirm POD')]")
    }

    confirmPODElectronicProofOfDelivery() {
        this.linkConfirmPOD().first().click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should("be.visible")

        return new ShippersReceipt()
    }

    confirmPickup(confirm) {
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").click()
            // this.msgBatchPickupsuccess().contains(msg)
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopUp("cancel").click()
            // this.msgBatchPickupsuccess().contains("cancelled")
        }
    }
    btnSendEmail() {
        return cy.xpath('//button//span[contains(text(),"send")]')
    }

    confirmSendEmail(confirm) {
        // cy.randomNumber(5).then((number) => {
        //     this.txtEmail().type("VmoVietNam" + number + "@gmail.com")
        //     this.txtSms().type("test send messge" + number)
        // })
        this.btnSendEmail().click()
        if (confirm == "yes") {
            this.common.btnConfirmAgainHintPopup("confirm").click()
            // this.common.msgSuccessShouldContainText("done successfully").should("be.visible")
        } else if (confirm == "no") {
            this.common.btnConfirmAgainHintPopup("cancel").click()
        }
    }

    confirmPOD(filePath) {
        this.common.addFileAttachments(filePath)
        this.common.btnConfirmAgainHintPopup("Confirm POD").first().click({ force: true })
        this.common.btnConfirmAgainHintPopup("confirm").click({ force: true })
    }

    proofOfDelivery() {
        this.common.btnConfirmAgainHintPopup("proof of delivery").click({ force: true })
        this.common.btnConfirmAgainHintPopup("confirm").click({ force: true })
        this.confirmSendEmail("yes")
    }
}

export default DriverReceipt;