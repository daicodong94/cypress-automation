import SiteList from "../../../support/pages/MasterData/components/site/SiteList"
import { Utility } from "../../../support/utility"






describe("MasterData - Site", () => {
    var data
    var objSite = new SiteList


    beforeEach(function () {
        var url = new Utility().getBaseUrl()
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
        data = logindata
        cy.visit(url)
        objSite.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Site")
        })
    })

    it("TC_01_Verify UI SIte", () => {
        objSite.titlePage("Site").should("be.visible")
    })

    it("TC_04: Check number item in page\n" + 
       "TC_05: Check when move from a page to another\n" +
       "TC_06: Check moving page has response \n" +
       "TC_07: Check moving page has no response when enter the number of pages that exceed the total number of pages\n" +
       "TC_08: Check jump to the last page", () => {
        // Number Item in page
        objSite.common.selectPageSize("30/page")
        // Move to Page from another page
        objSite.moveToPageWhenClickPageNumber("2", "2")
        // Go to Page Number
        objSite.common.goToPageNumber("3")
        // Input page exceed + Jump to last page
        objSite.ipPageExceed()
        objSite.common.btnJumpPageNext().should("be.disabled")
    })

    it("TC_10: Check searching Site by Customer name will have result\n" +
        "TC_11: Check searching Site by Site ID will have result\n" +
        "TC_12: Check searching Site by Site name have result\n" +
        "TC_13: Check searching Site by Province\n" +
        "TC_14: Check searching Site don't have result\n" +
        "TC_15: Check searching with space at the beginning and ending of key search will trim space", () => {
        cy.randomNumber(5).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            // Search by CustomerName
            objSite.common.searchByCondition("Automation")
            objSite.common.searchResultWithOutValue().should("be.visible")
            // Search by Site name
            objSite.common.searchByCondition(number)
            objSite.common.searchResultWithOutValue().should("be.visible")
            // Search by Province
            objSite.common.searchByCondition("นครราชสีมา")
            objSite.common.searchResultWithOutValue().should("be.visible")
            // Search by Site ID
            objSite.common.openDetailsView()
            cy.wait(2000)
            objSite.searchBySiteID()
            // Searching no Data
            objSite.common.searchByCondition("@@@@")
            objSite.searchNoData().should("be.visible")
            // Searching with space begining and ending
            objSite.common.searchByCondition("    Automation")
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.common.searchByCondition("Automation     ")
            objSite.common.searchResultWithOutValue().should("be.visible")
        })
    })

    it("TC_16: Check button Filter\n" +
        "TC_17: Check combo box Customer name\n" +
        "TC_18: Check searching by Customer name dropdown list\n" + 
        "TC_19: Check combo box Site type\n" + 
        "TC_20: Check searching by Site type dropdown list\n" +
        "TC_21: Check  combo box Province\n" + 
        "TC_22: Check searching by Province dropdown list\n" + 
        "TC_23: Check close Reset Filter button after filter is used \n" + 
        "TC_24: Check to Reset filter", () => {
        objSite.clickFilter()
        cy.wait(1000)
        objSite.titleSearchFilter("Status").should("be.visible")
        objSite.titleSearchFilter("Site Type").should("be.visible")
        objSite.titleSearchFilter("Province").should("be.visible")
        // Searching by Status Dropdown List
        objSite.searchingByValueOfDropdown("All", "1", "Active")
        cy.wait(2000)
        // Searching by Site Type dropdown list
        objSite.searchingByValueOfDropdown("All", "2", "Store")
        cy.wait(2000)
        // Searching by Province dropdown list
        objSite.searchingByValueOfDropdown("All", "3", "นครราชสีมา")
        // Reset Filter
        objSite.clickResetFilter()
        objSite.header.btnResetFilter().should("not.exist")
        // Close Reset Filter button
        objSite.searchingByValueOfDropdown("All", "1", "Active")
        objSite.header.btnResetFilter().click()
        cy.wait(1000)
        objSite.header.btnResetFilter().should("not.exist")
        
    })

    it("TC_25: Check UI of pop up Import screen\n" + "TC_29: Check Delete file \n" + "TC_30: Check Cancel button\n" + "TC_31: Check to drag file invalid format\n" + "TC_32: Check when importing the file xls or xlsx which is not matched the template file", () => {
        var filePath = "masterdataSite\\Template.xlsx"
        var filePath1 = "implement\\images.png"
        var filePath2 = "masterdataSite\\Template_not_matched.xlsx"
        objSite.clickBtnImport()
        cy.wait(2000)
        objSite.shouldUIPopupSite()
        objSite.common.attachImportFile(filePath)
        cy.wait(2000)
        // Check Delete file 
        objSite.clickIconDelete()
        cy.wait(1000)
        objSite.fileNameAttach("Template.xlsx").should("not.exist")
        // Check Cancel Button
        objSite.common.attachImportFile(filePath)
        cy.wait(2000)
        objSite.common.cancelImportFile("yes")
        // TC_31 + TC_32 Import file Invalid format and not matched template
        objSite.clickBtnImport()
        objSite.common.importFileInvalidType(filePath1, "The selected file is not a support file type, please check your file.")
        objSite.common.importFileNotMatchedTemplate(filePath2, "The selected file is in the invalid format, please check your file.")
    })

    it("TC_68 + 69: Check scroll function", () => {
        objSite.scrollFunction()
    })

    it("TC_73: Check change the status from having end date to no end date - Status Pending \n" + "TC_74: Check change the status from having end date to no end date ", () => {
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.iconDelete().click()
            cy.wait(2000)
            objSite.selectAndSetStatus("Active", "Site’s status has been updated", number)
            objSite.iconDelete().click()
            cy.wait(2000)
            objSite.selectAndSetStatusPending("Site’s status has been updated", number, "Pending")
        })
    })

    it("TC_75: Check the view history log", () => {
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.common.openDetailsView()
            objSite.btnHistoryLog().click()
            cy.wait(2000)
            objSite.siteHistoryLogScreenUI()
        })
    })

    it("TC_76: Check add the record will add a new row in view history log screen", () => {
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.header.clkBtnViewHistoryLog()
            cy.wait(2000)
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.searchResultWithOutValue().should("be.visible")
            objSite.activeType("Create").should("be.visible")
        })
    })

    it("TC_77: Check edit the record will add a new row in view history log screen", () => {
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.common.openDetailsView()
            cy.wait(3000)
            objSite.editSite()
            objSite.leftMenu.navigateToSubPage("Master Data" ,"Site")
            cy.wait(2000)
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.header.clkBtnViewHistoryLog()
            cy.wait(2000)
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.searchResultWithOutValue().should("be.visible")
            objSite.activeType("Edit").should("be.visible").eq(0)
        })
    })

    it("TC_78: Check the default sorting of  View history log screen", () => {
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.header.clkBtnViewHistoryLog()
            cy.wait(2000)
            objSite.siteName(number).should("be.visible").eq(0)
        })
    })

    it("TC_59: Check to Export all list Site\n" + "TC_60: Check to Export file that selected\n" + "TC_61 + TC_62 + TC_63: Check default sort by created date \n"
    + "TC_70: Check moving to Set Status screen\n" + "TC_71: Check after changing status will display correspondingly on table list\n" + "TC_72: Check change the status from having end date to no end date ", () => {
        cy.wait(2000)
        objSite.header.btnExport().click()
        cy.wait(2000)
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
        cy.randomNumber(10).then((number) => {
            objSite.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            objSite.common.openDetailsView()
            cy.wait(2000)
            objSite.sortByCreateDate("Created Date")
            // sort by status
            cy.reload()
            objSite.sortByStatus("Status")
            // set Status Site
            objSite.common.searchByCondition(number)
            cy.wait(1000)
            objSite.common.searchResultWithOutValue().should("be.visible")
            // objSite.scrollbar("left").scrollTo('right', {duration: 3000 })
            objSite.iconDelete().click()
            cy.wait(2000)
            objSite.selectAndSetStatus("Active", "Site’s status has been updated", number)
            // Check change the status from having end date to no end date 
            objSite.iconDelete().click()
            cy.wait(2000)
            objSite.selectAndSetStatus("Inactive", "Site’s status has been updated", number)
        })
    })

})