import Homepage from "../homepage/Homepage";

class Login {

    titleLoginPage(){
        return cy.xpath("//title[text()='login page']")
    }

    ipUsername() {
        return cy.xpath('//input[@id="username"]')
    }
    ipPassword() {
        return cy.xpath('//input[@id="pwd"]')
    }
    btnSubmit() {
        return cy.xpath('//button[@id="J-registr-submit"]')
    }

    inputUsername(username) {
        this.ipUsername().first().type(username)
    }

    inputPassword(password) {
        this.ipPassword().first().type(password)
    }

    clickBtnSubmit() {
        this.btnSubmit().click()
    }

    shouldBeTitleWhenUserLoginSuccessfully() {
        cy.title().should('eq', 'TMSUI-TAI CP')
    }

    login(username, password) {
        this.titleLoginPage().should("exist")
        this.inputUsername(username)
        this.inputPassword(password)
        cy.wait(1000)
        this.clickBtnSubmit()
        cy.wait(30000)
        this.shouldBeTitleWhenUserLoginSuccessfully()

        return new Homepage()
    }
}

export default Login;