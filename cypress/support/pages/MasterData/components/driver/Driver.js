import Common from "../../../TMS/components/common/Common"
import Header from "../../../TMS/components/fragments/Header"
import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import LeftMenu from "../../../TMS/components/fragments/LeftMenu"
class Driver {

    //CONSTRUCTOR
    constructor() {
        this.common = new Common()
        this.commonMD = new CommonMasterData()
        this.header = new Header()
        this.headerMD = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    //XPATHS
    lblInformationPage() {
        return cy.xpath("//div[@id='tab-information']")
    }

    lblVehiclesPage() {
        return cy.xpath("//div[@id='tab-second']")
    }

    lblCreateDriver() {
        return cy.xpath("//p[@class='font-medium text-xl']")
    }

    lblImportDriver() {
        return cy.xpath("//span[normalize-space()='Import Driver']")
    }

    lblImportPhoto() {
        return cy.xpath("//span[normalize-space()='Import Photo']")
    }

    btnMainFunction(func) {
        return cy.xpath("//button//span//div[contains(text(),'" + func + "')]")
    }

    impDriver() {
        return cy.xpath("//input[@accept='.xlsx, .xls, .xlsm']")
    }

    btnDeteleFileImportDriver() {
        return cy.xpath("//p[contains(.,'SampleImpDriver.xlsx')]/parent::div/following-sibling::div//*[local-name()='svg']")
    }

    btnImportDriver() {
        return cy.xpath("//button[normalize-space()='Cancel']/following-sibling::button[contains(.,'Import')]")
    }

    lblImport(imp) {
        return cy.xpath("//li//button//div[contains(text(),'" + imp + "')]")
    }

    btnClickToUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//button")
    }

    ipClickToUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//input[@class='invisible absolute']")
    }

    lblFileUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//div//span")
    }

    btnActionFileUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//div//*[local-name()='svg']")
    }

    cboSelectTitle() {
        return cy.xpath("//input[@placeholder='Select Title']")
    }

    lblTitle(title) {
        return cy.xpath("//li//span[normalize-space()='" + title + "']")
    }

    txtFirstName() {
        return cy.xpath("//input[@placeholder='Enter First Name']")
    }

    txtLastName() {
        return cy.xpath("//input[@placeholder='Enter Last Name']")
    }

    lblStatusMessageFields(mess) {
        // return cy.xpath("//div[@class='flex justify-between']//div[contains(text(),'" + mess + "')]")
        return cy.xpath("//div[contains(text(),'" + mess + "')]")
    }

    cboGender() {
        return cy.xpath("//input[@placeholder='Select Gender']")
    }

    lblGender(gender) {
        return cy.xpath("//li//span[normalize-space()='" + gender + "']")
    }

    txtEmail() {
        return cy.xpath("//input[@placeholder='Enter Email']")
    }

    dtpDateOfBirth(typeDate) {
        return cy.xpath("//div[contains(text(),'" + typeDate + "')]/following-sibling::span//div//div[2]//div[1]")
    }

    lblYearDOB() {
        return cy.xpath("//span[@class='el-date-picker__header-label']").first()
    }

    lblMonthDOB() {
        return cy.xpath("//span[@class='el-date-picker__header-label']").last()
    }

    lblYearToday(yearToday) {
        return cy.xpath("//td[@class='available today']//a[contains(.,'" + yearToday + "')]")
    }

    lblYear(year) {
        return cy.xpath("//td[@class='available']//a[contains(.,'" + year + "')]")
    }

    lblYearNowDisabledOnDateOfBirthDTP(year) {
        return cy.xpath("//td[@class='available disabled today']//a[contains(.,'" + year + "')]")
    }

    lblYearDisabled(year) {
        return cy.xpath("//td[@class='available disabled']//a[contains(.,'" + year + "')]")
    }

    lblY() {
        return cy.xpath("//td//a")
    }

    lblYearDisable(year) {
        return cy.xpath("//td[@class='available disabled']//a[contains(.,'" + year + "')]")
    }

    btnPrevYear() {
        return cy.xpath("//button[@aria-label='Previous Year']")
    }

    btnNextYear() {
        return cy.xpath("//button[@aria-label='Next Year']")
    }

    lblMonthDOB() {
        return cy.xpath("//span[@class='el-date-picker__header-label']").last()
    }

    lblMonth(month) {
        return cy.xpath("//table[@class='el-month-table']//div//a[contains(text(),'" + month + "')]")
    }

    lblToDay(today) {
        return cy.xpath("//td[@class='available today']//span[contains(.,'" + today + "')]")
    }

    lblToDayDOB(today) {
        return cy.xpath("//td[@class='available default']//span[contains(.,'" + today + "')]")
    }

    lblToDaySetED(today) {
        return cy.xpath("//td[@class='available today current']//span[contains(.,'" + today + "')]")
    }

    lblDayExpiredDate(date) {
        return cy.xpath("//td[@class='available']//span[contains(.,'" + date + "')]")
    }

    lblDay(day) {
        return cy.xpath("//td[contains(@class,'available')]//span[contains(.,'" + day + "')]")
    }

    lblDayDisable(day) {
        return cy.xpath("//td[@class='normal disabled']//span[contains(.,'" + day + "')]")
    }

    lblDayNextMonth(day) {
        return cy.xpath("//td[@class='next-month']//span[contains(.,'" + day + "')]")
    }

    lblDayPrevMonth(day) {
        return cy.xpath("//td[@class='prev-month']//span[contains(.,'" + day + "')]")
    }

    lblDayPrevMonthED(day) {
        return cy.xpath("//td[@class='prev-month disabled']//span[contains(.,'" + day + "')]")
    }

    btnSetDate() {
        return cy.xpath("//button[@class='el-button el-button--primary el-button--xs']")
    }

    btnCancelSetDate() {
        return cy.xpath("//button[@class='el-button text-primary-main el-button--text el-button--xs']")
    }

    cboPhoneNo() {
        return cy.xpath("(//input[@placeholder='Select'])[1]")
    }

    lblPhoneNo(phoneHead) {
        return cy.xpath("//li//span[normalize-space()='" + phoneHead + "']")
    }

    txtMobilePhoneNo() {
        return cy.xpath("//input[@placeholder='Enter Mobile Phone No.']")
    }

    txtMobilePhoneNo2() {
        return cy.xpath("//div[contains(text(),'Mobile phone no.')]/following-sibling::div//input")
    }

    cboLandlineNo() {
        return cy.xpath("(//input[@placeholder='Select'])[2]")
    }

    lblLandlineNo(LLNo) {
        return cy.xpath("//li//span[normalize-space()='" + LLNo + "']")
    }

    txtLandlinePhoneNo() {
        return cy.xpath("//input[@placeholder='Enter Landline Phone No.']")
    }

    txtExtension() {
        return cy.xpath("//input[@placeholder='Extension']")
    }

    txtNationalIDCardNumber() {
        return cy.xpath("(//input[@placeholder='Enter National ID Card Number'])[1]")
    }

    txtDriverLicenseNumber() {
        return cy.xpath("//input[@placeholder='Enter Driver License Number']")
    }

    cboDriverLicenseType() {
        return cy.xpath("//input[@placeholder='Select Driver License Type']")
    }

    lblDriverLicenseType(licenseType) {
        return cy.xpath("//li//span[normalize-space()='" + licenseType + "']")
    }

    cboDriverLicenseCategory() {
        return cy.xpath("//input[@placeholder='Select Driver License Category']")
    }

    lblDriverLicenseCategory(licenseCategory) {
        return cy.xpath("//li//span[normalize-space()='" + licenseCategory + "']")
    }

    btnSaveCreateNew() {
        return cy.xpath("//button[normalize-space()='Save']")
    }

    btnCancel() {
        return cy.xpath("//button[normalize-space()='Cancel']")
    }

    msgError() {
        return cy.xpath("//p[@class='el-message__content']")
    }

    btnConfirm() {
        return cy.xpath("//button[normalize-space()='Confirm']")
    }

    btnSetStatus() {
        return cy.xpath("//div[contains(text(),'Set Status')]")
    }

    chkSetStatus(checkbox) {
        return cy.xpath("//div[contains(@role,'radiogroup')]//span[contains(text(),'" + checkbox + "')]")
    }

    defaultCheckBoxButtonSetStatus(checkbox) {
        return cy.xpath("//div[contains(@role,'radiogroup')]//span[@class='el-radio__input is-checked']/following-sibling::span[contains(text(),'" + checkbox + "')]")
    }

    defaultOtherCheckBoxButtonSetStatus(checkbox) {
        return cy.xpath("//span[.='" + checkbox + "']/..//span")
        // return cy.xpath("//span[.='"+ checkbox +"']")
    }

    txtEffectiveDate() {
        return cy.xpath("//div[contains(text(),' Effective date / time')]/following-sibling::span//div//div//div[1]")
    }

    dtpSelectDateTime() {
        return cy.xpath("//div[contains(text(),'End date / time')]/following-sibling::div//*[local-name()='svg']")
    }

    txtSelectDateTime() {
        return cy.xpath("//div[contains(text(),'End date / time')]/following-sibling::div")
    }

    lblHour(hour) {
        return cy.xpath("(//div[@class='w-1/2 h-[226px] ps ps--active-y']//div[contains(text(),'" + hour + "')])[1]")
    }

    lblHourSelectDT(hour) {
        return cy.xpath("(//div[@class='w-1/2 h-[226px] ps ps--active-y']//div[contains(text(),'" + hour + "')])[2]")
    }

    lblHourED(hour) {
        return cy.xpath("(//div[@class='w-1/2 h-[226px] ps']//div[contains(text(),'" + hour + "')])[1]")
    }

    lblMinute(minute) {
        return cy.xpath("(//div[@class='w-1/2 h-[226px] selector-minute ps ps--active-y']//div[contains(text(),'" + minute + "')])[1]")
    }

    btnOKSetED() {
        return cy.xpath("//span[normalize-space()='Ok']")
    }

    lblStatus(status) {
        return cy.xpath("//span[.='" + status + "']")
    }

    btnSetSetStatus() {
        return cy.xpath("//span[normalize-space()='Set']")
    }

    lblEffectiveDateTime() {
        return cy.xpath("//div[.='Effective date:']/following-sibling::div")
    }

    lblEndDateTime() {
        return cy.xpath("//div[.='End date:']/following-sibling::div")
    }

    btnCancelSetStatus() {
        return cy.xpath("//button[@class='el-button el-button--default el-button--mini']")
    }

    btnCancelCancelSetStatus() {
        return cy.xpath("//button[@class='el-button el-button--default el-button--small']//span[contains(text(),'Cancel')]")
    }

    lblSetStatus() {
        return cy.xpath("//span[@class='el-dialog__title']")
    }

    btnAssign() {
        return cy.xpath("//span[contains(text(),'Assign')]")
    }

    txtSearchAssign() {
        return cy.xpath("//input[@placeholder='Search by Subcontractor ID, Company Tax ID, Company Registered Name, Company Name both TH and EN, Province']")
    }

    btnSearchAssign() {
        return cy.xpath("//button[@class='el-button flex items-center el-button--primary el-button--mini']")
    }

    tblSubcontractorID() {
        return cy.xpath("//td[1]//div//span")
    }

    btnSelectSub() {
        return cy.xpath("//td[1]//div//span[contains(.,'Select')]")
    }

    tblCompanyTaxID() {
        return cy.xpath("//td[4]//div//div")
    }

    tblCompanyName() {
        return cy.xpath("//td[5]//div//div")
    }

    lblNoData() {
        return cy.xpath("//div[.='No Data']")
    }

    btnDeleteKeywordAssign() {
        return cy.xpath("//input[@placeholder='Search by Subcontractor ID, Company Tax ID, Company Registered Name, Company Name both TH and EN, Province']/following-sibling::span//span//i")
    }

    cboPageNumber() {
        return cy.xpath("//span[@class='el-pagination__sizes']")
    }

    lblPageNumber(pageNum) {
        return cy.xpath("//li//span[normalize-space()='" + pageNum + "']")
    }

    btnReassign() {
        return cy.xpath("//span[normalize-space()='Reassign']")
    }

    lblAssignSubInfo(companyName) {
        return cy.xpath("//div[contains(text(),'" + companyName + "')]/following-sibling::div")
    }

    btnDeleteAssignSub() {
        return cy.xpath("(//div[contains(text(),'Subcontractor')])[2]/parent::div/following-sibling::*[name()='svg'][@class='cursor-pointer']")

    }

    btnOrderSubID() {
        return cy.xpath("//span[contains(text(),'Subcontractor ID')]/following-sibling::span//i[@class='sort-caret ascending']")
    }

    btnDeleteKeywordAssignVehicle() {
        return cy.xpath("//input[@placeholder='Search by License Plate Number, Provincial Sign, Company Registered Name, Vehicle ID, Site Code']/following-sibling::span//span//i")
    }

    chkAllVehicle() {
        return cy.xpath("//div[@class='el-table__fixed-header-wrapper']//table//th//div//span[@class='el-checkbox__inner']")
    }

    chkVehicle(vehicleID) {
        return cy.xpath("//td[2]//div//span[contains(text(),'" + vehicleID + "')]/ancestor::td/ancestor::div//tr[1]/td[1]//span[@class='el-checkbox__input']")
    }

    checkedVehicle(vehicleID) {
        return cy.xpath("//td[2]//div//span[contains(text(),'" + vehicleID + "')]/ancestor::td/ancestor::div//tr[1]/td[1]//span[@class='el-checkbox__input is-checked']")
    }

    lblValueSelected() {
        return cy.xpath("//div[@class='text-sm text-primary-85']")
    }

    btnClosePopupAssignVehicle() {
        return cy.xpath("//button[@class='el-dialog__headerbtn']//i")
    }

    btnRemoveVehicle() {
        return cy.xpath("//div[contains(text(),'Remove')]")
    }

    btnExportVehicle() {
        return cy.xpath("//div[contains(text(),'Export')]")
    }

    lblQuitCreate() {
        return cy.xpath("//span[normalize-space()='Quit creating driver']")
    }

    columnDriverID() {
        return cy.xpath("//td[1]//span")
    }

    columnSubcontractor() {
        return cy.xpath("//td[4]//span")
    }

    columnOccupiedVehicle() {
        return cy.xpath("//td[5]//span")
    }

    columnFirstName() {
        return cy.xpath("//td[6]//span")
    }

    columnLastName() {
        return cy.xpath("//td[7]//span")
    }

    columnMobilePhone() {
        return cy.xpath("//td[8]//span")
    }

    columnDriverLicenseNumber() {
        return cy.xpath("//td[9]//span")
    }

    columnDriverLicenseType() {
        return cy.xpath("//td[10]//span")
    }

    columnDriverLicenseExpiredDate() {
        return cy.xpath("//td[11]//span")
    }

    columnDriverStatus() {
        return cy.xpath("//td[13]//span//span")
    }

    btnSearchDriver() {
        return cy.xpath("//button[@class='el-button flex items-center el-button--primary el-button--mini']")
    }

    ipSearchDriver() {
        return cy.xpath("//input[@id='input-search']")
    }

    lblCompanyRegisterName() {
        return cy.xpath("//div[contains(@class,'text-base font-bold')]")
    }

    btnDeleteSearchDriver() {
        return cy.xpath("//input[@id='input-search']/following-sibling::span//span//i")
    }

    btnFilter() {
        return cy.xpath("//button[contains(.,'Filter')]")
    }

    filDriverLicenseType() {
        return cy.xpath("//div[contains(text(),'Driver License Type')]")
    }

    filDriverStatus() {
        return cy.xpath("//div[normalize-space()='Driver Status']")
    }

    cboFilDriverLicenseType() {
        return cy.xpath("//div[contains(text(),'Driver License Type')]/following-sibling::div")
    }

    cboFilDriverStatus() {
        return cy.xpath("//div[normalize-space()='Driver Status']/following-sibling::div")
    }

    btnResetFilter() {
        return cy.xpath("//div[normalize-space()='Reset filter']")
    }

    animationFilter() {
        return cy.xpath("//div[contains(text(),'Filter')]//*[local-name()='svg']")
    }

    numberPagePag(num) {
        return cy.xpath("//ul[@class='el-pager']//li[contains(text(),'" + num + "')]")
    }

    btnPrevPage() {
        return cy.xpath("//button[@class='btn-prev']")
    }

    btnNextPage() {
        return cy.xpath("//button[@class='btn-next']")
    }

    ipPageNumber() {
        return cy.xpath("//input[@type='number']")
    }

    btnViewHistoryLog() {
        return cy.xpath("//div[contains(text(),'View History Log')]")
    }

    lblHistory() {
        return cy.xpath("//p[contains(.,'History')]")
    }

    btnAssignVehicle() {
        return cy.xpath("//div[contains(text(),'Assign')]")
    }

    columnNationalIDNumberHis() {
        return cy.xpath("//td[19]//span")
    }

    columnSubcontractorHis() {
        return cy.xpath("//td[6]//span")
    }

    lblID() {
        return cy.xpath("//p[.='Driver ID:']/following-sibling::p")
    }

    txtSearchVehicle() {
        return cy.xpath("//input[@placeholder='Search by License Plate Number, Provincial Sign, Company Registered Name, Vehicle ID, Site Code']")
    }

    btnSearchVehicleAssign() {
        return cy.xpath("//input[@placeholder='Search by License Plate Number, Provincial Sign, Company Registered Name, Vehicle ID, Site Code']/parent::div/following-sibling::button")
    }

    columnVehicleID() {
        return cy.xpath("//td[2]//span")
    }

    lblQuitSettingStatus() {
        return cy.xpath("//span[normalize-space()='Quit setting status']")
    }

    lblMesQuitSetStatus() {
        return cy.xpath("//span[normalize-space()='Are you sure you want to quit setting status?']")
    }

    lblQuitEditingDriver() {
        return cy.xpath("//span[normalize-space()='Quit editing driver']")
    }

    lblMesQuitEditingDriver() {
        return cy.xpath("//div[contains(@class,'el-message-box__content')]//span[1]")
    }

    lblMessageTitle() {
        return cy.xpath("//div[@class='el-message-box__title']//span")
    }

    lblMessageContent() {
        return cy.xpath("//div[@class='el-message-box__content']")
    }

    columnFirstNameHis() {
        return cy.xpath("//td[11]//span")
    }

    columnDriverLicenseNumberHisLog() {
        return cy.xpath("//td[22]//span")
    }

    animationButtonEdit(action) {
        return cy.xpath("//span[contains(text(),'" + action + "')]/following-sibling::*[local-name()='svg']")
    }

    msgSuccess() {
        return cy.xpath("//i[@class='el-message__icon el-icon-success']//following-sibling::p[@class='el-message__content']")
    }

    errDriverNoSub() {
        return cy.xpath("//div[@class='text-white']")
    }

    txtFirstNamClear() {
        return cy.xpath("//div[contains(text(),' First name')]/following-sibling::span//input")
    }

    dtpExpiredDateDriver() {
        return cy.xpath("//div[contains(text(),'Select Expired Date')]")
    }




    //SCRIPTS
    createNewDriverV1(filePath, title, firstName, lastName, gender, day, email1st, email2st, phone, landLine, extension, nationalID, licenseNum, licenseType, licenseCategory) {
        this.btnClickToUpload("Driver photo").click()
        cy.wait(2000)
        this.ipClickToUpload("Driver photo").last().attachFile(filePath)
        cy.wait(2000)
        this.cboSelectTitle().click()
        this.lblTitle(title).click()
        cy.wait(2000)
        this.txtFirstName().type(firstName)
        cy.wait(2000)
        this.txtLastName().type(lastName)
        cy.wait(2000)
        this.cboGender().click()
        this.lblGender(gender).click()
        cy.wait(2000)
        this.dtpDateOfBirth("Date Of Birth").click()
        this.lblToDayDOB(day).click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.txtEmail().first().type(email1st + "@email.com")
        cy.wait(2000)
        this.txtEmail().last().type(email2st + "@email.com")
        cy.wait(2000)
        this.txtMobilePhoneNo().type(phone)
        cy.wait(2000)
        this.txtLandlinePhoneNo().type(landLine)
        cy.wait(2000)
        if (extension != "blank") {
            this.txtExtension().type(extension)
        }
        cy.wait(2000)
        this.btnClickToUpload("National ID photo").scrollIntoView()
        this.btnClickToUpload("National ID photo").click()
        this.ipClickToUpload("National ID photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtNationalIDCardNumber().type(nationalID)
        cy.wait(2000)
        this.dtpDateOfBirth("Expired Date").first().click()
        this.lblToDay(day).click()
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnClickToUpload("Driver License Photo").scrollIntoView()
        this.btnClickToUpload("Driver License Photo").click()
        this.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtDriverLicenseNumber().type(licenseNum)
        cy.wait(2000)
        this.cboDriverLicenseType().click()
        this.lblDriverLicenseType(licenseType).click()
        cy.wait(2000)
        this.cboDriverLicenseCategory().click()
        this.lblDriverLicenseCategory(licenseCategory).click()
        cy.wait(2000)
        this.dtpDateOfBirth("Issued Date").first().click()
        this.lblMonthDOB().click()
        this.lblMonth("Jan").last().click()
        this.lblDayPrevMonth("30").last().click({ force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.dtpDateOfBirth("Expired Date").last().click()
        const dayED2 = day
        cy.wait(2000)
        this.lblToDay(dayED2).last().click({ force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnSaveCreateNew().click({ force: true })
    }

    createNewDriverV2(filePath, title, firstName, lastName, gender, day, phone, landLine, extension, nationalID, licenseNum, licenseType, licenseCategory, subName) {
        this.btnClickToUpload("Driver photo").click()
        cy.wait(2000)
        this.ipClickToUpload("Driver photo").last().attachFile(filePath)
        cy.wait(2000)
        this.cboSelectTitle().click()
        this.lblTitle(title).click()
        cy.wait(2000)
        this.txtFirstName().type(firstName)
        cy.wait(2000)
        this.txtLastName().type(lastName)
        cy.wait(2000)
        this.cboGender().click()
        this.lblGender(gender).click()
        cy.wait(2000)
        this.dtpDateOfBirth("Date Of Birth").click()
        this.lblToDayDOB(day).click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.txtMobilePhoneNo().type(phone)
        cy.wait(2000)
        this.txtLandlinePhoneNo().type(landLine)
        cy.wait(2000)
        if (extension != "blank") {
            this.txtExtension().type(extension)
        }
        cy.wait(2000)
        this.btnClickToUpload("National ID photo").scrollIntoView()
        this.btnClickToUpload("National ID photo").click()
        this.ipClickToUpload("National ID photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtNationalIDCardNumber().type(nationalID)
        cy.wait(2000)
        this.dtpDateOfBirth("Expired Date").first().click()
        this.lblToDay(day).click()
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnClickToUpload("Driver License Photo").scrollIntoView()
        this.btnClickToUpload("Driver License Photo").click()
        this.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtDriverLicenseNumber().type(licenseNum)
        cy.wait(2000)
        this.cboDriverLicenseType().click()
        this.lblDriverLicenseType(licenseType).click()
        cy.wait(2000)
        this.cboDriverLicenseCategory().click()
        this.lblDriverLicenseCategory(licenseCategory).last().click({ force: true })
        cy.wait(2000)
        this.dtpDateOfBirth("Issued Date").first().click()
        this.lblToDay(day).first().click({ force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.dtpExpiredDateDriver().last().click({ force: true })
        cy.wait(2000)
        cy.selectYear(2030, 0)
        cy.selectMonth("February")
        cy.selectDay("today")
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnAssign().click()
        this.txtSearchAssign().should('be.visible').type(subName)
        this.btnSearchAssign().first().click()
        this.btnSelectSub().click()
        this.btnSaveCreateNew().click({ force: true })
    }

    createNewDriverV3(filePath, title, firstName, lastName, gender, day, phone, landLine, extension, nationalID, licenseNum, licenseType, licenseCategory, subName) {
        this.btnClickToUpload("Driver photo").click()
        cy.wait(2000)
        this.ipClickToUpload("Driver photo").last().attachFile(filePath)
        cy.wait(2000)
        this.cboSelectTitle().click()
        this.lblTitle(title).click()
        cy.wait(2000)
        this.txtFirstName().type(firstName)
        cy.wait(2000)
        this.txtLastName().type(lastName)
        cy.wait(2000)
        this.cboGender().click()
        this.lblGender(gender).click()
        cy.wait(2000)
        this.dtpDateOfBirth("Date Of Birth").click()
        this.lblToDayDOB(day).click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.txtMobilePhoneNo().type(phone)
        cy.wait(2000)
        this.txtLandlinePhoneNo().type(landLine)
        cy.wait(2000)
        if (extension != "blank") {
            this.txtExtension().type(extension)
        }
        cy.wait(2000)
        this.btnClickToUpload("National ID photo").scrollIntoView()
        this.btnClickToUpload("National ID photo").click()
        this.ipClickToUpload("National ID photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtNationalIDCardNumber().type(nationalID)
        cy.wait(2000)
        this.dtpDateOfBirth("Expired Date").first().click()
        this.lblToDay(day).click()
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.btnClickToUpload("Driver License Photo").scrollIntoView()
        this.btnClickToUpload("Driver License Photo").click()
        this.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        cy.wait(2000)
        this.txtDriverLicenseNumber().type(licenseNum)
        cy.wait(2000)
        this.cboDriverLicenseType().click()
        this.lblDriverLicenseType(licenseType).click()
        cy.wait(2000)
        this.cboDriverLicenseCategory().click()
        this.lblDriverLicenseCategory(licenseCategory).last().click({ force: true })
        cy.wait(2000)
        this.dtpDateOfBirth("Issued Date").first().click()
        this.lblToDay(day).first().click({ force: true })
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        cy.wait(2000)
        this.dtpExpiredDateDriver().last().click({ force: true })
        cy.wait(2000)
        cy.selectYear(2030, 0)
        cy.selectMonth("January")
        cy.selectDay("today")
        cy.wait(2000)
        this.btnSetDate().click({ multiple: true, force: true })
        this.btnSaveCreateNew().click({ force: true })
    }

    dtpIcon(name) {
        return cy.xpath("//div[text()='" + name + "']//following-sibling::*[local-name()='svg']")
    }

    createNewDriverEtoE(firstName, subcontractor, mobilePhone, action, confirm, click, verify) {
        this.btnMainFunction("Create").click({ force: true })
        // this.ipClickToUpload("Driver photo").last().attachFile(filePath)
        this.commonMD.ipSelectBox("Select Title").click({ force: true })
        this.commonMD.selectRandomValue()
        if (firstName == "generate") {
            cy.randomString(5).then((random) => {
                this.commonMD.inputDataToField("Enter First Name", random)
            })
        } else {
            this.commonMD.inputDataToField("Enter First Name", firstName)
        }
        cy.randomString(5).then((lastName) => {
            this.commonMD.inputDataToField("Enter Last Name", lastName)
        })
        this.commonMD.ipSelectBox("Select Gender").click({ force: true })
        this.commonMD.selectRandomValue()

        cy.wait(1000)
        //assign Subcontractor
        if (subcontractor != "empty") {
            this.commonMD.selectSubcontractor("Subcontractor", subcontractor)
        }

        this.dtpDateOfBirth("Date Of Birth").click({ force: true })
        cy.selectYear(2000, 0)
        cy.selectMonth("thisMonth")
        cy.selectDay(12)
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })
        // cy.randomNumber(9).then((phone) => {
        //     // var phoneNum = "01100";
        //     var finalResult = phone
        //     this.commonMD.inputDataToField("Enter Mobile Phone No.", finalResult)
        // })
        this.commonMD.inputDataToField("Enter Mobile Phone No.", mobilePhone)
        //National ID
        // this.btnClickToUpload("National ID photo").scrollIntoView()
        // this.ipClickToUpload("National ID photo").last().attachFile(filePath)
        cy.wait(1000)
        cy.randomNumber(13).then((nationalID) => {
            this.commonMD.inputDataToField("Enter National ID Card Number", nationalID)
        })
        this.dtpIcon("Select date").scrollIntoView().click({ force: true })
        // cy.selectYear(2050, 0)
        cy.selectYear(2030, 0)
        cy.selectMonth("thisMonth")
        cy.selectDay(10)
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })
        //Driver license
        // this.btnClickToUpload("Driver License Photo").scrollIntoView()
        // this.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        cy.wait(1000)
        cy.randomNumber(16).then((licenseNum) => {
            this.commonMD.inputDataToField("Enter Driver License Number", licenseNum)
        })
        this.commonMD.ipSelectBox("Select Driver License Type").click({ force: true })
        this.commonMD.selectRandomValue()
        this.commonMD.ipSelectBox("Select Driver License Category").click({ force: true })
        this.commonMD.spanValue("ส่วนบุคคล").last().click({ force: true })

        this.dtpDateOfBirth("Issued Date").first().click({ force: true })
        cy.selectYear("thisYear", 0)
        cy.selectMonth("thisMonth")
        cy.selectDay("today")
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })
        cy.wait(2000)
        this.dtpIcon("Select Expired Date").scrollIntoView().click({ force: true })
        // cy.selectYear(2050, 0)
        cy.selectYear(2030, 0)
        cy.selectMonth("thisMonth")
        cy.selectDay("today")
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        this.commonMD.confirmCreateData(action, confirm, click, verify)
        return new Driver()
    }

    verifyNewDriver(subcontractor, firstName, lastName, mobilePhone, driverLicenseNumber, driverLicenseType, driverLicenseExpDate) {
        this.columnSubcontractor().first().should(($el) => {
            expect($el).to.contain(subcontractor)
        })
        this.columnFirstName().first().should(($el) => {
            expect($el).to.have.text(firstName)
        })
        this.columnLastName().first().should(($el) => {
            expect($el).to.have.text(lastName)
        })
        this.columnMobilePhone().first().should(($el) => {
            expect($el).to.contain(mobilePhone)
        })
        this.columnDriverLicenseNumber().first().should(($el) => {
            expect($el).to.have.text(driverLicenseNumber)
        })
        this.columnDriverLicenseType().first().should(($el) => {
            expect($el).to.contain(driverLicenseType)
        })
        this.columnDriverLicenseExpiredDate().first().should(($el) => {
            expect($el).to.contain(driverLicenseExpDate)
        })
    }

    verifyWhenInputFirstName(text) {
        var count = 0
        for (var i = 0; i < text.length; i++) {
            if (this.txtFirstName().type('{backspace}')) {
                count++
            }
        }
        if (count = text.length) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifyWhenInputLastName(text) {
        var count = 0
        for (var i = 0; i < text.length; i++) {
            if (this.txtLastName().type('{backspace}')) {
                count++
            }
        }
        if (count = text.length) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifySpaceWhenInputFirstName(text) {
        var count = 0
        for (var i = 0; i < text.length; i++) {
            if (this.txtFirstName().type('{backspace}')) {
                count++
            }
        }
        if (count = text.length - 1) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifySpaceWhenInputEmail1st(text) {
        var count = 0
        for (var i = 0; i < text.length; i++) {
            if (this.txtEmail().first().type('{backspace}')) {
                count++
            }
        }
        if (count = text.length - 1) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifySpaceWhenInputEmail2nd(text) {
        var count = 0
        for (var i = 0; i < text.length; i++) {
            if (this.txtEmail().last().type('{backspace}')) {
                count++
            }
        }
        if (count = text.length - 1) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifySpaceWhenInputPhoneNo(phone) {
        var count = 0
        for (var i = 0; i < phone.length; i++) {
            if (this.txtMobilePhoneNo().type('{backspace}')) {
                count++
            }
        }
        if (count = phone.length) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifySpaceWhenInputLandlineNo(phone) {
        var count = 0
        for (var i = 0; i < phone.length; i++) {
            if (this.txtLandlinePhoneNo().type('{backspace}')) {
                count++
            }
        }
        if (count = phone.length - 1) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }

    verifyWhenNationalIDNumber(id) {
        var count = 0
        for (var i = 0; i < id.length; i++) {
            if (this.txtLandlinePhoneNo().type('{backspace}')) {
                count++
            }
        }
        if (count = id.length - 1) {
            cy.log(count)
            cy.log("Pass")
        }
        else {
            cy.log(count)
            cy.log("Fail")
        }
    }
}

export default Driver