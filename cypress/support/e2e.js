// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
// cypress/support/index.js -> cypress/support/e2e.js
import './commands';
import '@4tw/cypress-drag-drop';
import 'cypress-mochawesome-reporter/register';
import "cypress-real-events";
// import '@shelex/cypress-allure-plugin';
// import addContext from "mochawesome/addContext";
require('cypress-xpath')

// Alternatively you can use CommonJS syntax:
// require('./commands')
Cypress.on('uncaught:exception', (err, runnable) => {
	// returning false here prevents Cypress from
	// failing the test
	return false
})

// Cypress.on("test:after:run", (test, runnable) => {
// 	if (test.state === "failed") {
// 		const screenshot = `assets/${Cypress.spec.name}/${runnable.parent.title} -- ${test.title} (failed) (attempt 2).png`;
// 		addContext({ test }, screenshot);
// 	}
// });

afterEach(() => {
	cy.window().then(win => {
		// window.gc is enabled with --js-flags=--expose-gc chrome flag
		if (typeof win.gc === 'function') {
			// run gc multiple times in an attempt to force a major GC between tests
			win.gc();
			win.gc();
			win.gc();
			win.gc();
			win.gc();
		}
	});
});
