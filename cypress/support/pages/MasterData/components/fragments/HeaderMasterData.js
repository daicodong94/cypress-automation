class HeaderMasterData {

    pageTitle(title) {
        return cy.xpath("//span[@class='text-xl font-medium' and text()='" + title + "']")
    }

    pageTitleShouldBe(title) {
        this.pageTitle(title).should("be.visible")
    }

    txtSearch() {
        return cy.xpath('//input[@id="input-search"]')
    }

    btnSearch() {
        return cy.xpath('//button[@class="el-button flex button-search el-button--primary el-button--mini"]')
    }

    btnFilter() {
        return cy.xpath('//button//span[contains(text(),"Filter")]')
    }

    btnResetFilter() {
        return cy.xpath('//button//span[contains(text(),"Reset filter")]')
    }

    btnImport() {
        return cy.xpath('//button//span[contains(text(),"Import")]')
    }

    btnExport() {
        return cy.xpath('//button//span[contains(text(),"Export")]')
    }

    btnCreate() {
        return cy.xpath('//button//span[contains(text(),"Create")]')
    }

    btnViewHistoryLog() {
        return cy.xpath('//button//span[contains(text(),"View History Log")]')
    }

    clkBtnViewHistoryLog() {
        this.btnViewHistoryLog().click({ force: true })
    }

    clkBtnViewHistoryLog() {
        this.btnViewHistoryLog().click({ force: true })
    }

}
export default HeaderMasterData;