import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";


class ItemType{

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //XPATH
    txtItemtype(){
        return cy.xpath("//input[@data-vv-name='name']")
    }
    
    cboTemperatureControlType(){
        return cy.xpath("//input[@placeholder='please choose']")
    }

    lblTemperature(temp) {
        return cy.xpath("//li//span[normalize-space()='" + temp + "']")
    }

    txtTypeCode(){
        return cy.xpath("//input[@data-vv-name='code']")
    }

    txtKeyword(){
        return cy.xpath("//input[contains(@placeholder,'keywords (enter to add)')]")
    }

    columnPriority(){
        return cy.xpath("//td[2]//div")
    }

    columnItemType(){
        return cy.xpath("//td[3]//div//span")
    }

    columnTypeCode(){
        return cy.xpath("//td[4]//div")
    }

    columnStatus(){
        return cy.xpath("//td[5]//div")
    }

    columnDefault(){
        return cy.xpath("//td[6]//div")
    }

    columnKeyword(){
        return cy.xpath("//td[8]//div")
    }

    ipItemType(){
        return cy.xpath("//input[@placeholder='item type']")
    }

    lblItemType(item){
        return cy.xpath("//span[contains(text(), '" + item + "')]")
    }

    btnOperate(ope){
        return cy.xpath("//button//span[contains(text(),'" + ope + "')]")
    }

    btnCloseAddNewPopup(){
        return cy.xpath("//div[@aria-label='Add item type']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnCloseKeyword(){
        return cy.xpath("//i[@class='el-tag__close el-icon-close']")
    }

    btnNextPage(){
        return cy.xpath("//i[@class='el-icon el-icon-arrow-right']")
    }

    btnPrevPage(){
        return cy.xpath("//i[@class='el-icon el-icon-arrow-left']")
    }

    //SCRIPTS

    addNewProdType(type, temp, code, key){
        this.header.btnAtHeader("new").click()
        cy.wait(5000)
        this.txtItemtype().type(type)
        cy.wait(2000)
        this.cboTemperatureControlType().click()
        cy.wait(2000)
        this.lblTemperature(temp).click()
        cy.wait(2000)
        this.txtTypeCode().type(code)
        cy.wait(2000)
        this.txtKeyword().type(key)
        cy.wait(2000)
        this.common.btnConfirmAgainHintPopup("save").click()
        
    }

    verifyAddNewProdType(){
        this.common.msgSuccess().should('be.visible')
        ct.wait(5000)
        this.verifyNewItem(text, "normal")
    }

    searchItem(type){
        this.ipItemType().clear()
        this.ipItemType().type(type)
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({force : true})
    }

    verifyNewItem(text, temp){
        this.searchItem(text)
        cy.wait(5000)
        this.columnItemType().first().should('have.text', text)
        cy.wait(2000)
        this.columnTypeCode().first().should('have.text', text)
        cy.wait(2000)
        this.columnStatus().first().should('have.text', 'created')
        cy.wait(2000)
        this.columnDefault().first().should('have.text', temp)
        cy.wait(2000)
        this.columnKeyword().first().should('have.text', text)
        cy.wait(2000)
    }

    deleteItemType(type){
        this.searchItem(type)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        cy.wait(2000)
        this.header.btnAtHeader("delete").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    verifyItemJustHasBeenDeleted(type){
        this.common.msgSuccess().should('be.visible')
        this.searchItem(type)
        cy.wait(5000)
        this.lblItemType(type).should('not.to.exist')
    }

    enableItemType(type){
        this.searchItem(type)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        cy.wait(2000)
        this.header.btnAtHeader("enable").click()
    }

    disabledItemType(type){
        this.searchItem(type)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        this.header.btnAtHeader("enable").click()
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        this.header.btnAtHeader("disabled").click()
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    checkStatus(status){
        var count = 0
        this.columnStatus().each(($el)=>{
            const statusS = $el.text().trim()
            cy.log(statusS)
            if(statusS === status){
                count += 1       
            }
            else{
                cy.log("Fail")
            }
        }).then(()=>{   
            this.header.totalValue().should('contain', 'Total '+ count)
        })
    }

    verifyClickButtonTopOperate(){
        this.btnNextPage().click()
        this.columnItemType().last().then(($text) => {
            this.btnOperate("top").last().click({force : true})
            this.btnPrevPage().click()
            cy.wait(10000)
            this.columnPriority().first().should('have.text', '1')
            this.columnItemType().first().should('contain', $text.text().trim())
        })
    }

    verifyClickButtonBottomOperate(){
        this.columnItemType().first().then(($text)=>{
            this.common.selectPageSize("500/page")   
            this.btnOperate("bottom").first().click()
            cy.wait(10000)
            this.columnItemType().last().should('have.text', $text.text().trim())
        })
        
    }

    editItem(type, temp, key){
        this.searchItem(type)
        cy.wait(5000)
        this.common.openDetailView()
        cy.wait(5000)
        this.cboTemperatureControlType().click()
        cy.wait(2000)
        this.lblTemperature(temp).click()
        cy.wait(2000)
        this.btnCloseKeyword().click()
        this.txtKeyword().type(key)
        cy.wait(2000)
        this.header.btnAtHeader("save").click({multiple : true, force : true})
    }

    verifyEditItem(term, text){
        this.common.msgSuccess().should('be.visible')
        this.columnDefault().first().should('have.text', term)
        cy.wait(2000)
        this.columnKeyword().first().should('have.text', text)
        cy.wait(2000)
    }
}

export default ItemType;