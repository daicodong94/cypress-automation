import Homepage from "../../../support/pages/TMS/components/homepage/Homepage";
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood";
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp";
import Login from "../../../support/pages/TMS/components/login/Login";
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan";
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule";
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder";
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder";
import DispatchOrderTracking from "../../../support/pages/TMS/components/track/DispatchOrderTracking";
import ShippingOrderTracking from "../../../support/pages/TMS/components/track/ShippingOrderTracking";
import { Utility } from "../../../support/utility"

describe("Checking Our - Item type", () => {

    var objShippingOrderTracking = new ShippingOrderTracking(), data, objLogin = new Login(), objHome = new Homepage(), objDispatchOrderTracking = new DispatchOrderTracking(),
        objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objSchedule = new Schedule(), objDeliveryPlan = new DeliveryPlan(), objDeliverGoods = new DeliveryGood(), objPickup = new Pickup()
    var customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("TMS-Our-Service item", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(() => {
        cy.wait(10000)
        objHome.logout()
    })

    it.skip("TC_12 - Search shipping order tracking", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "disabled")
        objShippingOrderTracking.common.selectPageSize("500/page")

        objShippingOrderTracking.checkStatus("disabled")
    })

    it.skip("TC_13 - Check state is show in list search response at Shipping Order Tracking screen - CREATED", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("save", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.reload()
            objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
            // cy.wait(60000)
            objShippingOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objShippingOrderTracking.checkStatus("created")
        })
    })

    it.skip("TC_13 - Check state is show in list search response at Shipping Order Tracking screen - CHECKED", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
            // cy.wait(60000)
            objShippingOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objShippingOrderTracking.checkStatus("checked")
        })
    })

    it.skip("TC_13 - Check state is show in list search response at Shipping Order Tracking screen - DISPATCHED", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
            // cy.wait(60000)
            objShippingOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objShippingOrderTracking.checkStatus("dispatched")
        })
    })

    it.skip("TC_13 - Check state is show in list search response at Shipping Order Tracking screen - TRANSITED", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(2000)
            objPickup.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objPickup.common.openDetailView()
            cy.wait(2000)
            objPickup.chooseCompletionTime(5)
            cy.wait(2000)
            objPickup.clickbtnFunctionPickup("pickup completed")
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(5000)
            cy.reload()
            objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
            // cy.wait(60000)
            objShippingOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objShippingOrderTracking.checkStatus("transited")
        })
    })

    it.skip("TC_13 - Check state is show in list search response at Shipping Order Tracking screen - COMPLETED", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(2000)
            objPickup.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objPickup.common.openDetailView()
            cy.wait(2000)
            objPickup.chooseCompletionTime(5)
            cy.wait(2000)
            objPickup.clickbtnFunctionPickup("pickup completed")
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(5000)
            cy.reload()
            objDeliverGoods = objHome.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
            objDeliverGoods.common.searchByClickSuggestion("please enter the dispatch number", trackingNum)
            cy.wait(2000)
            objDeliverGoods.common.openDetailView()
            cy.wait(2000)
            objDeliverGoods.chooseTimeForDeliveryCompletionTime(30)
            cy.wait(2000)
            objDeliverGoods.header.btnAtHeader("Delivery completed").click()
            cy.wait(2000)
            objDeliverGoods.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            cy.reload()
            objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
            // cy.wait(60000)
            objShippingOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objShippingOrderTracking.checkStatusGreen("completed")
        })
    })

    it.skip("TC_14 - Check Closure feature with order which has state New at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "created")
        cy.wait(2000)
        objShippingOrderTracking.checkStatusWhenChooseOrderThenClickButtonClose("disabled")
        objShippingOrderTracking.common.selectPageSize("500/page")

        objShippingOrderTracking.checkStatus("disabled")
    })

    it.skip("TC_15 - Check Closure feature with order which has state Audited at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "checked")
        cy.wait(2000)
        objShippingOrderTracking.checkStatusWhenChooseOrderThenClickButtonClose("closed")
        objShippingOrderTracking.checkStatusGray("closed")
    })

    it.skip("TC_16 - Check Closure feature with order which has state dispatched at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "dispatched")
        cy.wait(2000)
        objShippingOrderTracking.checkStatusWhenChooseOrderThenClickButtonClose("closed")
        objShippingOrderTracking.checkStatusGray("closed")
    })

    it.skip("TC_17 - Check Closure feature with order which has state In Transit at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "transited")
        cy.wait(2000)
        objShippingOrderTracking.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(2000)
        objShippingOrderTracking.checkMessageErrorWhenClickButtonClose("confirm", "This order cannot closed!")
    })

    it.skip("TC_18 - Check Closure feature with order which has state Completed at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.searchBySelectStatus("status", "completed")
        cy.wait(2000)
        objShippingOrderTracking.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(2000)
        objShippingOrderTracking.checkMessageErrorWhenClickButtonClose("confirm", "This order has completed unable closed!")
    })

    it("TC_19 - Check paging at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.selectPageSize("20/page")
        cy.wait(2000)
        objShippingOrderTracking.common.selectPageSize("50/page")
        cy.wait(2000)
        objShippingOrderTracking.common.selectPageSize("100/page")
        cy.wait(2000)
        objShippingOrderTracking.common.selectPageSize("200/page")
        cy.wait(2000)
        objShippingOrderTracking.common.selectPageSize("500/page")
    })

    it("TC_20 - Check function Fold at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.clickLinkButton("more filter...")
        cy.screenshot('clickLinkButtonMore')
        cy.wait(2000)
        objShippingOrderTracking.common.clickLinkButton("collapse filter")
        cy.screenshot('clickLinkButtonFold')
    })

    it("TC_21 - Check function Set Column - Disable header at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.enableOrDisableColumn("shipping order number", "no")
    })

    it("TC_22 - Check function Set Column - Enabled header at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.enableOrDisableColumn("shipping order number", "yes")
    })

    it("TC_23 - Check function Set Column - Drag and drop to reorder at Shipping Order Tracking", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.setColumn("shipping order number", "status")
    })

    it("TC_24 - Check function Set Column - Reset at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.resetDefaultColumn()
        objShippingOrderTracking.btnCloseSetColumn().click()
    })

    it("TC_25 - Check go to next page at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBeNextPageWhenClickButtonNextPage()
    })

    it("TC_26 - Check cannot go to next page at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBeDisabledWhenClickButtonNextPageFirst()
    })

    it("TC_27 - Check go to previous page at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBePreviousPageWhenClickButtonPreviousPage()
    })

    it("TC_28 - Check cannot go to prev page at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBeDisabledWhenClickButtonPreviousPageFirst()
    })

    it("TC_29 - Check go to page by input page number at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBePageWhenInputPageNumber("3")
    })

    it("TC_30 - Check cannot go to page by input page number at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBePageWhenInputPageNumber("1000")
    })

    it("TC_32 - Check return to Shipping order screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.shouldBeBackShippingOrderTrackingScreenWhenClickButtonReturnInDetailScreen()
    })

    it("TC_33 - Check create new order by click copy a new order at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrder = objShippingOrderTracking.copyANewOrder()
        cy.randomString(8).then((numberTracking) => {
            objShippingOrder.copyANewOrderInTrack("save", numberTracking, "2")
            objShippingOrder.checkNumberTrackingExist(numberTracking)
        })
    })

    it("TC_34 - Check terminated order at Shipping Order Tracking screen", () => {
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", trackingNum, "Automation Company", product, temperature, siteA, siteB, 5);
            cy.reload()
            objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            // cy.wait(60000)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum, "first")
            cy.wait(5000)
            objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })
            objDeliveryPlan.header.btnAtHeader("create a dispatch order ").click()
            // cy.wait(60000)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", trackingNum, "second")
            cy.wait(5000)
            objDeliveryPlan.common.columnDispatchOrderNumber().last().then(($text) => {
                objDeliveryPlan.common.columnDispatchOrderNumber().last().dblclick({ force: true })
                cy.wait(2000)
                objDeliveryPlan.header.btnAtHeader("transit").click({ force: true })
                cy.wait(2000)
                objDeliveryPlan.common.textField("contact number").click({ force: true })
                cy.wait(2000)
                objDeliveryPlan.selectContactPerson("66251366473")
                cy.wait(5000)
                objDeliveryPlan.common.btnConfirmAgainHintPopup("save").click({ multiple: true, force: true })
                cy.wait(2000)
                cy.reload()
                objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                const dispatchOrderTransit = "" + $text.text().trim() + "-A"
                objDeliveryPlan.searchDispatrchOrder("dispatch number", dispatchOrderTransit)
                cy.wait(5000)
                objDeliveryPlan.searchResultWithOutValueAt2ndTable().first().click({ force: true })
                cy.wait(2000)
                objDeliveryPlan.header.btnAtHeader("confirm").last().click({ force: true })
                cy.wait(2000)
                cy.reload()
                objSchedule = objHome.leftMenu.navigateToPage("Schedule")
                // cy.wait(60000)
                objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
                cy.wait(2000)
                objSchedule.common.openDetailView()
                objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
                cy.wait(2000)
                objSchedule.lnkAddVehicle().click()
                cy.wait(2000)
                objSchedule.selectDriver("Auto Driver")
                cy.wait(5000)
                objSchedule.btnAction("confirm and dispatch").click()
                cy.wait(5000)
                cy.reload()
                objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(2000)
                objPickup.common.searchByClickSuggestion("dispatch number", $text.text().trim())
                cy.wait(2000)
                objPickup.common.openDetailView()
                cy.wait(2000)
                objPickup.chooseCompletionTime(5)
                cy.wait(2000)
                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(2000)
                objPickup.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(5000)
                cy.reload()
                objDeliverGoods = objHome.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                objDeliverGoods.common.searchByClickSuggestion("please enter the dispatch number", $text.text().trim())
                cy.wait(2000)
                objDeliverGoods.common.openDetailView()
                cy.wait(2000)
                objDeliverGoods.chooseTimeForDeliveryCompletionTime(30)
                cy.wait(2000)
                objDeliverGoods.header.btnAtHeader("Delivery completed").click()
                cy.wait(2000)
                objDeliverGoods.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(2000)
                cy.reload()
                objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
                // cy.wait(30000)
                objShippingOrderTracking.searchTrackingNumber($text.text().trim())
                cy.wait(5000)
                objShippingOrderTracking.common.openDetailView()
                cy.wait(2000)
                objShippingOrderTracking.header.btnAtHeader("Shipment terminated").click({ force: true })
                cy.wait(2000)
                objShippingOrderTracking.common.btnConfirmHintPopUp("confirm").click({ force: true })
                cy.wait(5000)
                objShippingOrderTracking.header.btnAtHeader("return").click({ force: true })
                cy.wait(2000)
                objShippingOrderTracking.header.btnAtHeader("inquire").click({ force: true })
                cy.wait(5000)
                objShippingOrderTracking.checkStatusGreen("completed")
            })
        })
    })

    it("TC_35 - Check Service information at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.header.btnAtHeader("service information").click({ force: true })
        objShippingOrderTracking.checkServiceInformation()
        objShippingOrderTracking.btnCloseServiceInfor().click()
    })

    it("TC_36 - Check Execution Information at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickExecutionInformation()
    })

    it("TC_37 - Check Billing Information at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickBillingInformation()
    })

    it("TC_38 - Check Record Information at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickRecordInformation()
        cy.randomString(8).then((text) => {
            objShippingOrderTracking.inputSomeText(text)
            objShippingOrderTracking.clickButtonSaveEditRecord()
            objShippingOrderTracking.shouldBeShowMessageSucess(text)
        })
    })

    it("TC_39 - Check File management at Shipping Order Tracking screen: Add attachments", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickFileManagement()
        var filePath = "shippingorder/Detail import.xls"
        objShippingOrderTracking.addFileAttachments(filePath)
        cy.wait(2000)
        objShippingOrderTracking.shouldBeShowMessageSuccessWhenAddFileAttachments(filePath.substr(14, 17))
    })

    it("TC_40 - Check File management at Shipping Order Tracking screen: View attachments", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickFileManagement()
    })

    it("TC_41 - Check File management at Shipping Order Tracking screen: Download attachments", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickFileManagement()
        objShippingOrderTracking.clickButtonDownloadPackageAttachment()
    })

    it("TC_42 - Check Operation record at Shipping Order Tracking screen", () => {
        objShippingOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Shipping Order Tracking")
        cy.wait(2000)
        objShippingOrderTracking.common.openDetailView()
        objShippingOrderTracking.clickOperationRecord()
    })
})