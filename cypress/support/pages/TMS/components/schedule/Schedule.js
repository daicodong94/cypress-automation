import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class Schedule {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    btnAction(action) {
        return cy.xpath("//div[@class='tab-content h-full']//button//span[text()='" + action + "']")
    }

    //status
    dispatchScheduleStatus(status) {
        return cy.xpath("//table//tr[@class='el-table__row']//td//span[text()='" + status + "']")
    }

    columnDispatchNumber() {
        return cy.xpath("//td//div//span[contains(text(),'R00')]")
    }

    withdrawSchedule(confirm) {
        this.common.searchResultWithOutValue().click()
        this.header.btnAtHeader("withdraw schedule").click()
        cy.wait(1000)
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").click()
            this.common.msgSuccess().should("be.visible")
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopUp("cancel").click()
            this.common.msgCancel().should("be.visible")
        }
    }

    replanDispatch() {
        this.header.searchResultWithOutValue().click()
        this.header.btnAtHeader("re-plan").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should("be.visible")
    }

    ipCarrier() {
        return cy.xpath("(//div[@class='search-candidate w450']//input)[last()]")
    }

    spanSearchResult() {
        return cy.xpath("//div[@class='search-candidate-list select-scroll']//span[@class='search-highlight']")
    }

    selectCarrier(carrier) {
        this.header.ipSearchBox("Please enter a subcontractor").click({ force: true })
        this.ipCarrier().should("be.visible", { timeout: 5000 }).type(carrier, { force: true })
        this.spanSearchResult().should("be.visible", { timeout: 5000 })
        this.spanSearchResult().last().click({ force: true })
    }

    selectConsignment(consignment) {
        this.header.ipSearchBox("Please select surcharge / discount").last().click({ force: true })
        cy.wait(1000)
        this.header.aDropdownValue(consignment).scrollIntoView().click({ force: true })
    }

    lnkAddVehicle() {
        return cy.xpath("//button[@class='el-button !font-bold el-button--text el-button--mini']//span[contains(text(),'+add a vehicle')]")
    }

    selectDriver(driver) {
        this.header.ipSearchBox("new phone number in capacity pool").click({ force: true })
        cy.wait(1000)
        this.header.spanDropdownValueDriver(driver).click({ force: true })
    }

    spanVehicleType(type) {
        return cy.xpath("(//div[@class='search-candidate-list'])[last()]//li[@class='search-candidate-item']//span[starts-with(text(),'" + type + " ')]")
    }

    changeActualVehicleType(type) {
        this.header.ipSearchBox("new car model in capacity pool").click({ force: true })
        this.spanVehicleType(type).should("be.visible", { timeout: 5000 }).its('length').then((len) => {
            this.spanVehicleType(type).eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    sendEmailTab() {
        return cy.xpath("//ul[@class='notice-item notice-item-email']")
    }

    //batch schedule
    ddlCarrier() {
        return cy.xpath("//div[@class='el-dialog__body']//div[@class='search-content el-input']//input")
    }

    ddlConsignment() {
        return cy.xpath("//div[@class='out-box search-content']")
    }

    ipSearchBox() {
        return cy.xpath("(//div[@class='search-cadidate-keyword']//input)[last()]")
    }

    cboItems() {
        return cy.xpath("//input[@placeholder='Please enter']")
    }

    lblServiceItemCarrier(item) {
        return cy.xpath("//a[contains(.,'" + item + "')]")
    }

    spanSearchHighlight(result) {
        return cy.xpath("//div[@class='partner-name-wrapper']//span[@class='search-highlight' and text()='" + result + "']")
    }

    selectCarrierToBatchSchedule(carrier) {
        this.ddlCarrier().click()
        this.ipSearchBox().type(carrier)
        cy.wait(1000)
        this.spanSearchHighlight(carrier).click()
    }

    selectConsignment2(consignment) {
        this.ddlConsignment().click()
        cy.wait(1000)
        this.common.lblElement(consignment).click()
    }

    batchSchedule(numberOfDispatch, carrier, consignment, confirm) {
        this.common.selectMultiRecord(numberOfDispatch)
        cy.wait(1000)
        this.header.btnAtHeader("batch schedule").click()
        cy.wait(1000)
        if (carrier == "blank") {
            this.common.btnDialogFooter("confirm").click()
            cy.wait(2000)
            this.common.btnDialogFooter("cancel").click()
        } else {
            this.selectCarrierToBatchSchedule(carrier)
            cy.wait(1000)
            if (consignment == "blank") {
                this.common.btnDialogFooter("confirm").click()
                cy.wait(2000)
                this.common.btnDialogFooter("cancel").click()
            } else {
                this.selectConsignment(consignment)
                if (confirm == "yes") {
                    this.common.btnDialogFooter("confirm").click()
                    this.common.msgSuccess().should("be.visible")
                } else if (confirm == "cancel") {
                    this.common.btnDialogFooter("cancel").click()
                    this.common.msgSuccess().should("not.exist")
                }
            }
        }
    }

    rePlan(confirm) {
        this.common.searchResultWithOutValue().click()
        this.header.btnAtHeader("re-plan").click()
        cy.wait(1000)
        if (confirm == "confirm") {
            this.common.btnConfirmHintPopUp("confirm").click()
            this.common.msgSuccess().should("be.visible")
        } else if (confirm == "cancel") {
            this.common.btnConfirmHintPopUp("cancel").click()
            this.common.msgAlert().should("be.visible")
            this.common.msgSuccess().should("not.exist")
        }
    }

    textAreaReason() {
        return cy.xpath('//textarea[@data-vv-name="reason"]')
    }

    selectDataAndSchedule(carrier, consignment, driver, action) {
        this.selectCarrier(carrier)
        cy.wait(1000)
        this.selectConsignment(consignment)
        cy.wait(1000)
        this.common.radioBtnDifferentFromRequest().click()
        this.textAreaReason().type("Create schedule with different from requested")
        this.lnkAddVehicle().click({force: true})
        this.selectDriver(driver)
        cy.wait(1000)
        this.btnAction(action).click({force: true})
    }

    firstDriver() {
        return cy.xpath("(//table[@class='common-table-native'])[last()]//tr[@class='search-candidate-item active']")
    }

    selectDataAndSchedule2(carrier, consignment, action) {
        this.selectCarrier(carrier)
        cy.wait(1000)
        this.selectConsignment(consignment)
        cy.wait(1000)
        this.common.radioBtnDifferentFromRequest().click()
        this.textAreaReason().type("Create schedule with different from requested")
        this.lnkAddVehicle().click()
        // this.selectDriver(driver)
        this.header.ipSearchBox("new driver in capacity pool").click({ force: true })
        cy.wait(1000)
        this.firstDriver().click({ force: true })
        cy.wait(1000)
        this.btnAction(action).click()
    }

    confirmAndDispatchE2E(dispatchNumber, subNameTH, namePDB, driverName) {
        this.common.searchByClickSuggestion("Dispatch Number", dispatchNumber.text().trim())
        this.common.openDetailView()
        this.selectCarrier(subNameTH)
        this.cboItems().click()
        this.cboItems().type(namePDB)
        this.lblServiceItemCarrier(namePDB).click()
        this.lnkAddVehicle().click()
        this.selectDriver(driverName)
        this.btnAction("Confirm and Dispatch").click({ force: true })
        this.common.msgSuccess().should('be.visible')
    }
}

export default Schedule;