import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"

class Permission {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //btnAtHeader("save/+add role")

    addRole(roleName) {
        this.header.btnAtHeader("+ add role").click({ force: true })
        cy.wait(1000)
        // cy.randomStringAndNumber(7).then((name) => {
        //     this.header.ipSearchBox("please enter").type(name)
        // })
        // this.header.ipSearchBox("please enter").type(roleName)
        cy.setText(this.header.ipSearchBox("please enter"), roleName)
        this.common.btnDialogFooter("save").click({ force: true })
    }

    role(roleName) {
        return cy.xpath("//div[contains(text(),'" + roleName + "')]")
    }

    btnDeleteRole(roleName) {
        return cy.xpath("//div[contains(text(),'" + roleName + "')]//button")
    }

    deleteRole(roleName) {
        // if (cy.checkElement(roleName) == true) {
        this.btnDeleteRole(roleName).click({ force: true })
        this.common.btnConfirmHintPopUp("confirm").click({ force: true })
        this.common.msgSuccess().should("be.visible")
        // } else {
        //     return
        // }
    }

    chkFeature(name) {
        return cy.xpath("//span[@class='el-tree-node__label' and text()='" + name + "']//parent::div//label//input")
    }

    checkChkBox(name, active) {
        if (active == "yes") {
            this.chkFeature(name).click({ force: true })
            cy.wait(1000)
            this.header.btnAtHeader("save").first().click({ force: true })
            this.common.msgSuccess().should("be.visible")
            this.chkFeature(name).should("be.checked")
        } else {
            this.chkFeature(name).uncheck({ force: true })
            cy.wait(1000)
            this.header.btnAtHeader("save").first().click({ force: true })
            this.common.msgSuccess().should("be.visible")
            this.chkFeature(name).should("not.be.checked")
        }
    }

    addPermission(name) {
        this.addRole()
        this.chkFeature(name).click({ force: true })
        this.header.btnAtHeader("save").last().click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }
}

export default Permission;