import Login from "../../../support/pages/TMS/components/login/Login";
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan";
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule";
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder";
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder";
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage";

describe("Delivery Plan", () => {
    var data, trackNumber, customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"
    var objLogin, objLoginPRD, objHome = new Homepage(), objShippingOrder = new ShippingOrder(),
        objCreateShippingOrder = new CreateShippingOrder(),
        objDeliveryPlan = new DeliveryPlan(),
        objSchedule = new Schedule()

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it("TC: Check flow to access DeliveryPlan - Access Page screen\n " +
        "TC: Check function DeliveryPlan - Inquire", () => {
            cy.randomString(6).then((trackingNum) => {
                // objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                // objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Confirm", trackingNum, customer, product, temperature, siteA, siteC, 5);
                objShippingOrder.common.msgSuccess().should("be.visible")
                trackNumber = trackingNum
                // cy.wait(50000)
                // cy.reload()
                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
                objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
            })
        })

    it("TC: Check function DeliveryPlan - More...\n " +
        "2TCs: Check function DeliveryPlan - Set Column - Disable header\n " +
        "2TCs: Enable header\n " +
        "2TCs: Drag&drop to reoder header\n " +
        "2TCs: Reset\n " +
        "TC: Check function DeliveryPlan - Pagination", () => {
            objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            //TC: Check function DeliveryPlan - More...
            objDeliveryPlan.hideAndUnHideSearch()
            cy.wait(1000)
            objDeliveryPlan.hideAndUnHideSearch2()
            //Set column
            objDeliveryPlan.resetDefaultColumn(1)
            objDeliveryPlan.setColumn(1, "warning", "Customer Name")
            objDeliveryPlan.enableOrDisableColumn(1, "warning", "no")
            objDeliveryPlan.resetDefaultColumn(1)
            cy.wait(1000)
            objDeliveryPlan.resetDefaultColumn(2)
            objDeliveryPlan.setColumn(2, "Line Name", "Job Status")
            objDeliveryPlan.enableOrDisableColumn(2, "Line Name", "no")
            objDeliveryPlan.resetDefaultColumn(2)
            //TC: Check function DeliveryPlan - Pagination
            cy.wait(1000)
            objDeliveryPlan.goToPageNumber(1, 2)
            cy.wait(1000)
            objDeliveryPlan.goToPageNumber(2, 2)
            cy.wait(1000)
            objDeliveryPlan.selectPageSize("100/page", 1)
            cy.wait(1000)
            objDeliveryPlan.selectPageSize("50/page", 2)
        })

    it("TC: Check function DeliveryPlan - Create A Dispatch Order", () => {
        objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
        objDeliveryPlan.createADispatchOrder(trackNumber)
        // cy.wait(50000)
        // cy.reload()
        // objDeliveryPlan = objHome.leftMenu.navigateToSubPage("plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackNumber, "second")
        objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
        cy.wait(1000)
        objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
    })

    it("TC: Check function DeliveryPlan - Delete", () => {
        objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackNumber, "second")
        objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
        cy.wait(1000)
        objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
        objDeliveryPlan.deleteDispatch()
        // cy.wait(50000)
        // cy.reload()
        // objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
        objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
    })

    it("TC: Check function DeliveryPlan - Withdraw", () => {
        objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
        objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
        objDeliveryPlan.withdrawOrder(trackNumber)
        objDeliveryPlan.common.msgSuccess().should("be.visible")
        cy.reload()
        objDeliveryPlan.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        //search again
        // objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
        // objDeliveryPlan.header.searchResult(trackNumber).should("not.exist")
    })

    it("TC: Check function DeliveryPlan - Confirm", () => {
        cy.randomString(7).then((trackingNum2) => {
            // objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            // objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Confirm", trackingNum2, customer, product, temperature, "AutoTestSite99", "SiteTestAuto", 3);
            objShippingOrder.common.msgSuccess().should("be.visible")
            // cy.wait(50000)
            // cy.reload()
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum2, "first")
            objDeliveryPlan.createADispatchOrder(trackingNum2)
            // cy.wait(50000)
            cy.reload()
            // objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackingNum2, "second")
            objDeliveryPlan.confirmDispatch()
            objDeliveryPlan.common.msgSuccess().should("be.visible")
            // cy.wait(50000)
            // cy.reload()
            objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("Dispatch Number", trackingNum2)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.header.searchResult(trackingNum2).should("be.visible")
        })
    })

    it("TC: Check function DeliveryPlan - Add to", () => {
        cy.randomString(8).then((dispatchID) => {
            //create dispatch
            // objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            // objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Confirm", dispatchID, customer, product, temperature, siteB, siteD, 4);
            objShippingOrder.common.msgSuccess().should("be.visible")
            // trackNumber3 = orderID
            // cy.wait(50000)
            // cy.reload()
            objShippingOrder.leftMenu.closeTab()
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", dispatchID, "first")
            objDeliveryPlan.createADispatchOrder(dispatchID)
            //create order
            cy.reload()
            cy.randomString(7).then((orderID) => {
                // objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                // objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()

                objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Confirm", orderID, customer, product, temperature, siteA, siteC, 10);
                objShippingOrder.common.msgSuccess().should("be.visible")
                // trackNumber = trackingNum
                // cy.wait(50000)
                // cy.reload()
                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", orderID, "first")
                objDeliveryPlan.header.searchResult(orderID).should("be.visible")
                //Add to
                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", dispatchID, "second")
                cy.wait(2000)
                objDeliveryPlan.addTo(orderID)
            })
        })
    })

    // it.only("test", () => {
    //     objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
    //     cy.randomString(21).then((track) => {
    //         objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
    //         objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithCargoDetailForQuantitySplit("save", track, customer, product, temperature, "TestT3", "TestT4", 20, 3, 18, 4)
    //         objDeliveryPlan.common.msgSuccess().should("be.visible")
    //         cy.wait(50000)
    //         objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
    //         // objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", track, "first")
    //         objDeliveryPlan.quantitySplit("waybill number", track, "first")
    //         cy.reload()

    //     })
    //     // trackNumber = "D002300003835T1"
    //     // objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
    //     // // objDeliveryPlan.quantitySplit("waybill number", trackNumber, "first")
    //     // // cy.reload()
    //     // trackNumber = "D002300002184T1"
    //     // objDeliveryPlan.routeSplit("waybill number", trackNumber, "first", "TestT5")
    // })

    // after(() => {
    //     objLogout = new Logout()
    //     objLogout.logout()
    // })
})