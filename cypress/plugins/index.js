/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
// module.exports = (on, config) => {
//   // `on` is used to hook into various events Cypress emits
//   // `config` is the resolved Cypress config
// }

import './commands'
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from failing the test
  return false
})

const { isFileExist, findFiles } = require('cy-verify-downloads');
module.exports = (on, config) => {
  on('task', { isFileExist, findFiles })
}

const { isFileExist, findFiles } = require('cy-verify-downloads');

module.exports = (on, config) => {
  on('task', { isFileExist, findFiles })
}

module.exports = (on, config) => {
  require('cypress-mochawesome-reporter/plugin')(on);
};

on('before:browser:launch', (browser = {}, launchOptions) => {
  console.log(
    'launching browser %s is headless? %s',
    browser.name,
    browser.isHeadless,
  )

  // the browser width and height we want to get
  // our screenshots and videos will be of that resolution
  const width = 1920
  const height = 1080

  console.log('setting the browser window size to %d x %d', width, height)

  if (browser.name === 'chrome' && browser.isHeadless) {
    launchOptions.args.push(`--window-size=${width},${height}`)

    // force screen to be non-retina and just use our given resolution
    launchOptions.args.push('--force-device-scale-factor=1')
  }

  if (browser.name === 'electron' && browser.isHeadless) {
    // might not work on CI for some reason
    launchOptions.preferences.width = width
    launchOptions.preferences.height = height
  }

  if (browser.name === 'firefox' && browser.isHeadless) {
    launchOptions.args.push(`--width=${width}`)
    launchOptions.args.push(`--height=${height}`)
  }

  // v4
  // module.exports = (on, config) => {
  //   on('before:browser:launch', (browser = {}, launchOptions) => {
  //     if (browser.family === 'chrome') {
  //       console.log('Adding --disable-dev-shm-usage...')
  //       launchOptions.args.push('--disable-dev-shm-usage')
  //     }

  //     return launchOptions
  //   })
  // }

  module.exports = (on, config) => {
    on('before:browser:launch', (browser = {}, launchOptions) => {
      if (browser.name === 'chrome') {
        launchOptions.args.push('--disable-web-security');
        launchOptions.args.push('--disable-site-isolation-trials');
        launchOptions.args.push('--max_old_space_size=19000');
        launchOptions.args.push('--disable-dev-shm-usage');
        launchOptions.args.push('--js-flags=--expose-gc');

        /** the rest of your plugins... **/
        require('cypress-log-to-output').install(on, (type, event) => {
          // return true or false from this plugin to control if the event is logged
          // `type` is either `console` or `browser`
          // if `type` is `browser`, `event` is an object of the type `LogEntry`:
          //  https://chromedevtools.github.io/devtools-protocol/tot/Log#type-LogEntry
          // if `type` is `console`, `event` is an object of the type passed to `Runtime.consoleAPICalled`:
          //  https://chromedevtools.github.io/devtools-protocol/tot/Runtime#event-consoleAPICalled

          // for example, to only show error events:

          if (event.level === 'error' || event.type === 'error') {
            return true
          }

          return false
        })

        return launchOptions;
      }
    });
  }

  // IMPORTANT: return the updated browser launch options
  return launchOptions
})