import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import DeliveryPlan from "../plan/DeliveryPlan"
import Schedule from "../schedule/Schedule"
import CreateShippingOrder from "../shippingorder/CreateShippingOrder"

class ShippingOrder {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.createShippingOrder = new CreateShippingOrder()
    }

    navigateToCreateShippingOrderPage() {
        this.header.btnAtHeader("create").should("be.visible").click({ force: true })
        cy.wait(3000 / 2)
        return new CreateShippingOrder()
    }

    ipSearchField(field) {
        return cy.xpath("//div[@class='mt5']//input[@placeholder='" + field + "']")
    }

    //Search
    searchByInputCondition(searchField, condition, success) {
        this.ipSearchField(searchField).focus().type(condition)
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(2000)
        if (success == "yes") {
            this.header.searchResult(condition).should("be.visible")
        } else if (success == "no") {
            this.header.searchResult(condition).should("not.exist")
        }
    }

    //Import
    ipImportDetailShippingOrder() {
        return cy.xpath("(//input[@type='file'])[1]")
    }

    ipImportWithoutDetailShippingOrder() {
        return cy.xpath("(//input[@type='file'])[last()]")
    }

    lblImportSuccess() {
        return cy.xpath("//div[contains(text(),'successfully imported')]")
    }

    btnCloseImportInfoIndication() {
        return cy.xpath("//div[@aria-label='import info indication']//button[@aria-label='Close']")
    }

    clickCloseImportInfoIndication() {
        this.btnCloseImportInfoIndication().click({ force: true })
    }

    ipTransOrder() {
        return cy.xpath("(//input[@class='el-input__inner'])[4]")
    }

    cboOrderStatus() {
        return cy.xpath("//input[@placeholder='Status']")
    }

    lblOrderStatus(status) {
        return cy.xpath("//span[normalize-space()='" + status + "']")
    }

    importDetailShippingOrderSuccess(filePath) {
        this.header.btnAtHeader("import").click({ force: true })
        cy.wait(1000)
        this.ipImportDetailShippingOrder().attachFile(filePath)
        cy.wait(2000)
        this.lblImportSuccess().should("be.visible")
        this.clickCloseImportInfoIndication()
    }

    importWithoutDetailShippingOrder(filePath) {
        this.header.btnAtHeader("import").click({ force: true })
        cy.wait(1000)
        this.ipImportWithoutDetailShippingOrder().attachFile(filePath)
        cy.wait(2000)
        this.lblImportSuccess().should("be.visible")
        this.clickCloseImportInfoIndication()
    }

    //Revise
    chkSelectShippingOrder() {
        return cy.xpath("//tr[@class='el-table__row']//input[@type='checkbox']")
    }

    ipReceivingTimeLatest() {
        return cy.xpath("(//input[@placeholder='date'])[last()]")
    }

    ipSelectDate() {
        return cy.xpath("(//div[@class='el-input el-input--small']//input[@placeholder='Select date'])[last()]")
    }

    btnOK() {
        return cy.xpath("(//span[contains(text(),'OK')])[last()]")
    }

    editLatestReceivingTime(days) {
        this.ipReceivingTimeLatest().click({ force: true })
        cy.wait(1000)
        var currentDate = new Date();
        cy.addDays(currentDate, days).then((futureDate) => {
            this.ipSelectDate().type(futureDate)
            this.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.btnOK().click({ force: true })
    }

    reviseShippingOrder(confirm) {
        this.chkSelectShippingOrder().check({ force: true })
        this.header.btnAtHeader("revise").click({ force: true })
        cy.wait(2000)
        this.editLatestReceivingTime(20)
        if (confirm == "revise") {
            this.common.btnDialogFooter(confirm).click({ force: true })
            this.common.msgSuccess().should("be.visible")
        } else if (confirm == "cancel") {
            this.common.btnDialogFooter(confirm).click({ force: true })
            this.common.msgSuccess().should("not.exist")
        }
    }

    //Confirm
    confirmShippingOrder() {
        this.chkSelectShippingOrder().check({ force: true })
        this.header.btnAtHeader("confirm").click({ force: true })
        // cy.wait(2000)
        this.common.msgSuccess().should("be.visible")
    }

    //Withdraw
    withdrawShippingOrder(action) {
        this.chkSelectShippingOrder().check({ force: true })
        cy.wait(2000)
        this.header.btnAtHeader("withdraw").trigger("click")
        cy.wait(1000)
        if (action == "confirm") {
            this.common.btnConfirmHintPopUp("confirm").click({ force: true })
            this.common.msgSuccess().should("be.visible")
        } else {
            cy.wait(2000)
            this.common.btnConfirmHintPopUp("cancel").click({ force: true })
        }
    }

    //delete
    deleteShippingOrder(action) {
        this.chkSelectShippingOrder().check({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("delete").click({ force: true })
        cy.wait(1000)
        if (action == "confirm") {
            this.common.btnConfirmHintPopUp("confirm").click({ force: true })
            this.common.msgSuccess().should("be.visible")
        } else {
            this.common.btnConfirmHintPopUp("cancel").click({ force: true })
        }
    }

    //direct schedule
    directScheduleShippingOrder() {
        this.chkSelectShippingOrder().check({ force: true })
        this.header.btnAtHeader("Direct Schedule").click({ force: true })
    }

    //collapse filter
    hideSearch() {
        return cy.xpath("//div[@data-old-padding-top and @style='display: none;']")
    }

    unHideSearch() {
        return cy.xpath("//div[@class='common-body-tms']//div[@class='mt5']")
    }

    hideAndUnHideSearchShippingOrder() {
        this.unHideSearch().should("be.visible")
        cy.wait(1000)
        this.header.btnAtHeader("collapse filter").click({ force: true })
        this.hideSearch().should("not.exist")
        this.header.btnAtHeader("more filter...").click({ force: true })
    }

    copyANewOrder(button, trackNumber, days) {
        this.common.openDetailView()
        cy.wait(2000)
        this.header.btnAtHeader("copy a new order").click({ force: true })
        cy.wait(1000)
        this.createShippingOrder.ipUpStreamTrackingNumber().clear().type(trackNumber)
        this.createShippingOrder.ipField("Please enter external tracking number").clear().type(trackNumber)
        // this.createShippingOrder.ipField("shipment number").clear().type(trackNumber)
        cy.wait(1000)
        this.createShippingOrder.chooseTargetPickUpTime()
        this.createShippingOrder.chooseTargetDeliveryTime(days)
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            cy.wait(2000)
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new DeliveryPlan()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            cy.wait(2000)
            return new Schedule()
        }
    }

    copyANewOrderInTrack(button, trackNumber, days) {
        this.createShippingOrder.ipUpStreamTrackingNumber().clear().type(trackNumber)
        this.createShippingOrder.ipField("Please enter external tracking number").clear().type(trackNumber)
        // this.createShippingOrder.ipField("shipment number").clear().type(trackNumber)
        cy.wait(1000)
        this.createShippingOrder.chooseTargetPickUpTime()
        this.createShippingOrder.chooseTargetDeliveryTime(days)
        cy.wait(1000)
        if (button === "Save") {
            this.header.btnAtHeader("Save").click({ multiple: true, force: true })
            cy.wait(2000)
            return new ShippingOrder()
        } else if (button === "Confirm") {
            this.header.btnAtHeader("Confirm").click({ multiple: true, force: true })
            this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
            return new DeliveryPlan()
        } else if (button === "Direct schedule") {
            this.header.btnAtHeader("Direct schedule").click({ multiple: true, force: true })
            this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
            cy.wait(2000)
            return new Schedule()
        }
    }

    checkNumberTrackingExist(numberTracking) {
        this.common.selectPageSize("500/page")
        this.common.columnUpstreamTracking().each(($el) => {
            if ($el.text().trim() === numberTracking) {
                expect($el).to.contain(numberTracking)
            }
        })
        this.common.columnShipmentNumber().each(($el) => {
            if ($el.text().trim() === numberTracking) {
                expect($el).to.contain(numberTracking)
            }
        })
    }

    verifyOrderHasJustCreate(trackingNum) {
        this.header.btnAtHeader("more filter...").click({ force: true })
        this.cboOrderStatus().click()
        this.lblOrderStatus("order_confirmed").click()
        // this.ipTransOrder().click({force : true})
        // this.ipTransOrder().realType(trackingNum)
        this.ipSearchField("External Tracking Number").focus().realType(trackingNum)
        cy.wait(1000)
        // this.ipTransOrder().type('{enter}')
        this.header.btnAtHeader("inquire").click({ force: true })
        this.common.columnStatus().should('have.text', 'order_confirmed')
    }

}

export default ShippingOrder;