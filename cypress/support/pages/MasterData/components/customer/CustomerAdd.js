import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import CustomerList from "./CustomerList";
class CustomerAdd {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
    }


    txtTitle(title) { return cy.xpath("//p[contains(.,'" + title + "')]") }
    txtSubTitle(title) { return cy.xpath("//div[normalize-space()='" + title + "']") }
    txaInformation(placeholder) { return cy.xpath("//textarea[@placeholder='" + placeholder + "']") }
    ddlInformation(placeholder) { return cy.xpath("//input[@placeholder='" + placeholder + "']") }
    btnImport() { return cy.xpath("//button[contains(@class,'el-button button-st font-normal el-button--primary el-button--mini')]") }
    btnAddKeyContact() { return cy.xpath("//button[@class='el-button base-icon-button el-button--primary el-button--mini']") }
    btnCorfirmLocation() { return cy.xpath("//div[contains(text(),'Confirm Location')]") }
    spanDropdownValue(value) { return cy.xpath("(//span[text()='" + value + "'])[last()]") }
    liDropdownValue(value) { return cy.xpath("(//li[text()='" + value + "'])[last()]") }
    imgGoogleMap() { return cy.xpath("//div[@class='vue-map-container']") }
    msgError(error) { return cy.xpath("//div[@class='text-error-5 text-sm error-message has-error' and contains(., '" + error + "')]") }
    notMaxLength(placeholder) { return cy.xpath("//input[@placeholder='" + placeholder + "']//parent::div[contains(@class,'border-danger')]") }
    btnImport() { return cy.xpath("(//button[contains(., 'Import')])[1]") }

    isAllInformationDisplayed() {
        this.txtTitle("Create customer").should("be.visible")
        this.txtSubTitle("Information").should("be.visible")
        this.txtSubTitle("Status").should("be.visible")
        this.ddlInformation("Enter Company Tax ID").should("be.visible")
        // this.inputTextbox("Enter Company Tax ID", "1234567892341")
        this.txaInformation("Enter Company Registered Name").should("be.visible")
        // this.inputTextArea("Enter Company Registered Name", "My company")
        this.txaInformation("Enter Thai Company Name").should("be.visible")
        // this.inputTextArea("Enter Thai Company Name", "Thai name")
        this.txaInformation("Enter English Company Name").should("be.visible")
        // this.inputTextArea("Enter English Company Name", "English name")
        this.ddlInformation("Select Customer type").should("be.visible")
        // this.selectDropdownlist("Select Customer type", "Individual")
        this.ddlInformation("Select Payment type").should("be.visible")
        // this.selectDropdownlist("Select Payment type", "Cash")
        this.ddlInformation("Select Customer Grouping L1").should("be.visible")
        // this.selectDropdownlist("Select Customer Grouping L1", "CPG")
        this.ddlInformation("Select Customer Grouping L2").should("be.visible")
        // this.selectDropdownlist("Select Customer Grouping L2", "CP ALL")
        this.ddlInformation("Select Credit Term").should("be.visible")
        // this.selectDropdownlist("Select Credit Term", "3 days")
        this.ddlInformation("Enter Phone no.").should("be.visible")
        // this.inputTextbox("Enter Phone no.", "12345654")
        this.txtSubTitle("Rate Card").should("be.visible")
        // this.ddlInformation("Search by Contract Code, Service Type, Rate Card Type, Vehicle Type. ").should("be.visible")

        this.txtSubTitle("Bank Information").scrollIntoView()
        this.txtSubTitle("Bank Information").should("be.visible")
        this.ddlInformation("Select Bank Name").should("be.visible")
        // this.selectDefaultDropdownlist("Select Bank Name", "Bangkok Bank")
        this.ddlInformation("Enter Account Number").should("be.visible")
        // this.inputTextbox("Enter Account Number", "122333322123")
        this.txaInformation("Enter Bank Account Name").should("be.visible")
        // this.inputTextArea("Enter Bank Account Name", "Daisy")



        this.txtSubTitle("Key Contact").scrollIntoView()
        this.btnAddKeyContact().should("be.visible")
        this.txtSubTitle("Key Contact").should("be.visible")
        this.ddlInformation("Select Title").should("be.visible")
        // this.selectDropdownlist("Select Title", "Mrs")
        this.ddlInformation("Enter Key Contact Person’s first name").should("be.visible")
        // this.inputTextbox("Enter Key Contact Person’s first name", "Huong")
        this.ddlInformation("Enter Key Contact Person’s last name").should("be.visible")
        // this.inputTextbox("Enter Key Contact Person’s last name", "Nguyen")
        this.ddlInformation("Enter Key Contact Person’s email").should("be.visible")
        // this.inputTextbox("Enter Key Contact Person’s email", "huevuthi1409@gmail.com")
        this.ddlInformation("Enter Mobile Phone no.").should("be.visible")
        // this.inputTextbox("Enter Mobile Phone no.", "12345654")
        this.ddlInformation("Enter Landline phone no.").should("be.visible")
        this.ddlInformation("Extension").should("be.visible")

        this.txtSubTitle("Headquarter Location").scrollIntoView()
        this.txtSubTitle("Headquarter Location").should("be.visible")
        this.txaInformation("Enter Address").should("be.visible")
        // this.inputTextArea("Enter Address", "Vietname")
        this.ddlInformation("Select Province").should("be.visible")
        // this.selectDefaultDropdownlist("Select Province", "Bangkok")
        this.ddlInformation("Select District").should("be.visible")
        // this.selectDropdownlist("Select District", "Duist")
        this.ddlInformation("Select Sub-District").should("be.visible")
        // this.selectDropdownlist("Select Sub-District", "Duist")
        this.ddlInformation("Select Postal Code").should("be.visible")
        // this.selectDropdownlist("Select Postal Code", "10300")
        this.btnCorfirmLocation().should("be.visible")

        this.imgGoogleMap().scrollIntoView()
        this.imgGoogleMap().should("be.visible")

    }

    inputTextbox(placeholder, value) {
        this.ddlInformation(placeholder).first().scrollIntoView()
        this.ddlInformation(placeholder).should("be.visible")
        this.ddlInformation(placeholder).first().clear({ force: true })
        this.ddlInformation(placeholder).first().type(value)
    }

    inputTextArea(placeholder, value) {
        this.txaInformation(placeholder).first().scrollIntoView()
        this.txaInformation(placeholder).should("be.visible")
        this.txaInformation(placeholder).first().clear({ force: true })
        this.txaInformation(placeholder).first().type(value)
    }

    selectDropdownlist(placeholder, value) {
        this.ddlInformation(placeholder).first().scrollIntoView()
        this.ddlInformation(placeholder).should("be.visible")
        this.ddlInformation(placeholder).first().click({ force: true })
        this.spanDropdownValue(value).first().click({ force: true })
    }

    selectDefaultDropdownlist(placeholder, value) {
        this.ddlInformation(placeholder).first().scrollIntoView()
        this.ddlInformation(placeholder).should("be.visible")
        this.ddlInformation(placeholder).first().click({ force: true })
        this.liDropdownValue(value).first().click({ force: true })
    }

    isErrorMessageDisplayed(error) {
        this.msgError(error).should("be.visible")
    }

    getInputAttribute(placeholder, attribute, value) {
        this.ddlInformation(placeholder).scrollIntoView()
        this.ddlInformation(placeholder).should("be.visible")
        this.ddlInformation(placeholder).invoke("attr", attribute).should("contain", value)
    }

    spanAllDropdownValue() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-select-dropdown__list'])[last()]//li//span")
    }

    verifyListData(value) {
        this.spanAllDropdownValue().each(($li) => {
            const expectedResult = []
            expectedResult.push($li.text())
            cy.log(expectedResult)
        }).then(() => {
            for (let index = 0; index < expectedResult.length; index++) {
                const element = expectedResult[index];
                if (element.includes(value)) {
                    return
                } else {
                    continue
                }
            }
            // expect(expectedResult).to.include.members(actualResult)
        })
    }

    btnConfirm(action) {
        return cy.xpath("//button//span[contains(text(), '" + action + "')]//parent::button")
    }

    //UI Rate Card
    verifyUIRateCardAfterInputThaiName() {
        this.common.divNoData().should("be.visible")
        this.btnImport().should("not.be.disabled")
    }

    verifyUIRateCardBeforeInputThaiName() {
        this.common.divNoData().scrollIntoView().should("be.visible")
        this.btnImport().scrollIntoView().should("be.disabled")
    }

    //Import rate card
    rdoActive() {
        return cy.xpath("//input[@class='el-switch__input']")
    }

    importFileSuccess(filePath) {
        this.btnImport().scrollIntoView().click({ force: true })
        this.common.attachImportFile(filePath)
        this.common.btnImportPopupAttachFileEnable().click({ force: true })
        this.rdoActive().click({ multiple: true, force: true })
        this.common.btnImportPopupAttachFileEnable().click({ force: true })
        this.common.msgImportSuccess().should("be.visible")
        this.common.btnImportPopupAttachFileEnable().click({ force: true })
    }

    ddlMobilePhone(field) {
        return cy.xpath("//div[contains(text(),'" + field + "')]//parent::div//div[@class='el-input el-input--base el-input--suffix']//input[@placeholder='Select']")
    }

    selectMobilePhonePrefix(field, value) {
        this.ddlMobilePhone(field).click({ force: true })
        this.common.spanValue(value).last().click({ force: true })
    }

    icRemoveKeyContact() {
        return cy.xpath("//div[@class='cursor-pointer text-xl']")
    }

    removeKeyContact() {
        this.icRemoveKeyContact().last().click({ force: true })
    }

    verifyClickAddKeyContact() {
        this.common.btnAction("Add Key Contact").scrollIntoView().click({ force: true })
        this.icRemoveKeyContact().should("be.visible")
    }

    btnAddKeyContactDisable() {
        return cy.xpath("//button//div[contains(text(), 'Add Key Contact')]//ancestor::button")
    }

    liSearchSuggestion() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-autocomplete-suggestion__list']//li)[last()]")
    }

    verifySearchSuggestion() {
        // // this.liSearchSuggestion().contains(text)
        // cy.wait(2000)
        this.liSearchSuggestion().click({ force: true })
    }

    btnConfirmLocationDisable() {
        return cy.xpath("//button//div[contains(text(), 'Confirm Location')]//ancestor::button")
    }

    //count text
    divLicensePlateLength() {
        return cy.xpath("//div[@class='text-sm text-secondary-45']")
    }

    countTextInputted() {
        this.divLicensePlateLength().last().should(($div) => {
            var text = $div.text()
            var formatText = text.trim().split('/')

            expect(formatText[0]).not.to.equal('0')
        })
    }

    createCustomer(compRegisterd, thaiComp, landline, extension, action, confirm, click, verify) {
        cy.randomNumber(13).then((tax) => {
            this.inputTextbox("Enter Company Tax ID", tax)
        })

        if (compRegisterd == "generate") {
            cy.randomString(6).then((registered) => {
                this.inputTextArea("Enter Company Registered Name", registered)
            })
        } else {
            this.inputTextArea("Enter Company Registered Name", compRegisterd)
        }

        if (thaiComp == "generate") {
            cy.randomString(6).then((random) => {
                this.inputTextArea("Enter Thai Company Name", random)
            })
        } else {
            this.inputTextArea("Enter Thai Company Name", thaiComp)
        }
        cy.randomString(6).then((EngComp) => {
            this.inputTextArea("Enter English Company Name", EngComp)
        })

        this.ddlInformation("Select Customer type").click({ force: true })
        this.common.selectRandomValue()
        this.ddlInformation("Select Payment type").click({ force: true })
        this.common.selectRandomValue()
        this.ddlInformation("Select Customer Grouping L1").click({ force: true })
        this.common.selectRandomValue()
        this.ddlInformation("Select Customer Grouping L2").click({ force: true })
        // cy.wait(1000)
        this.common.selectRandomValue()
        this.ddlInformation("Select Credit Term").click({ force: true })
        // cy.wait(1000)
        this.common.selectRandomValue()

        // cy.randomNumber(8).then((phone) => {
        //     this.ddlInformation("Enter Phone no.", phone)
        // })
        cy.randomString(8).then((customer) => {
            this.inputTextArea("Enter Customer SAP Code", customer)
        })

        this.ddlInformation("Select Bank Name").scrollIntoView().click({ force: true })
        cy.wait(2000)
        this.common.selectRandomValue()
        // cy.wait(1000)
        this.inputTextArea("Enter Bank Account Name", "Bank Account Test Auto")
        cy.randomNumber(12).then((accNumber) => {
            this.inputTextbox("Enter Account Number", accNumber)
        })

        this.ddlInformation("Select Title").click({ force: true })
        this.common.selectRandomValue()
        cy.randomString(4).then((firstName) => {
            this.inputTextbox("Enter Key Contact Person’s first name", firstName)
        })
        cy.randomString(3).then((lastName) => {
            this.inputTextbox("Enter Key Contact Person’s last name", lastName)
        })
        this.inputTextbox("Enter Key Contact Person’s email", "EmailAutoTest@gmail.com")
        cy.randomNumber(9).then((mobilePhone) => {
            this.inputTextbox("Enter Mobile Phone no.", mobilePhone)
        })
        if (landline != "empty") {
            cy.randomNumber(8).then((landline) => {
                this.inputTextbox("Enter Landline phone no.", landline)
            })
        }
        if (extension != "empty") {
            cy.randomNumber(8).then((extension) => {
                this.inputTextbox("Extension", extension)
            })
        }

        this.inputTextArea("Enter Address", "Auto Address")
        this.common.ipSelectBox("Select Province").click({ force: true })
        // cy.wait(2000)
        this.common.ddlValue().should("be.visible", { timeout: 10000 })
        var searchKeyProvince = "กรุงเทพมหานคร"
        this.common.ipSelectBox("Select Province").type(searchKeyProvince)
        cy.wait(500)
        this.common.verifySearchSuggestionAndSelect(searchKeyProvince)
        cy.wait(500)
        var searchKeyDistrict = "จตุจักร"
        this.ddlInformation("Select District").click({ force: true })
        this.common.ddlValue().should("be.visible", { timeout: 10000 })
        this.common.liDropdownValue(searchKeyDistrict).scrollIntoView().click({ force: true })
        // this.common.selectRandomValue()
        cy.wait(500)
        this.ddlInformation("Select Sub-District").click({ force: true })
        this.common.selectRandomValue()
        cy.wait(500)
        this.ddlInformation("Select Postal Code").click({ force: true })
        this.common.selectRandomValue()
        cy.wait(500)
        this.btnConfirm("Confirm Location").click({ force: true })
        this.common.confirmCreateData(action, confirm, click, verify)

        return new CustomerList()
    }
}


export default CustomerAdd