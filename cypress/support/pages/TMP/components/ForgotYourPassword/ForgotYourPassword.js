import LoginStaff from "../login/LoginStaff"

class ForgotYourPassword {

    btnContinue(){
        return cy.xpath('//button[.="CONTINUE"]')
    }

    lnkBackToLogin(){
        return cy.xpath('//span[@class="text-back"]')
    }

    txtEmail(){
        return cy.xpath('//input[@placeholder="Email"]')
    }

    lblNotFoundEmail(){
        return cy.xpath('//div[@class="text-error-5 text-sm h-5 mt-px"]')
    }

    lblWrongFormatEmail(){
        return cy.xpath('//div[@class="text-error-5 text-sm h-5 mt-px"]')
    }

    lnkBackToPreviousPage(){
        return cy.xpath('//span[normalize-space()="Back to previous page"]')
    }

    lnkBackToLoginWhenChooseMethodReset(){
        return cy.xpath('//span[contains(text(),"Back to log in")]')
    }

    chkMethodResetUseEmail(){
        return cy.xpath('//input[@id="email"]')
    }

    chKMethodPhoneNo(){
        return cy.xpath('//input[@id="phone"]')
    }

    btnResendOTP(){
        return cy.xpath('//span[contains(.,"Resend OTP")]')
    }

    btnChangeMethod(){
        return cy.xpath('//a[normalize-space()="Change recovery method"]')
    }

    txtOTP(){
        return cy.xpath('//input[@placeholder="Enter OTP code"]')
    }

    lblInvalidOTPCode(){
        return cy.xpath("//div[@class='text-error-5 text-sm mt-px']")
    }

    btnVerify(){
        return cy.xpath('//button[.="VERIFY"]')
    }

    txtPassword(){
        return cy.xpath('//input[@name="password"]')
    }

    txtConfirmPassword(){
        return cy.xpath('//input[@name="password_confirmation"]')
    }

    lblPasswordNotMatch(){
        return cy.xpath('//div[normalize-space()="Password do not match"]')
    }

    btnSetPassword(){
        return cy.xpath('//button[.="SET PASSWORD"]')
    }

    btnShowPassword(){
        return cy.xpath('(//img[@class="cursor-pointer"])[1]')
    }

    btnShowConfirmPassword(){
        return cy.xpath('(//img[@class="cursor-pointer"])[2]')
    }

    btnContToAllnowTMS(){
        return cy.xpath('//button[.="CONTINUE TO ALL NOW TMS"]')
    }

    observeButtonContinue(){
        this.btnContinue().should('be.disabled')
    }

    clickLinkForBackToLogin(){
        this.lnkBackToLogin().click()
        return new LoginStaff()
    }

    inputEmail(email){
        this.txtEmail().type(email)
    }

    clickButtonContinue(){
        this.btnContinue().click()
    }

    clickLinkForBackToPreviousPage(){
        this.lnkBackToPreviousPage().click()
    }
    
    clickLinkForBackToLoginWhenChooseMethodReset(){
        this.lnkBackToLoginWhenChooseMethodReset().click()
        return new LoginStaff()
    }

    showMessageNotFoundEmail(){
        this.lblNotFoundEmail().should('contain', 'We do not found your email in our system')
    }

    showMessageWrongFormatEmail(){
        this.lblWrongFormatEmail().should('contain', 'Invalid email format')
    }

    chooseMethodEmailForForgorYourPassword(){
        this.chkMethodResetUseEmail().click()
    }

    chooseMethodPhoneNoForForgorYourPassword(){
        this.chKMethodPhoneNo().click()
    }

    observeButtonResendOTP(){
        this.btnResendOTP().should('be.visible')
    }

    observeButtonChangeRecoveryMethod(){
        this.btnChangeMethod().should('be.visible')
    }

    inputOTP(OTP){
        this.txtOTP().type(OTP)
    }

    showMessageInvalidOTPCode(){
        this.lblInvalidOTPCode().should('contain', 'Invalid OTP')
    }

    clickButtonVerify(){
        this.btnVerify().click()
    }

    clickButtonResendOTP(){
        this.btnResendOTP().click()
    }

    clickButtonChangeRecoveryMethod(){
        this.btnChangeMethod().click()
    }

    inputPassword(password){
        this.txtPassword().type(password)
    }

    inputConfirmPassword(confirmPassword){
        this.txtConfirmPassword().type(confirmPassword)
    }

    clickButtonShowPassword(){
        this.btnShowPassword().click()
    }

    clickButtonShowConfirmPassword(){
        this.btnShowConfirmPassword().click()
    }

    clickButtonSetPassword(){
        this.btnSetPassword().click()
    }

    clickButtonContToAllnowTMS(){
        this.btnContToAllnowTMS().click()
    }

    observeButtonSetPasswordEnabled(){
        this.btnSetPassword().should('be.enabled')
    }

    observeButtonSetPasswordDisabled(){
        this.btnSetPassword().should('be.disabled')
    }

    showMessagePasswordNotMatch(){
        this.lblPasswordNotMatch().should('be.visible')
    }

    byPassSendOTP(){
        cy.randomNumber(6).then((Otp) => {
            var OTPCode = Otp
            console.log(OTPCode)
        cy.intercept('POST', '**/sendOtp', {code: OTPCode , message: 'send OTP success', items: true}).as('sendOtp')
        this.clickButtonContinue()
        cy.wait('@sendOtp').then((res) => {
            cy.log(JSON.stringify(res))
            console.log(JSON.stringify(res))
        })
        })       
    }

    byPassVerifyOTP(){
        cy.randomNumber(6).then((Otp) => {
            var OTPCode = Otp
            console.log(OTPCode)
        cy.intercept('POST','**/verificationOtp', {code: OTPCode , message: 'verify OTP success', items: true}).as('verificationOtp')
        this.clickButtonVerify()
        cy.wait('@verificationOtp').then((res) => {
            cy.log(JSON.stringify(res))
            console.log(JSON.stringify(res))
        })
        })
    }

    byPassModifyPassword(password, confirmPassword){
        cy.intercept('POST','**/modifyPassword', {fixture: 'forgot_password_api.json'}).as('modifyPassword')
        this.inputPassword(password)
        this.inputConfirmPassword(confirmPassword)
        this.clickButtonSetPassword()
        cy.wait('@modifyPassword').then((res) => {
            cy.log(JSON.stringify(res))
            console.log(JSON.stringify(res))
        })
    }

    byPassLogin(){
        cy.intercept('POST','**/login',{fixture : 'forgot_password_api.json'}).as('goLogin')
        this.clickButtonContToAllnowTMS()
        cy.wait('@goLogin').then((res) => {
            cy.log(JSON.stringify(res))
            console.log(JSON.stringify(res))
        })
    }
}

export default ForgotYourPassword;