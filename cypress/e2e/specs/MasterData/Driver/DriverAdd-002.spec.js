import  LoginSupplier from "../../../../support/pages/TMS/components/login/LoginSupplier";

describe("Master data - Driver - Add", () => {

    var data, objLogin, objHome, objVehicle, objDriverAdd

    beforeEach("TMS-Our-Itemtype", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            // // const url = new Utility().getBaseUrl()
            cy.visit("https://tms-stg.allnowgroup.com/")
            // if (url == "http://tmsui.allnow.tms.staging.internal/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objDriverAdd = objHome.leftMenu.navigateToSubPage("Master Data", "Driver")
        //     }   else if (url == "https://tms.allnowgroup.com") {
        //         cy.fixture("data_test.json").then((loginData) => {
        //             data = loginData
        //             cy.visit(data.url)
        //             objLogin = new Login()
        //             objHome = objLogin.login(data.username, data.password)
        //         })
        //     }
        })
    })

    it("Check when input 13 digits National ID Card Number textbox", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtNationalIDCardNumber().type("123")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 12 digits National ID Card Number textbox", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtNationalIDCardNumber().type("12")
        
        objDriverAdd.lblStatusMessageFields("Invalid The National ID Card Number").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 14 digits National ID Card Number textbox", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtNationalIDCardNumber().type("1234")
        
        objDriverAdd.verifyWhenNationalIDNumber("1234")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is text in National ID Card Number textbox ", () => {
          
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "ABCDEF"
        objDriverAdd.txtNationalIDCardNumber().type(phoneN0)
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.txtNationalIDCardNumber().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input data is special characters in National ID Card Number textbox ", () => {
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "@!%!%!@"
        objDriverAdd.txtNationalIDCardNumber().type(phoneN0)
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.txtNationalIDCardNumber().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check input phone number already existed into National ID Card Number textbox", () => {
        
        
        var filePath = "implement/images.png"
        const date = new Date()
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.createNewDriverV1(filePath, 'นาง', 'huyen', 'vmotest', 'หญิง', date.getDate(), 'atk', 'def', '123456777', '12345688', '12345678', '9058244122126', '87654321', 'บ.2', 'สาธารณะ')
        
        objDriverAdd.btnConfirm().click()
        objDriverAdd.lblVehiclesPage().click()
        objDriverAdd.lblInformationPage().click()
        cy.wait(10000)
        objDriverAdd.dtpDateOfBirth('Expired Date').first().scrollIntoView()
        
        objDriverAdd.lblStatusMessageFields("National ID Card Number is already existed").should('be.visible')
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the past for Expired Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        const date = new Date()
        const year = date.getFullYear() - 100
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
            if((parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1))) > 1){
                for(var  i = 0; i <= decade; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            else{
                for(var  i = 0; i < decade; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            objDriverAdd.lblYearDisabled(year).should('to.exist')
        })
    })

    it("Check select  date in the present for Expired Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        const date = new Date()
        objDriverAdd.lblToDay(date.getDate()).first().click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the future when date < 100 years from the current date  for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        const date = new Date()
        const year = 2023
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(year.substr(3,1) - $text.text().trim().substr(3,1) > 1){
                    for(var  i = 0; i <= decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time in the future when date = 100 years from the current date for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        const date = new Date()
        const year = date.getFullYear() + 100
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1)) > 1){
                    for(var  i = 0; i <= decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        const day = date.getDate()

        objDriverAdd.lblDay(day).first().click({force : true})
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time in the future when date >100 years from the current date for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        const date = new Date()
        const year = date.getFullYear() + 101
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1)) > 1){
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade - 1; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYearDisabled(year).should('not.to.selected')

        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when leave blank Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").first().click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when upload image <= 3MB  for Driver License Photo field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").click()
        
        var filePath = "implement/images.png"
        objDriverAdd.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        
        objDriverAdd.btnClickToUpload("Driver License Photo").should('be.disabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display files when upload file successfully", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").click()
        
        var filePath = "implement/images.png"
        objDriverAdd.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("Driver License Photo").should('have.text', 'images.png')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when click on uploaded image successfully into 'Driver License Photo ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").click()
        
        var filePath = "implement/images.png"
        objDriverAdd.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("Driver License Photo").should('to.exist')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check 'delete' icon when the image is uploaded", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").click()
        
        var filePath = "implement/images.png"
        objDriverAdd.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        
        objDriverAdd.btnActionFileUpload("Driver License Photo").last().click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").should('be.enabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check to upload the correct image format but exceed 3 MB", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver License Photo").click()
        
        var filePath = "implement/Image exceed 3MB.jpg"
        objDriverAdd.ipClickToUpload("Driver License Photo").last().attachFile(filePath)
        objDriverAdd.msgError().should('have.text', 'File size is exceed the allowable limit of 3 MB')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 8 number 'Driver License Number' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtDriverLicenseNumber().type("")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 7 number 'Driver License Number' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtDriverLicenseNumber().type("")
        
        objDriverAdd.lblStatusMessageFields("Invalid The Driver License Number").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 9 number 'Driver License Number", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var driverLicenseNumber = ""
        objDriverAdd.txtDriverLicenseNumber().type(driverLicenseNumber)
        
        objDriverAdd.verifySpaceWhenInputLandlineNo(driverLicenseNumber)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display of Driver License Type dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseType().click()
        
        objDriverAdd.lblDriverLicenseType("ท.1").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("ท.2").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("ท.3").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("ท.4").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("บ.1").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("บ.2").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("บ.3").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("บ.4").scrollIntoView().should('be.visible')
        objDriverAdd.lblDriverLicenseType("ส่วนบุคคล").last().scrollIntoView().should('be.visible')
    })

    it("Check select one value of Driver License Type dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseType().click()
        
        objDriverAdd.lblDriverLicenseType("บ.2").click()
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Get blank into Driver License Type field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseType().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check display of Driver License Category dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseCategory().click()
        
        objDriverAdd.lblDriverLicenseCategory("ส่วนบุคคล").should('be.visible')
        objDriverAdd.lblDriverLicenseCategory("สาธารณะ").should('be.visible')
        objDriverAdd.lblDriverLicenseCategory("สาธารณะ").should('be.visible')
        objDriverAdd.lblDriverLicenseCategory("ทุกประเภท").should('be.visible')
    })

    it("Check select one value of Driver License Category dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseCategory().click()
        
        objDriverAdd.lblDriverLicenseCategory("ส่วนบุคคล").click({multiple : true, force : true})
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Get blank into Driver License Category field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboDriverLicenseCategory().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check select date in the future for Issued Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        const date = new Date()
        const year = date.getFullYear() + 1
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            if(year > 2029){
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(year.substr(3,1) - $text.text().trim().substr(3,1) > 1){
                    for(var  i = 0; i <= decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            }
            else{
                objDriverAdd.lblYearDOB().click()
            }
            objDriverAdd.lblYearDisable(year).should('not.to.selected')
        })
    })

    it("Check select  date in the present for Issued Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        const date = new Date()
        objDriverAdd.lblToDay(date.getDate()).click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Issued Date").then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time  in the past for Issued Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        const year = "20"
        if(year < 2020){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
                if((parseInt(year.substr(3,1)) - parseInt($text.text().trim().substr(3,1))) > 1){
                    for(var  i = 0; i < decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
                else{
                    for(var  i = 0; i < decade - 1; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Issued Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, year + '/10/29')
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the past when date > 100 years from the current date  for Issued Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        const date = new Date()
        const year = date.getFullYear() - 201
        if(year < 2020){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
                if((parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1))) > 1){
                    for(var  i = 0; i < decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
                else{
                    for(var  i = 0; i < decade - 1; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
            })
        }
        else{
        }
        objDriverAdd.lblYearDisable(year).should('not.to.selected')
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
    })

    it("Check select time in the past when date = 100 years from the current date for Issued Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        const date = new Date()
        const year = date.getFullYear() - 100
        if(year < 2020){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
                if((parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1))) > 1){
                    for(var  i = 0; i <= decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
            })
        }
        else{
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Dec").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Issued Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the past when date < 100 years from the current date  for Issued Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        const date = new Date()
        const year = date.getFullYear() - 69
        if(year < 2020){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
                if((parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1))) > 1){
                    for(var  i = 0; i <= decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        objDriverAdd.btnPrevYear().click()
                    }
                }
            })
        }
        else{
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Issued Date").first().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when leave blank Issued Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Issued Date").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when leave blank Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check the activity of the Calendar box for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        objDriverAdd.lblDayNextMonth("").first().click()
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the past  for Expired Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        
        const year = "6"
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
            if((year.substr(3,1) - $text.text().trim().substr(3,1)) > 1){
                for(var  i = 0; i < decade; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            else{
                for(var  i = 0; i < decade - 1; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            objDriverAdd.lblYearDisable(year).should('to.exist')
        })
    })

    it("Check select  date in the present for Expired Date field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        
        const date = new Date()
        objDriverAdd.lblToDay(date.getDate()).first().click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the future when date < 100 years from the current date  for Expired Date field", () => {
        objDriverAdd.btnMainFunction("Create").click()      
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()      
        const year = 2023
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(year.substr(3,1) - $text.text().trim().substr(3,1) > 1){
                    for(var  i = 0; i <= decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, year + '/10/29')
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time in the future when date = 100 years from the current date for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        
        const date = new Date()
        const year = date.getFullYear() + 100
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1)) > 1){
                    for(var  i = 0; i <= decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYear(year).click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay(date.getDate()).first().click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().then(($text)=>{
            cy.checkFormatDay($text.text().trim())
            const date = new Date($text.text().trim())
            cy.checkFormatDateNormal(date, $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time in the future when date > 100 years from the current date for Expired Date field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Expired Date").last().click()
        
        const date = new Date()
        const year = date.getFullYear() + 101
        if(year > 2029){
            objDriverAdd.lblYearDOB().then(($text)=>{
                objDriverAdd.lblYearDOB().click()
                const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
                if(parseInt(year.toString().substr(3,1)) - parseInt($text.text().trim().substr(3,1)) > 1){
                    for(var  i = 0; i < decade; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    for(var  i = 0; i < decade - 1; i++){
                        if($text.text().trim() != year){
                            if(decade > 1){
                                objDriverAdd.btnNextYear().click()
                                if($text.text().trim() === year){
                                    break
                                }
                            }else{
                                if($text.text().trim() === year){
                                    break
                                }
                            }
                        }
                    }
                }
            })
        }
        else{
            objDriverAdd.lblYearDOB().click()
        }
        objDriverAdd.lblYearDisable(year).should('not.to.selected')
    })

    it("Check default value of Status field", () => { 
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        objDriverAdd.defaultCheckBoxButtonSetStatus("Active").should('be.visible')
        objDriverAdd.txtEffectiveDate().last().then(($el)=>{
            const date = new Date()
            cy.checkFormatDate(date, $el.text().trim())
        })
        objDriverAdd.defaultCheckBoxButtonSetStatus("No end date").should('be.visible')
    })

    it("Check tick on Inactive tickbox for Status", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
        
        objDriverAdd.defaultCheckBoxButtonSetStatus("No end date").should('not.to.exist')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnCancel().last().click()
        objDriverAdd.btnConfirm().click()
    })

    it("Check select  date in the past  for Effective date / time  by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().click()
        objDriverAdd.lblYearDOB().click()
        objDriverAdd.btnPrevYear().click()
        objDriverAdd.lblY().should('not.to.selected')
    })

    it("Check select  date in the future  for Effective date / time  by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().click()
        
        objDriverAdd.lblYearDOB().click()
        
        objDriverAdd.btnNextYear().click()
        
        objDriverAdd.lblYear("30").click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.lblHour("").scrollIntoView().click()
        
        objDriverAdd.lblMinute("").scrollIntoView().click()
        
        objDriverAdd.btnOKSetED().click()
        
        objDriverAdd.txtEffectiveDate().last().should('contain', '2030/10/29   13:00')
        objDriverAdd.txtEffectiveDate().last().then(($el)=>{
            const date = new Date($el.text().trim())
            cy.checkFormatDate(date, '2030/10/29   13:00')
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnCancel().last().click()
        objDriverAdd.btnConfirm().click()
    })

    it("Check select  date in the present  for Effective date / time  by calendar", () => { 
        objDriverAdd.btnMainFunction("Create").click()   
        objDriverAdd.btnSetStatus().click()    
        objDriverAdd.txtEffectiveDate().last().click()     
        const date = new Date()
        objDriverAdd.lblToDaySetED(date.getDate()).click()
        objDriverAdd.lblHour(date.getHours()).scrollIntoView().click()
        objDriverAdd.lblMinute(date.getMinutes()).scrollIntoView().click()
        objDriverAdd.btnOKSetED().click()
        objDriverAdd.txtEffectiveDate().last().then(($el)=>{
            const date = new Date()
            cy.checkFormatDate(date, $el.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check tick on Select date / time tickbox for End date / time ", () => {
        objDriverAdd.btnMainFunction("Create").click()
        objDriverAdd.btnSetStatus().click()
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check select date in End date / time is later than Active date/time", () => {   
        objDriverAdd.btnMainFunction("Create").click()   
        objDriverAdd.btnSetStatus().click()  
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()     
        objDriverAdd.dtpSelectDateTime().click()   
        objDriverAdd.lblDayNextMonth("").first().click()    
        objDriverAdd.lblHourED("").scrollIntoView().click()     
        objDriverAdd.lblMinute("").scrollIntoView().click()       
        objDriverAdd.btnOKSetED().click()
        objDriverAdd.txtSelectDateTime().then(($el) => {
            cy.checkFormatDay($el.text().trim())
            const date = new Date($el.text().trim())
            cy.checkFormatDate(date, $el.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnCancel().last().click()
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in End date / time is earlier than Active date/time", () => {  
        objDriverAdd.btnMainFunction("Create").click()  
        objDriverAdd.btnSetStatus().click()    
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()     
        objDriverAdd.dtpSelectDateTime().click()  
        objDriverAdd.lblDayPrevMonthED("").last().should('not.to.selected')   
        objDriverAdd.dtpSelectDateTime().dblclick({force : true}) 
        objDriverAdd.btnOKSetED().should('not.to.selected')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check status of Driver when select active date is current date", () => {
        objDriverAdd.btnMainFunction("Create").click()
        objDriverAdd.btnSetStatus().click()
        objDriverAdd.txtEffectiveDate().last().click()
        const date = new Date()
        objDriverAdd.lblToDaySetED(date.getDate()).click()
        objDriverAdd.lblHour(date.getHours()).scrollIntoView().click()
        objDriverAdd.lblMinute(date.getMinutes()).scrollIntoView().click()
        objDriverAdd.btnOKSetED().click()
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Select date / time").first().click()
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("No end date").first().click()
        objDriverAdd.btnSetSetStatus().click()   
        objDriverAdd.lblCreateDriver().trigger('mouseon')      
        objDriverAdd.lblStatus("Active").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check status of Driver when select active date is future", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().click()
        
        objDriverAdd.lblYearDOB().click()
        
        objDriverAdd.btnNextYear().click()
        
        objDriverAdd.lblYear("30").click()
        
        objDriverAdd.lblMonth("Oct").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.lblHour("").scrollIntoView().click()
        
        objDriverAdd.lblMinute("").click()
        
        objDriverAdd.btnOKSetED().click()
        
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblCreateDriver().trigger('mouseon')
        
        objDriverAdd.lblStatus("Pending").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check change status from Active to inactive", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.lblStatus("Active").should('be.visible')
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
        
        objDriverAdd.btnSetSetStatus().click()
        objDriverAdd.lblCreateDriver().trigger('mouseon')
        
        objDriverAdd.lblStatus("Inactive").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check change status from Pending to Active", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().click()
        
        objDriverAdd.lblYearDOB().click()
        
        objDriverAdd.btnNextYear().click()
        
        objDriverAdd.lblYear("30").click()
        
        objDriverAdd.lblMonth("Dec").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.lblHour("").scrollIntoView().click()
        
        objDriverAdd.lblMinute("").click()
        
        objDriverAdd.btnOKSetED().click()
        
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblCreateDriver().trigger('mouseon')
        
        objDriverAdd.lblStatus("Pending").trigger('mouseon').should('be.visible')
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().should('contain', '2030/12/29   13:00')
        
        objDriverAdd.txtEffectiveDate().last().click()
        
        objDriverAdd.lblYearDOB().click()
        
        objDriverAdd.btnPrevYear().click()
        
        const date = new Date()
        objDriverAdd.lblYearToday(date.getFullYear()).click()
        
        objDriverAdd.lblMonth("Dec").click()
        
        objDriverAdd.lblToDay(date.getDate()).click()
        objDriverAdd.lblHour(date.getHours()).scrollIntoView().click()
        objDriverAdd.lblMinute(date.getMinutes()).scrollIntoView().click()
        objDriverAdd.btnOKSetED().click()
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblCreateDriver().trigger('mouseon')
        
        objDriverAdd.lblStatus("Active").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check change status from Pending to Inactive", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().click()
        
        objDriverAdd.lblYearDOB().click()
        
        objDriverAdd.btnNextYear().click()
        
        objDriverAdd.lblYear("30").click()
        
        objDriverAdd.lblMonth("Dec").click()
        
        objDriverAdd.lblDay("").click()
        
        objDriverAdd.lblHour("").scrollIntoView().click()
        
        objDriverAdd.lblMinute("").click()
        
        objDriverAdd.btnOKSetED().click()
        
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblVehiclesPage().click()
        objDriverAdd.lblInformationPage().click()
        objDriverAdd.lblStatus("Pending").trigger('mouseon').should('be.visible')
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.txtEffectiveDate().last().should('contain', '2030/12/29   13:00')
        
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
        
        objDriverAdd.btnSetSetStatus().click()
        objDriverAdd.lblCreateDriver().trigger('mouseup')
        
        objDriverAdd.lblStatus("Inactive").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check change status from Inactive to Active", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
        
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblVehiclesPage().click()
        objDriverAdd.lblInformationPage().click()
        objDriverAdd.lblStatus("Inactive").trigger('mouseon').should('be.visible')
        
        objDriverAdd.btnSetStatus().click()
        
        objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Active").first().click()
        
        objDriverAdd.btnSetSetStatus().click()
        
        objDriverAdd.lblCreateDriver().trigger('mouseup')
        
        objDriverAdd.lblStatus("Active").trigger('mouseon').should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check click on the Cancel button of Set status popup  when changed some fields" +
    "Check click Confirm button in the pop up Quit seting status" +
    "Check click Cancel button in the pop up Quit seting status" +
    "Check click on the Cancel button when don't have any change", () => {
        objDriverAdd.btnMainFunction("Create").click() 
        objDriverAdd.lblEffectiveDateTime().then(($text)=>{
            cy.log($text.text().trim())
            objDriverAdd.btnSetStatus().click()
            objDriverAdd.btnCancelSetStatus().click()
            objDriverAdd.lblEffectiveDateTime().should('contain', $text.text().trim())
            objDriverAdd.btnSetStatus().click()
            objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverAdd.btnCancelSetStatus().click()
            objDriverAdd.btnConfirm().click()
            objDriverAdd.lblEffectiveDateTime().should('contain', $text.text().trim())
            objDriverAdd.btnSetStatus().click()
            objDriverAdd.defaultOtherCheckBoxButtonSetStatus("Inactive").first().click()
            objDriverAdd.btnCancelSetStatus().click()
            objDriverAdd.btnCancelCancelSetStatus().click()
            objDriverAdd.lblSetStatus().should('to.exist')
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnConfirm().click()
            objDriverAdd.btnCancel().first().click({force : true})
        })
    })

    it("Check display when hover on search box on Assign Subcontractor screen ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.txtSearchAssign().should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check search by [Subcontractor ID, Company Tax ID, Company Registered Name, Company Name both TH and EN, Province, Key Contact Person (First Name)]" +
    "Check search with exact keywords on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            
            objDriverAdd.tblSubcontractorID().first().should('have.text', $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check search with partial keywords on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.txtSearchAssign().type('{backspace}')
            objDriverAdd.btnSearchAssign().first().click()
            
            objDriverAdd.tblSubcontractorID().first().should('contain', $text.text().trim().substr(0,20))
        })
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check search with keywords is special character on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.txtSearchAssign().type("@!$@!$!")
        objDriverAdd.btnSearchAssign().first().click()
        
        objDriverAdd.lblNoData().first().should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check search with keywords does not exist in the system on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.txtSearchAssign().type("jbfasibasfas")
        objDriverAdd.btnSearchAssign().first().click()
        
        objDriverAdd.lblNoData().first().should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check press Enter from keyboard to search on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.txtSearchAssign().type('{enter}')
            
            objDriverAdd.tblSubcontractorID().first().should('have.text', $text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check delete inputted keywords on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            
            objDriverAdd.tblSubcontractorID().first().should('have.text', $text.text().trim())
            objDriverAdd.txtSearchAssign().click()
            objDriverAdd.btnDeleteKeywordAssign().click({force : true})
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.tblSubcontractorID().should('be.visible')
            objDriverAdd.txtSearchAssign().should('have.value', '')
        })
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check amount of customer displaying in the one page " +
    "Check click on any number page on Assign Subcontractor screen", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.cboPageNumber().first().click({force : true})
        
        objDriverAdd.lblPageNumber("/page").last().click({force : true})
        objDriverAdd.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '30/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverAdd.cboPageNumber().first().click({force : true})
        objDriverAdd.lblPageNumber("/page").last().click({force : true})
        objDriverAdd.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '50/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverAdd.cboPageNumber().first().click({force : true})
        objDriverAdd.lblPageNumber("/page").last().click({force : true})
        objDriverAdd.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '75/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        
        objDriverAdd.cboPageNumber().first().click({force : true})
        objDriverAdd.lblPageNumber("/page").last().click({force : true})
        objDriverAdd.cboPageNumber().first().then(($text) => {
            if($text.text().trim() == '100/page'){
                cy.log($text.text().trim())
                return true
            }
            else{
                cy.log($text.text().trim())
                return false
            }
        })
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check assign one Subcontractor for Driver", () => {
        
        objDriverAdd.btnMainFunction("Create").click()
        objDriverAdd.btnAssign().click()
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click() 
            objDriverAdd.tblCompanyTaxID().then(($text2) => {
                objDriverAdd.tblCompanyName().first().then(($text3) => {
                    objDriverAdd.btnSelectSub().click()
                    cy.log($text3.text().trim())
                    objDriverAdd.lblAssignSubInfo($text3.text().trim()).first().should(($el) =>{
                        expect($el).to.contain('Subcontractor ID: ' + $text.text().trim())
                    })
                    objDriverAdd.lblAssignSubInfo($text3.text().trim()).last().should(($el) =>{
                        expect($el).to.contain('Tax ID: ' + $text2.text().trim())
                    })
                })
            })
            objDriverAdd.btnDeleteAssignSub().should('be.visible')
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check reassign Subcontractor for Driver", () => {       
        objDriverAdd.btnMainFunction("Create").click()  
        objDriverAdd.btnAssign().click()  
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            cy.wait(5000)
            objDriverAdd.tblCompanyTaxID().then(($text2) => {
                objDriverAdd.tblCompanyName().first().then(($nameCompany) => {
                    objDriverAdd.btnSelectSub().click()
                    objDriverAdd.lblAssignSubInfo($nameCompany.text().trim()).first().should(($el) =>{
                        expect($el).to.contain('Subcontractor ID: ' + $text.text().trim())
                    })
                    objDriverAdd.lblAssignSubInfo($nameCompany.text().trim()).last().should(($el) =>{
                        expect($el).to.contain('Tax ID: ' + $text2.text().trim())
                    })
                })
            })
        })
        objDriverAdd.btnReassign().click({multiple : true, force : true}) 
        objDriverAdd.btnOrderSubID().click({multiple : true, force : true})
        cy.wait(5000)
        objDriverAdd.tblSubcontractorID().first().then(($text3) => {
            objDriverAdd.txtSearchAssign().type($text3.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            cy.wait(5000)
            objDriverAdd.tblCompanyTaxID().then(($text4) => {
                objDriverAdd.tblCompanyName().first().then(($nameCompany2) => {
                    objDriverAdd.btnSelectSub().click()
                    objDriverAdd.lblAssignSubInfo($nameCompany2.text().trim()).first().should(($el) =>{
                        expect($el).to.contain('Subcontractor ID: ' + $text3.text().trim())
                    })
                    objDriverAdd.lblAssignSubInfo($nameCompany2.text().trim()).last().should(($el) =>{
                        expect($el).to.contain('Tax ID: ' + $text4.text().trim())
                    })
                })
            })
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check delete assigned Subcontractor for Driver", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnAssign().click()
        
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            
            objDriverAdd.tblCompanyTaxID().then(($text2) => {
                objDriverAdd.tblCompanyName().first().then(($text3) => {
                    objDriverAdd.btnSelectSub().click()
                objDriverAdd.lblAssignSubInfo($text3.text().trim()).first().should(($el) =>{
                    expect($el).to.contain('Subcontractor ID: ' + $text.text().trim())
                })
                objDriverAdd.lblAssignSubInfo($text3.text().trim()).last().should(($el) =>{
                    expect($el).to.contain('Tax ID: ' + $text2.text().trim())
                })
            })
        })
            objDriverAdd.btnDeleteAssignSub().click()
            objDriverAdd.btnReassign().should('not.to.exist')
            objDriverAdd.btnAssign().trigger('mouseon').should('be.visible')
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display when hover on search box on Assign Vehicle screen ", () => {    
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        cy.wait(5000)
        objVehicle.columnSubcontractor().first().then(($subID) => {
            cy.log($subID.text().trim())
            cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
            objDriverAdd.btnMainFunction("Create").click()
            objDriverAdd.btnAssign().click()
            const subID = $subID.text().trim().substr(18, 20)
            cy.log(subID)
            objDriverAdd.txtSearchAssign().type(subID)
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.btnSelectSub().click()      
            objDriverAdd.lblVehiclesPage().click()        
            objDriverAdd.btnAssignVehicle().click()        
            objDriverAdd.txtSearchVehicle().last().should('be.visible')
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnCancel().click({force : true})
            objDriverAdd.btnConfirm().click()    
        })    
    })

    it("Check search with keywords by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site Code]" + 
    "Check search with exact keywords by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site Code] on Assign Vehicle screen", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.btnSearchVehicleAssign().last().click()     
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnCancel().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        })
    })

    it("Check search with partial keywords by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site Code] in pop up Assign Vehicle", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)
                const vehicleID = $vehiID.text().trim().substr(0, 19)
                objDriverAdd.txtSearchVehicle().last().type(vehicleID)      
                objDriverAdd.btnSearchVehicleAssign().last().click()     
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain(vehicleID)
                })
            })
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnCancel().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check search with keywords is special character in pop up Assign Vehicle", () => {       
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnSubcontractor().first().then(($subID) => {
            cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
            objDriverAdd.btnMainFunction("Create").click()
            objDriverAdd.btnAssign().click()
            const subID = $subID.text().trim().substr(18, 20)
            objDriverAdd.txtSearchAssign().type(subID)
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.btnSelectSub().click()      
            objDriverAdd.lblVehiclesPage().click()        
            objDriverAdd.btnAssignVehicle().click() 
            cy.wait(5000)
            objDriverAdd.txtSearchVehicle().last().type("!@$!$@!@%!@%!@") 
            objDriverAdd.btnSearchVehicleAssign().last().click()  
            objDriverAdd.lblNoData().first().should('be.visible')
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnCancel().click({force : true})
            objDriverAdd.btnConfirm().click()
        })
    })

    it("Check input keyword does not exist in the system in pop up Assign Vehicle", () => {     
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnSubcontractor().first().then(($subID) => {
            cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
            objDriverAdd.btnMainFunction("Create").click()
            objDriverAdd.btnAssign().click()
            const subID = $subID.text().trim().substr(18, 20)
            objDriverAdd.txtSearchAssign().type(subID)
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.btnSelectSub().click()      
            objDriverAdd.lblVehiclesPage().click()        
            objDriverAdd.btnAssignVehicle().click() 
            cy.wait(5000)
            objDriverAdd.txtSearchVehicle().last().type("sfas56f4as5fgas") 
            objDriverAdd.btnSearchVehicleAssign().last().click()  
            objDriverAdd.lblNoData().first().should('be.visible')
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnCancel().click({force : true})
            objDriverAdd.btnConfirm().click()
        })
    })

    it("Check press Enter from keyboard to search in pop up Assign Vehicle", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.txtSearchVehicle().last().type('{enter}')      
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnCancel().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        })
    })

    it("Check delete inputted keywords in pop up Assign Vehicle", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.txtSearchVehicle().last().type('{enter}')      
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
            })
            objDriverAdd.btnDeleteKeywordAssignVehicle().click()
            objDriverAdd.btnSearchVehicleAssign().last().click()  
            objDriverAdd.columnVehicleID().should('be.visible')
            objDriverAdd.txtSearchVehicle().last().should('have.value', '')
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnCancel().click({force : true})
            objDriverAdd.btnConfirm().click()
        })
    })

    it("Check amount of customer displaying in the one page " +
    "Check click on any number page on Assign Vehicle screen", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnSubcontractor().first().then(($subID) => {
            cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
            objDriverAdd.btnMainFunction("Create").click()
            objDriverAdd.btnAssign().click()
            const subID = $subID.text().trim().substr(18, 20)
            objDriverAdd.txtSearchAssign().type(subID)
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.btnSelectSub().click()      
            objDriverAdd.lblVehiclesPage().click()        
            objDriverAdd.btnAssignVehicle().click()        
            objDriverAdd.txtSearchVehicle().last().should('be.visible')  
        }) 
        cy.wait(10000)    
        objDriverAdd.cboPageNumber().first().click()
        objDriverAdd.lblPageNumber("/page").last().click()
        objDriverAdd.lblPageNumber("/page").last().should('have.text', '30/page')
        cy.wait(5000)
        objDriverAdd.cboPageNumber().first().click()
        objDriverAdd.lblPageNumber("/page").last().click()
        objDriverAdd.lblPageNumber("/page").last().should('have.text', '50/page')
        cy.wait(5000)
        objDriverAdd.cboPageNumber().first().click()
        objDriverAdd.lblPageNumber("/page").last().click()
        objDriverAdd.lblPageNumber("/page").last().should('have.text', '75/page')
        cy.wait(5000)
        objDriverAdd.cboPageNumber().first().click()
        objDriverAdd.lblPageNumber("/page").last().click()
        objDriverAdd.lblPageNumber("/page").last().should('have.text', '100/page')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnCancel().click({force : true})
        objDriverAdd.btnConfirm().click()  
    })

    it("Check assign one vehicle for Driver", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.btnSearchVehicleAssign().last().click()     
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
                objDriverAdd.chkVehicle($vehiID.text().trim()).last().should('be.visible').click()
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        })
    })

    it("Check assign multiple vehicle for Driver" + "Check assign all vehicle for Driver", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click()   
                cy.wait(5000)   
                objDriverAdd.chkAllVehicle().click({multiple : true, force : true})   
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        })
    })

    it("Check Unselect vehicle after selected successfully", () => {
        objDriverAdd.btnMainFunction("Create").click()
        objDriverAdd.btnAssign().click()
        objDriverAdd.tblSubcontractorID().first().then(($text) => {
            objDriverAdd.txtSearchAssign().type($text.text().trim())
            objDriverAdd.btnSearchAssign().first().click()
            objDriverAdd.btnSelectSub().click()
        })
        objDriverAdd.lblVehiclesPage().click() 
        objDriverAdd.btnAssignVehicle().click()
        cy.wait(5000)   
        objDriverAdd.columnVehicleID().last().then(($text) => {
            objDriverAdd.txtSearchVehicle().last().type($text.text().trim())      
            objDriverAdd.btnSearchVehicleAssign().last().click()     
            objDriverAdd.columnVehicleID().last().should(($el) => {
                expect($el).to.contain($text.text().trim())
            })
            objDriverAdd.chkVehicle($text.text().trim()).last().click()
            objDriverAdd.lblValueSelected().should('contain', '1 selected')
            objDriverAdd.chkAllVehicle().click({multiple : true, force : true})
            objDriverAdd.lblValueSelected().should('contain', '0 selected')
        })
    })

    it("Check remove vehicle that is assign for Driver", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.btnSearchVehicleAssign().last().click()     
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
                objDriverAdd.chkVehicle($vehiID.text().trim()).last().should('be.visible').click()
                objDriverAdd.btnAssign().last().click()
                objDriverAdd.chkVehicle($vehiID.text().trim()).last().click()
                objDriverAdd.btnRemoveVehicle().click()    
                objDriverAdd.lblNoData().should('be.visible')
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        })
    })

    it("Check export vehicle that is assign for Driver", () => {
        cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
        cy.wait(5000)
        objVehicle.btnFilter().click()
        objVehicle.cboVehicleStatus().click()
        objVehicle.lblVehicleStatus("Active").click()
        objVehicle.columnVehicleID().first().then(($vehiID) => {
            objVehicle.columnSubcontractor().first().then(($subID) => {
                cy.reload()
                objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                const subID = $subID.text().trim().substr(18, 20)
                objDriverAdd.txtSearchAssign().type(subID)
                objDriverAdd.btnSearchAssign().first().click()
                objDriverAdd.btnSelectSub().click()      
                objDriverAdd.lblVehiclesPage().click()        
                objDriverAdd.btnAssignVehicle().click() 
                cy.wait(5000)   
                objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                objDriverAdd.btnSearchVehicleAssign().last().click()     
                objDriverAdd.columnVehicleID().last().should(($el) => {
                    expect($el).to.contain($vehiID.text().trim())
                })
                objDriverAdd.chkVehicle($vehiID.text().trim()).last().should('be.visible').click()
                objDriverAdd.btnAssign().last().click()
                objDriverAdd.chkVehicle($vehiID.text().trim()).last().click()
                objDriverAdd.btnExportVehicle().should('be.visible')
            })
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check create Driver successfully" + 
    "Check Confirm button on Create Driver without Subcontractors and Vehicles information popup", () => {
        var filePath = "implement/images.png"
        const date = new Date()
        const day = date.getDate()
        cy.randomString(5).then((text)=>{
            cy.randomNumber(9).then((num)=>{
                objDriverAdd.btnMainFunction("Create").click()       
                objDriverAdd.createNewDriverV1(filePath, 'นาย', text, 'vmotest' + text, 'ชาย', day, text + '123', text + '234', num, num, ' ', num + '0123', '012' + num, 'บ.2', 'สาธารณะ')
                objDriverAdd.btnConfirm().click()
                objDriverAdd.msgSuccess().should('have.text', 'Create driver successfully')
            })
        }) 
    })

    it("Check button Cancel on Create Driver screen" +
    "Check Confirm button on Quit editng popup" +
    "Check Cancel button on Quit editng popup", () => {
        var filePath = "implement/images.png"
        const date = new Date()
        const day = date.getDate()
        cy.randomString(5).then((text)=>{
            cy.randomNumber(9).then((num)=>{
                objDriverAdd.btnMainFunction("Create").click()             
                objDriverAdd.createNewDriverV1(filePath, 'นาย', text, 'vmotest' + text, 'ชาย', day, text + '123', text + '234', num, num, ' ', num + '0123', '012' + num, 'บ.2', 'สาธารณะ')               
                objDriverAdd.btnCancel().last().click()
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.lblMessageTitle().should('have.text', 'Quit creating driver')
                objDriverAdd.lblMessageContent().should('have.text', 'Are you sure you want to quit creating driver? All changes you made will be lost immediately.')
                objDriverAdd.btnCancel().last().should('be.visible')
                objDriverAdd.btnConfirm().should('be.visible')
                objDriverAdd.btnCancel().last().click()
                objDriverAdd.btnCancel().first().click({force : true})
                objDriverAdd.btnConfirm().click()
            })
        }) 
    })

    it("Check to save Driver when assigned only subcontractor company & without vehicle", () => {
        var filePath = "implement/images.png"
        const date = new Date()
        const day = date.getDate()
        cy.randomString(5).then((text)=>{
            cy.randomNumber(9).then((num)=>{
                objDriverAdd.btnMainFunction("Create").click()
                objDriverAdd.btnAssign().click()
                objDriverAdd.tblSubcontractorID().first().then(($text) => {
                    objDriverAdd.txtSearchAssign().type($text.text().trim())
                    objDriverAdd.btnSearchAssign().first().click()
                    objDriverAdd.tblSubcontractorID().first().should('have.text', $text.text().trim())
                    objDriverAdd.btnSelectSub().click()
                })
                objDriverAdd.createNewDriverV1(filePath, 'นาย', text, 'vmotest' + text, 'ชาย', day, text + '123', text + '234', num, num, ' ', num + '0123', '012' + num, 'บ.2', 'สาธารณะ')
                cy.wait(10000)
                const mobilePhone = "-" + num.toString().substr(0,2) + "-" + num.toString().substr(2,3) + "-" + num.toString().substr(5,4)
                const month = date.getMonth() + 1
                var expDate
                if(day <= 9){
                    expDate = date.getFullYear().toString() + "-" + month.toString() + "-0" + day.toString()
                }else{
                    expDate = date.getFullYear().toString() + "-" + month.toString() + "-" + day.toString()
                }
                objDriverAdd.verifyNewDriver('', text, 'vmotest' + text, mobilePhone, '012' + num.toString().substr(0,5), 'บ.2',expDate)
            })
        }) 
    })

    it("Check to save Driver when assigned  subcontractor company & assigned   vehicle", () => {
        var filePath = "implement/images.png"
        const date = new Date()
        const day = date.getDate()
        cy.randomString(5).then((text)=>{
            cy.randomNumber(9).then((num)=>{
                cy.reload()
        objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Master Data", "Vehicle")
                cy.wait(5000)
                objVehicle.btnFilter().click()
                objVehicle.cboVehicleStatus().click()
                objVehicle.lblVehicleStatus("Active").click()
                objVehicle.columnVehicleID().first().then(($vehiID) => {
                    objVehicle.columnSubcontractor().first().then(($subID) => {
                        cy.reload()
objDriverAdd = objHome.leftMenu.navigateToSubPageDriver2()
                        objDriverAdd.btnMainFunction("Create").click()
                        objDriverAdd.btnAssign().click()
                        const subID = $subID.text().trim().substr(18, 20)
                        objDriverAdd.txtSearchAssign().type(subID)
                        objDriverAdd.btnSearchAssign().first().click()
                        objDriverAdd.btnSelectSub().click()      
                        objDriverAdd.lblVehiclesPage().click()        
                        objDriverAdd.btnAssignVehicle().click() 
                        cy.wait(5000)   
                        objDriverAdd.txtSearchVehicle().last().type($vehiID.text().trim())      
                        objDriverAdd.btnSearchVehicleAssign().last().click()     
                        objDriverAdd.columnVehicleID().last().should(($el) => {
                            expect($el).to.contain($vehiID.text().trim())
                        })
                    objDriverAdd.chkVehicle($vehiID.text().trim()).last().should('be.visible').click()
                    objDriverAdd.btnAssign().last().click()
                    objDriverAdd.lblInformationPage().click()
                    objDriverAdd.createNewDriverV1(filePath, 'นาย', text, 'vmotest' + text, 'ชาย', day, text + '123', text + '234', num, num, ' ', num + '0123', '012' + num, 'บ.2', 'สาธารณะ')
                    cy.wait(10000)
                    const mobilePhone = "-" + num.toString().substr(0,2) + "-" + num.toString().substr(2,3) + "-" + num.toString().substr(5,4)
                    const month = date.getMonth() + 1
                    var expDate
                    if(day <= 9){
                        expDate = date.getFullYear().toString() + "-" + month.toString() + "-0" + day.toString()
                    }else{
                        expDate = date.getFullYear().toString() + "-" + month.toString() + "-" + day.toString()
                    }
                    objDriverAdd.verifyNewDriver(subID, text, 'vmotest' + text, mobilePhone, '012' + num.toString().substr(0,5), 'บ.2',expDate)
                    })
                })
            })
        }) 
    })

    it("Check when save Driver without  Subcontractor and vehicle" +
    "Check Cancel button on Create Driver without Subcontractors and Vehicles information popup", () => {
        var filePath = "implement/images.png"
        const date = new Date()
        const day = date.getDate()
        cy.randomString(5).then((text)=>{
            cy.randomNumber(9).then((num)=>{
                objDriverAdd.btnMainFunction("Create").click()                
                objDriverAdd.createNewDriverV2(filePath, 'นาย', text, 'vmotest' + text, 'ชาย', day, num, num, ' ', num + '0123', '012' + num, 'บ.2', 'สาธารณะ')
                objDriverAdd.lblMessageTitle().should('have.text', 'Creating Driver without Subcontractors information')   
                objDriverAdd.lblMessageContent().should('have.text', 'Are you sure you want to create Driver without  Subcontractor information? The Driver’s status will be set  to inactive.')
                objDriverAdd.btnCancel().last().should('be.visible')
                objDriverAdd.btnConfirm().should('be.visible')
                objDriverAdd.btnCancel().last().click()
                objDriverAdd.lblCreateDriver().trigger('mouseon').should('be.visible')
                objDriverAdd.btnSaveCreateNew().click()
                objDriverAdd.btnConfirm().click()
                objDriverAdd.common.msgSuccess().should('be.visible')
            })
        })
        cy.wait(5000)
        objDriverAdd.animationButtonEdit("inactive").first().trigger('mouseon').should('be.visible')
    })  
})