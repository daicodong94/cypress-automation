import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import DriverReceipt from "./DriverReceipt";

class ShippersReceipt {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.driverReceipt = new DriverReceipt()
    }


    btnSendEmail() {
        return cy.xpath('//button//span[contains(text(),"send")]')
    }

    txtEmail() {
        return cy.xpath('//div[@class="tag-list tag-email-list"]//span')
    }

    txtSms() {
        return cy.xpath('//div[@class="tag-list tag-mobile-list"]//span')
    }

    confirmSendEmail(confirm) {
        // cy.randomNumber(5).then((number) => {
        //     this.txtEmail().type("VmoVietNam" + number + "@gmail.com")
        //     this.txtSms().type("test send messge" + number)
        // })
        this.btnSendEmail().click()
        if (confirm == "yes") {
            this.common.btnConfirmAgainHintPopup("confirm").click()
            // this.common.msgSuccessShouldContainText("done successfully").should("be.visible")
        } else if (confirm == "no") {
            this.common.btnConfirmAgainHintPopup("cancel").click()
        }
    }
}

export default ShippersReceipt;