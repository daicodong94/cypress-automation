import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import LeftMenuMasterData from "../fragments/LeftMenuMasterData"
import CreateSite from "./CreateSite"
import SiteAdd from "./SiteAdd"
import SiteList from "./SiteList"

class SiteEdit {
    constructor() {
        this.header = new HeaderMasterData()
        this.siteAdd = new SiteAdd()
        this.common = new CommonMasterData()
        this.leftMenu = new LeftMenuMasterData()
        this.createSite = new CreateSite()
        this.siteList = new SiteList()
    }

    editWindowTime(name) {
        this.siteAdd.windowTimeTab(name).click()
        this.iconEditSpecialDay().click()
        cy.wait(2000)
    }

    createSiteInputAllData(CustomerName, SiteCode, SiteName, SiteType, SiteTypeGroup, tabName, index, textName, Title, FirstName, LastName, Email, Extension, Address, Province, District, SubDistrict, PostalCode) {
        this.header.btnCreate().click()
        this.createSite.ipCustomerName("Enter Customer name", CustomerName)
        cy.wait(1000)
        this.createSite.ipInformationOnSite("Enter Code", SiteCode)
        cy.wait(1000)
        this.createSite.ipSiteTextareaInfor("Enter Site name" , SiteName)
        cy.wait(1000)
        this.createSite.selectInformation("Enter Site type", SiteType)
        cy.wait(1000)
        this.createSite.selectInformation("Select Site type group", SiteTypeGroup)
        cy.wait(1000)
        this.createSite.ckbDayChecked("Tuesday").scrollIntoView().click()
        this.createSite.ckbDayChecked("Wednesday").scrollIntoView().click()
        this.createSite.ckbDayChecked("Thursday").scrollIntoView().click()
        this.createSite.ckbDayChecked("Friday").scrollIntoView().click()
        this.createSite.ckbDayChecked("Saturday").scrollIntoView().click()
        this.createSite.ckbDayChecked("Sunday").scrollIntoView().click()      
        this.createSite.selectStartTimeAndEndTime()
        this.siteAdd.addSpecialDay(tabName, index, "today", "Add", textName)
        this.createSite.selectInformation("Select Title", Title)
        cy.wait(1000)
        this.createSite.ipInformationOnSite("Enter Key Contact Person’s first name", FirstName)
        cy.wait(1000)
        this.createSite.ipInformationOnSite("Enter Key Contact Person’s last name", LastName)
        cy.wait(1000)
        cy.randomNumber(5).then((number) => {
            this.createSite.ipInformationOnSite("Enter Key Contact Person’s email", Email + number + '@gmail.com')
        })
        cy.wait(1000)
        cy.randomNumber(9).then((mobilePhone) => {
            this.createSite.ipInformationOnSite("Enter Mobile Phone no.", mobilePhone)
        })
        cy.wait(1000)
        cy.randomNumber(8).then((landlinePhone) => {
            this.createSite.ipInformationOnSite("Enter Landline phone no.", landlinePhone)
        })
        this.createSite.ipInformationOnSite("Extension", Extension)
        cy.wait(1000)
        this.createSite.ipSiteTextareaInfor("Enter Address", Address)        
        cy.wait(1000)
        // Select Province
        this.createSite.ipCustomerName("Select Province", Province)
        cy.wait(1000)
        // Select District
        this.createSite.selectInformation("Select District", District)
        cy.wait(1000)
        // Select Sub District
        this.createSite.selectInformation("Select Sub-District", SubDistrict)
        cy.wait(1000)
        // Select PostalCode
        this.createSite.selectInformation("Select Postal Code", PostalCode)
        cy.wait(1000)
        this.createSite.btnConfirmLocation().click()
        cy.wait(2000)
        this.createSite.btnSave().click()
        cy.wait(2000)
    }

    columnDateAndRemark(name) {
        return cy.xpath('//th//div[contains(text(),"'+ name +'")]')
    }

    iconDeleteSpecialDay() {
        return cy.xpath('(//*[local-name()="svg" and @class="m-auto"])[2]')
    }

    iconEditSpecialDay() {
        return cy.xpath('(//*[local-name()="svg" and @class="m-auto"])[1]')
    }

    statusBtnConfirm(btnName, index) {
        return cy.xpath('(//span[contains(text(),"'+ btnName +'")]//ancestor::button)['+ index +']')
    }

    titleEditSiteScreen() {
        return cy.xpath('//p[contains(text(),"Edit site")]')
    }

    confirmCancelEditSpecialDayOff(confirm, title) {
        this.siteAdd.titlePopupQuitSpecialOff().should("be.visible")
        if (confirm == "confirm") {
            this.siteAdd.btnConfirmQuitSpecialOff("Confirm").click()
            this.titleEditSiteScreen().should("be.visible")
        } else if (confirm == "cancel") {
            this.siteAdd.btnConfirmQuitSpecialOff("Cancel").click()
            this.siteAdd.titleSpecialScreen(title).should("be.visible")
        }
    }

    msgEditSiteSuccess() {
        return cy.xpath('//p[text()="Update site successfully"]')
    }

    msgInvalidPhone(text) {
        return cy.xpath('//div[@class="text-error-5 text-sm error-message has-error" and contains(text(),"'+ text +'")]')
    }

    clearInformation(name) {
        return cy.xpath('//input[@placeholder="'+ name +'"]//ancestor::div[@class="el-input el-input--base"]')
    }

    selectSpecialTime(time) {
        return cy.xpath("//div[contains(text(),'Time')]//parent::div//span//div[@class='px-3 py-2 text-primary-85']")
    }
}   export default SiteEdit;