import RouteCode from "../../../support/pages/MasterData/components/RouteCode/RouteCode"

describe("", () => {

    var data
    var objRouteCode = new RouteCode()

    beforeEach(function () {
        cy.fixture("data_test_new_enhancement.json").then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            objRouteCode.commonMD.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Route Code")
        })
    })

    it("TC_15:Check when combining many the condition to search", () => {

        var routeCode = "880901"

        objRouteCode.btnFilter().click()
        objRouteCode.txtSearchOriginName().type(routeCode + "{enter}")
        objRouteCode.lblRouteCode().should('have.text', routeCode)

    })

    it("TC_24:Check case remove 1 Route Code successfully", () => {

        var filePath = "routecode/TC_24_RouteCode.xlsx"
        var routeCode = "880902"

        objRouteCode.btnImport().click()
        objRouteCode.importRouteCode(filePath)
        objRouteCode.chkboxRouteCode(routeCode).click({force: true})
        objRouteCode.removeRouteCode()
        objRouteCode.commonMD.msgSuccessShouldHaveText("Remove Route Code successfully")
    })

    it("TC_25:Check case remove many Route Code successfully", () => {

        var filePath = "routecode/TC_25_RouteCode.xlsx"
        var routeCode = "880903"
        var routeCodeNext = "880904"

        objRouteCode.btnImport().click()
        objRouteCode.importRouteCode(filePath)
        objRouteCode.chkboxRouteCode(routeCode).click({force: true})
        objRouteCode.chkboxRouteCode(routeCodeNext).click({force: true})
        objRouteCode.removeRouteCode()
        objRouteCode.commonMD.msgSuccessShouldHaveText("Remove Route Code successfully")
    })

    it("TC_27:Check display of Import Route Code pop up", () => {

        objRouteCode.btnImport().click()
        objRouteCode.importRouteCodePopUp()

    })

    it("TC_115:Check when searching data on' Select Route Code you want to owverwrite' screen", () => {

        var filePath = "routecode/TC_115_RouteCode.xlsx"
        var routeCode = "880901"

        objRouteCode.btnImport().click()
        objRouteCode.commonMD.attachImportFile(filePath)
        objRouteCode.btnImportFile().click()
        objRouteCode.popupSelectOverwrite().should("be.visible")
        objRouteCode.txtSearchOverwrite().type(routeCode + "{enter}")
        objRouteCode.lblOverwriteRouteCode().should('have.text', routeCode)

    })

})