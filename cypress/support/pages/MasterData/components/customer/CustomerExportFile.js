import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SiteList from "../site/SiteList";
import LoginSupplier from "../../../TMS/components/login/LoginSupplier";
import CustomerList from "./CustomerList";
class CustomerExportFile {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        this.siteList = new SiteList()
        this.login = new LoginSupplier()
        this.customerList = new CustomerList()
    }



}

export default CustomerExportFile