import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class Homepage{
    constructor(){
        this.header = new Header()
        this.leftMenu = new LeftMenu()
    }

    lblScreen(screen){
        return cy.xpath("//li[@class='common-menu-item active']//a[contains(.,'" + screen + "')]")
    }

    shouldBeNavigateToScreen(screen){
        this.lblScreen().should('contain', screen)
    }

    spanUsername() {
        return cy.xpath("(//span[@class='pl-2'])[1]")
    }

    btnQuit() {
        return cy.xpath("//span[text()='quit']")
    }

    txtPassword(){
        return cy.xpath('//input[@type="password"]')
    }

    txtConfirmPassword(){
        return cy.xpath('//input[@type="password"]')
    }

    btnSetPassword(){
        return cy.xpath('//button[contains(.,"Set Password")]')
    }

    btnContinueToAllNowTMS(){
        return cy.xpath("//button[contains(.,'CONTINUE TO ALL NOW TMS')]")
    }

    logout() {
        this.spanUsername().click()
        cy.wait(1000)
        this.btnQuit().click()
    }

}

export default Homepage;