import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SiteList from "../site/SiteList";
import CustomerList from "./CustomerList";
class CustomerImportFile {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        // this.siteList = new SiteList()
        this.customerList = new CustomerList()
    }
    txtImportFile() { return cy.xpath("//span[@class='el-dialog__title' and text()='Import Customer']") }
    ipUploadFile() { return cy.xpath('(//input[@type="file"])[1]') }
    txtFileName(fileName) { return cy.xpath("//div[text()='" + fileName + "") }
    btnImport() { return cy.xpath("//button[@class='el-button el-button--primary el-button--mini']//span[contains(text(),'Import')]") }
    btnCancel() { return cy.xpath("//button[@class='el-button el-button--default el-button--mini']//span[normalize-space()='Cancel']") }
    btnCancelQuit() { return cy.xpath("//button[@class='el-button el-button--default el-button--small']//span[contains(text(),'Cancel')]") }
    btnConfirmQuit() { return cy.xpath("//span[normalize-space()='Confirm']") }
    msgUnccess(msg) { return cy.xpath("//p[text()='" + msg + "']") }

    attachImportFile(filePath) {
        this.ipUploadFile().attachFile(filePath)
    }
    clickBtnCancleOrImport(condition) {
        if (condition == 'Cancel') {
            this.btnCancel().click({ multiple: true })
        }
        else if (condition == 'Import') {
            this.btnImport().click({ multiple: true })
        }
    }
    clickIconTrash() {
        this.iconTrash().click({ multiple: true })
    }
}

export default CustomerImportFile