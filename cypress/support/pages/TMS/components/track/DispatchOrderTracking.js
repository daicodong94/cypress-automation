import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import CreateShippingOrder from "../shippingorder/CreateShippingOrder";
import CommonMasterData from "../../../MasterData/components/common/CommonMasterData";

class DispatchOrderTracking {

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.commonMD = new CommonMasterData()
        this.createShippingOrder = new CreateShippingOrder()
    }

    lstStatus() {
        return cy.xpath('//span[contains(@class,"common-boder")]')
    }

    searchResultWithOutValueInDDLTrack() {
        return cy.xpath("//div[@class='list-out-box']//span[@class='search-highlight']")
    }

    lblFileManagement() {
        return cy.xpath('//div[@id="tab-4"]')
    }

    lblExecutionInformation() {
        return cy.xpath('//div[@id="tab-executInfo"]')
    }

    lblBillingInformation() {
        return cy.xpath('//div[@id="tab-statement"]')
    }

    lblRecordInformation() {
        return cy.xpath('//div[@id="tab-3"]')
    }

    lblOperationRecord() {
        return cy.xpath('//div[@id="tab-operationLog"]')
    }

    btnAddAttachments() {
        return cy.xpath('(//input[@type="file"])[1]')
    }

    lblServiceItemInformation() {
        return cy.xpath("//span[.='Service item information']")
    }

    btnClosePUDD() {
        return cy.xpath("//button//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnDownloadAttachmentDoc() {
        return cy.xpath("(//button[normalize-space()='download'])[2]")
    }

    btnSaveEdit() {
        return cy.xpath('(//span[contains(text(),"save")])[1]')
    }

    lblMessageRecord() {
        return cy.xpath('//div[@class="chat-message-content"]')
    }

    btnViewImplementation() {
        return cy.xpath("//button[@title='View implementation']")
    }

    lblDeliveryExecution() {
        return cy.xpath("//span[.='Delivery execution']")
    }

    lblTotalWeight() {
        return cy.xpath('(//div[@class="el-table__body-wrapper is-scrolling-none"]//table//tr//td//div)[7]')
    }

    lblTotalQuantity() {
        return cy.xpath('(//div[@class="el-table__body-wrapper is-scrolling-none"]//table//tr//td//div)[9]')
    }

    btnCloseSetColumn() {
        return cy.xpath("//div[@aria-label='dialog']//button[@aria-label='Close']")
    }

    btnCloseDownloadDoc() {
        return cy.xpath("//div[@aria-label='download documents']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnServiceItemInfor() {
        return cy.xpath("//div[@aria-label='Service item information']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnClosePopupServiceItemInfo() {
        return cy.xpath("//div[@aria-label='Service item information']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    chooseValueWhenSearchInDDLTrack() {
        return cy.xpath("//span[@class='search-highlight']")
    }

    checkChangeStatusEnabledToDisabledButtonSetColumnWhenAdjust(column) {
        this.header.divColumnToAdjust(column).should('have.attr', 'class', 'dragg-item').click()

    }

    checkChangeStatusDisabledToEnabledButtonSetColumnWhenAdjust(column) {
        this.header.divColumnToAdjust(column).should('have.attr', 'class', 'dragg-item itemBgGray').click()
    }

    columnStatus() {
        return cy.xpath("//td[3]//div")
    }

    shouldBeNextPageWhenClickButtonNextPage() {
        this.header.btnNext().click()
        this.header.ipPageNumber().then(function ($text) {
            if ($text.text().trim() !== 1) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
    }

    shouldBeDisabledWhenClickButtonPreviousPageFirst() {
        this.header.btnPrev().should('be.disabled')
    }

    shouldBeDisabledWhenClickButtonNextPageFirst() {
        this.header.totalValue().then(($text) => {
            this.common.selectPageSize("500/page")
            let value = Number.parseInt($text.text().trim().substr(6, 4))
            for (let i = 1; i < value / 500; i++) {
                this.header.btnNext().click()
            }
            this.header.btnNext().first().should('have.attr', 'disabled', 'disabled')
        })
    }

    shouldBePreviousPageWhenClickButtonPreviousPage() {
        this.header.btnNext().click()
        this.header.btnPrev().click()
        this.header.ipPageNumber().then(function ($text) {
            if ($text.text().trim() === 1) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
    }

    shouldBePageWhenInputPageNumber(pageNumber) {
        this.header.ipPageNumber().clear()
        this.header.ipPageNumber().type(pageNumber, { force: true })
        this.header.ipPageNumber().type('{enter}')
        cy.wait(2000)
        cy.screenshot('shouldBePageWhenInputPageNumber')
    }

    checkStatus(status) {
        var count = 0
        this.lstStatus().each(($el) => {
            const statusS = $el.text().trim()
            cy.log($el.text().trim())
            if (statusS === status) {
                count += 1
            }
            else {
                cy.log("Fail")
            }
        }).then(() => {
            this.header.totalValue().should('contain', 'Total ' + count)
        })
    }

    searchTrackingNumber(trackingNum) {
        this.header.ipSearchBox("Job Number (R)").should('be.visible').focus().realType(trackingNum)
        this.header.ipSearchBox("Job Number (R)").type('{enter}')
        this.chooseValueWhenSearchInDDLTrack().click({ multiple: true, force: true })
        this.header.btnAtHeader("inquire").click({ multiple: true, force: true })
    }

    clickExecutionInformation() {
        this.lblExecutionInformation().click({ force: true })
    }

    clickBillingInformation() {
        this.lblBillingInformation().click({ force: true })
    }

    clickOperationRecord() {
        this.lblOperationRecord().click({ force: true })
    }

    clickFileManagement() {
        this.lblFileManagement().click({ force: true })
    }

    addFileAttachments(filePath) {
        this.btnAddAttachments().attachFile(filePath)
    }

    checkServiceInformation() {
        this.lblServiceItemInformation().should("be.visible")
    }

    closePopupDownloadDocument() {
        this.btnClosePUDD().click({ multiple: true, force: true })
    }

    clickDownloadAttachmentDoc() {
        this.btnDownloadAttachmentDoc().click({ force: true })
    }

    nputSomeText(text) {
        cy.frameLoaded('#xhe0_iframe')
        cy.iframe().focus().type(text, { force: true })
    }

    clickButtonSaveEditRecord() {
        this.btnSaveEdit().click()
    }

    shouldBeShowMessageSucess(text) {
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('contain', 'done successfully')
        this.lblMessageRecord().each(($el) => {
            if ($el.text().trim() === text) {
                expect($el).to.contain(text)
            }
            else {
                cy.log($el.text().trim())
            }
        })
    }

    clickImplementation() {
        this.btnViewImplementation().first().click()
    }

    verifyImplementation(totalWeight, totalQuantity) {
        this.lblDeliveryExecution().should("be.visible")
        this.lblTotalWeight().then(($text1) => {
            if ($text1.text().trim() === totalWeight) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
        this.lblTotalQuantity().then(($text2) => {
            if ($text2.text().trim() === totalQuantity) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
    }

    shippingAndDispatchOrderTrackingStatus(status) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//span[text()= "' + status + '"]')
    }

    verifyStatusOrder(dispatchOrderNumber, status) {
        // cy.wait(2000)
        this.searchTrackingNumber(dispatchOrderNumber.text().trim())
        // cy.wait(2000)
        this.shippingAndDispatchOrderTrackingStatus(status).first().should('be.visible')
    }

    tabDataCorrection() {
        return cy.xpath("//div[@id='tab-dataCorrect']")
    }

    ipRouteCode() {
        return cy.xpath("//div[contains(text(),'Other Information')]//following::tr//td[contains(text(),'Route Code')]//following::input[1]")
    }

    ddlPaidByVehicleType() {
        return cy.xpath("//div[contains(text(),'Paid by Vehicle Type')]//following::input[1]")
    }

    icDeleteLicesePlateNumber() {
        return cy.xpath("(//div[text()='Subcontractor Information']//following::tr//td[contains(text(),'License Plate Number')]//following::i[@class='el-input__icon el-icon-caret-top'])[1]")
    }

    ipDeleteLicesePlateNumber() {
        return cy.xpath("(//div[text()='Subcontractor Information']//following::tr//td[contains(text(),'License Plate Number')]//following::input)[1]")
    }

    ipReason() {
        return cy.xpath("(//div[text()='Subcontractor Information']//following::tr//td[contains(text(),'Reason')]//following::textarea)[1]")
    }

    lblSubcontractor() {
        return cy.xpath("(//div[text()='Subcontractor Information']//following::tr//td[text()='Subcontractor']//following::td)[1]")
    }

    ddlDriver() {
        return cy.xpath("//tr[contains(@class,'search-candidate-item')]")
    }

    ipActualTotalNumberOfPackReceived() {
        return cy.xpath("//div[text()='Actual Received']//following-sibling::div//input[@editable='true']")
    }

    ipActualTotalNumberOfPackPaid() {
        return cy.xpath("//div[text()='Actual Paid']//following-sibling::div//input[@editable='true']")
    }

    selectRandomDriver() {
        this.ddlDriver().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.ddlDriver().its('length').then((len) => {
            this.ddlDriver().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    ddlVehicle() {
        return cy.xpath("(//div[@class='search-candidate-list'])[last()]//li[contains(@class,'search-candidate-item')]")
    }

    selectRandomVehicle() {
        this.ddlVehicle().last().scrollIntoView().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.ddlVehicle().its('length').then((len) => {
            this.ddlVehicle().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    chkboxSurcharge() {
        return cy.xpath("//div[@aria-label='Add Service Item']//div[@class='el-table__fixed-body-wrapper']//tr//label[@class='el-checkbox']")
    }

    selectRandomSurcharge() {
        this.chkboxSurcharge().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.chkboxSurcharge().its('length').then((len) => {
            this.chkboxSurcharge().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    divSuccess() {
        return cy.xpath("//div[@class='bg-success-1 border border-success-3 flex items-center text-base w-max py-2 px-4']")
    }

    btnAddSurcharge() {
        return cy.xpath("//button//span//i[@class='el-icon-plus']")
    }

    dataCorrection(changePackagesReceived) {
        this.tabDataCorrection().click({ force: true })
        cy.wait(500)
        this.ipRouteCode().click({ force: true })
        this.createShippingOrder.txtSearchField().should("be.visible", { timeout: 10000 })
        this.createShippingOrder.txtSearchField().realType("001488", { force: true })
        this.createShippingOrder.spanRouteSearchResult().should("be.visible", { timeout: 10000 })
        this.createShippingOrder.spanRouteSearchResult().click({ force: true })
        cy.wait(500)
        this.commonMD.ipSelectBox("Select Unit").first().click({ force: true })
        this.commonMD.spanValue("Pack/006").last().click({ force: true })
        this.commonMD.spanSelectedData().should("have.text", "Pack/006")
        cy.wait(500)
        this.commonMD.ipSelectBox("Select Unit").last().click({ force: true })
        this.commonMD.spanValue("Pack/006").last().click({ force: true })
        this.commonMD.spanSelectedData().should("have.text", "Pack/006")
        //get license of selected driver
        this.ipDeleteLicesePlateNumber().invoke('attr', 'test-data').as('expectedLicense')
        cy.log("expectedLicense is : " + cy.get('@expectedLicense'))
        //type reason
        this.ipReason().scrollIntoView().clear().type("Verify data correction", { force: true })
        if (changePackagesReceived == "yes") {
            cy.randomNumber(1).then((packages) => {
                if (packages > 1) {
                    // this.common.textField("Enter Actual Total Number of Packages Received").type(packages, { force: true })
                    // this.common.textField("Enter Actual Total Number of Packages Paid").type(packages, { force: true })
                    this.ipActualTotalNumberOfPackReceived().clear().type(packages, { force: true })
                    this.ipActualTotalNumberOfPackPaid().clear().type(packages, { force: true })
                } else {
                    // this.common.textField("Enter Actual Total Number of Packages Received").type(7, { force: true })
                    // this.common.textField("Enter Actual Total Number of Packages Paid").type(7, { force: true })
                    this.ipActualTotalNumberOfPackReceived().clear().type(7, { force: true })
                    this.ipActualTotalNumberOfPackPaid().clear().type(7, { force: true })
                }
            })
            cy.wait(500)
            //change license plate number
            this.icDeleteLicesePlateNumber().realHover().click({ force: true })
            this.ipDeleteLicesePlateNumber().click({ force: true })
            cy.wait(1000)
            this.selectRandomDriver()
            //change paid by vehicle type
            this.ddlPaidByVehicleType().click({ force: true })
            this.selectRandomVehicle()
            //add surcharge / discount items
            this.btnAddSurcharge().click({ force: true })
            cy.wait(500)
            this.selectRandomSurcharge()
            cy.wait(300)
            this.commonMD.btnConfirmHintPopup("Add", 2).click({ force: true })
        }
        cy.wait(400)
        //save: success msg > confirm
        this.header.btnAtHeader("Save").first().scrollIntoView().click({ force: true })
        this.common.btnConfirmAgainHintPopup("Confirm").click({ force: true })
        this.commonMD.msgSuccess().should("be.visible")
        this.header.btnAtHeader("confirm").click({ force: true })
        this.common.btnConfirmAgainHintPopup("Confirm").click({ force: true })
        this.commonMD.msgSuccess().should("be.visible")
        //check success
        this.divSuccess().should("be.visible")

        //all editted fields -> disable
        this.ipRouteCode().should('have.attr', 'disabled', 'disabled')
        this.common.textField("Enter Actual Total Number of Packages Received").should('have.attr', 'disabled', 'disabled')
        this.common.textField("Enter Actual Total Number of Packages Paid").should('have.attr', 'disabled', 'disabled')
        this.commonMD.ipSelectBox("Select Unit").first().should('have.attr', 'disabled', 'disabled')
        this.commonMD.ipSelectBox("Select Unit").first().last().should('have.attr', 'disabled', 'disabled')
        this.ipDeleteLicesePlateNumber().scrollIntoView().should('have.attr', 'disabled', 'disabled')
        this.ipReason().should('have.attr', 'disabled', 'disabled')
        this.ddlPaidByVehicleType().scrollIntoView().should('have.attr', 'disabled', 'disabled')
    }
}

export default DispatchOrderTracking;