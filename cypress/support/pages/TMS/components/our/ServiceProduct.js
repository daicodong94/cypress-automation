import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import Common from "../common/Common"

class ServiceProduct {

    //CONSTRUCTOR
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //XPATH
    txtNameProd() {
        return cy.xpath("//input[@data-vv-name='name']")
    }

    txtCodeProd() {
        return cy.xpath("//input[@data-vv-name='code']")
    }

    cboAttr() {
        return cy.xpath("//input[@placeholder='please choose']")
    }

    lblAttr(attr) {
        return cy.xpath("//li//span[normalize-space()='" + attr + "']")
    }

    txtShortNameProd() {
        return cy.xpath("//input[@data-vv-name='short name']")
    }

    txtRatioProd() {
        return cy.xpath("//input[@data-vv-name='ratio']")
    }

    btnClosePopupCreateNewProd() {
        return cy.xpath("//div[@aria-label='create new product']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnSaveNewProd() {
        return cy.xpath("//button[@class='el-button common-button-style el-button--primary el-button--mini']")
    }

    tblServiceItem() {
        return cy.xpath("//div[@class='common-right-content']//td[1]//div")
    }

    tblName() {
        return cy.xpath("//td[2]//div")
    }

    tblShortName() {
        return cy.xpath("//td[3]//div")
    }

    tblStatus() {
        return cy.xpath("//td[4]//div")
    }

    tblCode() {
        return cy.xpath("//td[5]//div")
    }

    tblAttr() {
        return cy.xpath("//td[6]//div")
    }

    tblVolumn() {
        return cy.xpath("//td[7]//div")
    }

    tblLayer() {
        return cy.xpath("//td[9]//div")
    }

    tblOperate() {
        return cy.xpath("//td[12]//div//span")
    }

    btnOperate(action) {
        return cy.xpath("//button//span[contains(text(),'" + action + "')]")
    }

    ipNewName() {
        return cy.xpath("//div[@class='common-right-content']//input[@data-vv-name='projectName']")
    }

    ipNewCode() {
        return cy.xpath("//div[@class='common-right-content']//input[@data-vv-name='projectNo']")
    }

    cboNewAttr() {
        return cy.xpath("//div[@class='common-right-content']//div[@class='el-input el-input--mini el-input--suffix']//input[@placeholder='please choose']")
    }

    ipNewShortName() {
        return cy.xpath("//div[@class='common-right-content']//input[@data-vv-name='projectShortName']")
    }

    ipNewRatio() {
        return cy.xpath("//div[@class='common-right-content']//input[@data-vv-name='ratio']")
    }

    cboServiceItem() {
        return cy.xpath("//input[@placeholder='add service item']")
    }

    lblServiceItem(item) {
        return cy.xpath("//li//span[normalize-space()='" + item + "']")
    }

    btnSaveEdit() {
        return cy.xpath("//div[@class='common-right-toolbar']//button//span[.='save']")
    }

    lblBasicInformation() {
        return cy.xpath("//div[@id='tab-baseInfo']")
    }

    btnCreateSubProd() {
        return cy.xpath("//button[@type='button']//span[contains(text(),'create a new sub-product')]")
    }

    nameChildProd() {
        return cy.xpath("(//input[@placeholder='please enter'])[5]")
    }

    nameChildCode() {
        return cy.xpath("(//input[@placeholder='please enter'])[6]")
    }

    nameChildShortName() {
        return cy.xpath("(//input[@placeholder='please enter'])[7]")
    }

    nameChildRatio() {
        return cy.xpath("(//input[@placeholder='please enter'])[8]")
    }

    lblNameProd() {
        return cy.xpath("//span[@class='fl']")
    }

    btnSaveAddChildProd() {
        return cy.xpath("//div[@aria-label='create a new sub-product']//div[@class='el-dialog__footer']//button[@type='button']")
    }

    body() {
        return cy.xpath("//div[@class='common-body-tms']")
    }

    btnDeleleService() {
        return cy.xpath("//div[@class='common-right-content']//td//div//span[contains(text(),'delete')]")
    }

    //SCRIPTS
    addNewProd(name, code, attr, shortName, ratio) {
        this.header.btnAtHeader("new").click({ force: true })
        cy.wait(1000)
        this.txtNameProd().type(name)
        cy.wait(1000)
        this.txtCodeProd().type(code)
        cy.wait(1000)
        this.cboAttr().click({ force: true })
        cy.wait(1000)
        this.lblAttr(attr).click({ multiple: true, force: true })
        cy.wait(1000)
        this.txtShortNameProd().type(shortName)
        cy.wait(1000)
        this.txtRatioProd().type(ratio)
        cy.wait(1000)
        this.btnSaveNewProd().click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }

    addNewProd2(name, code, attr, shortName, ratio) {
        this.header.btnAtHeader("new").click()
        cy.wait(3000/2)
        this.txtNameProd().clear()
        this.txtNameProd().type(name)
        cy.wait(1000)
        this.txtCodeProd().clear()
        this.txtCodeProd().type(code)
        cy.wait(1000)
        this.cboAttr().click()
        cy.wait(1000)
        this.lblAttr(attr).click({ multiple: true, force: true })
        cy.wait(1000)
        this.txtShortNameProd().clear()
        this.txtShortNameProd().type(shortName)
        cy.wait(1000)
        this.txtRatioProd().clear()
        this.txtRatioProd().type(ratio)
        cy.wait(1000)
        this.btnSaveNewProd().click()
    }

    searchProd(prod) {
        this.header.ipSearchBox("product name\\code").click()
        cy.wait(1000)
        this.header.ipSearchBox("product name\\code").clear()
        this.header.ipSearchBox("product name\\code").type(prod, { force: true })
        cy.wait(1000)
        this.header.btnAtHeader("inquire").click({ force: true })
    }

    verifyAddNewProd(name, code, attr, shortName, ratio, layer) {
        this.common.msgSuccess().should('be.visible')
        cy.wait(3000/2)
        this.tblName().first().should('contain', name)
        cy.wait(1000)
        this.tblShortName().first().should('contain', shortName)
        cy.wait(1000)
        this.tblStatus().first().should('contain', 'created')
        cy.wait(1000)
        this.tblCode().first().should('contain', code)
        cy.wait(1000)
        this.tblAttr().first().should('contain', attr)
        cy.wait(1000)
        this.tblVolumn().first().should('contain', ratio)
        cy.wait(1000)
        this.tblLayer().first().should('contain', layer)
        cy.wait(1000)
    }

    deleteProd(prod) {
        this.searchProd(prod)
        this.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(1000)
        this.btnOperate("delete").first().click({ multiple: true, force: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    deleteNewProd(name, code, attr, shortName, ratio) {
        this.addNewProd(name, code, attr, shortName, ratio)
        cy.wait(3000/2)
        this.btnOperate("delete").first().click({ multiple: true, force: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    verifyDeleteNewProd(name, code, attr, shortName, ratio) {
        this.common.msgSuccess().should('be.visible')
        cy.wait(3000/2)
        this.tblName().first().should('not.equal', name)
        cy.wait(1000)
        this.tblShortName().first().should('not.equal', shortName)
        cy.wait(1000)
        this.tblStatus().first().should('not.equal', 'created')
        cy.wait(1000)
        this.tblCode().first().should('not.equal', code)
        cy.wait(1000)
        this.tblAttr().first().should('not.equal', attr)
        cy.wait(1000)
        this.tblVolumn().first().should('not.equal', ratio)
        cy.wait(1000)
        this.tblLayer().first().should('not.equal', '0')
        cy.wait(1000)
    }

    enableProd(name, code, attr, shortName, ratio) {
        this.addNewProd(name, code, attr, shortName, ratio)
        this.common.msgSuccess().should('be.visible')
        this.searchProd(name)
        this.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(1000)
        this.btnOperate("enable").last().click({force : true})
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    pauseProd(name, code, attr, shortName, ratio) {
        this.addNewProd(name, code, attr, shortName, ratio)
        // cy.wait(3000/2)
        this.btnOperate("enable").first().click({ multiple: true, force: true })
        this.common.btnConfirmHintPopUp("confirm").click()
        // cy.wait(3000/2)
        this.common.msgSuccess().should("be.visible")
        this.btnOperate("pause").first().click({ multiple: true, force: true })
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    verifyOperateProd(prod, ope) {
        this.common.msgSuccess().should('be.visible')
        this.searchProd(prod)
        this.tblOperate().should('contain', ope)
    }

    verifyInquire(name, code, attr, shortName, ratio) {
        this.addNewProd(name, code, attr, shortName, ratio)
        cy.wait(3000/2)
        this.searchProd(name)
        cy.wait(3000/2)
        this.tblName().should('contain', name)
        cy.wait(1000)
        this.tblShortName().should('contain', shortName)
        cy.wait(1000)
        this.tblStatus().should('contain', 'created')
        cy.wait(1000)
        this.tblCode().should('contain', code)
        cy.wait(1000)
        this.tblAttr().should('contain', attr)
        cy.wait(1000)
        this.tblVolumn().should('contain', ratio)
        cy.wait(1000)
        this.tblLayer().should('contain', '0')
        cy.wait(1000)
    }

    editProd(name, code, attr, shortName, ratio, newName, newCode, newAttr, newShortName, newRatio) {
        this.addNewProd(name, code, attr, shortName, ratio)
        cy.wait(3000/2)
        this.searchProd(name)
        cy.wait(3000/2)
        this.common.openDetailView()
        cy.wait(1000)
        this.lblBasicInformation().click()
        this.ipNewName().click()
        this.ipNewName().clear()
        this.ipNewName().type(newName)
        cy.wait(1000)
        this.ipNewCode().click()
        this.ipNewCode().clear()
        this.ipNewCode().type(newCode)
        cy.wait(1000)
        this.cboNewAttr().click()
        this.lblAttr(newAttr).click({ multiple: true, force: true })
        cy.wait(1000)
        this.ipNewShortName().click()
        this.ipNewShortName().clear()
        this.ipNewShortName().type(newShortName)
        cy.wait(1000)
        this.ipNewRatio().click()
        this.ipNewRatio().clear()
        this.ipNewRatio().type(newRatio)
        cy.wait(1000)
        this.btnSaveEdit().click({ multiple: true, force: true })
    }

    verifyEditProd(name, code, attr, shortName, ratio, newName, newCode, newAttr, newShortName, newRatio) {
        this.tblName().should('contain', newName)
        this.tblName().should('not.equal', name)
        cy.wait(1000)
        this.tblShortName().should('contain', newShortName)
        this.tblShortName().should('not.equal', shortName)
        cy.wait(1000)
        this.tblCode().should('contain', newCode)
        this.tblCode().should('not.equal', code)
        cy.wait(1000)
        this.tblAttr().should('contain', newAttr)
        this.tblAttr().should('not.equal', attr)
        cy.wait(1000)
        this.tblVolumn().should('contain', newRatio)
        this.tblVolumn().should('not.equal', ratio)
        cy.wait(1000)
    }

    addChildProd(name, code, attr, shortName, ratio, childName, childCode, childShortName, childRatio) {
        this.enableProd(name, code, attr, shortName, ratio)
        cy.wait(3000/2)
        this.common.openDetailView()
        cy.wait(1000)
        this.btnCreateSubProd().click({ multiple: true, force: true })
        cy.wait(3000/2)
        this.nameChildProd().type(childName)
        cy.wait(1000)
        this.nameChildCode().type(childCode)
        cy.wait(1000)
        this.nameChildShortName().type(childShortName)
        cy.wait(1000)
        this.nameChildRatio().type(childRatio)
        cy.wait(1000)
        this.lblNameProd().should('contain', 'superior product：' + name)
        cy.wait(1000)
        this.btnSaveAddChildProd().click({ multiple: true, force: true })
        this.body().dblclick()
    }

    addServiceEntryToProd(item) {
        cy.wait(3000/2)
        this.common.openDetailView()
        cy.wait(1000)
        this.btnDeleleService().then(($el) => {
            this.btnDeleleService().click({ multiple: true, force: true })
            for (var i = 0; i < $el.length; i++) {
                this.common.btnConfirmHintPopUp("confirm").click({ multiple: true, force: true })
                cy.wait(1000)
            }
        })
        cy.wait(3000/2)
        this.cboServiceItem().click()
        this.lblServiceItem(item).click()
        this.tblServiceItem().should('contain', item)
    }

    addServiceEntryToProdV2(item) {
        this.common.openDetailView()
        // this.btnDeleleService().then(($el)=>{
        //     this.btnDeleleService().click({multiple : true, force : true})
        //     for(var i = 0; i < $el.length; i++){
        //         this.common.btnConfirmHintPopUp("confirm").click({multiple : true, force : true})
        //         cy.wait(1000)
        //     }
        // })
        // cy.wait(3000/2)
        this.cboServiceItem().click({force: true})
        cy.wait(1000)
        this.lblServiceItem(item).click({force: true})
        this.tblServiceItem().should('contain', item)
    }

    verifyServiceItem(item) {
        cy.wait(3000/2)
        this.common.openDetailView()
        cy.wait(3000/2)
        this.cboServiceItem().click()
        this.lblServiceItem(item).should('to.exist')
    }

    verifyDeleteServiceItem() {
        cy.wait(3000/2)
        this.common.openDetailView()
        cy.wait(1000)
        this.tblServiceItem().first().then(($text) => {
            this.btnDeleleService().first().click({ force: true })
            this.common.btnConfirmHintPopUp("confirm").click()
            this.common.msgSuccess().should('be.visible')
            cy.wait(3000/2)
            this.tblServiceItem().should('not.equal', $text.text().trim())
        })
    }
}

export default ServiceProduct