import CommonMasterData from "../../../support/pages/MasterData/components/common/CommonMasterData"
import Vehicle from "../../../support/pages/MasterData/components/vehicle/Vehicle"
import VehicleList from "../../../support/pages/MasterData/components/vehicle/VehicleList"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import Login from "../../../support/pages/TMS/components/login/Login"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import { Utility } from "../../../support/utility"

describe("MasterData - Vehicle - List", () => {
    var data
    var objHome = new Homepage(), objLogin, objLoginPRD, objVehicleList = new VehicleList()
    var id = "ANL-V000000000000242"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objVehicleList = objHome.leftMenu.navigateToSubPage("Master Data", "Vehicle")
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it.skip("TC_4_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abcdef' \n" +
        "TC_5_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abc'  \n" +
        "TC_6_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' a'  \n" +
        "TC_7_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' number' \n" +
        "TC_8_Check results when inputting keywords not include data of  License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site \n" +
        "TC_9_Check results when the system hasn't data match with keywords \n" +
        "TC_10_Check delete inputted keywords \n" +
        "TC_11_Check 'Search' box when don't input any keywords \n" +
        "TC_12_Check press 'Enter' from keyboard to search \n" +
        "TC_13_Check search when user is on page different page 1 \n" +
        "TC_14_Check click on 'Filter' button \n" +
        "TC_18_Check click on 'filter' button on second time \n" +
        "TC_9_Check results when the system hasn't data match with keywords \n" +
        "TC_8_Check results when inputting keywords not include data of  License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site", () => {
            //TC_4_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abcdef" 
            objVehicleList.addVehicle.searchByCondition(" ANL-V000000000000240")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_5_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abc"  
            objVehicleList.addVehicle.searchByCondition(" 000000000240")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_6_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " a"  
            objVehicleList.addVehicle.searchByCondition(" A")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_7_Check results search by [ License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " number"
            objVehicleList.addVehicle.searchByCondition(" 240")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_10_Check delete inputted keywords
            objVehicleList.addVehicle.clearSearchInputted()
            //TC_11_Check "Search" box when don't input any keywords
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_12_Check press "Enter" from keyboard to search
            objVehicleList.header.txtSearch().first().click({ force: true }).type("000000000240", { force: true })
            cy.wait(2000)
            objVehicleList.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleList.common.searchResultShouldContain("000000000240")
            //TC_13_Check search when user is on page different page 1
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.goToPageNumberWhenClickbtnJumpPage(3)
            objVehicleList.addVehicle.searchByCondition("ANL")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_14_Check click on "Filter" button
            objVehicleList.verifyFilterUI()
            //TC_18_Check click on "filter" button on second time
            objVehicleList.btnAction("Filter").click()
            objVehicleList.afterClickFilter().should("not.exist")
            //TC_8_Check results when inputting keywords not include data of  License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site 
            objVehicleList.addVehicle.searchByCondition("3454645611112")
            objVehicleList.common.divNoData().should("be.visible")
            //TC_9_Check results when the system hasn't data match with keywords
            objVehicleList.addVehicle.searchByCondition("#$%#$^$%^$")
            objVehicleList.common.divNoData().should("be.visible")
        })

    it("TC_22_Check click on any number page \n" +
        "TC_23_Check click on '<' icon \n" +
        "TC_23_Check click on '>' icon \n" +
        "TC_25_Check amount of Vehicle displaying in the one page \n" +
        "TC_26_Check input number of page into 'Go to' field when system has many page \n" +
        "C_34_Check 'set status' popup is displayed \n" +
        "TC_35_Check tick on 'Select date/time' radio button on 'set status' popup", () => {
            //TC_22_Check click on any number page
            objVehicleList.common.gotoPageByClickNumber(2)
            //TC_23_Check click on "<" icon
            objVehicleList.common.gotoPageByClickNumber(1)
            objVehicleList.common.btnJumpPagePrev().should("have.attr", "disabled", "disabled")
            //TC_24_Check click on "<" icon
            objVehicleList.common.gotoPageByClickNumber(2)
            objVehicleList.common.btnJumpPagePrev().click()
            objVehicleList.common.pageActive().contains('1')
            //TC_25_Check amount of Vehicle displaying in the one page 
            objVehicleList.common.verifyTypeOfPageSize()
            //TC_26_Check input number of page into "Go to" field when system has many page
            objVehicleList.common.goToPageNumber(2)
            //TC_34_Check "set status" popup is displayed
            objVehicleList.common.goToPageNumber(1)
            objVehicleList.filterData("Vehicle Status", "Active")
            objVehicleList.common.clickIconSetStatus()
            //TC_35_Check tick on "Select date/time" radio button on "set status" popup
            objVehicleList.addVehicle.checkRadioEndDateTime()
        })

    it("TC_46_Check click on the 'Cancel' button when don't have any change \n" +
        "TC_45_Check click on the 'Cancel' button when changed some fields", () => {
            //TC_46	Check click on the "Cancel" button when don't have any change
            objVehicleList.filterData("Vehicle Status", "Active")
            objVehicleList.common.clickIconSetStatus()
            objVehicleList.common.btnConfirmSetStatus("Cancel").click()
            //TC_45	Check click on the "Cancel" button when changed some fields
            objVehicleList.filterData("Vehicle Status", "Active")
            objVehicleList.common.clickIconSetStatus()
            objVehicleList.addVehicle.verifyCancelSetStatus()
            objVehicleList.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")

        })

    it("TC_58_Check search on 'View history log' screen \n" +
        "TC_59_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abcdef' on 'View history log' screen \n" +
        "TC_60_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abc'  on 'View history log' screen \n" +
        "TC_61_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' a'  on 'View history log' screen \n" +
        "TC_62_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' number'on 'View history log' screen \n" +
        "TC_63_Check results when inputting keywords not include data of License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site on 'View history log' screen \n" +
        "TC_64_Check results when the system hasn't data match with keywords on 'View history log' screen \n" +
        "TC_65_Check delete inputted keywords on the 'View history log' screen \n" +
        "TC_66_Check 'Search' box when don't input any keywords on the 'View history log' screen \n" +
        "TC_67_Check press 'Enter' from keyboard to search on the 'View history log' screen \n" +
        "TC_68_Check search when user is on page different page 1 on the 'View history log' screen \n" +
        "TC_74_Check click on ' > ' icon on the 'View history log' screen \n" +
        "TC_75_Check click on ' < ' icon on the 'View history log' screen \n" +
        "TC_76_Check amount of Vehicle displaying in the one page on the 'View history log' screen \n" +
        "TC_77_Check input number of page into 'Go to' field when system has many page on the 'View history log' screen", () => {
            //TC_58_Check search on "View history log" screen
            objVehicleList.common.navigateToViewHistoryLogScreen()
            objVehicleList.addVehicle.searchByCondition("ANL-")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_59_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abcdef" on "View history log" screen
            objVehicleList.addVehicle.searchByCondition(" ANL-V000000000000205")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_60_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abc"  on "View history log" screen
            objVehicleList.addVehicle.searchByCondition(" 000000205")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_61_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " a"  on "View history log" screen
            objVehicleList.addVehicle.searchByCondition(" A")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_62_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " number"on "View history log" screen
            objVehicleList.addVehicle.searchByCondition(" 205")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_63_Check results when inputting keywords not include data of License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site on "View history log" screen
            objVehicleList.addVehicle.searchByCondition("13546510415146")
            objVehicleList.common.divNoData().should("be.visible")
            //TC_64_Check results when the system hasn't data match with keywords on "View history log" screen
            objVehicleList.addVehicle.searchByCondition("#$%#$^$%^$")
            objVehicleList.common.divNoData().should("be.visible")
            //TC_65_Check delete inputted keywords on the "View history log" screen
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_66_Check "Search" box when don't input any keywords on the "View history log" screen
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_67_Check press "Enter" from keyboard to search on the "View history log" screen
            objVehicleList.header.txtSearch().first().click({ force: true }).type("000000000240", { force: true })
            cy.wait(2000)
            objVehicleList.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleList.common.searchResultShouldContain("000000000240")
            //TC_68_Check search when user is on page different page 1 on the "View history log" screen
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.goToPageNumber(2)
            objVehicleList.addVehicle.searchByCondition("ANL")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_74_Check click on ">" icon on the "View history log" screen
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.goToPageNumber(1)
            objVehicleList.common.btnJumpPagePrev().should("have.attr", "disabled", "disabled")
            //TC_75_Check click on "<" icon on the "View history log" screen
            objVehicleList.common.goToPageNumber(2)
            objVehicleList.common.btnJumpPagePrev().click()
            objVehicleList.common.pageActive().contains('1')
            //TC_76_Check amount of Vehicle displaying in the one page on the "View history log" screenTC_77_Check input number of page into "Go to" field when system has many page on the "View history log" screen
            objVehicleList.common.verifyTypeOfPageSize()
            //TC_77_Check input number of page into "Go to" field when system has many page on the "View history log" screen
            objVehicleList.common.goToPageNumber(2)
        })

    it.skip("Create history log", () => {
        objVehicleList.createHistoryLog(id)
    })

    it("TC_85_Check search on 'View history log' screen (on the 'Detail Vehicle' screen) \n" +
        "TC_86_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abcdef' (on the 'Detail Vehicle' screen) \n" +
        "TC_87_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' abc'  (on the 'Detail Vehicle' screen) \n" +
        "TC_88_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' a'  (on the 'Detail Vehicle' screen) \n" +
        "TC_89_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including ' number' (on the 'Detail Vehicle' screen) \n" +
        "TC_90_Check results when inputting keywords not include data of License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site (on the 'Detail Vehicle' screen) \n" +
        "TC_91_Check results when the system hasn't data match with keywords (on the 'Detail Vehicle' screen) \n" +
        "TC_92_Check delete inputted keywords (on the 'Detail Vehicle' screen) \n" +
        "TC_93_Check 'Search' box when don't input any keywords (on the 'Detail Vehicle' screen) \n" +
        "TC_94_Check press 'Enter' from keyboard to search (on the 'Detail Vehicle' screen) \n" +
        "TC_95_Check search when user is on page different page 1 (on the 'Detail Vehicle' screen) \n" +
        "TC_98_Check click on any number page if the system has 7 page (on the 'Detail Vehicle' screen) \n" +
        "TC_102_Check click on ' < ' icon (on the 'Detail Vehicle' screen) \n" +
        "TC_103_Check amount of Vehicle displaying in the one page  (on the 'Detail Vehicle' screen) \n" +
        "TC_104_Check input number of page into 'Go to' field when system has many page (on the 'Detail Vehicle' screen)", () => {
            //TC_85	Check search on "View history log" screen (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition(id)
            objVehicleList.common.searchResultWithOutValue().dblclick({ force: true })
            cy.wait(2000)
            objVehicleList.common.navigateToViewHistoryLogScreen()
            objVehicleList.addVehicle.searchByCondition("ANL-")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_86_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abcdef" (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition(id)
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_87_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " abc"  (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition(" ANL-V000000")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_88_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " a"  (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition(" A")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_89_Check results search by [License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site] when the system has data match with keywords including " number" (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition(" 000000002")
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_90_Check results when inputting keywords not include data of License Plate Number, Provincial Sign, Company Name, Vehicle ID , Site (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition("13546510415146")
            objVehicleList.common.divNoData().should("be.visible")
            //TC_91_Check results when the system hasn't data match with keywords (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.searchByCondition("#$%#$^$%^$")
            objVehicleList.common.divNoData().should("be.visible")
            //TC_92_Check delete inputted keywords (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_93_Check "Search" box when don't input any keywords (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_94_Check press "Enter" from keyboard to search (on the "Detail Vehicle" screen)
            objVehicleList.header.txtSearch().first().click({ force: true }).type("0000000002", { force: true })
            cy.wait(2000)
            objVehicleList.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleList.common.searchResultShouldContain("0000000002")
            //TC_95_Check search when user is on page different page 1 (on the "Detail Vehicle" screen)
            objVehicleList.addVehicle.clearSearchInputted()
            objVehicleList.addVehicle.btnSearch().click()
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            objVehicleList.common.goToPageNumber(3)
            objVehicleList.addVehicle.searchByCondition(id)
            objVehicleList.common.searchResultWithOutValue().should("be.visible")
            //TC_98_Check click on any number page if the system has 7 page (on the "Detail Vehicle" screen)
            objVehicleList.common.goToPageNumber(6)
            //TC_101	Check click on ">" icon (on the "Detail Vehicle" screen)
            objVehicleList.common.goToPageNumber(1)
            objVehicleList.common.btnJumpPagePrev().should("have.attr", "disabled", "disabled")
            //TC_102_Check click on "<" icon (on the "Detail Vehicle" screen)
            objVehicleList.common.goToPageNumber(2)
            objVehicleList.common.btnJumpPagePrev().click()
            objVehicleList.common.pageActive().contains('1')
            //TC_103_Check amount of Vehicle displaying in the one page  (on the "Detail Vehicle" screen)
            objVehicleList.common.verifyTypeOfPageSize()
            //TC_104_Check input number of page into "Go to" field when system has many page (on the "Detail Vehicle" screen)
            objVehicleList.common.goToPageNumber(3)

        })

    it("TC_105_Check input number of page into 'Go to' field when system has only 1 page (on the 'Detail Vehicle' screen)", () => {
        //TC_105_Check input number of page into "Go to" field when system has only 1 page (on the "Detail Vehicle" screen)
        objVehicleList.common.searchResultWithOutValue().dblclick({ force: true })
        cy.wait(2000)
        objVehicleList.common.navigateToViewHistoryLogScreen()
        objVehicleList.goToPageNumber(3)
        objVehicleList.common.pageActive().contains('1')
    })

    it("TC_110_Check UI of 'Import' screen \n" +
        "TC_121_Check click on 'Download template' button \n" +
        "TC_126_Import only file that different template file \n" +
        "TC_127_Import only file that have empty data \n" +
        "TC_143_Import file over 5MB \n" +
        "TC_144_Import file wrong format (except .xls, .xlsx file)", () => {
            //TC_110_Check UI of 'Import' screen 
            objVehicleList.common.openImportPopUp("Import Vehicle")
            //TC_121_Check click on "Download template" button
            objVehicleList.common.verifyDownload("Download Template")
            //TC_126_Import only file that different template file
            var filePath = "masterdataSite\\Template.xlsx"
            objVehicleList.common.importFileNotMatchedTemplate(filePath, "The selected file is in the invalid format, please check your file.")
            objVehicleList.common.cancelImportFile("yes")
            //TC_127_Import only file that have empty data
            objVehicleList.common.openImportPopUp("Import Vehicle")
            filePath = "masterdataSite\\Empty.xlsx"
            objVehicleList.common.importFileNotMatchedTemplate(filePath, "The selected file is empty, please check your file")
            objVehicleList.common.cancelImportFile("yes")
            //TC_143_Import file over 5MB
            objVehicleList.common.openImportPopUp("Import Vehicle")
            filePath = "masterdataSite\\5GBTemplate.xls"
            objVehicleList.common.attachImportFile(filePath)
            objVehicleList.msgErrorShouldHaveText("File size is exceed the allowable limit of 5 MB")
            objVehicleList.common.btnCancel().click()
            //TC_144_Import file wrong format (except .xls, .xlsx file)
            objVehicleList.common.openImportPopUp("Import Vehicle")
            filePath = "implement\\images.png"
            objVehicleList.common.attachImportFile(filePath)
            objVehicleList.msgErrorShouldHaveText("The selected file is not a support file type, please check your file.")
        })

    it("TC_145_Check click on 'Export file' button when the system exists data \n" +
        "TC_148_Check click on any item in 'List' screen", () => {
            //TC_145_Check click on "Export file" button when the system exists data
            objVehicleList.common.verifyDownload("Export")
            //TC_148_Check click on any item in "List" screen
            objVehicleList.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicleList.addVehicle.verifyDefaultUI()
        })
})