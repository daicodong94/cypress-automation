import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import ServiceProduct from "../../../support/pages/TMS/components/our/ServiceProduct";
import { Utility } from "../../../support/utility"

describe("Checking Our - Service product", () => {

    var objServiceproduct = new ServiceProduct(), data, objLogin, objHome, objShippingOrder, objCreateShippingOrder

    var code2 = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 10)
    
    beforeEach("TMS-Our-Service product", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(()=>{
        cy.wait(10000)
        objHome.logout()
    })

    it("TC_212: Check add to add  a new product", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        objServiceproduct.verifyAddNewProd("Cyber", code, "customer", "C1", "50", "0")
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_213: Check detele success of service product's", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.deleteNewProd("Cyber", code, "customer", "C1", "50")
        objServiceproduct.verifyDeleteNewProd("Cyber", code, "customer", "C1", "50")
    })

    it("TC_214: Check enable success of service product's", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.randomString(5).then((name) => {
            cy.randomString(2).then((shortName) => {
                objServiceproduct.enableProd(name + "prod_auto", code, "customer", shortName, 50)
                objServiceproduct.verifyOperateProd(name, "pause")
            })
        })
    })

    it("TC_214: Check timeout success of service product's", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.randomString(5).then((name) => {
            cy.randomString(2).then((shortName) => {
                objServiceproduct.pauseProd(name + "prod_auto", code, "customer", shortName, 50)
                objServiceproduct.verifyOperateProd(name, "enable")
            })
        })
    })

    it("TC_215: Check inquire value of service product's", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.verifyInquire("Cyber", code, "customer", "C1", "50")
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_216: Check edit information of service product", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.editProd("Cyber", code, "customer", "C1", "50", "Train_auto", code2, "subcontractor", "TA", "100")
        objServiceproduct.searchProd("Train_auto")
        objServiceproduct.verifyEditProd("Cyber", code, "customer", "C1", "50", "Train_auto", code2, "subcontractor", "TA", "100")
        objServiceproduct.deleteProd("Train_auto")
    })

    it("TC_217: Check inquire value of service product's", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.randomString(5).then((name) => {
            cy.randomString(2).then((shortName) => {
                objServiceproduct.addChildProd(name + "prod_auto", code, "customer", shortName, "50", "Cyber", code2, "C1", "100")
                objServiceproduct.verifyAddNewProd("Cyber", code2, "customer", "C1", "100", "1")
                objServiceproduct.deleteProd("Cyber")
                objServiceproduct.searchProd(name)
                objServiceproduct.btnOperate("pause").first().click({multiple : true, force :  true})
                objServiceproduct.common.btnConfirmHintPopUp("confirm").click()
            })
        })
    })

    it("TC_218: Check add a service entry to product", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        objServiceproduct.addServiceEntryToProd("ServiceItem_Item_Auto")
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_219: Check delete service entry from product", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        objServiceproduct.verifyDeleteServiceItem()
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_220: Check the effect to Shipping Order screen when creating new record at Service Product page ", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.randomString(5).then((name) => {
            cy.randomString(2).then((shortName) => {
                objServiceproduct.enableProd(name + "prod_auto", code, "customer", shortName, 50)
                cy.reload()
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                // cy.wait(30000)
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                // cy.wait(30000)
                objCreateShippingOrder.chooseValueFromDropdownShipper("Automation Company", name + "prod_auto")
            })
        })
    })

    it("TC_221: Check all required field when creating the new Service product - Name", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        cy.wait(5000)
        objServiceproduct.addNewProd2("Cyber", code, "customer", "C1", "50")
        objServiceproduct.common.txtError().should("contain", "Invalid parameter.Duplicate project name [Cyber]")
        objServiceproduct.btnClosePopupCreateNewProd().click()
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_221: Check all required field when creating the new Service product - Code", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        cy.wait(5000)
        objServiceproduct.addNewProd2("Plane", code, "customer", "C1", "50")
        objServiceproduct.common.txtError().should("contain", "Invalid parameter.Duplicate item number [" + code + "]")
        objServiceproduct.btnClosePopupCreateNewProd().click()
        objServiceproduct.deleteProd("Cyber")
    })

    it("TC_221: Check all required field when creating the new Service product - Short Name", () => {
        objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
        cy.wait(2000)
        const code = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceproduct.addNewProd("Cyber", code, "customer", "C1", "50")
        cy.wait(5000)
        objServiceproduct.addNewProd2("Plane", code2, "customer", "C1", "50")
        objServiceproduct.common.txtError().should("contain", "Invalid parameter.Repeat item (C1)")
        objServiceproduct.btnClosePopupCreateNewProd().click()
        objServiceproduct.deleteProd("Cyber")
    })
})