/// <reference types="cypress" />

import Login from "../../../support/pages/TMS/components/login/Login"


// Test suite
describe("Login system TMP", () => {
    before(function () {
        cy.fixture('data_test_tmp.json').then(function (data) {
            globalThis.data = data
        })
    })

    // Login system successfully
    it("Login with invalid username and password", () => {
        var login = new Login
        cy.visit(data.url)
        login.inputusername(data.username)
        login.inputpassword(data.password)
        login.clickBtnSubmit()
        cy.wait(5000)
        cy.title().should('eq', 'TMP-2.0项目')
    })

})


