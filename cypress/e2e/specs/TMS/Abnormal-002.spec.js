import Abnormal from "../../../support/pages/TMS/components/abnormal/Abnormal"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import Login from "../../../support/pages/TMS/components/login/Login"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import ShippingOrderTracking from "../../../support/pages/TMS/components/track/ShippingOrderTracking"
import OnTheWay from "../../../support/pages/TMS/components/Implement/OnTheWay"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"

describe("Abnormal02", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(),
        objAbnormal = new Abnormal(), objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objPickup = new Pickup(), objOnTheWay = new OnTheWay(), objSchedule = new Schedule(), objShippingOrderTracking = new ShippingOrderTracking(),
        objActivityTracking = new ActivityTracking()
    var trackNumber, exceptionID, carrier = "SYS_DEF_INDIVIDUAL", driver = "Auto Driver",
        customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it("TC: Verify record with exception type = 'Abnormal car change' from Implement>On the way page", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, customer, product, temperature, siteB, siteD, 0, 5);
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            cy.wait(45000)
            cy.reload()
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(0)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.confirmPickup("yes")
                objOnTheWay = objPickup.leftMenu.navigateToSubPage("Implement", "On the way")
                cy.wait(1000)
                objOnTheWay.common.searchByClickSuggestion("please enter the dispatch number", id)
                cy.wait(1000)
                objOnTheWay.common.openDetailView()
                cy.wait(1000)
                objOnTheWay.changeVehicle("jas mine")
                cy.reload()
                objAbnormal = objOnTheWay.leftMenu.navigateToPage("Abnormal")
                cy.wait(1000)
                objAbnormal.common.searchByClickSuggestion("order number", id)
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("abnormal car change").last().should("be.visible")
            })
        })
    })

    it("TC: Verify record with exception type = 'Order closed' from Track> Shipping order page", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, customer, product, temperature, siteB, "TestT3", 0, 5);
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            objShippingOrderTracking = objShippingOrder.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking")
            objShippingOrderTracking.searchTrackingNumber(id)
            cy.wait(1000)
            objShippingOrderTracking.common.searchResultWithOutValue().first().click({ force: true })
            objShippingOrderTracking.header.btnAtHeader("close").click()
            cy.wait(1000)
            objShippingOrderTracking.common.btnConfirmHintPopUp("confirm").click()
            objShippingOrderTracking.common.msgSuccess().should("be.visible")
            cy.reload()
            objAbnormal = objShippingOrderTracking.leftMenu.navigateToPage("Abnormal")
            cy.wait(1000)
            objAbnormal.common.searchByClickSuggestion("order number", id)
            objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
            objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
            objAbnormal.pickup.dispatchPickupStatus("order closed").last().should("be.visible")
        })
    })

    it("TC: Check input data in Waybill number field and save \n" +
        "TC: Check data display in Shipping Order Number \n" +
        "TC: Check Closure function with State = 'New/ Pending'", () => {
            cy.randomString(8).then((id) => {
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, customer, product, temperature, siteB, siteD, 0, 5);
                trackNumber = id
                objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("dispatch number", id)
                objSchedule.common.searchResultWithOutValue().should("be.visible")
                cy.wait(1000)
                objSchedule.common.openDetailView()
                cy.wait(1000)
                objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
                objSchedule.common.msgSuccess().should("be.visible")
                objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
                objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("dispatch number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

                //open detail view and get Waybill number: this.waybill
                objPickup.common.openDetailView()
                objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill2 = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill2)
                    cy.reload()
                    ////must go to activity tracking -> update status 2 times
                    objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    cy.wait(1000)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    //back to Pick up to pickup completed
                    objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    cy.wait(1000)
                    objPickup.common.searchByClickSuggestion("waybill number", id)
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                    objPickup.common.openDetailView()
                    cy.wait(1000)
                    objPickup.chooseCompletionTime(0)
                    cy.wait(1000)

                    objPickup.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    objPickup.confirmPickup("yes")
                    objAbnormal = objPickup.leftMenu.navigateToPage("Abnormal")
                    cy.wait(1000)
                    //TC: Check input data in Waybill number field and save
                    objAbnormal.header.btnAtHeader("new").click()
                    //////TC: Check data display in Shipping Order Number
                    objAbnormal.lblFieldDataAutoGenerate("task number").should("have.text", "")
                    objAbnormal.inputWaybillnumber(id)
                    objAbnormal.lblFieldDataAutoGenerate("task number").contains("002300")
                    //////
                    objAbnormal.createAbnormal(id, "other", "slight", "now", "save")
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.wait(1000)
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                    objAbnormal.pickup.dispatchPickupStatus("manual").should("be.visible")
                    objAbnormal.pickup.dispatchPickupStatus("other").last().should("be.visible")
                    //////TC: Check Closure function with state = "New/ Pending"
                    objAbnormal.closeAbnormal("new", 1)
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.reload()
                    objAbnormal.leftMenu.navigateToPage("Abnormal")
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("closed").should("be.visible")
                })
            })
        })

    it("TC: Check Reprocess with record has state = Closed/Processed \n" +
        "TC: Check don't input data in Waybill number field and click save \n" +
        "TC: Check input data in Waybill number field and confirm \n" +
        "TC: Check 'save' function with record has state = 'New/Pending' \n" +
        "TC: Check don't input data in Waybill number field and click confirm", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("closed").should("be.visible")
            /////TC: Check Reprocess with record has state = Closed/Processed
            objAbnormal.reProcessAbnormal("closed", 1)
            objAbnormal.common.btnConfirmHintPopUp("confirm").click()
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.reload()
            objAbnormal.leftMenu.navigateToPage("Abnormal")
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
            //TC: Check don't input data in Waybill number field and click save
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.header.btnAtHeader("save").click()
            objAbnormal.common.msgWarning().should("have.text", "The tracking number cannot be empty!")
            //TC: Check input data in Waybill number field and confirm
            objAbnormal.header.btnAtHeader("return").click()
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(trackNumber, "other", "slight", "now", "confirm")
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objAbnormal.pickup.dispatchPickupStatus("pending").should("be.visible")
            objAbnormal.pickup.dispatchPickupStatus("manual").should("be.visible")
            objAbnormal.pickup.dispatchPickupStatus("other").last().should("be.visible")
            //TC: Check 'save' function with record has state = 'New/Pending'
            objAbnormal.editAbnormal("new", "handler", "Johnny", "save")
            objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
            //TC: Check don't input data in Waybill number field and click confirm
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.header.btnAtHeader("confirm").click()
            objAbnormal.common.msgWarning().should("have.text", "The tracking number cannot be empty!")
        })

    it("TC: Check don't select data in Exception type field and save \n" +
        "TC: Check don't select data in Exception type field and confirm", () => {
            cy.randomString(8).then((id) => {
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, customer, product, temperature, siteB, siteD, 0, 5);
                exceptionID = id
                objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("dispatch number", id)
                objSchedule.common.searchResultWithOutValue().should("be.visible")
                cy.wait(1000)
                objSchedule.common.openDetailView()
                cy.wait(1000)
                objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
                objSchedule.common.msgSuccess().should("be.visible")
                objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
                objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("dispatch number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")
                cy.wait(1000)

                //open detail view and get Waybill number: this.waybill
                objPickup.common.openDetailView()
                objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill2 = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill2)
                    cy.reload()
                    ////must go to activity tracking -> update status 2 times
                    objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    cy.wait(1000)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    //back to Pick up to pickup completed
                    objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    cy.wait(1000)
                    objPickup.common.searchByClickSuggestion("waybill number", id)
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                    objPickup.common.openDetailView()
                    cy.wait(1000)
                    objPickup.chooseCompletionTime(0)
                    cy.wait(1000)

                    objPickup.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    objPickup.confirmPickup("yes")
                    objAbnormal = objPickup.leftMenu.navigateToPage("Abnormal")
                    cy.wait(1000)
                    //TC: Check don't select data in Exception type field and save
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "none", "slight", "now", "save")
                    objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
                    //TC: Check don't select data in Exception type field and confirm
                    objAbnormal.header.btnAtHeader("return").click()
                    cy.wait(1000)
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "none", "serious", "now", "confirm")
                    objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
                })
            })
        })

    it("TC: Check don't select data in Exception level field and save \n" +
        "TC: Check don't select data in Exception level field and confirm \n" +
        "TC: Check don't select time in Generation time field and click save \n" +
        "TC: Check don't select time in Generation time field and click confirm", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            //TC: Check don't select data in Exception level field and save
            // objAbnormal.header.btnAtHeader("return").click()
            // cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(exceptionID, "delivery waiting", "none", "now", "save")
            objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
            //TC: Check don't select data in Exception level field and confirm
            objAbnormal.header.btnAtHeader("return").click()
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(exceptionID, "order closed", "none", "now", "confirm")
            objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
            //TC: Check don't select time in Generation time field and click save
            objAbnormal.header.btnAtHeader("return").click()
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(exceptionID, "Pickup waiting", "slight", "none", "save")
            objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
            //TC: Check don't select time in Generation time field and click confirm
            objAbnormal.header.btnAtHeader("return").click()
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(exceptionID, "order closed", "serious", "none", "confirm")
            objAbnormal.common.msgError().should("have.text", "Occurrence time, exception type, and exception level cannot be empty")
        })

    it("TC: Check Return function in New Abnormal screen \n" +
        "TC: Check 'Add attachments' function in New Abnormal screen", () => {
            var filePath = "implement\\images.png"
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.header.btnAtHeader("return").should("be.visible").click()
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.common.addFileAttachments(filePath)
        })

    it("TC: Delete successfully (1 record) \n" +
        "TC: Delete successfully (Multiple  record)", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            cy.wait(1000)
            objAbnormal.deleteAbnormal(1, "new", "manual")
            objAbnormal.common.msgSuccess().should("be.visible")
            objAbnormal.deleteAbnormal(3, "new", "manual")
            objAbnormal.common.msgSuccess().should("be.visible")
        })

    it("TC: Delete failed (Record with source is system) \n" +
        "TC: Delete failed (The status in the column abnormal state is different from the status 'New') \n" +
        "TC: Check Closure function with State = 'Closed/Processed' \n" +
        "TC: Check Reprocess function with State = 'New/Pending'", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            objAbnormal.deleteAbnormal(2, "new", "system")
            objAbnormal.common.msgError().should("have.text", "Unable to continue execution.Only manually created exceptions can be deleted.")
            cy.reload()
            objAbnormal.leftMenu.navigateToPage("Abnormal")
            objAbnormal.deleteAbnormal(2, "pending", "manual")
            objAbnormal.common.msgError().should("be.visible")
            cy.reload()
            objAbnormal.leftMenu.navigateToPage("Abnormal")
            objAbnormal.closeAbnormal("closed", 1)
            objAbnormal.common.msgError().invoke("text").then((text) => {
                expect(text).to.eq("Close button, can only operate exceptions in new state and pending state.")
            })
            // objAbnormal.common.msgError().should("have.text", "Unable to continue execution.Only exceptions in the waiting state can be deleted.")
            objAbnormal.closeAbnormal("processed", 1)
            objAbnormal.common.msgError().invoke("text").then((text) => {
                expect(text).to.eq("Close button, can only operate exceptions in new state and pending state.")
            })
            // objAbnormal.common.msgError().should("have.text", "Unable to continue execution.Only exceptions in the waiting state can be deleted.")
            cy.wait(1000)
            objAbnormal.reProcessAbnormal("new", 1)
            objAbnormal.common.msgError().should("have.text", "Status must be Processed or Closed to be reprocessed.")
            objAbnormal.reProcessAbnormal("pending", 1)
            objAbnormal.common.msgError().should("have.text", "Status must be Processed or Closed to be reprocessed.")
        })

    it("TC: Check 'Reprocess' function with record has state = 'Processed/Closed' \n" +
        "TC: Check 'Confirm' function with record has state = 'New' \n" +
        "TC: Check 'Processing completed' function with record has state = 'Pending'", () => {
            cy.randomString(8).then((id) => {
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, customer, product, temperature, siteB, siteD, 0, 5);
                objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("dispatch number", id)
                objSchedule.common.searchResultWithOutValue().should("be.visible")
                cy.wait(1000)
                objSchedule.common.openDetailView()
                cy.wait(1000)
                objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
                objSchedule.common.msgSuccess().should("be.visible")
                objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
                objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("dispatch number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

                //open detail view and get Waybill number: this.waybill
                objPickup.common.openDetailView()
                objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill2 = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill2)
                    cy.reload()
                    ////must go to activity tracking -> update status 2 times
                    objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    cy.wait(1000)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    //back to Pick up to pickup completed
                    objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    cy.wait(1000)
                    objPickup.common.searchByClickSuggestion("waybill number", id)
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                    objPickup.common.openDetailView()
                    cy.wait(1000)
                    objPickup.chooseCompletionTime(0)

                    cy.wait(1000)
                    objPickup.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    objPickup.confirmPickup("yes")
                    objAbnormal = objPickup.leftMenu.navigateToPage("Abnormal")
                    objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
                    cy.wait(1000)
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "other", "slight", "now", "save")
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.wait(1000)
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                    objAbnormal.pickup.dispatchPickupStatus("manual").should("be.visible")
                    objAbnormal.pickup.dispatchPickupStatus("other").last().should("be.visible")
                    cy.wait(1000)
                    //TC: Check 'Confirm' function with record has state = 'New'
                    objAbnormal.common.searchResultWithOutValue().first().dblclick({ force: true })
                    objAbnormal.header.btnAtHeader("confirm").click()
                    objAbnormal.common.msgSuccess().should("be.visible")
                    objAbnormal.common.searchBySelectValue("abnormal state", "pending")
                    objAbnormal.pickup.dispatchPickupStatus("pending").should("be.visible")
                    //TC: Check 'Processing completed' function with record has state = 'Pending'
                    objAbnormal.common.searchResultWithOutValue().first().dblclick({ force: true })
                    objAbnormal.ipDefinitionOfResponsibility("Responsible party").click()
                    objAbnormal.common.lblElement("other side").last().click()
                    objAbnormal.header.ipSearchBox("Abnormal").last().click({ force: true })
                    cy.wait(1000)
                    objAbnormal.liSelectExceptionInformation().click({ force: true })
                    objAbnormal.header.btnAtHeader("processing completed").click()
                    objAbnormal.common.msgSuccess().should("be.visible")
                    objAbnormal.common.searchBySelectValue("abnormal state", "processed")
                    objAbnormal.pickup.dispatchPickupStatus("processed").should("be.visible")
                    //TC: Check 'Reprocess' function with record has state = 'Processed/Closed'
                    objAbnormal.common.searchResultWithOutValue().first().dblclick({ force: true })
                    objAbnormal.btnEditInside("reprocess").click()
                    objAbnormal.common.msgSuccess().should("be.visible")
                    objAbnormal.common.searchBySelectValue("abnormal state", "new")
                    objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                })
            })
        })
})