import Login from "../../../support/pages/TMS/components/login/Login"
import Logout from "../../../support/pages/TMS/components/logout/Logout"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"


describe("Implement Function", () => {
    var data, objLogin, objHome, objLoginPRD
    var objPickup = new Pickup()
    var objcreateOrder
    var msg = "background processing, you will be notifed you through the system information after the processing is completed. during this period, please do not update pick up/delivery status for orders!";


    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })
    
    // it("TC_36 - Don't fill in Pickup completion time field " , () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         cy.wait(2000)
    //         objPickup.pickupCompletionTime().trigger('mouseover')
    //         cy.wait(2000)
    //         objPickup.tooltipRequireCompletionTime("The pickupEndDt field is required.").contains("The pickupEndDt field is required.")            
    //     })
    // })

    // it("TC_37 - Pickup completion time is earlier than Pickup arrival time ", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.chooseArrivalTime(5)
    //         objPickup.chooseCompletionTime(3)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         objPickup.pickupCompletionTime().trigger('mouseover')
    //         // cy.wait(2000)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         objPickup.tooltipRequireCompletionTime("the pickup completed time cannot be earlier than the [ pickup arrival time, pickup start time]").contains("the pickup completed time cannot be earlier than the [ pickup arrival time, pickup start time]")
    //     })
    // })

    // it("TC_38 - Pickup completion time is earlier than Pickup start time ", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.chooseStartTimeday(5)
    //         objPickup.chooseCompletionTime(3)
    //         objPickup.pickupCompletionTime().trigger('mouseover')
    //         // cy.wait(2000)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         objPickup.tooltipRequireCompletionTime("the pickup completed time cannot be earlier than the [ pickup arrival time, pickup start time]").contains("the pickup completed time cannot be earlier than the [ pickup arrival time, pickup start time]")
    //     })
    // })

    // it("TC_46 - Search information by condition", () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         objPickup.header.searchResult(num).should("be.visible")
    //     })
    // })

    // // TC_48, TC_49, TC_50
    // it("TC_48, TC_49, TC_50 - Pagination", () => {
    //     objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //     cy.wait(1000)
    //     objPickup.goToPageNumber(1)
    //     cy.wait(1000)
    //     objPickup.goToPageNumber(2)
    //     cy.wait(1000)
    //     objPickup.common.selectPageSize("100/page")
    //     cy.wait(1000)
    //     objPickup.common.selectPageSize("50/page")     
    //     cy.wait(1000)
    //     objPickup.common.selectPageSize("20/page")        
    // })

    // // TC_51, TC_52
    // it("TC_51, TC_52 - Set and Reset Column", () => {
    //     objPickup.leftMenu.navigateToSubPage("implement", "Pick up ")
    //     objPickup.common.resetDefaultColumn("yes")
    //     cy.wait(5000)
    //     objPickup.common.setColumn("waybill status", "waybill number")
    //     objPickup.common.enableOrDisableColumn("waybill status", "no")
    //     objPickup.common.resetDefaultColumn("yes")
    // })
})