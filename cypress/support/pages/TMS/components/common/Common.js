import Abnormal from "../abnormal/Abnormal";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class Common {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
    }

    btnOK(){
        return cy.xpath("(//span[contains(text(),'OK')])[last()]")
    }

    txtSearchLocation() {
        return cy.xpath("(//div[@class='search-candidate']//div[@class='search-cadidate-keyword'])[last()]//input")
    }

    spanSearchLocationResult() {
        return cy.xpath("(//div[@class='search-candidate-list'])[last()]//div[@class='station-info']//span[@class='search-highlight']")
    }

    lblElement(value) {
        return cy.xpath("//span[text()='" + value + "']")
    }

    btnConfirmHintPopUp(action) {
        return cy.xpath("//div[@class='el-message-box']//button//span[contains(text(),'" + action + "')]")
    }

    btnResetSetColumn(reset) {
        return cy.xpath("//button//span[contains(text(),'" + reset + "')]")
    }

    btnConfirmAgainHintPopup(action) {
        return cy.xpath("(//button//span[contains(text(),'" + action + "')])[1]")
    }

    textField(placeholder) {
        return cy.xpath("//input[@placeholder='" + placeholder + "']")
    }

    lnkButton(name) {
        return cy.xpath("//button[@class='el-button common-style-shrinkage el-button--text el-button--mini']//span[text()='" + name + "']")
    }

    btnDialogFooter(button) {
        return cy.xpath("//div[@class='dialog-footer']//span[text()='" + button + "']")
    }

    msgError() {
        return cy.xpath("//i[@class='el-message__icon el-icon-error']//following-sibling::p[@class='el-message__content']")
    }

    txtError() {
        return cy.xpath("//p[@class='el-message__content']")
    }

    msgWarning() {
        return cy.xpath("//i[@class='el-message__icon el-icon-warning']//following-sibling::p")
    }

    msgAlert() {
        return cy.xpath("//i[@class='el-message__icon el-icon-info']")
    }

    msgSuccess() {
        return cy.xpath("//p[contains(text(),'success')]")
    }

    msgSuccessShouldContainText(msg) {
        return cy.xpath("//p[contains(text(),'" + msg + "')]")
    }

    msgCancel() {
        return cy.xpath("//p[contains(text(),'cancell')]")
    }

    msgNotificationRequired() {
        return cy.xpath("//div[contains(text(),'is required')]")
    }

    tdTrackNumberDetail(id) {
        return cy.xpath("//table[@class='common-table-native']//td[text()='" + id + "']")
    }

    btnTimeNow() {
        return cy.xpath('//span[contains(text(),"Now")]')
    }

    btnOK() {
        return cy.xpath("(//span[contains(text(),'OK')])[last()]")
    }
    txtStartDate() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-arrow-left'])[2]//following-sibling::div")
    }
    txtEndDate() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-arrow-right'])[2]//following-sibling::div")
    }
    //datepicker
    dtpDate() {
        return cy.xpath("//div[@class='el-picker-panel__body']")
    }
    btnNavigatePreviousMonth() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-arrow-left'])[2]")
    }
    btnNavigateNextMonth() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-arrow-right'])[2]")
    }
    btnnavigatePreviousYear() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-d-arrow-left'])[2]")
    }
    btnNavigateNextYear() {
        return cy.xpath("(//button[@class='el-picker-panel__icon-btn el-icon-d-arrow-right'])[2]")
    }


    inputTime(days) {
        var currentDate = new Date();
        cy.addDays(currentDate, days).then((futureDate) => {
            this.ipSelectDate().realType(futureDate)
            this.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.btnOK().click()
    }

    //fold
    hideSearch() {
        return cy.xpath("//div[@data-old-padding-top and @style='display: none;']")
    }

    unHideSearch() {
        return cy.xpath("//div[@data-old-padding-top and @style]")
    }

    hideAndUnHideSearch() {
        this.header.btnAtHeader("more filter").click()
        this.hideSearch().should("not.exist")
        cy.wait(1000)
        this.header.btnAtHeader("collapse filter").click()
        this.unHideSearch().should("exist")
    }

    columnDispatchNumber() {
        return cy.xpath('(//table[@class="el-table__body"])[2]//td[2]//div')
    }

    columnDispatchOrderNumber() {
        return cy.xpath('//td[2]//div')
    }

    columnShippingOrderNumber() {
        return cy.xpath("//tbody//div[contains(text(),'O00')]")
    }

    columnStatus() {
        return cy.xpath('//td[4]//div')
    }

    columnUpstreamTracking() {
        return cy.xpath('//td[6]//div')
    }

    columnShipmentNumber() {
        return cy.xpath('//td[7]//div')
    }

    showMore() {
        this.header.btnAtHeader("more").click()
    }

    lblStatus(status) {
        return cy.xpath("//li//span[normalize-space()='" + status + "']")
    }

    searchResult(value) {
        return cy.xpath("//table//tr[@class='el-table__row']//td//div[text()='" + value + "']")
    }

    searchResultWithOutValue() {
        return cy.xpath("//table//tr[contains(@class,'el-table__row')]")
    }

    openDetailView() {
        cy.wait(700)
        this.searchResultWithOutValue().first().dblclick({ force: true })
    }

    //change page size
    selectPageSize(size) {
        this.header.ddlPageSize().click()
        this.header.spanPageSize(size).click()
        cy.wait(2000)
        this.header.spanPageSize(size).should("be.exist")
    }

    //go to page
    goToPageNumber(page) {
        this.header.ipPageNumber().clear({ force: true })
        this.header.ipPageNumber().realType(page)
        this.header.ipPageNumber().type('{enter}')
        cy.wait(2000)
    }

    //clear filter by dropdown
    clearFilterForDropdown(box) {
        this.header.ipSearchBox(box).click()
        this.header.icDeleteSearchFilterDropdown().click()
        this.header.btnAtHeader("inquire").click()
    }

    //clear filter by input
    clearFilterForInput(box) {
        this.header.ipSearchBox(box).click({ force: true })
        this.header.icDeleteSearchFilterInput().click({ force: true })
        this.header.btnAtHeader("inquire").click({ force: true })
    }

    //set column
    resetDefaultColumn(reset) {
        this.header.btnAtHeader("set column").click()
        cy.wait(1000)
        this.header.btnSetColumnAction("reset").click()
        cy.wait(1000)
        if (reset == "yes") {
            this.header.btnConfirmActionSetColumn("confirm").click()
            this.msgSuccess().should("be.visible")
        } else {
            this.header.btnConfirmActionSetColumn("cancel").click()
        }
    }

    setColumn(source, target) {
        this.header.btnAtHeader("set column").click()
        cy.wait(1000)
        this.header.divColumnToAdjust(source).move(this.header.divColumnToAdjust(target))
        // this.divDragItem(source).drag(this.divDragItem(target))
        // cy.dndNative(this.divDragItem(source), this.divDragItem(target))
        cy.wait(1000)
        this.header.btnSetColumnAction("confirm").click()
        cy.wait(1000)
        this.msgSuccess().should("be.exist")
    }

    enableOrDisableColumn(column, active) {
        this.header.btnAtHeader("set column").click({ force: true })
        cy.wait(1000)
        this.header.divColumnToAdjust(column).click()
        cy.wait(1000)
        this.header.btnSetColumnAction("confirm").click()
        cy.wait(1000)
        this.msgSuccess().should("be.exist")
        if (active == "yes") {
            this.header.divColumnName(column).should("be.visible")
        } else {
            this.header.divColumnName(column).should("not.exist")
        }
    }
    //Search
    searchByClickSuggestion(searchField, condition) {
        this.header.ipSearchBox(searchField).last().click({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox(searchField).last().clear({ force: true })
        this.header.ipSearchBox(searchField).last().focus().type(condition, {force: true})
        this.header.ipSearchBox(searchField).last().type("{enter}")
        cy.wait(1000)
        this.header.spanSearchSuggestionDeliveryPlanPage(condition).should("be.visible")
        this.header.spanSearchSuggestionDeliveryPlanPage(condition).click({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("inquire").click({ force: true })
    }

    searchByInputDataToFilter(searchField, condition) {
        this.header.ipSearchBox(searchField).click()
        cy.wait(2000)
        // this.header.ipSearchBox(searchField).clear({ force: true })
        // cy.wait(1000)
        this.header.ipEnterDataToFilter().realType(condition, { force: true })
        cy.wait(2000)
        this.header.spanSearchFilterResult().click()
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(2000)
    }

    searchBySelectValue(searchField, value) {
        this.header.ipSearchBox(searchField).click({ force: true })
        cy.wait(1000)
        this.header.spanDropdownValue(value).click()
        cy.wait(1000)
        this.header.btnAtHeader("inquire").click()
        cy.wait(2000)
    }

    searchBySelectStatus(searchField, status) {
        this.header.ipSearchBox(searchField).click()
        cy.wait(2000)
        this.lblStatus(status).click()
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(2000)
    }

    searchByInputOrderNumber(searchField, condition) {
        this.header.ipSearchBox(searchField).click()
        // cy.wait(2000)
        // this.header.ipSearchBox(searchField).realType(condition)
        cy.wait(1000)
        var characters = this.trimString(condition)
        for (let index = 0; index < characters.length; index++) {
            this.header.ipSearchBox(searchField).last().focus().realType(characters[index])
            cy.wait(100)
        }
        cy.wait(1000)
        // cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        // cy.wait(2000)
    }

    spanSearchSuggestionDeliveryPlanPageForSplitOrder(searchKey, position) {
        return cy.xpath("(//div[@class='select-style']//a//span[@class='search-highlight' and text()='" + searchKey + "'])[" + position + "]")
    }

    btnSearchOrder(btnName) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[1]")
    }

    btnSearchDispatch(btnName) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[2]")
    }

    //order-A and order-B
    searchByClickSuggestionAtDeliveryPlanPageForOrderSplit(searchField, searchKey, place, position) {
        this.header.ipSearchBox(searchField).click({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox(searchField).focus().realType(searchKey)
        this.header.ipSearchBox(searchField).type("{enter}")
        // var characters = this.trimString(searchKey)
        // for (let index = 0; index < characters.length; index++) {
        //     this.header.ipSearchBox(searchField).focus().realType(characters[index])
        //     cy.wait(100)
        // }
        cy.wait(1000)
        this.spanSearchSuggestionDeliveryPlanPageForSplitOrder(searchKey, position).should("be.visible")
        this.spanSearchSuggestionDeliveryPlanPageForSplitOrder(searchKey, position).click({ force: true })
        cy.wait(1000)
        if (place == "first") {
            this.btnSearchOrder("inquire").click({ force: true })
        }
        if (place == "second") {
            this.btnSearchDispatch("inquire").click({ force: true })
        }
        cy.wait(1000)
    }

    wayBillNumber() {
        return cy.xpath('//div[@class="common-right-title"]')
    }

    //this.waybill to call value
    // getWaybillNumber() {
    //     this.openDetailView()
    //     this.wayBillNumber().invoke('text').then((wayBillNumber) => {
    //         var waybill = wayBillNumber.trim().substring(15)
    //         cy.wrap(waybill).as('waybill')
    //         console.log("waybill is " +this.waybill)
    //         cy.log("waybill is " +this.waybill)
    //     })
    // }

    getWaybillNumber() {
        this.openDetailView()
        this.wayBillNumber().invoke('text').as('fullWaybill')
    }

    splitWaybill(inputtedWaybill) {
        var waybillSplit = inputtedWaybill
        cy.log("waybill is : " + inputtedWaybill)
        waybillSplit = waybillSplit.trim().substring(15)
        cy.wrap(waybillSplit).as('waybill')
    }

    // test() {
    //     cy.get('[data-cy=AmountAll]', {
    //         timeout: 15000
    //     }).invoke('text').then((text) => {
    //         const amountAllBefore = parseInt(text.replace(/\D/g, '')) //this will only work if text.replace(/\D/g, '') returns a number but is string
    //         cy.wrap(amountAllBefore).as('myAlias')
    //     })

    //     cy.get('[data-cy=AmountAll]', {
    //         timeout: 15000
    //     }).invoke('text').then((text) => {
    //         const amountAllAfter = parseInt(text.replace(/\D/g, '')) //this will only work if text.replace(/\D/g, '') returns a number but is string
    //         expect(this.myAlias).to.be.greaterThan(amountAllAfter)
    //     })
    // }

    clickLinkButton(name) {
        this.lnkButton(name).click()
    }

    inputPageNumber(pageNumber) {
        this.header.ipPageNumber().realType(pageNumber, { force: true })
    }

    rowNumber(number) {
        return cy.xpath("(//table//tr[contains(@class, 'el-table__row')])[" + number + "]")
    }

    selectMultiRecord(record) {
        // if (this.rowNumber(record).should("not.exist")) {
        //     throw new Error("Don't have enough data to test!")
        // } else {
        for (let index = 1; index <= record; index++) {
            this.rowNumber(index).click({ force: true })
        }
        // }
    }

    noData() {
        this.lblElement("No Data").should("be.visible")
    }

    icCloseSetColumnDialog() {
        return cy.xpath("//div[@class='custom-table-title']//following-sibling::button")
    }

    closeSetColumn() {
        this.icCloseSetColumnDialog().click()
        this.header.btnSetColumnAction("reset").should("not.exist")
    }

    icClosePopUp(popUpName) {
        return cy.xpath("//span[text()='" + popUpName + "']//following-sibling::button//i")
    }

    closePopUp(popUpName) {
        this.icClosePopUp(popUpName).click()
    }

    clickButtonResetSetColumn(reset) {
        this.btnResetSetColumn(reset).click({ multiple: true, force: true })
        this.btnConfirmAgainHintPopup("confirm").click({ multiple: true, force: true })
    }

    spanHandleException() {
        return cy.xpath("//div[@class='el-dialog__body']//span[@class='except-no']")
    }

    redirectToAbnormalByClickHandleException() {
        this.spanHandleException().should("be.visible", { timeout: 10000 })
        this.spanHandleException().click({ force: true })
        return new Abnormal()
    }

    btnAddAttachments() {
        return cy.xpath('(//input[@type="file"])[1]')
    }

    addFileAttachments(filePath) {
        this.btnAddAttachments().attachFile(filePath)
        this.msgSuccess().should("be.visible")
    }

    ddlValue() {
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]")
    }

    liValueDropdownList2() {
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]//li")
    }

    spanItemType(item) {
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]//li//span[contains(text(),'"+ item +"')]")
    }

    selectRandomValue() {
        this.ddlValue().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.liValueDropdownList2().its('length').then((len) => {
            this.liValueDropdownList2().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    pNoDataSearchResult() {
        return cy.xpath("//p[@class='el-select-dropdown__empty']")
    }

    trimString(trackNumber) {
        var myArr = trackNumber.split("")
        return myArr
    }

    radioBtnDifferentFromRequest() {
        return cy.xpath('//span[contains(text(),"Different from Requested")]')
    }

    // inputTextOnebyOne(text, ){
    //     var characters = this.trimString(text)
    //     for (let index = 0; index < characters.length; index++) {
    //         this.header.ipSearchBox(searchField).last().focus().realType(characters[index])
    //         cy.wait(100)
    //     }
    // }

    // searchByClickSuggestion(searchField, condition) {
    //     cy.wait(1000)
    //     var characters = this.trimString(condition)
    //     this.header.ipSearchBox(searchField).last().click({ force: true })
    //     for (let index = 0; index < characters.length; index++) {
    //         this.header.ipSearchBox(searchField).last().realType(characters[index], { force: true })
    //         cy.wait(100)
    //     }
    //     cy.wait(1000)
    //     this.header.spanSearchSuggestionDeliveryPlanPage(condition).should("be.visible")
    //     this.header.spanSearchSuggestionDeliveryPlanPage(condition).click({ force: true })
    //     // cy.wait(1000)
    //     this.header.btnAtHeader("inquire").click({ force: true })
    // }
}

export default Common;