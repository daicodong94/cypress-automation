import HeaderMasterData from "../fragments/HeaderMasterData";
import LoginSupplier from "../../../TMS/components/login/LoginSupplier";
import LeftMenu from "../../../TMS/components/fragments/LeftMenu";

class CommonMasterData {
    constructor() {
        this.login = new LoginSupplier()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    liDropdownValue(value) {
        return cy.xpath('(//li//span[contains(text(),"' + value + '")])[last()]')
    }

    ddlPageSize() {
        return cy.xpath('//div[@class="el-select el-select--mini"]')
    }

    ipPageNumber() {
        return cy.xpath('//div[@class="el-input el-input--mini el-pagination__editor is-in-pagination"]//input')
    }

    spanPageSize(size) {
        return cy.xpath('//span[text()="' + size + '"]')
    }

    btnJumpPageNext() {
        return cy.xpath('//button[@class="btn-next"]')
    }

    btnJumpPagePrev() {
        return cy.xpath('//button[@class="btn-prev"]')
    }

    pageActive() {
        return cy.xpath('//li[@class="number active"]')
    }

    availablePageNumber() {
        return cy.xpath("//ul[@class='el-pager']//li")
    }

    pageNumber(number) {
        return cy.xpath('//ul[@class="el-pager"]//li[text()="' + number + '"]')
    }

    searchResultWithOutValue() {
        return cy.xpath('(//table//tr[@class="el-table__row cursor-pointer"])[1]')
    }

    searchResultWithValue() {
        return cy.xpath("(//tr[@class='el-table__row cursor-pointer'])[1]//td//span")
    }

    searchResultShouldContain(value) {
        const expectedResult = []
        this.searchResultWithValue().each(($li) => {
            expectedResult.push($li.text())
        }).then(() => {
            cy.log(expectedResult)
            for (let index = 0; index < expectedResult.length; index++) {
                const element = expectedResult[index];
                if (element.includes(value)) {
                    return
                } else {
                    continue
                }
            }
            // expect(expectedResult).to.include.members(actualResult)
        })
    }

    btnClicktoUpLoad() {
        return cy.xpath('//button//div[text()="Click to Upload"]')
    }

    ipUploadFile() {
        return cy.xpath('(//input[@type="file"])[1]')
    }

    titlePopupImportFile(title) {
        return cy.xpath('//div[@class="el-dialog__header"]//span[text()="' + title + '"]')
    }

    btnDownloadTemplate() {
        return cy.xpath('//div[contains(text(),"download template")]')
    }

    btnImportPopupAttachFileDisabled() {
        return cy.xpath('//button[@class="el-button el-button--primary el-button--mini is-disabled"]')
    }

    icTrash() {
        return cy.xpath("//*[local-name()='svg' and @class='cursor-pointer']")
    }

    removeImportFile() {
        this.icTrash().click({ force: true })
    }

    btnCancel() {
        return cy.xpath('//button//span[contains(text(),"Cancel")]')
    }

    btnConfirm(action) {
        return cy.xpath("//button//span[contains(text(), '" + action + "')]//parent::button")
    }

    btnImportPopupAttachFileEnable() {
        return cy.xpath('//button[@class="el-button el-button--primary el-button--mini"]')
    }

    btnConfirmHintPopup(action, location) {
        return cy.xpath('(//button//span[contains(text(),"' + action + '")])[' + location + ']')
    }

    btnConfirmHintPopupVer2(name) {
        return cy.xpath("//div[@class='el-message-box__btns']//button//span[contains(text(),'" + name + "')]")
    }

    titlePopupQuitImport() {
        return cy.xpath('//span[text()="Quit importing"]')
    }

    msgImportNotSuccess(msg) {
        return cy.xpath('//p[contains(text(),"' + msg + '")]')
    }

    msgSuccess() {
        return cy.xpath("//div[@role='alert' and contains(@class,'el-message el-message--success')]//p")
    }

    msgImportSuccess() {
        return cy.xpath("//div[contains(text(),'Import successfully')]")
    }

    loginAndNavigateToSite(username, password, menu, subMenu) {
        this.login.loginWithUserandPassword(username, password)
        cy.wait(3000)
        this.leftMenu.navigateToSubPage(menu, subMenu)
        cy.wait(2000)
    }

    msgSuccessShouldHaveText(msg) {
        this.msgSuccess().should("have.text", msg)
    }

    verifyDownload(button) {
        this.btnAction(button).click()
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
    }

    openImportPopUp(title) {
        this.btnAction("Import").click()
        this.titlePopupImportFile(title).should("be.visible")
        this.btnAction("Download Template").should("be.visible")
        this.btnAction("Click to Upload").should("be.visible")
    }

    //change page size
    selectPageSize(size) {
        this.ddlPageSize().click()
        this.spanPageSize(size).click()
        cy.wait(2000)
        this.spanPageSize(size).should("be.exist")
    }

    //go to page
    goToPageNumber(page) {
        this.ipPageNumber().clear()
        this.ipPageNumber().realType(page)
        this.ipPageNumber().realType('{enter}')
        cy.wait(2000)
        this.pageActive().should("have.text", page)
    }

    gotoPageByClickNumber(min) {
        this.availablePageNumber().should("have.length.at.least", min)
        this.pageNumber(min).click({ force: true })
        this.pageActive().contains(min)
    }

    goToPageNumberWhenClickbtnJumpPage(page) {
        this.ipPageNumber().clear()
        this.ipPageNumber().realType(page)
        if (page > 1) {
            this.btnJumpPageNext("right").click()
        } else if (page < 6) {
            this.btnJumpPagePrev("left").click()
        }
        cy.wait(2000)
    }

    searchByCondition(condition) {
        this.header.txtSearch().clear({ force: true })
        this.header.txtSearch().type(condition)
        cy.wait(2000)
        this.header.btnSearch().click({ force: true })
    }

    openDetailsView() {
        this.searchResultWithOutValue().first().dblclick({ force: true })
    }

    attachImportFile(filePath) {
        this.ipUploadFile().attachFile(filePath)
    }

    cancelImportFile(confirm) {
        this.btnCancel().click()
        cy.wait(1000)
        this.titlePopupQuitImport().should("be.visible")
        if (confirm == "yes") {
            this.btnConfirmHintPopup("Confirm", "1").click()
            this.titlePopupQuitImport().should("not.be.visible")
        } else if (confirm == "no") {
            this.btnConfirmHintPopup("Cancel", "2").click()
            this.titlePopupQuitImport().should("be.visible")
        }
    }

    importFileInvalidType(filePath, message) {
        this.attachImportFile(filePath)
        cy.wait(2000)
        this.msgImportNotSuccess(message).should("be.visible")
    }

    importFileNotMatchedTemplate(filePath, message) {
        this.attachImportFile(filePath)
        cy.wait(2000)
        this.btnImportPopupAttachFileEnable().click({ force: true })
        cy.wait(1000)
        this.msgImportNotSuccess(message).should("be.visible")
    }

    divNoData() {
        return cy.xpath("//div[contains(text(),'No Data')]")
    }

    ipSelectBox(field) {
        return cy.xpath("//input[@placeholder='" + field + "']")
    }

    txaInformation(placeholder) {
        return cy.xpath("//textarea[@placeholder='" + placeholder + "']")
    }

    spanValue(value) {
        return cy.xpath("//span[text()='" + value + "']")
    }

    ddlInformation(placeholder) { return cy.xpath("//input[@placeholder='" + placeholder + "']") }

    selectDataByClick(field, value) {
        this.ipSelectBox(field).scrollIntoView().click({ force: true })
        this.spanValue(value).click({ force: true })
        this.spanSelectedData().should("have.text", value)
    }

    inputDataToTextarea(field, value) {
        this.txaInformation(field).scrollIntoView().click({ force: true })
        this.txaInformation(field).clear().type(value)
    }

    spanSelectedData() {
        return cy.xpath("(//li[@class='el-select-dropdown__item selected hover'])[last()]//span")
    }

    inputDataToField(field, value) {
        this.ipSelectBox(field).scrollIntoView().click({ force: true })
        this.ipSelectBox(field).clear().type(value)
    }

    msgError() {
        return cy.xpath("//div[@class='text-error-5 text-sm error-message has-error']")
    }

    msgErrorShouldBe(message) {
        this.msgError().contains(message)
    }

    msgAlertError() {
        return cy.xpath("//div[@role='alert' and @class='el-message el-message--error']//p")
    }

    msgAlertErrorShouldHaveText(msg) {
        this.msgAlertError().contains(msg)
    }

    //Set Status
    lnkSetStatus() {
        return cy.xpath("//div[contains(text(),'Set Status')]")
    }

    dialogSetStatus() {
        return cy.xpath("//div[@role='dialog' and @aria-label='Set status']")
    }

    rdoGroup(name) {
        return cy.xpath("//div[contains(text(),'" + name + "')]//parent::div//span//div//input")
    }

    verifyChecked(name) {
        return cy.xpath("//div[contains(text(),'" + name + "')]//parent::div//span//label")
    }

    divEffectiveDataTime() {
        return cy.xpath("//div[contains(text(),'Effective date / time')]//parent::div//span//div//div[@class='text-base text-primary-85']")
    }

    btnConfirmQuitSettingStatus(name) {
        return cy.xpath("//div[@class='el-message-box__btns']//button//span[contains(text(),'" + name + "')]")
    }

    btnConfirmSetStatus(name) {
        return cy.xpath("//div[@class='flex justify-end p-4']//button//span[contains(text(),'" + name + "')]")
    }

    icCloseDialog() {
        return cy.xpath("//button[@class='el-dialog__headerbtn']")
    }

    openSetStatusPopUp() {
        this.lnkSetStatus().scrollIntoView().click({ force: true })
        this.icCloseDialog().should("be.visible")
    }

    closeSetStatusPopUp() {
        this.icCloseDialog().click({ force: true })
        this.btnConfirmQuitSettingStatus("Confirm").click({ force: true })
        this.icCloseDialog().should("not.exist")
    }


    //date picker
    dtpField(name) {
        return cy.xpath("//input[@type='text' and @placeholder='" + name + "']//ancestor::div[@class='relative w-full']")
    }

    divDatePicker() {
        return cy.xpath("(//div[@class='el-picker-panel el-date-picker el-popper has-time tw tw-datepicker'])[last()]")
    }

    btnNavigateMonthYear(name) {
        return cy.xpath("(//button[@aria-label='" + name + "'])[last()]")
    }

    spanCurrentMonth() {
        return cy.xpath("(//div[@class='el-date-picker__header'])[last()]//span[2]")
    }

    spanCurrentYear() {
        return cy.xpath("(//div[@class='el-date-picker__header'])[last()]//span[1]")
    }

    divAvailableDate() {
        return cy.xpath("(//div[@class='el-picker-panel__content'])[last()]//td[contains(@class,'available')]//div//span")
    }

    divDisabledDate() {
        return cy.xpath("(//div[@class='el-picker-panel__content'])[last()]//td[@class='normal disabled']//div//span")
    }

    btnConfirmDatePicker(name) {
        return cy.xpath("(//div[@class='datepicker-action w-full flex justify-end'])[last()]//button//span[text()='" + name + "']")
    }

    ddlPagesize() {
        return cy.xpath("//div[@class='el-select el-select--mini']")
    }

    verifyTypeOfPageSize() {
        this.ddlPagesize().click({ force: true })
        this.spanPageSize("30/page").should("be.exist")
        this.spanPageSize("50/page").should("be.exist")
        this.spanPageSize("75/page").should("be.exist")
        this.spanPageSize("100/page").should("be.exist")
    }

    icSetStatus() {
        return cy.xpath("//div[@class='el-tooltip flex items-center cursor-pointer']//*[local-name()='svg'][2]")
    }

    clickIconSetStatus() {
        this.icSetStatus().first().scrollIntoView().click({ force: true })
        this.dialogSetStatus().should("be.visible")
    }

    btnAction(action) {
        return cy.xpath("//button//div[contains(text(), '" + action + "')]")
    }

    lblHistory() {
        return cy.xpath("//p[contains(text(),'History')]")
    }

    navigateToViewHistoryLogScreen() {
        this.btnAction("View History Log").click({ force: true })
        this.lblHistory().should("be.visible")
    }

    //verify data from dropdown list
    liValueDropdownList() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-select-dropdown__list'])[last()]//li")
    }

    checkDataServiceGroup(field, listData) {
        this.ipSelectBox(field).scrollIntoView().click({ force: true })
        const expectedResult = listData
        const actualResult = []
        console.log(expectedResult)
        this.liValueDropdownList().each(($li) => {
            actualResult.push($li.text())
        }).then(() => {
            expect(expectedResult).to.include.members(actualResult)
            // if (expectedResult.length == actualResult.length){
            //     for (let i = 0; i < expectedResult.length; i++) {
            //         if (expectedResult[i] != actualResult[i]){
            //             console.log(expectedResult[i] + " -"  + actualResult[i])
            //         }
            //     }
            // }
        })
    }

    //select random
    ddlValue(){
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]")
    }

    liValueDropdownList2() {
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]//li")
    }

    liSearchSuggestion() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-autocomplete-suggestion__list']//li)[last()]")
    }

    verifySearchSuggestionAndSelect(text) {
        this.liSearchSuggestion().contains(text)
        this.liSearchSuggestion().click({ force: true })
    }

    //random from 0 to n
    selectRandomValue() {
        this.ddlValue().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.liValueDropdownList2().its('length').then((len) => {
            this.liValueDropdownList2().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }
    //random from 1 to n
    selectRandomValueVer2() {
        this.ddlValue().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.liValueDropdownList2().its('length').then((len) => {
            this.liValueDropdownList2().eq(Math.floor(Math.random() * ((len - 2) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    //random from - to n-2
    selectRandomValue2() {
        this.ddlValue().should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.liValueDropdownList2().its('length').then((len) => {
            this.liValueDropdownList2().eq(Math.floor(Math.random() * ((len - 3) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    //confirm create
    confirmCreateData(action, confirm, click, verify) {
        cy.wait(1000)
        if (action != "empty") {
            this.btnConfirm(action).click({ force: true })
        }
        if (confirm != "empty") {
            this.btnConfirmHintPopupVer2(confirm).should("be.visible")
        }
        if (click != "empty") {
            this.btnConfirmHintPopupVer2(confirm).click({ force: true })
        }
        if (verify == "success") {
            this.msgSuccess().should("be.visible")
        } else {
            this.msgError().should("be.visible")
        }
    }

    //assgin subcontractor
    btnAssign(name) {
        return cy.xpath("//div[@class='text-base font-medium' and text()='" + name + "']//following-sibling::button//span")
    }
    btnSearch() {
        return cy.xpath("//div[@class='flex w-full w-[480px]']//button")
    }
    btnSearchInPopUp() {
        return cy.xpath("//div[@class='flex w-full py-4']//button")
    }
    trimString(trackNumber) {
        var myArr = trackNumber.split("")
        return myArr
    }
    searchByConditionV2(condition) {
        cy.wait(1000)
        this.header.txtSearch().first().clear({ force: true })
        this.header.txtSearch().first().click({ force: true })
        // var characters = this.trimString(condition)
        // for (let index = 0; index < characters.length; index++) {
        //     this.header.txtSearch().first().focus().realType(characters[index], { force: true })
        //     cy.wait(100)
        // }
        // cy.wait(1000)
        this.header.txtSearch().first().realType(condition, { force: true })
        cy.wait(1000)
        this.btnSearchInPopUp().first().click({ force: true })
        cy.wait(1000)
    }
    lblSiteIDExpected() {
        return cy.xpath("//tr[@class='el-table__row']//span[contains(text(), 'ANL')]")
    }
    lnkSelect() {
        return cy.xpath("//div[@class='text-sm text-primary-main font-medium cursor-pointer']")
    }
    lblSubIDActual() {
        return cy.xpath("//div[@class='p-4 flex items-center justify-between border-t border-neutral-1']//div//div[contains(text(),'Subcontractor ID')]")
    }
    icDeleteSubcontractor() {
        return cy.xpath("//*[local-name()='svg' and @class='cursor-pointer']//parent::div/div/div[@class='text-sm text-secondary-45' and contains(text(),'Subcontractor ID:')]")
    }

    iconDeleteSuncontractorShouldBe() {
        this.icDeleteSubcontractor().should("be.visible")
    }
    selectSubcontractor(button, condition) {
        if (button != "empty") {
            this.btnAssign(button).click({ force: true })
        }
        cy.wait(1000)
        this.searchByConditionV2(condition)
        this.lblSiteIDExpected().first().then(($el) => {
            var expectSubID = $el.text().trim()
            cy.log("expected Subcontractor ID is " + expectSubID)
            // cy.wait(1000)
            this.lnkSelect().first().click({ force: true })
            cy.wait(1000)
            this.lblSubIDActual().then(($el1) => {
                var actualSubID = $el1.text().trim()
                cy.log("actual Sub ID is " + actualSubID)
                expect(actualSubID).to.include(expectSubID)
            })
        })
        cy.wait(1000)
        this.iconDeleteSuncontractorShouldBe()
    }
}

export default CommonMasterData;