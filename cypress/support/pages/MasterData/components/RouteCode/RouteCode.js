import Common from "../../../TMS/components/common/Common"
import Header from "../../../TMS/components/fragments/Header"
import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import LeftMenu from "../../../TMS/components/fragments/LeftMenu"

class RouteCode {
    //CONSTRUCTOR
    constructor() {
        this.common = new Common()
        this.commonMD = new CommonMasterData()
        this.header = new Header()
        this.headerMD = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    //xpath
    btnFilter() {
        return cy.xpath("//span[contains(text(),'Filter')]/parent::button")
    }

    txtSearchOriginName() {
        return cy.xpath("//input[@placeholder='Search by Route Code, Origin ID, Shipto ID']")
    }

    lblRouteCode() {
        return cy.xpath("//td[contains(@class, 'el-table_1_column_2')]//span")
    }

    btnImport() {
        return cy.xpath("//span[contains(text(), 'Import')]/parent::button")
    }

    btnImportFile() {
        return cy.xpath("(//span[contains(text(), 'Import')]/parent::button)[2]")
    }

    btnBackToRouteCode() {
        return cy.xpath("(//span[contains(text(), 'Back to Route Code')]/parent::button)")
    }

    importRouteCode(filePath) {
        this.commonMD.attachImportFile(filePath)
        this.btnImportFile().click()
        cy.wait(3000)
        this.btnBackToRouteCode().click()
    }

    chkboxRouteCode(routeCode) {
        return cy.xpath("//span[contains(text(), '"+ routeCode +"')]//parent::div/parent::div/parent::td/preceding-sibling::td//label")
    }

    btnRemove() {
        return cy.xpath("//span[contains(text(), 'Remove')]/parent::button")
    }

    btnConfirmRemove() {
        return cy.xpath("(//div[@aria-label='Remove Route Codes']//button)[3]")
    }

    removeRouteCode() {
        this.btnRemove().click()
        this.btnConfirmRemove().click()
    }

    lblPopUpTitle() {
        return  cy.xpath("(//span[@class='el-dialog__title'])[2]")
    }

    btnDownloadTemplate() {
        return cy.xpath("//div[contains(text(), 'Download Template')]//parent::span/parent::button")
    }

    btnClickToUpload() {
        return cy.xpath("//div[contains(text(), 'Click to Upload')]//parent::span/parent::button")
    }

    importRouteCodePopUp() {
        this.lblPopUpTitle().should("have.text","Import Route Code")
        this.btnDownloadTemplate().should("be.visible")
        this.btnClickToUpload().should("be.visible")
    }

    popupSelectOverwrite() {
        return cy.xpath("//div[@aria-label='Import Route Code']//div[contains(text(),'Select Route Code you want to')]")
    }

    txtSearchOverwrite() {
        return cy.xpath("(//input[@id='input-search'])[2]")
    }

    lblOverwriteRouteCode() {
        return cy.xpath("//div[@aria-label='Import Route Code']//table/tbody/tr/td[2]//span")
    }

}

export default RouteCode;