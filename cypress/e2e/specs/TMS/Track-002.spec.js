import Homepage from "../../../support/pages/TMS/components/homepage/Homepage";
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood";
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp";
import Login from "../../../support/pages/TMS/components/login/Login";
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan";
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule";
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder";
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder";
import DispatchOrderTracking from "../../../support/pages/TMS/components/track/DispatchOrderTracking";
import ShippingOrderTracking from "../../../support/pages/TMS/components/track/ShippingOrderTracking";
import { Utility } from "../../../support/utility"

describe("Checking Our - Item type", () => {

    var objShippingOrderTracking = new ShippingOrderTracking(), data, objLogin = new Login(), objHome = new Homepage(), objDispatchOrderTracking = new DispatchOrderTracking(),
        objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objSchedule = new Schedule(), objDeliveryPlan = new DeliveryPlan(), objDeliverGoods = new DeliveryGood(), objPickup = new Pickup()
    var customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"
    beforeEach("TMS-Our-Service item", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(() => {
        cy.wait(10000)
        objHome.logout()
    })

    it("TC_45 - Check paging at Dispatch Order Tracking", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.selectPageSize("20/page")
        cy.wait(2000)
        objDispatchOrderTracking.common.selectPageSize("50/page")
        cy.wait(2000)
        objDispatchOrderTracking.common.selectPageSize("100/page")
        cy.wait(2000)
        objDispatchOrderTracking.common.selectPageSize("200/page")
        cy.wait(2000)
        objDispatchOrderTracking.common.selectPageSize("500/page")
    })

    it("TC_46 - Check function Fold at Dispatch Order Tracking", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.clickLinkButton("more filter...")
        cy.screenshot('clickLinkButtonMore')
        cy.wait(2000)
        objDispatchOrderTracking.common.clickLinkButton("collapse filter")
        cy.screenshot('clickLinkButtonFold')
    })

    it("TC_47 - Check function Set Column - Disable header at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.enableOrDisableColumn("dispatch number", "no")
    })

    it("TC_48 - Check function Set Column - Enabled header at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.enableOrDisableColumn("dispatch number", "yes")
    })

    it("TC_49 - Check function Set Column - Drag and drop to reorder at Dispatch Order Tracking", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.setColumn("dispatch number", "status")
    })

    it("TC_50 - Check function Set Column - Reset at Dispatch Order Tracking", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.resetDefaultColumn()
        objDispatchOrderTracking.btnCloseSetColumn().click()
    })

    it("TC_51 - Check go to next page at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBeNextPageWhenClickButtonNextPage()
    })

    it("TC_52 - Check cannot go to next page at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBeDisabledWhenClickButtonNextPageFirst()
    })

    it("TC_53 - Check go to previous page at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBePreviousPageWhenClickButtonPreviousPage()
    })

    it("TC_54 - Check cannot go to prev page at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBeDisabledWhenClickButtonPreviousPageFirst()
    })

    it("TC_55 - Check go to page by input page number at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBePageWhenInputPageNumber("3")
    })

    it("TC_56 - Check cannot go to page by input page number at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.shouldBePageWhenInputPageNumber("1000")
    })

    it("TC_57 - Check search dispatch order tracking", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
            // cy.wait(60000)
            objDispatchOrderTracking.searchTrackingNumber(trackingNum)
        })
    })

    it.skip("TC_58 - Check state is show in list search response - CREATED", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 5);
            cy.reload()
            objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            // cy.wait(60000)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum, "first")
            cy.wait(5000)
            objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })
            objDeliveryPlan.header.btnAtHeader("create a dispatch order ").click()
            cy.reload()
            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
            // cy.wait(60000)
            objDispatchOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objDispatchOrderTracking.checkStatus("created")
        })
    })

    it.skip("TC_58 - Check state is show in list search response - NO_DISPATCHED", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 5);
            cy.reload()
            objDeliveryPlan = objHome.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            // cy.wait(60000)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum, "first")
            cy.wait(5000)
            objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })
            objDeliveryPlan.header.btnAtHeader("create a dispatch order ").click()
            // cy.wait(60000)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", trackingNum, "first")
            objDeliveryPlan.common.searchResultWithOutValue().click({ force: true })
            objDeliveryPlan.common.columnDispatchNumber().then(($text) => {
                objDeliveryPlan.header.btnAtHeader("confirm").click()
                cy.wait(5000)
                cy.reload()
                objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
                // cy.wait(60000)
                objDispatchOrderTracking.searchTrackingNumber($text.text().trim())
                cy.wait(5000)
                objDispatchOrderTracking.checkStatus("no_dispatched")
            })
        })
    })

    it.skip("TC_58 - Check state is show in list search response - DISPATCHED", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
            // cy.wait(60000)
            objDispatchOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objDispatchOrderTracking.checkStatus("dispatched")
        })
    })

    it.skip("TC_58 - Check state is show in list search response - TRANSITED", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 5);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(2000)
            objPickup.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objPickup.common.openDetailView()
            cy.wait(2000)
            objPickup.chooseCompletionTime(5)
            cy.wait(2000)
            objPickup.clickbtnFunctionPickup("pickup completed")
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(5000)
            cy.reload()
            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
            // cy.wait(60000)
            objDispatchOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objDispatchOrderTracking.checkStatus("transited")
        })
    })

    it.skip("TC_58 - Check state is show in list search response - FINISHED", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objSchedule = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, "Automation Company", "运输项目", "Phamar", siteA, siteB, 8);
            cy.wait(5000)
            cy.reload()
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            // cy.wait(60000)
            objSchedule.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.selectCarrier("SYS_DEF_INDIVIDUAL")
            cy.wait(2000)
            objSchedule.lnkAddVehicle().click()
            cy.wait(2000)
            objSchedule.selectDriver("Auto Driver")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click()
            cy.wait(5000)
            cy.reload()
            objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(2000)
            objPickup.common.searchByClickSuggestion("dispatch number", trackingNum)
            cy.wait(2000)
            objPickup.common.openDetailView()
            cy.wait(2000)
            objPickup.chooseCompletionTime(4)
            cy.wait(2000)
            objPickup.clickbtnFunctionPickup("pickup completed")
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            objPickup.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(5000)
            cy.reload()
            objDeliverGoods = objHome.leftMenu.navigateToSubPage("implement", "deliver goods")
            objDeliverGoods.common.searchByClickSuggestion("please enter the dispatch number", trackingNum)
            cy.wait(2000)
            objDeliverGoods.common.openDetailView()
            cy.wait(2000)
            objDeliverGoods.chooseTimeForDeliveryCompletionTime(5)
            cy.wait(2000)
            objDeliverGoods.header.btnAtHeader("Delivery completed").click()
            cy.wait(2000)
            objDeliverGoods.common.btnConfirmHintPopUp("confirm").click()
            cy.wait(2000)
            cy.reload()
            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
            // cy.wait(60000)
            objDispatchOrderTracking.searchTrackingNumber(trackingNum)
            cy.wait(5000)
            objDispatchOrderTracking.checkStatus("finished")
        })
    })

    it.skip("TC_58 - Check state is show in list search response - CLOSED", () => {
        objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
        objPickup.common.selectPageSize("500/page")
        objPickup.common.columnStatus().each(($el, index) => {
            const status = $el.text().trim()
            if (status === "for_delivered") {
                objPickup.common.columnDispatchOrderNumber().eq(index).then(($text) => {
                    cy.log($el.text().trim())
                    objPickup.common.columnStatus().eq(index).dblclick({ force: true }).then(() => {
                        cy.wait(2000)
                        objPickup.chooseArrivalTimeLessDay("1")
                        cy.wait(2000)
                        objPickup.clickbtnFunctionPickup("empty")
                        cy.wait(2000)
                        objPickup.common.btnConfirmHintPopUp("confirm").click()
                    })
                    cy.reload()
                    objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
                    // cy.wait(60000)
                    objDispatchOrderTracking.searchTrackingNumber($text.text().trim())
                    cy.wait(5000)
                    objDispatchOrderTracking.checkStatus("closed")

                })
                return false
            }
        })
    })

    it("TC_60 - Check return to Dispatch order screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        cy.wait(2000)
        objDispatchOrderTracking.header.btnAtHeader("return").click({ force: true })
    })

    it("TC_61 - Check Download documents at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        cy.wait(2000)
        objDispatchOrderTracking.header.btnAtHeader("download documents").click({ force: true })
        cy.wait(5000)
        objDispatchOrderTracking.header.btnAtHeader("download the delivery note").click({ force: true })
        cy.wait(10000)
        cy.verifyDownload("Download the dispatch order.pdf")
        objDispatchOrderTracking.header.btnAtHeader("package and download all").click({ force: true })
        cy.wait(10000)
        objDispatchOrderTracking.closePopupDownloadDocument()
        cy.wait(5000)
        objDispatchOrderTracking.clickFileManagement()
        cy.wait(5000)
        var filePath = "shippingorder/Detail import.xls"
        objDispatchOrderTracking.addFileAttachments(filePath)
        cy.wait(5000)
        objDispatchOrderTracking.header.btnAtHeader("download documents").click({ force: true })
        cy.wait(5000)
        objDispatchOrderTracking.clickDownloadAttachmentDoc()
        cy.wait(5000)
        cy.verifyDownload("Detail import.xls")
        objDispatchOrderTracking.closePopupDownloadDocument()
    })

    it("TC_62 - Check Service information at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        cy.wait(2000)
        objDispatchOrderTracking.header.btnAtHeader("Service Information").click({ force: true })
        objDispatchOrderTracking.checkServiceInformation()
        objDispatchOrderTracking.btnClosePopupServiceItemInfo().click({ force: true })
    })

    it("TC_63 - Check Reanalyse at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        cy.wait(2000)
        objDispatchOrderTracking.header.btnAtHeader("reanalyse").click({ force: true })
        objDispatchOrderTracking.common.txtError().should('contain', 'Tracking data is being refreshed, please check again later!')
    })

    it("TC_67 - Check Record Information at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("track", "Shipping Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        objDispatchOrderTracking.clickRecordInformation()
        cy.randomString(7).then((text) => {
            objDispatchOrderTracking.inputSomeText(text)
            objDispatchOrderTracking.clickButtonSaveEditRecord()
            objDispatchOrderTracking.shouldBeShowMessageSucess(text)
        })
    })

    it("TC_68 - Check Reanalyse at Dispatch Order Tracking screen", () => {
        objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
        cy.wait(2000)
        objDispatchOrderTracking.common.openDetailView()
        cy.wait(5000)
        objDispatchOrderTracking.clickFileManagement()
        cy.wait(5000)
        var filePath = "shippingorder/Detail import.xls"
        objDispatchOrderTracking.addFileAttachments(filePath)
        objDispatchOrderTracking.common.txtError().should("contain", (filePath.substr(14, 17) + "uploaded successfully"))
    })
})