import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import { Utility } from "../../../support/utility"

describe("Checking Our - User Organization", () => {

    var objUserOrganization, data, objLogin, objHome
    

    beforeEach("TMS-Our-UserOrganization", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(()=>{
        cy.wait(10000)
        objHome.logout()
    })

    it("TC_246 - Check edit information of user management", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        cy.randomString(5).then((name) => {
            cy.randomString(5).then((newName) => {
                const number = Math.floor(Math.random() * 100000000) + 10000000;
                objUserOrganization.editUserInformation(name, number, "name", name, newName)
                objUserOrganization.deleteUser("name", newName)
            })
        })
    })

    it("TC_248 - Check validation of Name field when creating the new people < 50 char and have spec + number", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        cy.randomString(5).then((name) => {
            const specName = name + "66a@"
            const number = Math.floor(Math.random() * 100000000) + 10000000;
            objUserOrganization.addNewUser(specName, name, number)
            objUserOrganization.verifyDataInquire(specName, name, number, "name", specName)
            objUserOrganization.deleteUser("name", specName)
        })
    })

    it("TC_249 - Check validation of Name field when creating the new people", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        cy.randomString(5).then((name) => {
            const number = Math.floor(Math.random() * 100000000) + 10000000;
            objUserOrganization.addNewUser(name, name, number)
            cy.wait(5000)
            cy.randomString(5).then((name2) => {
                const phone = Math.floor(Math.random() * 100000000) + 10000000;
                objUserOrganization.addNewUser(name, name2, phone)
            })
            objUserOrganization.deleteUser("name", name)
        })
    })

    it("TC_254 - Check validation of Name field when creating the new people", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        cy.randomString(5).then((name) => {
            const number = Math.floor(Math.random() * 100000000) + 10000000;
            objUserOrganization.addNewUser(name, name, number)
            cy.wait(5000)
            objUserOrganization.addNewUser(name, name, number)
            objUserOrganization.common.txtError().should('contain', 'data exception.Cell phone number already existed.')
            objUserOrganization.btnCloseAddPopup().click({force : true})
            objUserOrganization.deleteUser("name", name)
        })
    })

    it("TC_255 - Check validation of Cell phone field when creating the new people", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        objUserOrganization.checkInValidPhoneNumber("66123456")
    })

    it("TC_257 - Check validation of Cell phone field when creating the new people", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        cy.wait(10000)
        objUserOrganization.checkInValidPhoneNumber("66!12342")
    })
})