import Common from "../common/Common";
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu";

class ExceptionType {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    ipField(name) {
        return cy.xpath("//input[@data-vv-name='" + name + "']")
    }

    ddlField(name) {
        return cy.xpath("//td[text()='" + name + "']//following-sibling::td//div[@class='el-input el-input--mini el-input--suffix']//input")
    }

    createExceptionType(name, level, time, type, illustrate) {
        this.header.btnAtHeader("new").click({ force: true })
        if (name != "empty") {
            this.ipField("exceptionName").type(name)
        }
        if (level != "empty") {
            this.ddlField("default level").click({ force: true })
            this.header.spanDropdownValue(level).click({ force: true })
        }
        if (time != "empty") {
            this.ipField("timeLimit").type(time)
        }
        if (type != "empty") {
            this.ddlField("type").click({ force: true })
            this.header.spanDropdownValue(type).click({ force: true })
        }
        if (illustrate != "empty") {
            this.header.ipSearchBox("(maximum 500 characters)").type(illustrate)
        }
        this.common.btnDialogFooter("save").click({ force: true })
    }

    deleteExceptionType(searchField, condition, confirm) {
        this.common.searchByInputOrderNumber(searchField, condition)
        this.common.searchResultWithOutValue().click({ force: true })
        this.header.btnAtHeader("delete").click({ force: true })
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").click({ force: true })
            this.common.msgSuccess().should("be.visible")
        } else {
            this.common.btnConfirmHintPopUp("cancel").click({ force: true })
        }
    }

    divStatus(status) {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[3]//div[text()='" + status + "']")
    }

    checkStatus(status) {
        var count = 0
        if (status != "empty") {
            this.divStatus(status).each(($el) => {
                const actual = $el.text().trim()
                // cy.log(actual)
                if (actual === status) {
                    count += 1
                }
                else {
                    cy.log("Fail")
                }
            }).then(() => {
                this.header.totalValue().should('contain', 'Total ' + count)
            })
        } else {
            this.common.searchResultWithOutValue().then(el => {
                count = el.length
            }).then(() => {
                this.header.totalValue().should('contain', 'Total ' + count)
            })
        }
    }

    tdFieldInExceptionDetail(name) {
        return cy.xpath("//td[text()='" + name + "']")
    }

    divColumnInExceptionDetail(name) {
        return cy.xpath("//span[contains(text(),'+ create')]//following::th//div[text()='" + name + "']")
    }

    detailExceptionTypeUI() {
        this.tdFieldInExceptionDetail("type name").should("be.visible")
        this.tdFieldInExceptionDetail("default level").should("be.visible")
        this.tdFieldInExceptionDetail("processing time limit").should("be.visible")
        this.tdFieldInExceptionDetail("automatic monitoring").should("be.visible")
        this.tdFieldInExceptionDetail("illustrate").should("be.visible")
        this.tdFieldInExceptionDetail("type").should("be.visible")
        this.tdFieldInExceptionDetail("attributes").should("be.visible")
        this.header.btnAtHeader("+ create").first().should("be.visible")
        this.divColumnInExceptionDetail("reason name").should("be.visible")
        this.divColumnInExceptionDetail("code").should("be.visible")
        this.divColumnInExceptionDetail("attributes").should("be.visible")
        this.divColumnInExceptionDetail("operate").should("be.visible")
    }

    editExceptionType(name, level, type) {
        if (name != "empty") {
            this.ipField("exceptionName").type(name)
        }
        if (level != "empty") {
            this.ddlField("default level").click({ force: true })
            this.header.spanDropdownValue(level).click({ force: true })
        }
        if (type != "empty") {
            this.ddlField("type").click({ force: true })
            this.header.spanDropdownValue(type).click({ force: true })
        }
        this.header.btnAtHeader("save").first().click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }
}

export default ExceptionType;