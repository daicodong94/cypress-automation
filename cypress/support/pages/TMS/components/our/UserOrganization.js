import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"

class UserOrganization{

    //CONSTRUCTOR
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }


    //XPATH
    txtNameNU(){
        return cy.xpath("//input[@data-vv-name='userName']")
    }

    txtStaffCodeNU(){
        return cy.xpath("//input[@placeholder='staff code']")
    }

    cboGenderNU(){
        return cy.xpath("//input[@placeholder='Select']")
    }

    lblGenderNU(gender){
        return cy.xpath("//li//span[normalize-space()='" + gender + "']")
    }

    txtPositionNU(){
        return cy.xpath("//input[@placeholder='position']")
    }

    txtMailNU(){
        return cy.xpath("//input[@data-vv-name='email']")
    }

    txtPhoneNU(){
        return cy.xpath("//input[@data-vv-name='mobile']")
    }

    lblUserCard(){
        return cy.xpath("//div[@class='userorgan-list-face']//div")
    }

    lblUserName(user){
        return cy.xpath("//span[contains(text(), '" + user + "')]")
    }

    lblStatus(){
        return cy.xpath("//li//span[@class='serial text-red']")
    }

    lblDepartment(){
        return cy.xpath("//div[@id='tab-0']")
    }

    lblUserOrgan(){
        return cy.xpath("//div[@class='userorgan-dept']")
    }

    txtUserOrgan(){
        return cy.xpath("(//input[contains(@placeholder,'name of the organization')])")
    }

    btnConfirmReviewUserOrgan(){
        return cy.xpath("//div[@aria-label='revise']//div[@class='el-dialog__footer']//button[@type='button']")
    }

    btnConfirmConfirmReviewUserOrgan(){
        return cy.xpath("(//span[contains(text(),'confirm')])[3]")
    }

    txtUserNameAddToUserOrgan(){
        return cy.xpath("(//input[contains(@placeholder,'organization')])[1]")
    }

    btnNewUserOrgan(action){
        return cy.xpath("//div[@class='userorgan-dept']//button//span[contains(text(), '" + action + "')]")
    }

    btnSaveAddUserOrgan(){
        return cy.xpath("//div[contains(@aria-label,'add an organization')]//span[contains(text(),'save')]")
    }

    lblUserOrganChild(){
        return cy.xpath('//div[@class="el-tree-node__children"]//div[@class="userorgan-dept"]')
    }

    btnConfirmDeleteUserOrganChild(){
        return cy.xpath("//button[contains(@class,'el-button el-button--default el-button--small el-button--primary')]//span[contains(text(),'confirm')]")
    }

    lblRole(){
        return cy.xpath("//div[@id='tab-1']")
    }

    btnAddNewRole(){
        return cy.xpath("//button//span[contains(text(),'+add new role')]")
    }

    ipRoleName(){
        return cy.xpath("//input[contains(@placeholder,'role name')]")
    }

    btnSaveRoleName(){
        return cy.xpath("//div[contains(@aria-label,'Add Role')]//div[contains(@class,'el-dialog__footer')]//button[contains(@type,'button')]")
    }

    btnConfirmSaveRoleName(){
        return cy.xpath("//button[@class='el-button el-button--default el-button--small el-button--primary ']//span[contains(text(),'confirm')]")
    }

    lblRoleName(){
        return cy.xpath("//div[@class='userorgan-enable-role']")
    }

    btnRole(action){
        return cy.xpath("//div[@class='userorgan-enable-role']//div//button//span[contains(text(),'" + action + "')]")
    }

    ipRoleNameRevise(){
        return cy.xpath("//div[contains(@aria-label,'reviserole')]//input[contains(@placeholder,'role name')]")
    }
    
    btnConfirmRevise(){
        return cy.xpath("//div[contains(@aria-label,'reviserole')]//div[contains(@class,'el-dialog__footer')]//button[contains(@type,'button')]")
    }

    btnCloseRoleOnUser(){
        return cy.xpath("//i[contains(@class,'el-tag__close el-icon-close')]")
    }

    btnCloseAddPopup(){
        return cy.xpath("(//i[@class='el-dialog__close el-icon el-icon-close'])[2]")
    }

    lblRoleCard(){
        return cy.xpath("//span[@class='el-tag el-tag--info el-tag--mini el-tag--light']")
    }

    btnDeleteRoleCard(){
        return cy.xpath("//span[@class='el-tag el-tag--info el-tag--mini el-tag--light']//i")
    }

    txtNewUserName(){
        return cy.xpath("//input[@data-vv-name='new_userName']")
    }
    btnSaveEdit(){
        return cy.xpath("//div[@aria-label='personnel modification']//div[@class='el-dialog__footer']//button[@type='button']")
    }

    lblNewUserName(){
        return cy.xpath("//span[@class='user-card-name']")
    }

    lblNewPhone(){
        return cy.xpath("//i[@class='el-icon-phone-outline user-attr-color']")
    }

    lblNewMail(){
        return cy.xpath("//i[@class='el-icon-message user-attr-color']")
    }

    erMesInvalidPhoneNumber(){
        return cy.xpath('//div[@x-placement="top-start"]')
    }

    ipCellPhone() {
        return cy.xpath("//input[contains(@placeholder,'cell phone')]")
    }

    lblRoleNameForAddData(){
        return cy.xpath("//div[contains(text(),'All Role')]")
    }

    //SCRIPTS
    addNewUser(name, mail, number){
        this.header.btnAtHeader("new").first().click()
        cy.wait(5000)
        this.txtNameNU().type(name)
        cy.wait(2000)
        this.txtMailNU().type(mail + "@email.com")
        cy.wait(2000)
        this.txtPhoneNU().type("66" + number)
        cy.wait(2000)
        this.common.btnConfirmAgainHintPopup("submit").click()
    }

    verifyUserJustHasBeenRemove(user){
        this.lblUserName(user).should('not.to.exist')
    }

    verifyDataInquire(name, mail, number, searchField, condition){
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblNewUserName().should('contain', name)
        cy.wait(2000)
        this.lblNewMail().should('contain', mail + "@email.com")
        cy.wait(2000)
        this.lblNewPhone().should('contain', "66" + number)
        cy.wait(2000)
    }

    removeUser(name, number, searchField, condition){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblUserCard().click()
        cy.wait(2000)
        this.header.btnAtHeader("remove").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().clear()
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.verifyUserJustHasBeenRemove(name)
    }

    deleteUser(searchField, condition){
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().clear()
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblUserCard().click({multiple : true, force : true})
        cy.wait(2000)
        this.header.btnAtHeader("remove").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
    }

    verifyStatusUser(status){
        this.lblStatus().should('have.text', status)
    }

    enableUser(name, number, searchField, condition){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblUserCard().click()
        cy.wait(2000)
        this.header.btnAtHeader("disabled").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.lblUserCard().click()
        cy.wait(2000)
        this.header.btnAtHeader("enable").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
        cy.wait(2000)
    }

    disabledUser(name, number, searchField, condition, status){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblUserCard().click()
        cy.wait(2000)
        this.header.btnAtHeader("disabled").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.common.msgSuccess().should('be.visible')
        cy.wait(2000)
        this.verifyStatusUser(status)
    }

    checkUniquePhone(name, number, searchField, condition){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.addNewUser(name, name, number)
        this.common.txtError().should('contain', 'data exception.Cell phone number already existed.')
        this.btnCloseAddPopup().click()
    }

    checkUniqueMail(name, number, searchField, condition){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        const phone = Math.floor(Math.random() * 100000000) + 10000000;
        this.addNewUser(name, name, phone)
        this.common.txtError().should('contain', 'Failed to save data.Email is already existed.')
        this.btnCloseAddPopup().click()
    }

    checkAddUserToDepartment(name, number, searchField, condition){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblDepartment().click()
        cy.wait(2000)
        this.lblUserOrgan().first().trigger('mouseover')
        this.btnNewUserOrgan("new").first().click({force : true})
        cy.wait(2000)
        this.txtUserNameAddToUserOrgan().type(name)
        cy.wait(2000)
        this.btnSaveAddUserOrgan().click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.lblUserOrganChild().last().should('contain', name)
    }

    checkReviseInDepartment(){
        this.lblDepartment().click()
        cy.wait(2000)
        this.lblUserOrgan().first().trigger('mouseover')
        this.btnNewUserOrgan("revise").first().click({force : true})
        cy.wait(2000)
        this.txtUserOrgan().type("User organization")
        cy.wait(2000)
        this.btnConfirmReviewUserOrgan().click()
        cy.wait(2000)
        this.btnConfirmConfirmReviewUserOrgan().click()
        cy.wait(2000)
        this.lblUserOrgan().first().should("contain", "User organization")  
    }

    checkDeleteChildUserOrgan(){
        cy.wait(5000)
        this.lblUserOrganChild().last().trigger('mouseover')
        cy.wait(2000)
        this.btnNewUserOrgan("delete").last().click({force : true})
        cy.wait(2000)
        this.btnConfirmDeleteUserOrganChild().click()
    }

    verifyDeleteChildUserOrgan(UO){
        this.common.msgSuccess().should('be.visible')
        this.lblUserOrganChild().should('not.equal', UO)
    }

    addNewRole(role){
        this.lblRole().click()
        cy.wait(2000)
        this.btnAddNewRole().click()
        cy.wait(2000)
        this.ipRoleName().type(role)
        cy.wait(2000)
        this.btnSaveRoleName().click()
        cy.wait(2000)
        this.btnConfirmSaveRoleName().click()
        this.common.msgSuccess().should('be.visible')
        cy.wait(5000)
        this.lblRoleName().first().should('contain', role)
    }

    checkReviseRole(role){
        this.lblRoleName().first().trigger('mouseover')
        cy.wait(2000)
        this.btnRole("rename").first().click({force : true})
        cy.wait(2000)
        this.ipRoleNameRevise().type(role)
        cy.wait(2000)
        this.btnConfirmRevise().click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
        cy.wait(5000)
        this.lblRoleName().first().should('contain', role)
    }

    checkDeleteRole(name){
        this.lblRoleName().first().trigger('mouseover')
        cy.wait(2000)
        this.btnRole("delete").first().click({force : true})
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
        cy.wait(5000)
        this.lblRoleName().first().then(($text)=>{
            if($text.text().trim != name){
                cy.log("Pass")
            }
            else{
                cy.log($text.text().trim)
                cy.log("Fail")
            }
        })
    }

    addUserToRole(name, number, searchField, condition, role){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.addNewRole(role)
        this.lblUserCard().trigger('dragstart')
        this.lblRoleName().first().trigger('drop')
        cy.wait(5000)
        this.lblRoleCard().should('contain', role)
        cy.wait(2000)
        this.lblRoleName().first().then(($text)=>{
            if($text.text().trim().substr(8,1) == "1"){
                cy.log("Pass")
            }
            else{
                cy.log($text.text().trim())
            }
        })
    }

    addUserToRoleForAddData(){
        this.lblRole().click()
        cy.wait(2000)
        this.lblUserCard().trigger('dragstart')
        this.lblRoleNameForAddData().scrollIntoView()
        this.lblRoleNameForAddData().trigger('drop')
    }

    checkUserOurRole(name, number, searchField, condition, role){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.addNewRole(role)
        this.lblUserCard().trigger('dragstart')
        this.lblRoleName().first().trigger('drop')
        cy.wait(5000)
        this.lblRoleCard().should('contain', role)
        cy.wait(2000)
        this.btnDeleteRoleCard().click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.lblRoleName().first().then(($text)=>{
            if($text.text().trim().substr(8,1) == "0"){
                cy.log("Pass")
            }
            else{
                cy.log($text.text().trim())
            }
        })
    }

    editUserInformation(name, number, searchField, condition, newName){
        this.addNewUser(name, name, number)
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblUserCard().dblclick()
        cy.wait(2000)
        this.txtNewUserName().click()
        this.txtNewUserName().clear()
        this.txtNewUserName().type(newName)
        cy.wait(2000)
        this.btnSaveEdit().click()
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(5000)
        this.header.ipSearchBox(searchField).first().click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).first().clear()
        this.header.ipSearchBox(searchField).first().type(newName, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(5000)
        this.lblNewUserName().should('contain', newName)
        cy.wait(2000)
        this.lblNewMail().should('contain', name + "@email.com")
        cy.wait(2000)
        this.lblNewPhone().should('contain', "66" + number)
        cy.wait(2000)
    }

    checkInValidPhoneNumber(number){
        this.header.btnAtHeader("new").first().click()
        cy.wait(5000)
        this.txtPhoneNU().type(number)
        this.txtNameNU().click()
        this.txtPhoneNU().click()
        this.erMesInvalidPhoneNumber().should('be.visible')
        this.erMesInvalidPhoneNumber().should('have.text', 'The mobile field format is invalid')
        this.btnCloseAddPopup().click()
    }

    checkUserWeb04E2E(cellPhone, driverName){
        this.header.ipSearchBox('cell phone').first().click()                                                                                
        this.header.ipSearchBox('cell phone').first().type("66" + cellPhone, { force: true })                                                                                
        this.header.btnAtHeader("inquire").click({ force: true })                                                                               
        this.lblNewUserName().should('contain', driverName)                                                                               
        this.lblNewPhone().should('contain', "66" + cellPhone)
    }
}

export default UserOrganization