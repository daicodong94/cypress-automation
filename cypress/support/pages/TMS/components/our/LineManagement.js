import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import CreateShippingOrder from "../shippingorder/CreateShippingOrder"
import ShippingOrder from "../shippingorder/ShippingOrder"

class LineManagement {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.shippingOrder = new ShippingOrder()
        this.createShippingOrder = new CreateShippingOrder()
        // this.site = new Site()
    }

    //XPATH
    spanSearchSiteResult(siteName) {
        return cy.xpath("(//div[@class='search-candidate-list']//div[@class='station-info']//span[text()='" + siteName + "'])[last()]")
    }

    ddlSite(number) {
        return cy.xpath("//div[@class='el-dialog__body']//tbody//tr[@class][" + number + "]//input[@readonly]")
    }

    ipCity() {
        return cy.xpath("//div[@class='el-dialog__body']//input[@placeholder='city']")
    }

    cityResult(city) {
        return cy.xpath("(//span[text()='" + city + "'])[last()]")
    }

    divTime(time) {
        return cy.xpath("(//div[text()='" + time + "'])[last()]")
    }

    txtLineName(){
        return cy.xpath("//input[@data-vv-name='name']")
    }

    cboCity(){
        return cy.xpath("(//input[@placeholder='city'])[2]")
    }

    lblCity(city) {
        return cy.xpath("//li//span[normalize-space()='" + city + "']")
    }

    txtLineType(){
        return cy.xpath("//input[@placeholder='line type']")
    }

    lblLineType(type){
        return cy.xpath("//li//span[normalize-space()='" + type + "']")
    }

    txtLineCode(){
        return cy.xpath("//input[@data-vv-name='lineCode']")
    }
    cboDefaultDepartureTime(){
        return cy.xpath("//input[@placeholder='default departure time']")
    }

    lblDefaultDepartureTime(time){
        return cy.xpath("//div[contains(text(),'" + time + "')]")
    }

    lnkNewRoute(){
        return cy.xpath("//button//span[contains(text(),'+ save')]")
    }

    cboRouteSite(){
        return cy.xpath("(//input[@placeholder='site'])[1]")
    }

    cboRouteSiteEdit(){
        return cy.xpath("(//input[@placeholder='please choose'])[last()]")
    }

    txtRouteSite(){
        return cy.xpath("(//div[@class='search-cadidate-keyword']//input)[last()]")
    }

    spanSearchLocationResult() {
        return cy.xpath("(//div[@class='search-candidate-list'])[last()]//div[@class='station-info']//span[@class='search-highlight']")
    }

    columnLineName(){
        return cy.xpath("//td[2]//div")
    }

    columnStatus(){
        return cy.xpath("//td[3]//div")
    }

    columnLineCode(){
        return cy.xpath("//td[4]//div")
    }

    columnLineType(){
        return cy.xpath("//td[5]//div")
    }

    columnLineCity(){
        return cy.xpath("//td[6]//div")
    }

    columnPoints(){
        return cy.xpath("//td[7]//div")
    }

    ipFile(){
        return cy.xpath("//input[contains(@name,'file')]")
    }

    lblImportSuccess(){
        return cy.xpath("//div[contains(text(),'import successful')]")
    }

    btnCloseImport(){
        return cy.xpath("(//button[@class='el-dialog__headerbtn'])[last()]")
    }

    lblLineName(line){
        return cy.xpath("//span[contains(text(), '" + line + "')]")
    }

    btnSave(){
        return cy.xpath("//span[normalize-space()='save']")
    }

    lblComboboxEdit(){
        return cy.xpath("//div[@data-vv-name='city']")
    }

    cboCityEdit(){
        return cy.xpath("(//td[contains(text(),'city')]/following-sibling::td//div//input)[1]")
    }

    btnSaveEdit(){
        return cy.xpath("//button[contains(@class,'el-button common-button-style el-button--primary el-button--small')]//span[contains(text(),'save')]")
    }

    btnDeleteLine(){
        return cy.xpath("(//button[contains(@title,'delete')])[last()]")
    }

    lblLongitude(){
        return cy.xpath("//td[3]//div")
    }

    lblLatitude(){
        return cy.xpath("//td[4]//div")
    }

    btnCloseAddNewPopup(){
        return cy.xpath("//div[@aria-label='add line']//i[@class='el-dialog__close el-icon el-icon-close']")
    }
    

    //SCRIPTS

    addNewLine(line, time){
        this.header.btnAtHeader("new").click()
        cy.wait(2000)
        this.txtLineName().type(line)
        cy.wait(2000)
        this.cboCity().click()
        this.cboCity().type("เมืองอัตโนมัติ")
        cy.wait(2000)
        this.lblCity("เมืองอัตโนมัติ").last().click({force : true})
        cy.wait(2000)
        this.txtLineType().click()
        this.lblLineType("station").click({force : true})
        cy.wait(2000)
        this.txtLineCode().type(line)
        cy.wait(2000)
        this.cboDefaultDepartureTime().click()
        this.lblDefaultDepartureTime(time).click()
        cy.wait(2000)
        this.lnkNewRoute().click()
        cy.wait(2000)
        this.cboRouteSite().click()
        cy.wait(2000)
        this.txtRouteSite().type("TestT1")
        cy.wait(2000)
        this.spanSearchLocationResult().click()
        cy.wait(2000)
        this.common.btnConfirmAgainHintPopup("save").last().click()
    }

    searchLine(searchField, condition){
        this.header.ipSearchBox(searchField).click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).clear()
        this.header.ipSearchBox(searchField).type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(2000)
    }

    verifyNewLine(line){   
        this.searchLine("line name", line)
        cy.wait(5000) 
        this.columnLineName().first().should('have.text', line)
        cy.wait(2000)
        this.columnStatus().first().should('have.text', "created")
        cy.wait(2000)
        this.columnLineCode().first().should('have.text', line)
        cy.wait(2000)
        this.columnLineType().first().should('have.text', "station")
        cy.wait(2000)
        this.columnLineCity().first().should('have.text', 'คู่มือเมือง')
        cy.wait(2000)
        this.columnPoints().first().should('have.text', '1')
    }

    verifyEditLine(searchField, condition, line){     
        this.header.ipSearchBox(searchField).click()
        cy.wait(2000)
        this.header.ipSearchBox(searchField).clear()
        this.header.ipSearchBox(searchField).type(condition, { force: true })
        cy.wait(2000)
        this.header.btnAtHeader("inquire").click({ force: true })
        cy.wait(2000)
        this.columnLineName().first().should('have.text', line)
        cy.wait(2000)
        this.columnStatus().first().should('have.text', "created")
        cy.wait(2000)
        this.columnLineCode().first().should('have.text', line)
        cy.wait(2000)
        this.columnLineType().first().should('have.text', "station")
        cy.wait(2000)
        this.columnLineCity().first().should('have.text', 'คู่มือเมือง')
        cy.wait(2000)
        this.columnPoints().first().should('have.text', '2')
    }

    importLine(filePath){
        this.header.btnAtHeader("import").click()
        cy.wait(2000)
        this.ipFile().attachFile(filePath)
        cy.wait(5000)
        this.lblImportSuccess().should('be.visible')
        this.btnCloseImport().click()
    }

    importFailFile(filePath){
        this.header.btnAtHeader("import").click()
        cy.wait(2000)
        this.ipFile().attachFile(filePath)
        this.common.txtError().should('have.text', 'it must be an excel file, it is recommended to download the template first and then fill in!')
        this.btnCloseImport().click()
    }

    verifyLineJustHasBeenDeleted(line){
        this.lblLineName(line).should('not.to.exist')
    }

    deleteLine(searchField, condition){
        this.searchLine(searchField, condition)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        this.header.btnAtHeader("delete").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
        this.searchLine(searchField, condition)
        this.verifyLineJustHasBeenDeleted(condition)
    }

    enabledLine(searchField, condition){
        this.searchLine(searchField, condition)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        this.header.btnAtHeader("enable").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
    }

    disabledLine(searchField, condition){
        this.enabledLine(searchField, condition)
        cy.wait(5000)
        this.common.searchResultWithOutValue().first().click({force : true})
        this.header.btnAtHeader("disabled").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('be.visible')
    }

    addRoute(searchField, condition){
        this.searchLine(searchField, condition)
        this.common.openDetailView()
        cy.wait(2000)
        this.lnkNewRoute().click()
        cy.wait(2000)
        this.cboRouteSiteEdit().click()
        cy.wait(2000)
        this.txtRouteSite().type("TestT2")
        cy.wait(2000)
        this.spanSearchLocationResult().click()
        cy.wait(2000)
        this.btnSaveEdit().click()
        cy.wait(5000)
    }

    verifyNewRoute(){
        this.common.openDetailView()
        cy.wait(2000)
        this.lblLongitude().last().should('be.visible').then(($test)=>{
            cy.log($test.text().trim())
        })
        this.lblLatitude().last().should('be.visible').then(($test)=>{
            cy.log($test.text().trim())
        })    
    }

    editLine(searchField, condition, line, time){
        this.searchLine(searchField, condition)
        this.common.openDetailView()
        this.txtLineName().click()
        this.txtLineName().clear()
        this.txtLineName().type(line)
        cy.wait(2000)
        this.lblComboboxEdit().click({force : true})
        this.cboCityEdit().type("คู่มือเมือง",{force : true})
        cy.wait(2000)
        this.lblCity("คู่มือเมือง").last().click({force : true})
        cy.wait(2000)
        this.txtLineCode().click()
        this.txtLineCode().clear()
        this.txtLineCode().type(line)
        cy.wait(2000)
        this.cboDefaultDepartureTime().click()
        this.lblDefaultDepartureTime(time).click()
        cy.wait(2000)
        this.lnkNewRoute().click()
        cy.wait(2000)
        this.cboRouteSiteEdit().click()
        cy.wait(2000)
        this.txtRouteSite().type("TestT2")
        cy.wait(2000)
        this.spanSearchLocationResult().click()
        cy.wait(2000)
        this.btnSaveEdit().click()
        cy.wait(5000)
    }

    deleteRoute(){
        this.common.openDetailView()
        cy.wait(2000)
        this.btnDeleteLine().click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
        cy.wait(2000)
        this.cboRouteSiteEdit().then(($text)=>{
            if($text.text().trim() != "TestT2"){
                cy.log("Pass")
            }
            else{
                cy.log($text.text().trim())
                cy.log("Fail")
            }
        })
    }

    chooseSite(site, number) {
        this.ddlSite(number).click()
        cy.wait(1000)
        this.createShippingOrder.txtSearchLocation().type(site)
        cy.wait(1000)
        this.spanSearchSiteResult(site).click()
    }

    createLineManagementContain3Sites() {
        // this.header.btnAtHeader("new").click()
        cy.wait(1000)
        // this.common.textField("please enter").type(lineName)
        // this.common.textField("line code").type(lineCode)

        // this.ipCity("city").click()
        // this.ipCity("city").focus().type("蚌埠市")
        // cy.wait(1000)
        // this.cityResult().click()
        // cy.wait(1000)

        // this.common.textField("default departure time").click()
        // this.divTime("01:00").click()
        // //site1
        // this.common.lnkButton("+ save").click()
        // cy.wait(1000)
        // this.chooseSite(site1, 1)
        // cy.wait(1000)
        // //site 2
        // this.common.lnkButton("+ save").click()
        // cy.wait(1000)
        // this.chooseSite(site2, 2)
        // cy.wait(1000)
        // //site 3
        // this.common.lnkButton("+ save").click()
        // cy.wait(1000)
        // this.chooseSite(site3, 3)
        // cy.wait(2000)
        //save
        // this.common.btnDialogFooter(" save").click()
        // this.header.msgSuccess().should("be.visible")
        // cy.wait(1000)
        // this.enableLine(lineName)
    }

    enableLine(lineName) {
        this.shippingOrder.searchByInputCondition("line name", lineName, "yes")
        this.header.searchResult(lineName).click()
        cy.wait(1000)
        this.header.btnAtHeader("enable").click()
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click()
        this.header.msgSuccess().should("be.visible")
    }

    deleteSite(lineName) {
        this.shippingOrder.searchByInputCondition("line name", lineName, "yes")
        this.header.searchResult(lineName).click()
        cy.wait(1000)
        this.header.btnAtHeader("delete")
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click()
        this.header.msgSuccess().should("be.visible")
    }
}

export default LineManagement;