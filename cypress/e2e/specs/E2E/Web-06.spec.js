import E2E from "../../../support/pages/E2E/E2E"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"

describe("Flow to complete order in TMS When Confirm Order directly", () => {
    var data, objLogin, objHome = new Homepage(),
        objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objDeliveryPlan = new DeliveryPlan(), objSchedule = new Schedule()
    var objE2E = new E2E()
    var filePath = "implement/images.png"
    var trackNumber, temperature = "Chill", url = new Utility().getBaseUrl()

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            cy.visit(url)
            objLogin = new LoginSupplier()
            objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
        })
    })

    it('WEB-06: Route Split Order Use Data available', () => {
        cy.randomStringAndNumber(8).then((track) => {
            trackNumber = track
            //create shipping order
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithCargoDetailForQuantitySplit("Confirm", track, "Automation Company", "Service for Shipper", temperature, "AutoTestSite99", "SiteTestAuto", 20, 10, 20, 30)
            objShippingOrder.leftMenu.closeTab()
            // 10. Go to Plan > Delivery Plan 
            //  10.1 Click Inquire > Select Order1 > Create a dispatch Order
            //  10.2 On the Disptach Order > Select Dispatch Number of Order > Click Confirm
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.routeSplit("waybill number", track, "first", "AutoTestSite99")
            cy.reload()
            objDeliveryPlan.common.searchByClickSuggestionAtDeliveryPlanPageForOrderSplit("waybill number", trackNumber, "first", 1)
            //create dispach order
            objDeliveryPlan.createADispatchOrder(trackNumber)
            cy.wait(3000 / 2)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackNumber, "second")
            objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
            objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
            //confirm dispatch
            cy.wait(1000)
            objDeliveryPlan.confirmDispatch()
            objDeliveryPlan.common.msgSuccess().should("be.visible")
            var apiLogin = "https://account-stg.allnowgroup.com/mobile/driver-login"
            var driverID = "", accessToken = ""
            var mobilePhone = "778847221"
            var password = "20000412"
            cy.log("password:" + password)
            var body1 = {
                "countryCode": 66,
                "phone": mobilePhone,
                "password": password,
                "accessOtherDevice": true,
                "deviceId": "b791071c2d28be26"
            }
            const options = {
                method: 'POST',
                url: apiLogin,
                body: body1
            }
            cy.request(options).then((response) => {
                expect(response.status).equal(200)
                driverID = response.body.items.user.driverId
                accessToken = response.body.items.token.access_token
                cy.log("driverID1 is: " + driverID, "accessToke is: " + accessToken)

                objDeliveryPlan.leftMenu.closeTab()
                objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("Dispatch Number", track)
                cy.wait(1000)

                objSchedule.columnDispatchNumber().then((dispatchNumber) => {
                    var dispatch = dispatchNumber.text().trim()

                    //13. Go to DriverA in DriverApp again > Click Accept
                    //verify fmctoken
                    var authorize = accessToken
                    var fcmTokenLink = "" + url + "/wapi/api/v1/notification/fcmtoken"
                    var bodyFMCToken = {
                        "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                        "driverID": driverID,
                        "deviceID": "b791071c2d28be26",
                        "createdDatetime": "2023-03-21T16:14:45.073Z"
                    }
                    const options2 = {
                        method: 'POST',
                        url: fcmTokenLink,
                        headers: { authorization: authorize },
                        body: bodyFMCToken
                    }
                    cy.log("options2 is: " + JSON.stringify(options2))
                    cy.request(options2).then((response) => {
                        expect(response.status).equal(200)
                    })

                    objSchedule.common.openDetailView()
                    objSchedule.header.searchResult(track).should("be.visible")
                    cy.wait(1000)
                    //assign carrier + driver
                    objSchedule.selectDataAndSchedule("WIDGUU8", "Service for Carrier", "DriverYZCRLGBNGS", "Confirm and Dispatch")
                    objSchedule.common.msgSuccess().should("be.visible")
                    //Call API pending message
                    var pendingMessage = "" + url + "/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                    const optionsPendingMessage = {
                        method: 'GET',
                        url: pendingMessage,
                        headers: { authorization: authorize }
                    }
                    cy.log("option pending msg is: " + JSON.stringify(optionsPendingMessage))
                    cy.request(optionsPendingMessage).then((response) => {
                        expect(response.status).equal(200)
                        expect(response.body.result).not.be.empty
                        var messageID = response.body.result[0].msgId
                        cy.log("message id is: " + messageID)

                        //Accept message
                        var acceptMessage = "" + url + "/wapi/api/v1/notification/accepted-message"
                        var bodyAcceptMsg = {
                            "msgID": messageID,
                            "jobNo": dispatch,
                            "ackDateTime": "2023-03-21T16:43:46.073Z"
                        }
                        const optionsAcceptMessage = {
                            method: 'POST',
                            url: acceptMessage,
                            headers: { authorization: authorize },
                            body: bodyAcceptMsg
                        }
                        cy.log("option accept msg is: " + JSON.stringify(optionsAcceptMessage))
                        cy.request(optionsAcceptMessage).then((response) => {
                            expect(response.status).equal(200)
                            expect(response.body.result).equal(1)
                        })
                    })

                    // 12. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                    //  12.1 Click Update Driver 1st time --> Status Shipment = Shipment_Arrival_Pickup
                    //  12.2 Click Update Driver 2nd time --> Status Shipment = Shipment_Loading
                    // 13. Go to Implement > Pickup > Select Dispatch Number of Order1  > Click Pickup Completed --> Status Shipment = Shipment_Ontheway
                    // 14. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                    //   14.1 Click Update Driver 1st time  --> Status Shipment = Shipment_arrival_destination
                    //   14.2 Click Update Driver 2nd time --> Status Shipment = Shipment_unloading
                    // 15. Go to Implement > Delivery Goods > Select Dispatch Number of Order1  > Click Delivery Completed
                    // 16. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD
                    // 17. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order
                    objE2E.updateShipmentStatusToShipmentDelivered(dispatch, filePath, "yes", "split", "inprogress")
                    //15 to 26
                    objDeliveryPlan.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                    objDeliveryPlan.common.searchByClickSuggestionAtDeliveryPlanPageForOrderSplit("waybill number", track, "first", 1)
                    //create dispach order
                    objDeliveryPlan.createADispatchOrder(track)
                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", track, "second")
                    objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
                    cy.wait(1000)
                    objDeliveryPlan.header.searchResult(track).should("be.visible")
                    //confirm dispatch
                    objDeliveryPlan.confirmDispatch()

                    cy.request(options).then((response) => {
                        expect(response.status).equal(200)
                        driverID = response.body.items.user.driverId
                        accessToken = response.body.items.token.access_token
                        cy.log("driverID2 is: " + driverID, "accessToke is: " + accessToken)

                        objDeliveryPlan.leftMenu.closeTab()
                        objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                        objSchedule.common.searchByClickSuggestion("Dispatch Number", track)
                        cy.wait(1000)
                        objSchedule.columnDispatchNumber().then((dispatchNumber2) => {
                            var dispatch2 = dispatchNumber2.text().trim()

                            //13. Go to DriverA in DriverApp again > Click Accept
                            //verify fmctoken
                            var authorize2 = accessToken
                            var fcmTokenLink2 = "" + url + "/wapi/api/v1/notification/fcmtoken"
                            var bodyFMCToken2 = {
                                "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                                "driverID": driverID,
                                "deviceID": "b791071c2d28be26",
                                "createdDatetime": "2023-03-21T16:14:45.073Z"
                            }
                            const options3 = {
                                method: 'POST',
                                url: fcmTokenLink2,
                                headers: { authorization: authorize2 },
                                body: bodyFMCToken2
                            }
                            cy.log("options3 is: " + JSON.stringify(options2))
                            cy.request(options3).then((response) => {
                                expect(response.status).equal(200)
                            })

                            objSchedule.common.openDetailView()
                            objSchedule.header.searchResult(track).should("be.visible")
                            cy.wait(1000)
                            //assign carrier + driver
                            objSchedule.selectDataAndSchedule("WIDGUU8", "Service for Carrier", "DriverYZCRLGBNGS", "Confirm and Dispatch")
                            objSchedule.common.msgSuccess().should("be.visible")
                            //Call API pending message
                            var pendingMessage2 = "" + url + "/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                            const optionsPendingMessage2 = {
                                method: 'GET',
                                url: pendingMessage2,
                                headers: { authorization: authorize }
                            }
                            cy.log("option pending msg is: " + JSON.stringify(optionsPendingMessage))
                            cy.request(optionsPendingMessage2).then((response) => {
                                expect(response.status).equal(200)
                                expect(response.body.result).not.be.empty
                                var messageID2 = response.body.result[0].msgId
                                cy.log("message id is: " + messageID2)

                                //Accept message
                                var acceptMessage2 = "" + url + "/wapi/api/v1/notification/accepted-message"
                                var bodyAcceptMsg2 = {
                                    "msgID": messageID2,
                                    "jobNo": dispatch2,
                                    "ackDateTime": "2023-03-21T16:43:46.073Z"
                                }
                                const optionsAcceptMessage2 = {
                                    method: 'POST',
                                    url: acceptMessage2,
                                    headers: { authorization: authorize },
                                    body: bodyAcceptMsg2
                                }
                                cy.log("option accept msg is: " + JSON.stringify(optionsAcceptMessage2))
                                cy.request(optionsAcceptMessage2).then((response) => {
                                    expect(response.status).equal(200)
                                    expect(response.body.result).equal(1)
                                })
                            })

                            // 9. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                            //  9.1 Click Update Driver 1st time --> Status Shipment = Shipment_Arrival_Pickup
                            //  9.2 Click Update Driver 2nd time --> Status Shipment = Shipment_Loading
                            // 10. Go to Implement > Pickup > Select Dispatch Number of Order1  > Click Pickup Completed --> Status Shipment = Shipment_Ontheway
                            // 11. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                            //   11.1 Click Update Driver 1st time  --> Status Shipment = Shipment_arrival_destination
                            //   11.2 Click Update Driver 2nd time --> Status Shipment = Shipment_unloading
                            // 12. Go to Implement > Delivery Goods > Select Dispatch Number of Order1  > Click Delivery Completed
                            // 13. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD
                            // 14. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order
                            objE2E.updateShipmentStatusToShipmentDelivered(dispatch2, filePath, "yes", "split", "success")
                        })
                    })
                })
            })
        })
    })
})