import LoginSupplier from "./LoginSupplier"

class ResetNewPassword {

    constructor() {
        this.Login = new LoginSupplier()
    }
    txtNewPassword() {
        return cy.xpath('(//input[@type="password"])[1]')
    }

    txtConfirmNewPassword() {
        return cy.xpath('(//input[@type="password"])[2]')
    }

    btnSetPassword() {
        return cy.xpath('//button[@type="button"]//div[text() =" Set Password "]')
    }

    titleSetPasswordSreen() {
        return cy.xpath('//div[contains(text(),"Set your password")]')
    }

    btnShowPassword() {
        return cy.xpath('(//img[@src="/assets/EyeInvisible.ab4b2d8a.svg"])[1]')
    }

    btnShowConfirmPassword() {
        return cy.xpath('(//img[@src="/assets/EyeInvisible.ab4b2d8a.svg"])[2]')
    }

    btnHidePassword() {
        return cy.xpath('(//i[@class="mdi-eye-outline mdi v-icon notranslate v-theme--light text-secondary-45"])[1]')
    }

    btnHideConfirmPassword() {
        return cy.xpath('(//i[@class="mdi-eye-outline mdi v-icon notranslate v-theme--light text-secondary-45"])[2]')
    }

    titleSetPasswordSuccess() {
        return cy.xpath('//div[contains(text(),"Thank You")]')
    }

    btnContinueAllnowTMS() {
        return cy.xpath('//button[@type="button"]//div[text() = "CONTINUE TO ALL NOW TMS"]')
    }

    chkValidationPassword() {
        return cy.xpath('//input[@type="checkbox"]')
    }

    lblLengthPassword() {
        return cy.xpath('(//div[@class="v-counter"])[1]')
    }

    lblPasswordNotMatched() {
        return cy.xpath('//div[contains(text(),"Passwords do not match")]')
    }

    ipNewPassword(password) {
        this.txtNewPassword().type(password)
    }

    ipConfirmPassword(confirmpassword) {
        this.txtConfirmNewPassword().type(confirmpassword)
    }

    shouldBeTitleSetPasswordScreen() {
        this.titleSetPasswordSreen().should('be.visible')
    }

    clickButtonSetPassword() {
        this.btnSetPassword().click()
    }

    shouldBeTitleSetPasswordSuccess() {
        this.titleSetPasswordSuccess().should('be.visible')
    }

    clickbtnContinueAllNow() {
        this.btnContinueAllnowTMS().click()
    }

    btnSetPasswordShouldBeDisabled() {
        cy.xpath('//button[@class="v-btn v-btn--disabled v-theme--light bg-primary v-btn--density-default v-btn--size-default v-btn--variant-contained"]').should('be.disabled')
    }

    shouldBePasswordNotMatched() {
        this.lblPasswordNotMatched().contains('Passwords do not match')
    }

    setNewPassword(password, confirmPassword) {
        this.ipNewPassword(password)
        this.ipConfirmPassword(confirmPassword)
        cy.intercept('POST', '/forgotPassword/modifyPassword', {code: "123456", message: "update new password success", item: true}).as('modifyPassword')
        this.clickButtonSetPassword()
        cy.wait('@modifyPassword').then((res) => {
            console.log(JSON.stringify(res))
            cy.log(JSON.stringify(res))
        })
        cy.wait(2000)
        this.shouldBeTitleSetPasswordSuccess()
        this.clickbtnContinueAllNow()
        cy.wait(5000)
    }

}
export default ResetNewPassword;