import LeftMenu from "../../../TMS/components/fragments/LeftMenu";
import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import CreateSite from "./CreateSite";

class SiteList {
    constructor() {
        this.common = new CommonMasterData()
        this.createSite = new CreateSite()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    titlePage(title) {
        return cy.xpath('//div//span[text()="'+ title +'"]')
    }

    siteID() {
        return cy.xpath('//p[@class="text-base text-neutral-13 font-medium"]')
    }

    searchNoData() {
        return cy.xpath('//div[contains(text(),"No Data")]')
    }

    titleSearchFilter(title) {
        return cy.xpath('(//span[contains(text(),"'+ title +'")])[last()]')
    }

    dropdownFilter(name, location) {
        return cy.xpath('(//input[@placeholder="'+ name +'"])['+ location +']')
    }

    clearValueDropdown(location) {
        return cy.xpath('(//div[@class="el-input el-input--base el-input--suffix"])['+ location +']')
    }

    iconDelete() {
        return cy.xpath('(//*[local-name()="svg" and @class="cursor-pointer"])[1]')
    }

    fileNameAttach(name) {
        return cy.xpath('//div[contains(text(),"'+ name +'")]')
    }

    sortAscending(name) {
        return cy.xpath('//div[contains(text(),"'+ name +'")]//i[@class="sort-caret ascending"]')
    }

    sortDescending(name) {
        return cy.xpath('//div[contains(text(),"'+ name +'")]//i[@class="sort-caret descending"]')
    }

    effectiveDate() {
        return cy.xpath('(//div[@class="text-black"])[1]')
    }

    endDate() {
        return cy.xpath('(//div[@class="text-black"])[2]')
    }

    dateSortFunction(date) {
        return cy.xpath('(//div[contains(text(),"'+ date +'")])[1]')
    }

    scrollbar(location) {
        return cy.xpath('//div[@class="el-table__body-wrapper is-scrolling-'+ location +'"]')
    }

    statusSite(status) {
        return cy.xpath('(//div[@class="cell"]//span[contains(text(),"'+ status +'")])[1]')
    }

    rdBtnSetStatus(name) {
        return cy.xpath('//span[@class="el-radio__label" and contains(text(),"'+ name +'")]')
    }

    setStatusChecked(name) {
        return cy.xpath('//label[@class="el-radio is-checked"]//span[contains(text(),"'+ name +'")]')
    }

    btnSetStatus() {
        return cy.xpath('//button//span[text()="Set"]')
    }

    btnNextMonth() {
        return cy.xpath('//button[@aria-label="Next Month"]')
    }

    dateFuture() {
        return cy.xpath('//span[contains(text(),"25")]')
    }

    selectHourAndMinutesEffective(time, location) {
        return cy.xpath('(//div[@class="absolute"]//div[contains(text(),"'+ time +'")])['+ location + ']')
    }

    timeEffective() {
        return cy.xpath('//div[@class="text-base text-primary-85"]')
    }

    spanBtnOk() {
        return cy.xpath('//span[contains(text(),"Ok")]')
    }

    btnHistoryLog() {
        return cy.xpath('//div[contains(text(),"View History Log")]')
    }

    siteName(name) {
        return cy.xpath('//p[contains(text(),"'+ name +'")]')
    }

    titleSiteHistoryLog() {
        return cy.xpath('//p[contains(text(),"History")]')
    }

    activeType(status) {
        return cy.xpath('//div[contains(text(),"'+ status +'")]')
    }

    moveToPageWhenClickPageNumber(number, pageNumber) {
        this.common.pageNumber(number).click()
        cy.wait(2000)
        this.common.pageActive().should("have.text", pageNumber)
    }

    ipPageExceed() {
        this.common.ipPageNumber().invoke('attr', 'max').then((pageMax) => {
            var pageExceed = parseInt(pageMax) + 1
            cy.log("page exceed =" + pageExceed)
            this.common.ipPageNumber().clear()
            this.common.ipPageNumber().type(pageExceed)
            this.common.ipPageNumber().type('{enter}')
            cy.wait(2000)
            this.common.pageActive().should("have.text", pageMax)
        })
    }

    searchBySiteID() {
        this.siteID().invoke('text').then((SiteID) => {
            cy.go('back')
            this.common.searchByCondition(SiteID)
            this.common.searchResultWithOutValue().should("be.visible")
        })
    }

    clickFilter() {
        this.header.btnFilter().should('exist').click()
    }

    searchingByValueOfDropdown(name, location, value) {
        this.dropdownFilter(name, location).click()
        this.dropdownFilter(name, location).type(value)
        this.titleSearchFilter(value).click()
        this.common.searchResultWithOutValue().should("be.visible")
    }

    clickResetFilter() {
        this.header.btnResetFilter().click()
    }

    shouldUIPopupSite() {
        this.common.titlePopupImportFile("Import Site").should("be.visible")
        this.common.btnDownloadTemplate().should("be.visible")
        this.common.btnClicktoUpLoad().should("be.visible")
        this.common.btnCancel().should("be.visible")
        this.common.btnImportPopupAttachFileDisabled().should("be.disabled")
    }

    clickBtnImport() {
        this.header.btnImport().click()
    }

    clickIconDelete() {
        this.iconDelete().click()
    }

    sortByCreateDate(name) {
        this.effectiveDate().invoke('text').then((date) => {
            var newdate = date.trim().substring(0, 13)
            cy.go('back')
            cy.wait(1000)
            this.sortDescending(name).click()
            cy.wait(1000)
            this.dateSortFunction(newdate).eq(0)
            this.sortAscending(name).click()
            cy.wait(2000)
            this.dateSortFunction(newdate).should("not.exist")
        })
    }

    sortByStatus(name) {
        this.scrollbar("left").scrollTo('right', {easing: 'linear' })
        cy.wait(1000)
        this.sortDescending(name).click()
        cy.wait(1000)
        this.statusSite("Pending").eq(0)
        this.sortAscending(name).click()
        cy.wait(1000)
        this.statusSite("Active").eq(0)
        // this.statusSite("Active").eq(0)
        // this.sortAscending(name).click()
        // cy.wait(1000)
        // this.statusSite("Inactive").eq(0)
    }

    scrollFunction() {
        this.scrollbar("left").scrollTo('right', {duration: 3000 })
        cy.wait(1000)
        this.scrollbar("right").scrollTo('bottom', {duration: 3000 })
        cy.wait(1000)
    }

    selectAndSetStatus(status, message, condition) {
        if(status == "Active") {
            this.rdBtnSetStatus("Inactive").click()
            this.btnSetStatus().click()
            this.common.msgSuccess(message).should("be.visible")
            this.common.searchByCondition(condition)
            // this.scrollbar("left").scrollTo('right', {duration: 3000 })
            this.titleSearchFilter("Inactive").should("be.visible")
        } else if(status == "Inactive") {
            this.rdBtnSetStatus("Active").click()
            this.btnSetStatus().click()
            this.common.msgSuccess(message).should("be.visible")
            this.common.searchByCondition(condition)
            // this.scrollbar("left").scrollTo('right', {duration: 3000 })
            this.titleSearchFilter("Active").should("be.visible")
        }
    }

    selectAndSetStatusPending(message, condition, status) {
        this.rdBtnSetStatus("Active").click()
        cy.wait(2000)
        this.timeEffective().click()
        cy.wait(1000)
        this.btnNextMonth().click()
        cy.wait(1000)
        this.dateFuture().click()
        this.selectHourAndMinutesEffective("02", "1").click()
        this.selectHourAndMinutesEffective("02", "2" ).click()
        this.spanBtnOk().click()
        cy.wait(2000)
        this.btnSetStatus().click()
        this.common.msgSuccess(message).should("be.visible")
        this.common.searchByCondition(condition)
        this.titleSearchFilter(status).should("be.visible")
    }

    btnSearch() {
        return cy.xpath('//button[@class="el-button flex items-center el-button--primary el-button--mini"]')
    }

    siteHistoryLogScreenUI() {
        this.titleSiteHistoryLog().should("be.visible")
        this.btnSearch().should("be.visible")
        this.header.txtSearch().should("be.visible")
    }

    editSite() {
        this.createSite.btnEditSite().click()
        this.createSite.ipInformation("Enter Customer name").clear()
        this.createSite.ipCustomerName("Enter Customer name", "EtoECustomerTH53311640")
        cy.wait(2000)
        this.createSite.btnSave().click()
        this.createSite.btnEditSite().click()
        this.createSite.ipInformation("Enter Customer name").clear()
        this.createSite.ipCustomerName("Enter Customer name", "EtoECustomerTH53311640")
        cy.wait(2000)
        this.createSite.btnSave().click()
        cy.wait(2000)
        this.common.msgSuccess("Update site successfully").should("be.visible")
    }

    clickCreateSite() {
        this.header.btnCreate().click({force : true})
        return new CreateSite()
    }

    searchResultWithOutValue() {
        return cy.xpath('(//table//tr[@class="el-table__row"])[1]')
    }

}
export default SiteList;