import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import ServiceItem from "../../../support/pages/TMS/components/our/ServiceItem";
import ServiceProduct from "../../../support/pages/TMS/components/our/ServiceProduct";
import { Utility } from "../../../support/utility"

describe("Checking Our - Service item", () => {

    var objServiceItem = new ServiceItem(), objServiceproduct = new ServiceProduct(), data, objLogin, objHome
    

    beforeEach("TMS-Our-Service item", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })

    afterEach(()=>{
        cy.wait(10000)
        objHome.logout()
    })

    it("TC_120: Check new to add a service entry", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.addNewItem("Carton", "place an order", "carrier", "any", code, "base")
        objServiceItem.verifyAddNewItem("Carton", "place an order", "base")
        objServiceItem.deleteItem()
    })

    it("TC_121: Check detele success of service items's", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.addNewItem("Carton", "place an order", "carrier", "any", code, "base")
        cy.wait(5000)
        objServiceItem.searchItem("Carton")
        cy.wait(5000)
        objServiceItem.deleteItem()
        objServiceItem.verifyDeleteItem("Carton")
    })

    it("TC_122: Check disable success of service items's", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.wait(5000)
        cy.randomString(5).then((name) => {
            objServiceItem.addNewItem(name + "_item_auto", "place an order", "carrier", "any", code, "base")
            cy.wait(5000)
            objServiceItem.searchItem(name + "_item_auto")
            cy.wait(5000)
            objServiceItem.disabledServiceItem()
        })
    })

    it("TC_123: Check enable success of service items's", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.wait(5000)
        cy.randomString(5).then((name) => {
            objServiceItem.addNewItem(name + "_item_auto", "place an order", "carrier", "any", code, "base")
            cy.wait(5000)
            objServiceItem.searchItem(name + "_item_auto")
            cy.wait(5000)
            objServiceItem.enableServiceItem()
        })
    })

    it("TC_124: Check inquire value of service items's", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.verifyInquire("Carton", "place an order", "carrier", "any", code, "base")
        cy.wait(5000)
        objServiceItem.deleteItem()
    })

    it("TC_125: Check change serial", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.verifySerial("Carton", "place an order", "carrier", "any", code, "base", "29")
        cy.wait(5000)
        objServiceItem.common.searchResultWithOutValue().click({force : true})
        objServiceItem.deleteItem()
    })

    it("TC_126: Check edit information of service item", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        const newCode = "Prod " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 10)
        objServiceItem.editServiceItem("Carton", "place an order", "carrier", "any", code, "base", "Shield", "delivery", "our", "none", newCode, "exception")
        objServiceItem.verifyAddNewItem("Shield", "delivery", "exception")
        objServiceItem.deleteItem()
    })

    it("TC_127: Check the effect to Service product screen when creating new record at Service item page ", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const codeItem = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        cy.wait(5000)
        cy.randomString(5).then((name) => {
            objServiceItem.addNewItem(name + "_item_auto", "place an order", "carrier", "any", codeItem, "base")
            cy.wait(5000)
            objServiceItem.searchItem(name + "_item_auto")
            cy.wait(5000)
            objServiceItem.enableServiceItem()
            cy.wait(5000)
            cy.reload()
            objServiceproduct = objHome.leftMenu.navigateToSubPage("Master Data","Service Product")
            cy.wait(2000)
            objServiceproduct.verifyServiceItem(name + "_item_auto")
        })
    })

    it("TC_128: Check all required field when creating the new Service item - Name", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.addNewItem("CartonA", "place an order", "carrier", "any", code, "base")
        cy.wait(5000)
        cy.reload()
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        objServiceItem.addNewItem("CartonA", "place an order", "carrier", "any", code, "base")
        objServiceItem.common.txtError().should("have.text", "Invalid parameter.Existing service name [CartonA].")
        objServiceItem.btnCloseAddNewPopup().click()
        cy.wait(5000)
        objServiceItem.searchItem("CartonA")
        cy.wait(5000)
        objServiceItem.deleteItem()
    })

    it("TC_129: Check all required field when creating the new Service item - Code", () => {
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        cy.wait(2000)
        const code = "Item " + Math.random().toString(30).substring(2,4) + Math.floor(Math.random() * 1000)
        objServiceItem.addNewItem("CartonA", "place an order", "carrier", "any", code, "base")
        cy.wait(5000)
        cy.reload()
        objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data","Service Item")
        objServiceItem.addNewItem("CartonB", "place an order", "carrier", "any", code, "base")
        objServiceItem.common.txtError().should("have.text", "Invalid parameter.Existing service item code ["+ code +"].")
        objServiceItem.btnCloseAddNewPopup().click()
        cy.wait(5000)
        objServiceItem.searchItem("CartonA")
        cy.wait(5000)
        objServiceItem.deleteItem()
    })
})