import ShippingOrder from "../shippingorder/ShippingOrder"
import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class ShippingOrderTracking {

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    lstStatus() {
        return cy.xpath('//span[@class="common-boder-def undefined"]')
    }

    lstStatusGray() {
        return cy.xpath('//span[@class="common-boder-def common-boder-gray"]')
    }

    lstStatusGreen() {
        return cy.xpath('//span[@class="common-boder-def common-boder-Dark-green"]')
    }

    iconX() {
        return cy.xpath('//i[@class="el-tag__close el-icon-close"]')
    }

    lblFileManagement() {
        return cy.xpath('//div[@id="tab-fileManager"]')
    }

    lblExecutionInformation() {
        return cy.xpath('//div[@id="tab-execute"]')
    }

    lblBillingInformation() {
        return cy.xpath('//div[@id="tab-statement"]')
    }

    lblRecordInformation() {
        return cy.xpath('//div[@id="tab-3"]')
    }

    lblOperationRecord() {
        return cy.xpath('//div[@id="tab-operationLog"]')
    }

    txtEditMode() {
        return cy.xpath('//table[@class="xheLayout"]//tr[2]//iframe')
    }

    btnSaveEdit() {
        return cy.xpath('(//span[contains(text(),"save")])[1]')
    }

    btnConfirmEdit() {
        return cy.xpath('//button[@class="el-button el-button--default el-button--small el-button--primary "]')
    }

    btnAddAttachments() {
        return cy.xpath('(//input[@type="file"])[1]')
    }

    btnDownloadPackageAttachment() {
        return cy.xpath('(//span[contains(text(),"Download package")])[1]')
    }

    msgAddFileSuccess() {
        return cy.xpath('//div[@class="el-message el-message--success is-closable"]')
    }

    lblMessageRecord() {
        return cy.xpath('//div[@class="chat-message-content"]')
    }

    lblFileAttactmentName() {
        return cy.xpath('//div[@class="file-name"]')
    }

    searchResultWithOutValueInDDLTrack(value) {
        // return cy.xpath("//li[@class='el-select-dropdown__item multiple']//span[text()='" + value + "']").first()
        return cy.xpath("//span[@style='color:red' and text()='" + value + "']")
    }

    chooseValueWhenSearchInDDLTrack(trackingNum){
        return cy.xpath("//div[@class='select-style']//a//span[@style='color:red' and text() = '"+ trackingNum +"']")
    }

    lblServiceItemInformation() {
        return cy.xpath("//span[.='Service item information']")
    }

    btnCloseSetColumn() {
        return cy.xpath("//div[@aria-label='dialog']//button[@aria-label='Close']")
    }

    btnCloseServiceInfor() {
        return cy.xpath("//div[contains(@aria-label,'Service item information')]//i[contains(@class,'el-dialog__close el-icon el-icon-close')]")
    }

    columnStatus() {
        return cy.xpath("//td[3]//div")
    }

    checkStatus(status) {
        var count = 0
        this.lstStatus().each(($el) => {
            const statusS = $el.text().trim()
            cy.log(statusS)
            expect($el).to.have.text(statusS)
            if (statusS === status) {
                count += 1
            }
            else {
                cy.log("Fail")
            }
        }).then(() => {
            this.header.totalValue().should('contain', 'Total ' + count)
        })
    }

    checkStatusGray(status) {
        var count = 0
        this.lstStatusGray().each(($el) => {
            const statusS = $el.text().trim()
            cy.log(statusS)
            expect($el).to.have.text(statusS)
            if (statusS === status) {
                count += 1
            }
            else {
                cy.log("Fail")
            }
        }).then(() => {
            this.header.totalValue().should('contain', 'Total ' + count)
        })
    }

    checkStatusGreen(status) {
        var count = 0
        this.lstStatusGreen().each(($el) => {
            const statusS = $el.text().trim()
            cy.log(statusS)
            expect($el).to.have.text(statusS)
            if (statusS === status) {
                count += 1
            }
            else {
                cy.log("Fail")
            }
        }).then(() => {
            this.header.totalValue().should('contain', 'Total ' + count)
        })
    }

    clearStatus() {
        this.iconX().click()
    }

    checkStatusWhenChooseOrderThenClickButtonClose(status) {
        this.common.columnShippingOrderNumber().first().then(($text) => {
            cy.log($text.text().trim())
            this.common.searchResultWithOutValue().first().click({ force: true })
            cy.wait(2000)
            this.header.btnAtHeader("close").click()
            this.common.btnConfirmHintPopUp("confirm").click()
            this.common.msgSuccess().should('be.visible').then(($text) => {
                cy.log($text.text().trim())
            })
            this.clearStatus()
            this.common.searchBySelectStatus("status", status)
            this.common.searchByInputOrderNumber("shipping order", $text.text().trim())
            this.common.columnShippingOrderNumber().first().then(($text2) => {
                cy.log($text2.text().trim())
                if ($text.text().trim() === $text2.text().trim()) {
                    cy.log("Pass")
                }
                else {
                    cy.log($text2.text().trim())
                    cy.log("Fail")
                }
            })
        })
    }

    checkMessageErrorWhenClickButtonClose(action, text) {
        this.header.btnAtHeader("close").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp(action).click()
        this.common.msgError().should('be.visible')
        this.common.txtError().then(($text) => {
            if ($text.text().trim() === text) {
                this.common.txtError().should('contain', text)
                cy.log($text.text().trim())
            }
            else {
                cy.log("Fail")
                cy.log($text.text().trim())
            }
        })
    }

    checkChangeStatusEnabledToDisabledButtonSetColumnWhenAdjust(column) {
        this.header.divColumnToAdjust(column).should('have.attr', 'class', 'dragg-item')

    }

    checkChangeStatusDisabledToEnabledButtonSetColumnWhenAdjust(column) {
        this.header.divColumnToAdjust(column).should('have.attr', 'class', 'dragg-item itemBgGray')
    }

    shouldBeNextPageWhenClickButtonNextPage() {
        this.header.btnNext().click()
        this.header.ipPageNumber().then(function ($text) {
            if ($text.text().trim() !== 1) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
    }

    shouldBeDisabledWhenClickButtonPreviousPageFirst() {
        this.header.btnPrev().should('be.disabled')
    }

    shouldBeDisabledWhenClickButtonNextPageFirst() {
        this.header.totalValue().then(($text) => {
            this.common.selectPageSize("500/page")
            let value = Number.parseInt($text.text().trim().substr(6, 4))
            for (let i = 1; i < value / 500; i++) {
                this.header.btnNext().click()
            }
            this.header.btnNext().first().should('have.attr', 'disabled', 'disabled')
        })
    }

    shouldBePreviousPageWhenClickButtonPreviousPage() {
        this.header.btnNext().click()
        this.header.btnPrev().click()
        this.header.ipPageNumber().then(function ($text) {
            if ($text.text().trim() === 1) {
                cy.log("Pass")
            }
            else {
                cy.log("Fail")
            }
        })
    }

    shouldBePageWhenInputPageNumber(pageNumber) {
        this.header.ipPageNumber().clear()
        this.header.ipPageNumber().type(pageNumber, { force: true })
        this.header.ipPageNumber().type('{enter}')
        cy.wait(2000)
        cy.screenshot('shouldBePageWhenInputPageNumber')
    }

    shouldBeBackShippingOrderTrackingScreenWhenClickButtonReturnInDetailScreen() {
        this.common.openDetailView()
        this.header.btnAtHeader("return").click()
        this.header.btnAtHeader("close").should('be.visible')
        cy.screenshot('shipingorderScreen')
    }

    copyANewOrder() {
        this.common.openDetailView()
        cy.wait(2000)
        this.header.btnAtHeader("copy a new order").click()
        cy.wait(3000)
        return new ShippingOrder()
    }

    clickRecordInformation() {
        this.lblRecordInformation().click()
    }

    clickFileManagement() {
        this.lblFileManagement().click()
    }

    addFileAttachments(filePath) {
        this.btnAddAttachments().attachFile(filePath)
    }

    clickButtonDownloadPackageAttachment() {
        this.btnDownloadPackageAttachment().click()
        this.btnDownloadPackageAttachment().should('be.visible')
    }

    shouldBeShowMessageSuccessWhenAddFileAttachments(name) {
        this.common.msgSuccess().should('be.visible')
        this.lblFileAttactmentName().should('contain', name)
    }

    inputSomeText(text) {
        cy.frameLoaded('#xhe0_iframe')
        cy.iframe().focus().type(text, { force: true })
    }

    clickButtonSaveEditRecord() {
        this.btnSaveEdit().click()
    }

    shouldBeShowMessageSucess(text) {
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should('contain', 'done successfully')
        this.lblMessageRecord().each(($el) => {
            if ($el.text().trim() === text) {
                expect($el).to.contain(text)
            }
            else {
                cy.log($el.text().trim())
            }
        })
    }

    searchTrackingNumber(trackingNum){
        this.header.ipSearchBox("shipping order").should('be.visible').focus().realType(trackingNum) 
        this.header.ipSearchBox("shipping order").type('{enter}')        
        this.chooseValueWhenSearchInDDLTrack(trackingNum).click({force: true})
        this.header.btnAtHeader("inquire").click({multiple: true, force: true })
    }

    shippingAndDispatchOrderTrackingStatus(status) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//span[text()= "' + status + '"]')
    }

    // verifyFileDownloadPackage(){
    //     this.common.columnShippingOrderNumber().first().then(($text)=>{
    //         this.common.openDetailView()
    //         this.clickFileManagement()
    //         cy.wait(4000)
    //         var d = new Date();
    //         if(d.getMonth() < 10){
    //             var datestring = d.getFullYear() + "0" + d.getMonth() + "" + d.getDate() + "" + d.getHours() + "" + d.getMinutes() + "" + d.getSeconds()

    //         }
    //         else{
    //             var datestring = d.getFullYear() + "" + d.getMonth() + "" + d.getDate() + "" + d.getHours() + "" + d.getMinutes() + "" + d.getSeconds()
    //         }
    //         var pathFile = "shippingorder.track.attachment-'" + $text.text().trim() + datestring + "'"
    //         this.clickButtonDownloadPackageAttachment()
    //         cy.verifyDownload(pathFile)
    //     })
    // }

    checkServiceInformation() {
        this.lblServiceItemInformation().should("be.visible")
    }

    clickExecutionInformation() {
        this.lblExecutionInformation().click({ force: true })
    }

    clickBillingInformation() {
        this.lblBillingInformation().click({ force: true })
    }

    clickOperationRecord() {
        this.lblOperationRecord().click({ force: true })
    }

    verifyStatusOrder(shippingOrderNumber, status){
        cy.wait(2000)
        this.searchTrackingNumber(shippingOrderNumber.text().trim()) 
        cy.wait(2000)                                                     
        this.shippingAndDispatchOrderTrackingStatus(status).first().should('be.visible')
    }
}

export default ShippingOrderTracking;