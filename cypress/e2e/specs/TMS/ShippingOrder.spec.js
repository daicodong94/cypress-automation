import Login from "../../../support/pages/TMS/components/login/Login";
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan";
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule";
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder";
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder";
import { Utility } from "../../../support/utility";
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage";

describe("Shipping Order", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(), objShippingOrder = new ShippingOrder()
    var objCreateShippingOrder = new CreateShippingOrder(), objCreateShippingOrder2, objCreateShippingOrder3
    var objDeliveryPlan = new DeliveryPlan(), objSchedule = new Schedule()
    var trackNumber, orderToDirectSchedule, orderToDelete, customer = "Automation Company", product = "Service for Shipper", temperature = "Chill"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    //Set column
    it("TC: Check function Set Column - Disable header \n" +
        "TC: Check function Set Column - Enable header \n" +
        "TC: Check function Set Column - Drag&drop to reoder header \n" +
        "TC: Check function Set Column - Reset", () => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objShippingOrder.common.resetDefaultColumn("yes")
            objShippingOrder.common.setColumn("information", "Status")
            objShippingOrder.common.enableOrDisableColumn("information", "no")
            objShippingOrder.common.resetDefaultColumn("yes")
        })

    it("TC: Check flow to access Shipping Order screen \n" +
        "TC: Check function Fold \n " +
        "TC: Check pagination \n " +
        "TC: Check function Return", () => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objShippingOrder.hideAndUnHideSearchShippingOrder()
            objShippingOrder.common.goToPageNumber(3)
            objShippingOrder.common.selectPageSize("50/page")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            cy.wait(2000)
            objShippingOrder = objCreateShippingOrder.returnToShippingOrderPage()
        })

    // Create shippingOrder
    it("TC: Check create Shipping Order - Save", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum) => {
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder404("Save", trackingNum, customer, product, temperature, "TestT1", "TestT2", 5);
            objShippingOrder.common.msgSuccess().should("be.visible")
            trackNumber = trackingNum
        })
    })
    it("TC: Check create Shipping Order - Confirm", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum2nd) => {
            objCreateShippingOrder2 = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder2.createNewShippingOrder404("Confirm", trackingNum2nd, customer, product, temperature, "AutoTestSite99", "SiteTestAuto", 5);
            objShippingOrder.common.msgSuccess().should("be.visible")
        })
    })
    it("TC: Check create Shipping Order - Direct dispatch", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum3rd) => {
            objCreateShippingOrder3 = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder3.createNewShippingOrder404("Direct Schedule", trackingNum3rd, customer, product, temperature, "TestT2", "TestT4", 5);
        })
    })

    // Search
    it("TC: Check function Inquire \n" +
        "TC: Check Revise Shipping Order function \n" +
        "TC: Check function Confirm", () => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objShippingOrder.common.clearFilterForDropdown("order status")
            objShippingOrder.searchByInputCondition("please enter the transport order \\ outer order", trackNumber, "yes")
            // objShippingOrder.searchByInputDataToFilter("please enter the shipper", "mikel")
            // objShippingOrder.searchBySelectCondition("departure city", "北京市")
            //TC: Check Revise Shipping Order function
            objShippingOrder.reviseShippingOrder("revise")
            //TC: Check function Confirm
            cy.wait(2000)
            objShippingOrder.confirmShippingOrder()
            // cy.wait(35000)
            cy.reload()
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
            objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
        })

    //Wihdraw - SKIP
    // it("Check function Withdraw", () => {
    //     cy.reload()
    //     objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("plan", "delivery plan")
    //     objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
    //     objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
    //     cy.reload()
    //     objShippingOrder = objDeliveryPlan.leftMenu.navigateToPage("Shipping order")
    //     objShippingOrder.header.clearFilter("order status")
    //     objShippingOrder.searchByInputCondition("please enter the transport order \\ outer order", "DYXPBAYRSZZTENPYDOSIF", "yes")
    //     cy.wait(2000)
    //     objShippingOrder.withdrawShippingOrder("confirm")
    //     objShippingOrder.header.searchResult("EFMKYKBTWPZUHVTPIXLJA").should("be.visible")
    // })

    it("Create new order for direct schedule", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum2) => {
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder404("Save", trackingNum2, customer, product, temperature, "TestT3", "TestT4", 5)
            orderToDirectSchedule = trackingNum2
        })
    })
    it("TC: Check function Direct schedule", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        objShippingOrder.common.clearFilterForDropdown("order status")
        objShippingOrder.searchByInputCondition("External Tracking Number", orderToDirectSchedule, "yes")
        objShippingOrder.directScheduleShippingOrder()
        cy.wait(1000)
        objShippingOrder.common.msgSuccess().should("be.visible")
        // cy.wait(40000)
        cy.reload()
        objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("Dispatch Number", orderToDirectSchedule)
        objSchedule.common.searchResultWithOutValue().should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        objSchedule.header.searchResult(orderToDirectSchedule).should("be.visible")
    })

    //Copy a new order
    it("TC: Check function Copy a new order - Save", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(7).then((trackingNum4) => {
            objShippingOrder.common.clearFilterForDropdown("order status")
            objShippingOrder.searchByInputCondition("External Tracking Number", trackNumber, "yes")
            cy.wait(5000)
            objShippingOrder.copyANewOrder("Save", trackingNum4, 15)
            objShippingOrder.common.msgSuccess().should("be.visible")
            orderToDelete = trackingNum4
        })
    })

    it("TC: Check function Copy a new order - Confirm", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum5) => {
            objShippingOrder.common.clearFilterForDropdown("order status")
            objShippingOrder.searchByInputCondition("External Tracking Number", trackNumber, "yes")
            cy.wait(1000)
            objShippingOrder.copyANewOrder("Confirm", trackingNum5, 15)
            objShippingOrder.common.msgSuccess().should("be.visible")
        })
    })

    it("TC: Check function Copy a new order - Direct schedule", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        cy.randomString(11).then((trackingNum6) => {
            objShippingOrder.common.clearFilterForDropdown("order status")
            objShippingOrder.searchByInputCondition("External Tracking Number", trackNumber, "yes")
            cy.wait(2000)
            objShippingOrder.copyANewOrder("Direct Schedule", trackingNum6, 15)
        })
    })

    //Delete
    it("TC: Check function Delete", () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        objShippingOrder.common.clearFilterForDropdown("order status")
        objShippingOrder.searchByInputCondition("External Tracking Number", orderToDelete, "yes")
        objShippingOrder.deleteShippingOrder("confirm")
        objShippingOrder.header.searchResult(orderToDelete).should("not.exist")
    })

    // it.only("", () => {
    //     objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
    //     cy.randomString(21).then((track) => {
    //         objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
    //         objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithCargoDetailForQuantitySplit("save", track, customer, product, temperature, "TestT3", "TestT4", 20, 3, 18, 4)
    //     })
    // })

    // after(() => {
    //     objLogout = new Logout()
    //     objLogout.logout()
    // })
})