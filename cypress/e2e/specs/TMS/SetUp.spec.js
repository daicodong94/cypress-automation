import Abnormal from "../../../support/pages/TMS/components/abnormal/Abnormal"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import Login from "../../../support/pages/TMS/components/login/Login"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import AccountInformation from "../../../support/pages/TMS/components/setup/AccountInformation"
import ExceptionType from "../../../support/pages/TMS/components/setup/ExceptionType"
import Permission from "../../../support/pages/TMS/components/setup/Permission"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"

describe("Set Up", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(),
        objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objAccountInfor = new AccountInformation(), objPermission = new Permission(), objAbnormal = new Abnormal(),
        objExceptionType = new ExceptionType(), objPickup = new Pickup(), objSchedule = new Schedule(),
        objActivityTracking = new ActivityTracking()
    var beforeCount, afterCount, roleName = "autoTestAddRole", exTypeName = "autoTestExceptionType", trackNumber,
        customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    //Account information
    it("TC: Check case can update account information when update logo", () => {
        objAccountInfor = objHome.leftMenu.navigateToSubPage("Set Up", "Account Information")
        var filePath = "implement\\images.png"
        objAccountInfor.updateAvatar(filePath)
    })

    it("TC: Check update information fail if only click on [Save] button", () => {
        objAccountInfor = objHome.leftMenu.navigateToSubPage("Set Up", "Account Information")
        beforeCount = objAccountInfor.countRows()
        objAccountInfor.inputContact("mikel", "6643566735", "nguyen.thihuyen@ascendcorp.com", "AT test")
        objAccountInfor.header.btnAtHeader("save").click()
        objAccountInfor.common.btnConfirmHintPopUp("confirm").click()
        objAccountInfor.common.msgSuccess().should("be.visible")
        cy.reload()
        objAccountInfor.leftMenu.navigateToSubPage("Set Up", "Account Information")
        afterCount = objAccountInfor.countRows()
        if (afterCount - beforeCount == 0) {
            return true
        } else {
            return false
        }
    })

    it("TC: Check Add contact success", () => {
        objAccountInfor = objHome.leftMenu.navigateToSubPage("Set Up", "Account Information")
        beforeCount = objAccountInfor.countRows()
        objAccountInfor.header.btnAtHeader("+add contacts").click()
        objAccountInfor.inputContact("mikel", "6643566735", "nguyen.thihuyen@ascendcorp.com", "AT test")
        objAccountInfor.icSaveContact().click()
        objAccountInfor.common.msgSuccess().should("be.visible")
        afterCount = objAccountInfor.countRows()
        if (afterCount - beforeCount == 1) {
            return true
        } else {
            return false
        }
    })

    it("TC: Check delete contact", () => {
        objAccountInfor = objHome.leftMenu.navigateToSubPage("Set Up", "Account Information")
        beforeCount = objAccountInfor.countRows()
        objAccountInfor.header.btnAtHeader("+add contacts").click()
        objAccountInfor.inputContact("mikel", "6643566735", "nguyen.thihuyen@ascendcorp.com", "AT test")
        objAccountInfor.icSaveContact().click()
        cy.wait(1000)
        objAccountInfor.deleteContact()
        afterCount = objAccountInfor.countRows()
        if (afterCount - beforeCount == 0) {
            return true
        } else {
            return false
        }
    })

    //Permission
    it("TC: Check Add role success \n" +
        "TC: Verify function if add a new role which already exists in system \n" +
        "TC: Check edit permission for role \n" +
        "TC: Check delete character successfull \n" +
        "TC: Check function [add role] after click on icon close on the pop up", () => {
            //TC: Check Add role success
            objPermission = objHome.leftMenu.navigateToSubPage("Set Up", "Permission")
            // objPermission.deleteRole(roleName)
            cy.wait(1000)
            objPermission.addRole(roleName)
            objPermission.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objPermission.role(roleName).should("be.exist")
            ///TC: Verify function if add a new role which already exists in system
            cy.wait(1000)
            objPermission.addRole(roleName)
            objPermission.common.msgError().should("have.text", "Name of an existing role.")
            //TC: Check function [add role] after click on icon close on the pop up
            objPermission.common.closePopUp("add role")
            //
            ////TC: Check edit permission for role
            cy.reload()
            objPermission.role(roleName).click()
            cy.wait(1000)
            objPermission.checkChkBox("Shipping order", "yes")
            cy.wait(1000)
            objPermission.checkChkBox("Shipping order", "no")
            ////
            ///
            //TC: Check delete character successfull
            cy.wait(1000)
            objPermission.deleteRole(roleName)
            cy.wait(1000)
            objPermission.role(roleName).should("not.exist")
        })

    //Exception type
    it("TC: Check paging of the Exception type \n" +
        "TC: Create exception type when user not fill Default level/Processing time limit field \n" +
        "TC: Create exception type only fill require fields \n" +
        "TC: Create exception type fill all of fields \n" +
        "TC: Check delete exception type \n" +
        "TC: Check search the exception type by status \n" +
        "TC: Check search the exception type by Type name/Status \n" +
        "TC: Don't choose condition to search, check displaying all data", () => {
            objExceptionType = objHome.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            //TC: Check paging of the Exception type
            objExceptionType.common.selectPageSize("20/page")
            objExceptionType.common.selectPageSize("500/page")
            //
            //TC: Create exception type when user not fill Default level/Processing time limit field
            cy.wait(1000)
            objExceptionType.createExceptionType(exTypeName, "empty", "empty", "work", "test auto")
            objExceptionType.common.msgNotificationRequired().should("exist")
            //
            //TC: Create exception type only fill require fields
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.createExceptionType(exTypeName, "slight", 2, "empty", "empty")
            objExceptionType.common.msgSuccess().should("be.visible")
            objExceptionType.deleteExceptionType("type name", exTypeName, "yes")
            //
            //TC: Create exception type fill all of fields
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.createExceptionType(exTypeName, "slight", 2, "work", "test auto")
            objExceptionType.common.msgSuccess().should("be.visible")
            //TC: Check search the exception type by status
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.common.selectPageSize("500/page")
            objExceptionType.common.searchBySelectValue("status", "enabled")
            objExceptionType.checkStatus("enabled")
            cy.wait(1000)
            objExceptionType.common.searchBySelectValue("status", "created")
            objExceptionType.checkStatus("created")
            //Tc: Check search the exception type by Type name/Status
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.common.searchByInputOrderNumber("type name", exTypeName)
            objExceptionType.common.searchBySelectValue("status", "created")
            objExceptionType.common.searchResult(exTypeName)
            //
            //TC: Check delete exception type
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.deleteExceptionType("type name", exTypeName, "yes")
            //TC: Don't choose condition to search, check displaying all data
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.common.selectPageSize("500/page")
            objExceptionType.header.btnAtHeader("inquire").click()
            objExceptionType.checkStatus("empty")
            objExceptionType.common.lblElement("No Data").should("not.exist")
        })

    it("TC: Check can to shown detail exception type information \n" +
        "TC: Check edit Type information \n" +
        "TC: Check delete Exception type", () => {
            objExceptionType = objHome.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.createExceptionType(exTypeName, "slight", 2, "empty", "empty")
            objExceptionType.common.msgSuccess().should("be.visible")
            //TC: Check can to shown detail exception type information
            objExceptionType.common.searchByInputOrderNumber("type name", exTypeName)
            objExceptionType.common.openDetailView()
            objExceptionType.detailExceptionTypeUI()
            //TC: Check edit Type information
            objExceptionType.editExceptionType("edit auto", "serious", "bill")
            //TC: Check delete Exception type
            cy.reload()
            objExceptionType.leftMenu.navigateToSubPage("Set Up", "Exception Type")
            objExceptionType.deleteExceptionType("type name", exTypeName, "yes")
        })


    it("TC: Check 'Delivery waiting' feature \n" +
        "TC: Check 'Logistics and distribution problems' feature \n" +
        "TC: Check 'Pickup waiting' feature", () => {
            cy.randomString(8).then((id) => {
                objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("direct schedule", id, "Automation Company", product, temperature, siteB, siteD, 0, 5);
                trackNumber = id
                objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
                objSchedule.common.searchByClickSuggestion("dispatch number", id)
                objSchedule.common.searchResultWithOutValue().should("be.visible")
                cy.wait(1000)
                objSchedule.common.openDetailView()
                cy.wait(1000)
                objSchedule.selectDataAndSchedule("SYS_DEF_INDIVIDUAL", "Auto Driver", "confirm and dispatch")
                objSchedule.common.msgSuccess().should("be.visible")
                objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
                cy.wait(42000)
                cy.reload()
                objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("dispatch number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

                //open detail view and get Waybill number: this.waybill
                objPickup.common.openDetailView()
                objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill2 = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill2)
                    cy.reload()
                    ////must go to activity tracking -> update status 2 times
                    objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    cy.wait(1000)
                    objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    //back to Pick up to pickup completed
                    objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    cy.wait(1000)
                    objPickup.common.searchByClickSuggestion("waybill number", id)
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                    objPickup.common.openDetailView()
                    cy.wait(1000)
                    objPickup.chooseCompletionTime(0)
                    cy.wait(1000)

                    objPickup.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    objPickup.confirmPickup("yes")
                    objAbnormal = objPickup.leftMenu.navigateToPage("Abnormal")
                    cy.wait(1000)
                    //TC: Check "Delivery waiting" feature
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "delivery waiting", "slight", "now", "confirm")
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.wait(1000)
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("delivery waiting").should("be.visible")
                    //TC: Check "Logistics and distribution problems" feature 
                    //bug
                    objAbnormal.common.clearFilter("order number")
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "Logistics and distribution problem", "slight", "now", "confirm")
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.wait(1000)
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("Logistics and distribution problem").should("be.visible")
                    //TC: Check "Pickup waiting" feature 
                    objAbnormal.common.clearFilter("order number")
                    objAbnormal.header.btnAtHeader("new").click()
                    objAbnormal.createAbnormal(id, "Pickup waiting", "slight", "now", "confirm")
                    objAbnormal.common.msgSuccess().should("be.visible")
                    cy.wait(1000)
                    objAbnormal.common.searchByClickSuggestion("order number", id)
                    objAbnormal.pickup.dispatchPickupStatus("Pickup waiting").should("be.visible")
                })
            })
        })


    it("TC: Check 'Other' feature \n" +
        "TC: Check 'Product date problem' feature \n" +
        "TC: Check 'The type and quantity of goods are wrong' feature \n" +
        "TC: Check 'Sign outside fence' feature", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            //TC: Check "Other" feature 
            // objAbnormal.common.clearFilter("order number")
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(trackNumber, "other", "slight", "now", "confirm")
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("other").should("be.visible")
            //TC: Check "Product date problem" feature 
            objAbnormal.common.clearFilter("order number")
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(trackNumber, "production date problem", "slight", "now", "confirm")
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("production date problem").should("be.visible")
            //TC: Check "The type and quantity of goods are wrong" feature 
            //bug
            objAbnormal.common.clearFilter("order number")
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(trackNumber, "The type and quantity of goods are wrong", "slight", "now", "confirm")
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("The type and quantity of goods are wrong").should("be.visible")
            //TC: Check "Sign outside fence" feature 
            objAbnormal.common.clearFilter("order number")
            objAbnormal.header.btnAtHeader("new").click()
            objAbnormal.createAbnormal(trackNumber, "Sign outside the fence", "slight", "now", "confirm")
            objAbnormal.common.msgSuccess().should("be.visible")
            cy.wait(1000)
            objAbnormal.common.searchByClickSuggestion("order number", trackNumber)
            objAbnormal.pickup.dispatchPickupStatus("Sign outside the fence").should("be.visible")
        })
})