import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class AccountInformation {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //btnAtHeader("save/+add contacts/upload avatar")

    txtDescription() {
        return cy.xpath("//textarea[@class='el-textarea__inner']")
    }

    ipMailingAddress() {
        return cy.xpath("//td[text()='mailing address']//following-sibling::td//input")
    }

    ipAbbreviation() {
        return cy.xpath("//td[text()='abbreviation']//following-sibling::td//input")
    }

    ipContact() {
        return cy.xpath("//input[@data-vv-name='contactName0']")
    }

    ipCellPhone() {
        return cy.xpath("//input[@data-vv-name='contactMobile0']")
    }

    ipMail() {
        return cy.xpath("//input[@data-vv-name='counactEmail0']")
    }

    ipJobTitle() {
        return cy.xpath("//input[@data-vv-name='jobDescription0']")
    }

    icSaveContact() {
        return cy.xpath("(//button[@title='save'])[1]")
    }

    icRevise() {
        return cy.xpath("//button[@title='revise']")
    }

    icDelete() {
        return cy.xpath("//button[@title='delete']")
    }

    updateAvatar(filePath) {
        this.common.btnAddAttachments().attachFile(filePath)
        this.header.btnAtHeader("save").click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should("be.visible")
    }

    inputContact(contact, phone, mail, job) {
        this.ipContact().type(contact)
        this.ipCellPhone().type(phone)
        this.ipMail().type(mail)
        this.ipJobTitle().type(job)
    }

    deleteContact() {
        this.icDelete().first().click()
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should("be.visible")
    }

    countRows() {
        let count = 0
        this.common.searchResultWithOutValue().then($elements => {
            count = $elements.length
            return count
        })
    }
}

export default AccountInformation;