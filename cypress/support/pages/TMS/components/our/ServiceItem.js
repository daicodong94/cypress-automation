import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import Common from "../common/Common"

class ServiceItem {

    //CONSTRUCTOR
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //XPATH

    txtNameItem() {
        return cy.xpath("//input[@data-vv-name='serviceName']")
    }

    cboSelectTime() {
        return cy.xpath("//div[@data-vv-name='choseNode']")
    }

    lblTime(time) {
        return cy.xpath("//li//span[normalize-space()='" + time + "']")
    }

    cboServer() {
        return cy.xpath("//div[@data-vv-name='provider']")
    }

    lblServer(server) {
        return cy.xpath("//li//span[normalize-space()='" + server + "']")
    }

    cboBusinessSync() {
        return cy.xpath("//div[@data-vv-name='synmode']")
    }

    lblBusSynd(sync) {
        return cy.xpath("//li//span[normalize-space()='" + sync + "']")
    }

    txtCode() {
        return cy.xpath("//input[@data-vv-name='code']")
    }

    cboServiceType() {
        return cy.xpath("//div[@data-vv-name='serviceType']")
    }

    lblType(type) {
        return cy.xpath("//li//span[normalize-space()='" + type + "']")
    }

    btnSaveAddNewItem() {
        return cy.xpath("//div[contains(@aria-label,'new service item')]//button//span[contains(text(),'Save')]")
    }

    tblSerial() {
        return cy.xpath("//td[2]//div")
    }

    tblServiceItem() {
        return cy.xpath("//td[3]//div")
    }

    tblStatus() {
        return cy.xpath("//td[4]//div")
    }

    tblSelectTime() {
        return cy.xpath("//td[5]//div")
    }

    tblAttributes() {
        return cy.xpath("//td[6]//div")
    }

    tblServiceType() {
        return cy.xpath("//td[7]//div")
    }

    lblServiceItem(item) {
        return cy.xpath("//div[contains(text(),'" + item + "')]")
    }

    btnSequence() {
        return cy.xpath("(//button[contains(@title,'sequence')])")
    }

    lblHintSerial() {
        return cy.xpath("//div[@class='el-message-box__message']//p")
    }

    btnSaveEditInfor() {
        return cy.xpath("//div[@class='common-right-toolbar']//button[@type='button']")
    }

    txtChangeSerial() {
        return cy.xpath("//div[@class='el-input el-input--mini']//input[@type='text']")
    }

    txtNewNameItem() {
        return cy.xpath("//input[contains(@placeholder,'please enter the service name (maximum 32 characters)')]")
    }

    cboNewSelectTime() {
        return cy.xpath("//div[@class='common-right-content']//input[@placeholder='select time']")
    }

    cboNewServer() {
        return cy.xpath("//div[@class='common-right-content']//input[@placeholder='server']")
    }

    cboNewBusinessSync() {
        return cy.xpath("//div[@class='common-right-content']//input[@placeholder='business synchronization']")
    }

    txtNewCode() {
        return cy.xpath("//div[@class='common-right-content']//input[@placeholder='code']")
    }

    cboNewServiceType() {
        return cy.xpath("//div[@class='common-right-content']//input[@placeholder='service type']")
    }

    btnCloseAddNewPopup() {
        return cy.xpath("//div[@aria-label='new service item']//i[@class='el-dialog__close el-icon el-icon-close']")
    }

    btnCloseChangeSerialPopup() {
        return cy.xpath("//i[@class='el-message-box__close el-icon-close']")
    }

    //SCRIPTS

    addNewItem(name, server, sync, code, type) {
        this.header.btnAtHeader("new").click({ force: true })

        this.txtNameItem().type(name)

        // this.lblTime(time).click({ multiple: true, force: true })
        this.cboServer().click({ force: true })
        this.common.selectRandomValue()
        this.cboSelectTime().click({ force: true })
        this.lblServer(server).click({ multiple: true, force: true })
        cy.wait(1000)
        this.cboBusinessSync().click({ forve: true })
        this.lblBusSynd(sync).click({ multiple: true, force: true })
        this.txtCode().type(code)
        this.cboServiceType().click({ force: true })
        this.lblType(type).click({ multiple: true, force: true })
        this.btnSaveAddNewItem().click({ force: true })
    }


    searchItem(item) {
        this.header.ipSearchBox("service item").click({ force: true })
        this.header.ipSearchBox("service item").clear()
        this.header.ipSearchBox("service item").type(item, { force: true })
        this.header.btnAtHeader("inquire").click({ force: true })
        this.lblServiceItem(item).should('be.visible')
    }

    verifyInquire(name, time, server, sync, code, type) {
        this.addNewItem(name, time, server, sync, code, type)
        cy.wait(3000/2)
        this.searchItem(name)
        cy.wait(3000/2)
        this.tblServiceItem().should('contain', name)
        cy.wait(1000)
        this.tblStatus().should('contain', "created")
        cy.wait(1000)
        this.tblSelectTime().should('contain', time)
        cy.wait(1000)
        this.tblAttributes().should('contain', "sys")
        cy.wait(1000)
        this.tblServiceType().should('contain', type)
    }

    verifyAddNewItem(name, time, type) {
        this.common.msgSuccess().should('be.visible')
        cy.wait(3000/2)
        this.searchItem(name)
        cy.wait(3000/2)
        this.tblServiceItem().should('contain', name)
        cy.wait(1000)
        this.tblStatus().should('contain', "created")
        cy.wait(1000)
        this.tblSelectTime().should('contain', time)
        cy.wait(1000)
        this.tblAttributes().should('contain', "sys")
        cy.wait(1000)
        this.tblServiceType().should('contain', type)
    }

    deleteItem() {
        this.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("delete").click({ forve: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click({ forve: true })
    }

    verifyDeleteItem(name) {
        this.common.msgSuccess().should('be.visible')
        cy.wait(3000/2)
        this.searchItem(name)
        cy.wait(3000/2)
        this.lblServiceItem(name).should('not.to.exist')
    }

    enableServiceItem() {
        this.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("enable").click({ forve: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click({ forve: true })
        this.common.msgSuccess().should('be.visible')
        // this.tblStatus().first().should('contain', 'Active')
    }

    disabledServiceItem() {
        this.common.searchResultWithOutValue().first().click({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("disabled").click({ forve: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click({ forve: true })
        this.common.msgSuccess().should('be.visible')
        cy.wait(1000)
        this.tblStatus().first().should('contain', 'disabled')
    }

    verifySerial(name, time, server, sync, code, type, serial) {
        this.addNewItem(name, time, server, sync, code, type)
        cy.wait(3000/2)
        this.searchItem(name)
        cy.wait(3000/2)
        this.tblSerial().then(($text1) => {
            this.btnSequence().click({ forve: true })
            cy.wait(3000/2)
            const serialItem = $text1.text().substring(0, 2)
            this.lblHintSerial().should('contain', 'please enter serial number!(the current serial number is[' + serialItem + '])')
            cy.wait(1000)
            this.txtChangeSerial().type(serial)
            cy.wait(1000)
            this.common.btnConfirmHintPopUp("confirm").click({ forve: true })
            this.common.msgSuccess().should('be.visible')
            cy.wait(3000/2)
            this.header.btnAtHeader("inquire").click({ force: true })
            cy.wait(3000/2)
            this.tblSerial().then(($text2) => {
                const newSerialItem = $text2.text().substring(0, 2)
                if (newSerialItem == serial) {
                    cy.log("Pass")
                    this.btnSequence().click({ forve: true })
                    cy.wait(3000/2)
                    this.lblHintSerial().should('contain', 'please enter serial number!(the current serial number is[' + newSerialItem + '])')
                    this.btnCloseChangeSerialPopup().click({ force: true })
                }
                else {
                    cy.log("Fail")
                    cy.log($text2.text().trim())
                }
            })
        })
    }

    editServiceItem(name, time, server, sync, code, type, newName, newTime, newServer, newSync, newCode, newType) {
        this.addNewItem(name, time, server, sync, code, type)
        cy.wait(3000/2)
        this.searchItem(name)
        cy.wait(3000/2)
        this.common.openDetailView()
        this.txtNewNameItem().clear()
        this.txtNewNameItem().type(newName)
        cy.wait(1000)
        this.cboNewSelectTime().click({ forve: true })
        this.lblTime(newTime).last().click({ multiple: true, force: true })
        cy.wait(1000)
        this.cboNewServer().click({ forve: true })
        this.lblServer(newServer).last().click({ multiple: true, force: true })
        cy.wait(1000)
        this.cboNewBusinessSync().click({ forve: true })
        this.lblBusSynd(newSync).last().click({ multiple: true, force: true })
        cy.wait(1000)
        this.txtNewCode().clear()
        this.txtNewCode().type(newCode)
        cy.wait(1000)
        this.cboNewServiceType().click({ forve: true })
        this.lblType(newType).last().click({ multiple: true, force: true })
        cy.wait(1000)
        this.btnSaveEditInfor().click({ forve: true })
        this.common.btnConfirmHintPopUp("confirm").click({ forve: true })
    }
}
export default ServiceItem