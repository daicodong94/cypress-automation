import LeftMenu from "../../../TMS/components/fragments/LeftMenu";
import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import AddVehicle from "./VehicleAdd";
import Vehicle from "./VehicleList";

class VehicleListdd {
    constructor() {
        this.vehicle = new Vehicle()
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
        this.addVehicle = new AddVehicle()
    }

    afterClickFilter() {
        return cy.xpath("//button[@class='el-button base-icon-button el-button--primary_outline el-button--mini plain']")
    }

    verifyFilterUI() {
        this.btnAction("Filter").click()
        this.afterClickFilter().should("be.visible")
    }

    filterField(field) {
        return cy.xpath("//div[text()='" + field + "']//following-sibling::div//span[@class='el-input__suffix']")
    }

    filterData(field, value) {
        this.btnAction("Filter").click()
        this.filterField(field).click({ force: true })
        this.common.spanValue(value).click({ force: true })
    }

    resetFilter() {
        this.btnAction("Reset Filter").click()
    }

    createHistoryLog(id) {
        this.addVehicle.searchByCondition(id)
        this.common.searchResultWithOutValue().dblclick({ force: true })
        for (let index = 0; index <= 300; index++) {
            this.btnAction("Edit").click({ force: true })
            this.common.ipSelectBox("Select Vehicle Model Year").click()
            this.addVehicle.selectRandomValue()
            this.addVehicle.confirmCreateVehicle("Save", "Confirm", "yes")
            this.common.msgSuccess().should("be.visible")
        }
    }

    //go to page
    goToPageNumber(page, verify) {
        this.common.ipPageNumber().clear()
        this.common.ipPageNumber().type(page)
        this.common.ipPageNumber().type('{enter}')
        if (verify == "yes") {
            this.common.pageActive().should("have.text", page)
        }
    }

    verifyImportUI() {
        this.btnAction("Import").click({ force: true })
    }

    msgAlertError() {
        return cy.xpath("//div[@role='alert' and @class='el-message el-message--error']//p")
    }

    msgAlertErrorShouldHaveText(msg) {
        this.msgAlertError().should("have.text", msg)
    }

}

export default VehicleListdd;