import SiteList from "../../../MasterData/components/site/SiteList"
import HomePage from "../../components/homepage/Homepage"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"

class LoginSupplier {

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
    }

    titleLoginpage() {
        return cy.xpath('//div[contains(text(),"Log in to your account")]')
    }

    titleForgetPasswordpage() {
        return cy.xpath('//div[contains(text(),"Forgot password ?")]')
    }

    titleSelectOTPscreen() {
        return cy.xpath('//div[contains(text(),"How do you want to reset your password?")]')
    }

    titleEnterOTPScreen() {
        return cy.xpath('//div[contains(text(),"Enter OTP")]')
    }

    txtUsername() {
        return cy.xpath('//input[@name="email"]')
    }

    txtPassword() {
        return cy.xpath('//input[@name="password"]')
    }

    ckbRememberAccount() {
        return cy.xpath('//input[@type="checkbox"]')
    }

    lnkForgetPassword() {
        return cy.xpath('//a[contains(text(),"Forget your password?")]')
    }

    btnLogin() {
        return cy.xpath('//div[@class="form-footer"]//button[@type="button"]')
    }

    spanUsername() {
        return cy.xpath("//span[@class='fl username']")
    }

    btnfunclogout(text) {
        return cy.xpath("//button//span[text()='" + text + "']")
    }

    btnContinue() {
        return cy.xpath('//button[@type="button"]')
    }

    lnkBacktologin() {
        return cy.xpath('//a[contains(text(),"Back to log in")]')
    }

    lnkBacktopreviouspage() {
        return cy.xpath('//a[contains(text(),"Back to previous page")]')
    }

    lnkResendOTP() {
        return cy.xpath('//div[contains(text(),"Resend OTP")]')
    }

    lnkChangeRecoveryMethod() {
        return cy.xpath('//a[contains(text(),"Change recovery method")]')
    }

    msgErrorForgetPassword(text) {
        return cy.xpath('//span[contains(text(),"' + text + '")]')
    }

    msgLoginUnSuccessfully(text) {
        return cy.xpath('//span[text()="' + text + '"]')
    }

    radiobtnGetOtpbyPhone() {
        return cy.xpath('(//input[@type="radio"])[1]')
    }

    radiobtnGetOtpbyEmail() {
        return cy.xpath('(//input[@type="radio"])[2]')
    }

    txtOTPCode() {
        return cy.xpath('//input[@placeholder="Enter OTP code"]')
    }

    msgInvalidOTPCode() {
        return cy.xpath('//span[contains(text(),"Invalid OTP Code")]')
    }

    btnVerify() {
        return cy.xpath('//button[@type="button"]//div[text() ="VERIFY"]')
    }

    msgGetOTPAgain() {
        return cy.xpath('//div[contains(text(),"Get OTP again in")]')
    }

    ///////////////////////////////////////////////////////////////////////////

    logout() {
        this.spanUsername().click()
        this.btnfunclogout("quit").click()
        cy.wait(3000)
        this.titleLoginpage().should('exist')
    }

    inputUsername(username) {
        this.txtUsername().type(username, { force: true })
    }

    inputPassword(password) {
        this.txtPassword().type(password, { force: true })
    }

    clickbtnLogin() {
        this.btnLogin().click({ force: true })
    }

    clickckbRememberAccount() {
        this.ckbRememberAccount().click()
    }

    clicklnkForgetPassword() {
        this.lnkForgetPassword().click()
    }

    clicklnkForgetPassword() {
        this.lnkForgetPassword().click()
    }

    clicklnkBacktologin() {
        this.lnkBacktologin().click()
    }

    clickbtnContinute() {
        this.btnContinue().click()
    }

    clicklnkBacktoPreviouspage() {
        this.lnkBacktopreviouspage().click()
    }

    clickGetOTPbyPhone() {
        this.radiobtnGetOtpbyPhone().click()
    }

    clickGetOTPbyEmail() {
        this.radiobtnGetOtpbyEmail().click()
    }

    clickbtnVerify() {
        this.btnVerify().click()
    }

    shouldbetilteLoginSuccessfully() {
        cy.title().should('eq', 'ALL NOW TMS Master Data')
    }

    clickResendOTP() {
        this.lnkResendOTP().click()
    }

    clickChangeRecoveryMethod() {
        this.lnkChangeRecoveryMethod().click()
    }

    shouldMessageLoginUnSuccessfully(message) {
        this.msgLoginUnSuccessfully(message).should('exist')
    }

    shouldbeTitleForgetPassowrd() {
        this.titleForgetPasswordpage().should('be.visible')
    }

    shouldbetitleSelectOTPscreen() {
        this.titleSelectOTPscreen().should('be.visible')
    }

    shouldbemsgErrorForgetPassword(msg) {
        this.msgErrorForgetPassword(msg).should('be.visible')
    }

    shouldbeBtnLoginDisable() {
        this.btnLogin().should('be.disabled')
    }

    /////////////////////////////////////////////////////////////////////////

    loginWithUserandPassword(username, password) {
        this.titleLoginpage().should('exist')
        this.inputUsername(username)
        this.inputPassword(password)
        cy.wait(1000)
        this.clickbtnLogin({ force: true })
        cy.wait(3000)
        return new HomePage()
    }

    navigateForgetPasswordScreen() {
        this.titleLoginpage().should('exist')
        this.clicklnkForgetPassword()
    }

    navigateSelectOTPScreen(username) {
        this.navigateForgetPasswordScreen()
        cy.wait(2000)
        this.inputUsername(username)
        this.clickbtnContinute()
    }

    chooseOTPbyEmail(username) {
        this.navigateSelectOTPScreen(username)
        cy.wait(2000)
        this.clickGetOTPbyEmail()
        cy.wait(50000)
        this.clickbtnContinute()
        cy.wait(2000)
        this.titleEnterOTPScreen().should('be.visible')
    }

    chooseOTPbyPhone(username) {
        this.navigateSelectOTPScreen(username)
        cy.wait(2000)
        this.clickGetOTPbyPhone()
        cy.wait(50000)
        this.clickbtnContinute()
        cy.wait(2000)
        this.titleEnterOTPScreen().should('be.visible')
    }

    inputOTPCode(code) {
        this.txtOTPCode().type(code)
    }

    shouldbemsgInvalidOTPCodeWhenGetOTPbyEmail(username, otpcode) {
        this.chooseOTPbyEmail(username)
        this.inputOTPCode(otpcode)
        cy.wait(2000)
        this.msgInvalidOTPCode().should('be.visible')
    }

    shouldbemsgInvalidOTPCodeWhenGetOTPbyPhone(username, otpcode) {
        this.chooseOTPbyPhone(username)
        this.inputOTPCode(otpcode)
        cy.wait(2000)
        this.msgInvalidOTPCode().should('be.visible')
    }
    callApiSendOTPSuccess() {
        const URL = "http://tmsui.allnow.tms.staging.internal/forgotPassword/sendOtp"
        const body = {
            "target": "iFmNs22R7u09T3NHyyzBD1AjNYO+Y6tPT8wZ0TOjXNBfA/J/fHqjgw==",
            "noticeType": "1"
        }
        cy.request('POST', URL, body).then((response) => {
            expect(response.status).eq(200)
            const resbody = response.body
            expect(resbody.code).eq("SEND_OTP_RESET_PASSWORD_SUCCESS")
            expect(resbody.message).eq("send OTP reset by email success")
        })
    }
    mockingOtpAndverificationOtp(username) {
        this.navigateSelectOTPScreen(username)
        this.clickGetOTPbyEmail()
        cy.wait(2000)
        cy.randomNumber(6).then((Otp) => {
            const OTPCode = Otp
            console.log(OTPCode)
            cy.intercept('POST', '/forgotPassword/sendOtp', { code: OTPCode, message: 'send OTP reset by email success', items: true }).as('sendOtp')
            this.clickbtnContinute()
            cy.wait('@sendOtp').then((res) => {
                cy.log(JSON.stringify(res))
                console.log(JSON.stringify(res))
            })
            this.inputOTPCode(OTPCode)
            cy.intercept('POST', '/forgotPassword/verificationOtp', { code: OTPCode, message: 'verification otp success', items: true }).as('verificationOtp')
            this.clickbtnVerify()
            cy.wait('@verificationOtp').then((response) => {
                cy.log(JSON.stringify(response))
                console.log(JSON.stringify(response))
            })
        })
    }

    navigateToMasterDataSite() {
        cy.visit("http://tmsui.allnow.tms.staging.internal/master-data/site/list")
        return new SiteList
    }

}
export default LoginSupplier;