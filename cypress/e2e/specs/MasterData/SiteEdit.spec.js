import SiteEdit from "../../../support/pages/MasterData/components/site/SiteEdit"
import { Utility } from "../../../support/utility"





describe("MasterData - SiteEdit", () => {
    var data
    var objSiteEdit = new SiteEdit


    beforeEach(function () {
        var url = new Utility().getBaseUrl()
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
        data = logindata
        cy.visit(url)
        objSiteEdit.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Site")
        })
    })


    it("TC_186: Check default value on Customer Name dropdown list\n" + "TC_187: Check updated value on Customer Name dropdown list\n", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("AQAVMO", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.shouldDataCustomercorrect("Enter Customer name", "AQAVMO")
            objSiteEdit.siteAdd.editCustomer("AutomationTester")
            cy.reload()
            objSiteEdit.leftMenu.navigateToSubPage("Site")
            cy.wait(2000)
            objSiteEdit.common.searchByCondition(number)
            cy.wait(2000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.createSite.ipInformation("Enter Customer name").type("AutomationTester")
            objSiteEdit.createSite.selectCustomerName("AutomationTester").should("be.visible")
            cy.reload()
            cy.wait(2000)
            objSiteEdit.siteAdd.editCustomer("AQAVMO")
        })
    })

    it("TC_188: Check when updated input including text, numerical values and special character in Site Code text box\n" + 
    "TC_189: Check when updated input text data into Site Code text box\n" + "TC_190: Check when updated input numerical data into Site Code text box\n" + "TC_191: Check when updated input special character data into Site Code text box\n" + 
    "TC_193: Check when updated input data less than maxlength into Site Code text box\n" + "TC_194: Check when updated input data equal to maxlength into Site Code text box\n" + "TC_195: Check when updated input data more than maxlength into Site Code text box\n" + "TC_196: Check when get blank into Site Code text box" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.createSite.ipInformation("Enter Code").clear({force: true })
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Code", "12345abcde", "20")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Code", "abcdeabcde", "20")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Code", "1234567890", "20")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Code", "@@@@@@@@@@", "20")
            objSiteEdit.siteAdd.validationInputData("20", "Enter Code", "1234567890abcdeabcde", "20")
            objSiteEdit.siteAdd.validationInputData("20", "Enter Code", "1234567890abcdedabcdeadadada", "20")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
        
    })

    it("TC_192: Check when updated input same existing data into Site Code text box", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            cy.randomNumber(10).then((Sitecode) => {
                objSiteEdit.createSite.createNewSite("Automation", Sitecode, Sitecode, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
                objSiteEdit.common.searchByCondition(number)
                cy.wait(1000)
                objSiteEdit.common.searchResultWithOutValue().should("be.visible")
                objSiteEdit.common.openDetailsView()
                cy.wait(3000)
                objSiteEdit.createSite.btnEditSite().click()
                cy.wait(2000)
                objSiteEdit.createSite.ipInformation("Enter Code").clear({force: true })
                objSiteEdit.createSite.ipInformation("Enter Code").type(Sitecode)
                objSiteEdit.createSite.btnSave().click()
                objSiteEdit.siteAdd.msgInforAlreadyExist("Site Code is already existed").scrollIntoView().should("be.visible")
                objSiteEdit.siteAdd.cancelCreateSite()
            })
        })
    })

    it("TC_197: Check when updated input including text, numerical values and special character in Site Name text box\n" + 
    "TC_198: Check when updated input text data into Site Name text box\n" + "TC_199: Check when updated input numerical data into Site Name text box\n" + "TC_200: Check when updated input special character data into Site Name text box\n" + 
    "TC_202: Check when updated input data less than maxlength into Site Name text box\n" + "TC_203: Check when updated input data equal to maxlength into Site Name text box\n" + "TC_204: Check when updated input data more than maxlength into Site Name text box\n" + "TC_205: Check when get blank into Site Name text box" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.createSite.ipTextareaInfor("Enter Site name").clear({force: true })
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter Site name", "12345abcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter Site name", "abcdeabcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter Site name", "1234567890", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter Site name", "@@@@@@@@@@", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("150", "Enter Site name", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("160", "Enter Site name", "150")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
        
    })

    it("TC_201: Check when updated input same existing data into Site Name text box", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            cy.randomNumber(10).then((SiteName) => {
                objSiteEdit.createSite.createNewSite("Automation", SiteName, SiteName, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
                objSiteEdit.common.searchByCondition(number)
                cy.wait(1000)
                objSiteEdit.common.searchResultWithOutValue().should("be.visible")
                objSiteEdit.common.openDetailsView()
                cy.wait(3000)
                objSiteEdit.createSite.btnEditSite().click()
                cy.wait(2000)
                objSiteEdit.createSite.ipTextareaInfor("Enter Site name").clear({force: true })
                objSiteEdit.createSite.ipTextareaInfor("Enter Site name").type(SiteName)
                objSiteEdit.createSite.btnSave().click()
                objSiteEdit.siteAdd.msgInforAlreadyExist("Site name is already existed").scrollIntoView().should("be.visible")
                objSiteEdit.siteAdd.cancelCreateSite()
            })
        })
    })

    it("TC_206: Check when updated input data (text, numerical values and special character) into Site Short name text box\n" + 
    "TC_207: Check when updated input all text into Site Short name text box\n" + "TC_208: Check when updated input all numerical values into Site Short name text box\n" + "TC_209: Check when updated input all special characters into Site Short name text box\n" + 
    "TC_211: Check when updated input less than maxlength into Site Short name text box\n" + "TC_212: Check when updated input equal to maxlength into Site Short name text box\n" + "TC_213: Check when updated input more than maxlength into Site Short name text box\n" + "TC_214: Check when get blank Site Short name text box" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.validationInputData("10", "Enter Site short name", "12345abcde", "50")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Site short name", "abcdeabcde", "50")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Site short name", "1234567890", "50")
            objSiteEdit.siteAdd.validationInputData("10", "Enter Site short name", "@@@@@@@@@@", "50")
            objSiteEdit.siteAdd.ipSiteShortNameMoreThanMaxlength("50", "Enter Site short name", "50")
            objSiteEdit.siteAdd.ipSiteShortNameMoreThanMaxlength("60", "Enter Site short name", "50")
            cy.randomNumber(10).then((ShortName) => {
                objSiteEdit.createSite.ipInformation("Enter Site short name").clear({force: true })
                objSiteEdit.siteAdd.validationInputData("10", "Enter Site short name", ShortName, "50")
                objSiteEdit.createSite.btnSave().click()
                cy.randomNumber(5).then((number)=> {
                    objSiteEdit.leftMenu.navigateToSubPage("Site")
                    objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
                    objSiteEdit.common.searchByCondition(number)
                    cy.wait(1000)
                    objSiteEdit.common.searchResultWithOutValue().should("be.visible")
                    objSiteEdit.common.openDetailsView()
                    cy.wait(3000)
                    objSiteEdit.createSite.btnEditSite().click()
                    cy.wait(2000)
                    objSiteEdit.createSite.btnSave().should("be.enabled")
                    objSiteEdit.createSite.ipInformation("Enter Site short name").clear({force: true })
                    objSiteEdit.createSite.ipInformation("Enter Site short name").type(ShortName)
                    objSiteEdit.createSite.btnSave().click()
                    objSiteEdit.siteAdd.msgInforAlreadyExist("Site short name is already existed.").scrollIntoView().should("be.visible")
                    objSiteEdit.siteAdd.cancelCreateSite()
                })
            })
        })
    })

    it("TC_215: Check updated value on Site Type dropdown list\n" + 
    "TC_218: Check when get blank into Site Sub Type/DC text box" , () => {
        cy.fixture("site.json").then((siteListData) => {
            data = siteListData
            objSiteEdit.siteAdd.clickBtnCreate()
            objSiteEdit.siteAdd.checkDataServiceGroup("Enter Site type", data.siteType)
            objSiteEdit.createSite.ipInformation("Select Site type group").invoke('attr', 'disabled').then((status) => {
                expect(status).eq("disabled")
            })
            objSiteEdit.createSite.selectValue("Store").click()
            objSiteEdit.siteAdd.checkDataServiceGroup("Select Site type group", data.siteSubTypestore)
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_216: Check updated value on Site Sub Type dropdown list", () => {
        cy.fixture("site.json").then((siteListData) => {
            data = siteListData
            objSiteEdit.siteAdd.clickBtnCreate()
            objSiteEdit.createSite.selectInformation("Enter Site type", "DC")
            objSiteEdit.siteAdd.checkDataServiceGroup("Select Site type group", data.siteSubDC)
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_217: Check when get blank into Site Sub Type/Store type text box\n" + 
    "TC_220: Check when input data (text, numerical values and special character) into Site Sub-Type: Additional detail text box\n" + 
    "TC_221: Check when input all text into Site Sub-Type: Additional detail text box\n" + 
    "TC_222: Check when input all numerical values into Site  Sub-Type: Additional detail text box\n" + 
    "TC_223: Check when input all special characters into Site Sub-Type: Additional detail text box\n" + 
    "TC_224: Check when input less than maxlength into Site Sub-Type: Additional detail text box\n" + 
    "TC_225: Check when input equal to maxlength into Site Sub-Type: Additional detail text box\n" + 
    "TC_226: Check when input more than maxlength into Site Sub-Type: Additional detail text box\n" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            // objSiteAdd.createSite.selectInformation("Enter Site type", "Store")
            objSiteEdit.common.selectDataByClick("Select Site type group", "Other")
            objSiteEdit.createSite.ipTextareaInfor("Enter additional detail").should("be.visible")
            objSiteEdit.createSite.ipTextareaInfor("Enter additional detail").type(number)
            objSiteEdit.createSite.btnSave().should("be.enabled")
            objSiteEdit.createSite.btnSave().click()
            cy.wait(2000)
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.createSite.ipTextareaInfor("Enter additional detail").clear()
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter additional detail", "12345abcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter additional detail", "abcdeabcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter additional detail", "1234567890", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Enter additional detail", "@@@@@@@@@@", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("150", "Enter additional detail", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("160", "Enter additional detail", "150")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_226: Check add more days into Open hour\n" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.ckbDayChecked("Tuesday").click()
            objSiteEdit.createSite.startAndEndTime("Start time").should("be.visible")    
            objSiteEdit.createSite.startAndEndTime("End time").should("be.visible")
        })
    })

    it("TC_228: Check display Holiday, Special day-off\n" + 
    "TC_235: Check display of Edit screen on Holiday, Special Day -Off \n" + 
    "TC_236: Check display addition day off on Special day-off\n" + 
    "TC_238: Check when select date < current date on Holiday Special day-off\n" +
    "TC_241: Check cancel selecting date for 'Date' field on edit screen of Holiday Special Day-Off", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-holiday", "1","Special day-off","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.windowTimeTab("tab-holiday").click()
            objSiteEdit.columnDateAndRemark("Date").should("be.visible")
            objSiteEdit.columnDateAndRemark("Remark").should("be.visible")
            objSiteEdit.iconEditSpecialDay().should("be.visible")
            objSiteEdit.iconDeleteSpecialDay().should("be.visible")
            objSiteEdit.editWindowTime("tab-holiday")
            cy.wait(2000)
            objSiteEdit.siteAdd.titleSpecialScreen("Special Day-Off").should("be.visible")
            objSiteEdit.siteAdd.selectSpecialDate().should("be.visible")
            objSiteEdit.siteAdd.txtAreaSpecialRemark("Special day-off").should("be.visible")
            objSiteEdit.statusBtnConfirm("Edit", "1").should("be.disabled")
            objSiteEdit.statusBtnConfirm("Cancel", "1").should("be.enabled")
            objSiteEdit.siteAdd.selectSpecialDate().click()
            var date = new Date().getDate()
            cy.log("date =" + date)
            objSiteEdit.common.divAvailableDate().contains(date)
            objSiteEdit.common.btnConfirmHintPopup("Set", "1").click()
            var hour = new Date().getHours()
            objSiteEdit.selectSpecialTime("Start time").click()
            var hourPast = hour - 1
            cy.wait(1000)
            objSiteEdit.siteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
            objSiteEdit.statusBtnConfirm("Cancel", "1").click()
            objSiteEdit.common.btnConfirmHintPopup("Confirm", "1").click()
            objSiteEdit.titleEditSiteScreen().should("be.visible")
        })
    })

    it("TC_253: Check click confirm to cancel to edit new Holiday, Special Day-Off\n" + 
    "TC_254: Check click cancel to cancel to edit new Holiday, Special Day-Off\n" + 
    "TC_260: Check function of Edit -Holiday, Special day off screen\n" + 
    "TC_261: Check function of Delete -Holiday, Special day off screen", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-holiday", "1","Special day-off","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.editWindowTime("tab-holiday")
            cy.wait(2000)
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("confirm")
            objSiteEdit.editWindowTime("tab-holiday")
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("cancel", "Special Day-Off")
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").clear()
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").type("abcdef")
            objSiteEdit.common.btnConfirmHintPopup("Edit", "1").click()
            objSiteEdit.createSite.btnSave().click()
            objSiteEdit.msgEditSiteSuccess().should("be.visible")
            // Delete special day off
            cy.wait(2000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.iconDeleteSpecialDay().click()
            objSiteEdit.siteAdd.divNoData("1").should("be.visible")
        })
    })

    it("TC_255: Check when updated input data equal to maxlength into Remark text box\n" + 
    "TC_256: Check when updated input data less than maxlength into Remark text box\n" + 
    "TC_257: Check when updated input data more than maxlength into Remark text box\n" + 
    "TC_258: Check when update to blank into Remark text box\n" +
    "TC_259: Check when update existing data into Remark text box\n", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-holiday", "1","Special day-off","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.editWindowTime("tab-holiday")
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").clear()
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "12345abcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "abcdeabcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "1234567890", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "@@@@@@@@@@", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("150", "Special day-off", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("160", "Special day-off", "150")
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("confirm")
        })
    })

    it("TC_263: Check display Holiday, Special day-on\n" + 
    "TC_270: Check display of Edit screen on Holiday, Special Day -On \n" + 
    "TC_271: Check display addition day off on Special day-on\n" + 
    "TC_274: Check when select date < current date on Holiday Special day-on\n" +
    "TC_277: Check cancel selecting date for 'Date' field on edit screen of Holiday Special Day-On", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-special", "2","Special day-on","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.windowTimeTab("tab-special").click()
            objSiteEdit.columnDateAndRemark("Date").should("be.visible")
            objSiteEdit.columnDateAndRemark("Remark").should("be.visible")
            objSiteEdit.iconEditSpecialDay().should("be.visible")
            objSiteEdit.iconDeleteSpecialDay().should("be.visible")
            objSiteEdit.editWindowTime("tab-special")
            cy.wait(2000)
            objSiteEdit.siteAdd.titleSpecialScreen("Special Day-On").should("be.visible")
            objSiteEdit.siteAdd.selectSpecialDate().should("be.visible")
            objSiteEdit.siteAdd.txtAreaSpecialRemark("Special day-off").should("be.visible")
            objSiteEdit.statusBtnConfirm("Edit", "1").should("be.disabled")
            objSiteEdit.statusBtnConfirm("Cancel", "1").should("be.enabled")
            objSiteEdit.siteAdd.selectSpecialDate().click()
            var date = new Date().getDate()
            cy.log("date =" + date)
            objSiteEdit.common.divAvailableDate().contains(date)
            objSiteEdit.common.btnConfirmHintPopup("Set", "1").click()
            var hour = new Date().getHours()
            objSiteEdit.selectSpecialTime("Start time").click()
            var hourPast = hour - 1
            cy.wait(1000)
            objSiteEdit.siteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
            objSiteEdit.statusBtnConfirm("Cancel", "1").click()
            objSiteEdit.common.btnConfirmHintPopup("Confirm", "1").click()
            objSiteEdit.titleEditSiteScreen().should("be.visible")
        })
    })

    it("TC_289: Check click confirm to cancel to edit new Holiday, Special Day-On\n" +
    "TC_295: Check function of Edit -Holiday, Special day off screen\n" + 
    "TC_296: Check function of Delete -Holiday, Special day off screen" , () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-special", "2","Special day-on","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.editWindowTime("tab-special")
            cy.wait(2000)
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("confirm")
            objSiteEdit.editWindowTime("tab-special")
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("confirm")
            objSiteEdit.editWindowTime("tab-special")
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("cancel", "Special Day-On")
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").clear()
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").type("abcdef")
            objSiteEdit.common.btnConfirmHintPopup("Edit", "1").click()
            objSiteEdit.createSite.btnSave().click()
            objSiteEdit.msgEditSiteSuccess().should("be.visible")
            // Delete special day off
            cy.wait(2000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.iconDeleteSpecialDay().click()
            objSiteEdit.siteAdd.divNoData("2").should("be.visible")
        })
    })

    it("TC_290: Check when updated input data equal to maxlength into Remark text box\n" + 
    "TC_291: Check when updated input data less than maxlength into Remark text box\n" + 
    "TC_292: Check when updated input data more than maxlength into Remark text box\n" + 
    "TC_293: Check when update to blank into Remark text box\n" +
    "TC_294: Check when update existing data into Remark text box\n", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSiteInputAllData("Automation", number, number, "Store", "Compact HypermarketU","tab-special", "2","Special day-on","นาย", "Trinh", "Dai", "daicodong","12345678", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteEdit.editWindowTime("tab-special")
            objSiteEdit.createSite.ipTextareaInfor("Special day-off").clear()
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "12345abcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "abcdeabcde", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "1234567890", "150")
            objSiteEdit.siteAdd.validationInputDataSiteName("10", "Special day-off", "@@@@@@@@@@", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("150", "Special day-off", "150")
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("160", "Special day-off", "150")
            objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
            objSiteEdit.confirmCancelEditSpecialDayOff("confirm")
        })
    })

    it("TC_299: Check select data on Title dropdown list\n" + 
    "TC_300: Get blank into First name field\n" + 
    "TC_305: Get blank into Last name field", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            cy.fixture("site.json").then((siteListData) => {
                data = siteListData
                objSiteEdit.siteAdd.checkDataServiceGroup("Select Title", data.title)
                objSiteEdit.common.selectDataByClick("Select Title", "นาย")
                objSiteEdit.createSite.ipInformation("Enter Key Contact Person’s first name").clear()
                objSiteEdit.createSite.ipInformation("Enter Key Contact Person’s last name").clear()
                objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
                objSiteEdit.siteAdd.cancelCreateSite()
            })
        })
    })

    it("TC_301: Check input 30 characters into  First name  field\n" + 
    "TC_302: Check input 31 characters into First name field\n" + 
    "TC_306: Check input 30 characters into Last name field\n" + 
    "TC_307: Check input 31 characters into Last name field", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            objSiteEdit.common.selectDataByClick("Select Title", "นาย")
            objSiteEdit.siteAdd.validationInputData("30", "Enter Key Contact Person’s first name", "abcdeabcdeabcdeabcdeabcdeabcde", "30")
            objSiteEdit.siteAdd.validationInputData("30", "Enter Key Contact Person’s first name", "abcdeabcdeabcdeabcdeabcdeabcdee", "30")
            // Input Validation LastName
            objSiteEdit.siteAdd.validationInputData("30", "Enter Key Contact Person’s last name", "abcdeabcdeabcdeabcdeabcdeabcde", "30")
            objSiteEdit.siteAdd.validationInputData("30", "Enter Key Contact Person’s last name", "abcdeabcdeabcdeabcdeabcdeabcdee", "30")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_303: Check input special characters into  First name field\n" + 
    "TC_304: Check input number into  First name field\n" + 
    "TC_308: Check input special characters into Last name field\n" + 
    "TC_309: Check input number into Last name field", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            objSiteEdit.common.selectDataByClick("Select Title", "นาย")
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s first name", "1234567890", "Invalid The First Name")
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s first name", "@@@@@@@@@@", "Invalid The First Name")
            // Input Validation LastName
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s last name", "1234567890", "Invalid The Last Name")
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s last name", "@@@@@@@@@@", "Invalid The Last Name")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_310: Add correct email into Email\n" +
        "TC_311: Get blank into Key contact email field\n" +
        "TC_312: Add  email without @ for Email field\n" + 
        "TC_313: Add  email without . for Email field\n" + 
        "TC_314: Add  email without domain for Email field", () => {
        cy.randomNumber(5).then((number) => {
            objSiteEdit.createSite.createSiteWithOutEmail("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.createSite.msgCreateSiteSuccess().should("be.visible")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abcgmail.com", "Invalid Email")
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abc@gmailcom", "Invalid Email")
            objSiteEdit.siteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abc@gmail.", "Invalid Email")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_316: Check select one value from dropdown list country code number  for Mobile phone no. field\n" + 
    "TC_317: Check input maxlength characters into Mobile  phone  no.\n" + 
    "TC_318: Check input less than maxlength characters into Mobile  phone  no.", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()        
            objSiteEdit.siteAdd.dropDownMobilePhone().scrollIntoView().click()
            objSiteEdit.createSite.selectValue("66").first().click({multiple : true, force : true})
            objSiteEdit.createSite.ipInformation("Enter Mobile Phone no.").clear()
            objSiteEdit.siteAdd.validationInputData("9", "Enter Mobile Phone no.", "999999999", "9")
            objSiteEdit.siteAdd.validationInputData("9", "Enter Mobile Phone no.", "9999999999", "9")
            objSiteEdit.siteAdd.validationInputData("7", "Enter Mobile Phone no.", "9999999", "9")
            objSiteEdit.msgInvalidPhone("Invalid The Mobile Phone No.").should("be.visible")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_319: Check input over maxlength into Mobile  phone  no.\n" + 
    "TC_320: Check input space into Mobile phone no.\n" + 
    "TC_321: Check input text and special characters into Mobile phone no.\n" + 
    "TC_323: Get blank into Mobile phone no. field", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()        
            objSiteEdit.siteAdd.dropDownMobilePhone().scrollIntoView().click()
            objSiteEdit.createSite.selectValue("66").first().click({multiple : true, force : true})
            objSiteEdit.createSite.ipInformation("Enter Mobile Phone no.").clear()
            objSiteEdit.siteAdd.validationInputData("0", "Enter Mobile Phone no.", "    ", "9" )
            objSiteEdit.siteAdd.validationInputData("0", "Enter Mobile Phone no.", "acbdceee", "9" )
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_324: Check select one value from dropdown list country code number  for Landline phone no. field\n" + 
    "TC_325: Check input maxlength characters into Landline phone no.\n" + 
    "TC_326: Check input over maxlength into Landline phone no.\n" + 
    "TC_327: Check input text and special characters into Landline phone no.\n" + 
    "TC_329: Get blank into Landline phone no. field", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()  
            objSiteEdit.siteAdd.dropDownMobilePhone().scrollIntoView().click()
            objSiteEdit.createSite.selectValue("66").first().click({multiple : true, force : true})
            objSiteEdit.createSite.ipInformation("Enter Landline phone no.").clear()
            objSiteEdit.siteAdd.validationInputData("8", "Enter Landline phone no.", "99999999", "8" )
            objSiteEdit.siteAdd.validationInputData("8", "Enter Landline phone no.", "9999999999", "8" )
            objSiteEdit.siteAdd.validationInputData("0", "Enter Landline phone no.", "acbdce@@@", "8" )
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_330: Check maxlength into Extension\n" +
    "TC_331: Check input over maxlength into Extension\n" + 
    "TC_332: Check input characters different from numbers into Extension", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            // objSiteEdit.clearInformation("Extension").clear()
            objSiteEdit.siteAdd.validationInputData("8", "Extension", "99999999", "8")
            objSiteEdit.siteAdd.validationInputData("8", "Extension", "999999999", "8")
            objSiteEdit.clearInformation("Extension").clear()
            objSiteEdit.siteAdd.validationInputData("0", "Extension", "acbdce@@@", "8")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_333: Check Add key contact\n" + 
    "TC_334: Check Add over 3 key contact person\n" + 
    "TC_335: Check Delete key contact person", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()  
            objSiteEdit.siteAdd.btnAddKeyContact().click()
            cy.wait(2000)
            objSiteEdit.siteAdd.newKeyContact().should("be.visible")
            objSiteEdit.siteAdd.btnAddKeyContact().click()
            objSiteEdit.siteAdd.shouldbeDisabledBtnAddKeyContact().should("be.disabled")
            objSiteEdit.siteAdd.iconDeleteContact("3").click()
            objSiteEdit.siteAdd.iconDeleteContact("2").click()
            objSiteEdit.siteAdd.newKeyContact().should("not.exist")
            objSiteEdit.siteAdd.shouldbeDisabledBtnAddKeyContact().should("be.enabled")
            // objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_338: Check when updated input data more than maxlength into Address text box\n" + 
    "TC_340: Check when get blank into Address text box", () => {
        cy.randomNumber(5).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.createSite.msgCreateSiteSuccess().should("be.visible")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            objSiteEdit.createSite.ipTextareaInfor("Enter Address").clear()
            objSiteEdit.siteAdd.ipSiteNameMoreThanMaxlength("160", "Enter Address", "150")
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_342: Check when get blank Province dropdown list\n" +  
    "TC_344: Check when get blank District dropdown list\n" + 
    "TC_346: Check when get blank Sub-District dropdown list\n" + 
    "TC_348: Check when get blank Postal Code dropdown list", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.addLocation("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay")
            objSiteEdit.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.createSite.ipCustomerName("Select Province", "นครราชสีมา")
            objSiteEdit.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.createSite.selectInformation("Select District", "เมืองนครราชสีมา")
            objSiteEdit.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.createSite.selectInformation("Select Sub-District", "ในเมือง")
            objSiteEdit.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteEdit.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteEdit.siteAdd.cancelCreateSite()
        })
    })

    it("TC_356: Check UI of Set Status screen\n" + 
    "TC_357: Check validation of Set Status screen", () => {
        objSiteEdit.siteAdd.navigateToSetStatusScreen()
        objSiteEdit.siteAdd.shouldUISetStatusScreen()
        objSiteEdit.siteAdd.selectEffectDateTime().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSiteEdit.common.divAvailableDate().contains(date)
        cy.selectDay("today")
        var hour = new Date().getHours()
        var hourfuture = hour + 1
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        var hourPast = hour - 1
        cy.wait(1000)
        objSiteEdit.siteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSiteEdit.siteAdd.hourAndMinutesSetStatus(hourfuture, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSiteEdit.siteAdd.hourAndMinutesSetStatus(minutes2, "2").click()
        } else if (minutes > 23) {
            objSiteEdit.siteAdd.hourAndMinutesSetStatus(minutes, "1").click()
        } else {
            objSiteEdit.siteAdd.hourAndMinutesSetStatus(minutes, "2").click()
        }
        objSiteEdit.siteAdd.btnOKStatus().click()
    })


    it("TC_354: Check function of Edit Screen\n" + 
    "TC_355: Check click on cancel button when editing data\n", () => {
        cy.randomNumber(10).then((number) => {
            objSiteEdit.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "เมืองนครราชสีมา", "ในเมือง", "30000")
            objSiteEdit.createSite.msgCreateSiteSuccess().should("be.visible")
            objSiteEdit.common.searchByCondition(number)
            cy.wait(1000)
            objSiteEdit.common.searchResultWithOutValue().should("be.visible")
            objSiteEdit.common.openDetailsView()
            cy.wait(3000)
            objSiteEdit.createSite.btnEditSite().click()
            // Check Edit Site Successfully
            cy.randomNumber(10).then((sitename) => {
                objSiteEdit.createSite.ipTextareaInfor("Enter Site name").clear()
                objSiteEdit.createSite.ipSiteTextareaInfor("Enter Site name", sitename)
                objSiteEdit.createSite.btnSave().click()
                objSiteEdit.msgEditSiteSuccess().should("be.visible")
                // Cancel Edit Site
                objSiteEdit.leftMenu.navigateToSubPage("Site")
                objSiteEdit.common.searchByCondition(sitename)
                cy.wait(1000)
                objSiteEdit.common.searchResultWithOutValue().should("be.visible")
                objSiteEdit.common.openDetailsView()
                cy.wait(3000)
                objSiteEdit.createSite.btnEditSite().click()
                objSiteEdit.createSite.ipTextareaInfor("Enter Site name").clear()
                objSiteEdit.createSite.ipSiteTextareaInfor("Enter Site name", "345345345345343")
                objSiteEdit.common.btnConfirmHintPopup("Cancel", "1").click()
                cy.wait(1000)
                objSiteEdit.common.btnConfirmHintPopup("Cancel", "2").click()
                objSiteEdit.titleEditSiteScreen().should("be.visible")
                // Check click confirm to cancel to Edit Site
                objSiteEdit.siteAdd.cancelCreateSite()
                objSiteEdit.siteList.titlePage("Site").should("be.visible")
            })
        })
    })  

})