import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import Login from "../../../support/pages/TMS/components/login/Login"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import { Utility } from "../../../support/utility"

describe("Flow to complete order in TMS When Confirm Order directly", () => {
    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objCustomerList = objHome.leftMenu.navigateToSubPage("Master Data", "Customer")
                objCustomerAdd = objCustomerList.clickCreateButton()
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })
})