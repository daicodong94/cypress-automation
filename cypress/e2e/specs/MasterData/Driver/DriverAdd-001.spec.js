import  LoginSupplier from "../../../../support/pages/TMS/components/login/LoginSupplier";


describe("Master data - Driver - Add", () => {

    var data, objLogin, objHome, objMasterData, objDriverAdd
    var filePath = "implement/images.png"

    beforeEach("TMS-Our-Itemtype", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            // // const url = new Utility().getBaseUrl()
            cy.visit("https://tms-stg.allnowgroup.com/")
            // if (url == "http://tmsui.allnow.tms.staging.internal/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objDriverAdd = objHome.leftMenu.navigateToSubPage("Master Data", "Driver")
        //     }   else if (url == "https://tms.allnowgroup.com") {
        //         cy.fixture("data_test.json").then((loginData) => {
        //             data = loginData
        //             cy.visit(data.url)
        //             objLogin = new Login()
        //             objHome = objLogin.login(data.username, data.password)
        //         })
        //     }
        })
    })

    it("Check to upload the correct image format =< 3 MB", () => {
        objDriverAdd.btnMainFunction("Create").click()       
        objDriverAdd.btnClickToUpload("Driver photo").click()       
        objDriverAdd.ipClickToUpload("Driver photo").last().attachFile(filePath)      
        objDriverAdd.btnClickToUpload("Driver photo").should('be.disabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check to upload the correct image format but exceed 3 MB", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver photo").click()
        
        var filePath = "implement/Image exceed 3MB.jpg"
        objDriverAdd.ipClickToUpload("Driver photo").last().attachFile(filePath)
        objDriverAdd.msgError().should('have.text', 'File size is exceed the allowable limit of 3 MB')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display files when upload file successfully", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver photo").click()
        

        objDriverAdd.ipClickToUpload("Driver photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("Driver photo").should('have.text', 'images.png')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when click on uploaded image successfully into 'Driver photo ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver photo").click()
        

        objDriverAdd.ipClickToUpload("Driver photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("Driver photo").should('to.exist')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check 'delete' icon when the image is uploaded", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver photo").click()
        

        objDriverAdd.ipClickToUpload("Driver photo").last().attachFile(filePath)
        
        objDriverAdd.btnActionFileUpload("Driver photo").last().click()
        
        objDriverAdd.btnClickToUpload("Driver photo").should('be.enabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display of Title dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboSelectTitle().click()
        
        objDriverAdd.lblTitle("นาย").should('be.visible')
        
        objDriverAdd.lblTitle("นาง").should('be.visible')
        
        objDriverAdd.lblTitle("นางสาว").should('be.visible')
    })

    it("Check display of 'Title' dropdown list", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboSelectTitle().click()
        
        objDriverAdd.lblTitle("นาย").click()
        objDriverAdd.cboSelectTitle().should('have.value', 'นาย')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Get blank into 'Title' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboSelectTitle().dblclick()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when input data is number in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtFirstName().type("68")
        
        objDriverAdd.lblStatusMessageFields("Invalid First Name").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is text in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtFirstName().type("abcdefghiklmn")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is numeric and special char in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtFirstName().type("68@$!@@!")
        
        objDriverAdd.lblStatusMessageFields("Invalid First Name").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 30 characters in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        cy.randomString(30).then((firstName) => {
            objDriverAdd.txtFirstName().type(firstName)
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 31 characters in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        cy.randomString(31).then((firstName) => {
            objDriverAdd.txtFirstName().type(firstName)
            objDriverAdd.verifyWhenInputFirstName(firstName)
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check input data with space before/ after 'First name'", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var firstName = "abcdefghiklmn"
        objDriverAdd.txtFirstName().type(firstName)
        
        objDriverAdd.verifySpaceWhenInputFirstName(firstName)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check leave blank in the 'First name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtFirstName().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when input data is number in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLastName().type("68")
        
        objDriverAdd.lblStatusMessageFields("Invalid Last Name").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is text in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLastName().type("abcdefghiklmn")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is numeric and special char in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLastName().type("68@$!@@!")
        
        objDriverAdd.lblStatusMessageFields("Invalid Last Name").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 30 characters in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        cy.randomString(30).then((lastName) => {
            objDriverAdd.txtLastName().type(lastName)
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 31 characters in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        cy.randomString(31).then((lastName) => {
            objDriverAdd.txtFirstName().type(lastName)
            objDriverAdd.verifyWhenInputLastName(lastName)
            objDriverAdd.btnCancel().first().click({force : true})
            objDriverAdd.btnConfirm().click()
        })
    })

    it("Check leave blank in the 'Last name' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLastName().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when input data in the 'Last name' field the same as the 'First name' field.", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtFirstName().type("abcdefghiklmn")
        
        objDriverAdd.txtLastName().type("abcdefghiklmn")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is already existed in 'Last Name' field & 'First Name' file", () => {
        
        

        const date = new Date()
        const day = date.getDate()
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.createNewDriverV1(filePath, 'นาง', 'huyen', 'vmotest', 'หญิง', day, 'atk', 'def', '123456777', '12345688', '12345678', '1234567890123', '87654321', 'บ.2', 'สาธารณะ')
        
        objDriverAdd.btnConfirm().click()
        cy.wait(10000)
        objDriverAdd.txtFirstName().scrollIntoView()
        objDriverAdd.lblStatusMessageFields("First Name and Last Name is already existed").should('be.visible')
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()

    })

    it("Check display of 'Gender' dropdown list)", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboGender().click()
        
        objDriverAdd.lblGender("ชาย").should('be.visible')
        
        objDriverAdd.lblGender("หญิง").should('be.visible')
        
        objDriverAdd.lblGender("ไม่ระบุ").should('be.visible')
    })

    it("Check select one of 'Gender' dropdown list)", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboGender().click()
        
        objDriverAdd.lblGender("ชาย").click()
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Get blank into 'Gender' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboGender().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when leave blank into 'Date of Birth' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check the activity of the Calendar box for 'Date of Birth' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const date = new Date()
        const day = date.getDate() - 1
        objDriverAdd.lblDayPrevMonth("").last().click({force : true})
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").then(($text)=>{
            cy.checkFormatDay($text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({multiple : true, force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select date in the future for 'Date Of Birth ' field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const year = ""
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
            if(parseInt(year.substr(3,1)) - parseInt($text.text().trim().substr(3,1))  > 1){
                for(var  i = 0; i <= decade; i++){
                    if($text.text().trim() != year){
                        if(decade > 1){
                            objDriverAdd.btnNextYear().as('btn').click()
                            if($text.text().trim() === year){
                                break
                            }
                        }else{
                            if($text.text().trim() === year){
                                break
                            }
                        }
                    }
                }
            }
            else{
                for(var  i = 0; i < decade; i++){
                    if($text.text().trim() != year){
                        if(decade > 1){
                            objDriverAdd.btnNextYear().click()
                            if($text.text().trim() === year){
                                break
                            }
                        }else{
                            if($text.text().trim() === year){
                                break
                            }
                        }
                    }
                }
            }
            objDriverAdd.lblYearDisable(year).should('not.to.selected')
        })
    })

    it("Check select  date in the present for 'Date Of Birth ' field by calendar", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const date = new Date()
        const year = date.getFullYear()
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
            if(parseInt(year.toString().substr(3,1), 10) - parseInt($text.text().trim().substr(3,1), 10) > 1){
                for(var  i = 0; i <= decade; i++){
                    if($text.text().trim() != year){
                        if(decade > 1){
                            objDriverAdd.btnNextYear().click()
                            if($text.text().trim() === year){
                                break
                            }
                        }else{
                            if($text.text().trim() === year){
                                break
                            }
                        }
                    }
                }
            }
            else{
                for(var  i = 0; i < decade; i++){
                    if($text.text().trim() != year){
                        if(decade > 1){
                            objDriverAdd.btnNextYear().click()
                            if($text.text().trim() === year){
                                break
                            }
                        }else{
                            if($text.text().trim() === year){
                                break
                            }
                        }
                    }
                }
            }
            objDriverAdd.lblYearNowDisabledOnDateOfBirthDTP(year).should('not.to.selected')
        })
    })

    it("Check select time in the past when user is less than 18 years old for 'Date Of Birth ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const year = "2004"
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
            if((year.substr(3,1) - $text.text().trim().substr(3,1)) > 1){
                for(var  i = 0; i <= decade; i++){
                    objDriverAdd.btnNextYear().click()
                }
            }
            else if(year < 2009){}
            else{
                for(var  i = 0; i < decade; i++){
                    objDriverAdd.btnNextYear().click()
                }
            }
            objDriverAdd.lblYearDisabled(year).should('not.to.selected')
        })
    })

    it("Check select time in the past when user was 18 years old for 'Date Of Birth ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const date = new Date()
        objDriverAdd.lblToDayDOB(date.getDate()).click()
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").then(($text)=>{
            cy.checkFormatDay($text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()

    })

    it("Check select time in the past when user was between 18 and 100 years old for 'Date Of Birth ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        objDriverAdd.lblDayPrevMonth("").last().click({force : true})
        
        objDriverAdd.btnSetDate().click({multiple : true, force : true})
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").then(($text)=>{
            cy.checkFormatDay($text.text().trim())
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select time in the past when user was 100 years old for 'Date Of Birth ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
        
        const date = new Date()
        const year = date.getFullYear() - 100
        objDriverAdd.lblYearDOB().then(($text)=>{
            objDriverAdd.lblYearDOB().click()
            const decade = (parseInt($text.text().trim(), 10) - parseInt(year, 10))/10
            if((parseInt(year.toString().substr(3,1), 10) - parseInt($text.text().trim().substr(3,1), 10)) > 1){
                for(var  i = 0; i < decade; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            else{
                for(var  i = 0; i < decade - 1; i++){
                    objDriverAdd.btnPrevYear().click()
                }
            }
            objDriverAdd.lblYear(year).click()
            
            objDriverAdd.lblMonth("Dec").click()
            
            objDriverAdd.lblDay(date.getDate()).last().click()
            
            objDriverAdd.btnSetDate().click({multiple : true, force : true})
            
            objDriverAdd.dtpDateOfBirth("Date Of Birth").then(($text)=>{
                cy.checkFormatDay($text.text().trim())
            })
        })
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    // it("Check select time in the past when user is less than 18 years old for 'Date Of Birth ' field", () => {
    //     
    //     
    //     objDriverAdd.btnMainFunction("Create").click()
    //     
    //     objDriverAdd.dtpDateOfBirth("Date Of Birth").click()
    //     
    //     const year = ""
    //     objDriverAdd.lblYearDOB().then(($text)=>{
    //         objDriverAdd.lblYearDOB().click()
    //         const decade = (parseInt(year, 10) - parseInt($text.text().trim(), 10))/10
    //         if((year.substr(3,1) - $text.text().trim().substr(3,1)) > 1){
    //             for(var  i = 0; i <= decade; i++){
    //                 objDriverAdd.btnNextYear().click()
    //             }
    //         }
    //         else if(year < 2009){}
    //         else{
    //             for(var  i = 0; i < decade; i++){
    //                 objDriverAdd.btnNextYear().click()
    //             }
    //         }
    //         objDriverAdd.lblYear(year).should('not.to.selected')
    //     })
    // })

    it("Check input correct email into 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atk@email.com")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add without @ for 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atkemail.com")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without . for 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atk@emailcom")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without domain for 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atk@email")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without domain for 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var email = "atk@email.com"
        objDriverAdd.txtEmail().first().type(email)
        
        objDriverAdd.verifySpaceWhenInputEmail1st(email)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input email is already existed in 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atk@email.com")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when leave blank into 1st Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().click()
        objDriverAdd.txtEmail().last().click()
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check input correct email into 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().type("atk@email.com")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add without @ for 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().type("atkemail.com")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without . for 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().type("atk@emailcom")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without domain for 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().type("atk@email")
        
        objDriverAdd.lblStatusMessageFields("Invalid Email").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check add email without domain for 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var email = "atk@email.com"
        objDriverAdd.txtEmail().last().type(email)
        
        objDriverAdd.verifySpaceWhenInputEmail2nd(email)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input email is already existed in 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().type("atk@email.com")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when leave blank into 2nd Email field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().last().click()
        objDriverAdd.txtEmail().first().click()
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when input email the same into 1st Email field and 2nd Email field ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtEmail().first().type("atk@email.com")
        
        objDriverAdd.txtEmail().last().type("atk@email.com")
        
        objDriverAdd.lblStatusMessageFields("st and 2nd Email can’t be the same email").should('be.visible', {multiple : true})
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check select one value from dropdown list country code number  for  'Moblie  phone  no.' field ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboPhoneNo().click({force : true})
        
        objDriverAdd.lblPhoneNo("").click({multiple : true, force : true})
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 9 number 'Moblie  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtMobilePhoneNo().type("")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 8 number 'Moblie  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtMobilePhoneNo().type("")
        
        objDriverAdd.lblStatusMessageFields("Invalid Mobile Phone No.").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 10 number 'Moblie  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = ""
        objDriverAdd.txtMobilePhoneNo().type(phoneN0)
        
        objDriverAdd.verifySpaceWhenInputPhoneNo(phoneN0)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is text in the Moblie  phone  no textbox ", () => {
          
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "ABCDEF"
        objDriverAdd.txtMobilePhoneNo().type(phoneN0)
        
        objDriverAdd.verifySpaceWhenInputPhoneNo(phoneN0)
        objDriverAdd.txtExtension().click()
        objDriverAdd.txtMobilePhoneNo().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input data is special characters in the Moblie  phone  no. textbox ", () => {
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "@!%!%!@"
        objDriverAdd.txtMobilePhoneNo().type(phoneN0)
        
        objDriverAdd.verifySpaceWhenInputPhoneNo(phoneN0)
        objDriverAdd.txtExtension().click()
        objDriverAdd.txtMobilePhoneNo().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input exist number 'Moblie  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = ""
        objDriverAdd.txtMobilePhoneNo().type(phoneN0)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when leave blank in the 'Moblie  phone  no.'' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.txtLandlinePhoneNo().click()
        
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check select one value from dropdown list country code number  for  'Landline  phone  no.' field ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.cboLandlineNo().click()
        
        objDriverAdd.lblLandlineNo("").click({multiple : true, force : true})
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 8 number 'Landline  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLandlinePhoneNo().type("")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 7 number 'Landline  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLandlinePhoneNo().type("")
        
        objDriverAdd.lblStatusMessageFields("Invalid Landline Phone No").should('be.visible')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 9 number 'Landline  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = ""
        objDriverAdd.txtLandlinePhoneNo().type(phoneN0)
        
        objDriverAdd.verifySpaceWhenInputLandlineNo(phoneN0)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input data is text in the 'Landline  phone  no.' textbox ", () => {
          
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "ABCDEF"
        objDriverAdd.txtLandlinePhoneNo().type(phoneN0)
        objDriverAdd.txtExtension().click()
        objDriverAdd.txtLandlinePhoneNo().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input data is special characters in the 'Landline  phone  no.' textbox ", () => {
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "@!%!%!@"
        objDriverAdd.txtLandlinePhoneNo().type(phoneN0)
        objDriverAdd.txtExtension().click()
        objDriverAdd.txtLandlinePhoneNo().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input exist number 'Landline  phone  no.' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtLandlinePhoneNo().type("")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input exist number 'Landline  phone  no.' textbox ", () => {   
        objDriverAdd.btnMainFunction("Create").click()
        objDriverAdd.txtLandlinePhoneNo().click()
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input 8 number 'extension' textbox ", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtExtension().type("")
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when input 9 number 'extension", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = ""
        objDriverAdd.txtExtension().type(phoneN0)
        
        objDriverAdd.verifySpaceWhenInputLandlineNo(phoneN0)
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when blank number 'extension", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.txtExtension().click()
        objDriverAdd.btnSaveCreateNew().should('be.disabled')
    })

    it("Check when input data is text in the extension textbox ", () => {
          
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "ABCDEF"
        objDriverAdd.txtExtension().type(phoneN0)
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.txtExtension().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when input data is special characters in then extension textbox ", () => {
        
        objDriverAdd.btnMainFunction("Create").click()
        
        var phoneN0 = "@!%!%!@"
        objDriverAdd.txtExtension().type(phoneN0)
        objDriverAdd.txtMobilePhoneNo().click()
        objDriverAdd.txtExtension().should('have.value', '')
        objDriverAdd.btnCancel().first().click({force : true})
    })

    it("Check when upload image correct format  for National ID photo field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        

        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        
        objDriverAdd.btnClickToUpload("National ID photo").should('be.disabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when upload image incorrect format(.ppt, .docx, .xls, .xlsx file) for National ID photo field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        
        const filePath = "customer/Empty.xlsx"
        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        
        objDriverAdd.common.msgError().should('be.visible')
        objDriverAdd.common.msgError().should('have.text', 'The selected file is not a support file type, please check your file.')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when upload image <= 3MB  for National ID photo field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        

        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        
        objDriverAdd.btnClickToUpload("National ID photo").should('be.disabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check display files when upload file successfully", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        

        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("National ID photo").should('have.text', 'images.png')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check when click on uploaded image successfully into 'National ID photo ' field", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("Driver photo").click()
        

        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        objDriverAdd.lblFileUpload("National ID photo").should('to.exist')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check 'delete' icon when the image is uploaded", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        

        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        
        objDriverAdd.btnActionFileUpload("National ID photo").last().click()
        
        objDriverAdd.btnClickToUpload("National ID photo").should('be.enabled')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    it("Check to upload the correct image format but exceed 3 MB", () => {
        
        
        objDriverAdd.btnMainFunction("Create").click()
        
        objDriverAdd.btnClickToUpload("National ID photo").click()
        
        var filePath = "implement/Image exceed 3MB.jpg"
        objDriverAdd.ipClickToUpload("National ID photo").last().attachFile(filePath)
        objDriverAdd.msgError().should('have.text', 'File size is exceed the allowable limit of 3 MB')
        objDriverAdd.btnCancel().first().click({force : true})
        objDriverAdd.btnConfirm().click()
    })

    // it("Check when blank 'National ID photo' field", () => {
    //     
    //     
    //     objDriverAdd.btnMainFunction("Create").click()
    //     
    //     objDriverAdd.createNewDriverV1("", "นาย", "Hung", "Nguyen", "ชาย", "", "atk", "def", "", "", "654321", "123", "", "บ.2", "ส่วนบุคคล")
    //     cy.wait(10000)

    //     objDriverAdd.lblStatusMessageFields("National Id Photo is required").should('be.visible')

    // })
})

