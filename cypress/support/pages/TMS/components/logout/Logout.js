class Logout {

    titleLoginPage(){
        return cy.xpath("//title[text()='login page']")
    }

    imgAvatar(){
        return cy.xpath("//div[@class='header-status pr-5']")
    }

    spanUsername() {
        return cy.xpath("//span[@class='fl username']")
    }

    txtQuit(){
        return cy.xpath("//button[@class='el-button button-profile el-button--text el-button--mini']//span[text()='quit']")
    }

    btnQuit() {
        return cy.xpath("//span[text()='quit']")
    }

    btnRevise() {
        return cy.xpath("//span[text()='revise']")
    }

    logout() {
        this.imgAvatar().trigger('mouseover')
        this.txtQuit().click({force:true})
        cy.wait(1000)
        // this.btnQuit().click()
    }
}
export default Logout;