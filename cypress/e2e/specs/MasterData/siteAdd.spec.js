import SiteAdd from "../../../support/pages/MasterData/components/site/SiteAdd"
import { Utility } from "../../../support/utility"





describe("MasterData - SiteAdd", () => {
    var data
    var objSiteAdd = new SiteAdd


    beforeEach(function () {
        var url = new Utility().getBaseUrl()
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
        data = logindata
        cy.visit(url)
        objSiteAdd.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Site")
        })
    })

    it("TC_01_Check initial display defaut in all fields", () => {
        objSiteAdd.shouldUICreateSiteScreen()
    })

    it("TC_07: Check default value of Customer Name dropdown list\n" + "TC_08: Check displayed data of Customer Name dropdown list when update data on Customer screen\n" + "TC_09: Check when type the key search on Customer Name dropdown list", () => {
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.createNewSite("AQAVMO", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.common.searchByCondition(number)
            cy.wait(1000)
            objSiteAdd.common.searchResultWithOutValue().should("be.visible")
            objSiteAdd.common.openDetailsView()
            cy.wait(3000)
            objSiteAdd.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteAdd.shouldDataCustomercorrect("Enter Customer name", "AQAVMO")
            // TC_08 Check displayed data of Customer Name dropdown list when update data on Customer screen
            objSiteAdd.editCustomer("AutomationTester")
            cy.reload()
            objSiteAdd.leftMenu.navigateToSubPage("Master Data" ,"Site")
            cy.wait(2000)
            objSiteAdd.common.searchByCondition(number)
            cy.wait(2000)
            objSiteAdd.common.searchResultWithOutValue().should("be.visible")
            objSiteAdd.common.openDetailsView()
            cy.wait(3000)
            objSiteAdd.createSite.btnEditSite().click()
            cy.wait(2000)
            objSiteAdd.createSite.ipInformation("Enter Customer name").type("AutomationTester")
            objSiteAdd.createSite.selectCustomerName("AutomationTester").should("be.visible")
            cy.reload()
            cy.wait(2000)
            objSiteAdd.editCustomer("AQAVMO")
        })
    })

    it("TC_10: Check when input data (text, numerical values and special character) into Site Code text box\n" + 
    "TC_11: Check when input all text into Site Code text box\n" + "TC_12: Check when input all numerical values into Site Code text box\n" + "TC_13: Check when input all special characters into Site Code text box\n" + 
    "TC_14: Check when input less than maxlength into Site Code text box\n" + "TC_15: Check when input equal to maxlength into Site Code text box\n" + "TC_16: Check when input more than maxlength into Site Code text box\n" + "TC_17: Check when get blank Site Code text box" , () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.validationInputData("10", "Enter Code", "12345abcde", "20")
        objSiteAdd.validationInputData("10", "Enter Code", "abcdeabcde", "20")
        objSiteAdd.validationInputData("10", "Enter Code", "1234567890", "20")
        objSiteAdd.validationInputData("10", "Enter Code", "@@@@@@@@@@", "20")
        objSiteAdd.validationInputData("20", "Enter Code", "1234567890abcdeabcde", "20")
        objSiteAdd.validationInputData("20", "Enter Code", "1234567890abcdedabcdeadadada", "20")
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        // objSiteAdd.cancelCreateSite()
    })

    it("TC_18: Check when input existing code into Site Code textbox", () => {
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.msgCreateSiteSuccess().should("be.visible")
            objSiteAdd.createSite.createNewSite("Automation", number, "12312312345", "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            cy.wait(2000)
            objSiteAdd.msgInforAlreadyExist("Site Code is already existed").scrollIntoView().should("be.visible")
        })
    })

    it("TC_19: Check when input data (text, numerical values and special character) into Site Name text box\n" + 
    "TC_20: Check when input all text into Site Name text box\n" + "TC_21: Check when input all numerical values into Site Name text box\n" + "TC_22: Check when input all special characters into Site Name text box\n" + 
    "TC_23: Check when input less than maxlength into Site Name text box\n" + "TC_24: Check when input equal to maxlength into Site Name text box\n" + "TC_25: Check when input more than maxlength into Site Name text box\n" + "TC_26: Check when get blank Site Name text box" , () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.validationInputDataSiteName("10", "Enter Site name", "12345abcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter Site name", "abcdeabcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter Site name", "1234567890", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter Site name", "@@@@@@@@@@", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("150", "Enter Site name", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("160", "Enter Site name", "150")
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_27: Check when input existing code into Site Name textbox", () => {
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.createNewSite("Automation", "hhaah31313", number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            cy.wait(2000)
            objSiteAdd.msgInforAlreadyExist("Site name is already existed").scrollIntoView().should("be.visible")
            objSiteAdd.cancelCreateSite()
        })
    })

    it("TC_28: Check when input data (text, numerical values and special character) into Site Short name text box\n" + 
    "TC_29: Check when input all text into Site Short name text box\n" + "TC_30: Check when input all numerical values into Site Short name text box\n" + "TC_31: Check when input all special characters into Site Short name text box\n" + 
    "TC_32: Check when input less than maxlength into Site Short name text box\n" + "TC_33: Check when input equal to maxlength into Site Short name text box\n" + "TC_34: Check when input more than maxlength into Site Short name text box\n" + "TC_35: Check when get blank Site Short name text box" , () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.validationInputData("10", "Enter Site short name", "12345abcde", "50")
        objSiteAdd.validationInputData("10", "Enter Site short name", "abcdeabcde", "50")
        objSiteAdd.validationInputData("10", "Enter Site short name", "1234567890", "50")
        objSiteAdd.validationInputData("10", "Enter Site short name", "@@@@@@@@@@", "50")
        objSiteAdd.ipSiteShortNameMoreThanMaxlength("50", "Enter Site short name", "50")
        objSiteAdd.ipSiteShortNameMoreThanMaxlength("60", "Enter Site short name", "50")
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_37: Check default value of Site Type dropdown list\n" + 
    "TC_38: Check when get blank Site Type dropdown list\n" + 
    "TC_39: Check displayed data of  Site Sub-Type dropdown list when select Site Type is Store", () => {
        cy.fixture("site.json").then((siteListData) => {
            data = siteListData
            objSiteAdd.clickBtnCreate()
            objSiteAdd.checkDataServiceGroup("Enter Site type", data.siteType)
            objSiteAdd.createSite.ipInformation("Select Site type group").invoke('attr', 'disabled').then((status) => {
                expect(status).eq("disabled")
            })
            objSiteAdd.createSite.selectValue("Store").click()
            objSiteAdd.checkDataServiceGroup("Select Site type group", data.siteSubTypestore)
        })
    })

    it("TC_40: Check default value of Site Type dropdown list", () => {
        cy.fixture("site.json").then((siteListData) => {
            data = siteListData
            objSiteAdd.clickBtnCreate()
            objSiteAdd.createSite.selectInformation("Enter Site type", "DC")
            objSiteAdd.checkDataServiceGroup("Select Site type group", data.siteSubDC)
            // objSiteAdd.cancelCreateSite()
        })
    })

    it("TC_41: Check when select Site Sub- type = Blank if select Site Type is Store\n" + 
    "TC_42: Check when input data (text, numerical values and special character) into Site Sub-Type: Additional detail text box\n" + 
    "TC_43: Check when input all text into Site Sub-Type: Additional detail text box\n" + 
    "TC_44: Check when input all numerical values into Site  Sub-Type: Additional detail text box\n" + 
    "TC_45: Check when input all special characters into Site Sub-Type: Additional detail text box\n" + 
    "TC_46: Check when input less than maxlength into Site Sub-Type: Additional detail text box\n" + 
    "TC_47: Check when input equal to maxlength into Site Sub-Type: Additional detail text box\n" + 
    "TC_48: Check when input more than maxlength into Site Sub-Type: Additional detail text box\n" +
    "TC_49: Check when get blank Site Sub-Type: Additional detail text box\n", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.createSite.selectInformation("Enter Site type", "Store")
        objSiteAdd.common.selectDataByClick("Select Site type group", "Other")
        objSiteAdd.createSite.ipTextareaInfor("Enter additional detail").should("be.visible")
        objSiteAdd.validationInputDataSiteName("10", "Enter additional detail", "12345abcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter additional detail", "abcdeabcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter additional detail", "1234567890", "150")
        objSiteAdd.validationInputDataSiteName("10", "Enter additional detail", "@@@@@@@@@@", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("150", "Enter additional detail", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("160", "Enter additional detail", "150")
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_51: Check default status of date checkboxes on Open Hour tab\n" + 
    "TC_52: Check when uncheck date checkbox on Open Hour tab\n" + 
    "TC_54: Check display when no data on table list on Holiday, Special Day-Off tab", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.defaultCheckboxOpenHourTab()
        objSiteAdd.uncheckDayOpenHour("Monday")
        objSiteAdd.shouldNoDataOnWindowTimeTab()
    })

    it("TC_55: Check default sorting data of table list Holiday, Special Day-Off\n" + 
    "TC_56: Check sorting by Date column\n" + 
    "TC_57: Check amount of records displaying per page\n" + 
    "TC_58: Check changing page with back and next arrow \n" + 
    "TC_61: Check when select current date for 'Date' field\n" + 
    "TC_75: Check when get blank  Remark  text box\n" + 
    "TC_76: Check when input existing text into Remark textbox", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-holiday", "1", "today", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.common.btnJumpPageNext().should("be.disabled")
        objSiteAdd.common.btnJumpPagePrev().should("be.disabled")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "nextday", "Add", "Special day-off")
        objSiteAdd.siteList.sortAscending("Date").click()
        objSiteAdd.siteList.sortDescending("Date").click()
        objSiteAdd.common.selectPageSize("30/page")
        objSiteAdd.common.selectPageSize("10/page")
        cy.wait(2000)
        objSiteAdd.common.btnJumpPageNext().should("be.enabled")
        objSiteAdd.common.btnJumpPageNext().click()
        objSiteAdd.common.pageNumber(2).should("be.visible")

    })

    it("TC_59: Check display of Add Holiday, Special Day -Off screen\n" + 
    "TC_61: Check when select current date for 'Date' field\n" + 
    "TC_62: Check cancel selecting date for 'Date' field", () => {
        objSiteAdd.navigateToSpecialScreen("tab-holiday", "1")
        cy.wait(2000)
        objSiteAdd.titleSpecialScreen("Special Day-Off").should("be.visible")
        objSiteAdd.selectSpecialDate().should("be.visible")
        objSiteAdd.txtAreaSpecialRemark("Special day-off").should("be.visible")
        objSiteAdd.statusBtnConfirm("Add", "2").should("be.disabled")
        objSiteAdd.statusBtnConfirm("Cancel", "1").should("be.enabled")
        objSiteAdd.selectSpecialDate().click()
        cy.selectDay("today")
        objSiteAdd.common.btnConfirmDatePicker("Cancel").click()
        objSiteAdd.common.btnConfirmDatePicker("Set").should("not.be.visible")
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })

    it("TC_63: Check default value of  Start Time time picker when select Date = current date", () => {
        objSiteAdd.navigateToSpecialScreen("tab-holiday", "1")
        cy.wait(2000)
        objSiteAdd.selectSpecialDate().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSiteAdd.common.divAvailableDate().contains(date)
    } )

    it( "TC_65: Check default value of  End Time time picker when select Date = current date\n" +
        "TC_66: Check default value of End Time time picker when select end date > current date\n" +
        "TC_67: Check when select Start Time = End Time" , () => {
        objSiteAdd.navigateToSpecialScreen("tab-holiday", "1")
        objSiteAdd.selectSpecialDate().click()
        cy.selectDay("today")
        objSiteAdd.common.btnConfirmDatePicker("Set").click()
        var hour = new Date().getHours()
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        objSiteAdd.selectSpecialTime("Start time").click()
        var hourPast = hour - 1
        cy.wait(1000)
        objSiteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSiteAdd.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSiteAdd.createSite.selectHourAndMinutes(minutes2, "2").click()
        } else if (minutes > 23) {
            objSiteAdd.createSite.selectHourAndMinutes(minutes, "1").click()
        } else {
            objSiteAdd.createSite.selectHourAndMinutes(minutes, "2").click()
        }
        objSiteAdd.createSite.btnOK().click()
        cy.wait(2000)
        objSiteAdd.selectSpecialTime("End time").click()
        cy.wait(1000)
        objSiteAdd.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        if (hour < minutes) {
            objSiteAdd.timeDisabled(minutes, "1").scrollIntoView().contains(minutes)
        } else if (hour > minutes) {
            objSiteAdd.timeDisabled(minutes, "2").scrollIntoView().contains(minutes)
        }
        // objSiteAdd.timeDisabled(minutes).scrollIntoView().contains(minutes)
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })

    it("TC_68: Check when input data (text, numerical values and special character) into Remark text box\n" + 
    "TC_69: Check when input all text into Remark text box\n" + 
    "TC_70: Check when input all numerical values into Remark text box\n" + 
    "TC_71: Check when input all special characters into Remark text box\n" + 
    "TC_72: Check when input less than maxlength into Remark text box\n" + 
    "TC_73: Check when input equal to maxlength into Remark text box\n" + 
    "TC_74: Check when input more than maxlength into Remark text box", () => {
        objSiteAdd.navigateToSpecialScreen("tab-holiday", "1")
        objSiteAdd.validationInputDataSiteName("10", "Special day-off", "12345abcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-off", "abcdeabcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-off", "1234567890", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-off", "@@@@@@@@@@", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("150", "Special day-off", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("160", "Special day-off", "150")
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })
    
    it("TC_77: Check display when hover mouse on Special Day-off date", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-holiday", "1", "today", "Add", "Special day-off")
        objSiteAdd.windowTimeTab("tab-holiday").click()
        cy.wait(1000)
        objSiteAdd.btnAdd("1").click()
        cy.wait(2000)
        objSiteAdd.selectSpecialDate().click()
        objSiteAdd.todayDisabled().trigger('mouseover')
        objSiteAdd.tooltipSpecialDayOf().should("exist")
    })
    
    it("TC_78: Check click confirm to cancel to add new Holiday, Special Day-Off\n" + 
    "TC_79: Check click cancel to cancel to add new Holiday, Special Day-Off\n" + 
    "TC_80: Check Delete of Holiday, Special Day-off\n" + 
    "TC_81: Check when create Special day off same date and time as previous Special day off", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-holiday", "1", "today", "Cancel", "Special day-off")
        objSiteAdd.confirmCancelCreateSpecialDayOff("confirm")
        objSiteAdd.addSpecialDay("tab-holiday", "1", "today", "Cancel", "Special day-off")
        objSiteAdd.confirmCancelCreateSpecialDayOff("cancel", "Special Day-Off")
        objSiteAdd.btnConfirmAddSpecialDay("Add").click()
        cy.wait(2000)
        objSiteAdd.btnAdd("1").click()
        objSiteAdd.selectSpecialDate().click()
        objSiteAdd.divAvailableTodayDate().should("not.exist")
        objSiteAdd.btnConfirmQuitSpecialOff("Cancel").click()
        objSiteAdd.statusBtnConfirm("Cancel", "1").click()
        objSiteAdd.confirmCancelCreateSpecialDayOff("confirm")
        objSiteAdd.iconDeleteSpecialDay().scrollIntoView().click()
        objSiteAdd.divNoData("1").should("be.visible")
    })

    it("TC_83: Check display when no data on table list on Holiday, Special Day-on tab", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.shouldNoDataOnWindowTimeTab()
    })

    it("TC_84: Check sorting data of table list Holiday, Special Day-On\n" + 
    "TC_85: Check sorting by Date column\n" + 
    "TC_86: Check amount of records displaying per page\n" + 
    "TC_87: Check changing page with back and next arrow \n" + 
    "TC_89: Check default value of Date picker on Holiday special day-on\n" + 
    "TC_105: Check when get blank  in Remark text box on Holiday special Day-On\n" + 
    "TC_106: Check when input existing text into Remark textbox on Holiday special Day-On", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-special", "2", "today", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.common.btnJumpPageNext().should("be.disabled")
        objSiteAdd.common.btnJumpPagePrev().should("be.disabled")
        objSiteAdd.addSpecialDay("tab-special", "2", "nextday", "Add", "Special day-on")
        objSiteAdd.siteList.sortAscending("Date").click()
        objSiteAdd.siteList.sortDescending("Date").click()
        objSiteAdd.common.selectPageSize("30/page")
        objSiteAdd.common.selectPageSize("10/page")
        cy.wait(2000)
        objSiteAdd.common.btnJumpPageNext().should("be.enabled")
        objSiteAdd.common.btnJumpPageNext().click()
        objSiteAdd.common.pageNumber(2).should("be.visible")

    })

    it("TC_82, 88: Check display of Add Holiday, Special Day -On screen\n" + 
    "TC_90: Check when select current date for 'Date' field\n" + 
    "TC_92: Check cancel selecting date for 'Date' field", () => {
        objSiteAdd.navigateToSpecialScreen("tab-special", "2")
        cy.wait(2000)
        objSiteAdd.titleSpecialScreen("Special Day-On").should("be.visible")
        objSiteAdd.selectSpecialDate().should("be.visible")
        objSiteAdd.txtAreaSpecialRemark("Special day-on").should("be.visible")
        objSiteAdd.statusBtnConfirm("Add", "3").should("be.disabled")
        objSiteAdd.statusBtnConfirm("Cancel", "1").should("be.enabled")
        objSiteAdd.selectSpecialDate().click()
        cy.selectDay("today")
        objSiteAdd.common.btnConfirmDatePicker("Cancel").click()
        objSiteAdd.common.btnConfirmDatePicker("Set").should("not.be.visible")
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })

    it("TC_93: Check default value of  Start Time time picker when select Date = current date on Holiday special Day-On", () => {
        objSiteAdd.navigateToSpecialScreen("tab-special", "2")
        cy.wait(2000)
        objSiteAdd.selectSpecialDate().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSiteAdd.common.divAvailableDate().contains(date)
    } )

    it( "TC_95: Check default value of  End Time time picker when select Date = current date\n" +
        "TC_96: Check default value of End Time time picker when select end date > current date\n" +
        "TC_97: Check when select Start Time = End Time" , () => {
        objSiteAdd.navigateToSpecialScreen("tab-special", "2")
        objSiteAdd.selectSpecialDate().click()
        cy.selectDay("today")
        objSiteAdd.common.btnConfirmDatePicker("Set").click()
        var hour = new Date().getHours()
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        objSiteAdd.selectSpecialTime("Start time").click()
        var hourPast = hour - 1
        cy.wait(1000)
        objSiteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSiteAdd.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSiteAdd.createSite.selectHourAndMinutes(minutes2, "2").click()
        } else if (minutes > 23) {
            objSiteAdd.createSite.selectHourAndMinutes(minutes, "1").click()
        } else {
            objSiteAdd.createSite.selectHourAndMinutes(minutes, "2").click()
        }
        objSiteAdd.createSite.btnOK().click()
        cy.wait(2000)
        objSiteAdd.selectSpecialTime("End time").click()
        cy.wait(1000)
        objSiteAdd.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        if (hour < minutes) {
            objSiteAdd.timeDisabled(minutes, "1").scrollIntoView().contains(minutes)
        } else if (hour > minutes) {
            objSiteAdd.timeDisabled(minutes, "2").scrollIntoView().contains(minutes)
        }
        // objSiteAdd.timeDisabled(minutes).scrollIntoView().contains(minutes)
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })

    it.only("TC_98: Check when input data (text, numerical values and special character) into Remark text box\n" + 
    "TC_99: Check when input all text into Remark text box\n" + 
    "TC_100: Check when input all numerical values into Remark text box\n" + 
    "TC_101: Check when input all special characters into Remark text box\n" + 
    "TC_102: Check when input less than maxlength into Remark text box\n" + 
    "TC_103: Check when input equal to maxlength into Remark text box\n" + 
    "TC_104: Check when input more than maxlength into Remark text box", () => {
        objSiteAdd.navigateToSpecialScreen("tab-special", "2")
        objSiteAdd.validationInputDataSiteName("10", "Special day-on", "12345abcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-on", "abcdeabcde", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-on", "1234567890", "150")
        objSiteAdd.validationInputDataSiteName("10", "Special day-on", "@@@@@@@@@@", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("150", "Special day-on", "150")
        objSiteAdd.ipSiteNameMoreThanMaxlength("160", "Special day-on", "150")
        objSiteAdd.btnClosePopup().last().click()
        objSiteAdd.btnConfirmQuitSpecialOff("Confirm").click()
    })
    
    it.only("TC_107: Check display when hover mouse on Special Day-on date", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-special", "2", "today", "Add", "Special day-on")
        objSiteAdd.windowTimeTab("tab-special").click()
        cy.wait(1000)
        objSiteAdd.btnAdd("2").click()
        cy.wait(2000)
        objSiteAdd.selectSpecialDate().click()
        objSiteAdd.todayDisabled().should("be.visible").scrollIntoView().trigger('mouseover')
        objSiteAdd.tooltipSpecialDayOf().should("exist")
    })
    
    it.only("TC_108: Check click confirm to cancel to add new Holiday, Special Day-On\n" + 
    "TC_109: Check click cancel to cancel to add new Holiday, Special Day-On\n" + 
    "TC_110: Check Delete of Holiday, Special Day-on\n" + 
    "TC_111: Check when create Holiday Special day on same date and time as previous Holiday Special day on", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-special", "2", "today", "Cancel", "Special day-on")
        objSiteAdd.confirmCancelCreateSpecialDayOff("confirm")
        objSiteAdd.addSpecialDay("tab-special", "2", "today", "Cancel", "Special day-on")
        objSiteAdd.confirmCancelCreateSpecialDayOff("cancel", "Special Day-On")
        objSiteAdd.btnConfirmAddSpecialDay("Add").click()
        cy.wait(2000)
        objSiteAdd.btnAdd("2").click()
        objSiteAdd.selectSpecialDate().click()
        objSiteAdd.divAvailableTodayDate().should("not.exist")
        objSiteAdd.btnConfirmQuitSpecialOff("Cancel").click()
        objSiteAdd.statusBtnConfirm("Cancel", "1").click()
        objSiteAdd.confirmCancelCreateSpecialDayOff("confirm")
        objSiteAdd.iconDeleteSpecialDay().scrollIntoView().click()
        objSiteAdd.divNoData("2").should("be.visible")
    })

    it.only("TC_112: Check when create Holiday Special day on same date and time as Holiday Special day off", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.addSpecialDay("tab-holiday", "1", "today", "Add", "Special day-off")
        objSiteAdd.windowTimeTab("tab-special").click()
        objSiteAdd.btnAdd("2").click()
        objSiteAdd.selectSpecialDate().click()
        objSiteAdd.divAvailableTodayDate().should("not.exist")
        objSiteAdd.btnConfirmQuitSpecialOff("Cancel").click()
        objSiteAdd.statusBtnConfirm("Cancel", "1").click()
        objSiteAdd.confirmCancelCreateSpecialDayOff("confirm")
    })

    it.only("TC_113: Check display data dropdown list Title\n" + 
    "TC_114: Check select data on Title dropdown list\n" + 
    "TC_115: Get blank into First name field\n" + 
    "TC_120: Get blank into Last name field", () => {
        cy.fixture("site.json").then((siteListData) => {
            data = siteListData
            objSiteAdd.clickBtnCreate()
            objSiteAdd.checkDataServiceGroup("Select Title", data.title)
            objSiteAdd.common.selectDataByClick("Select Title", "นาย")
            objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteAdd.cancelCreateSite()
        })
    })

    it.only("TC_116: Check input 30 characters into  First name  field\n" + 
    "TC_117: Check input 31 characters into First name field\n" + 
    "TC_121: Check input 30 characters into Last name field\n" + 
    "TC_122: Check input 31 characters into Last name field", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.common.selectDataByClick("Select Title", "นาย")
        objSiteAdd.validationInputData("30", "Enter Key Contact Person’s first name", "abcdeabcdeabcdeabcdeabcdeabcde", "30")
        objSiteAdd.validationInputData("30", "Enter Key Contact Person’s first name", "abcdeabcdeabcdeabcdeabcdeabcdee", "30")
        // Input Validation LastName
        objSiteAdd.validationInputData("30", "Enter Key Contact Person’s last name", "abcdeabcdeabcdeabcdeabcdeabcde", "30")
        objSiteAdd.validationInputData("30", "Enter Key Contact Person’s last name", "abcdeabcdeabcdeabcdeabcdeabcdee", "30")
        objSiteAdd.cancelCreateSite()
    })

    it.only("TC_118: Check input special characters into  First name field\n" + 
    "TC_119: Check input number into  First name field\n" + 
    "TC_123: Check input special characters into Last name field\n" + 
    "TC_124: Check input number into Last name field", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.common.selectDataByClick("Select Title", "นาย")
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s first name", "1234567890", "Invalid First Name")
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s first name", "@@@@@@@@@@", "Invalid First Name")
        // Input Validation LastName
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s last name", "1234567890", "Invalid Last Name")
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s last name", "@@@@@@@@@@", "Invalid Last Name")
        objSiteAdd.cancelCreateSite()
    })

    it.only("TC_125: Add correct email into Email\n" +
        "TC_126: Get blank into Key contact email field", () => {
        cy.randomNumber(5).then((number) => {
            objSiteAdd.createSite.createSiteWithOutEmail("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.msgCreateSiteSuccess().should("be.visible")
        })
    })

    it.only("TC_127: Add  email without @ for Email field\n" + 
    "TC_128: Add  email without . for Email field\n" + 
    "TC_129: Add  email without domain for Email field", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abcgmail.com", "Invalid Email")
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abc@gmailcom", "Invalid Email")
        objSiteAdd.validationInputFirstLastName("Enter Key Contact Person’s email", "abc@gmail.", "Invalid Email")
        objSiteAdd.cancelCreateSite()
    })

    it.only("TC_131: Check select one value from dropdown list country code number  for Mobile phone no. field\n" + 
    "TC_132: Check input maxlength characters into Mobile  phone  no.\n", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.dropDownMobilePhone().scrollIntoView().click()
        objSiteAdd.createSite.selectValue("66").first().click({multiple : true, force : true})
        objSiteAdd.validationInputData("9", "Enter Mobile Phone no.", "999999999", "9" )
        objSiteAdd.validationInputData("9", "Enter Mobile Phone no.", "9999999999", "9" )
        objSiteAdd.cancelCreateSite()
    })

    it("TC_133: Check input over maxlength into Mobile  phone  no.\n" + 
    "TC_134: Check input space into Mobile phone no.\n" + 
    "TC_135: Check input text and special characters into Mobile phone no.\n" + 
    "TC_137: Get blank into Mobile phone no. field", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.dropDownMobilePhone().scrollIntoView().click()
        objSiteAdd.createSite.selectValue("66").first().click({multiple : true, force : true})
        objSiteAdd.validationInputData("0", "Enter Mobile Phone no.", "    ", "9" )
        objSiteAdd.validationInputData("0", "Enter Mobile Phone no.", "acbdceee", "9" )
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_138: Check select one value from dropdown list country code number  for Landline phone no. field\n" + 
    "TC_139: Check input maxlength characters into Landline phone no.\n" + 
    "TC_140: Check input over maxlength into Landline phone no.\n" + 
    "TC_141: Check input text and special characters into Landline phone no.\n" + 
    "TC_143: Get blank into Landline phone no. field", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.dropDownMobilePhone().scrollIntoView().click()
        objSiteAdd.createSite.selectValue("66").first().click({multiple : true, force : true})
        objSiteAdd.validationInputData("8", "Enter Landline phone no.", "99999999", "8" )
        objSiteAdd.validationInputData("8", "Enter Landline phone no.", "9999999999", "8" )
        objSiteAdd.validationInputData("0", "Enter Landline phone no.", "acbdce@@@", "8" )
        objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_144: Check maxlength into Extension\n" +
    "TC_145: Check input over maxlength into Extension\n" + 
    "TC_146: Check input characters different from numbers into Extension", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.validationInputData("8", "Extension", "99999999", "8")
        objSiteAdd.validationInputData("8", "Extension", "999999999", "8")
        objSiteAdd.validationInputData("0", "Extension", "acbdce@@@", "8")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_147: Check Add key contact\n" + 
    "TC_148: Check Add over 3 key contact person\n" + 
    "TC_149: Check Delete key contact person", () => {
        objSiteAdd.clickBtnCreate()
        objSiteAdd.btnAddKeyContact().click()
        cy.wait(2000)
        objSiteAdd.newKeyContact().should("be.visible")
        objSiteAdd.btnAddKeyContact().click()
        objSiteAdd.shouldbeDisabledBtnAddKeyContact().should("be.disabled")
        objSiteAdd.iconDeleteContact("3").click()
        objSiteAdd.iconDeleteContact("2").click()
        objSiteAdd.newKeyContact().should("not.exist")
        objSiteAdd.shouldbeDisabledBtnAddKeyContact().should("be.enabled")
        // objSiteAdd.cancelCreateSite()
    })

    it("TC_152: Check when input existing adress into Address textbox", () => {
        cy.randomNumber(5).then((number) => {
            objSiteAdd.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.msgCreateSiteSuccess().should("be.visible")
        })
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.msgCreateSiteSuccess().should("be.visible")
        })
    })

    it("TC_155: Check when get blank Province dropdown list\n" + 
    "TC_156: Check when type the key search on Province dropdown list\n" + 
    "TC_158: Check when get blank District dropdown list\n" + 
    "TC_160: Check when get blank Sub-District dropdown list\n" + 
    "TC_162: Check when get blank Postal Code dropdown list", () => {
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.addLocation("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay")
            objSiteAdd.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteAdd.createSite.ipCustomerName("Select Province", "นครราชสีมา")
            objSiteAdd.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteAdd.createSite.selectInformation("Select District", "ขามทะเลสอ")
            objSiteAdd.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteAdd.createSite.selectInformation("Select Sub-District", "บึงอ้อ")
            objSiteAdd.createSite.btnConfirmLocationStatus().should("be.disabled")
            objSiteAdd.common.btnImportPopupAttachFileDisabled().should("be.disabled")
            objSiteAdd.cancelCreateSite()
        })
    })

    it("TC_167: Check UI of Set Status screen\n" + 
    "TC_168: Check displayed value of Effective date/time date picker\n" + 
    "TC_169: Check displayed status when set effective date/time= current date/time and no end date/time", () => {
        objSiteAdd.navigateToSetStatusScreen()
        objSiteAdd.shouldUISetStatusScreen()
        objSiteAdd.selectEffectDateTime().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSiteAdd.common.divAvailableDate().contains(date)
        cy.selectDay("today")
        var hour = new Date().getHours()
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        var hourPast = hour - 1
        cy.wait(1000)
        objSiteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSiteAdd.hourAndMinutesSetStatus(hour, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSiteAdd.hourAndMinutesSetStatus(minutes2, "2").click()
        } else if (minutes > 23) {
            objSiteAdd.hourAndMinutesSetStatus(minutes, "1").click()
        } else {
            objSiteAdd.hourAndMinutesSetStatus(minutes, "2").click()
        }
        objSiteAdd.btnOKStatus().click()
    })

    it("TC_175: Check displayed status when set status = Inactive", () => {
        objSiteAdd.navigateToSetStatusScreen()
        objSiteAdd.shouldUISetStatusScreen()
        objSiteAdd.siteList.rdBtnSetStatus("Inactive").click()
        objSiteAdd.siteList.btnSetStatus().click()
        cy.wait(2000)
        objSiteAdd.statusSite("Inactive").scrollIntoView().should("be.visible")
        objSiteAdd.cancelCreateSite()
    })

    it("TC_176: Check click confirm to cancel to set status\n" + 
    "TC_177: Check click cancel to cancel to set status", () => {
        objSiteAdd.navigateToSetStatusScreen()
        objSiteAdd.shouldUISetStatusScreen()
        objSiteAdd.siteList.rdBtnSetStatus("Inactive").click()
        objSiteAdd.cancelSetStatusSite("no", "3")
        objSiteAdd.cancelSetStatusSite("yes")
    })

    it("TC_178: Check create a site successfully\n" + 
    "TC_179: Check click confirm to cancel to create a new site\n" + 
    "TC_180: Check click cancel to cancel to create a new site", () => {
        cy.randomNumber(10).then((number) => {
            objSiteAdd.createSite.createNewSite("Automation", number, number, "Store", "Compact HypermarketU", "นาย", "Trinh", "Dai", "daicodong", "SonTay", "นครราชสีมา", "ขามทะเลสอ", "บึงอ้อ", "30280")
            objSiteAdd.createSite.msgCreateSiteSuccess().should("be.visible")
        })
        // Check click cancel to cancel to create a new site
        objSiteAdd.clickBtnCreate()
        objSiteAdd.createSite.ipCustomerName("Enter Customer name", "Automation")
        objSiteAdd.common.btnConfirmHintPopup("Cancel", "1").click()
        cy.wait(1000)
        objSiteAdd.common.btnConfirmHintPopup("Cancel", "2").click()
        objSiteAdd.titleSiteAdd().should("be.visible")
        // Check click confirm to cancel to create a new site
        objSiteAdd.cancelCreateSite()
        objSiteAdd.siteList.titlePage("Site").should("be.visible")
    })

})