import Login from "../../../support/pages/TMS/components/login/Login"
import Logout from "../../../support/pages/TMS/components/logout/Logout"
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import E2E from "../../../support/pages/E2E/e2e"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"


describe("Implement Function", () => {
    var data, objLogin, objHome, objLoginPRD
    var objPickup = new Pickup(), objE2E = new E2E(), objDeliveryPlan = new DeliveryPlan()
    var objcreateOrder
    var objDeliveryGood = new DeliveryGood()
    var msg = "background processing, you will be notifed you through the system information after the processing is completed. during this period, please do not update pick up/delivery status for orders!";


    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it.skip("TC_28 - Delivery multiple orders with the same waybill status - For_delivered status ", () => {
        cy.randomNumber(21).then((trackingNum) => {
            objPickup.createOrderAndSchedule(trackingNum, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
            cy.wait(50000)
            cy.reload()
            objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(2000)
            objPickup.common.searchByClickSuggestion("dispatch number", trackingNum)
            objPickup.common.openDetailView()
            objPickup.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill)
                // Update shiment status to Shipment Loading
                objPickup.leftMenu.navigateToPage("Activity Tracking")
                objPickup.tabShipment().click()
                objPickup.txtSearch().type(waybill)
                objPickup.iconSearch().click()
                objPickup.shipmentNo(waybill).click()
                objPickup.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                objPickup.upDateShipmentStatus("Update driver", "Shipment_Loading")
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
            })
            cy.reload()
            cy.wait(10000)
            cy.randomNumber(21).then((trackingNum1) => {
                objPickup.createOrderAndSchedule(trackingNum1, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
                cy.wait(50000)
                cy.reload()
                objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(2000)
                objPickup.common.searchByClickSuggestion("dispatch number", trackingNum1)
                objPickup.common.openDetailView()
                objPickup.wayBillNumber().invoke('text').then((wayBillNumber) => {
                    var waybill = wayBillNumber.trim().substring(15)
                    cy.log("waybillNumber = " + waybill)
                    // Update shiment status to Shipment Loading
                    objPickup.leftMenu.navigateToPage("Activity Tracking")
                    objPickup.tabShipment().click()
                    objPickup.txtSearch().type(waybill)
                    objPickup.iconSearch().click()
                    objPickup.shipmentNo(waybill).click()
                    objPickup.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                    objPickup.upDateShipmentStatus("Update driver", "Shipment_Loading")
                    objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                })
                objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
                objPickup.select2OrderandBatchPickup("yes", msg)
            })
        })
    })

    // it.skip("TC_29 - Delivery multiple orders with the same waybill status - Not For_delivered status ", () => {
    //     objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //     var msgerror = "the state of transportation must be for the state of delivery"
    //     objPickup.choose2OrderandclickbtnBatchPickup()
    //     objPickup.msgBatchPickupsuccess().contains(msgerror)
    // })

    // it.skip("TC_30 - Pickup multiple orders doesn't same carrier", () => {
    //     cy.randomNumber(21).then((number) => {
    //         objPickup.createOrderAndSchedule(number, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //     })
    //     cy.reload()
    //     cy.wait(10000)
    //     cy.randomNumber(21).then((number) => {
    //         objPickup.createOrderAndSchedule(number, "Jared_Carrier", "Jared_Carrier")
    //     })
    //     cy.wait(5000)
    //     cy.reload()
    //     cy.wait(55000)
    //     objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //     objPickup.choose2OrderandclickbtnBatchPickup()
    //     cy.wait(2000)
    //     objPickup.confirmPickup("yes")
    //     objPickup.msgBatchPickupsuccess().should("be.visible")
    // })

    // it("TC_31 - Pickup multiple orders with the different waybill status ", () => {
    //     cy.randomNumber(21).then((number) => {
    //         objPickup.createOrderAndSchedule(number, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //     })
    //     cy.reload()
    //     cy.wait(10000)
    //     cy.randomNumber(21).then((number) => {
    //         objPickup.createOrderAndSchedule(number, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         objPickup.select2OrderandBatchPickup("yes", msg)
    //     })
    //     cy.reload()
    //     cy.wait(10000)
    //     cy.randomNumber(21).then((number) => {
    //         objPickup.createOrderAndSchedule(number, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         objPickup.choose2OrderandclickbtnBatchPickup()
    //         objPickup.msgBatchPickupsuccess().contains("the state of transportation must be for the state of delivery")
    //     })
    // })

    // // TC_32 + TC_39
    // it("TC_32, TC_39  - Set up pickup (arrival and loading) and delivery complement time successfully", () => {
    //     cy.randomNumber(20).then((trackingnumber) => {
    //         objPickup.createOrderAndSchedule(trackingnumber, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(5000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", trackingnumber)
    //         cy.wait(2000)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         // objPickup.chooseStartTime()
    //         objPickup.chooseCompletionTime(10)
    //         // objPickup.chooseArrivalTime(5)
    //         cy.wait(1000)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         cy.wait(2000)
    //         objPickup.confirmPickup("yes")
    //         cy.wait(2000)
    //         objPickup.btnconfirm().click()
    //         cy.wait(10000)
    //         objPickup.dispatchPickupStatus("shipped").should("be.visible")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "On the way ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("please enter the dispatch number", trackingnumber)
    //         cy.wait(2000)
    //         objPickup.dispatchPickupStatus("transited").should("be.visible")
    //     })
    // })

    // // TC_33 + TC_42
    // it("TC_33, TC_42 - Set up arrival time only - Successfully", () => {
    //     cy.randomNumber(20).then((trackingnum) => {
    //         objPickup.createOrderAndSchedule(trackingnum, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", trackingnum)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.chooseArrivalTimeNow()
    //         cy.wait(1000)
    //         objPickup.clickbtnFunctionPickup("empty")
    //         objPickup.confirmPickup("yes")
    //         cy.wait(5000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToPage("Abnormal ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("order number", trackingnum)
    //         cy.wait(2000)
    //         objPickup.abnormalException("empty").should("be.visible")
    //     })
    // })

    // it("TC_34 - Save Pickup time successfully " , () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.chooseArrivalTimeNow()
    //         cy.wait(1000)
    //         objPickup.chooseCompletionTime(10)
    //         objPickup.clickbtnFunctionPickup("save")
    //         objPickup.confirmPickup("yes")
    //         objPickup.msgBatchPickupsuccess().contains("done successfully")
    //     })
    // })

    // it("TC_35 - Set up pickup  delivery complement time successfully " , () => {
    //     cy.randomNumber(20).then((num) => {
    //         objPickup.createOrderAndSchedule(num, "SYS_DEF_INDIVIDUAL", "SYS_DEF_INDIVIDUAL")
    //         cy.wait(55000)
    //         cy.reload()
    //         cy.wait(10000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Pick Up ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", num)
    //         objPickup.dispatchPickupStatus("to be picked up").should("be.visible")
    //         cy.wait(5000)
    //         objPickup.common.openDetailView()
    //         cy.wait(2000)
    //         objPickup.chooseCompletionTime(10)
    //         objPickup.clickbtnFunctionPickup("pickup compeleted")
    //         cy.wait(2000)
    //         objPickup.confirmPickup("yes")
    //         cy.wait(2000)
    //         objPickup.btnconfirm().click()
    //         cy.wait(10000)
    //         objPickup.dispatchPickupStatus("shipped").should("be.visible")
    //         cy.reload()
    //         cy.wait(55000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "On the way ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("please enter the dispatch number", num)
    //         cy.wait(2000)
    //         objPickup.dispatchPickupStatus("transited").should("be.visible")
    //         cy.reload()
    //         cy.wait(55000)
    //         objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods ")
    //         cy.wait(2000)
    //         objPickup.common.searchByClickSuggestion("please enter the tracking number", num)
    //         objPickup.ckbChooseOrder("3").should("be.visible")
    //     })
    // })

    it.skip("TC: New Flow Pick up and Delivery Order", () => {
        cy.randomString(10).then((num) => {
            objPickup.leftMenu.navigateToPage("Shipping order")
            objPickup.shippingorder.navigateToCreateShippingOrderPage()
            objPickup.createshippingorder.createNewShippingOrderWithCargoDetailForQuantitySplit("confirm", num, "Automation Company", "Service for Shipper", "Frozen", "AutoTestSite99", "SiteTestAuto", 20, 10, 20, 30)
            cy.reload()
            objDeliveryPlan.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            // Step 8: Route Split Order
            objDeliveryPlan.routeSplit("waybill number", num, "first", "AutoTestSite99")
            cy.reload()
            objDeliveryPlan.common.searchByClickSuggestionAtDeliveryPlanPageForOrderSplit("waybill number", num, "first", 1)
            //create dispach order
            objDeliveryPlan.createADispatchOrder(num)
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", num, "second")
            objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
            cy.wait(2000)
            objDeliveryPlan.header.searchResult(num).should("be.visible")
            //confirm dispatch
            objDeliveryPlan.confirmDispatch()
            objDeliveryPlan.common.msgSuccess().should("be.visible")
            cy.reload()
            objE2E.latePickUpAbnormal(num, "implement/images.png", num, "normal")
        })
    })

    it.only("TC: New Flow Pick up and Delivery Order", () => {
        cy.randomNumber(20).then((num) => {
            objPickup.createOrderAndSchedule(num, "Automation Company", "Service for Shipper", "Frozen", "AutoTestSite99", "SiteTestAuto", 20)
            cy.wait(50000)
            cy.reload()
            objE2E.latePickUpAbnormal(num, "implement/images.png", num, "normal")
        })
    })

})