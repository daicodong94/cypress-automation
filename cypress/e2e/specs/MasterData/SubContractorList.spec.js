import SubcontractorList from "../../../support/pages/MasterData/components/subcontractor/SubcontractorList"
import { Utility } from "../../../support/utility"


describe("MasterData - Subcontractor List", () => {
    var data
    var objSubContractorList = new SubcontractorList

    beforeEach(function () {
        var url = new Utility().getBaseUrl()
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
        data = logindata
        cy.visit(url)
        objSubContractorList.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Subcontractor")
        })
    })

    it("TC_5: Check the display of table list when search unexisting data on search bar\n" + 
    "TC_1: Check initial display defaut in all fields\n" + 
    "TC_6: Check when inputing including space in search bar\n" + 
    "TC_7: Check when inputing key search of Sub-contractor ID\n" + 
    "TC_8: Check when inputing key search of Company Tax ID\n" + 
    "TC_9: Check when inputing key search of Company Registered Name\n" + 
    "TC_10: Check when inputing key search of Company Name (TH)\n" + 
    "TC_11: Check when inputing key search of Company Name (EN)\n" + 
    "TC_12: Check when inputing key search of Province" , () => {
        cy.randomNumber(20).then((searchValue) => {
            objSubContractorList.titleSubcontractorList().should("be.visible")
            objSubContractorList.searchByCondition(searchValue)
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            objSubContractorList.searchByCondition("AutomationTesting      ")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Subcontractor ID
            objSubContractorList.searchByCondition("ANL-S000000000000830")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Company tax ID
            objSubContractorList.searchByCondition("9999999999999")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Company Register Name
            objSubContractorList.searchByCondition("AutomationTesting")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Company Name (TH)
            objSubContractorList.searchByCondition("AQAVMO")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Company Name (ENG)
            objSubContractorList.searchByCondition("AQAVMO")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
            // Search by Province
            objSubContractorList.searchByCondition("แดนเนรมิต แดนเนรมิต")
            objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        })  
    })

    it("TC_13: Check when search by unexsiting Subcontractor ID\n" + 
    "TC_14: Check when search by unexsiting Company Tax ID\n" + 
    "TC_15: Check when search by unexsiting Company Registered Name\n" + 
    "TC_16: Check when search by unexsiting Company Name (TH)\n" + 
    "TC_17: Check when search by unexsiting Company Name (EN)\n" + 
    "TC_18: Check when search by unexsiting Province" , () => {
        cy.randomNumber(20).then((searchValue) => {
            objSubContractorList.searchByCondition(searchValue)
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Subcontractor ID
            objSubContractorList.searchByCondition("ANL-S0000000000023")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Company tax ID
            objSubContractorList.searchByCondition("123323")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Company Register Name
            objSubContractorList.searchByCondition("AutomationVietNam")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Company Name (TH)
            objSubContractorList.searchByCondition("AQAVMO1234")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Company Name (ENG)
            objSubContractorList.searchByCondition("AQAVMO1234")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
            // Search by unexisting Province
            objSubContractorList.searchByCondition("SonTay")
            objSubContractorList.common.divNoData().should("be.visible")
            objSubContractorList.btnExport().should("be.disabled")
        })
    })

    it("TC_19: Check the display when click on Filter button\n" + 
    "TC_20: Check when choosing Active in Status dropdown list\n" + 
    "TC_21: Check when choosing Inactive in Status dropdown list", () => {
        objSubContractorList.clickBtnFilter()
        objSubContractorList.dropdownSubcontractorStatus().should("be.visible")
        objSubContractorList.searchingByValueOfDropdown("All", "1", "Active")
        objSubContractorList.searchingByValueOfDropdown("All", "1", "Inactive")
    })

    it("TC_25: Check horizontal scroll\n" + 
    "TC_26: Check Scroll up/down\n" + 
    "TC_27: Check click icon Set status ", () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.siteList.scrollFunction()
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.siteList.titlePage("Set status").should("be.visible")
    })

    it("TC_28: Check default of Set status screen\n" + 
    "TC_29: Check display of Effective date/time picker", () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.siteList.titlePage("Set status").should("be.visible")
        objSubContractorList.siteList.rdBtnSetStatus("Active").should("be.visible")
        objSubContractorList.siteList.rdBtnSetStatus("Inactive").should("be.visible")
        objSubContractorList.siteList.setStatusChecked("Active").should("be.visible")
        objSubContractorList.siteList.setStatusChecked("No end date").should("be.visible")
        objSubContractorList.btnActionSetStatus("Cancel").should("be.enabled")
        objSubContractorList.btnActionSetStatus("Set").should("be.disabled")
        objSubContractorList.siteAdd.selectEffectDateTime().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSubContractorList.common.divAvailableDate().contains(date)
        cy.selectDay("today")
        var hour = new Date().getHours()
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        var hourPast = hour - 1
        cy.wait(1000)
        objSubContractorList.siteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSubContractorList.siteAdd.hourAndMinutesSetStatus(hour, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes2, "2").click()
        } else if (minutes > 23) {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes, "1").click()
        } else {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes, "2").click()
        }
        objSubContractorList.siteAdd.btnOKStatus().click()
    })

    it("TC_35: Check change End date/time > Effective date/ time" , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.siteList.titlePage("Set status").should("be.visible")
        objSubContractorList.siteList.rdBtnSetStatus("Select date / time").click()
        objSubContractorList.siteAdd.selectDateTime().click()
        var date = new Date().getDate()
        cy.log("date =" + date)
        objSubContractorList.common.divAvailableDate().contains(date)
        cy.selectDay("today")
        var hour = new Date().getHours()
        var minutes = new Date().getMinutes()
        var minutes2 = String(new Date().getMinutes()).padStart(2, "0")
        cy.log("minutes =" + minutes2)
        var hourPast = hour - 1
        cy.wait(1000)
        objSubContractorList.siteAdd.timeDisabled(hourPast, "1").scrollIntoView().contains(hourPast)
        objSubContractorList.siteAdd.hourAndMinutesSetStatus(hour, "1").scrollIntoView().click()
        if (minutes < 10) {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes2, "2").click()
        } else if (minutes > 23) {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes, "1").click()
        } else {
            objSubContractorList.siteAdd.hourAndMinutesSetStatus(minutes, "2").click()
        }
        objSubContractorList.siteAdd.btnOKStatus().click()
    })

    it("TC_37: Check edit Status from Active to Inactive " , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.selectAndSetStatus("Active", "Subcontractor's status has been updated")
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.selectAndSetStatus("Inactive", "Subcontractor's status has been updated")
    })

    it("TC_38: Check click confirm to cancel to set status\n" + 
    "TC_39: Check click cancel to cancel to set status" , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.iconSetStatus().scrollIntoView().click()
        objSubContractorList.siteList.rdBtnSetStatus("Inactive").click()
        objSubContractorList.siteAdd.cancelSetStatusSite("no", "2")
        objSubContractorList.siteAdd.cancelSetStatusSite("yes")
    })

    it("TC_40: Check click on a record on table list\n" + 
    "TC_116: Check the function of click a record on table list", () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.common.openDetailsView()
        cy.wait(2000)
        objSubContractorList.titleSubcontractorDetails().should("be.visible")
    })

    it("TC_41: Check the display when click on Import button\n" + 
    "TC_42: Check UI of import screen" , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.btnImport().click()
        cy.wait(2000)
        objSubContractorList.shouldUIPopupImportSubcontractor()
    })

    it("TC_46: Check when delete the import file \n" +
    "TC_66: Check when click on confirm to cancel of import \n" +
    "TC_67: Check when click on cancel to cancel of import \n" +
    "TC_68: Check showing poup confirm message when cancelling import file with some issues\n" +
    "TC_69: Check showing poup confirm message when cancelling import file with all issues\n" + 
    "TC_70: Check when importing the file which is not an excel file\n" + 
    "TC_71: Check when importing the file which is exceed 5MB\n" +
    "TC_72: Check when importing the file xls or xlsx which is not matched the template file" , () => {
        var filePath = "masterdataSite\\Template.xlsx"
        var filePath1 = "implement\\images.png"
        var filePath2 = "masterdataSite\\Template_not_matched.xlsx"
        objSubContractorList.btnImport().click()
        cy.wait(2000)
        objSubContractorList.shouldUIPopupImportSubcontractor()
        objSubContractorList.common.attachImportFile(filePath)
        cy.wait(2000)
        // Check Delete file 
        objSubContractorList.siteList.clickIconDelete()
        cy.wait(1000)
        objSubContractorList.siteList.fileNameAttach("Template.xlsx").should("not.exist")
        // Check Cancel Button
        objSubContractorList.common.attachImportFile(filePath)
        cy.wait(2000)
        objSubContractorList.common.cancelImportFile("no")
        objSubContractorList.common.cancelImportFile("yes")
        objSubContractorList.btnImport().click()
        objSubContractorList.common.importFileInvalidType(filePath1, "The selected file is not a support file type, please check your file.")
        objSubContractorList.common.importFileNotMatchedTemplate(filePath2, "The selected file is in the invalid format, please check your file.")
        objSubContractorList.common.attachImportFile("masterdataSite/5GBTemplate.xls")
        objSubContractorList.common.msgImportNotSuccess("File size is exceed the allowable limit of 5 MB").should("be.visible")
    })

    it("TC_114: Check the display when click on Import button\n" + 
    "TC_115: Check the function of click on Create button" , () => {
        cy.wait(2000)
        objSubContractorList.btnExport().click()
        cy.wait(2000)
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
        objSubContractorList.btnCreate().click()
        cy.wait(2000)
        objSubContractorList.lblTitle().should("be.visible")
    })

    it("TC_117: Check the default display of  View history log screen\n" , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.common.openDetailsView()
        objSubContractorList.siteList.btnHistoryLog().click()
        objSubContractorList.titleHistoryLog().should("be.visible")
    })

    it("TC_119: Check add the record will add a new row in view history log screen\n" , () => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.siteList.btnHistoryLog().click()
        objSubContractorList.titleHistoryLog().should("be.visible")
        objSubContractorList.searchByCondition("AutomationTesting")
        objSubContractorList.common.searchResultWithOutValue().should("be.visible")
        objSubContractorList.activeType("Create").should("be.visible")
    })

    it("TC_121: Check the pagination\n" + 
    "TC_122: Check the records when choosing number of records/page on table list\n" +
    "TC_123: Check the icon when creating 30 records/page\n" +
    "TC_124: Check the records when choosing number of click on next page on table list\n" +
    "TC_125: Check the icon when creating more than 30 records/page\n" +
    "TC_126: Check the icon when creating more than 30 records/page\n" +
    "TC_127: Check the icon when creating more than 60 records/page\n" +
    "TC_128: Check the function in Go to textbox when input over current pages\n" +
    "TC_129: Check the function in Go to textbox when input under current pages\n" +
    "TC_130: Check the function in Go to textbox when input current page" ,() => {
        objSubContractorList.titleSubcontractorList().should("be.visible")
        objSubContractorList.common.selectPageSize("30/page")
        // Move to Page from another page
        objSubContractorList.siteList.moveToPageWhenClickPageNumber("2", "2")
        // Go to Page Number
        objSubContractorList.common.goToPageNumber("3")
        // Input page exceed + Jump to last page
        objSubContractorList.siteList.ipPageExceed()
        objSubContractorList.common.btnJumpPageNext().should("be.disabled")
    })



})