import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class Track{

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }
}

export default Track;