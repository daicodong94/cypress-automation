import CreateSite from "../../../MasterData/components/site/CreateSite";
import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import DeliveryPlan from "../plan/DeliveryPlan";
import Schedule from "../schedule/Schedule";
import ShippingOrder from "./ShippingOrder";

class CreateShippingOrder {

    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.createSite = new CreateSite()
    }

    btnSave() {
        return cy.xpath('//button[@class="el-button common-button-style el-button--primary el-button--small"]//span[contains(text(),"Save")]')
    }

    ipField(placeholder) {
        return cy.xpath("//input[@placeholder='" + placeholder + "']")
    }

    ddlShipper() {
        return cy.xpath('//input[@placeholder="Please select the customer name"]')
    }

    ipUpStreamTrackingNumber() {
        return cy.xpath("//input[@placeholder='Please enter upstream tracking number']")
    }

    ddlSurchargeOrDiscount() {
        return cy.xpath("//input[@placeholder='Surcharge / Discount']")
    }

    ddlTemperutureControlStandard() {
        return cy.xpath('//input[@placeholder="Please enter temperature type"]')
    }

    ipTotalWeightKG() {
        return cy.xpath('//input[@placeholder="Please enter weight (kg)"]')
    }

    ddlContactNumberShipping() {
        return cy.xpath('(//input[@placeholder="Please enter contact phone number (required)"])[1]')
    }

    ddlSiteOrigin() {
        return cy.xpath("(//input[@placeholder='Please select site code/name'])[1]")
    }

    ddlDestinationInfor() {
        return cy.xpath("(//input[@placeholder='Please select site code/name'])[2]")
    }

    ddlSiteOrigin404() {
        return cy.xpath("//div[@data-vv-name='OriginContact Number']//input[@placeholder='Please enter contact number']")
    }

    ddlDestinationInfor404() {
        return cy.xpath("//div[@data-vv-name='DestinationContact Number']//input[@placeholder='Please enter contact number']")
    }

    ddlCityShipping() {
        return cy.xpath('(//input[@placeholder="Please select provinces, cities and counties (required)"])[1]')
    }

    ddlAddressShipping() {
        return cy.xpath('(//input[@placeholder="allow the input address selection (required)"])[1]')
    }

    ddlContactNumberReceiving() {
        return cy.xpath("(//input[@placeholder='Please enter contact phone number (required)'])[2]")
    }

    ddlCityReceiving() {
        return cy.xpath('(//input[@placeholder="Please select provinces, cities and counties (required)"])[2]')
    }

    ddlAddressReceiving() {
        return cy.xpath('(//input[@placeholder="allow the input address selection (required)"])[2]')
    }

    ipDeclaredValue() {
        return cy.xpath('//input[@placeholder="Please enter the statement value (required)"]')
    }

    ipTargetPickUpTime() {
        return cy.xpath("//div[@data-vv-name='OriginTarget Pick Up Date']//input[@placeholder='Please select the time']")
    }

    ipTargetDestinationTime() {
        return cy.xpath("//div[@data-vv-name='DestinationTarget Delivery Time']//input[@placeholder='Please select the time']")
    }

    txtSearchField() {
        return cy.xpath("(//div[@class='search-candidate w450']//input)[last()]")
    }

    spanSearchShipperResult() {
        return cy.xpath("//div[@class='search-candidate w450']//span[@class='search-highlight']")
    }

    lnkSurchargeOrDiscount(text) {
        return cy.xpath("//div[@class='el-tree-node__content']//a[text()='" + text + "']")
    }

    spanTemperature(temperature) {
        return cy.xpath("//div[@class='el-scrollbar']//li[@class='el-select-dropdown__item']//span[text()='" + temperature + "']")
    }

    txtSearchLocation() {
        return cy.xpath("(//div[@class='search-candidate']//div[@class='search-cadidate-keyword'])[last()]//input")
    }

    spanSearchLocationResult() {
        return cy.xpath("(//div[@class='search-candidate-list'])[last()]//div[@class='station-info']//span[@class='search-highlight']")
    }

    ipSelectDate() {
        return cy.xpath("(//div[@class='el-input el-input--small']//input[@placeholder='Select date'])[last()]")
    }

    btnNow() {
        return cy.xpath('//span[contains(text(),"Now")]')
    }

    btnOK() {
        return cy.xpath("(//button[text()='Ok'])[last()]")
    }

    chkCargoDetailCalculation() {
        return cy.xpath("//span[@class='el-checkbox__inner']")
    }

    lnkAddTo() {
        return cy.xpath("//button[@class='el-button pd0 el-button--text el-button--mini']")
    }

    ipItemTypeForCargo() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[1]//div[1]//input[1]")
    }

    ipItemNameForCargo() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[2]//div[1]//input[1]")
    }

    ipPackageLength() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[5]//div[1]//input[1]")
    }

    ipPackingWeightCargo() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[7]//div[1]//input[1]")
    }

    ipTotalPackagesForCargo() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[8]//div[1]//input[1]")
    }

    ipTotalQuantity() {
        return cy.xpath("//tbody//tr[contains(@class,'el-table__row')]//td[9]//div[1]//input[1]")
    }

    ipVolumePackage() {
        return cy.xpath('//input[@placeholder="Please enter Volume package cubic meter"]')
    }

    ipPackageWidth() {
        return cy.xpath('//input[@placeholder="Please enter Package Width m"]')
    }

    ipPackageHeight() {
        return cy.xpath('//input[@placeholder="Please enter Package Height m"]')
    }

    ipQuantity() {
        return cy.xpath("//input[@data-vv-name='goodsQty']")
    }

    btnDelete() {
        return cy.xpath("(//div//button/span//i[@class='el-icon-delete'])[1]")
    }

    spanCalculateResult(value) {
        return cy.xpath("//span[@aaac='entry-create']//span[contains(text(),'" + value + "')]//i")
    }

    cboItemType() {
        return cy.xpath("//input[contains(@placeholder,'Please enter the product type')]")
    }

    lblItemType(term) {
        return cy.xpath("//li//span[normalize-space()='" + term + "']")
    }

    clickShippingOrder() {
        this.lnkShippingOrder().click({ force: true })
    }

    spanRouteSearchResult() {
        return cy.xpath("(//div[@class='partner-info partner-info-fix']//span)[last()]")
    }

    selectBusinessUnit() {
        this.common.textField("Please enter business unit").click({ force: true })
        this.common.selectRandomValue()
    }

    selectItemType() {
        this.common.textField("Please select item type").click({ force: true })
        this.common.selectRandomValue()
    }

    chooseOtherInformation(route) {
        this.common.textField("Please enter destination type").click({ force: true })

        this.common.selectRandomValue()
        this.common.textField("Please enter route code").click({ force: true })

        this.txtSearchField().should("be.visible", { timeout: 10000 })
        this.txtSearchField().realType(route, { force: true })

        this.spanRouteSearchResult().should("be.visible", { timeout: 10000 })
        this.spanRouteSearchResult().click({ force: true })
    }

    chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product) {
        this.ddlShipper().click({ force: true })
        this.txtSearchField().should("be.visible", { timeout: 10000 }).realType(shipper)
        // cy.wait(1000)
        // this.txtSearchField().realType(shipper)
        // cy.wait(1000)
        this.spanSearchShipperResult().first().should('be.visible').click({ force: true })
        cy.wait(1000)
        this.ddlSurchargeOrDiscount().click({ force: true })
        cy.wait(1000)
        this.lnkSurchargeOrDiscount(product).click({ force: true })
    }

    inputNumberOfReceipts() {
        // cy.randomNumber(2).then((number) => {
        // if (10 < number < 100) {
        this.common.textField("Number of Receipts").type(6, { force: true })
        // } else {
        //     this.common.textField("Number of Receipts").type(2, { force: true })
        // }
        // })
    }

    chooseTemperature(temperature) {
        this.ddlTemperutureControlStandard().click({ force: true })
        cy.wait(300)
        this.spanTemperature(temperature).should("be.visible", { timeout: 10000 })
        cy.wait(300)
        this.spanTemperature(temperature).scrollIntoView().click({ force: true })
    }

    spanVehicleType10W() {
        return cy.xpath("(//ul[contains(@class,'el-scrollbar__view')])[last()]//li//span[starts-with(text(),'10')]")
    }

    btnAddAModel() {
        return cy.xpath("//button//span[contains(text(),'Add a model')]")
    }

    selectRandomRequestedVehicleType() {
        this.common.textField("Please select requested vehicle type").click({ force: true })
        this.btnAddAModel().click({ force: true })
        this.common.textField("please select a Vehicle Type").should("be.visible", { timeout: 5000 }).click({ force: true })
        cy.wait(300)
        this.spanVehicleType10W().its('length').then((len) => {
            this.spanVehicleType10W().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    inputTotalWeightKG() {
        cy.randomNumber(3).then((weight) => {
            // if (weight > 100) {
            this.ipTotalWeightKG().type(weight, { force: true })
            // } else {
            //     this.ipTotalWeightKG().type(2, { force: true })
            // }
        })
    }

    inputDeclaredValue() {
        cy.randomNumber(2).then((num) => {
            if (num > 1) {
                this.ipDeclaredValue().type(num, { force: true })
            } else {
                this.ipDeclaredValue().type(2, { force: true })
            }
        })
    }

    inputTotalVolume() {
        cy.randomNumber(2).then((num) => {
            if (num > 1) {
                this.common.textField("Please enter the volume (cubic meter)").type(num, { force: true })
            } else {
                this.common.textField("Please enter the volume (cubic meter)").type(2, { force: true })
            }
        })
    }

    inputTotalNumberOfItemPerPackages() {
        cy.randomNumber(2).then((num) => {
            if (num > 1) {
                this.common.textField("Please enter total number of item per packages").type(num, { force: true })
            } else {
                this.common.textField("Please enter total number of item per packages").type(2, { force: true })
            }
        })
    }

    inputTotalNumberOfPackages() {
        cy.randomNumber(2).then((num) => {
            if (num > 1) {
                this.common.textField("Please enter total number of package").type(num, { force: true })
            } else {
                this.common.textField("Please enter total number of package").type(2, { force: true })
            }
        })
    }

    inputRemarksForCargo() {
        cy.randomNumber(2).then((num) => {
            if (num > 10) {
                this.ipQuantity().type(num, { force: true })
            } else {
                this.ipQuantity().type(10, { force: true })
            }
        })
    }

    selectOriginInfor(site) {
        this.ddlSiteOrigin().click({ force: true })
        this.txtSearchLocation().should("be.visible", { timout: 10000 })
        this.txtSearchLocation().realType(site, { force: true })
        cy.wait(500)
        this.spanSearchLocationResult().should("be.visible", { timout: 10000 })
        cy.wait(500)
        this.spanSearchLocationResult().click({ force: true })
    }

    selectOriginInfor404(site) {
        this.ddlSiteOrigin404().click({ force: true })
        this.txtSearchLocation().should("be.visible", { timout: 10000 })
        this.txtSearchLocation().realType(site, { force: true })
        cy.wait(500)
        this.spanSearchLocationResult().should("be.visible", { timout: 10000 })
        cy.wait(500)
        this.spanSearchLocationResult().click({ force: true })
    }

    selectDeliveryInfor(site) {
        this.ddlDestinationInfor().click({ force: true })
        this.txtSearchLocation().should("be.visible", { timout: 10000 })
        this.txtSearchLocation().realType(site, { force: true })
        cy.wait(500)
        this.spanSearchLocationResult().should("be.visible", { timout: 10000 })
        cy.wait(500)
        this.spanSearchLocationResult().click({ force: true })
    }

    selectDeliveryInfor404(site) {
        this.ddlDestinationInfor404().click({ force: true })
        this.txtSearchLocation().should("be.visible", { timout: 10000 })
        this.txtSearchLocation().realType(site, { force: true })
        cy.wait(500)
        this.spanSearchLocationResult().should("be.visible", { timout: 10000 })
        cy.wait(500)
        this.spanSearchLocationResult().click({ force: true })
    }

    chooseContactNumberOnShipping(contact) {
        this.ddlContactNumberShipping().click({ force: true })
        cy.wait(1000)
        this.txtSearchLocation().type(contact, { force: true })
        cy.wait(1000)
        this.spanSearchLocationResult().click({ force: true })
    }

    chooseContactNumberOnReceiving(contact) {
        this.ddlContactNumberReceiving().click({ force: true })
        cy.wait(1000)
        this.txtSearchLocation().type(contact, { force: true })
        cy.wait(1000)
        this.spanSearchLocationResult().click({ force: true })
    }

    startAndEndTime(time) {
        return cy.xpath('//div[@class="relative w-full"]//div[contains(text(),"' + time + '")]')
    }

    divEndTime() {
        return cy.xpath("(//div[@class='relative w-full'])[last()]//div[@class='py-2 text-primary-85']")
    }

    chooseTargetPickUpTime() {
        this.ipTargetPickUpTime().click({ force: true })
        cy.wait(1000)
        cy.selectDay("today")
        this.btnOK().click({ force: true })
        this.ipTargetPickUpTime().click({ force: true })
        // var hour = String(new Date().getHours()).padStart(2, "0")
        // var minutes = String(new Date().getMinutes()).padStart(2, "0")
        // cy.log("minutes =" + minutes)
        // this.startAndEndTime("Start time").last().click()
        // this.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        // if (minutes > 23) {
        //     this.createSite.selectHourAndMinutes(minutes, "1").scrollIntoView().click()
        // } else if (minutes < 23) {
        //     this.createSite.selectHourAndMinutes(minutes, "2").scrollIntoView().click()
        // }
        // this.createSite.btnOK().click({ force: true })
        // // Select End Time
        // this.startAndEndTime("End time").last().click()
        this.divEndTime().click({ force: true })
        this.createSite.selectHourAndMinutes("23", "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("59", "1").scrollIntoView().click()
        this.createSite.btnOK().click({ force: true })
        // this.btnNow().last().click({force: true})
        this.btnOK().click({ force: true })
    }

    chooseTargetPickUpTimeNow() {
        this.ipTargetPickUpTime().click({ force: true })
        cy.wait(1000)
        this.btnNow().last().click({ force: true })
    }

    inputTargetPickupTime(days) {
        this.ipTargetPickUpTime().click({ force: true })
        cy.wait(1000)
        var currentDate = new Date();
        var expectedDate = currentDate.getDate() + days
        // cy.addDays(currentDate, days).then((futureDate) => {
        // this.ipSelectDate().type(futureDate, { force: true })
        // this.ipSelectDate().type("{enter}")
        cy.selectDay(expectedDate)
        // })
        // cy.wait(1000)
        // this.btnOK().click({ force: true })
        this.btnOK().click({ force: true })
        this.ipTargetPickUpTime().click({ force: true })
        // var hour = String(new Date().getHours()).padStart(2, "0")
        // var minutes = String(new Date().getMinutes()).padStart(2, "0")
        // cy.log("minutes =" + minutes)
        // this.startAndEndTime("Start time").click()
        // this.createSite.selectHourAndMinutes(hour, "1").scrollIntoView().click()
        // if (minutes > 23) {
        //     this.createSite.selectHourAndMinutes(minutes, "1").scrollIntoView().click()
        // } else if (minutes < 23) {
        //     this.createSite.selectHourAndMinutes(minutes, "2").scrollIntoView().click()
        // }
        // this.createSite.btnOK().click({ force: true })
        // Select End Time
        // this.startAndEndTime("End time").last().click()
        this.divEndTime().click({ force: true })
        this.createSite.selectHourAndMinutes("23", "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("59", "1").scrollIntoView().click()
        this.createSite.btnOK().click({ force: true })
        this.btnOK().click({ force: true })
    }

    chooseTargetDeliveryTime(days) {
        this.ipTargetDestinationTime().click({ force: true })
        cy.wait(1000)
        var currentDate = new Date();
        cy.addDays(currentDate, days).then((futureDate) => {
            this.ipSelectDate().type(futureDate, { force: true })
            this.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        // this.btnOK().click({ force: true })
        // this.ipTargetDestinationTime().click({ force: true })
        // Select Start Time
        this.startAndEndTime("Start Time").last().click({ force: true })
        this.createSite.selectHourAndMinutes("00", "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("00", "2").scrollIntoView().click()
        this.createSite.btnOK().click({ force: true })
        // Select End Time
        this.startAndEndTime("End Time").last().click()
        this.createSite.selectHourAndMinutes("23", "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("59", "1").scrollIntoView().click()
        this.createSite.btnOK().click({ force: true })
        this.btnOK().click({ force: true })
    }

    chooseTargetDeliveryTime404(days) {
        this.ipTargetDestinationTime().click({ force: true })
        cy.wait(1000)
        var currentDate = new Date();
        cy.addDays(currentDate, days).then((futureDate) => {
            this.ipSelectDate().type(futureDate, { force: true })
            this.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.btnOK().click({ force: true })
    }

    clickBtnReturn() {
        this.header.btnAtHeader("return").click({ force: true })
    }

    clickButtonSave() {
        // this.btnSave().click({ force: true })
        this.header.btnAtHeader("Save").click({ force: true })
        return new ShippingOrder()
    }

    clickBtnConfirm() {
        this.header.btnAtHeader("return").click({ force: true })
    }

    clickBtnDirectSchedule() {
        this.header.btnAtHeader("Direct Schedule").click({ force: true })
        return new Schedule()
    }

    clickLnkValueAddedServices() {
        this.header.btnAtHeader("value-added services").click({ force: true })
    }

    checkChkCargoDetailCalculation() {
        this.chkCargoDetailCalculation().click({ force: true })
    }

    clickLnkAddTo() {
        this.lnkAddTo().click({ force: true })
    }

    inputItemTypeForCargo(description) {
        this.ipItemTypeForCargo().type(description, { force: true })
    }

    inputPackageLengthForCargo(num) {
        this.ipPackageLength().type(num, { force: true })
    }

    inputPackingWeight(unit) {
        this.ipPackingWeightCargo().type(unit, { force: true })
    }

    inputTotalPackagesForCargo(weight) {
        this.ipTotalPackagesForCargo().type(weight, { force: true })
    }

    inputTotalQuantityForCargo(quantity) {
        this.ipTotalQuantity().type(quantity, { force: true })
    }

    deleteCargo() {
        this.btnDelete().click({ force: true })
    }

    ddlPackagingUnitForCargo() {
        return cy.xpath('//input[@placeholder="Please enter "]')
    }

    lblPackagingUnitForCargo(unit) {
        return cy.xpath("//li//span[normalize-space()='" + unit + "']")
    }

    lstStation() {
        return cy.xpath("//div[@class='station-info']//span")
    }

    inputCargoDetailCalculation(num, weight, quantity) {
        this.checkChkCargoDetailCalculation()
        this.clickLnkAddTo()
        cy.wait(1000)
        this.inputItemTypeForCargo("IceCream")
        this.ipItemNameForCargo().type("Ice")
        this.inputPackageLengthForCargo(num)
        // this.ipVolumePackage().type("100", {force: true})
        this.ipPackageWidth().type("10", { force: true })
        this.ipPackageHeight().type("10", { force: true })
        this.ddlPackagingUnitForCargo().click({ force: true })
        cy.wait(1000)
        this.lblPackagingUnitForCargo("Volume/003").click()
        this.inputPackingWeight("10")
        this.inputTotalPackagesForCargo(weight)
        this.inputTotalQuantityForCargo(quantity)
    }

    createNewShippingOrder(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, days) {
        this.leftMenu.navigateToPage("Shipping order")
        this.header.btnAtHeader("create").should("be.visible").click({ force: true })
        cy.wait(1000)
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        // this.inputNumberOfReceipts()
        this.inputTotalNumberOfPackages()
        cy.wait(500)
        // this.inputDeclaredValue()
        this.inputTotalWeightKG()
        this.inputTotalNumberOfItemPerPackages()
        this.inputTotalVolume()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.chooseTargetPickUpTime()
        // this.inputDeliveryTimeforShipping(0)
        this.selectBusinessUnit()
        this.selectItemType()
        this.chooseTargetDeliveryTime(days)
        cy.wait(500)
        this.chooseTemperature(temperature)
        this.chooseOtherInformation("981486")
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    //select requested vehicle type
    createNewShippingOrderWithRequestedVehicleType(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, days) {
        // this.leftMenu.navigateToPage("Shipping order")
        // this.header.btnAtHeader("create").should("be.visible").click({ force: true })
        // cy.wait(3000 / 2)
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        this.selectRandomRequestedVehicleType()
        this.header.btnAtHeader("Confirm").last().click({ force: true })
        this.inputTotalNumberOfPackages()
        cy.wait(500)
        this.inputTotalWeightKG()
        this.inputTotalNumberOfItemPerPackages()
        this.inputTotalVolume()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.chooseTargetPickUpTime()
        // this.inputDeliveryTimeforShipping(0)
        this.selectBusinessUnit()
        this.selectItemType()
        this.chooseTargetDeliveryTime(days)
        cy.wait(500)
        this.chooseTemperature(temperature)
        this.chooseOtherInformation("981486")
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").first().click({ force: true })
            return new ShippingOrder()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    //default pickup time is Now
    createNewShippingOrderPickupNow(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, days) {
        this.leftMenu.navigateToPage("Shipping order")
        this.header.btnAtHeader("create").should("be.visible").click({ force: true })
        cy.wait(1000)
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        // this.inputNumberOfReceipts()
        this.inputTotalNumberOfPackages()
        cy.wait(500)
        // this.inputDeclaredValue()
        this.inputTotalWeightKG()
        this.inputTotalNumberOfItemPerPackages()
        this.inputTotalVolume()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.chooseTargetPickUpTimeNow()
        // this.inputDeliveryTimeforShipping(0)
        this.selectBusinessUnit()
        this.selectItemType()
        this.chooseTargetDeliveryTime(days)
        cy.wait(500)
        this.chooseTemperature(temperature)
        this.chooseOtherInformation("981486")
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    createNewShippingOrder404(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, days) {
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        // this.inputNumberOfReceipts()
        cy.wait(500)
        // this.inputDeclaredValue()
        this.inputTotalWeightKG()
        // this.ipDeclaredValue().type(25, { force: true })
        this.inputTotalNumberOfItemPerPackages()
        this.inputTotalNumberOfPackages()
        this.inputTotalVolume()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.chooseTargetPickUpTime()
        this.chooseTargetDeliveryTime(days)
        cy.wait(500)
        this.selectItemType()
        cy.wait(500)
        this.selectBusinessUnit()
        this.chooseTemperature(temperature)
        this.chooseOtherInformation("981486")
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    //default delivery time is Now
    createNewShippingOrderWithCargoDetailForQuantitySplit(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, days, num, weight, quantity) {
        this.leftMenu.navigateToPage("Shipping order")
        this.header.btnAtHeader("create").should("be.visible").click({ force: true })
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        // this.ipField("shipment number").type(trackNumber)
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        // this.inputNumberOfReceipts()
        cy.wait(1000)
        this.inputCargoDetailCalculation(num, weight, quantity)
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.chooseTargetPickUpTime()
        this.selectBusinessUnit()
        this.selectItemType()
        this.chooseTemperature(temperature)
        this.chooseTargetDeliveryTime(days)
        cy.wait(500)
        this.chooseOtherInformation("981486")
        cy.wait(1000)
        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new DeliveryPlan()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    //user manually input delivery time
    createNewShippingOrderVer2(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, deliveryTime, receiptTime) {
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("Please enter external tracking number").type(trackNumber, { force: true })
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        this.inputTotalNumberOfPackages()
        cy.wait(500)
        this.inputTotalWeightKG()
        // this.inputDeclaredValue()
        this.inputTotalNumberOfItemPerPackages()
        this.inputTotalVolume()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        // this.chooseTargetPickUpTime()
        cy.wait(500)
        // this.selectBusinessUnit()
        this.selectItemType()
        this.inputTargetPickupTime(deliveryTime)
        this.chooseTargetDeliveryTime(receiptTime)
        cy.wait(500)
        this.selectBusinessUnit()
        this.chooseOtherInformation("981486")
        cy.wait(1000)

        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new DeliveryPlan()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    //user manually input delivery time + input quantity cargo information
    createNewShippingOrderVer3(button, trackNumber, shipper, product, temperature, shippingSite, receivingSite, deliveryTime, receiptTime) {
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("external tracking number").type(trackNumber, { force: true })
        // this.ipField("shipment number").type(trackNumber)
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        this.inputNumberOfReceipts()
        cy.wait(500)
        this.inputTotalWeightKG()
        // this.inputDeclaredValue()
        this.inputTotalNumberOfItemPerPackages()
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.inputTargetPickupTime(deliveryTime)
        this.chooseTargetDeliveryTime(receiptTime)
        cy.wait(500)
        this.selectBusinessUnit()
        this.chooseOtherInformation("981486")
        cy.wait(1000)

        if (button == "Save") {
            this.header.btnAtHeader("Save").click({ force: true })
            return new ShippingOrder()
        } else if (button == "Confirm") {
            this.header.btnAtHeader("Confirm").click({ force: true })
            return new DeliveryPlan()
        } else if (button == "Direct Schedule") {
            this.header.btnAtHeader("Direct Schedule").click({ force: true })
            return new Schedule()
        }
        cy.wait(1000)
    }

    inputWeightText(weight) {
        this.ipTotalWeightKG().type(weight, { force: true })
    }

    inputQuantityText(quantity) {
        this.ipQuantity().type(quantity, { force: true })
    }

    createNewShippingOrderVer4(trackNumber, shipper, product, temperature, weight, quantity, shippingSite, receivingSite, deliveryTime, receiptTime) {
        this.ipUpStreamTrackingNumber().type(trackNumber, { force: true })
        this.ipField("external tracking number").type(trackNumber, { force: true })
        // this.ipField("shipment number").type(trackNumber)
        this.chooseValueFromDropdownCustomerNameAndSurcharge(shipper, product)
        this.inputNumberOfReceipts()
        cy.wait(500)
        this.inputWeightText(weight)
        cy.wait(500)
        this.inputOuterPackagesText(quantity)
        cy.wait(500)
        this.selectOriginInfor(shippingSite)
        this.selectDeliveryInfor(receivingSite)
        cy.wait(500)
        this.inputTargetPickupTime(deliveryTime)
        this.chooseTargetDeliveryTime(receiptTime)
        cy.wait(500)
        this.chooseOtherInformation("981486")
        this.chooseTemperature(temperature)
        cy.wait(500)
        this.header.btnAtHeader("Direct Schedule").click({ force: true })
    }

    returnToShippingOrderPage() {
        this.header.btnAtHeader("return").click({ force: true })
        cy.wait(2000)
        this.header.btnAtHeader("create").should("be.visible")
        return new ShippingOrder()
    }

    checkItemType(item) {
        this.cboItemType().click({ force: true })
        cy.wait(2000)
        this.lblItemType(item).scrollIntoView()
        cy.wait(2000)
        this.lblItemType(item).should('be.visible')
    }
}

export default CreateShippingOrder;