import Driver from "../../../support/pages/MasterData/components/driver/Driver"
import VehicleAdd from "../../../support/pages/MasterData/components/vehicle/VehicleAdd"
import VehicleList from "../../../support/pages/MasterData/components/vehicle/VehicleList"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import ServiceItem from "../../../support/pages/TMS/components/our/ServiceItem"
import ServiceProduct from "../../../support/pages/TMS/components/our/ServiceProduct"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"

Cypress._.times(500, () => {
    describe("Flow to complete order in TMS When Confirm Order directly", () => {
        var objLogin, objHome = new Homepage(),
            objVehicleList = new VehicleList(), objVehicleAdd = new VehicleAdd(),
            objDriver = new Driver(), objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
            objServiceItem = new ServiceItem(), objServiceProduct = new ServiceProduct()

        var vehicleNameTH = "EtoEVehicleTH"
        var filePath = "implement/images.png", driverName = "METER"

        before("Login TMS systems", () => {
            cy.visit("https://tms-pt.allnowgroup.com")
            objLogin = new LoginSupplier()
            objHome = objLogin.loginWithUserandPassword("auto@gmail.com", "Vmo@1234")
        })

        // it("TC: Check create Shipping Order - Save", () => {
        //     objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        //     cy.randomString(21).then((trackingNum) => {
        //         objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
        //         objShippingOrder = objCreateShippingOrder.createNewShippingOrder("save", trackingNum, "Automation Company", "运输项目", "Phamar", "TestT1", "TestT2", 20);
        //         objShippingOrder.common.msgSuccess().should("be.visible")
        //         // trackNumber = trackingNum
        //     })
        // })

        it("test", () => {
            // siteA	TestT1 sitecode: 85962
            // siteB	TestT2 sitecode: 5174066350
            // subcontractor	EtoESubcontractorTH09583620
            objDriver = objHome.leftMenu.navigateToSubPage("Master Data", "Driver")
            cy.randomString(10).then((name) => {
                driverName += name
                objDriver = objDriver.createNewDriverEtoE(filePath, driverName, "EtoESubcontractorTH09583620", "Save", "empty", "empty", "success")
                cy.wait(2000)
                cy.reload()
                objVehicleList = objDriver.leftMenu.navigateToSubPage("Master Data", "Vehicle")
                objVehicleAdd = objVehicleList.goToCreateVehiclePage()

                cy.randomNumber(15).then((randomVehicleName) => {
                    vehicleNameTH += randomVehicleName
                    objVehicleList = objVehicleAdd.createVehicle("TestT1", "EtoESubcontractorTH09583620", driverName, "yes", "Save", "empty", "empty", "success")
                })
            })
        })

    //     it.only("", () => {
    //         objServiceItem = objHome.leftMenu.navigateToSubPage("Master Data", "Service Item")
    //         // objServiceItem = objVehicleList.leftMenu.navigateToSubPageWithoutMenu("Service Item")
    //         cy.randomString(5).then((serviceNameA) => {
    //             const serviceItemCodeA = "Item " + Math.random().toString(30).substring(2, 4) + Math.floor(Math.random() * 1000)
    //             objServiceItem.addNewItem(serviceNameA + "_item_auto", "place an order", "carrier", "any", serviceItemCodeA, "base")
    //             objServiceItem.common.msgSuccess().should("be.visible")
    //             objServiceItem.searchItem(serviceNameA + "_item_auto")
    //             cy.wait(1000)
    //             objServiceItem.enableServiceItem()

    //             //    7.2 Add New Service ItemB with Status = Enable
    //             cy.reload()
    //             cy.randomString(5).then((serviceNameB) => {
    //                 const serviceItemCodeB = "Item " + Math.random().toString(30).substring(2, 4) + Math.floor(Math.random() * 1000)
    //                 objServiceItem.addNewItem(serviceNameB + "_item_auto", "place an order", "carrier", "any", serviceItemCodeB, "base")
    //                 objServiceItem.common.msgSuccess().should("be.visible")
    //                 objServiceItem.searchItem(serviceNameB + "_item_auto")
    //                 cy.wait(1000)
    //                 objServiceItem.enableServiceItem()

    //                 cy.reload()
    //                 // 8. Go to Master Data > Service Product  
    //                 objServiceProduct = objServiceItem.leftMenu.navigateToSubPage("Master Data", "Service Product")
    //                 // objServiceProduct = objServiceItem.leftMenu.navigateToSubPageWithoutMenu("Service Product")
    //                 //   8.1 Add New Service ProductA, atrribute = Customer  with Status = Enable
    //                 cy.randomString(5).then((namePDA) => {
    //                     const serviceProductCodeA = "Prod " + Math.random().toString(30).substring(2, 4) + Math.floor(Math.random() * 1000)
    //                     cy.randomString(3).then((shortNamePDA) => {
    //                         objServiceProduct.enableProd(namePDA + "prod_auto", serviceProductCodeA, "customer", shortNamePDA, 50)
    //                         objServiceProduct.verifyOperateProd(namePDA, "pause")
    //                         //Go to Detail Service ProductA > Add ServiceItemA for this Service ProductA
    //                         objServiceProduct.addServiceEntryToProdV2(serviceNameA + "_item_auto")
    //                         cy.wait(4000)

    //                         cy.reload()
    //                         //   8.2 Add New Service ProductB, atrribute = Subcontractor with Status = Enable 
    //                         cy.randomString(5).then((namePDB) => {
    //                             const serviceProductCodeB = "Prod " + Math.random().toString(30).substring(2, 4) + Math.floor(Math.random() * 1000)
    //                             cy.randomString(3).then((shortNamePDB) => {
    //                                 objServiceProduct.enableProd(namePDB + "prod_auto", serviceProductCodeB, "subcontractor", shortNamePDB, 21)
    //                                 objServiceProduct.verifyOperateProd(namePDB, "pause")
    //                                 //Go to Detail Service ProductB > Add ServiceItemB for this Service ProductB
    //                                 objServiceProduct.addServiceEntryToProdV2(serviceNameA + "_item_auto")
    //                             })
    //                         })
    //                     })
    //                 })
    //             })
    //         })
    //     })
    })
})


