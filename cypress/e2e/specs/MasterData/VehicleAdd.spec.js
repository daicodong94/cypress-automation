import CommonMasterData from "../../../support/pages/MasterData/components/common/CommonMasterData"
import Driver from "../../../support/pages/MasterData/components/driver/Driver"
import Vehicle from "../../../support/pages/MasterData/components/vehicle/Vehicle"
import VehicleAdd from "../../../support/pages/MasterData/components/vehicle/VehicleAdd"

describe("MasterData - Vehicle - Add", () => {
    var data, expectedResult, actualResult
    var objVehicleAdd = new VehicleAdd(), objVehicle = new Vehicle(), objDriverAdd = new Driver()

    beforeEach(function () {
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            objVehicle.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Vehicle")
            // objVehicle.leftMenu.navigateToSubPage("Vehicle")
        })
    })

    it("TC_7_Check data from 'Vehicle type' \n" +
        "TC_10_Check data from 'Service group' \n" +
        "TC_13_Check data from 'Work type' \n" +
        "TC_16_Check data from 'Insurance' \n" +
        "TC_22_Check select 'other' data from drop-down lists of Vehicle brand field \n" +
        "TC_23_Input 50 characters on the text box after user select 'other' data from drop-down list of Vehicle brand field \n" +
        "TC_26_Check data from 'Vehicle Model Year' \n" +
        "TC_29_Check data from 'Fuel Type' \n" +
        "TC_42_Check data from 'Provincial Sign' field \n" +
        "TC_45_Check data from 'Plate color' field", () => {
            cy.fixture("Vehicle.json").then(function (listData) {
                data = listData
                objVehicleAdd = objVehicle.goToCreateVehiclePage()
                objVehicleAdd.verifyDefaultUI()
                //TC_7_Check data from 'Vehicle type'
                objVehicleAdd.checkDataServiceGroup("Select Vehicle Type", data.vehicleList)
                //TC_10_Check data from 'Service group'
                objVehicleAdd.checkDataServiceGroup("Select Service Group", data.serviceList)
                //TC_13_Check data from "Work type" 
                objVehicleAdd.checkDataServiceGroup("Select Work Type", data.workTypeList)
                //TC_16	Check data from 'Insurance'
                objVehicleAdd.checkDataServiceGroup("Select Insurance", data.insuranceList)
                //TC_20_Check data from 'Vehicle brand'
                objVehicleAdd.checkDataServiceGroup("Select Vehicle Brand", data.vehicleBrandList)
                //TC_22_Check select 'other' data from drop-down lists of Vehicle brand field
                objVehicleAdd.common.selectDataByClick("Select Vehicle Brand", "อื่นๆ")
                objVehicleAdd.common.ipSelectBox("Enter additional detail").should("be.visible")
                //TC_23_Input 50 characters on the text box after user select 'other' data from drop-down list of Vehicle brand field
                objVehicleAdd.common.inputDataToField("Enter additional detail", data.fiftyCharacters)
                //TC_26_Check data from 'Vehicle Model Year'
                objVehicleAdd.checkDataServiceGroup("Select Vehicle Model Year", objVehicleAdd.getListVehicleModelYear())
                //TC_29_Check data from 'Fuel Type'
                objVehicleAdd.checkDataServiceGroup("Select Fuel Type", data.listFuelType)
                //////TC_42_Check data from 'Provincial Sign' field
                //////FAIL: objAddVehicle.checkDataServiceGroup("Select Provincal Sign", data.listProvincalSign)
                //TC_45_Check data from 'Plate color' field
                objVehicleAdd.checkDataServiceGroup("Select Plate Color", data.listPlateColor)
            })
        })

    it("TC_1_Check display default screen of Information \n" +
        "TC_4_Check display all select fields \n" +
        "TC_6_Check display 'Drivers' screen \n" +
        "TC_8_Check select one data from 'Vehicle type' \n" +
        "TC_9_Leave blank 'Vehicle type' field \n" +
        "TC_11_Check select one data from 'Service group' \n" +
        "TC_12_Leave blank 'Service group' field \n" +
        "TC_14_Check select one data from 'Work type' \n" +
        "TC_15_Leave blank 'Work type' field \n" +
        "TC_17_Check select one data from 'Insurance' \n" +
        "TC_19_Leave blank 'Insurance' field \n" +
        "TC_21_Check select one data from 'Vehicle brand' \n" +
        "TC_25_Leave blank 'Vehicle brand' field \n" +
        "TC_28_Leave blank 'Vehicle Model Year' field \n" +
        "TC_30_Check select one data from 'Fuel Type' \n" +
        "TC_31_Leave blank 'Fuel Type' field \n" +
        "TC_44_Leave blank 'Provincial Sign' field \n" +
        "TC_46_Check select one data from 'Plate color' field \n" +
        "TC_47_Leave blank 'Plate color' field \n" +
        "TC_52_Leave blank 'Registered Date' field \n" +
        "TC_61_Leave blank 'Vehicle Width' field \n" +
        "TC_70_Leave blank 'Vehicle Length' field \n" +
        "TC_79_Leave blank 'Vehicle Height' field \n" +
        "TC_97_Leave blank 'Issue Date' field \n" +
        "TC_104_Leave blank 'Expire Date' field", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //TC_1_Check display default screen of Information
            objVehicleAdd.verifyDefaultUI()

            //TC_9_Leave blank "Vehicle type" field
            //TC_12_Leave blank 'Service group' field
            //TC_15_Leave blank 'Work type' field
            //TC_19_Leave blank 'Insurance' field
            //TC_25_Leave blank 'Vehicle brand' field
            //TC_28_Leave blank 'Vehicle Model Year' field
            //TC_31_Leave blank 'Fuel Type' field
            //TC_44_Leave blank 'Provincial Sign' field
            //TC_47_Leave blank 'Plate color' field
            //TC_52_Leave blank 'Registered Date' field
            //TC_61_Leave blank 'Vehicle Width' field
            //TC_70_Leave blank 'Vehicle Length' field
            //TC_79_Leave blank 'Vehicle Height' field
            //TC_88_Leave blank 'Vehicle Max Height' field
            //TC_97_Leave blank 'Issue Date' field
            //TC_104_Leave blank 'Expire Date' field
            objVehicleAdd.checkDataDefault()
            objVehicleAdd.btnConfirm("Save").should("have.attr", "disabled", "disabled")


            //TC_8_Check select one data from 'Vehicle type'
            objVehicleAdd.common.selectDataByClick("Select Vehicle Type", "10 ล้อตู้แห้ง7.5 ม. ประตู 10 บาน")
            //TC_11_Check select one data from 'Service group' 
            objVehicleAdd.common.selectDataByClick("Select Service Group", "Frozen")
            //TC_14_Check select one data from 'Work type' 
            objVehicleAdd.common.selectDataByClick("Select Work Type", "Main Fleet")
            //TC_17_Check select one data from 'Insurance'
            objVehicleAdd.common.selectDataByClick("Select Insurance", "ประกันภัยรถยนต์ชั้น 2")
            //TC_21_Check select one data from 'Vehicle brand'
            objVehicleAdd.common.selectDataByClick("Select Vehicle Brand", "เดวา")
            //TC_27_Check select one data from 'Vehicle Model Year'
            objVehicleAdd.common.selectDataByClick("Select Vehicle Model Year", "2022")
            //TC_30_Check select one data from 'Fuel Type'
            objVehicleAdd.common.selectDataByClick("Select Fuel Type", "ซูเปอร์เพาเวอร์ GSH95")
            //TC_43_Check select one data from 'Provincial Sign' field
            objVehicleAdd.common.selectDataByClick("Select Provincal Sign", "สุราษฎร์ธานี")
            //TC_46_Check select one data from 'Plate color' field
            objVehicleAdd.common.selectDataByClick("Select Plate Color", "ขาว")


            //TC_4_Check display all select fields
            objVehicleAdd.verifySelectFields()
            //TC_6_Check display 'Drivers' screen 
            objVehicleAdd.verifyDefaultUIDriversTab()
        })

    it("TC_32_Check input correct format 1 of License Plate number on 'License Plate number' \n" +
        "TC_33_Check input correct format 2 of license plate number on 'License Plate number' \n" +
        "TC_34_Check input license plate number start by number 0 \n" +
        "TC_35_Check input license plate number different list follow (Ex: ข้) \n" +
        "TC_36_Check input license plate number inlude language different Thai language \n" +
        "TC_37_Check input license plate number include special characters \n" +
        "TC_39_Check input 10 characters on 'License plate number' field", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            objVehicleAdd.verifyDefaultUI()
            objVehicleAdd.checkDataDefault()
            //TC_32_Check input correct format 1 of License Plate number on 'License Plate number'
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "1กข1234")
            //TC_33_Check input correct format 2 of license plate number on 'License Plate number'
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "80-1234")
            //TC_34_Check input license plate number start by number 0 
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "0กข1234")
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "00-1234")
            //TC_35_Check input license plate number different list follow (Ex: ข้)
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "1ข้ข1234")
            //TC_36_Check input license plate number inlude language different Thai language 
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "1abc1234")
            //TC_37_Check input license plate number include special characters
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "1@#$1234")
            //TC_39_Check input 10 characters on 'License plate number' field
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "12345678900")
            objVehicleAdd.inputTextToField("Enter Plate Number (ex. กข1234)", "1ฒท-7378ฒท1")

        })

    it("TC_48_Check fomat from 'Registered Date' field \n" +
        "TC_53_Input decimal number minimum: 01.01 on 'Vehicle Width' field \n" +
        "TC_54_Input decimal number maximum: 99.99 on 'Vehicle Width' field \n" +
        "TC_55_Input integer number minimum: 1 on 'Vehicle Width' field \n" +
        "TC_56_Input integer number minimum: 99 on 'Vehicle Width' field \n" +
        "TC_57_Input integer number minimum: 0/00.00 on 'Vehicle Width' field \n" +
        "TC_58_Input number: 100.1 on 'Vehicle Width' field \n" +
        "TC_59_Input number over 2 decimal: 55.555 on 'Vehicle Width' field \n" +
        "TC_60_Input speacial characters/letters @#@$%$/abc 'Vehicle Width' field \n" +
        "TC_62_Input number minimum: 01.01 on 'Vehicle Length' field \n" +
        "TC_63_Input number minimum: 99.99 on 'Vehicle Length' field \n" +
        "TC_64_Input integer number minimum: 1 on 'Vehicle Length' field \n" +
        "TC_65_Input integer number maximum: 99 on 'Vehicle Length' field \n" +
        "TC_66_Input number: 0/00.00 on 'Vehicle Length' field \n" +
        "TC_67_Input number: 100.1 on 'Vehicle Length' field \n" +
        "TC_68_Input number over 2 decimal: 55.555 on 'Vehicle Length' field \n" +
        "TC_69_Input speacial characters/letters @#@$%$/abc 'Vehicle Length' field", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //TC_48_Check fomat from 'Registered Date' field
            objVehicleAdd.verifyRegisteredDatePickerDisplaying()
            //VEHICLE WIDTH(M)
            //TC_53_Input decimal number minimum: 01.01 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "01.01")
            //TC_54_Input decimal number maximum: 99.99 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "99.99")
            //TC_55_Input integer number minimum: 1 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            //TC_56_Input integer number minimum: 99 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "99")
            //TC_57_Input integer number minimum: 0/00.00 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "0")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "00.00")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            //TC_58_Input number: 100.1 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "100.1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Width max is 99.99")
            //TC_59_Input number over 2 decimal: 55.555 on 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "55.555")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Width")
            //TC_60_Input speacial characters/letters @#@$%$/abc 'Vehicle Width' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "@#@$%$")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Width")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Width", "abc")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Width")

            //VEHICLE LENGTH (M)
            //TC_62_Input number minimum: 01.01 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "01.01")
            //TC_63_Input number minimum: 99.99 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "99.99")
            //TC_64_Input integer number minimum: 1 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            //TC_65_Input integer number maximum: 99 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "99")
            //TC_66_Input number: 0/00.00 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "0")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "00.00")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            //TC_67_Input number: 100.1 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "100.1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Length max is 99.99")
            //TC_68_Input number over 2 decimal: 55.555 on 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "55.555")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Length")
            //TC_69_Input speacial characters/letters @#@$%$/abc 'Vehicle Length' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "@#@$%$")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Length")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Length", "abc")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Length")
        })

    it("TC_71_Input number minimum: 01.01 on 'Vehicle Height' field \n" +
        "TC_72_Input number minimum: 99.99 on 'Vehicle Height' field \n" +
        "TC_73_Input integer number minimum: 1 on 'Vehicle Height' field \n" +
        "TC_74_Input integer number maximum: 99 on 'Vehicle Height' field \n" +
        "TC_75_Input number: 0/00.00 on 'Vehicle Height' field \n" +
        "TC_76_Input number: 100.1 on 'Vehicle Height' field \n" +
        "TC_77_Input number over 2 decimal: 55.555 on 'Vehicle Height' field \n" +
        "TC_78_Input speacial characters/letters @#@$%$/abc 'Vehicle Height' field \n" +
        "TC_80_Input number minimum: 01.01 on 'Vehicle Max Weight' field \n" +
        "TC_81_Input number minimum: 50.50 on 'Vehicle Max Weight' field \n" +
        "TC_82_Input integer number minimum: 1 on 'Vehicle Max Weight' field \n" +
        "TC_83_Input integer number maximum: 50 on 'Vehicle Max Weight' field \n" +
        "TC_84_Input number: 0/00.00 on 'Vehicle Max Weight' field \n" +
        "TC_85_Input number: 50.51 on 'Vehicle Max Weight' field \n" +
        "TC_86_Input number over 2 decimal: 50.499 on 'Vehicle Max Weight' field \n" +
        "TC_87_Input speacial characters/letters @#@$%$/abc 'Vehicle Max Weight' field", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //VEHICLE HEIGHT(M)
            //TC_71_Input number minimum: 01.01 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "01.01")
            //TC_72_Input number maximum: 99.99 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "99.99")
            //TC_73_Input integer number minimum: 1 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            //TC_74_Input integer number maximum: 99 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "99")
            //TC_75_Input integer number minimum: 0/00.00 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "0")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "00.00")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            //TC_76_Input number: 100.1 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "100.1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Height max is 99.99")
            //TC_77_Input number over 2 decimal: 55.555 on 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "55.555")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Height")
            //TC_78_Input speacial characters/letters @#@$%$/abc 'Vehicle Height' field
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "@#@$%$")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Height")
            objVehicleAdd.common.inputDataToField("Enter Vehicle Height", "abc")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Height")

            //VEHICLE MAX WEIGHT
            //TC_80_Input number minimum: 01.01 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "01.01")
            //TC_81_Input number maximum: 50.50 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "50.50")
            //TC_82_Input integer number minimum: 1 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "1")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            //TC_83_Input integer number maximum: 99 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "50")
            //TC_84_Input integer number minimum: 0/00.00 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "0")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "00.00")
            objVehicleAdd.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            //TC_85_Input number: 50.51 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "50.51")
            //TC_86_Input number over 2 decimal: 55.499 on 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "50.499")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
            //TC_87_Input speacial characters/letters @#@$%$/abc 'Vehicle Max Weight' field
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "@#@$%$")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
            objVehicleAdd.common.inputDataToField("Enter Max Weight", "abc")
            objVehicleAdd.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
        })

    it("TC_93_Check fomat from 'Issue Date' field \n" +
        "TC_98_Check fomat from 'Expire Date' field \n" +
        "TC_105_Check display default status \n" +
        "TC_108_Check tick on Inactive tickbox for 'Status' \n" +
        "TC_109_Check click on 'Select date/time' field", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //TC_105_Check display default status
            objVehicleAdd.checkDataDefault()
            //TC_93_Check fomat from 'Issue Date' field
            objVehicleAdd.verifyIssuedDateField("Select Issued Date")
            //TC_98_Check fomat from 'Expire Date' field
            objVehicleAdd.verifyIssuedDateField("Select Expired Date")
            //TC_106_Check redirect to status pop up 
            // fail: 2022/14/10    17:07 != 2022/14/10 17:07
            objVehicleAdd.common.openSetStatusPopUp()
            // objAddVehicle.verifyDefaultStatusPopUp()
            //TC_109_Check click on 'Select date/time' field
            objVehicleAdd.checkRadioEndDateTime()
            //TC_108_Check tick on Inactive tickbox for 'Status'
            objVehicleAdd.checkRadioStatusInActive()
        })

    it("TC_116_Check tick on Select date / time tickbox for 'End date / time' \n" +
        "TC_117_Check click on 'End date / time' field \n" +
        "TC_134_Check amount of Site displaying in the one page on the 'Assign Site' screen \n" +
        "TC_135_Check input number of page into 'Go to' field when system has many page on the 'Assign Site' screen \n" +
        "TC_140_Check select one site for vehicle when system have site data \n" +
        "TC_150_Check delete assigned site for vehicle\n" +
        "TC_151_Check reassign site for vehicle", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //TC_116_Check tick on Select date / time tickbox for 'End date / time'
            objVehicleAdd.common.openSetStatusPopUp()
            objVehicleAdd.checkRadioEndDateTime()
            objVehicleAdd.common.closeSetStatusPopUp()
            //TC_117_Check click on 'End date / time' field
            objVehicleAdd.common.openSetStatusPopUp()
            objVehicleAdd.verifySelectDateTimePickerDisplay()
            objVehicleAdd.common.closeSetStatusPopUp()
            //TC_124_Check click on the "Cancel" button when changed some fields
            objVehicleAdd.common.openSetStatusPopUp()
            objVehicleAdd.verifyCancelSetStatus()
            objVehicleAdd.common.closeSetStatusPopUp()
            // //TC_127_Check click on the "Cancel" button when don't have any change
            objVehicleAdd.common.openSetStatusPopUp()
            objVehicleAdd.common.closeSetStatusPopUp()
            //// TC_132_Check click on '>' icon on the 'Assign Site' 
            //// objAddVehicle.openSitePopUp()
            //// objAddVehicle.verifyNavigateToLastPage()
            //// TC_133_Check click on "<" icon on the "Assign Site" screen
            //// objAddVehicle.verifyNavigateToFirstPage()
            //TC_134_Check amount of Site displaying in the one page on the "Assign Site" screen
            objVehicleAdd.openSitePopUp()
            objVehicleAdd.selectPageSize("Assign Site", "50/page")
            //TC_135_Check input number of page into "Go to" field when system has many page on the "Assign Site" screen
            objVehicleAdd.goToPageNumber(3)
            //TC_140_Check select one site for vehicle when system have site data
            objVehicleAdd.selectAndVerifySite("empty", "9163822708")
            //TC_151_Check reassign site for vehicle
            objVehicleAdd.selectAndVerifySite("Site", "2713088432")
            //TC_150_Check delete assigned site for vehicle
            objVehicleAdd.deleteAssignedSite()
            objVehicleAdd.buttonAssignShouldBe()

        })

    it("TC_142_Check results search when the system has data match with keywords including ' abcdef' \n" +
        "TC_143_Check results search when the system has data match with keywords including ' abc' \n" +
        "TC_144_Check results search when the system has data match with keywords including ' a'  \n" +
        "TC_145_Check results search when the system has data match with keywords including ' number' \n" +
        "TC_146_Check results when the system hasn't data match with keywords \n" +
        "TC_147_Check delete inputted keywords \n" +
        "TC_148_Check press 'Enter' from keyboard to search \n" +
        "TC_149_Check select site for vehicle when system has no site data", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            objVehicleAdd.openSitePopUp()
            //TC_142_Check results search when the system has data match with keywords including " abcdef" 
            objVehicleAdd.searchByCondition("Nonthaburi")
            objVehicleAdd.common.searchResultShouldContain("Nonthaburi")
            //TC_143_Check results search when the system has data match with keywords including " abc"  
            objVehicleAdd.searchByCondition(" Nontha")
            objVehicleAdd.common.searchResultShouldContain("Nontha")
            //TC_144_Check results search when the system has data match with keywords including " a"  
            objVehicleAdd.searchByCondition(" A")
            objVehicleAdd.common.searchResultShouldContain("A")
            //TC_145_Check results search when the system has data match with keywords including " number"
            objVehicleAdd.searchByCondition("45890")
            objVehicleAdd.common.searchResultShouldContain("45890")
            //TC_147_Check delete inputted keywords
            objVehicleAdd.clearSearchInputted()
            //TC_148_Check press "Enter" from keyboard to search
            objVehicleAdd.header.txtSearch().first().click({ force: true }).type("4788518803", { force: true })
            cy.wait(2000)
            objVehicleAdd.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleAdd.common.searchResultShouldContain("4788518803")
            //TC_146_Check results when the system hasn't data match with keywords
            objVehicleAdd.searchByCondition("###$$$$@@@@!!!")
            objVehicleAdd.common.divNoData().should("be.visible")
            //TC_149_Check select site for vehicle when system has no site data
            objVehicleAdd.clickButtonCreateNewSite()
        })

    it("TC_153_Check 'Assign Subcontractor' screen when system has no site data \n" +
        "TC_158_Check amount of Site displaying in the one page on the 'Assign Subcontractor' screen \n" +
        "TC_166_Check results search when the system has data match with keywords including ' abcdef' \n" +
        "TC_167_Check results search when the system has data match with keywords including ' abc' \n" +
        "TC_168_Check results search when the system has data match with keywords including ' a' \n" +
        "TC_169_Check results search when the system has data match with keywords including ' number' \n" +
        "TC_172_Check press 'Enter' from keyboard to search \n" +
        "TC_171_Check delete inputted keywords \n" +
        "TC_172_Check press 'Enter' from keyboard to search \n" +
        "TC_174_Check delete assigned subcontractor for vehicle \n" +
        "TC_170_Check results when the system hasn't data match with keywords \n" +
        "TC_173_Check select subcontractor for vehicle when system has no site data", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            objVehicleAdd.openSubcontractorPopUp()
            //TC_158_Check amount of Site displaying in the one page on the "Assign Subcontractor" screen
            objVehicleAdd.verifyTypeOfPageSize("Assign Subcontractor")
            objVehicleAdd.selectPageSize("Assign Subcontractor", "50/page")
            //TC_166_Check results search when the system has data match with keywords including " abcdef" 
            objVehicleAdd.searchByCondition(" ANL-S000000000000265")
            objVehicleAdd.common.searchResultShouldContain("ANL-S000000000000265")
            //TC_171_Check delete inputted keywords
            objVehicleAdd.clearSearchInputted()
            cy.wait(2000)
            objVehicleAdd.btnSearch().first().click({ force: true })
            objVehicleAdd.common.searchResultWithValue().should("be.visible")
            //TC_167_Check results search when the system has data match with keywords including " abc"  
            objVehicleAdd.searchByCondition(" S000000000000265")
            objVehicleAdd.common.searchResultShouldContain("S000000000000265")
            //TC_168_Check results search when the system has data match with keywords including " a"  
            objVehicleAdd.searchByCondition(" A")
            objVehicleAdd.common.searchResultShouldContain("A")
            //TC_172_Check press "Enter" from keyboard to search
            objVehicleAdd.header.txtSearch().clear({ force: true })
            objVehicleAdd.header.txtSearch().first().click({ force: true }).type("KF", { force: true })
            cy.wait(2000)
            objVehicleAdd.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleAdd.common.searchResultShouldContain("KF")
            //TC_169_Check results search when the system has data match with keywords including " number"
            objVehicleAdd.searchByCondition(" 265")
            objVehicleAdd.common.searchResultShouldContain("265")
            //TC_174_Check delete assigned subcontractor for vehicle
            objVehicleAdd.lnkSelect().first().click({ force: true })
            objVehicleAdd.verifyAfterDeleteSite()
            //TC_170_Check results when the system hasn't data match with keywords
            //TC_153_Check "Assign Subcontractor" screen when system has no site data 
            objVehicleAdd.openSubcontractorPopUp()
            objVehicleAdd.searchByCondition("@#$#%#%%")
            objVehicleAdd.verifyUIAssignContractorScreenNoData()
            //TC_173_Check select subcontractor for vehicle when system has no site data
            objVehicleAdd.clickButtonCreateNewSubcontractor()
        })

    it("TC_179_Check results search when the system has data match with keywords including ' abcdef' \n" +
        "TC_180_Check results search when the system has data match with keywords including ' abc' \n" +
        "TC_181_Check results search when the system has data match with keywords including ' a'  \n" +
        "TC_182_Check results search when the system has data match with keywords including ' number' \n" +
        "TC_183_Check results when the system hasn't data match with keywords \n" +
        "TC_184_Check delete inputted keywords \n" +
        "TC_185_Check press 'Enter' from keyboard to search \n" +
        "TC_187_To check 'Cancel' button on 'Assign driver' popup when there is no action \n" +
        "TC_188_To check 'Cancel' button on 'Assign driver' popup when some drivers are ticked", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            objVehicleAdd.verifyDefaultUIDriversTab()
            objVehicleAdd.clickAssignButton()
            //TC_179_Check results search when the system has data match with keywords including " abcdef" 
            objVehicleAdd.searchDriver(" Hunter")
            objVehicleAdd.common.searchResultShouldContain("Hunter")
            //TC_184_Check delete inputted keywords
            objVehicleAdd.header.txtSearch().last().click({ force: true })
            objVehicleAdd.icClearSearchInputted().click({ force: true })
            cy.wait(2000)
            objVehicleAdd.btnSearch().last().click({ force: true })
            objVehicleAdd.common.searchResultWithValue().should("be.visible")
            //TC_185_Check press "Enter" from keyboard to search
            objVehicleAdd.header.txtSearch().clear({ force: true })
            objVehicleAdd.header.txtSearch().last().click({ force: true }).type("D000672", { force: true })
            cy.wait(2000)
            objVehicleAdd.header.txtSearch().last().type('{enter}')
            cy.wait(1000)
            objVehicleAdd.common.searchResultShouldContain("D000672")
            //TC_180_Check results search when the system has data match with keywords including " abc"  
            objVehicleAdd.searchDriver(" nter")
            objVehicleAdd.common.searchResultShouldContain("nter")
            //TC_181_Check results search when the system has data match with keywords including " a"  
            objVehicleAdd.searchDriver(" H")
            objVehicleAdd.common.searchResultShouldContain("H")
            //TC_182_Check results search when the system has data match with keywords including " number"
            objVehicleAdd.searchDriver("000662")
            objVehicleAdd.common.searchResultShouldContain("000662")
            //TC_187_To check "Cancel" button on "Assign driver" popup when there is no action
            objVehicleAdd.common.icCloseDialog().click({ force: true })
            objVehicleAdd.clickAssignButton()
            objVehicleAdd.btnAssignDriver("Cancel").click({ force: true })
            objVehicleAdd.btnAction("Assign").should("be.visible")
            cy.wait(2000)
            //TC_188_To check "Cancel" button on "Assign driver" popup when some drivers are ticked
            objVehicleAdd.clickAssignButton()
            // objAddVehicle.searchDriver("000662")
            // objAddVehicle.chkSelectDriver().first().click({ force: true })
            cy.wait(2000)
            objVehicleAdd.selectAllData()
            cy.wait(1000)
            objVehicleAdd.btnAssignDriver("Cancel").click({ force: true })
            cy.wait(1000)
            objVehicleAdd.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")
            objVehicleAdd.common.btnConfirmHintPopupVer2("Cancel").click({ force: true })
            //TC_183_Check results when the system hasn't data match with keywords
            objVehicleAdd.clickAssignButton()
            objVehicleAdd.searchDriver("###$$$$@@@@!!!")
            objVehicleAdd.common.divNoData().should("be.visible")
        })

    it("TC_177_Check assign one driver for vehicle \n" +
        "TC_186_Check assign driver from result search \n" +
        "TC_193_Check amount of Site displaying in the one page on the 'Assign Driver' screen", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            objVehicleAdd.verifyDefaultUIDriversTab()
            objVehicleAdd.clickAssignButton()
            //TC_193_Check amount of Site displaying in the one page on the "Assign Driver" screen
            objVehicleAdd.verifyTypeOfPageSize("Assign Driver")
            objVehicleAdd.selectPageSize("Assign Driver", "50/page")

            //TC_186_Check assign driver from result search
            //TC_177_Check assign one driver for vehicle
            objVehicleAdd.searchDriver("ANL-D000662")
            objVehicleAdd.verifyAssignDriver()
            objVehicleAdd.occupiedOff().first().should("be.exist")
            //TC_201_Check switch toggle button to on for one driver
            objVehicleAdd.btnOccupied().first().click({ force: true })
            objVehicleAdd.occupiedOn().first().should("be.exist")
            //TC_204_Check switch toggle button to off for one driver
            objVehicleAdd.btnOccupied().first().click({ force: true })
            objVehicleAdd.occupiedOff().first().should("be.exist")
        })

    it("TC_210_Check click 'Save' button When the driver has not been occupied \n" +
        'TC_209_Check click "Save" button When the subcontractor has not been assigned \n' +
        "TC_210_Check click 'Save' button When the driver has not been occupied \n" +
        'TC_211_Check complete create vehicle : status of vehicle is active \n' +
        'TC_212_Check complete create vehicle : status of vehicle is inactive \n' +
        'TC_213_Check complete create vehicle when click "Confirm" button on "Confirm" popup \n' +
        'TC_214_Check complete create vehicle when click "Cancel" button on "Confirm" popup \n' +
        "TC_217_Check the vehicle's status when not assigned to any site \n" +
        "TC_218_Check the vehicle's status when not assigned to any subcontractor \n" +
        "TC_219_Check the vehicle's status when not assigned to any driver", () => {
            objVehicleAdd = objVehicle.goToCreateVehiclePage()
            //occupied err
            objVehicleAdd.createVehicle("empty", "ANL-S000000000000310", "ANL-D000672", "yes", "Save", "Confirm", "no")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            //TC_209_Check click "Save" button When the subcontractor has not been assigned
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "empty", "ANL-D000672", "yes", "Save", "Confirm", "no")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            //TC_210_Check click 'Save' button When the driver has not been occupied
            //occupied err
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "ANL-D000672", "no", "Save", "Confirm", "no")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            //TC_211_Check complete create vehicle : status of vehicle is active
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "ANL-D000672", "yes", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //TC_212_Check complete create vehicle : status of vehicle is inactive
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "ANL-D000672", "yes", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //TC_213_Check complete create vehicle when click "Confirm" button on "Confirm" popup
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "ANL-D000672", "yes", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //TC_214_Check complete create vehicle when click "Cancel" button on "Confirm" popup
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "ANL-D000672", "no", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //215,216
            //TC_217_Check the vehicle's status when not assigned to any site
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("empty", "ANL-S000000000000310", "ANL-D000672", "no", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //TC_218_Check the vehicle's status when not assigned to any subcontractor
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "empty", "ANL-D000672", "no", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Cancel", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
            //TC_219_Check the vehicle's status when not assigned to any driver
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("ANL-S000000000000834", "ANL-S000000000000310", "empty", "yes", "Save", "Confirm", "yes")
            objVehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
        })

    it.only('cr', () => {
        cy.randomString(10).then((firstName) => {
            objDriverAdd = objVehicle.leftMenu.navigateToSubPage("Driver")
            var filePath = "implement/images.png"
            const date = new Date()
            const day = date.getDate()
            objDriverAdd.commonMD.btnAction("Create").click()
            cy.randomNumber(13).then((nationalID) => {
                cy.randomNumber(8).then((licenseNum) => {
                    cy.randomNumber(9).then((mobilePhone) => {
                        cy.randomNumber(8).then((landLine) => {
                            objDriverAdd.vehicleAdd.selectSubcontractor("Subcontractor", "AutomationTest")
                            objDriverAdd.createNewDriverV1(filePath, 'นางสาว', firstName, 'AuToTest', 'ไม่ระบุ', day, firstName, 'AuToTest', mobilePhone, landLine, 'blank', nationalID, licenseNum, 'บ.2', 'ส่วนบุคคลตลอดชีพ')
                            objDriverAdd.commonMD.msgSuccessShouldHaveText("success")
                        })
                    })
                })
            })
            //need create driver for each vehicle
            objVehicle = objDriverAdd.leftMenu.navigateToSubPage("Vehicle")
            objVehicle.goToCreateVehiclePage()
            objVehicleAdd.createVehicle("SiteCode21", "ANL-S000000000000092", firstName, "yes", "Save", "Confirm", "yes")
            // objVehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
            objVehicleAdd.common.msgSuccessShouldHaveText("success")
        })

    })
})