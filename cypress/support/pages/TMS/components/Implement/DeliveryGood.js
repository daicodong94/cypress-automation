import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import CreateShippingOrder from "../shippingorder/CreateShippingOrder";
import Pickup from "./PickUp";


class DeliveryGood {
    constructor() {
        this.header = new Header()
        this.common = new Common()
        this.pickup = new Pickup()
        this.leftMenu = new LeftMenu()
        this.createShippingOrder = new CreateShippingOrder()
    }

    ipShippingInfor(label) {
        return cy.xpath("(//td[text()='" + label + "']//following-sibling::td//div//input)[1]")
    }

    inputShippingInforTime(label, days) {
        this.ipShippingInfor(label).click()
        cy.wait(1000)
        var date = new Date()
        cy.addDays(date, days).then((futureDate) => {
            this.createShippingOrder.ipSelectDate().type(futureDate)
            this.createShippingOrder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click()
    }

    inputShippingInfor(label, length) {
        // this.ipShippingInfor(label).click()
        // cy.wait(1000)
        // var date = new Date()
        // cy.addDays(date, days).then((futureDate) => {
        //     this.createShippingOrder.ipSelectDate().type(futureDate)
        //     this.createShippingOrder.ipSelectDate().type("{enter}")
        // })
        cy.randomNumber(length).then((number) => {
            this.ipShippingInfor(label).clear().type(number)
        })
        // cy.wait(1000)
        // this.common.btnOK().click()
    }

    ipReceiptTemperature() {
        return cy.xpath("//input[@data-vv-name='receivedGoodsTemperature']")
    }

    inputReceiptTemperature(temp) {
        this.ipReceiptTemperature().type(temp)
    }

    btnConfirmReturnShipping() {
        return cy.xpath("//div[@id='pane-0']//span[text()='confirm']")
    }

    tabDeliveryGood() {
        return cy.xpath('//li[@class="common-menu-item active"]//a[text()="deliver goods "]')
    }

    navigateDeliveryGoodPageSuccess() {
        this.tabDeliveryGood().should("be.visible")
    }

    msgBulkDelivery() {
        return cy.xpath('//p[@class="el-message__content"]')
    }

    dispatchOrdertrackingStatus(status) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//span[text()= "' + status + '"]')
    }

    deliveryCompletionTime() {
        return cy.xpath('(//td[@class="press"])[4]')
    }

    msgDeliveryCompletedSuccess() {
        return cy.xpath('//p[text()="done successfully"]')
    }

    statusOrderOfDriverReceipt() {
        return cy.xpath('(//table//tr[@class="el-table__row"]//td//div[text()= "wait"])[1]')
    }

    btnConfirmPopupReturnShipping() {
        return cy.xpath('(//button[@type="button"]//span[text()="confirm"])[1]')
    }

    titleMaptrackingAndTransportProcess(title) {
        return cy.xpath('//div[@class="el-dialog__header"]//span[text()="' + title + '"]')
    }

    createOrdertoDeliveryGood(trackingNum, carrier, name) {
        this.pickup.createOrderAndSchedule(trackingNum, carrier, name)
        cy.wait(50000)
        cy.reload()
        this.leftMenu.navigateToSubPage("implement", "Pick up ")
        this.common.searchByClickSuggestion("dispatch number", trackingNum)
        cy.wait(5000)
        this.common.openDetailView()
        cy.wait(2000)
        this.pickup.chooseCompletionTime(10)
        this.pickup.clickbtnFunctionPickup("pickup compeleted")
        cy.wait(2000)
        this.pickup.confirmPickup("yes")
        cy.wait(2000)
        this.pickup.btnconfirm().click()
        this.pickup.dispatchPickupStatus("shipped").should("be.visible")
        cy.reload()
        cy.wait(50000)
    }

    bulkDeliveryOrderConfirm(confirm, msg) {
        this.pickup.ckbChooseOrder("3").click()
        this.pickup.ckbChooseOrder("4").click()
        this.header.btnAtHeader("Bulk delivery").click()
        if (confirm == "yes") {
            this.common.btnConfirmHintPopUp("confirm").click()
            this.msgBulkDelivery().contains(msg)
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopUp("cancel").click()
            this.msgBulkDelivery().contains(msg)
        }
    }

    chooseTimeForDeliveryCompletionTime(days) {
        this.deliveryCompletionTime().click()
        cy.wait(1000)
        var date = new Date()
        cy.addDays(date, days).then((futuredate) => {
            this.pickup.createshippingorder.ipSelectDate().type(futuredate)
            this.pickup.createshippingorder.ipSelectDate().type("{enter}")
        })
        cy.wait(1000)
        this.common.btnOK().click()
    }

    deliveryCompleted() {
        this.common.openDetailView()
        this.chooseTimeForDeliveryCompletionTime(5)
        this.pickup.clickbtnFunctionPickup("Delivery completed")
        cy.wait(1000)
        this.pickup.confirmPickup("yes")
        cy.wait(1000)
        this.msgDeliveryCompletedSuccess().should("be.visible")
        this.pickup.btnconfirm().click()
    }

    deliverySave() {
        this.common.openDetailView()
        cy.wait(5000)
        this.chooseTimeForDeliveryCompletionTime(5)
        this.pickup.clickbtnFunctionPickup("save")
        cy.wait(1000)
        this.msgDeliveryCompletedSuccess().should("be.visible")
    }

    deliveryReject() {
        this.common.openDetailView()
        cy.wait(5000)
        this.chooseTimeForDeliveryCompletionTime(5)
        cy.wait(2000)
        this.pickup.clickbtnFunctionPickup("reject")
        cy.wait(2000)
        this.btnConfirmPopupReturnShipping().click()
        cy.wait(2000)
        this.pickup.confirmPickup("yes")
        cy.wait(1000)
        this.msgDeliveryCompletedSuccess().should("be.visible")
        cy.reload()
        cy.wait(10000)
    }

    mapTrackingAndTransportProcess(btnname, title) {
        this.common.openDetailView()
        cy.wait(2000)
        this.pickup.clickbtnFunctionPickup(btnname)
        cy.wait(2000)
        this.titleMaptrackingAndTransportProcess(title).should("be.visible")
    }

    deliveryWithOutOfRangeTeperature() {
        this.common.openDetailView()
        cy.wait(3000)
        this.inputReceiptTemperature("80")
        cy.wait(1000)
        this.chooseTimeForDeliveryCompletionTime(5)
        this.pickup.clickbtnFunctionPickup("Delivery completed")
        cy.wait(2000)
        this.pickup.confirmPickup("yes")
        cy.wait(2000)
        this.msgDeliveryCompletedSuccess().should("be.visible")
        this.pickup.btnconfirm().click()
        // cy.wait(5000)
        cy.reload()
    }

}
export default DeliveryGood;