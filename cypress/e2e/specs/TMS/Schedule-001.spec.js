import Login from "../../../support/pages/TMS/components/login/Login"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"

describe("Schedule01", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(), objShippingOrder = new ShippingOrder(),
        objCreateShippingOrder = new CreateShippingOrder(), objDeliveryPlan = new DeliveryPlan(),
        objSchedule = new Schedule()
    var trackNumber, carrier = "SYS_DEF_INDIVIDUAL", consignment = "Service for Carrier", driver = "Auto Driver",
        customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it.only("TC: Check search information by conditions", () => {
        cy.randomString(7).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, customer, product, temperature, siteA, "SiteTestAuto", 5);
            trackNumber = trackingNum
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(2000)
            objSchedule.common.openDetailView()
            objSchedule.header.searchResult(trackNumber).should("be.visible")
        })
    })

    it("TC: Check pagination \n" +
        "TC: Check changing page with inputting number directly in the textbox \n" +
        "TC: Check UI of Search component \n" +
        "TC: Reset column setting as the default \n" +
        "TC: Check column setting", () => {
            objSchedule = objHome.leftMenu.navigateToPage("Schedule")
            objSchedule.common.goToPageNumber(3)
            objSchedule.common.selectPageSize("200/page")
            //TC: Check UI of Search component
            objSchedule.common.hideAndUnHideSearch()
            //set column
            objSchedule.common.resetDefaultColumn("yes")
            objSchedule.common.setColumn("dispatch number", "status")
            objSchedule.common.enableOrDisableColumn("dispatch number", "no")
            objSchedule.common.resetDefaultColumn("yes")
        })

    it("TC: Check keep function with orders have status Not dispatched- blank Carrier field", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("save").click()
        objSchedule.common.msgError().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- no vehicle information", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.lnkAddVehicle().click()
        objSchedule.btnAction("save").click()
        objSchedule.common.msgWarning().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- blank Consignment Products/Items field", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.btnAction("save").click()
        objSchedule.common.msgSuccess().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- input valid data", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.lnkAddVehicle().click()
        objSchedule.selectDriver(driver)
        objSchedule.btnAction("save").click()
        objSchedule.common.msgSuccess().should("be.visible")
    })

    it("TC: Check Re-plan order unsuccessfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.rePlan("cancel")
    })

    it("TC: Check Re-plan order successfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.rePlan("confirm")
        objDeliveryPlan = objSchedule.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", trackNumber, "second")
    })

    it("TC: Check batch schedule unsuccessfully after clicking on Cancel button", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        cy.wait(2000)
        objSchedule.common.searchBySelectValue("status of dispatch order", "job_created")
        cy.wait(1000)
        objSchedule.batchSchedule(3, carrier, consignment, "cancel")
    })

    it("TC: Check batch schedule unsuccessfully when stay Carrier blank", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        cy.wait(2000)
        objSchedule.common.searchBySelectValue("status of dispatch order", "job_created")
        cy.wait(1000)
        objSchedule.batchSchedule(3, "blank", consignment, "confirm")
        objSchedule.common.msgWarning().should("be.visible")
    })

    it("TC: Check batch schedule unsuccessfully when stay Shipping product blank", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        cy.wait(2000)
        objSchedule.common.searchBySelectValue("status of dispatch order", "job_created")
        cy.wait(1000)
        objSchedule.batchSchedule(3, carrier, "blank", "confirm")
        objSchedule.common.msgWarning().should("be.visible")
    })

    it("TC: Check search information by conditions - No data returned", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        cy.wait(2000)
        objSchedule.header.btnAtHeader("more filter...").click()
        objSchedule.common.searchBySelectValue("source of dispatch order", "auto_plan")
        objSchedule.common.searchBySelectValue("item type", "Food")
        objSchedule.common.noData()
    })

    // after(() => {
    //     objLogout = new Logout()
    //     objLogout.logout()
    // })
})