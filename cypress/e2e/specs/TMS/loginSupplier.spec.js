import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
// import  commands from "../../support/commands.js";
import ResetNewPassword from "../../../support/pages/TMS/components/login/ResetNewPassword.js";


//Test suite
describe("Login newenhancement system TMS", () => {
    var data
    var objLogin = new LoginSupplier()
    var objHomePage
    var objResetNewPassword
    var OTPCode
    var objResetPassword = new ResetNewPassword()

    before(function () {
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
            data = logindata
        })
    })

    // Login system successfully
    it("TC_2 - Login Successfully", () => {
        cy.visit(data.urltms)
        objHomePage = objLogin.loginWithUserandPassword(data.usernametms, data.password)
        objLogin.shouldbetilteLoginSuccessfully()
        // objLogin.logout()
    })

    // Login unsuccessfully when input invalid email and password
    it("TC_4 - Login unsuccessfully", () => {
        cy.visit(data.urltms)
        objLogin.loginWithUserandPassword(data.usernameincorrect, data.passwordincorrect)
        cy.wait(5000)
        objLogin.shouldMessageLoginUnSuccessfully("Email or password is incorrect")

    })

    // Login unsuccessfully when input data invalid in email
    it("TC_5 - Login unsuccessfully", () => {
        cy.visit(data.urltms)
        objLogin.inputUsername("daicodong")
        cy.wait(1000)
        objLogin.msgLoginUnSuccessfully("Invalid email format").should('be.visible')
    })

    // Check Login Unsuccessfully when get blank to "Username" fields
    it("TC_6 - Check Login Unsuccessfully when get blank to Username fields", () => {
        cy.visit(data.urltms)
        objLogin.inputPassword(data.password)
        cy.wait(3000)
        // objLogin.msgLoginUnSuccessfully("Invalid email format").should('be.visible')
        objLogin.shouldbeBtnLoginDisable()
    })

    // Check Login Unsuccessfully when get blank to "Password" fields
    it("TC_7 - Check Login Unsuccessfully when get blank to Password fields", () => {
        cy.visit(data.urltms)
        objLogin.inputUsername(data.usernameincorrect)
        cy.wait(3000)
        objLogin.shouldbeBtnLoginDisable()
    })

    //Check Remember password function when check on "Remember me" checkbox
    it("TC_8 - Check Remember password function when check on Remember me checkbox", () => {
        objLogin.txtUsername().clear()
        objLogin.inputUsername(data.usernametms)
        objLogin.inputPassword(data.password)
        objLogin.clickckbRememberAccount()
        objLogin.clickbtnLogin()
        cy.wait(3000)
        objLogin.shouldbetilteLoginSuccessfully()
        cy.reload()
        cy.wait(3000)
        objLogin.shouldbetilteLoginSuccessfully()
        objLogin.logout()
    })
    
    // Check Remember password function after deleting cookies of web browser
    it("TC_9 - Check Remember password function after deleting cookies of web browser", () => {
        objLogin.loginWithUserandPassword(data.usernametms, data.password)
        objLogin.shouldbetilteLoginSuccessfully()
        cy.clearCookies()
        cy.visit(data.urltms)
        objLogin.titleLoginpage().should('be.visible')
    })
    
    // Check Forgot Password function when click on link "Forgot password"
    it("TC_10 - Check navigation when click on link Forget your password?", () => {
        cy.visit(data.urltms)
        objLogin.navigateForgetPasswordScreen()
        cy.wait(2000)
        objLogin.shouldbeTitleForgetPassowrd()
        objLogin.btnContinue().should('be.disabled')
    })

    // Check click link Back to Login
    it("TC_11 - Check Forgot Password function when click on link Back to login", () => {
        cy.visit(data.urltms)
        objLogin.navigateForgetPasswordScreen()
        cy.wait(2000)
        objLogin.clicklnkBacktologin()
        cy.wait(2000)
        objLogin.titleLoginpage().should('be.visible')
    })

    // Check navigation after inputing correct email in Frogot password screen
    it("TC_12 - Check navigation after inputing correct email in Frogot password screen", () => {
        cy.visit(data.urltms)
        objLogin.navigateSelectOTPScreen(data.usernametms)
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
        objLogin.btnContinue().should('be.disabled')
    })

    // Check navigation after click "Back to previous page" link
    it("TC_13 - Check navigation after click Back to previous page link", () => {
        cy.visit(data.urltms)
        objLogin.navigateSelectOTPScreen(data.usernametms)
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
        objLogin.clicklnkBacktoPreviouspage()
        cy.wait(2000)
        objLogin.shouldbeTitleForgetPassowrd()
    })

    // Check navigation after click "Back to log in" link
    it("TC_14 - Check navigation after click Back to log in link",() => {
        cy.visit(data.urltms)
        objLogin.navigateSelectOTPScreen(data.usernametms)
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
        objLogin.clicklnkBacktologin()
        cy.wait(1000)
        objLogin.titleLoginpage().should('be.visible')
    })

    // Check by inputing unregistered email in Forgot password screen
    it("TC_15 - Check by inputing unregistered email in Forgot password screen",() => {
        cy.visit(data.urltms)
        objLogin.navigateForgetPasswordScreen()
        cy.wait(2000)
        objLogin.inputUsername("daicodong@gmail.com")
        objLogin.clickbtnContinute()
        cy.wait(2000)
        // objLogin.shouldbeTitleForgetPassowrd()
        objLogin.btnContinue().should('be.disabled')
        objLogin.msgErrorForgetPassword("We do not found your email in our system").should('exist')
    })

    // Check by inputing wrong fromat email in Forgot password screen
    it("TC_16 - Check by inputing wrong fromat email in Forgot password screen", () => {
        cy.visit(data.urltms)
        objLogin.navigateForgetPasswordScreen()
        cy.wait(2000)
        objLogin.inputUsername("daicodong")
        objLogin.shouldbemsgErrorForgetPassword("Invalid email format")
    })

    // Check display when OTP code is expired
    it("TC_18 - Check display when OTP code is expired", () => {
        cy.visit(data.urltms)
        cy.wait(2000)
        objLogin.chooseOTPbyPhone(data.usernametmsOTP)
        cy.wait(30000)
        cy.wait(30000)
        objLogin.lnkResendOTP().should('be.visible')
        objLogin.lnkChangeRecoveryMethod().should('be.visible')
        objLogin.clickChangeRecoveryMethod()
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
    })

    // Check by inputing OTP code less than 6 digits
    it("TC_20 - Check by inputing invalid OTP code", () => {
        cy.visit(data.urltms)
        cy.wait(2000)
        objLogin.shouldbemsgInvalidOTPCodeWhenGetOTPbyPhone(data.usernametmsOTP, "12345")
    })

    // Check by inputing OTP code greater than 6 digits
    it("TC_21 - Check by inputing invalid OTP code", () => {
        objLogin.txtOTPCode().clear()
        objLogin.inputOTPCode("1234567")
        objLogin.msgInvalidOTPCode().should('be.visible')
    })

    // Check by inputing OTP code unexisting
    it("TC_22 - Check by inputing invalid OTP code", () => {
        objLogin.txtOTPCode().clear()
        objLogin.inputOTPCode("123456")
        objLogin.clickbtnVerify()
        cy.wait(2000)
        objLogin.msgInvalidOTPCode().should('exist')
    })

    // Check resending new OTP
    it("TC_24 - Check resending new OTP", () => {
        cy.visit(data.urltms)
        cy.wait(2000)
        objLogin.chooseOTPbyPhone(data.usernametmsOTP)
        cy.wait(30000)
        cy.wait(30000)
        objLogin.lnkResendOTP().should('be.visible')
        objLogin.lnkChangeRecoveryMethod().should('be.visible')
        objLogin.clickResendOTP()
        cy.wait(2000)
        objLogin.msgGetOTPAgain().should('exist')
    })
    
    // Check change recovery method
    it("TC_25 - Check change recovery method", () => {
        objLogin.clickChangeRecoveryMethod()
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
    })

    // Check by inputing OTP code less than 6 ditgits when choosen GET OTP by email
    it("TC_28 - Check by inputing OTP code less than 6 ditgits", () => {
        cy.visit(data.urltms)
        cy.wait(2000)
        objLogin.shouldbemsgInvalidOTPCodeWhenGetOTPbyEmail(data.usernametms, "12345")
    })

    // Check by inputing OTP code greater than 6 digits when choosen GET OTP by Email
    it("TC_29 - Check by inputing invalid OTP code greater than 6 ditgits", () => {
        objLogin.txtOTPCode().clear()
        objLogin.inputOTPCode("1234567")
        objLogin.msgInvalidOTPCode().should('be.visible')
    })

    // Check by inputing OTP code unexisting when choosen GET OTP by email
    it("TC_30 - Check by inputing invalid OTP code choose method GET OTP by email", () => {
        objLogin.txtOTPCode().clear()
        objLogin.inputOTPCode("123456")
        objLogin.clickbtnVerify()
        cy.wait(2000)
        objLogin.msgInvalidOTPCode().should('exist')
    })

    // Check the display of buttons in set password screen
    it("TC_32 - Check resending new OTP choose method GET OTP by email", () => {
        cy.visit(data.urltms)
        cy.wait(2000)
        objLogin.chooseOTPbyEmail(data.usernametms)
        cy.wait(30000)
        cy.wait(30000)
        objLogin.lnkResendOTP().should('be.visible')
        objLogin.lnkChangeRecoveryMethod().should('be.visible')
        objLogin.clickResendOTP()
        cy.wait(2000)
        objLogin.msgGetOTPAgain().should('exist')
    })
    
    // Check change recovery method
    it("TC_33 - Check change recovery method choose method GET OTP by email", () => {
        objLogin.clickChangeRecoveryMethod()
        cy.wait(2000)
        objLogin.shouldbetitleSelectOTPscreen()
    })

    // Change new passowrd successfully
    it("TC_34 - Check change into new password successfully", () => {
        cy.visit(data.urltms)
        objResetNewPassword = objLogin.mockingOtpAndverificationOtp(data.usernametms)
        cy.wait(2000)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.newpassword, data.newpassword)
        // Set password default
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.password, data.password)
        // objLogin.logout()
    })

    // Set new password equal maxlength
    it("TC_37 - Check by inputing maxlength in text box", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.passwordmaxlength, data.passwordmaxlength)
        // Set password default
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.password, data.password)
        // objLogin.logout()
    })

    // Set new password equal minlength
    it("TC_38 - Check by inputing minlength in text box", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.passwordminlength, data.passwordminlength)
        // Set password default
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.setNewPassword(data.password, data.password)
        // objLogin.logout()
    })

    // set new password less than 7 character
    it("TC_39 - Check by not inputing in valid range in text box", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordinvalidrange)
        objResetPassword.ipConfirmPassword(data.passwordinvalidrange)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
        cy.wait(2000)
        // objResetPassword.checkboxvalidationPassword().uncheck()
    })

    // set new password greater than 65 character
    it("TC_40 - Check by not inputing in valid range in text box", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordgreaterthanmaxlength)
        objResetPassword.ipConfirmPassword(data.passwordgreaterthanmaxlength)
        cy.wait(2000)
        objResetPassword.lblLengthPassword().contains('64 / 64')
    })

    // set new password lower case
    it("TC_41 - Check by inputing the invalid characters in text box", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordlowercase)
        objResetPassword.ipConfirmPassword(data.passwordlowercase)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password upper case
    it("TC_42 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passworduppercase)
        objResetPassword.ipConfirmPassword(data.passworduppercase)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password contains username
    it("TC_43 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordcontainsusername)
        objResetPassword.ipConfirmPassword(data.passwordcontainsusername)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password contains all upper case
    it("TC_44 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordalluppercase)
        objResetPassword.ipConfirmPassword(data.passwordalluppercase)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password contains all lower case
    it("TC_45 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordalllowercase)
        objResetPassword.ipConfirmPassword(data.passwordalllowercase)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password contains all number digit
    it("TC_46 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordalldigit)
        objResetPassword.ipConfirmPassword(data.passwordalldigit)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // set new password does not contains at least one character special
    it("TC_47 - Check Forgot Password validation when reset password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.passwordnonespecialcharacter)
        objResetPassword.ipConfirmPassword(data.passwordnonespecialcharacter)
        cy.wait(2000)
        objResetPassword.btnSetPasswordShouldBeDisabled()
    })

    // Input confirm password that does not match with new password
    it("TC_48 - Check input new confirmed password not the same as new password", () => {
        cy.visit(data.urltms)
        objLogin.mockingOtpAndverificationOtp(data.usernametms)
        objResetPassword.shouldBeTitleSetPasswordScreen()
        objResetPassword.ipNewPassword(data.newpassword)
        objResetPassword.ipConfirmPassword(data.passwordnotmatched)
        cy.wait(2000)
        objResetPassword.shouldBePasswordNotMatched()
    })

})
