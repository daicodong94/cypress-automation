import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import { Utility } from "../../../support/utility"

describe("Checking Our - Item type", () => {

    var objItemType, data, objLogin, objHome, objShippingOrder, objCreateShippingOrder

    beforeEach("TMS-Our-Itemtype", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            }   else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((loginData) => {
                    data = loginData
                    cy.visit(data.url)
                    objLogin = new Login()
                    objHome = objLogin.login(data.username, data.password)
                })
            }
        })
    })
    
    afterEach(()=>{
        objHome.logout()
    })

    it("TC_299 - Check add to add a product type", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.verifyNewItem(text, "Phamar")  
            objItemType.deleteItemType(text)
        })
    })

    it("TC_300 - Check delete success of  the type of item's", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.deleteItemType(text)
            objItemType.verifyItemJustHasBeenDeleted(text)
        })
    })

    it("TC_301 - Check disable success of  the type of item's", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.disabledItemType(text)
            objItemType.common.msgSuccess().should('be.visible')
            cy.wait(5000)
            objItemType.columnStatus().should('have.text', 'disabled')
        })
    })

    it("TC_302 - Check enable success of  the type of item's", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.enableItemType(text)
            objItemType.common.msgSuccess().should('be.visible')
            cy.wait(5000)
            objItemType.columnStatus().should('have.text', 'enabled')
        })
    })

    it("TC_303 - Check inquire value of the type of item's", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.verifyNewItem(text, "Phamar")  
            objItemType.deleteItemType(text)
        })
    })

    it("TC_304 - Check all required field when creating the new Item type", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            cy.wait(5000)
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.common.txtError().should('contain', 'The item type already exists')
            objItemType.btnCloseAddNewPopup().click() 
            objItemType.deleteItemType(text)
        cy.wait(2000)
        })
    })

    it("TC_305 - Check top button", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        objItemType.verifyClickButtonTopOperate()
    })

    it("TC_306 - Check bottom button", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        objItemType.verifyClickButtonBottomOperate()
    })

    it("TC_307 - Check edit information the type of item", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            cy.wait(5000)
            cy.randomString(5).then((key) => {
                objItemType.editItem(text, "Automation", key)
                objItemType.verifyEditItem("Automation", key)
                objItemType.deleteItemType(text)
            })
        })
    })

    it("TC_308 - Check all info of item show when creating the order", () => {
        objItemType = objHome.leftMenu.navigateToSubPage("Master Data","Item Type")
        cy.wait(2000)
        cy.randomString(10).then((text) => {
            objItemType.addNewProdType(text, "Phamar", text, text)
            objItemType.enableItemType(text)
            cy.wait(5000)
            cy.reload()
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objCreateShippingOrder.checkItemType(text)
        })
    })
})