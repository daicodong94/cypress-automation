import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";

class DeliveryPlan {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    btnSearchOrder(btnName) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[1]")
    }

    btnSearchDispatch(btnName) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[2]")
    }

    ipContactNumber() {
        return cy.xpath("//div[@class='station-info']/ancestor::div//input")
    }

    spanSearchResult() {
        return cy.xpath("//span[@class='search-highlight']")
    }

    columnShipmentOrder() {
        return cy.xpath("//td//div[contains(text(),'D00')]")
    }

    columnDispatchNumber() {
        return cy.xpath("//td//div[contains(text(),'R00')]")
    }

    trimString(trackNumber) {
        var myArr = trackNumber.split("")
        return myArr
    }

    searchByClickSuggestionAtDeliveryPlanPage(searchField, searchKey, place) {
        cy.wait(1000)
        var characters = this.trimString(searchKey)
        for (let index = 0; index < characters.length; index++) {
            this.header.ipSearchBox(searchField).last().focus().type(characters[index])
            cy.wait(200)
        }
        cy.wait(1000)
        this.header.spanSearchSuggestionDeliveryPlanPage(searchKey).should("be.visible", { timeout: 10000 })
        this.header.spanSearchSuggestionDeliveryPlanPage(searchKey).click({ force: true })
        cy.wait(1000)
        if (place == "first") {
            this.btnSearchOrder("inquire").click({ force: true })
        }
        if (place == "second") {
            this.btnSearchDispatch("inquire").click({ force: true })
        }
        cy.wait(1000)
    }

    createADispatchOrder(trackNumber) {
        this.header.searchResult(trackNumber).first().click({ force: true })
        this.header.btnAtHeader("create a dispatch order").click({ force: true })
        cy.wait(1000)
        cy.get("body").then((body) => {
            if (body.find("el-message-box").length > 0) {
                this.common.btnConfirmHintPopUp("Cancel").click({ force: true })
            }
        })
        this.common.msgSuccess().should("be.visible")
    }

    withdrawOrder(trackNumber) {
        this.header.searchResult(trackNumber).first().click({ force: true })
        this.header.btnAtHeader("withdraw").click({ force: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click({ force: true })
    }

    //collapse filter1
    hideSearch() {
        return cy.xpath("//div[@data-old-padding-top and @style='display: none;']")
    }

    unHideSearch() {
        return cy.xpath("//div[@data-old-padding-top and @style]")
    }
    hideAndUnHideSearch() {
        cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'more filter')])[1]").click({ force: true })
        this.hideSearch().should("not.exist")
        cy.wait(1000)
        cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'collapse filter')])[1]").click({ force: true })
        this.unHideSearch().should("exist")
    }

    //collapse filter2
    hideSearch2() {
        return cy.xpath("(//div[@data-old-padding-top and @style='display: none;'])[2]")
    }

    unHideSearch2() {
        return cy.xpath("(//div[@data-old-padding-top and @style])[2]")
    }

    hideAndUnHideSearch2() {
        cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'more filter')])[last()]").click({ force: true })
        this.unHideSearch2().should("be.visible")
        cy.wait(1000)
        cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'collapse filter')])[last()]").click({ force: true })
        this.hideSearch2().should("not.exist")
    }

    //set column
    btnAtHeader(btnName, position) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[" + position + "]")
    }

    divColumnToAdjust(value) {
        return cy.xpath("//div[@class='dragg-item' and text()='" + value + "']")
    }

    icCloseSetColumnDialog() {
        return cy.xpath("//div[@class='custom-table-title']//following-sibling::button")
    }

    btnSetColumnAction(button) {
        return cy.xpath("//div[@class='custom-table-title']//ancestor::div[@role='dialog']//div[@class='dialog-footer']/button/span[text()='" + button + "']")
    }

    btnConfirmActionSetColumn(button) {
        return cy.xpath("//p[contains(text(),'reset')]//following-sibling::div/button/span[contains(text(),'" + button + "')]")
    }

    divColumnName(column) {
        return cy.xpath("//thead[@class='has-gutter']//tr//th//div[text()='" + column + "']")
    }

    resetDefaultColumn(position) {
        this.btnAtHeader("set column", position).click({ force: true })
        cy.wait(1000)
        this.btnSetColumnAction("reset").click({ force: true })
        this.btnConfirmActionSetColumn("confirm").click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }

    setColumn(position, source, target) {
        this.btnAtHeader("set column", position).click({ force: true })
        cy.wait(1000)
        this.divColumnToAdjust(source).move(this.divColumnToAdjust(target))
        cy.wait(1000)
        this.btnSetColumnAction("confirm").click({ force: true })
        this.common.msgSuccess().should("be.exist")
    }

    enableOrDisableColumn(position, column, active) {
        this.btnAtHeader("set column", position).click({ force: true })
        cy.wait(1000)
        this.divColumnToAdjust(column).click({ force: true })
        cy.wait(1000)
        this.btnSetColumnAction("confirm").click({ force: true })
        this.common.msgSuccess().should("be.exist")
        if (active == "yes") {
            this.divColumnName(column).should("be.visible")
        } else {
            this.divColumnName(column).should("not.exist")
        }
    }

    //Page size
    ddlPageSize(position) {
        return cy.xpath("(//span[@class='el-pagination__sizes'])[" + position + "]")
    }

    spanPageSize(size) {
        return cy.xpath("(//span[text()='" + size + "'])[2]")
    }

    spanPageSizeActive(size) {
        return cy.xpath("(//li[@class='el-select-dropdown__item selected hover']//span[text()='" + size + "'])[" + position + "]")
    }

    selectPageSize(size, position) {
        this.ddlPageSize(position).click({ force: true })
        this.spanPageSize(size).click({ force: true })
        cy.wait(1000)
        this.spanPageSize(size, position).should("be.exist")
    }

    //Page number
    ipPageNumber(position) {
        return cy.xpath("(//input[@type='number'])[" + position + "]")
    }

    goToPageNumber(position, page) {
        this.ipPageNumber(position).type(page)
        this.ipPageNumber(position).type('{enter}')
        cy.wait(1000)
    }

    //Delete
    searchResultWithOutValueAt2ndTable() {
        return cy.xpath("//div[@class='splitpanes__pane pane-item-4']//table//tr[@class='el-table__row']")
    }

    deleteDispatch() {
        this.searchResultWithOutValueAt2ndTable().click({ force: true })
        this.header.btnAtHeader("delete").click({ force: true })
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click({ force: true })
    }

    //Confirm dispatch
    confirmDispatch() {
        this.searchResultWithOutValueAt2ndTable().click({ force: true })
        this.header.btnAtHeader("Confirm").click({ force: true })
    }

    //Add to
    searchResultWithOutValueAt1stTable() {
        return cy.xpath("(//table//tr[@class='el-table__row'])[1]")
    }

    addTo(trackNumber) {
        this.searchResultWithOutValueAt1stTable().click({ force: true })
        this.searchResultWithOutValueAt2ndTable().click({ force: true })
        this.header.btnAtHeader("add to").click({ force: true })
        this.common.msgSuccess().should("be.visible")
        this.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
        this.header.searchResult(trackNumber).should("be.visible")
    }

    selectContactPerson(number) {
        this.ipContactNumber().last().type(number)
        cy.wait(1000)
        this.spanSearchResult().last().click({ force: true })
    }

    searchDispatchOrder(searchField, searchKey) {
        this.header.ipSearchBox(searchField).click({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox(searchField).focus().type(searchKey)
        this.header.ipSearchBox(searchField).type("{enter}")
        cy.wait(1000)
        this.btnSearchDispatch("inquire").click({ force: true })
        cy.wait(1000)
    }

    originalWeight() {
        return cy.xpath("//td[@class='title' and text()='Total Weight (kg)']//parent::tr//td[@class='press table-cell-readonly']")
    }

    splittedWeight() {
        return cy.xpath("//td[@class='title' and text()='Total Weight (kg)']//parent::tr//td[@class='press']//input")
    }

    caculateWeightSplit() {
        var intNumber
        this.originalWeight().then(($label) => {
            var strNumber = $label.text().trim()
            // cy.log("text is : " + strNumber)
            intNumber = parseInt(strNumber)
            if (intNumber % 2 != 0) {
                intNumber += 1
                // cy.log("number is1 " + intNumber)
            }
        })
        // cy.log("number is " + intNumber)
        return intNumber / 2
    }

    btnSaveSplit() {
        return cy.xpath("//div[@class='el-dialog__wrapper dialog-common-style plan-break']//button//span")
    }

    quantitySplit(searchField, searchKey, place) {
        this.searchByClickSuggestionAtDeliveryPlanPage(searchField, searchKey, place)
        cy.wait(1000)
        this.header.searchResult(searchKey).first().dblclick({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("quantity split").click({ force: true })

        this.originalWeight().then(($label) => {
            var strNumber = $label.text().trim()
            var intNumber = parseInt(strNumber)
            if (intNumber % 2 != 0) {
                intNumber += 1
            }
            intNumber /= 2
            cy.setText(this.splittedWeight(), intNumber)
            cy.wait(1000)
            this.btnSaveSplit().click({ force: true })
            this.common.msgSuccess().should("be.visible")
        })
    }

    routeSplit(searchField, searchKey, place, site) {
        this.searchByClickSuggestionAtDeliveryPlanPage(searchField, searchKey, place)
        cy.wait(1000)
        this.header.searchResult(searchKey).first().dblclick({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("route split").click({ force: true })
        this.header.ipSearchBox("Please select a site code/name").click({ force: true })
        cy.wait(1000)
        this.common.txtSearchLocation().click({ force: true }).type(site)
        cy.wait(1000)
        this.common.spanSearchLocationResult().click({ force: true })
        cy.wait(1000)
        this.common.btnDialogFooter("Save").first().click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }
}

export default DeliveryPlan;