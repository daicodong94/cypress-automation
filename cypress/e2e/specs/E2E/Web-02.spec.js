import E2E from "../../../support/pages/E2E/E2E"
import CustomerAdd from "../../../support/pages/MasterData/components/customer/CustomerAdd"
import CustomerList from "../../../support/pages/MasterData/components/customer/CustomerList"
import Driver from "../../../support/pages/MasterData/components/driver/Driver"
import CreateSite from "../../../support/pages/MasterData/components/site/CreateSite"
import SiteList from "../../../support/pages/MasterData/components/site/SiteList"
import SubcontractorAdd from "../../../support/pages/MasterData/components/subcontractor/SubcontractorAdd"
import SubcontractorList from "../../../support/pages/MasterData/components/subcontractor/SubcontractorList"
import VehicleAdd from "../../../support/pages/MasterData/components/vehicle/VehicleAdd"
import VehicleList from "../../../support/pages/MasterData/components/vehicle/VehicleList"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import Login from "../../../support/pages/TMS/components/login/Login"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import ServiceItem from "../../../support/pages/TMS/components/our/ServiceItem"
import ServiceProduct from "../../../support/pages/TMS/components/our/ServiceProduct"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"


describe("Flow to complete order in TMS When Confirm Order directly", () => {
    var data, objLogin, objHome = new Homepage(), objLoginPRD,
        objCustomerList = new CustomerList(), objCustomerAdd = new CustomerAdd(),
        objSubcontractorList = new SubcontractorList(), objSubcontractorAdd = new SubcontractorAdd(),
        objVehicleList = new VehicleList(), objVehicleAdd = new VehicleAdd(),
        objSiteList = new SiteList(), objCreateSite = new CreateSite(), objDriver = new Driver(),
        objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objDeliveryPlan = new DeliveryPlan(), objSchedule = new Schedule(),
        objServiceItem = new ServiceItem(), objServiceProduct = new ServiceProduct()
    var customerNameTH = "CustomerTH", subNameTH = "SubconTH"
    var objE2E = new E2E(), siteName = "SiteTest"
    var filePath = "implement/images.png", driverName = "Driver"
    var trackNumber, temperature = "Chill", url = new Utility().getBaseUrl()

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            cy.visit(url)
            objLogin = new LoginSupplier()
            objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            objCustomerList = objHome.leftMenu.navigateToSubPage("Master Data", "Customer")
            objCustomerList.pageTitleShouldBe("Customer")
        })
    })

    // before("Login TMS systems", () => {
    //     cy.visit("https://supplier-pt.allnowgroup.com")
    //     objLogin = new LoginSupplier()
    //     objHome = objLogin.loginWithUserandPassword("auto@gmail.com", "Vmo@1234")
    //     objCustomerList = objHome.leftMenu.navigateToSubPage("Master Data", "Customer")
    //     objCustomerList.pageTitleShouldBe("Customer")
    // })

    it('WEB-02: Flow to complete order in TMS when saving Order', () => {
        cy.randomNumber(10).then((randomCustomerName) => {
            customerNameTH += randomCustomerName
            //1. Login TMS > Go to Master Data> Customer > Create New CustomerA with Status = Active 
            objCustomerAdd = objCustomerList.clickCreateButton()
            objCustomerList = objCustomerAdd.createCustomer(customerNameTH, customerNameTH, "empty", "empty", "Save", "empty", "empty", "success")
            //2. Go to Master Data > Subcontractor > Create New SubcontractorA with Status = Active
            objSubcontractorList = objCustomerList.leftMenu.navigateToSubPage("Master Data", "Subcontractor")
            objSubcontractorList.header.pageTitleShouldBe("Subcontractor")
            objSubcontractorAdd = objSubcontractorList.clickCreateSubcontractor()

            cy.randomNumber(11).then((randomSubcontractorName) => {
                subNameTH += randomSubcontractorName
                objSubcontractorList = objSubcontractorAdd.createSubcontractor(subNameTH, "generate", "empty", "empty", "Save", "empty", "empty", "success")
                cy.reload()
                objSiteList = objCustomerList.leftMenu.navigateToSubPage("Master Data", "Site")
                objSiteList.header.pageTitleShouldBe("Site")
                objCreateSite = objSiteList.clickCreateSite()
                //3.Go to Master Data > Site > Create New SiteA with Status = Active
                cy.randomNumber(9).then((number) => {
                    siteName += number
                    objSiteList = objCreateSite.createNewSiteEtoE(customerNameTH, number, siteName, "Store", "Compact HypermarketU", "นาย", "Trinh", "Jared", "AutoTest", "ThaiLand", "นครราชสีมา")
                    objDriver = objSiteList.leftMenu.navigateToSubPageDriver2("Master Data")
                    objDriver.headerMD.pageTitleShouldBe("Driver")
                    //4. Go to Master Data > Driver > Create New DriverA with Status = Active
                    cy.randomString(10).then((name) => {
                        driverName += name
                        var mobilePhone = number
                        var month = String(new Date().getMonth() + 1).padStart(2, "0")
                        var password = "2000" + month + "12"
                        cy.log("password:" + password)
                        cy.log("account:" + mobilePhone)
                        objDriver = objDriver.createNewDriverEtoE(driverName, subNameTH, mobilePhone, "Save", "empty", "empty", "success")
                        //  4.1 Assign SubcontractorA for this DriverA
                        //  5.1 Assign SubcontractorA for this VehicleA
                        //  5.2 Assign SiteA for this VehicleA
                        //  5.3 Go to Driver tab > Assign DriverA and Occupied for this Vehicle
                        objVehicleList = objDriver.leftMenu.navigateToSubPage("Master Data", "Vehicle")
                        objVehicleList.header.pageTitleShouldBe("Vehicle")
                        cy.reload()
                        objVehicleAdd = objVehicleList.goToCreateVehiclePage()
                        objVehicleList = objVehicleAdd.createVehicle(number, subNameTH, "yes", "Save", "empty", "empty", "success")

                        //6. Go to Master Data > Route Code > Import New Route CodeA => manual

                        // 7. Go to Master Data > Service Item >
                        //    7.1 Add New Service ItemA with Status = Enable
                        cy.reload()
                        objServiceItem = objVehicleList.leftMenu.navigateToSubPage("Master Data", "Service Item ")
                        cy.randomString(5).then((serviceNameA) => {
                            const serviceItemCodeA = "Item " + Math.random().toString(30).substring(2, 6) + Math.floor(Math.random() * 1000)
                            objServiceItem.addNewItem(serviceNameA + "_item_auto", "place an order", "any", serviceItemCodeA, "base")
                            objServiceItem.common.msgSuccess().should("be.visible")
                            objServiceItem.searchItem(serviceNameA + "_item_auto")
                            cy.wait(1000)
                            objServiceItem.enableServiceItem()
                            // objServiceItem.common.clearFilterForInput("Item Name")
                            //    7.2 Add New Service ItemB with Status = Enable
                            cy.randomString(5).then((serviceNameB) => {
                                const serviceItemCodeB = "Item " + Math.random().toString(30).substring(2, 6) + Math.floor(Math.random() * 1000)
                                objServiceItem.addNewItem(serviceNameB + "_item_auto", "place an order", "any", serviceItemCodeB, "base")
                                objServiceItem.common.msgSuccess().should("be.visible")
                                objServiceItem.searchItem(serviceNameB + "_item_auto")
                                cy.wait(1000)
                                objServiceItem.enableServiceItem()
                                objServiceItem.leftMenu.closeTab()
                                // 8. Go to Master Data > Service Product  
                                objServiceProduct = objServiceItem.leftMenu.navigateToSubPage("Master Data", "Service Product")
                                //   8.1 Add New Service ProductA, atrribute = Customer  with Status = Enable
                                cy.randomString(5).then((namePDA) => {
                                    const serviceProductCodeA = "Prod " + Math.random().toString(30).substring(2, 6) + Math.floor(Math.random() * 1000)
                                    cy.randomString(4).then((shortNamePDA) => {
                                        objServiceProduct.enableProd(namePDA + "prod_auto", serviceProductCodeA, "customer", shortNamePDA, 50)
                                        objServiceProduct.verifyOperateProd(namePDA, "pause")
                                        //Go to Detail Service ProductA > Add ServiceItemA for this Service ProductA
                                        cy.wait(1000)
                                        objServiceProduct.common.clearFilterForInput("product name\\code")
                                        //   8.2 Add New Service ProductB, atrribute = Subcontractor with Status = Enable 
                                        cy.randomString(5).then((namePDB) => {
                                            const serviceProductCodeB = "Prod " + Math.random().toString(30).substring(2, 6) + Math.floor(Math.random() * 1000)
                                            cy.randomString(4).then((shortNamePDB) => {
                                                objServiceProduct.enableProd(namePDB + "prod_auto", serviceProductCodeB, "subcontractor", shortNamePDB, 21)
                                                objServiceProduct.verifyOperateProd(namePDB, "pause")
                                                //Go to Detail Service ProductB > Add ServiceItemB for this Service ProductB
                                                objServiceProduct.leftMenu.closeTab()
                                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                // 9. Login TMS > Go to Shipping Order > Create New Order1 > Click Confirm
                                                cy.randomStringAndNumber(9).then((track) => {
                                                    trackNumber = track
                                                    //create shipping order and confirm
                                                    objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Save", trackNumber, customerNameTH, namePDA + "prod_auto", temperature, "TestT1", siteName, 20);
                                                    // objShippingOrder.common.msgSuccess().should("be.visible")
                                                    objShippingOrder.header.btnAtHeader("more filter").click({ force: true })
                                                    objShippingOrder.common.clearFilterForDropdown("Status")
                                                    objShippingOrder.searchByInputCondition("External Tracking Number", trackNumber, "yes")
                                                    objShippingOrder.confirmShippingOrder()
                                                    // 10. Go to Plan > Delivery Plan 
                                                    //  10.1 Click Inquire > Select Order1 > Create a dispatch Order
                                                    //  10.2 On the Disptach Order > Select Dispatch Number of Order > Click Confirm 
                                                    objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                                                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackNumber, "first")
                                                    //create dispach order
                                                    objDeliveryPlan.createADispatchOrder(trackNumber)
                                                    objDeliveryPlan.leftMenu.closeTab()
                                                    objDeliveryPlan.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                                                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackNumber, "second")
                                                    objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
                                                    objDeliveryPlan.header.searchResult(trackNumber).should("be.visible")
                                                    //confirm dispatch
                                                    objDeliveryPlan.confirmDispatch()
                                                    objDeliveryPlan.common.msgSuccess().should("be.visible")
                                                    objDeliveryPlan.leftMenu.closeTab()
                                                    //9. Login DriverA in DriverApp successfully
                                                    var apiLogin = "https://account-stg.allnowgroup.com/mobile/driver-login"
                                                    var driverID = "", accessToken = ""
                                                    var body1 = {
                                                        "countryCode": 66,
                                                        "phone": mobilePhone,
                                                        "password": password,
                                                        "accessOtherDevice": true,
                                                        "deviceId": "b791071c2d28be26"
                                                    }
                                                    const options = {
                                                        method: 'POST',
                                                        url: apiLogin,
                                                        body: body1
                                                    }
                                                    cy.request(options).then((response) => {
                                                        expect(response.status).equal(200)
                                                        driverID = response.body.items.user.driverId
                                                        accessToken = response.body.items.token.access_token
                                                        cy.log("driverID1 is: " + driverID, "accessToke is: " + accessToken)

                                                        objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                                                        objSchedule.common.searchByClickSuggestion("Dispatch Number", trackNumber)
                                                        cy.wait(1000)
                                                        objSchedule.columnDispatchNumber().then((dispatchNumber) => {
                                                            var dispatch = dispatchNumber.text().trim()

                                                            //13. Go to DriverA in DriverApp again > Click Accept
                                                            //verify fmctoken
                                                            var authorize = accessToken
                                                            var fcmTokenLink = ""+ url +"//wapi/api/v1/notification/fcmtoken"
                                                            var bodyFMCToken = {
                                                                "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                                                                "driverID": driverID,
                                                                "deviceID": "b791071c2d28be26",
                                                                "createdDatetime": "2023-03-21T16:14:45.073Z"
                                                            }
                                                            const options2 = {
                                                                method: 'POST',
                                                                url: fcmTokenLink,
                                                                headers: { authorization: authorize },
                                                                body: bodyFMCToken
                                                            }
                                                            cy.log("options2 is: " + JSON.stringify(options2))
                                                            cy.request(options2).then((response) => {
                                                                expect(response.status).equal(200)
                                                            })

                                                            objSchedule.common.openDetailView()
                                                            objSchedule.header.searchResult(trackNumber).should("be.visible")
                                                            cy.wait(1000)
                                                            //assign carrier + driver
                                                            objSchedule.selectDataAndSchedule(subNameTH, namePDB + "prod_auto", driverName, "Confirm and Dispatch")
                                                            objSchedule.common.msgSuccess().should("be.visible")

                                                            //Call API pending message
                                                            var pendingMessage = ""+ url +"/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                                                            const optionsPendingMessage = {
                                                                method: 'GET',
                                                                url: pendingMessage,
                                                                headers: { authorization: authorize }
                                                            }
                                                            cy.log("option pending msg is: " + JSON.stringify(optionsPendingMessage))
                                                            cy.request(optionsPendingMessage).then((response) => {
                                                                expect(response.status).equal(200)
                                                                expect(response.body.result).not.be.empty
                                                                var messageID = response.body.result[0].msgId
                                                                cy.log("message id is: " + messageID)

                                                                //Accept message
                                                                var acceptMessage = ""+ url +"/wapi/api/v1/notification/accepted-message"
                                                                var bodyAcceptMsg = {
                                                                    "msgID": messageID,
                                                                    "jobNo": dispatch,
                                                                    "ackDateTime": "2023-03-21T16:43:46.073Z"
                                                                }
                                                                const optionsAcceptMessage = {
                                                                    method: 'POST',
                                                                    url: acceptMessage,
                                                                    headers: { authorization: authorize },
                                                                    body: bodyAcceptMsg
                                                                }
                                                                cy.log("option accept msg is: " + JSON.stringify(optionsAcceptMessage))
                                                                cy.request(optionsAcceptMessage).then((response) => {
                                                                    expect(response.status).equal(200)
                                                                    expect(response.body.result).equal(1)
                                                                })
                                                            })
                                                            // 12. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                                                            //  12.1 Click Update Driver 1st time --> Status Shipment = Shipment_Arrival_Pickup
                                                            //  12.2 Click Update Driver 2nd time --> Status Shipment = Shipment_Loading
                                                            // 13. Go to Implement > Pickup > Select Dispatch Number of Order1  > Click Pickup Completed --> Status Shipment = Shipment_Ontheway
                                                            // 14. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                                                            //   14.1 Click Update Driver 1st time  --> Status Shipment = Shipment_arrival_destination
                                                            //   14.2 Click Update Driver 2nd time --> Status Shipment = Shipment_unloading
                                                            // 15. Go to Implement > Delivery Goods > Select Dispatch Number of Order1  > Click Delivery Completed
                                                            // 16. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD
                                                            // 17. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order
                                                            // bug https://truemoney.atlassian.net/browse/TMSB-4812 cannot update status
                                                            objE2E.updateShipmentStatusToShipmentDelivered(dispatch, filePath, "yes", "normal")
                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})