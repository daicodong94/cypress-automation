import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import ShippingOrder from "../shippingorder/ShippingOrder"

class Site {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.shippingOrder = new ShippingOrder()
    }

    deleteSite(siteName) {
        this.shippingOrder.searchByInputCondition("site code/name", siteName, "none")
        if (this.header.searchResult(siteName).should("be.visible").eq(true)) {
            this.header.searchResult(siteName).click()
            this.header.btnAtHeader("delete").click()
            this.common.btnConfirmHintPopUp("confirm")
            this.common.msgSuccess().should("be.visible")
        }
    }

    spanTemperature(siteType) {
        return cy.xpath("(//div[@class='el-scrollbar']//li[@class='el-select-dropdown__item']//span[text()='" + siteType + "'])[last()]")
    }

    cityResult() {
        return cy.xpath("//li[@class='el-cascader__suggestion-item']//span")
    }

    createSite(siteName, siteType, city, address) {
        this.shippingOrder.searchByInputCondition("site code/name", siteName, "none")
        cy.get("body").then(body => {
            // console.log(body.find("div:contains(" + siteName + ")"))
            if (body.find("div:contains(" + siteName + ")").length > 0) {
                return
            } else {
                this.header.btnAtHeader("new").click()
                cy.wait(1000)
                //site name
                this.common.textField("please enter a site name").type(siteName)
                cy.wait(1000)
                //site type
                this.common.textField("please select a site type").click()
                this.spanTemperature(siteType).click()
                cy.wait(1000)
                //site code
                this.common.textField("please enter site code").type(siteName + "testCode")
                cy.wait(1000)
                //city
                this.common.textField("please choose").click()
                this.common.textField("please choose").focus().type(city)
                cy.wait(1000)
                this.cityResult().click()
                cy.wait(1000)
                //address
                this.common.textField("please enter").type(address)
                cy.wait(3000)
                this.common.btnDialogFooter("save").click()
                this.common.msgSuccess().should("be.visible")
                this.enableSite(siteName)
                this.editSite(siteName)
            }
        })
    }

    tabSiteStaff() {
        return cy.xpath("//div[@id='tab-siteStaff']")
    }

    lnkNew() {
        return cy.xpath("//button[@class='el-button pd0 el-button--text el-button--mini']//span[text()='new']")
    }

    ipContactPerson() {
        return cy.xpath("//input[@data-vv-name='contactName0']")
    }

    ipContactNumber() {
        return cy.xpath("//input[@data-vv-name='contactMobile0']")
    }

    ipMail() {
        return cy.xpath("//input[@data-vv-name='counactEmail0']")
    }

    ipJobTitle() {
        return cy.xpath("//input[@data-vv-name='jobDescription0']")
    }

    iconSave() {
        return cy.xpath("//div[@class='cell']//button[@title='save']")
    }

    addContactPerson(person, mail, job) {
        mail = mail + "@gmail.com"
        cy.randomNumber(9).then((phone) => {
            this.ipContactNumber().type("66" + phone)
        })
        this.ipContactPerson().type(person)
        this.ipMail().type(mail)
        this.ipJobTitle().type(job)
        cy.wait(2000)
        this.iconSave().click()
        this.common.msgSuccess().should("be.visible")
    }

    editSite(siteName) {
        this.shippingOrder.searchByInputCondition("site code/name", siteName, "yes")
        this.header.searchResult(siteName).dblclick()
        cy.wait(1000)
        this.tabSiteStaff().click()
        cy.wait(1000)
        this.common.lnkButton("new").click()
        // this.lnkNew().click()
        cy.wait(1000)
        this.addContactPerson(siteName + "test", siteName + "test", "driver")
    }

    enableSite(siteName) {
        this.shippingOrder.searchByInputCondition("site code/name", siteName, "yes")
        this.header.searchResult(siteName).click()
        cy.wait(1000)
        this.header.btnAtHeader("enable").click()
        cy.wait(1000)
        this.common.btnConfirmHintPopUp("confirm").click()
        this.common.msgSuccess().should("be.visible")
    }
}

export default Site;