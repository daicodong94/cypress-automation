import  LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import { Utility } from "../../../support/utility"

describe("Checking Our - User Organization", () => {

    var objUserOrganization, data, objLogin, objHome
    

    beforeEach("TMS-Our-UserOrganization", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            // const url = new Utility().getBaseUrl()
            cy.visit("https://supplier-pt.allnowgroup.com/")
            // if (url == "https://supplier-pt.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            // }   else if (url == "https://tms.allnowgroup.com") {
            //     cy.fixture("data_test.json").then((loginData) => {
            //         data = loginData
            //         cy.visit(data.url)
            //         objLogin = new Login()
            //         objHome = objLogin.login(data.username, data.password)
            //     })
            // }
        })
    })

    // afterEach(()=>{
    //     cy.wait(10000)
    //     objHome.logout()
    // })

    it("TC_232 - Check add to add people", () => {
        objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        for(let i = 0; i < 45; i++){
            cy.randomString(5).then((name) => {
                const number = "00000" + (Math.floor(Math.random(1000) * 10000) + 1000).toString();
                const nameUO = name + "0"
                objUserOrganization.addNewUser(nameUO, nameUO, number)
                objUserOrganization.header.ipSearchBox("name").first().click()
                objUserOrganization.header.ipSearchBox("name").first().type(nameUO, { force: true })
                objUserOrganization.header.btnAtHeader("inquire").click({ force: true })
                objUserOrganization.addUserToRoleForAddData()
                // objUserOrganization.deleteUser("name", name)
                cy.reload()
                objHome.logout()
                objHome = objLogin.loginWithUserandPassword(nameUO + "@email.com", "zxy123456")
                objHome.txtPassword().first().type("Vmo@123456")
                objHome.txtConfirmPassword().last().type("Vmo@123456")
                objHome.btnSetPassword().click({ force : true })
                objHome.btnContinueToAllNowTMS().dblclick({ multiple: true , force : true})
                objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
            })
        }

        // objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
        // cy.wait(10000)
        // cy.randomString(5).then((name) => {
        //     const number = Math.floor(Math.random() * 100000000) + 10000000;
        //     objUserOrganization.addNewUser(name, name, number)
        //     objUserOrganization.deleteUser("name", name)
        // })


    })

    // it("TC_233 - Check relieve success of user organization's", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.removeUser(name, number, "name", name)
    //     })
    // })

    // it("TC_234 - Check disable success of user organization's", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.disabledUser(name, number, "name", name, "disabled")
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_235 - Check enable success of user organization's", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.enableUser(name, number, "name", name)
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_236 - Check inquire value of user organization's", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.addNewUser(name, name, number)
    //         objUserOrganization.verifyDataInquire(name, name, number, "name", name)
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_237 - Check all required field when creating the new User - phone number", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.checkUniquePhone(name, number, "name", name)
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_237 - Check all required field when creating the new User - mail", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.checkUniqueMail(name, number, "name", name)
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_238 - Check department add a organization", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.checkAddUserToDepartment(name, number, "name", name)
    //         objUserOrganization.checkDeleteChildUserOrgan()
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_239 - Check department revise organization", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     objUserOrganization.checkReviseInDepartment()
    // })

    // it("TC_240 - Check department delete organization", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         const number = Math.floor(Math.random() * 100000000) + 10000000;
    //         objUserOrganization.checkAddUserToDepartment(name, number, "name", name)
    //         objUserOrganization.checkDeleteChildUserOrgan()
    //         objUserOrganization.verifyDeleteChildUserOrgan(name)
    //         objUserOrganization.deleteUser("name", name)
    //     })
    // })

    // it("TC_241 - Check role add a new characters", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         objUserOrganization.addNewRole(name)
    //         objUserOrganization.checkDeleteRole(name)
    //     })
    // })

    // it("TC_242 - Check role revise", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         objUserOrganization.addNewRole(name)
    //         cy.randomString(5).then((role) => {
    //             objUserOrganization.checkReviseRole(role)
    //             objUserOrganization.checkDeleteRole(name)
    //         })
    //     })
    // })

    // it("TC_243 - Check role delele", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         objUserOrganization.addNewRole(name)
    //         objUserOrganization.checkDeleteRole(name)
    //     })
    // })

    // it("TC_244 - Check add user to role", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         cy.randomString(5).then((role) => {
    //             const number = Math.floor(Math.random() * 100000000) + 10000000;
    //             objUserOrganization.addUserToRole(name, number, "name", name, role)
    //             objUserOrganization.deleteUser("name", name)
    //             objUserOrganization.checkDeleteRole(name)
    //         })
    //     })
    // })

    // it("TC_245 - Check user our role", () => {
    //     objUserOrganization = objHome.leftMenu.navigateToSubPage("Master Data","User Organization")
    //     cy.wait(10000)
    //     cy.randomString(5).then((name) => {
    //         cy.randomString(5).then((role) => {
    //             const number = Math.floor(Math.random() * 100000000) + 10000000;
    //             objUserOrganization.checkUserOurRole(name, number, "name", name, role)
    //             objUserOrganization.deleteUser("name", name)
    //             objUserOrganization.checkDeleteRole(name)
    //         })
    //     })
    // })
})