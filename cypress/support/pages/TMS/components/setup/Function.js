import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"

class Function {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    //scheduled on the same date
    ddlScheduleOffCarrier() {
        return cy.xpath("//label[text()='schedule offline carrier capacity information']//following-sibling::div//input")
    }

    selectScheduleOffCarrier(value) {
        this.ddlScheduleOffCarrier().click()
        this.common.lblElement(value).click()
        this.header.btnAtHeader("save").click()
        this.common.msgSuccess().should("be.visible")
    }
}

export default Function;