import LeftMenu from "../../../TMS/components/fragments/LeftMenu";
import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import CreateSite from "./CreateSite";
import SiteList from "./SiteList";

class SiteAdd {
    constructor() {
        this.header = new HeaderMasterData()
        this.common = new CommonMasterData()
        this.siteList = new SiteList()
        this.createSite = new CreateSite()
        this.leftMenu = new LeftMenu()
    }

    titleSiteAdd() {
        return cy.xpath('//p[text()="Create site"]')
    }

    ipCompanyName() {
        return cy.xpath('//textarea[@placeholder="Enter Thai Company Name"]')
    }

    numberCharacter(number, maxlength) {
        return cy.xpath('//div[contains(text(),"'+ number +'/ '+ maxlength +'")]')
    }

    msgInforAlreadyExist(message) {
        return cy.xpath('//div[contains(text(),"'+ message +'")]')
    }

    ckbDayChecked(days) {
        return cy.xpath('//span[text()="'+ days +'"]//preceding-sibling::span[@class="el-checkbox__input is-checked"]')
    }

    ckbDayUnChecked(days) {
        return cy.xpath('//span[text()="'+ days +'"]//preceding-sibling::span[@class="el-checkbox__input"]')
    }

    selectTime(days, time) {
        return cy.xpath('//span[text()="'+ days +'"]//ancestor::div[@class="flex items-center gap-x-20 h-[38px] px-4 mb-4"]//div[contains(text(),"'+ time +'")]')
    }

    divNoData(location) {
        return cy.xpath('(//div[text()="No Data"])['+ location +']')
    }

    windowTimeTab(name) {
        return cy.xpath('//div[@id="'+ name +'"]')
    }
    
    shouldUICreateSiteScreen() {
        this.header.btnCreate().click()
        cy.wait(2000)
        this.titleSiteAdd().should("be.visible")
        this.createSite.ipInformation("Enter Customer name").should("be.visible")
        this.createSite.ipInformation("Enter Code").should("be.visible")
        this.createSite.ipTextareaInfor("Enter Site name").should("be.visible")
        this.createSite.ipInformation("Enter Site type").should("be.visible")
        this.createSite.ipInformation("Select Site type group").should("be.visible")
        // this.createSite.ipTextareaInfor("Enter Address").scrollIntoView().should("be.visible")
    }

    editCustomer(CustomerName) {
        this.leftMenu.navigateToSubPage("Master Data" ,"Customer")
        cy.wait(2000)
        this.common.searchByCondition("ANL-C000000000003853")
        cy.wait(3000)
        this.common.openDetailsView()
        cy.wait(2000)
        this.createSite.btnEditSite().click()
        cy.wait(2000)
        this.ipCompanyName().clear()
        cy.wait(2000)
        this.ipCompanyName().type(CustomerName)
        cy.wait(2000)
        cy.randomNumber(5).then((number) => {
            this.createSite.ipTextareaInfor("Enter Customer SAP Code").clear()
            this.createSite.ipSiteTextareaInfor("Enter Customer SAP Code", number)
        })
        this.createSite.btnSave().click()
        cy.wait(1000)
        this.common.msgSuccess("Edit customer successfully").should("be.visible")
    }

    shouldDataCustomercorrect(infor, customer) {
        this.createSite.ipInformation(infor).click()
        this.createSite.selectCustomerName(customer).should("be.visible")
    }

    clickBtnCreate() {
        this.header.btnCreate().click()
        cy.wait(2000)
    }

    validationInputData(length, infor, value, maxlength) {
        cy.randomNumber(length).then((number) => {
            this.createSite.ipInformationOnSite(infor, value)
            cy.wait(2000)
            this.numberCharacter(length, maxlength).should("be.visible")
            this.createSite.ipInformation(infor).clear({force: true })
        })
    }

    validationInputDataSiteName(length, infor, value, maxlength) {
        cy.randomNumber(length).then((number) => {
            this.createSite.ipSiteTextareaInfor(infor, value)
            cy.wait(2000)
            this.numberCharacter(length, maxlength).should("be.visible")
            this.createSite.ipTextareaInfor(infor).clear()
        })
    }

    ipSiteNameMoreThanMaxlength(length, infor, maxlength) {
        cy.randomNumber(length).then((number) => {
            var newdata = number + '1223456789'
            if (length == "150") {
                this.createSite.ipSiteTextareaInfor(infor, number)
                cy.wait(2000)
                this.numberCharacter(length, maxlength).should("be.visible")
                this.createSite.ipTextareaInfor(infor).clear()
            } else if (length > "150") {
                this.createSite.ipSiteTextareaInfor(infor, newdata)
                cy.wait(2000)
                this.numberCharacter("150", maxlength).should("be.visible")
                this.createSite.ipTextareaInfor(infor).clear()
            }
        })
    }

    ipSiteShortNameMoreThanMaxlength(length, infor, maxlength) {
        cy.randomNumber(length).then((number) => {
            var newdata = number + '1223456789'
            if (length == "50") {
                this.createSite.ipInformationOnSite(infor, number)
                cy.wait(2000)
                this.numberCharacter(length, maxlength).should("be.visible")
                this.createSite.ipInformation(infor).clear()
            } else if (length > "50") {
                this.createSite.ipInformationOnSite(infor, newdata)
                cy.wait(2000)
                this.numberCharacter("50", maxlength).should("be.visible")
                this.createSite.ipInformation(infor).clear()
            }
        })
    }

    liValueDropdownList() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-select-dropdown__list'])[last()]//li")
    }

    checkDataServiceGroup(field, listData) {
        this.common.ipSelectBox(field).scrollIntoView().click({ force: true })
        const expectedResult = listData
        const actualResult = []
        console.log(expectedResult)
        this.liValueDropdownList().each(($li) => {
            actualResult.push($li.text())
        }).then(() => {
            console.log(actualResult)
            expect(expectedResult).to.include.members(actualResult)
        })
    }

    cancelCreateSite() {
        this.common.btnConfirmHintPopup("Cancel", "1").click()
        cy.wait(1000)
        this.common.btnConfirmHintPopup("Confirm", "1").click()
    }

    defaultCheckboxOpenHourTab() {
        this.ckbDayChecked("Monday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Tuesday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Wednesday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Thursday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Friday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Saturday").scrollIntoView().should("be.visible")
        this.ckbDayChecked("Sunday").scrollIntoView().should("be.visible")
    }

    uncheckDayOpenHour(days) {
        this.ckbDayChecked(days).click()
        cy.wait(2000)
        this.ckbDayUnChecked(days).scrollIntoView().should("be.visible")
        this.selectTime(days, "Start time").should("not.exist")
        this.selectTime(days, "End time").should("not.exist")
    }

    shouldNoDataOnWindowTimeTab() {
        this.windowTimeTab("tab-holiday").click()
        cy.wait(1000)
        this.divNoData("1").should("be.visible")
        cy.wait(1000)
        this.windowTimeTab("tab-special").click()
        cy.wait(1000)
        this.divNoData("2").should("be.visible")
    }

    btnAdd(location) {
        return cy.xpath('(//button//span[contains(text(),"Add")])['+ location +']')
    }

    selectSpecialTime(time) {
        return cy.xpath("//div[contains(text(),'Time')]//parent::div//span//div[contains(text(),'"+ time +"')]")
    }

    selectSpecialDate() {
        return cy.xpath('//div[contains(text(),"Date")]//ancestor::div[@class="col-span-12"]//div[@class="relative w-full bg-white"]')
    }

    txtAreaSpecialRemark(name) {
        return cy.xpath('//textarea[@placeholder="'+ name +'"]')
    }

    btnConfirmAddSpecialDay(btnName) {
        return cy.xpath('//div[@class="flex justify-end p-4"]//button//span[contains(text(),"'+ btnName +'")]')
    }

    nextDayOfToday() {
        return cy.xpath('((//div[@class="el-picker-panel__content"])[last()]//td[@class="available"]//div)[1]')
    }

    nextDayOfMonth() {
        return cy.xpath('((//div[@class="el-picker-panel__content"])[last()]//td[@class="next-month"]//div)[1]')
    }

    btnNextMonth() {
        return cy.xpath('//button[@aria-label="Next Month"]')
    }

    titleSpecialScreen(title) {
        return cy.xpath('//div[@class="el-dialog__header"]//span[contains(text(),"'+ title +'")]')
    }

    statusBtnConfirm(btnName, index) {
        return cy.xpath('(//span[contains(text(),"'+ btnName +'")]//ancestor::button)['+ index +']')
    }

    timeDisabled(time, index) {
        return cy.xpath('(//div[@class="cursor-pointer flex items-center justify-center h-[28px] text-disabled-25 pointer-events-none"][last() and contains(text(),"'+ time +'")])['+ index +']')
    }

    todayDisabled() {
        return cy.xpath('//td[@class="today disabled tw-tooltip"]//div')
    }

    tooltipSpecialDayOf() {
        return cy.xpath('//span[@class="tooltiptext"]')
    }

    btnConfirmQuitSpecialOff(btnName) {
        return cy.xpath('(//button//span[contains(text(),"'+ btnName +'")])[last()]')
    }

    titlePopupQuitSpecialOff() {
        return cy.xpath('//div[@class="el-message-box__title"]//span')
    }

    iconDeleteSpecialDay() {
        return cy.xpath('(//*[local-name()="svg" and @class="m-auto"])[2]')
    }

    btnClosePopup() {
        return cy.xpath('//i[@class="el-dialog__close el-icon el-icon-close"]')
    }

    dropDownMobilePhone() {
        return cy.xpath('//div[contains(text(),"Mobile Phone No.")]//ancestor::div[@class="col-span-6 -my-[22px]"]//input[@placeholder="Select"]')
    }

    dropDownLanlinePhone() {
        return cy.xpath('//div[contains(text(),"Landline phone no.")]//ancestor::div[@class="col-span-6 "]//input[@placeholder="Select"]')
    }

    btnAddKeyContact() {
        return cy.xpath('//button//div[contains(text(),"Add Key Contact")]')
    }

    shouldbeDisabledBtnAddKeyContact() {
        return cy.xpath('//div[contains(text(),"Add Key Contact")]//ancestor::button')
    }

    newKeyContact() {
        return cy.xpath('//div[contains(text(),"Key Contact Person 2")]')
    }

    iconDeleteContact(index) {
        return cy.xpath('(//div[@class="cursor-pointer text-xl"]//*[local-name()="svg"])['+ index +']')
    }

    addSpecialDay(name, location, condition, btnName, textName) {
        this.windowTimeTab(name).click()
        cy.wait(1000)
        this.btnAdd(location).click()
        cy.wait(2000)
        this.selectSpecialDate().click()
        if (condition == "today") {
            cy.selectDay("today")
        } else if (condition == "nextday") {
            this.btnNextMonth().click()
            this.nextDayOfToday().click()
        }
        this.common.btnConfirmDatePicker("Set").click()
        var date = new Date()
        var Hour = date.getHours()
        var StartHour = Hour + 1
        var EndHour = Hour + 2
        cy.log("hour =" + Hour)
        cy.log("date =" + date)
        // Select Start Time
        this.selectSpecialTime("Start time").click()
        this.createSite.selectHourAndMinutes(StartHour, "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("05", "2").scrollIntoView().click()
        this.createSite.btnOK().click()
        cy.wait(2000)
        // Select End Time
        this.selectSpecialTime("End time").click()
        this.createSite.selectHourAndMinutes(EndHour, "1").scrollIntoView().click()
        this.createSite.selectHourAndMinutes("06", "2").scrollIntoView().click()
        this.createSite.btnOK().click()
        cy.wait(2000)
        this.txtAreaSpecialRemark(textName).type("ACBD")
        this.btnConfirmAddSpecialDay(btnName).click()
    }

    navigateToSpecialScreen(name, index) {
        this.clickBtnCreate()
        this.windowTimeTab(name).click()
        this.btnAdd(index).click()
        cy.wait(2000)
    }

    confirmCancelCreateSpecialDayOff(confirm, title) {
        this.titlePopupQuitSpecialOff().should("be.visible")
        if (confirm == "confirm") {
            this.btnConfirmQuitSpecialOff("Confirm").click()
            this.titleSiteAdd().should("be.visible")
        } else if (confirm == "cancel") {
            this.btnConfirmQuitSpecialOff("Cancel").click()
            this.titleSpecialScreen(title).should("be.visible")
        }
    }

    msgInvalidInputName(msg) {
        return cy.xpath('//div[contains(text(),"'+ msg +'")]')
    }

    validationInputFirstLastName(infor, value, msg) {
            this.createSite.ipInformationOnSite(infor, value)
            cy.wait(2000)
            this.msgInvalidInputName(msg).should("be.visible")
            this.createSite.ipInformation(infor).clear()
    }

    linkSetStatus() {
        return cy.xpath('//div[contains(text(),"Set Status")]')
    }

    effectiveEndDate(name) {
        return cy.xpath('//div[contains(text(),"'+ name +'")]')
    }

    selectEffectDateTime() {
        return cy.xpath('//div[contains(text(),"Effective date / time")]//ancestor::div[@class="col-span-12"]//div[@class="relative w-full"]')
    }

    navigateToSetStatusScreen() {
        this.clickBtnCreate()
        this.linkSetStatus().click()
    }
    
    shouldUISetStatusScreen() {
        this.titleSpecialScreen("Set status").should("be.visible")
        this.siteList.rdBtnSetStatus("Active").should("be.visible")
        this.siteList.rdBtnSetStatus("Inactive").should("be.visible")
        this.effectiveEndDate("Effective date / time").should("be.visible")
        this.effectiveEndDate(" End date / time").should("be.visible")
        this.siteList.rdBtnSetStatus("No end date").should("be.visible")
        this.siteList.rdBtnSetStatus("Select date / time").should("be.visible")
    }

    hourAndMinutesSetStatus(time, location) {
        return cy.xpath('(//div[@class="rounded-sm dropdown-container bg-white z-20 swing-in-top-fwd absolute"]//div[contains(text(),"'+ time +'")])['+ location + ']')
    }

    btnOKStatus() {
        return cy.xpath('//button//span[contains(text(),"Ok")]')
    }

    selectDateTime() {
        return cy.xpath('//div[contains(text(),"Select date / time")]//ancestor::div[@class="col-span-12"]//div[@class="relative w-full"]')
    }

    statusSite(status) {
        return cy.xpath('//span[contains(text(),"'+ status +'")]')
    }

    titlePopupQuitSettingStatus() {
        return cy.xpath('//span[contains(text(),"Quit setting status")]')
    }

    divAvailableTodayDate() {
        return cy.xpath("(//div[@class='el-picker-panel__content'])[last()]//td[contains(@class,'available today')]//div//span")
    }

    cancelSetStatusSite(confirm, index) {
        this.common.btnCancel().click()
        cy.wait(1000)
        this.titlePopupQuitSettingStatus().should("be.visible")
        if (confirm == "yes") {
            this.common.btnConfirmHintPopup("Confirm", "1").click()
            this.titlePopupQuitSettingStatus().should("not.be.visible")
        } else if (confirm == "no") {
            this.common.btnConfirmHintPopup("Cancel", index).click()
            this.titlePopupQuitSettingStatus().should("be.visible")
        }
    }
}
export default SiteAdd;