import Vehicle from "../../../support/pages/MasterData/components/vehicle/Vehicle"
import VehicleEdit from "../../../support/pages/MasterData/components/vehicle/VehicleEdit"
import VehicleList from "../../../support/pages/MasterData/components/vehicle/VehicleList"

describe("MasterData - Vehicle - Edit", () => {
    var data
    var objVehicle = new Vehicle(), objVehicleList = new VehicleList(), objVehicleEdit = new VehicleEdit()
    // var objLogin = new LoginSupplier(), objHome
    var id = "ANL-V000000000000255"

    beforeEach(function () {
        cy.fixture('data_test_new_enhancement.json').then(function (logindata) {
            data = logindata
            cy.visit(data.urlMasterData)
            objVehicleEdit.common.loginAndNavigateToSite(data.usernametms, data.password, "Master Data", "Vehicle")
        })
    })

    it('TC_3_Check display UI on "Edit vehicle" screen \n' +
        'TC_5	Check display all textboxes \n' +
        'TC_7	Check display all select fields \n' +
        'TC_8	Check display "Drivers" screen', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            //TC_3	Check display UI on "Edit vehicle" screen
            objVehicleEdit = objVehicle.gotoEditVehiclePage()
            //TC_5	Check display all textboxes
            objVehicleEdit.verifyDisplayTextboxes(id)
            //TC_7	Check display all select fields 
            objVehicleEdit.verifyDisplaySelectFields()
            //TC_8	Check display "Drivers" screen
            objVehicleEdit.vehicleAdd.verifyDefaultUIDriversTab()
            //
        })

    it('TC_10_Check select one data from "Vehicle type" \n' +
        'TC_12_Check data from "Service group" \n' +
        'TC_13_Check select one data from "Service group" \n' +
        'TC_15_Check data from "Work type" \n' +
        'TC_16_Check select one data from "Work type" \n' +
        'TC_18_Check data from "Insurance" \n' +
        'TC_19_Check select one data from "Insurance" \n' +
        'TC_22_Check select one data from "Vehicle brand" \n' +
        'TC_21_Check data from "Vehicle brand" \n' +
        'TC_23	Check select "other" data from drop-down lists of Vehicle brand field \n' +
        'TC_24	Input characters include number, text and special characters on the text box after user select "other" data from drop-down list of Vehicle brand field \n' +
        'TC_25	Input 50 characters on the text box after user select "other"data from drop-down list of Vehicle brand field \n' +
        'TC_28	Check data from "Vehicle Model Year" \n' +
        'TC_29	Check select one data from "Vehicle Model Year" \n' +
        'TC_31	Check data from "Fuel Type" \n' +
        'TC_32	Check select one data from "Fuel Type" \n' +
        "TC_44_Check select one data from 'Provincial Sign' field \n" +
        "TC_46_Check data from 'Plate color' field \n" +
        "TC_47_Check select one data from 'Plate color' field", () => {
            cy.fixture("Vehicle.json").then(function (listData) {
                data = listData
                objVehicleEdit.vehicleAdd.searchByCondition(id)
                objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
                objVehicle.gotoEditVehiclePage()
                objVehicleEdit.verifyDefaultUI()
                //TC_10_Check select one data from "Vehicle type" 
                objVehicleEdit.common.selectDataByClick("Select Vehicle Type", "10 ล้อตู้แห้ง7.5 ม. ประตู 10 บาน")
                //TC_12_Check data from "Service group" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Service Group", data.serviceList)
                //TC_13_Check select one data from "Service group" 
                objVehicleEdit.common.selectDataByClick("Select Vehicle Type", "ImEx")
                //TC_15_Check data from "Work type" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Work Type", data.workTypeList)
                //TC_16_Check select one data from "Work type" 
                objVehicleEdit.common.selectDataByClick("Select Work Type", "รถหลัก")
                //TC_18	Check data from "Insurance" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Insurance", data.insuranceList)
                //TC_19	Check select one data from "Insurance" 
                objVehicleEdit.common.selectDataByClick("Select Insurance", "ประกันภัยรถยนต์ชั้น 2")
                //TC_21	Check data from "Vehicle brand" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Vehicle Brand", data.vehicleBrandList)
                //TC_22	Check select one data from "Vehicle brand"
                objVehicleEdit.common.selectDataByClick("Select Vehicle Brand", "สแกนเนียร์")
                //TC_23	Check select "other" data from drop-down lists of Vehicle brand field
                objVehicleEdit.common.selectDataByClick("Select Vehicle Brand", "อื่นๆ")
                //TC_24	Input characters include number, text and special characters on the text box after user select "other" data from drop-down list of Vehicle brand field
                objVehicleEdit.common.ipSelectBox("Enter additional detail").should("be.visible")
                objVehicleEdit.common.ipSelectBox("Enter additional detail").type("@!#@asd32423")
                //TC_25	Input 50 characters on the text box after user select "other"data from drop-down list of Vehicle brand field
                objVehicleEdit.common.ipSelectBox("Enter additional detail").clear({ force: true })
                objVehicleEdit.common.inputDataToField("Enter additional detail", data.fiftyCharacters)
                //TC_28	Check data from "Vehicle Model Year" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Vehicle Model Year", objVehicleEdit.vehicleAdd.getListVehicleModelYear())
                //TC_29	Check select one data from "Vehicle Model Year" 
                objVehicleEdit.common.selectDataByClick("Select Vehicle Model Year", "2022")
                //TC_31	Check data from "Fuel Type" 
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Fuel Type", data.listFuelType)
                //TC_32	Check select one data from "Fuel Type" 
                objVehicleEdit.common.selectDataByClick("Select Fuel Type", "พรีเมี่ยมดีเซล B7")
                //TC_46_Check data from 'Plate color' field
                objVehicleEdit.vehicleAdd.checkDataServiceGroup("Select Plate Color", data.listPlateColor)
                //TC_44_Check select one data from 'Provincial Sign' field
                objVehicleEdit.common.selectDataByClick("Select Provincal Sign", "สุราษฎร์ธานี")
                //TC_47_Check select one data from 'Plate color' field
                cy.wait(2000)
                objVehicleEdit.common.selectDataByClick("Select Plate Color", "ขาว")

                objVehicleEdit.siteAdd.cancelCreateSite("Cancel", "1")
            })
        })

    it('TC_27	Leave blank "Vehicle brand" field \n' +
        'TC_53	Leave blank "Registered Date" field \n' +
        'TC_62	Leave blank "Vehicle Width" field \n' +
        'TC_71	Leave blank "Vehicle Length" field \n' +
        'TC_80	Leave blank "Vehicle Height" field \n' +
        'TC_89	Leave blank "Vehicle Max Weight" field \n' +
        'TC_98	Leave blank "Issue Date" field \n' +
        'TC_105	Leave blank "Expire Date" field', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.verifyDefaultUI()
            //TC_27	Leave blank "Vehicle brand" field
            //TC_53	Leave blank "Registered Date" field
            //TC_62	Leave blank "Vehicle Width" field
            //TC_71	Leave blank "Vehicle Length" field
            //TC_80	Leave blank "Vehicle Height" field
            //TC_89	Leave blank "Vehicle Max Weight" field
            //TC_98	Leave blank "Issue Date" field
            //TC_105	Leave blank "Expire Date" field

            objVehicleEdit.vehicleAdd.btnConfirm("Save").should("have.attr", "disabled", "disabled")
        })

    it("TC_34_Check input correct format 1 of License Plate number on 'License Plate number' \n" +
        "TC_35_Check input correct format 2 of license plate number on 'License Plate number' \n" +
        "TC_36_Check input license plate number start by number 0 \n" +
        "TC_37_Check input license plate number different list follow (Ex: ข้) \n" +
        "TC_38_Check input license plate number inlude language different Thai language \n" +
        "TC_39_Check input license plate number include special characters \n" +
        "TC_41_Check input 10 characters on 'License plate number' field", () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.verifyDefaultUI()
            //TC_34_Check input correct format 1 of License Plate number on 'License Plate number'
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "1กข1234")
            //TC_35_Check input correct format 2 of license plate number on 'License Plate number'
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "80-1234")
            //TC_36_Check input license plate number start by number 0 
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "0กข1234")
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "00-1234")
            //TC_37_Check input license plate number different list follow (Ex: ข้)
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "1ข้ข1234")
            //TC_38_Check input license plate number inlude language different Thai language 
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "1abc1234")
            //TC_39_Check input license plate number include special characters
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "1@#$1234")
            //TC_41_Check input 10 characters on 'License plate number' field
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "12345678900")
            objVehicleEdit.common.inputDataToField("Enter Plate Number (ex. กข1234)", "1ฒท-7378ฒท1")

            objVehicleEdit.siteAdd.cancelCreateSite("Cancel", "1")
        })

    it("TC_49_Check fomat from 'Registered Date' field \n" +
        "TC_54_Input decimal number minimum: 01.01 on 'Vehicle Width' field \n" +
        "TC_55_Input decimal number maximum: 99.99 on 'Vehicle Width' field \n" +
        "TC_56_Input integer number minimum: 1 on 'Vehicle Width' field \n" +
        "TC_57_Input integer number minimum: 99 on 'Vehicle Width' field \n" +
        "TC_58_Input integer number minimum: 0/00.00 on 'Vehicle Width' field \n" +
        "TC_59_Input number: 100.1 on 'Vehicle Width' field \n" +
        "TC_60_Input number over 2 decimal: 55.555 on 'Vehicle Width' field \n" +
        "TC_61_Input speacial characters/letters @#@$%$/abc 'Vehicle Width' field \n" +
        "TC_63_Input number minimum: 01.01 on 'Vehicle Length' field \n" +
        "TC_64_Input number minimum: 99.99 on 'Vehicle Length' field \n" +
        "TC_65_Input integer number minimum: 1 on 'Vehicle Length' field \n" +
        "TC_66_Input integer number maximum: 99 on 'Vehicle Length' field \n" +
        "TC_67_Input number: 0/00.00 on 'Vehicle Length' field \n" +
        "TC_68_Input number: 100.1 on 'Vehicle Length' field \n" +
        "TC_69_Input number over 2 decimal: 55.555 on 'Vehicle Length' field \n" +
        "TC_70_Input speacial characters/letters @#@$%$/abc 'Vehicle Length' field", () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.verifyDefaultUI()

            //TC_49_Check fomat from 'Registered Date' field
            objVehicleEdit.vehicleAdd.verifyRegisteredDatePickerDisplaying()
            //VEHICLE WIDTH(M)
            //TC_54_Input decimal number minimum: 01.01 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "01.01")
            //TC_55_Input decimal number maximum: 99.99 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "99.99")
            //TC_56_Input integer number minimum: 1 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            //TC_57_Input integer number minimum: 99 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "99")
            //TC_58_Input integer number minimum: 0/00.00 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "0")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "00.00")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Width min is 1.01")
            //TC_59_Input number: 100.1 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "100.1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Width max is 99.99")
            //TC_60_Input number over 2 decimal: 55.555 on 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "55.555")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Width")
            //TC_61_Input speacial characters/letters @#@$%$/abc 'Vehicle Width' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "@#@$%$")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Width")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Width", "abc")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Width")

            //VEHICLE LENGTH (M)
            //TC_63_Input number minimum: 01.01 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "01.01")
            //TC_64_Input number minimum: 99.99 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "99.99")
            //TC_65_Input integer number minimum: 1 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            //TC_66_Input integer number maximum: 99 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "99")
            //TC_67_Input number: 0/00.00 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "0")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "00.00")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Length min is 1.01")
            //TC_68_Input number: 100.1 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "100.1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Length max is 99.99")
            //TC_69_Input number over 2 decimal: 55.555 on 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "55.555")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Length")
            //TC_70_Input speacial characters/letters @#@$%$/abc 'Vehicle Length' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "@#@$%$")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Length")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Length", "abc")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Length")

            objVehicleEdit.siteAdd.cancelCreateSite("Cancel", "1")
        })

    it("TC_72_Input number minimum: 01.01 on 'Vehicle Height' field \n" +
        "TC_73_Input number minimum: 99.99 on 'Vehicle Height' field \n" +
        "TC_74_Input integer number minimum: 1 on 'Vehicle Height' field \n" +
        "TC_75_Input integer number maximum: 99 on 'Vehicle Height' field \n" +
        "TC_76_Input number: 0/00.00 on 'Vehicle Height' field \n" +
        "TC_77_Input number: 100.1 on 'Vehicle Height' field \n" +
        "TC_78_Input number over 2 decimal: 55.555 on 'Vehicle Height' field \n" +
        "TC_79_Input speacial characters/letters @#@$%$/abc 'Vehicle Height' field \n" +
        "TC_81_Input number minimum: 01.01 on 'Vehicle Max Weight' field \n" +
        "TC_82_Input number minimum: 50.50 on 'Vehicle Max Weight' field \n" +
        "TC_83_Input integer number minimum: 1 on 'Vehicle Max Weight' field \n" +
        "TC_84_Input integer number maximum: 50 on 'Vehicle Max Weight' field \n" +
        "TC_85_Input number: 0/00.00 on 'Vehicle Max Weight' field \n" +
        "TC_86_Input number: 50.51 on 'Vehicle Max Weight' field \n" +
        "TC_87_Input number over 2 decimal: 50.499 on 'Vehicle Max Weight' field \n" +
        "TC_88_Input speacial characters/letters @#@$%$/abc 'Vehicle Max Weight' field", () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.verifyDefaultUI()

            //VEHICLE HEIGHT(M)
            //TC_72_Input number minimum: 01.01 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "01.01")
            //TC_73_Input number maximum: 99.99 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "99.99")
            //TC_74_Input integer number minimum: 1 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            //TC_75_Input integer number maximum: 99 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "99")
            //TC_76_Input integer number minimum: 0/00.00 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "0")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "00.00")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Height min is 1.01")
            //TC_77_Input number: 100.1 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "100.1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Height max is 99.99")
            //TC_78_Input number over 2 decimal: 55.555 on 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "55.555")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Height")
            //TC_79_Input speacial characters/letters @#@$%$/abc 'Vehicle Height' field
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "@#@$%$")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Height")
            objVehicleEdit.common.inputDataToField("Enter Vehicle Height", "abc")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Height")

            //VEHICLE MAX WEIGHT
            //TC_81_Input number minimum: 01.01 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "01.01")
            //TC_82_Input number maximum: 999.99 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "999.99")
            //TC_82_Input integer number minimum: 1 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            //TC_83_Input integer number maximum: 1 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "1")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            //TC_84	Input integer number maximum: 999 on "Vehicle Max Weight" field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "999")
            //TC_85_Input integer number minimum: 0/00.00 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "0")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "00.00")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Max Weight min is 1.01")
            //TC_86_Input number: 1000 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "10000")
            objVehicleEdit.common.msgErrorShouldBe("Vehicle Max Weight max is 999.99")
            //TC_87_Input number over 2 decimal: 999.899 on 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "999.899")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
            //TC_88_Input speacial characters/letters @#@$%$/abc 'Vehicle Max Weight' field
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "@#@$%$")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
            objVehicleEdit.common.inputDataToField("Enter Max Weight", "abc")
            objVehicleEdit.common.msgErrorShouldBe("Invalid Vehicle Max Weight")
        })

    it("TC_94_Check fomat from 'Issue Date' field \n" +
        'TC_96	Check select date in the past  as  B.E.Year (current year + 544) on "Issue Date" field \n' +
        "TC_99_Check fomat from 'Expire Date' field \n" +
        'TC_100	Input correct format: YYYY/MM/DD on "Expire Date" field \n' +
        'TC_101	Check input correct B.E.Year (current year + 544) on "Expire Date" field \n' +
        'TC_103	Input date in the future ( BE year : 2023 + 544) on "Expire Date " field \n' +
        'TC_104	Check select date in the past as B.E.Year (current year + 544) on "Expire Date" field \n' +
        "TC_105_Check display default status \n" +
        "TC_108_Check tick on Inactive tickbox for 'Status' \n" +
        "TC_109_Check click on 'Select date/time' field", () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.verifyDefaultUI()

            //TC_94_Check fomat from 'Issue Date' field
            objVehicleEdit.vehicleAdd.verifyIssuedDateField("Select Issued Date")

            //TC_96	Check select date in the past  as  B.E.Year (current year + 544) on "Issue Date" field 
            cy.wait(2000)
            objVehicleEdit.svgCompulsoryThirdPartyInsuranceIssuedDate().click()
            cy.selectYear("thisYear", -5)
            cy.selectMonth("thisMonth")
            cy.selectDay("today")
            objVehicleEdit.common.btnConfirmDatePicker("Set").click()

            //TC_99_Check fomat from 'Expire Date' field
            objVehicleEdit.vehicleAdd.verifyIssuedDateField("Select Expired Date")

            //TC_100	Input correct format: YYYY/MM/DD on "Expire Date" field
            //TC_101	Check input correct B.E.Year (current year + 544) on "Expire Date" field
            cy.wait(2000)
            objVehicleEdit.svgCompulsoryThirdPartyInsuranceExpiredDate().click()
            cy.selectYear("thisYear", 0)
            cy.selectMonth("thisMonth")
            cy.selectDay("today")
            objVehicleEdit.common.btnConfirmDatePicker("Set").click()

            //TC_103	Input date in the future ( BE year : 2023 + 544) on "Expire Date " field
            cy.wait(2000)
            objVehicleEdit.svgCompulsoryThirdPartyInsuranceExpiredDate().click()
            cy.selectYear("thisYear", 2)
            cy.selectMonth("thisMonth")
            cy.selectDay("today")
            objVehicleEdit.common.btnConfirmDatePicker("Set").click()

            //TC_104	Check select date in the past as B.E.Year (current year + 544) on "Expire Date" field 
            cy.wait(2000)
            objVehicleEdit.svgCompulsoryThirdPartyInsuranceExpiredDate().click()
            cy.selectYear("thisYear", -1)
            cy.selectMonth("thisMonth")
            cy.selectDay("today")
            objVehicleEdit.common.btnConfirmDatePicker("Set").click()

            //TC_106_Check redirect to status pop up 
            // fail: 2022/14/10    17:07 != 2022/14/10 17:07
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            //TC_109_Check click on 'Select date/time' field
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.checkRadioEndDateTime()
            //TC_108_Check tick on Inactive tickbox for 'Status'
            objVehicleEdit.vehicleAdd.checkRadioStatusInActive()
        })

    it('TC_106	Check display default status \n' +
        'TC_109	Check tick on Inactive tickbox for "Status" \n' +
        'TC_110	Check click on "Active date/time" field \n' +
        'TC_117	Check tick on Select date / time tickbox for "End date / time " \n' +
        'TC_118_Check click on "End date / time" field', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            //TC_106	Check display default status
            //failed because Subcontactor status display wrong
            //// objVehicleEdit.verifyDisplayDefaultStatus()
            //TC_109	Check tick on Inactive tickbox for "Status"
            objVehicleEdit.common.lnkSetStatus().click()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.checkRadioStatusInActive()
            objVehicleEdit.common.closeSetStatusPopUp()
            //TC_110	Check click on "Active date/time" field
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.verifySelectDateTimePickerDisplay()
            objVehicleEdit.common.closeSetStatusPopUp()
            //TC_117	Check tick on Select date / time tickbox for "End date / time "
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.checkRadioEndDateTime()
            //TC_118_Check click on 'End date / time' field
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.verifySelectDateTimePickerDisplay()
            objVehicleEdit.common.closeSetStatusPopUp()


            // objVehicle.gotoEditVehiclePage()
        })

    it('TC_129	Check click on the "Cancel" button when changed some fields \n' +
        'TC_130	Check click on the "Cancel" button on "Confirm" popup \n' +
        'TC_131	Check click on the "Confirm" button on "Confirm" popup \n' +
        'TC_132	Check click on the "Cancel" button when dont have any change', () => {
            //TC_129	Check click on the "Cancel" button when changed some fields
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusInActive()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.verifyCancelSetStatus()
            objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")
            //TC_130	Check click on the "Cancel" button on "Confirm" popup
            objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").click()
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusInActive()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.verifyCancelSetStatus()
            objVehicleEdit.common.btnConfirmHintPopupVer2("Cancel").click()
            objVehicleEdit.common.closeSetStatusPopUp()
            //TC_131	Check click on the "Confirm" button on "Confirm" popup
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.vehicleAdd.checkRadioStatusInActive()
            objVehicleEdit.vehicleAdd.checkRadioStatusActive()
            objVehicleEdit.vehicleAdd.verifyCancelSetStatus()
            objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").click()
            //TC_132	Check click on the "Cancel" button when don't have any change
            objVehicleEdit.common.openSetStatusPopUp()
            objVehicleEdit.common.btnConfirmSetStatus("Cancel").click()
        })

    it('TC_139	Check amount of Site displaying in the one page on the "Assign Site" screen \n' +
        'TC_140	Check input number of page into "Go to" field when system has many page on the "Assign Site" screen \n' +
        'TC_145	Check select one site for vehicle when system have site data \n' +
        'TC_155	Check delete assigned site for vehicle \n' +
        'TC_156	Check reassign site for vehicle \n' +
        'TC_163	Check amount of Site displaying in the one page on the "Assign Subcontractor" screen \n' +
        'TC_171	Check results search when the system has data match with keywords including " abcdef" \n' +
        'TC_172	Check results search when the system has data match with keywords including " abc" \n' +
        'TC_173	Check results search when the system has data match with keywords including " a"  \n' +
        'TC_174	Check results search when the system has data match with keywords including " number" \n' +
        'TC_175	Check results when the system hasnt data match with keywords \n' +
        'TC_176	Check delete inputted keywords \n' +
        'TC_177	Check press "Enter" from keyboard to search \n' +
        'TC_178	Check select subcontractor for vehicle when system has no site data \n' +
        'TC_179	Check delete assigned subcontractor for vehicle', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            //TC_139	Check amount of Site displaying in the one page on the "Assign Site" screen
            objVehicleEdit.vehicleAdd.openSitePopUp()
            objVehicleEdit.vehicleAdd.verifyTypeOfPageSize("Assign Site")
            //TC_140	Check input number of page into "Go to" field when system has many page on the "Assign Site" screen
            objVehicleEdit.vehicleAdd.goToPageNumber(3)
            //TC_145	Check select one site for vehicle when system have site data
            objVehicleEdit.vehicleAdd.selectAndVerifySite("empty", "9163822708")
            //TC_156	Check reassign site for vehicle
            objVehicleEdit.vehicleAdd.selectAndVerifySite("Site", "LUKE_SITE")
            //TC_155	Check delete assigned site for vehicle
            objVehicleEdit.vehicleAdd.deleteAssignedSite()
            objVehicleEdit.vehicleAdd.buttonAssignShouldBe()
            //TC_163	Check amount of Site displaying in the one page on the "Assign Subcontractor" screen
            objVehicleEdit.vehicleAdd.openSubcontractorPopUp()
            objVehicleEdit.vehicleAdd.verifyTypeOfPageSize("Assign Subcontractor")
            //TC_171	Check results search when the system has data match with keywords including " abcdef" 
            objVehicleEdit.vehicleAdd.searchByCondition(" YG entertainment")
            objVehicleEdit.common.searchResultShouldContain("YG entertainment")
            //TC_172	Check results search when the system has data match with keywords including " abc"  
            objVehicleEdit.vehicleAdd.searchByCondition(" entertainment")
            objVehicleEdit.common.searchResultShouldContain("entertainment")
            //TC_173	Check results search when the system has data match with keywords including " a"  
            objVehicleEdit.vehicleAdd.searchByCondition(" Y")
            objVehicleEdit.common.searchResultShouldContain("Y")
            //TC_174	Check results search when the system has data match with keywords including " number"
            objVehicleEdit.vehicleAdd.searchByCondition(" 4549589348543")
            objVehicleEdit.common.searchResultShouldContain("4549589348543")
            //TC_176	Check delete inputted keywords
            objVehicleEdit.vehicleAdd.clearSearchInputted()
            //TC_177	Check press "Enter" from keyboard to search
            objVehicleEdit.header.txtSearch().first().click({ force: true }).type("4549589348543", { force: true })
            cy.wait(2000)
            objVehicleEdit.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleEdit.common.searchResultShouldContain("4549589348543")
            //TC_179	Check delete assigned subcontractor for vehicle
            objVehicleEdit.vehicleAdd.lnkSelect().first().click({ force: true })
            objVehicleEdit.vehicleAdd.verifyAfterDeleteSite()
            //TC_175	Check results when the system hasn't data match with keywords
            objVehicleEdit.vehicleAdd.openSubcontractorPopUp()
            objVehicleEdit.vehicleAdd.searchByCondition("#@$@#$@#")
            objVehicleEdit.common.divNoData().should("be.visible")
            //TC_178	Check select subcontractor for vehicle when system has no site data
            objVehicleEdit.vehicleAdd.clickButtonCreateNewSubcontractor()
        })

    it('TC_147	Check results search when the system has data match with keywords including " abcdef" \n' +
        'TC_148	Check results search when the system has data match with keywords including " abc" \n' +
        'TC_149	Check results search when the system has data match with keywords including " a" \n' +
        'TC_150	Check results search when the system has data match with keywords including " number" \n' +
        'TC_151	Check results when the system hasnt data match with keywords \n' +
        'TC_152	Check delete inputted keywords \n' +
        'TC_153	Check press "Enter" from keyboard to search \n' +
        'TC_154	Check select site for vehicle when system has no site data', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            objVehicleEdit.vehicleAdd.openSitePopUp()
            cy.wait(2000)
            //TC_147	Check results search when the system has data match with keywords including " abcdef" 
            objVehicleEdit.vehicleAdd.searchByCondition(" Bakery")
            objVehicleEdit.common.searchResultShouldContain("Bakery")
            //TC_148	Check results search when the system has data match with keywords including " abc"  
            objVehicleEdit.vehicleAdd.searchByCondition(" Bak")
            objVehicleEdit.common.searchResultShouldContain("Bak")
            //TC_149	Check results search when the system has data match with keywords including " a"  
            objVehicleEdit.vehicleAdd.searchByCondition(" B")
            objVehicleEdit.common.searchResultShouldContain("B")
            //TC_150	Check results search when the system has data match with keywords including " number"
            objVehicleEdit.vehicleAdd.searchByCondition(" 00000000957")
            objVehicleEdit.common.searchResultShouldContain("00000000957")
            //TC_152	Check delete inputted keywords
            objVehicleEdit.vehicleAdd.clearSearchInputted()
            //TC_153	Check press "Enter" from keyboard to search
            objVehicleEdit.header.txtSearch().first().click({ force: true }).type("4788518803", { force: true })
            cy.wait(2000)
            objVehicleEdit.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleEdit.common.searchResultShouldContain("4788518803")
            //TC_151	Check results when the system hasn't data match with keywords
            objVehicleEdit.vehicleAdd.searchByCondition("#@$@#$@#")
            objVehicleEdit.common.divNoData().should("be.visible")
            //TC_154	Check select site for vehicle when system has no site data
            objVehicleEdit.vehicleAdd.clickButtonCreateNewSite()
        })

    it('TC_182	Check assign one driver for vehicle \n' +
        'TC_184	Check search by [Company Registered Name, Driver ID, Driver Name, Driver License Number] \n' +
        'TC_185	Check results search when the system has data match with keywords including " abcdef" \n' +
        'TC_186	Check results search when the system has data match with keywords including " abc"  \n' +
        'TC_187	Check results search when the system has data match with keywords including " a"  \n' +
        'TC_188	Check results search when the system has data match with keywords including " number" \n' +
        'TC_189	Check results when the system hasnt data match with keywords \n' +
        'TC_190	Check delete inputted keywords \n' +
        'TC_191	Check press "Enter" from keyboard to search \n' +
        'TC_192	Check assign driver from result search \n' +
        'TC_193	To check "Cancel" button on "Assign driver" popup when there is no action \n' +
        'TC_194	To check "Cancel" button on "Assign driver" popup when some drivers are ticked', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            cy.wait(1000)
            objVehicleEdit.vehicleAdd.verifyDefaultUIDriversTab()
            objVehicleEdit.vehicleAdd.clickAssignButton()
            //TC_182	Check assign one driver for vehicle
            //TC_192	Check assign driver from result search
            objVehicleEdit.vehicleAdd.searchDriver("ANL-D001009")
            objVehicleEdit.vehicleAdd.verifyAssignDriver()
            objVehicleEdit.vehicleAdd.occupiedOff().first().should("be.exist")
            //TC_184	Check search by [Company Registered Name, Driver ID, Driver Name, Driver License Number]
            //TC_185	Check results search when the system has data match with keywords including " abcdef" 
            objVehicleEdit.vehicleAdd.clickAssignButton()
            cy.wait(1000)
            objVehicleEdit.vehicleAdd.searchDriver(" Stella")
            objVehicleEdit.common.searchResultShouldContain("Stella")
            //TC_186	Check results search when the system has data match with keywords including " abc"  
            objVehicleEdit.vehicleAdd.searchDriver(" BHD movie")
            objVehicleEdit.common.searchResultShouldContain("BHD movie")
            //TC_187	Check results search when the system has data match with keywords including " a"  
            objVehicleEdit.vehicleAdd.searchDriver(" S")
            objVehicleEdit.common.searchResultShouldContain("S")
            //TC_188	Check results search when the system has data match with keywords including " number"
            objVehicleEdit.vehicleAdd.searchDriver(" 1009")
            objVehicleEdit.common.searchResultShouldContain("1009")
            //TC_190	Check delete inputted keywords
            objVehicleEdit.vehicleAdd.clearSearchInputted()
            //TC_191	Check press "Enter" from keyboard to search
            objVehicleEdit.header.txtSearch().first().click({ force: true }).type("ANL-D001009", { force: true })
            cy.wait(2000)
            objVehicleEdit.header.txtSearch().first().type('{enter}')
            cy.wait(1000)
            objVehicleEdit.common.searchResultShouldContain("ANL-D001009")
            //TC_189	Check results when the system hasn't data match with keywords
            objVehicleEdit.vehicleAdd.searchDriver("@#$@#%@")
            objVehicleEdit.common.divNoData().should("be.visible")
            //TC_193	To check "Cancel" button on "Assign driver" popup when there is no action
            objVehicleEdit.common.icCloseDialog().click({ force: true })
            objVehicleEdit.vehicleAdd.clickAssignButton()
            objVehicleEdit.vehicleAdd.btnAssignDriver("Cancel").click({ force: true })
            objVehicleEdit.common.btnAction("Assign").should("be.visible")
            cy.wait(2000)
            //TC_194	To check "Cancel" button on "Assign driver" popup when some drivers are ticked
            objVehicleEdit.vehicleAdd.clickAssignButton()
            cy.wait(2000)
            objVehicleEdit.vehicleAdd.selectAllData()
            cy.wait(1000)
            objVehicleEdit.vehicleAdd.btnAssignDriver("Cancel").click({ force: true })
            cy.wait(1000)
            objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")
            objVehicleEdit.common.btnConfirmHintPopupVer2("Cancel").click({ force: true })
        })

    it('TC_209	Check switch toggle button to on for one driver \n' +
        'TC_212	Check switch toggle button to off for one driver \n' +
        'TC_216	Check click "Save" button When the site has not been assigned', () => {
            objVehicleEdit.vehicleAdd.searchByCondition(id)
            objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
            objVehicle.gotoEditVehiclePage()
            cy.wait(1000)

            objVehicleEdit.vehicleAdd.openSitePopUp()
            objVehicleEdit.vehicleAdd.selectAndVerifySite("Site", "LUKE_SITE")
            objVehicleEdit.vehicleAdd.deleteAssignedSite()
            objVehicleEdit.vehicleAdd.buttonAssignShouldBe()

            objVehicleEdit.vehicleAdd.verifyDefaultUIDriversTab()
            objVehicleEdit.vehicleAdd.clickAssignButton()

            cy.wait(1000)
            //TC_209	Check switch toggle button to on for one driver
            objVehicleEdit.vehicleAdd.searchDriver("ANL-D001009")
            objVehicleEdit.vehicleAdd.verifyAssignDriver()
            objVehicleEdit.vehicleAdd.btnOccupied().first().click({ force: true })
            objVehicleEdit.vehicleAdd.occupiedOn().first().should("be.exist")
            //TC_212	Check switch toggle button to off for one driver
            objVehicleEdit.vehicleAdd.btnOccupied().first().click({ force: true })
            objVehicleEdit.vehicleAdd.occupiedOff().first().should("be.exist")
            //TC_216	Check click "Save" button When the site has not been assigned
            objVehicleEdit.vehicleAdd.btnOccupied().first().click({ force: true })
            objVehicleEdit.vehicleAdd.occupiedOn().first().should("be.exist")
            cy.wait(1000)
            objVehicleEdit.vehicleAdd.btnConfirm("Save").click()
            objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")
        })

    it('TC_218	Check click "Save" button When the driver has not been occupied', () => {
        objVehicleEdit.vehicleAdd.searchByCondition(id)
        objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
        objVehicle.gotoEditVehiclePage()
        cy.wait(1000)
        objVehicleEdit.vehicleAdd.verifyDefaultUIDriversTab()
        objVehicleEdit.vehicleAdd.clickAssignButton()
        //TC_218	Check click "Save" button When the driver has not been occupied
        objVehicleEdit.vehicleAdd.searchDriver("ANL-D001009")
        objVehicleEdit.vehicleAdd.verifyAssignDriver()
        objVehicleEdit.vehicleAdd.btnConfirm("Save").click()
        objVehicleEdit.common.btnConfirmHintPopupVer2("Confirm").should("be.visible")
    })

    it('', () => {
        objVehicleEdit.vehicleAdd.searchByCondition(id)
        objVehicleEdit.common.searchResultWithOutValue().dblclick({ force: true })
        objVehicle.gotoEditVehiclePage()
        //TC_219	Check complete edit vehicle when set up status of vehicle is active
        //when create vehicle: vehicle already had site, subcontractor, drivers
        //now only click edit and save
        objVehicleEdit.vehicleAdd.confirmCreateVehicle("Save", "Confirm", "yes")
        objVehicleEdit.common.msgSuccessShouldHaveText("success")
        //TC_220	Check complete edit vehicle when set up status of vehicle is inactive
        //change status to inactive -> click edit -> click save

        //TC_221	Check complete edit vehicle when click "Confirm" button on "Confirm" popup
        //open driver tab -> remove driver -> assign driver + not occupied -> save + confirm

        //TC_222	Check complete edit vehicle when click "Cancel" button on "Confirm" popup
        //open driver tab -> remove driver -> assign driver + not occupied -> save + cancel

        //TC_223	Check status of vehicle when status vehicle is active and status of assigned site is inactive
        //edit + assign inactive subcontractor
    })
})