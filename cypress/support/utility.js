export class Utility {
    getBaseUrl() {
        let envi = Cypress.env('ENV'); //Get the value of evnironment variable i.e ENV
        if (envi == 'production') //Check the value
            return "https://tms.allnowgroup.com"; //return desired url
        else if (envi == 'staging')
            return "https://tms-stg.allnowgroup.com";
    }
}