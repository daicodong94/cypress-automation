
import Common from "../common/Common";
import LeftMenu from "../fragments/LeftMenu";

class ActivityTracking {
    constructor() {
        // this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
    }

    msgSuccess(msg) {
        return cy.xpath("//p[contains(text(),'" + msg + "')]")
    }

    tabShipment() {
        return cy.xpath('//div[@id="tab-shipment"]')
    }

    txtSearch() {
        return cy.xpath("(//div[@class='search-block']//input[contains(@placeholder,'Search')])[last()]")
    }

    iconSearch() {
        return cy.xpath('(//button[@id="btn-search"])[3]')
    }

    shipmentNo(number) {
        return cy.xpath('//tr[@class="el-table__row cursor-pointer"]//span[contains(text(),"' + number + '")]')
    }

    btnUpdateShipment() {
        return cy.xpath("//span[@class='el-dropdown-link text-primary-main cursor-pointer' and @role='button']//div")
    }

    updateShipment(status) {
        return cy.xpath('(//ul[@class="el-dropdown-menu el-popper el-dropdown-menu--mini"]//li[contains(text(),"' + status + '")])[last()]')
    }

    shipmentStatus(status) {
        return cy.xpath('(//div[contains(text(),"' + status + '")])[1]')
    }

    resultsSearch(result) {
        return cy.xpath('//li//span[contains(text(),"' + result + '")]')
    }

    trackingOrderStatus(status) {
        return cy.xpath('(//div[@class="p-4 grow overflow-auto"]//div[@class="el-step__title is-finish" and contains(text(),"' + status + '")])[last()]')
    }

    orderNumber() {
        return cy.xpath('//div[contains(text(),"Parent Order:")]//following-sibling::div[@class="text-primary-main cursor-pointer"]')
    }

    jobNumber() {
        return cy.xpath('//div[contains(text(),"Parent Job:")]//following-sibling::div[@class="text-primary-main cursor-pointer"]')
    }

    columnStatus(status) {
        return cy.xpath("//td//span[contains(.,'" + status + "')]")
    }

    orderTab() {
        return cy.xpath("//div[@id='tab-order']")
    }

    txtSearchOrderTab() {
        return cy.xpath("(//input[@placeholder='Search....'])[1]")
    }

    iconSearchOrderTab() {
        return cy.xpath('(//button[@id="btn-search"])[1]')
    }

    jobTab() {
        return cy.xpath("//div[@id='tab-job']")
    }

    txtSearchJobTab() {
        return cy.xpath("(//input[@placeholder='Search....'])[2]")
    }

    iconSearchJobTab() {
        return cy.xpath('(//button[@id="btn-search"])[2]')
    }

    openRecordToUpdateDriver(order) {
        return cy.xpath("(//td[contains(.,'" + order + "')]/ancestor::tr)[1]")
    }

    navigateToShipmentTabAndSearch(waybill) {
        this.tabShipment().click({ force: true })
        this.txtSearch().click({ force: true }).type(waybill, { force: true })
        // this.txtSearch().type(waybill, { force: true })
        // cy.wait(3000/2)
        this.resultsSearch(waybill).click({ force: true })
        this.iconSearch().click({ force: true })
        this.shipmentNo(waybill).click({ force: true })
    }

    upDateShipmentStatus(status, shipmentStatus) {
        this.btnUpdateShipment().click({ force: true })
        cy.wait(1000)
        this.updateShipment(status).scrollIntoView().click({ force: true })
        this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
        cy.wait(1000)
        this.msgSuccess("Driver have been updated").should("be.visible")
        this.shipmentStatus(shipmentStatus).should("be.visible")
    }

    lblTrackHistory(status) {
        return cy.xpath("//div[contains(text(),'Tracking')]/following-sibling::div//div[contains(text(),'" + status + "')]")
    }

    viewTrackHistory(status) {
        this.lblTrackHistory(status).should('be.visible')
    }
    orderStatusFinished() {
        this.trackingOrderStatus("Order_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Order_Confirmed").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Order_Processing").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Order_Picking_Up").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Order_Ontheway").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Order_Delivered").scrollIntoView().should("be.visible")
    }

    jobStatusFinished() {
        this.trackingOrderStatus("Dispatch_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Accepted").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Picking_Up").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Ontheway").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Arrival_Destination").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Handed_Over_Receiver").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Delivered").scrollIntoView().should("be.visible")
    }

    shipmentStatusFinished() {
        this.trackingOrderStatus("Shipment_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Processing").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Dispatched").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Arrival_Pickup").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Loading").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Ontheway").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Arrival_Destination").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Unloading").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Delivered").scrollIntoView().should("be.visible")
    }

    orderSplitStatusFinished() {
        this.trackingOrderStatus("Order_Created").should("be.visible")
        this.trackingOrderStatus("Order_Confirmed").should("be.visible")
        this.trackingOrderStatus("Order_Processing").should("be.visible")
        this.trackingOrderStatus("Order_Delivered").should("be.visible")
    }

    jobSplitStatusFinished() {
        this.trackingOrderStatus("Dispatch_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Accepted").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Picking_Up").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Ontheway").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Arrival_Destination").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Handed_Over_Receiver").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Job_Delivered").scrollIntoView().should("be.visible")
    }

    shipmentSplitStatusFinished() {
        this.trackingOrderStatus("Shipment_Created").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Processing").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Arrival_Pickup").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Loading").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Ontheway").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Arrival_Destination").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Unloading").scrollIntoView().should("be.visible")
        this.trackingOrderStatus("Shipment_Delivered").scrollIntoView().should("be.visible")
    }

    verifyShipmentStatusOrder(shipmentNumber, status) {
        cy.wait(2000)
        this.tabShipment().click()
        cy.wait(2000)
        this.txtSearch().should('be.visible').click()
        this.txtSearch().realType(shipmentNumber.text().trim())
        this.resultsSearch(shipmentNumber.text().trim()).should('be.visible').click({ multiple: true, force: true })
        this.iconSearch().click()
        this.columnStatus(status).should('be.visible')
    }

    verifyOrderStatusOrder(shippingOrderNumber, status) {
        cy.wait(2000)
        this.orderTab().click()
        cy.wait(2000)
        this.txtSearchOrderTab().should('be.visible').click()
        this.txtSearchOrderTab().realType(shippingOrderNumber.text().trim())
        this.resultsSearch(shippingOrderNumber.text().trim()).should('be.visible').click({ multiple: true, force: true })
        this.iconSearchOrderTab().click()
        this.columnStatus(status).should('be.visible')
    }

    verifyJobStatusOrder(dispatchNumber, status) {
        cy.wait(2000)
        this.jobTab().click()
        cy.wait(2000)
        this.txtSearchJobTab().should('be.visible').click()
        this.txtSearchJobTab().realType(dispatchNumber.text().trim())
        this.resultsSearch(dispatchNumber.text().trim()).should('be.visible').click({ multiple: true, force: true })
        this.iconSearchJobTab().click()
        this.columnStatus(status).should('be.visible')
    }

    verifyShipmentUpdatedStatusOrder(shipmentNumber, status) {
        cy.wait(2000)
        this.tabShipment().click()
        cy.wait(2000)
        this.txtSearch().should('be.visible').click()
        this.txtSearch().realType(shipmentNumber.text().trim())
        this.resultsSearch(shipmentNumber.text().trim()).should('be.visible').click({ multiple: true, force: true })
        this.iconSearch().click()
        this.openRecordToUpdateDriver(shipmentNumber.text().trim()).dblclick()
        this.btnUpdateShipment().click()
        this.updateShipment("Update Status for Driver").click()
        this.common.btnConfirmHintPopUp("Confirm").click()
        this.shipmentStatus(status).should('be.visible')
    }

    openShipmentToUpdate(shipmentNumber) {
        cy.wait(100)
        this.tabShipment().click()
        cy.wait(500)
        this.txtSearch().should('be.visible').click()
        this.txtSearch().realType(shipmentNumber)
        this.resultsSearch(shipmentNumber).should('be.visible').click({ multiple: true, force: true })
        this.iconSearch().click()
        cy.wait(500)
        this.openRecordToUpdateDriver(shipmentNumber).dblclick()
    }

    spanTimeRequiredUpdateOrderStatus() {
        return cy.xpath('//span[@component="BaseDateTimePicker"]//div[contains(text(),"Select Date/Time")]')
    }

    selectHourAndMinutes(time, location) {
        return cy.xpath('(//div[@class="rounded-sm dropdown-container bg-white z-20 swing-in-top-fwd custom-time-dropdown absolute"]//div[contains(text(),"' + time + '")])[' + location + ']')
    }

    selectMinutes(time) {
        return cy.xpath('(//div[@class="rounded-sm dropdown-container bg-white z-20 swing-in-top-fwd custom-time-dropdown absolute"]//div[contains(text(),"' + time + '")])')
    }

    btnOK() {
        return cy.xpath('(//button//span[contains(text(),"Ok")])[last()]')
    }

    btnSubmit() {
        return cy.xpath('//button//span[contains(text(),"Submit")]')
    }

    ipInformationOrder(infor) {
        return cy.xpath('//span[contains(text(),"' + infor + '")]//ancestor::div[@class="col-span-3"]//following-sibling::input')
    }

    ipPickupTemperature() {
        return cy.xpath('//input[@placeholder="Enter Temperature"]')
    }

    divlastHour() {
        return cy.xpath("//div[@class='w-1/2 h-[226px] ps ps--active-y']//div[@class='cursor-pointer flex items-center justify-center h-[28px]'][last()]")
    }

    divlastMinutes() {
        return cy.xpath("//div[@class='w-1/2 h-[226px] selector-minute ps ps--active-y']//div[@class='cursor-pointer flex items-center justify-center h-[28px]'][last()]")
    }

    selectTime() {
        cy.wait(55000)
        cy.selectDay("today")
        cy.wait(10000)
        // var hour = new Date().getHours()
        // var minutes = String(new Date().getMinutes() + 1).padStart(2, "0")
        // cy.log("hour: " + hour)
        // cy.log("minutes: " + minutes)
        // this.selectHourAndMinutes(hour, index).click({ force: true })
        // this.selectMinutes(minutes).last().click({ force: true })
        this.divlastHour().click({ force: true })
        this.divlastMinutes().click({ force: true })
        this.btnOK().click({ force: true })
    }

    upDateOrderStatus(status, shipmentStatus) {
        this.btnUpdateShipment().click({ force: true })
        cy.wait(1000)
        this.updateShipment(status).scrollIntoView().click({ force: true })
        cy.wait(1000)
        this.spanTimeRequiredUpdateOrderStatus().click({ force: true })
        this.selectTime()
        if (shipmentStatus == "Shipment_Ontheway") {
            this.ipInformationOrder("Picked Up Weight ( Kg )").clear()
            this.ipInformationOrder("Picked Up Weight ( Kg )").type(5, { force: true })
            this.ipInformationOrder("Actual Number of Package").clear()
            this.ipInformationOrder("Actual Number of Package").type(5, { force: true })
            this.ipInformationOrder("Actual Number of Item per Package").clear()
            this.ipInformationOrder("Actual Number of Item per Package").type(5, { force: true })
            this.ipInformationOrder("Actual Volume (Square)").clear()
            this.ipInformationOrder("Actual Volume (Square)").type(5, { force: true })
            this.ipPickupTemperature().type(10)
        }
        if (shipmentStatus == "Shipment_Delivered") {
            this.ipPickupTemperature().type(10)
            this.ipInformationOrder("Damaged Quantity").clear()
            this.ipInformationOrder("Damaged Quantity").type(5, { force: true })
            this.ipInformationOrder("Actual Number of Package").last().clear()
            this.ipInformationOrder("Actual Number of Package").last().type(5, { force: true })
            this.ipInformationOrder("Actual Number of Item per Package").last().clear()
            this.ipInformationOrder("Actual Number of Item per Package").last().type(5, { force: true })
            this.ipInformationOrder("Damaged Package").clear()
            this.ipInformationOrder("Damaged Package").type(5, { force: true })
            this.ipInformationOrder("Rejected Quantity").clear()
            this.ipInformationOrder("Rejected Quantity").type(0, { force: true })
            this.ipInformationOrder("Rejected Package").clear()
            this.ipInformationOrder("Rejected Package").type(0, { force: true })
        }
        this.btnSubmit().click()
        this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
        cy.wait(1000)
        this.msgSuccess("Update driver status successfully").should("be.visible")
        cy.wait(1000)
        this.shipmentStatus(shipmentStatus).should("be.visible")
    }

    btnShipmentDelivered() {
        return cy.xpath('//p[contains(text(),"Shipment _Delivered")]//ancestor::div[@class="el-step is-horizontal is-center"]//button//div')
    }

    spanSelectDateTime(infor) {
        return cy.xpath('//span[contains(text(),"' + infor + '")]//ancestor::div[@class="col-span-4"]//following-sibling::span[@component="BaseDateTimePicker"]//div[contains(text(),"Select Date/Time")]')
    }

    spanDeliveredTime() {
        return cy.xpath('//span[contains(text(),"Delivered Date and Time")]//ancestor::div[@class="col-span-3"]//following-sibling::span[@component="BaseDateTimePicker"]//div[contains(text(),"Select Date/Time")]')
    }

    spanOnthewayDateTime() {
        return cy.xpath('//span[contains(text(),"On The Way Date and Time")]//ancestor::div[@class="col-span-3"]//following-sibling::span[@component="BaseDateTimePicker"]//div[contains(text(),"Select Date/Time")]')
    }

    updateShipmentStatusToShipmentDelivered(status, shipmentStatus, abnormal) {
        this.btnUpdateShipment().click({ force: true })
        cy.wait(1000)
        this.updateShipment(status).scrollIntoView().click({ force: true })
        cy.wait(1000)
        this.btnShipmentDelivered().should("be.visible").click({ force: true })
        this.spanSelectDateTime("Arrival Pickup Date and Time").click({ force: true })
        this.selectTime("1")
        this.spanSelectDateTime("Pickup Dock Assign Date and Time").click({ force: true })
        this.selectTime("3")
        this.spanSelectDateTime("Loading Date and Time").click({ force: true })
        this.selectTime("5")
        // Input information Ontheway
        this.spanOnthewayDateTime().click({ force: true })
        this.selectTime("7")
        // this.ipInformationOrder("Picked Up Weight ( Kg )").clear()
        // this.ipInformationOrder("Picked Up Weight ( Kg )").type(5, { force: true })
        // this.ipInformationOrder("Actual Number of Package").first().clear()
        // this.ipInformationOrder("Actual Number of Package").first().type(5, { force: true })
        // this.ipInformationOrder("Actual Number of item per Package").first().clear()
        // this.ipInformationOrder("Actual Number of item per Package").first().type(5, { force: true })
        // this.ipInformationOrder("Actual Volume (Square)").clear()
        // this.ipInformationOrder("Actual Volume (Square)").type(5, { force: true })
        this.ipPickupTemperature().first().type(10, { force: true })
        this.spanSelectDateTime("Arrival Destination Date and Time").click({ force: true })
        this.selectTime("9")
        this.spanSelectDateTime("Deliver Dock Assign Date and Time").click({ force: true })
        this.selectTime("11")
        this.spanSelectDateTime("Unloading Date and Time").click({ force: true })
        this.selectTime("13")
        // Input information Delivered
        this.spanDeliveredTime().click({ force: true })
        this.selectTime("15")
        if (abnormal == "yes") {
            this.ipPickupTemperature().last().type(80, { force: true })
        } else {
            this.ipPickupTemperature().last().type(5, { force: true })
        }
        // this.ipInformationOrder("Damaged Quantity").clear()
        // this.ipInformationOrder("Damaged Quantity").type(5, { force: true })
        // this.ipInformationOrder("Actual Number of Package").last().clear()
        // this.ipInformationOrder("Actual Number of Package").last().type(5, { force: true })
        // this.ipInformationOrder("Actual Number of Item per Package").last().clear()
        // this.ipInformationOrder("Actual Number of Item per Package").last().type(5, { force: true })
        // this.ipInformationOrder("Damaged Package").clear()
        // this.ipInformationOrder("Damaged Package").type(5, { force: true })
        // this.ipInformationOrder("Rejected Quantity").clear()
        // this.ipInformationOrder("Rejected Quantity").type(0, { force: true })
        // this.ipInformationOrder("Rejected Package").clear()
        // this.ipInformationOrder("Rejected Package").type(0, { force: true })
        this.btnSubmit().click()
        this.common.btnConfirmHintPopUp("Confirm").click({ force: true })
        cy.wait(1000)
        this.msgSuccess("Update driver status successfully").should("be.visible")
        cy.wait(1000)
        this.shipmentStatus(shipmentStatus).should("be.visible")
    }

}
export default ActivityTracking;