import HomePage from "../HomePage/HomePage"
import ForgotYourPassword from "../ForgotYourPassword/ForgotYourPassword"

class LoginStaff {

    txtEmail(){
        return cy.xpath('//input[@placeholder="Email"]')
    }

    txtPassword(){
        return cy.xpath('//input[@placeholder="Password"]')
    }

    btnLogin(){
        return cy.xpath('//button[normalize-space()="LOGIN"]')
    }

    chkRememberMe(){
        return cy.xpath('//span[@class="el-checkbox__inner"]')
    }

    btnForgotYourPassword(){
        return cy.xpath('//span[@class="text-sm flex justify-between color-1890FF"]')
    }

    lblLoginIncorrect(){
        return cy.xpath('//div[@class="el-alert__title"]')
    }

    lblEmailWrongFormat(){
        return cy.xpath('//div[@class="text-error-5 text-sm error-message"]')
    }

    aleErrorServer(){
        return cy.xpath('//p[@class="el-message__content"]')
    }

    inputEmail(email){
        this.txtEmail().type(email)
    }

    inputPassword(password){
        this.txtPassword().type(password)
    }

    clickCheckBoxRememberMe(){
        this.chkRememberMe().click()
    }

    login(email, password){
        this.inputEmail(email)
        this.inputPassword(password)
        this.btnLogin().click()
        return new HomePage()
    }

    showMessageLoginIncorrect(){
        this.lblLoginIncorrect().should('contain', 'Email or password is incorrect')
    }

    showMessageWrongFormatEmail(){
        // this.lblEmailWrongFormat().then(function ($text){
        //     cy.log($text.text().trim())
        //     if($text.text().trim().localeCompare("Invalid email format")===0){
                
        //     }
        // })
        this.lblEmailWrongFormat().should('contain', 'Invalid email format')
    }

    clickButtonLogin(){
        this.btnLogin().click()
    }

    observeButtonLogin(){
        this.btnLogin().should('be.disabled')
    }

    clearCookies(){
        cy.clearCookies(3600000)
    }

    clickButtonForgotYourPassword(){
        this.btnForgotYourPassword().click()
        return new ForgotYourPassword()
    }

    showMessageWhenErrorServer(){
        this.aleErrorServer().should('contain', 'Error occured, please try again.')
        // this.aleErrorServer().then(function ($text){
        //     cy.log($text.text().trim())
        // })
    }
}

export default LoginStaff;