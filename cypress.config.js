const { defineConfig } = require('cypress')
const { isFileExist, findFiles } = require('cy-verify-downloads')
const fs = require('fs')
const xlsx = require("node-xlsx").default;
const path = require("path");
// const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = defineConfig({
  viewportWidth: 1920,
  viewportHeight: 1080,
  defaultCommandTimeout: 80000,
  pageLoadTimeout: 120000,
  requestTimeout: 50000,
  responseTimeout: 50000,
  chromeWebSecurity: false,
  projectId: 'wcn44e',
  video: true,
  videoCompression: false,
  numTestsKeptInMemory: 0,
  screenshotOnRunFailure: true,
  screenshotsFolder: "cypress/screenshots",
  downloadsFolder: "cypress/downloads",
  trashAssetsBeforeRuns: true,
  reporter: 'cypress-mochawesome-reporter',
  retries: 0,
  chromeWebSecurity: false,
  reporterOptions: {
    reportDir: "cypress/reports",
    charts: true,
    reportPageTitle: 'TMS-Automation-report',
    embeddedScreenshots: true,
    inlineAssets: true,
    saveAllAttempts: false,
    embeddedScreenshots: true
  },
  e2e: {
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on),
        on('task', {
          log(message) {
            console.log(message)
            return null
          },
        })
      on('task', { isFileExist, findFiles });
      require('cypress-mochawesome-reporter/plugin')(on)
      on("task", {
        parseXlsx({ filePath }) {
          return new Promise((resolve, reject) => {
            try {
              const jsonData = xlsx.parse(fs.readFileSync(filePath));
              resolve(jsonData);
            } catch (e) {
              reject(e);
            }
          });
        }
      });
      on('task', {
        //   log(message) {
        //     console.log(message)
        //     return null
        //   },
        // findFiles({ msg }) {
        //   return null
        // },
        // isFileExist({ mes }) {
        //   return null
        // }
        countFiles(folderName) {
          return new Promise((resolve, reject) => {
            fs.readdir(folderName, (err, files) => {
              if (err) {
                return reject(err)
              }
              resolve(files.length)
            })
          })
        }
      })
    },
    specPattern: 'cypress/e2e/**/*.{js,jsx,ts,tsx}',
  },
})