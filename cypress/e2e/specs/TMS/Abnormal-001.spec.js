import Abnormal from "../../../support/pages/TMS/components/abnormal/Abnormal"
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import Login from "../../../support/pages/TMS/components/login/Login"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"

describe("Abnormal01", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(),
        objAbnormal = new Abnormal(), objShippingOrder = new ShippingOrder(), objCreateShippingOrder = new CreateShippingOrder(),
        objPickup = new Pickup(), objDeliverGood = new DeliveryGood(), objSchedule = new Schedule(), objActivityTracking = new ActivityTracking()
    var carrier = "SYS_DEF_INDIVIDUAL", product = "Service for Shipper", driver = "Auto Driver", surcharge = "Service for Carrier", customer = "Automation Company"
    var customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"
    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it("TC: Check flow to access Abnormal screen \n" +
        "TC: Check UI in case click on more...button \n" +
        "TC: Check UI in case click on fold button \n" +
        "TC: Check display column on the list Abnormal \n" +
        "TC: Check don't show column on the list Abnormal \n" +
        "TC: Check drag column on the list Abnormal \n" +
        "TC: Search- no result found \n" +
        "TC: Check UI Search", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            cy.wait(1000)
            objAbnormal.header.tabActive("Abnormal ").should("be.exist")
            objAbnormal.common.hideAndUnHideSearch()
            //TC: Check display column on the list Abnormal/ TC: Check don't show column on the list Abnormal/ TC: Check drag column on the list Abnormal
            objAbnormal.common.resetDefaultColumn("yes")
            objAbnormal.common.setColumn("shipping site", "delivery site")
            objAbnormal.common.enableOrDisableColumn("orderCreatorName", "no")
            //TC: Search- no result found
            cy.wait(1000)
            objAbnormal.header.btnAtHeader("more filter...").click()
            objAbnormal.common.searchBySelectValue("exception type", "other")
            objAbnormal.common.searchBySelectValue("source", "customer")
            objAbnormal.common.noData()
            objAbnormal.header.btnAtHeader("collapse filter").click()
            //TC: Check UI Search
            objAbnormal.common.hideAndUnHideSearch()
        })

    it("TC: TC: Check Reset column in case click on Sure button \n" +
        "TC: Check Reset column in case click on Cancel button \n" +
        "TC: Check close Heading setting popup in case click on X button \n" +
        "TC: Check pagination", () => {
            objAbnormal = objHome.leftMenu.navigateToPage("Abnormal")
            objAbnormal.common.resetDefaultColumn("yes")
            objAbnormal.common.resetDefaultColumn("cancel")
            objAbnormal.common.closeSetColumn()
            objAbnormal.common.goToPageNumber(3)
            objAbnormal.common.selectPageSize("200/page")
        })

    // it("TC: Verify record with exception type = 'Empty' from Implement>Pick up page", () => {
    //     cy.randomString(21).then((id) => {
    //         objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
    //         objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
    //         objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Direct Schedule", id, customer, product, "MAN", "TestT2", "TestT5", 20);
    //         cy.wait(40000)
    //         cy.reload()
    //         objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
    //         objSchedule.common.searchByClickSuggestion("dispatch number", id)
    //         objSchedule.common.searchResultWithOutValue().should("be.visible")
    //         cy.wait(1000)
    //         objSchedule.common.openDetailView()
    //         cy.wait(1000)
    //         objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
    //         objSchedule.common.msgSuccess().should("be.visible")
    //         objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
    //         cy.wait(40000)
    //         cy.reload()
    //         objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
    //         cy.wait(1000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", id)
    //         objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")
    //         cy.wait(1000)
    //         objPickup.common.openDetailView()
    //         cy.wait(1000)
    //         objPickup.chooseArrivalTimeNow()
    //         cy.wait(1000)
    //         objPickup.clickbtnFunctionPickup("empty")
    //         cy.wait(1000)
    //         objPickup.confirmPickup("yes")
    //         cy.wait(10000)
    //         cy.reload()
    //         objAbnormal = objPickup.leftMenu.navigateToPage("Abnormal")
    //         cy.wait(1000)
    //         objAbnormal.common.searchByClickSuggestion("order number", id)
    //         objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
    //         objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
    //         objAbnormal.pickup.dispatchPickupStatus("empty").last().should("be.visible")
    //     })
    // })

    // it("TC: Verify record with exception type = 'Reject' from Implement>Deliver goods page", () => {
    //     cy.randomString(21).then((id) => {
    //         objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
    //         objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
    //         objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Direct Schedule", id, customer, product, "MAN", "TestT2", "TestT5", 15);
    //         cy.wait(40000)
    //         cy.reload()
    //         objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
    //         objSchedule.common.searchByClickSuggestion("dispatch number", id)
    //         objSchedule.common.searchResultWithOutValue().should("be.visible")
    //         cy.wait(1000)
    //         objSchedule.common.openDetailView()
    //         cy.wait(1000)
    //         objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
    //         objSchedule.common.msgSuccess().should("be.visible")
    //         objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
    //         cy.wait(40000)
    //         cy.reload()
    //         objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
    //         cy.wait(1000)
    //         objPickup.common.searchByClickSuggestion("dispatch number", id)
    //         objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")
    //         cy.wait(1000)
    //         objPickup.common.openDetailView()
    //         cy.wait(1000)
    //         objPickup.chooseCompletionTime(-5)
    //         cy.wait(1000)
    //         objPickup.clickbtnFunctionPickup("pickup completed")
    //         cy.wait(1000)
    //         objPickup.confirmPickup("yes")
    //         cy.reload()
    //         cy.wait(40000)
    //         objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
    //         cy.wait(1000)
    //         objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
    //         cy.wait(1000)
    //         objDeliverGood.common.openDetailView()
    //         cy.wait(1000)
    //         var filePath = "implement\\images.png"
    //         objDeliverGood.common.addFileAttachments(filePath)
    //         objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
    //         cy.wait(1000)
    //         objDeliverGood.pickup.clickbtnFunctionPickup("reject")
    //         cy.wait(1000)
    //         objDeliverGood.btnConfirmReturnShipping().click({ force: true })
    //         objDeliverGood.common.btnConfirmHintPopUp("confirm").click({ force: true })
    //         objDeliverGood.common.msgSuccess().should("be.visible")
    //         cy.wait(10000)
    //         cy.reload()
    //         objAbnormal = objDeliverGood.leftMenu.navigateToPage("Abnormal")
    //         cy.wait(1000)
    //         objAbnormal.common.searchByClickSuggestion("order number", id)
    //         objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
    //         objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
    //         objAbnormal.pickup.dispatchPickupStatus("reject").last().should("be.visible")
    //     })
    // })

    it("TC: Verify record with exception type = 'delivery late' from Implement>Pickup page", () => {
        cy.randomString(11).then((id) => {
            // objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            // objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderPickupNow("Direct Schedule", id, customer, product, "Chill", "AutoTestSite99", "SiteTestAuto", 5);
            // cy.wait(40000)
            cy.reload()
            // objShippingOrder.common.msgSuccess().should("be.visible")
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, surcharge, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            // cy.wait(40000)
            cy.reload()
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(-2)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.confirmPickup("yes")
                cy.reload()
                // cy.wait(40000)
                objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                cy.wait(1000)
                objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
                cy.wait(1000)
                objDeliverGood.common.openDetailView()
                // var filePath = "implement\\images.png"
                // objDeliverGood.common.addFileAttachments(filePath)
                objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
                cy.wait(1000)
                objDeliverGood.pickup.clickbtnFunctionPickup("Delivery completed")
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objAbnormal = objDeliverGood.common.redirectToAbnormalByClickHandleException()
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("delivery late").last().should("be.visible")
            })
        })
    })

    it("TC: Verify record with exception type = 'pick up late' from Implement>Pickup page", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer2("Direct Schedule", id, customer, product, temperature, siteB, siteC, -10, -5);
            // cy.wait(40000)
            // cy.reload()
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            // cy.wait(40000)
            // cy.reload()
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")
            // cy.wait(1000)
            // objPickup.common.openDetailView()
            // cy.wait(1000)
            // objPickup.chooseCompletionTime(-2)
            // cy.wait(1000)

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(-2)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objPickup.common.btnConfirmHintPopUp("confirm").click()
                objAbnormal = objPickup.common.redirectToAbnormalByClickHandleException()
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("pick up late").last().should("be.visible")
            })
        })
    })

    it("TC: Verify record with exception type = 'delivery over temperature' from Implement> Deliver goods page", () => {
        cy.randomString(7).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Direct Schedule", id, customer, product, "BA", siteB, siteD, 5);
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(0)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.confirmPickup("yes")
                objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                cy.wait(1000)
                objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
                cy.wait(1000)
                objDeliverGood.common.openDetailView()
                cy.wait(1000)
                // var filePath = "implement\\images.png"
                // objDeliverGood.common.addFileAttachments(filePath)
                objDeliverGood.inputReceiptTemperature(65)
                objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
                cy.wait(1000)
                objDeliverGood.pickup.clickbtnFunctionPickup("Delivery completed")
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                objAbnormal = objPickup.common.redirectToAbnormalByClickHandleException()
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("delivery over temperature").last().should("be.visible")
            })
        })
    })

    it("TC: Verify record with exception type = 'cargo damage' from Implement>Deliver goods page", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("Direct Schedule", id, customer, product, "MAN", siteB, siteC, 5);
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(0)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.confirmPickup("yes")
                objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                cy.wait(1000)
                objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
                cy.wait(1000)
                objDeliverGood.common.openDetailView()
                cy.wait(1000)
                objDeliverGood.inputShippingInfor("Damaged item quantity", 1)
                objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
                cy.wait(1000)
                objDeliverGood.pickup.clickbtnFunctionPickup("Delivery completed")
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                objAbnormal = objPickup.common.redirectToAbnormalByClickHandleException()
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("cargo damage").last().should("be.visible")
            })
        })
    })

    it("TC: Verify record with exception type = 'Lost' from Implement>Deliver goods page", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderVer3("Direct Schedule", id, customer, product, "MAN", siteA, siteD, 0, 5);
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(1000)
            objSchedule.common.openDetailView()
            cy.wait(1000)
            objSchedule.selectDataAndSchedule(carrier, driver, "confirm and dispatch")
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
            objPickup = objSchedule.leftMenu.navigateToSubPage("Implement", "Pick Up")
            cy.wait(1000)
            objPickup.common.searchByClickSuggestion("dispatch number", id)
            objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")

            //open detail view and get Waybill number: this.waybill
            objPickup.common.openDetailView()
            objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
                var waybill2 = wayBillNumber.trim().substring(15)
                cy.log("waybillNumber = " + waybill2)
                cy.reload()
                ////must go to activity tracking -> update status 2 times
                objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
                objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
                cy.wait(1000)
                objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
                //back to Pick up to pickup completed
                objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
                cy.wait(1000)
                objPickup.common.searchByClickSuggestion("waybill number", id)
                objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
                objPickup.common.openDetailView()
                cy.wait(1000)
                objPickup.chooseCompletionTime(0)
                cy.wait(1000)

                objPickup.clickbtnFunctionPickup("pickup completed")
                cy.wait(1000)
                objPickup.confirmPickup("yes")
                objDeliverGood = objPickup.leftMenu.navigateToSubPage("Implement", "Deliver Goods")
                cy.wait(1000)
                objDeliverGood.common.searchByClickSuggestion("please enter the dispatch number", id)
                cy.wait(1000)
                objDeliverGood.common.openDetailView()
                cy.wait(1000)
                objDeliverGood.inputShippingInfor("Quantity of goods received", 1)
                objDeliverGood.inputShippingInforTime("Delivery completion time", 0)
                cy.wait(1000)
                objDeliverGood.pickup.clickbtnFunctionPickup("Delivery completed")
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                cy.wait(1000)
                objDeliverGood.common.btnConfirmHintPopUp("confirm").click()
                objAbnormal = objPickup.common.redirectToAbnormalByClickHandleException()
                objAbnormal.pickup.dispatchPickupStatus("new").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("system").should("be.visible")
                objAbnormal.pickup.dispatchPickupStatus("lost").last().should("be.visible")
            })
        })
    })

    // it.only("abc", () => {
    //     var id = "D002300003822T1"
    //     //D002300003991T1
    //     objPickup = objHome.leftMenu.navigateToSubPage("Implement", "Pick Up")
    //     objPickup.common.searchByClickSuggestion("waybill number", id)
    //     objPickup.dispatchPickupStatus("SHIPMENT_DISPATCHED").should("be.visible")
    //     // cy.wait(1000)
    //     // objPickup.common.openDetailView()
    //     // cy.wait(1000)
    //     // objPickup.chooseCompletionTime(0)
    //     // cy.wait(1000)

    //     //open detail view and get Waybill number: this.waybill
    //     objPickup.common.openDetailView()
    //     objPickup.common.wayBillNumber().invoke('text').then((wayBillNumber) => {
    //         var waybill2 = wayBillNumber.trim().substring(15)
    //         cy.log("waybillNumber = " + waybill2)
    //         cy.reload()
    //         ////must go to activity tracking -> update status 2 times
    //         objActivityTracking = objPickup.leftMenu.navigateToPage("Activity Tracking")
    //         objActivityTracking.navigateToShipmentTabAndSearch(waybill2)
    //         objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Arrival_Pickup")
    //         cy.wait(1000)
    //         objActivityTracking.upDateShipmentStatus("Update driver", "Shipment_Loading")
    //         //back to Pick up to pickup completed
    //         objPickup = objActivityTracking.leftMenu.navigateToSubPage("Implement", "Pick Up")
    //         cy.wait(1000)
    //         objPickup.common.searchByClickSuggestion("waybill number", id)
    //         objPickup.dispatchPickupStatus("SHIPMENT_LOADING").should("be.visible")
    //         objPickup.common.openDetailView()
    //         cy.wait(1000)
    //         objPickup.chooseCompletionTime(0)
    //         cy.wait(1000)

    //         objPickup.clickbtnFunctionPickup("pickup completed")
    //         cy.wait(1000)
    //         objPickup.confirmPickup("yes")
    //         cy.reload()
    //     })
    // })

})