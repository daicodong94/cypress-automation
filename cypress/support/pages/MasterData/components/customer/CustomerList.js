
import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
// import SiteList from "../site/SiteList";
import CustomerDetail from "./CustomerDetail";
import CustomerImportFile from "./CustomerImportFile";
import CustomerExportFile from "./CustomerExportFile";
import CustomerAdd from "./CustomerAdd";
import CustomerEdit from "./CustomerEdit";
import LeftMenu from "../../../TMS/components/fragments/LeftMenu";
class CustomerList {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
    }

    pageTitle(title) {
        return cy.xpath("//div[@id='title-container']//span[text()='" + title + "']")
    }

    pageTitleShouldBe(title) {
        this.pageTitle(title).should("be.visible")
    }

    txtCustomerTitle() {
        return cy.xpath("//span[@class='text-xl font-medium text-primary-85' and text()='Customer']")
    }

    txtSearch() {
        return cy.xpath("//input[@id='input-search']")
    }

    btnSearch() {
        return cy.xpath("//button[@id='btn-search']")
    }

    btnFilter() {
        return cy.xpath("//span[normalize-space()='Filter']")
    }

    btnImport() {
        return cy.xpath("//span[normalize-space()='Import']")
    }

    btnExport() {
        return cy.xpath("//span[normalize-space()='Export']")
    }

    btnCreate() {
        return cy.xpath("//button[@class='el-button el-button--primary button-st el-button--mini']")
    }

    btnViewHistoryLog() {
        return cy.xpath("//span[normalize-space()='View History Log']")
    }

    txtColumnName(columnName) {
        return cy.xpath("//th[@rowspan=1]//div[@class='cell' and text()='" + columnName + "']")
    }

    iconPen(index) {
        return cy.get("svg[class*='cursor-pointer']")
    }

    sliceBar(direction) {
        return cy.xpath("//div[@class='el-table__body-wrapper is-scrolling-" + direction + "']")
    }

    txtResult(column) {
        return cy.xpath("//tr[@class='el-table__row']//p[@class='el-tooltip' and text()='" + column + "']")
    }

    txtNoData() {
        return cy.xpath("//div[text()='No Data']")
    }

    txtPage(page) {
        return cy.xpath("//li[@class='number' and text()='" + page + "']")
    }

    txtFilter(column) {
        return cy.xpath("//span[text()='" + column + "']//following-sibling::div")
    }

    ddlFilter(columnName) {
        return cy.xpath("//span[text()='" + columnName + "']//following-sibling::div//child::div")
    }

    lstFilter(filter) {
        return cy.xpath("//span[text()='" + filter + "']")
    }

    btnResetFilter() {
        return cy.xpath("//span[normalize-space()='Reset filter']")
    }

    iconPoint() {
        return cy.get("circle[cx='4']")
    }

    btnDescending(column) {
        return cy.xpath("//div[@class='cell' and contains(., '" + column + "')]//child::span[@class='caret-wrapper']//child::i[@class='sort-caret descending']")
    }

    btnAscending(column) {
        return cy.xpath("//div[@class='cell' and contains(., '" + column + "')]//child::span[@class='caret-wrapper']//child::i[@class='sort-caret ascending']")
    }

    ddlPagination() {
        return cy.xpath("//input[@placeholder='Select']")
    }

    lstPagination(page) {
        return cy.xpath("//span[.='" + page + "/page']")
    }

    totalRow() {
        return cy.xpath("//tr[@class='el-table__row']")
    }

    columnStatus(){
        return cy.xpath("//td[14]//span")
    }

    txtGoTo() { return cy.xpath("//input[@type='number']") }
    txtSetStatus() { return cy.xpath("//span[text()='Set status']") }
    radioBtnSetStatus(button) { return cy.xpath("//span[@class='el-radio__label' and contains(., '" + button + "')]//preceding-sibling::span") }
    btnCancelStatus() { return cy.xpath("//button[contains(@class,'el-button el-button--default el-button--mini')]//span[contains(text(),'Cancel')]") }
    btnSetStatus() { return cy.xpath("//button[contains(@class,'el-button el-button--primary el-button--mini')]") }
    msgUpdatedSuccess() { return cy.xpath("//p[contains(., 'status has been updated')]") }
    btnCancelQuit() { return cy.xpath("//button[contains(@class,'el-button el-button--default el-button--small')]//span[contains(text(),'Cancel')]") }
    btnConfirmQuit() { return cy.xpath("//span[normalize-space()='Confirm']") }
    txtHistory() { return cy.xpath("//span[text()='History']") }
    txtInTable() { return cy.xpath("//div[@class='cell']//p[@class='el-tooltip']") }

    isDefaultDataDisplayed() {
        this.txtCustomerTitle().should("be.visible")
        this.btnSearch().should("be.visible")
        this.txtSearch().should("be.visible")
        this.btnFilter().should("be.visible")
        this.btnImport().should("be.visible")
        this.btnExport().should("be.visible")
        this.btnCreate().should("be.visible")
        this.btnViewHistoryLog().should("be.visible")
        this.txtColumnName("Customer ID").should("be.visible")
        this.txtColumnName("Created Date").should("be.visible")
        this.txtColumnName("Updated Date").should("be.visible")
        this.txtColumnName("Company Tax ID").should("be.visible")
        this.txtColumnName("Company Registered Name").should("be.visible")
        this.txtColumnName("Company Registered Name").should("be.visible")
        this.txtColumnName("Company Name (TH)").should("be.visible")
        this.txtColumnName("Company Name (EN)").should("be.visible")
        this.sliceBar("left").scrollTo('right', { easing: 'linear' })
        this.txtColumnName("Payment Type").should("be.visible")
        this.txtColumnName("Customer Grouping L1").should("be.visible")
        this.txtColumnName("Customer Grouping L2").should("be.visible")
        this.txtColumnName("Province").should("be.visible")
        this.txtColumnName("Status").should("be.visible")
        cy.wait(2000)
    }

    searchByValue(searchData) {
        this.txtSearch().clear()
        this.txtSearch().type(searchData)
        this.btnSearch().click({ force: true })
        cy.wait(3000)
        this.txtResult(searchData).each((item) => { cy.wrap(item).should("contain.text", searchData) })
    }

    searchByNotMatchValue(searchData) {
        this.txtSearch().clear()
        this.txtSearch().type(searchData)
        this.btnSearch().click({ force: true })
        cy.wait(3000)
        this.txtNoData().should("be.visible")
        this.txtResult(searchData).should("not.exist")
    }

    searchDefault() {
        this.txtSearch().clear()
        this.btnSearch().click({ force: true })
        cy.wait(3000)
        this.txtNoData().should("not.exist")
    }

    searchByValueEnter(searchData) {
        this.txtSearch().clear()
        this.txtSearch().type('{enter}')
        this.btnSearch().click({ force: true })
        cy.wait(3000)
        this.txtResult(searchData).each((item) => { cy.wrap(item).should("contain.text", searchData) })
    }

    navigateToPage(page) {
        this.txtPage(page).click({ force: true })
    }

    clickToButtonFilter() {
        this.btnFilter().click({ force: true })
        cy.wait(2000)
    }

    selectFilterDropdownList(columnName, filter) {
        this.ddlFilter(columnName).first().click({ force: true })
        cy.wait(2000)
        this.lstFilter(filter).first().click({ force: true })
        cy.wait(1000)
    }

    clickButtonRestFilter() {
        this.btnResetFilter().click({ force: true })
        cy.wait(5000)
    }

    sort(column, option) {
        if (option == "descending") {
            this.btnDescending(column).click({ force: true })
        }
        else if (option == "asending") {
            this.btnAscending(column).click({ force: true })
        }
    }

    clickToJumpPage(direction) {
        if (direction == "next") {
            this.common.btnJumpPageNext().click({ force: true })
        }
        else if (direction == "previous") {
            this.common.btnJumpPagePrev().click({ force: true })
        }
    }

    selectPagination(amount) {
        this.ddlPagination().click({ force: true })
        this.lstPagination(amount).click({ force: true })
        this.totalRow().should("have.length", amount)

    }

    inputPageNumberGoTo(pageNumber) {
        this.txtGoTo().clear()
        this.txtGoTo().type(pageNumber)
        this.txtGoTo().type('{enter}')
        this.common.pageActive().should("have.text", pageNumber)
    }

    clickToIconPen(status, index) {
        this.iconPen(index).click({ force: true })
        cy.wait(1000)
        this.txtSetStatus().should("be.visible")

    }

    selectStatusRadioButton(status) {
        if (status == "Active") {
            this.radioBtnSetStatus("Inactive").click({ force: true })
            this.radioBtnSetStatus(status).click({ force: true })
        }
        else if (status == "Inactive") {
            this.radioBtnSetStatus("Active").click({ force: true })
            this.radioBtnSetStatus(status).click({ force: true })
        }
    }

    cancelOrSetStatus(condition) {
        if (condition == "Cancel") {
            this.btnCancelStatus().click({ force: true })
        }
        else if (condition == "Set") {
            this.btnSetStatus().click({ force: true })
        }
    }

    clickQuitSetting(condition) {
        this.btnCancelQuit().should("be.visible")
        this.btnConfirmQuit().should("be.visible")
        if (condition == "Cancel") {
            this.btnCancelQuit().click({ force: true })
        }
        if (condition == "Confirm") {
            this.btnConfirmQuit().click({ force: true })
        }
    }

    dblClickItemInListCustomer() {
        this.txtInTable().first().dblclick({ force: true })
        return new CustomerDetail()
    }

    clickBtnFile(button) {
        if (button == "Import") {
            this.btnImport().click({ force: true })
            return new CustomerImportFile()
        }
        if (button == "Export") {
            this.btnExport().click({ force: true })
            return new CustomerExportFile()
        }
    }

    clickCreateButton() {
        this.btnCreate().click({ force: true })
        return new CustomerAdd()
    }

    clickEditButton() {
        this.common.btnAction("Edit").click({ force: true })
        return new CustomerEdit()
    }
}
export default CustomerList