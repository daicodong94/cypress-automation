// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
require('cy-verify-downloads').addCustomCommand();
// require('@reportportal/agent-js-cypress/lib/commands/reportPortalCommands');


Cypress.Commands.add('back', () => {
    cy.go('back')
})

Cypress.Commands.add('setText', (locator, text) => {
    locator.scrollIntoView()
    locator.click({ force: true })
    locator.clear({ force: true })
    locator.type(text)
})

Cypress.Commands.add('checkElement', (locator) => {
    cy.get('body').then($body => {
        if ($body.find(locator).length > 0) {
            //evaluates as true if button exists at all
            locator.then($state => {
                if ($state.is(':visible')) {
                    //you get here only if button EXISTS and is VISIBLE
                    locator.should("be.visible")
                    cy.log("Element exist and visible")
                } else {
                    //you get here only if button EXISTS but is INVISIBLE
                    locator.should("be.exist")
                    cy.log("Element exist but not visible")
                }
            })
        } else {
            //you get here if the button DOESN'T EXIST
            cy.log("Element doesn't exist")
            assert.isOk('everything', 'everything is OK');
        }
    })
})

Cypress.Commands.add('randomString', (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
})

Cypress.Commands.add('randomNumber', (length) => {
    var result = '';
    var number = '123456789';
    var numberLength = number.length;
    for (var i = 0; i < length; i++) {
        result += number.charAt(Math.floor(Math.random() *
            numberLength));
    }
    return result;
})

Cypress.Commands.add('randomStringAndNumber', (length) => {
    var result = '';
    var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    var strLength = str.length;
    for (var i = 0; i < length; i++) {
        result += str.charAt(Math.floor(Math.random() *
            strLength));
    }
    return result;
})

//format MMMM-YY-dd
Cypress.Commands.add('addDays', (date, days) => {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result.toISOString().slice(0, 10);
})

Cypress.Commands.add('lessDays', (date, days) => {
    var result = new Date(date);
    result.setDate(result.getDate() - days);
    return result.toISOString().slice(0, 10);
})

Cypress.Commands.add("parseXlsx", (inputFile) => {
    return cy.task('parseXlsx', { filePath: inputFile })
});

//drag and drop native
Cypress.Commands.add('dndNative', (source, target, pixel) => {
    var dataTransfer = new DataTransfer()
    function inner(obj) {
        cy.get(source).trigger("dragstart", { dataTransfer });
        cy.get(target).trigger("dragover", obj);
        cy.get(target).trigger("drop", { dataTransfer });
        cy.get(source).trigger("dragend");
    }
    if (pixel) {
        cy.get(target).then(($el) => {
            var x1 = $el[0].getBoundingClientRect().left;
            var x2 = $el[0].getBoundingClientRect().width;
            var xc = x1 + x2 / 2;
            var y1 = $el[0].getBoundingClientRect().top;
            var y2 = $el[0].getBoundingClientRect().height;
            var yc = y1 + y2 / 2;
            inner({
                clientX: xc,
                clientY: yc,
            });
        });
    } else {
        inner(null);
    }
})

Cypress.Commands.add('checkFormatDay', (dateVal) => {
    var dateVal = dateVal;
    if (dateVal == null)
        return false;
    var validatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
    var dateValues = dateVal.match(validatePattern);
    if (dateValues == null)
        return false;
    var dtYear = dateValues[1];
    var dtMonth = dateValues[3];
    var dtDay = dateValues[5];
    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return cy.log("Fail");
    }
    return cy.log("Pass: " + dateVal + " exactly format yyyy/MM/dd");
})


Cypress.Commands.add("checkFormatDate", (date, dayChecked) => {
    var d = date.getDate();
    var m = date.getMonth() + 1; //Month from 0 to 11
    var y = date.getFullYear();
    var h = date.getHours()
    var min = date.getMinutes()
    var day = '' + y + '/' + (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '   ' + (h <= 9 ? '0' + h : h) + ':' + (min <= 9 ? '0' + min : min)
    assert(dayChecked == day, "Selected date:  " + dayChecked)
    assert(dayChecked == day, "Effective date  " + day)
    assert(dayChecked == day, "Selected date is the same with effective date")
})

Cypress.Commands.add("checkFormatDateNormal", (date, dayChecked) => {
    var d = date.getDate();
    var m = date.getMonth() + 1; //Month from 0 to 11
    var y = date.getFullYear();
    var day = '' + y + '/' + (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d)
    assert(dayChecked == day, "Selected date:  " + dayChecked)
    assert(dayChecked == day, "Effective date  " + day)
    assert(dayChecked == day, "Selected date is the same with effective date")
})

// Cypress.Commands.add("getText", (string) => {
//     string.then(($text) => {
//         return $text.text().trim()
//     })
// })


Cypress.Commands.add("getCurrDate", (type) => {
    var currentdate = new Date();
    var hours = String(currentdate.getHours()).padStart(2, "0");
    var minutes = String(currentdate.getMinutes()).padStart(2, "0");
    var month = String(currentdate.getMonth()).padStart(2, "0");
    var day = String(currentdate.getDate()).padStart(2, "0");
    var dateTime

    // YYYY-MM-DD HH:mm
    if (type == "hyphen") {
        dateTime = currentdate.getFullYear() + "-"
            + (parseInt(month) + 1) + "-"
            + day + " "
            + hours + ":"
            + minutes

        // YYYY/MM/DD HH:mm
    } else if (type == "slash") {
        dateTime = currentdate.getFullYear() + "/"
            + (parseInt(month) + 1) + "/"
            + day + " "
            + hours + ":"
            + minutes
    }

    return dateTime;
})

require('cy-verify-downloads').addCustomCommand();

import 'cypress-file-upload';
import 'cypress-iframe';

import CommonMasterData from './pages/MasterData/components/common/CommonMasterData';
import DateUtils from './pages/MasterData/components/utility/DateUtils';

var commonMasterData = new CommonMasterData();
var dateUtils = new DateUtils();

Cypress.Commands.add("selectYear", (inputtedYear, plus) => {
    console.log("Inputted year is " + inputtedYear)
    // const currYear = new Date().getFullYear()

    commonMasterData.spanCurrentYear().then(($year) => {
        var actualYear = $year.text().trim()

        if (inputtedYear == "thisYear") {
            inputtedYear = actualYear
        }
        var year = parseInt(inputtedYear) + plus
        cy.log("Yearrrrrrrrr: " + year)

        if (actualYear == year) {
            cy.log(inputtedYear + " year is selected!")
            return
        } else {
            if (year < actualYear) {
                commonMasterData.btnNavigateMonthYear("Previous Year").click({ force: true })
            } else if (year > actualYear) {
                commonMasterData.btnNavigateMonthYear("Next Year").click({ force: true })
            }
        }
        cy.selectYear(inputtedYear, plus)
    })
})

Cypress.Commands.add("selectMonth", (inputtedMonth) => {
    console.log("Inputted month is " + inputtedMonth)
    let currMonth = new Date().getMonth() + 1
    let giveMonth = dateUtils.getMonthIndexFromName(currMonth)

    commonMasterData.spanCurrentMonth().then(($month) => {
        var actualMonth = $month.text().trim()
        if (actualMonth == inputtedMonth || inputtedMonth == "thisMonth") {
            cy.log(inputtedMonth + " month is selected!")
            return
        } else {
            if (giveMonth < actualMonth) {
                commonMasterData.btnNavigateMonthYear("Previous Month").click({ force: true })
            } else if (giveMonth > actualMonth) {
                commonMasterData.btnNavigateMonthYear("Next Month").click({ force: true })
            }
        }
        cy.selectMonth(inputtedMonth)
    })
})

Cypress.Commands.add("selectDay", (inputtedDay) => {
    let currDay = new Date().getDate()
    if (inputtedDay == "today") {
        inputtedDay = currDay
    }
    // inputtedDay == "today" ? currDay : inputtedDay
    console.log("Today is " + inputtedDay)
    commonMasterData.divAvailableDate().contains(inputtedDay).first().click({ force: true })

    // if (inputtedDay == "today") {
    //     inputtedDay = currDay
    //     commonMasterData.divAvailableDate().eq(inputtedDay - 1).click({ force: true })
    // } else {
    //     commonMasterData.divAvailableDate().eq(inputtedDay - 1).click({ force: true })
    // }
})

//clear text field then type value into textfield
Cypress.Commands.add("clearThenType", { prevSubject: true }, (subject, text) => {
    cy.wrap(subject).clear({ force: true }).type(text);
})

Cypress.Commands.add("clearThenType", { prevSubject: true }, (subject, text) => {
    cy.wrap(subject).clear().type(text);
});

// Cypress.Commands.add("typeValueIntoField", {prevSubject: true}, (locator, condition) => {
//     var characters = condition.split("")
//     // var characters = this.trimString(condition)
//     // locator().last().click({ force: true })
//     for (let index = 0; index < characters.length; index++) {
//         // const element = array[index];
//         cy.xpath(locator).last().type(characters[index], { force: true })
//         cy.wait(100)
//     }

    // const chartValues = [150, 250, 350, 450, 550, 750, 900];
    // for (const value of chartValues) {
    //     locator().click(value, chartValue1).then(() => {
    //         mainElementsPage.mainSideBarHeader()
    //             .should("not.include.text", "(pon.)")
    //             .should("not.include.text", "(wt.)")
    //             .should("not.include.text", "(śr.)");
    //     });
    // }
// });