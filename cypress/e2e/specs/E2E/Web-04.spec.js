import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"
import DispatchOrderTracking from "../../../support/pages/TMS/components/track/DispatchOrderTracking"
import DriverReceipt from "../../../support/pages/TMS/components/receipt/DriverReceipt"
import UserOrganization from "../../../support/pages/TMS/components/our/UserOrganization"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"

describe("Flow to complete order in TMS when Schedule Order with Automatically Add", () => {
    var data, objLogin = new LoginSupplier(), objHome = new Homepage(), objDeliveryPlan = new DeliveryPlan(),
        objShippingOrder = new ShippingOrder(), objSchedule = new Schedule(), objActivityTracking = new ActivityTracking()
    var objDispatchOrderTracking = new DispatchOrderTracking(),
        objDriverReceipt = new DriverReceipt(), objUserOrganization = new UserOrganization(),
        objCreateShippingOrder = new CreateShippingOrder()
    var filePath = "implement/images.png"
    var temperature = "Chill", cusName = "Automation Company", surcharge = "Service for Shipper", url = new Utility().getBaseUrl()

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            cy.visit(url)
            objLogin = new LoginSupplier()
            objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
        })
    })

    it('WEB-04: Flow to complete order in TMS when Schedule Order with Automatically Add', () => {
        objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
        objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()

        //9. Login TMS > Go to Shipping Order > Create New Order1 from SiteA to SiteB > Click Confirm
        cy.log("9. Login TMS > Go to Shipping Order > Create New Order1 from SiteA to SiteB > Click Confirm")
        cy.randomString(8).then((trackingNum) => {
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder404("Confirm", trackingNum, cusName, surcharge, temperature, "TestT1", "TestT2", 5)
            //Verify status order
            cy.wait(300)
            objShippingOrder.verifyOrderHasJustCreate(trackingNum)
            objShippingOrder.common.columnShippingOrderNumber().should('be.visible').then(($shippingOrderNumber) => {
                cy.wait(300)
                objShippingOrder.leftMenu.closeTab()

                //10.1 Click Inquire > Select Order1 > Create a dispatch Order
                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                cy.log("10.1 Click Inquire > Select Order1 > Create a dispatch Order")
                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum, "first")
                objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })

                //10.1 Click Inquire > Select Order1 > Create a dispatch Order 
                objDeliveryPlan.columnShipmentOrder().first().should('be.visible').then(($shipmentNumber) => {
                    var shipmentOrder1 = $shipmentNumber.text().trim()
                    objDeliveryPlan.header.btnAtHeader("create a dispatch order ").click()
                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackingNum, "second")
                    objDeliveryPlan.columnDispatchNumber().then(($dispatchNumber) => {
                        cy.reload()

                        cy.log("11. Go to Shipping Order > Create New Order2  from SiteB to SiteA > Click Confirm")
                        //11. Go to Shipping Order > Create New Order2  from SiteB to SiteA> Click Confirm
                        cy.randomString(7).then((trackingNum2) => {
                            objShippingOrder = objDeliveryPlan.leftMenu.navigateToPage("Shipping order")
                            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                            objShippingOrder = objCreateShippingOrder.createNewShippingOrder404("Confirm", trackingNum2, cusName, surcharge, temperature, "TestT2", "TestT1", 5)
                            objShippingOrder.verifyOrderHasJustCreate(trackingNum2)
                            objShippingOrder.common.columnShippingOrderNumber().then(() => {
                                objShippingOrder.leftMenu.closeTab()

                                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", trackingNum2, "first")
                                objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })
                                objDeliveryPlan.columnShipmentOrder().first().should('be.visible').then(($shipmentNumber2) => {
                                    var shipmentOrder2 = $shipmentNumber2.text().trim()
                                    cy.log("11.1 Click Inquire > Click Automatically Add  button")
                                    //11.1 Click Inquire > Click Automatically Add  button 
                                    objDeliveryPlan.header.btnAtHeader("automatically add").click()
                                    objDeliveryPlan.common.btnConfirmHintPopUp("confirm").click()
                                    cy.wait(1000)

                                    cy.reload()

                                    //12. On the Disptach Order > Select Dispatch Number of Order12 > Click Confirm
                                    cy.log("12. On the Disptach Order > Select Dispatch Number of Order12 > Click Confirm")
                                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", trackingNum, "second")
                                    objDeliveryPlan.common.searchResultWithOutValue().last().click({ force: true })
                                    objDeliveryPlan.header.btnAtHeader("Confirm").last().click({ force: true })
                                    cy.wait(1000)

                                    //9. Login DriverA in DriverApp successfully
                                    var apiLogin = "https://account-stg.allnowgroup.com/mobile/driver-login"
                                    var driverID = "", accessToken = ""
                                    var body1 = {
                                        "countryCode": 66,
                                        "phone": "964539242",
                                        "password": "20000412",
                                        "accessOtherDevice": true,
                                        "deviceId": "b791071c2d28be26"
                                    }
                                    const options = {
                                        method: 'POST',
                                        url: apiLogin,
                                        body: body1
                                    }
                                    cy.request(options).then((response) => {
                                        expect(response.status).equal(200)
                                        driverID = response.body.items.user.driverId
                                        accessToken = response.body.items.token.access_token
                                        cy.log("driverID1 is: " + driverID, "accessToke is: " + accessToken)

                                        objDeliveryPlan.leftMenu.closeTab()

                                        //13. Go to Schedule > View Detail Dispatch Number of Order12  > Assign DriverA&VehicleA > Click Confirm and Dispatch
                                        cy.log("13. Go to Schedule > View Detail Dispatch Number of Order12  > Assign DriverA&VehicleA > Click Confirm and Dispatch")
                                        objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                                        objSchedule.common.searchByClickSuggestion("Dispatch Number", $dispatchNumber.text().trim())
                                        cy.wait(1000)

                                        objSchedule.columnDispatchNumber().then((dispatchNumber) => {
                                            var dispatch = dispatchNumber.text().trim()

                                            //13. Go to DriverA in DriverApp again > Click Accept
                                            //verify fmctoken
                                            var authorize = accessToken
                                            var fcmTokenLink = "" + url + "/wapi/api/v1/notification/fcmtoken"
                                            var bodyFMCToken = {
                                                "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                                                "driverID": driverID,
                                                "deviceID": "b791071c2d28be26",
                                                "createdDatetime": "2023-03-21T16:14:45.073Z"
                                            }
                                            const options2 = {
                                                method: 'POST',
                                                url: fcmTokenLink,
                                                headers: { authorization: authorize },
                                                body: bodyFMCToken
                                            }
                                            cy.log("options2 is: " + JSON.stringify(options2))
                                            cy.request(options2).then((response) => {
                                                expect(response.status).equal(200)
                                            })

                                            objSchedule.common.openDetailView()
                                            cy.wait(1000)
                                            objSchedule.selectDataAndSchedule("O4HS1EB", "Service for Carrier", "DriverCCWOCRYBES", "Confirm and Dispatch")
                                            objSchedule.common.msgSuccess().should("be.visible")

                                            //check organization
                                            objUserOrganization = objSchedule.leftMenu.navigateToSubPage("Master Data", "User Organization")
                                            objUserOrganization.checkUserWeb04E2E("964539242", "DriverCCWOCRYBES")
                                            cy.wait(300)
                                            objUserOrganization.leftMenu.closeTab()

                                            //Call API pending message
                                            var pendingMessage = "" + url + "/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                                            const optionsPendingMessage = {
                                                method: 'GET',
                                                url: pendingMessage,
                                                headers: { authorization: authorize }
                                            }
                                            cy.log("option pending msg is: " + JSON.stringify(optionsPendingMessage), " and: " + optionsPendingMessage)
                                            cy.request(optionsPendingMessage).then((response) => {
                                                expect(response.status).equal(200)
                                                expect(response.body.result).not.be.empty
                                                var messageID = response.body.result[0].msgId
                                                cy.log("message id is: " + messageID)

                                                //Accept message
                                                var acceptMessage = "" + url + "/wapi/api/v1/notification/accepted-message"
                                                var bodyAcceptMsg = {
                                                    "msgID": messageID,
                                                    "jobNo": dispatch,
                                                    "ackDateTime": "2023-03-21T16:43:46.073Z"
                                                }
                                                const optionsAcceptMessage = {
                                                    method: 'POST',
                                                    url: acceptMessage,
                                                    headers: { authorization: authorize },
                                                    body: bodyAcceptMsg
                                                }
                                                cy.log("option accept msg is: " + JSON.stringify(optionsAcceptMessage))
                                                cy.request(optionsAcceptMessage).then((response) => {
                                                    expect(response.status).equal(200)
                                                    expect(response.body.result).equal(1)
                                                })
                                            })

                                            // //14. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number12 > Click Update Status 
                                            cy.log("Select Shipment tab > View Detail Shipment Number1 > Click Update Status")
                                            //Verify status activity tracking
                                            objActivityTracking = objDispatchOrderTracking.leftMenu.navigateToPage("Activity Tracking")
                                            objActivityTracking.openShipmentToUpdate(shipmentOrder1)
                                            objActivityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                                            objActivityTracking.leftMenu.closeTab()

                                            // Bill2
                                            //17.1 Click Update Driver 1st time  --> Status Shipment = Shipment_arrival_destination
                                            cy.log("17. Select Shipment tab > View Detail Shipment Number2 > Click Update Status")
                                            objActivityTracking = objHome.leftMenu.navigateToPage("Activity Tracking")
                                            objActivityTracking.openShipmentToUpdate(shipmentOrder2)
                                            objActivityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                                            objActivityTracking.leftMenu.closeTab()

                                            //Verify status dispatch order tracking
                                            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
                                            objDispatchOrderTracking.verifyStatusOrder($dispatchNumber, 'job_delivered')
                                            //Verify data correction
                                            objDispatchOrderTracking.common.openDetailView()
                                            objDispatchOrderTracking.dataCorrection("yes")
                                            objDispatchOrderTracking.lblSubcontractor().then(($el) => {
                                                const expectSub = $el.text().trim()
                                                objDispatchOrderTracking.leftMenu.btnCloseMenu().last().click({ force: true })
                                                // verify data correction
                                                objDriverReceipt = objDispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                                                objDriverReceipt.common.searchByClickSuggestion("Dispatch Number", $dispatchNumber.text().trim())
                                                objDriverReceipt.common.openDetailView()
                                                objDriverReceipt.navigateToMissionInforTab()
                                                objDriverReceipt.lblCarrier().should("have.text", expectSub)
                                                objDriverReceipt.navigateToPODInforTab()
                                                objDriverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                                                cy.get('@expectedLicense').then(expectedLicense => {
                                                    cy.get('@actualLicense').then(actualLicense => {
                                                        expect(expectedLicense).eq(actualLicense)
                                                    })
                                                })
                                            })
                                            //19. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD
                                            cy.log("19. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD \n" +
                                                "20. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order")

                                            objDriverReceipt.confirmPOD(filePath)
                                            objDriverReceipt.common.msgSuccessShouldContainText("done successfully").should("be.visible")
                                            // Send Proof Delivery
                                            objDriverReceipt.common.openDetailView()
                                            objDriverReceipt.proofOfDelivery()
                                            // 
                                            objDriverReceipt.common.msgSuccess("done successfully").should("be.visible")
                                            cy.wait(1000)
                                            objDriverReceipt.leftMenu.btnCloseMenu().last().click({ force: true })
                                            //21. Go to Activity Tracking
                                            cy.log("21. Go to Activity Tracking\n" + "22 View Order Detail > Verify Track History\n" + "23 View Job Detail > Verify Track History\n" + "24 View Shipment Detail > Verify Track History")
                                            objActivityTracking = objDriverReceipt.leftMenu.navigateToPage("Activity Tracking")
                                            cy.wait(1000)
                                            objActivityTracking.tabShipment().click({ force: true })
                                            objActivityTracking.txtSearch().click({ force: true })
                                            objActivityTracking.txtSearch().type($shipmentNumber.text().trim())
                                            objActivityTracking.resultsSearch($shipmentNumber.text().trim()).click({ multiple: true, force: true })
                                            objActivityTracking.iconSearch().click({ force: true })
                                            objActivityTracking.openRecordToUpdateDriver($shipmentNumber.text().trim()).dblclick({ force: true })
                                            objActivityTracking.shipmentStatusFinished()
                                            cy.go('back')
                                            cy.wait(1000)
                                            objActivityTracking.jobTab().click({ force: true })
                                            objActivityTracking.txtSearchJobTab().click({ force: true })
                                            objActivityTracking.txtSearchJobTab().realType($dispatchNumber.text().trim())
                                            objActivityTracking.resultsSearch($dispatchNumber.text().trim()).last().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.iconSearchJobTab().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.openRecordToUpdateDriver($dispatchNumber.text().trim()).dblclick({ force: true })
                                            objActivityTracking.viewTrackHistory("Dispatch_Created")
                                            objActivityTracking.viewTrackHistory("Job_Created")
                                            objActivityTracking.viewTrackHistory("Job_Accepted")
                                            objActivityTracking.viewTrackHistory("Job_Ontheway")
                                            objActivityTracking.viewTrackHistory("Job_Delivered")
                                            cy.go('back')
                                            cy.wait(1000)
                                            objActivityTracking.orderTab().click({ force: true })
                                            objActivityTracking.txtSearchOrderTab().click()
                                            objActivityTracking.txtSearchOrderTab().type($shippingOrderNumber.text().trim())
                                            objActivityTracking.resultsSearch($shippingOrderNumber.text().trim()).last().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.iconSearchOrderTab().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.openRecordToUpdateDriver($shippingOrderNumber.text().trim()).dblclick({ force: true })
                                            objActivityTracking.orderStatusFinished()
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})