import E2E from "../../../support/pages/E2E/E2E"
import ActivityTracking from "../../../support/pages/TMS/components/activitytracking/ActivityTracking"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import DeliveryGood from "../../../support/pages/TMS/components/Implement/DeliveryGood"
import Pickup from "../../../support/pages/TMS/components/Implement/PickUp"
import Login from "../../../support/pages/TMS/components/login/Login"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import DriverReceipt from "../../../support/pages/TMS/components/receipt/DriverReceipt"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import DispatchOrderTracking from "../../../support/pages/TMS/components/track/DispatchOrderTracking"
import ShippingOrderTracking from "../../../support/pages/TMS/components/track/ShippingOrderTracking"
import { Utility } from "../../../support/utility"

describe("Shipping from 2 origins to 3 desination site, Requested Vehicle Type 10W but Actual Vehicle Type 6W and 4W", () => {
    var data, objLogin = new LoginSupplier(), objHome = new Homepage(), objLoginPRD, objDeliveryPlan = new DeliveryPlan(),
        objShippingOrder = new ShippingOrder(), objSchedule = new Schedule(), objActivityTracking = new ActivityTracking()
    var objShippingOrderTracking = new ShippingOrderTracking(), objDispatchOrderTracking = new DispatchOrderTracking(),
        objPickup = new Pickup(), objDriverReceipt = new DriverReceipt(),
        objCreateShippingOrder = new CreateShippingOrder(), objDeliverGoods = new DeliveryGood()
    var objE2E = new E2E(), url = new Utility().getBaseUrl()
    var filePath = "implement/images.png"
    var cusName = "Automation Company", surcharge = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            cy.visit(url)
            objLogin = new LoginSupplier()
            objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
        })
    })

    it('WEB-11: Shipping from 2 origins to 3 desination site, Requested Vehicle Type 10W but Actual Vehicle Type 6W and 4W', () => {
        //9. Login TMS > Go to Shipping Order > Create New Order1 from SiteA to SiteB > Click Confirm
        cy.log("9. Login TMS > Go to Shipping Order > Create New Order1 from SiteA to SiteB > Click Confirm")
        //create order1 siteA -> siteB, Requested Vehicle Type = 10W and confirm
        cy.randomString(8).then((order1) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithRequestedVehicleType("Confirm", order1, cusName, surcharge, temperature, siteA, siteB, 5);
            //Verify status order
            objShippingOrder.verifyOrderHasJustCreate(order1)
            objShippingOrder.common.columnShippingOrderNumber().should('be.visible').then(($shippingOrderNumber) => {
                //10.1 Click Inquire > Select Order1 > Create a dispatch Order
                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                cy.log("10.1 Click Inquire > Select Order1 > Create a dispatch Order")
                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", order1, "first")
                objDeliveryPlan.common.searchResultWithOutValue().first().click({ force: true })

                //10.1 Click Inquire > Select Order1 > Create a dispatch Order1 
                objDeliveryPlan.columnShipmentOrder().first().should('be.visible').then(($shipmentNumber) => {
                    var shipmentOrder1 = $shipmentNumber.text().trim()
                    objDeliveryPlan.header.btnAtHeader("create a dispatch order ").click()
                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", order1, "second")
                    objDeliveryPlan.columnDispatchNumber().then(($dispatch12) => {
                        cy.reload()
                        cy.log("Create order2 siteA -> siteC, Requested Vehicle Type = 10W and confirm")
                        //11. Create order2 siteA -> siteC, Requested Vehicle Type = 10W and confirm
                        cy.randomString(9).then((order2) => {
                            objShippingOrder = objDeliveryPlan.leftMenu.navigateToPage("Shipping order")
                            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                            objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithRequestedVehicleType("Confirm", order2, cusName, surcharge, temperature, siteA, siteC, 5);
                            objShippingOrder.verifyOrderHasJustCreate(order2)
                            objShippingOrder.common.columnShippingOrderNumber().then(($shippingOrderNumber2) => {
                                objShippingOrder.leftMenu.closeTab()

                                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", order2, "first")
                                objDeliveryPlan.columnShipmentOrder().first().should('be.visible').then(($shipmentNumber2) => {
                                    var shipmentOrder2 = $shipmentNumber2.text().trim()
                                    cy.log("10. Add order2 to dispatch1 : order12")
                                    //11.1 Add order2 to dispatch1 : dispatch order12
                                    objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", order1, "second")
                                    objDeliveryPlan.addTo(order2)
                                    objDeliveryPlan.header.searchResult(order1).should("be.visible")
                                    //confirm order12
                                    cy.wait(1000)
                                    objDeliveryPlan.confirmDispatch()
                                    objDeliveryPlan.common.msgSuccess().should("be.visible")

                                    //9. Login DriverA in DriverApp successfully
                                    var apiLogin = "https://account-stg.allnowgroup.com/mobile/driver-login"
                                    var driverID = "", accessToken = ""
                                    var phone = "849994858", password = "20000412"
                                    var body1 = {
                                        "countryCode": 66,
                                        "phone": phone,
                                        "password": password,
                                        "accessOtherDevice": true,
                                        "deviceId": "b791071c2d28be26"
                                    }
                                    const options = {
                                        method: 'POST',
                                        url: apiLogin,
                                        body: body1
                                    }
                                    cy.request(options).then((response) => {
                                        expect(response.status).equal(200)
                                        driverID = response.body.items.user.driverId
                                        accessToken = response.body.items.token.access_token
                                        cy.log("driverID1 is: " + driverID, "accessToke is: " + accessToken)

                                        objDeliveryPlan.leftMenu.closeTab()

                                        // 11. Go to Schedule > View Detail Dispatch Number of Order12  > Assign DriverA&VehicleA with Vehicle Type = 6W > Click Confirm and Dispatch
                                        cy.log("11. Go to Schedule > View Detail Dispatch Number of Order12  > Assign DriverA&VehicleA with Vehicle Type = 6W > Click Confirm and Dispatch")
                                        objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                                        objSchedule.common.searchByClickSuggestion("Dispatch Number", $dispatch12.text().trim())
                                        cy.wait(1000)

                                        objSchedule.columnDispatchNumber().then((dispatchNumber) => {
                                            var dispatch = dispatchNumber.text().trim()

                                            //13. Go to DriverA in DriverApp again > Click Accept
                                            //verify fmctoken
                                            var authorize = accessToken
                                            var fcmTokenLink = "" + url + "/wapi/api/v1/notification/fcmtoken"
                                            var bodyFMCToken = {
                                                "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                                                "driverID": driverID,
                                                "deviceID": "b791071c2d28be26",
                                                "createdDatetime": "2023-03-21T16:14:45.073Z"
                                            }
                                            const options2 = {
                                                method: 'POST',
                                                url: fcmTokenLink,
                                                headers: { authorization: authorize },
                                                body: bodyFMCToken
                                            }
                                            cy.log("options2 is: " + JSON.stringify(options2))
                                            cy.request(options2).then((response) => {
                                                expect(response.status).equal(200)
                                            })

                                            objSchedule.common.openDetailView()
                                            cy.wait(1000)
                                            //assign carrier + driver
                                            objSchedule.selectDataAndSchedule("N20S8AT", "Service for Carrier", "DriverCZIHQPZDGR", "Confirm and Dispatch")
                                            objSchedule.common.msgSuccess().should("be.visible")

                                            cy.wait(300)
                                            objSchedule.leftMenu.closeTab()

                                            //Call API pending message
                                            var pendingMessage = "" + url + "/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                                            const optionsPendingMessage = {
                                                method: 'GET',
                                                url: pendingMessage,
                                                headers: { authorization: authorize }
                                            }
                                            cy.log("option pending msg is: " + JSON.stringify(optionsPendingMessage), " and: " + optionsPendingMessage)
                                            cy.request(optionsPendingMessage).then((response) => {
                                                expect(response.status).equal(200)
                                                expect(response.body.result).not.be.empty
                                                var messageID = response.body.result[0].msgId
                                                cy.log("message id is: " + messageID)

                                                //Accept message
                                                var acceptMessage = "" + url + "/wapi/api/v1/notification/accepted-message"
                                                var bodyAcceptMsg = {
                                                    "msgID": messageID,
                                                    "jobNo": dispatch,
                                                    "ackDateTime": "2023-03-21T16:43:46.073Z"
                                                }
                                                const optionsAcceptMessage = {
                                                    method: 'POST',
                                                    url: acceptMessage,
                                                    headers: { authorization: authorize },
                                                    body: bodyAcceptMsg
                                                }
                                                cy.log("option accept msg is: " + JSON.stringify(optionsAcceptMessage))
                                                cy.request(optionsAcceptMessage).then((response) => {
                                                    expect(response.status).equal(200)
                                                    expect(response.body.result).equal(1)
                                                })
                                            })

                                            //12. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number12 > Click Update Status 
                                            cy.log("12. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number12 > Click Update Status ")
                                            objActivityTracking = objDispatchOrderTracking.leftMenu.navigateToPage("Activity Tracking")
                                            objActivityTracking.openShipmentToUpdate(shipmentOrder1)
                                            objActivityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                                            objActivityTracking.leftMenu.closeTab()

                                            // Order2
                                            objActivityTracking = objDispatchOrderTracking.leftMenu.navigateToPage("Activity Tracking")
                                            objActivityTracking.openShipmentToUpdate(shipmentOrder2)
                                            objActivityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                                            objActivityTracking.leftMenu.closeTab()

                                            cy.log("Web-15.01 Go to Track > Dispatch Order Tracking")
                                            objDispatchOrderTracking = objHome.leftMenu.navigateToSubPage("Track ", "Dispatch Order Tracking")
                                            objDispatchOrderTracking.verifyStatusOrder($dispatch12, 'job_delivered')
                                            cy.wait(500)
                                            //Verify data correction
                                            objDispatchOrderTracking.common.openDetailView()
                                            objDispatchOrderTracking.dataCorrection("yes")
                                            objDispatchOrderTracking.lblSubcontractor().then(($el) => {
                                                const expectSub = $el.text().trim()
                                                objDispatchOrderTracking.leftMenu.closeTab()

                                                cy.log("Web-16.01 Go to Receipt > Driver Receipt")
                                                // verify data correction order1
                                                objDriverReceipt = objDispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                                                objDriverReceipt.common.searchByClickSuggestion("Dispatch Number", $dispatch12.text().trim())
                                                objDriverReceipt.common.openDetailView()
                                                objDriverReceipt.navigateToMissionInforTab()
                                                objDriverReceipt.lblCarrier().should("have.text", expectSub)
                                                objDriverReceipt.navigateToPODInforTab()
                                                objDriverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                                                cy.get('@expectedLicense').then(expectedLicense => {
                                                    cy.get('@actualLicense').then(actualLicense => {
                                                        expect(expectedLicense).eq(actualLicense)
                                                    })
                                                })
                                                // verify data correction order2
                                                cy.reload()
                                                objDriverReceipt.common.searchByClickSuggestion("Dispatch Number", $shipmentNumber.text().trim())
                                                objDriverReceipt.common.openDetailView()
                                                objDriverReceipt.navigateToMissionInforTab()
                                                objDriverReceipt.lblCarrier().should("have.text", expectSub)
                                                objDriverReceipt.navigateToPODInforTab()
                                                objDriverReceipt.divPlateNumber().invoke('text').as('actualLicense2')
                                                cy.get('@expectedLicense').then(expectedLicense => {
                                                    cy.get('@actualLicense2').then(actualLicense2 => {
                                                        expect(expectedLicense).eq(actualLicense2)
                                                        objDriverReceipt.leftMenu.closeTab()
                                                    })
                                                })
                                            })

                                            cy.log("Web-17.01 Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD \n" +
                                                "20. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order")

                                            cy.log("View Disptach Number of Order12( shipment number 1) > Upload attachment > Confirm POD")
                                            objDriverReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                                            objDriverReceipt.common.searchByClickSuggestion("waybill number", $shipmentNumber.text().trim())
                                            objDriverReceipt.common.openDetailView()
                                            objDriverReceipt.confirmPOD(filePath)
                                            objDriverReceipt.common.msgSuccessShouldContainText("done successfully").should("be.visible")
                                            // Send Proof Delivery
                                            objDriverReceipt.common.openDetailView()
                                            objDriverReceipt.proofOfDelivery()
                                            objDriverReceipt.common.msgSuccess("done successfully").should("be.visible")
                                            objDriverReceipt.leftMenu.closeTab()
                                            objDriverReceipt.leftMenu.closeTab()
                                            cy.log("View Disptach Number of Order12 (shipment number 2) > Upload attachment > Confirm POD")
                                            objDriverReceipt = objHome.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                                            objDriverReceipt.common.searchByClickSuggestion("waybill number", shipmentOrder2)
                                            objDriverReceipt.common.openDetailView()
                                            objDriverReceipt.confirmPOD(filePath)
                                            objDriverReceipt.common.msgSuccessShouldContainText("done successfully").should("be.visible")
                                            // Send Proof Delivery
                                            objDriverReceipt.common.openDetailView()
                                            objDriverReceipt.proofOfDelivery()
                                            objDriverReceipt.common.msgSuccess("done successfully").should("be.visible")
                                            cy.wait(200)
                                            objDriverReceipt.leftMenu.closeTab()

                                            //21. Go to Activity Tracking
                                            cy.log("21. Go to Activity Tracking\n" + "22 View Order Detail > Verify Track History\n" + "23 View Job Detail > Verify Track History\n" + "24 View Shipment Detail > Verify Track History")
                                            objActivityTracking = objDriverReceipt.leftMenu.navigateToPage("Activity Tracking")
                                            cy.wait(500)
                                            objActivityTracking.tabShipment().click({ force: true })
                                            objActivityTracking.txtSearch().click({ force: true })
                                            objActivityTracking.txtSearch().realType(shipmentOrder1)
                                            objActivityTracking.resultsSearch(shipmentOrder1).click({ multiple: true, force: true })
                                            objActivityTracking.iconSearch().click({ force: true })
                                            cy.wait(500)
                                            objActivityTracking.openRecordToUpdateDriver(shipmentOrder1).dblclick({ force: true })
                                            objActivityTracking.shipmentStatusFinished()
                                            cy.go('back')
                                            cy.wait(1000)
                                            objActivityTracking.jobTab().click({ force: true })
                                            objActivityTracking.txtSearchJobTab().click({ force: true })
                                            objActivityTracking.txtSearchJobTab().realType(dispatch)
                                            objActivityTracking.resultsSearch(dispatch).last().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.iconSearchJobTab().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.openRecordToUpdateDriver(dispatch).dblclick({ force: true })
                                            objActivityTracking.viewTrackHistory("Dispatch_Created")
                                            objActivityTracking.viewTrackHistory("Job_Created")
                                            objActivityTracking.viewTrackHistory("Job_Accepted")
                                            objActivityTracking.viewTrackHistory("Job_Ontheway")
                                            objActivityTracking.viewTrackHistory("Job_Delivered")
                                            cy.go('back')
                                            cy.wait(1000)
                                            objActivityTracking.orderTab().click({ force: true })
                                            objActivityTracking.txtSearchOrderTab().click()
                                            objActivityTracking.txtSearchOrderTab().realType($shippingOrderNumber.text().trim())
                                            objActivityTracking.resultsSearch($shippingOrderNumber.text().trim()).last().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.iconSearchOrderTab().click({ force: true })
                                            cy.wait(1000)
                                            objActivityTracking.openRecordToUpdateDriver($shippingOrderNumber.text().trim()).dblclick({ force: true })
                                            objActivityTracking.orderStatusFinished()

                                            objActivityTracking.leftMenu.closeTab()
                                            ///////////////////////////////////////////////////////////////////////
                                            //9.3 Create New Order3 with Route CodeA from SiteB to SiteD, Requested Vehicle Type = 10W > Click Confirm
                                            cy.log("9.3 Create New Order3 with Route CodeA from SiteB to SiteD, Requested Vehicle Type = 10W > Click Confirm")
                                            objShippingOrder = objActivityTracking.leftMenu.navigateToPage("Shipping order")
                                            cy.randomStringAndNumber(7).then((order3) => {
                                                objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
                                                objShippingOrder = objCreateShippingOrder.createNewShippingOrderWithRequestedVehicleType("Confirm", order3, cusName, surcharge, temperature, siteB, siteD, 5);
                                                // objShippingOrder.common.msgSuccess().should("be.visible")
                                                objShippingOrder.leftMenu.closeTab()
                                                cy.log("10. Go to Plan > Delivery Plan > Select Order1 > Create a dispatch Order > Click Confirm ")
                                                // 10. Go to Plan > Delivery Plan 
                                                //  10.1 Click Inquire > Select Order3 > Create a dispatch Order
                                                //  10.2 On the Disptach Order > Select Dispatch Number of Order > Click Confirm 
                                                objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
                                                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", order3, "first")
                                                //create dispach order
                                                objDeliveryPlan.createADispatchOrder(order3)
                                                cy.reload()
                                                objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("Dispatch Number", order3, "second")
                                                objDeliveryPlan.searchResultWithOutValueAt2ndTable().dblclick({ force: true })
                                                objDeliveryPlan.header.searchResult(order3).should("be.visible")
                                                //confirm dispatch
                                                objDeliveryPlan.confirmDispatch()
                                                objDeliveryPlan.common.msgSuccess().should("be.visible")

                                                //9. Login DriverA in DriverApp successfully
                                                var apiLogin3 = "https://account-stg.allnowgroup.com/mobile/driver-login"
                                                var driverID3 = "", accessToken3 = ""
                                                var body3 = {
                                                    "countryCode": 66,
                                                    "phone": phone,
                                                    "password": password,
                                                    "accessOtherDevice": true,
                                                    "deviceId": "b791071c2d28be26"
                                                }
                                                const options3 = {
                                                    method: 'POST',
                                                    url: apiLogin3,
                                                    body: body3
                                                }
                                                cy.request(options3).then((response) => {
                                                    expect(response.status).equal(200)
                                                    driverID3 = response.body.items.user.driverId
                                                    accessToken3 = response.body.items.token.access_token
                                                    cy.log("driverID3 is: " + driverID3, "accessToke is: " + accessToken3)

                                                    objDeliveryPlan.leftMenu.closeTab()

                                                    cy.log("11. Go to Schedule > View Detail Dispatch Number of Order  > Assign DriverA&VehicleA > Click Confirm and Dispatch")
                                                    // 11. Go to Schedule > View Detail Dispatch Number of Order  > Assign DriverA&VehicleA > Click Confirm and Dispatch
                                                    objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
                                                    objSchedule.common.searchByClickSuggestion("Dispatch Number", order3)
                                                    cy.wait(1000)

                                                    objSchedule.columnDispatchNumber().then((dispatchNumber) => {
                                                        var dispatch3 = dispatchNumber.text().trim()

                                                        //13. Go to DriverA in DriverApp again > Click Accept
                                                        //verify fmctoken
                                                        var authorize3 = accessToken
                                                        var fcmTokenLink3 = "" + url + "/wapi/api/v1/notification/fcmtoken"
                                                        var bodyFMCToken3 = {
                                                            "fcmToken": "fcrf2h6eRi2ucjlAsw_dxV:APA91bEOxLdNGwmOx-YFeS3Jm-TR7Prrkm8EUFhHvq7d4kYiRO34d4X8U2gnV3lnSyUECPWvw4l8itHNK--7Pj5tFv9TMvbZ0DJt6YIy_lyxXAskN624vy1bePoyE6qk-uWN1YrpukqW",
                                                            "driverID": driverID3,
                                                            "deviceID": "b791071c2d28be26",
                                                            "createdDatetime": "2023-03-21T16:14:45.073Z"
                                                        }
                                                        const options3 = {
                                                            method: 'POST',
                                                            url: fcmTokenLink3,
                                                            headers: { authorization: authorize3 },
                                                            body: bodyFMCToken3
                                                        }
                                                        cy.log("options3 is: " + JSON.stringify(options3))
                                                        cy.request(options3).then((response) => {
                                                            expect(response.status).equal(200)
                                                        })

                                                        objSchedule.common.openDetailView()
                                                        objSchedule.header.searchResult(order3).should("be.visible")
                                                        cy.wait(1000)
                                                        //assign carrier + driver
                                                        objSchedule.selectDataAndSchedule("N20S8AT", "Service for Carrier", "DriverCZIHQPZDGR", "Confirm and Dispatch")
                                                        objSchedule.common.msgSuccess().should("be.visible")
                                                        //Call API pending message
                                                        var pendingMessage3 = "" + url + "/wapi/api/v1/notification/driverId/" + driverID + "/pending-messages"
                                                        const optionsPendingMessage3 = {
                                                            method: 'GET',
                                                            url: pendingMessage3,
                                                            headers: { authorization: authorize3 }
                                                        }
                                                        cy.log("option pending msg3 is: " + JSON.stringify(optionsPendingMessage3), " and: " + optionsPendingMessage3)
                                                        cy.request(optionsPendingMessage3).then((response) => {
                                                            expect(response.status).equal(200)
                                                            expect(response.body.result).not.be.empty
                                                            var messageID3 = response.body.result[0].msgId

                                                            //Accept message
                                                            var acceptMessage3 = "" + url + "/wapi/api/v1/notification/accepted-message"
                                                            var bodyAcceptMsg3 = {
                                                                "msgID": messageID3,
                                                                "jobNo": dispatch3,
                                                                "ackDateTime": "2023-03-21T16:43:46.073Z"
                                                            }
                                                            const optionsAcceptMessage3 = {
                                                                method: 'POST',
                                                                url: acceptMessage3,
                                                                headers: { authorization: authorize3 },
                                                                body: bodyAcceptMsg3
                                                            }
                                                            cy.log("option pending msg is: " + JSON.stringify(optionsAcceptMessage3))
                                                            cy.request(optionsAcceptMessage3).then((response) => {
                                                                expect(response.status).equal(200)
                                                                // expect(response.body.result).not.be.empty
                                                                expect(response.body.result).not.equal(0)
                                                            })
                                                        })
                                                        // 9. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                                                        //  9.1 Click Update Driver 1st time --> Status Shipment = Shipment_Arrival_Pickup
                                                        //  9.2 Click Update Driver 2nd time --> Status Shipment = Shipment_Loading
                                                        // 10. Go to Implement > Pickup > Select Dispatch Number of Order1  > Click Pickup Completed --> Status Shipment = Shipment_Ontheway
                                                        // 11. Go to Activity Tracking > Select Shipment tab > View Detail Shipment Number1 > Click Update Status 
                                                        //   11.1 Click Update Driver 1st time  --> Status Shipment = Shipment_arrival_destination
                                                        //   11.2 Click Update Driver 2nd time --> Status Shipment = Shipment_unloading
                                                        // 12. Go to Implement > Delivery Goods > Select Dispatch Number of Order1  > Click Delivery Completed
                                                        // 13. Go to Receipt > Driver Receipt > View Disptach Number of Order1 > Upload attachment > Confirm POD
                                                        // 14. Go to Receipt > Shipper Receipt > Send Proof of Delivery Order
                                                        objE2E.updateShipmentStatusToShipmentDelivered(dispatch3, filePath, "yes", "normal")
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})

