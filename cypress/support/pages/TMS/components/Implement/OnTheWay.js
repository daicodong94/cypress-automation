import Common from "../common/Common"
import Header from "../fragments/Header"
import LeftMenu from "../fragments/LeftMenu"
import CreateShippingOrder from "../shippingorder/CreateShippingOrder"
import Pickup from "./PickUp"
import Schedule from "../schedule/Schedule"

class OnTheWay {
    constructor() {
        this.header = new Header()
        this.common = new Common()
        this.pickup = new Pickup()
        this.leftMenu = new LeftMenu()
        this.createShippingOrder = new CreateShippingOrder()
        this.schedule = new Schedule()
    }

    transportDispatch(site) {
        this.header.btnAtHeader("transport").click()
        this.common.textField("site").click()
        this.createShippingOrder.txtSearchLocation().type(site)
        cy.wait(1000)
        this.createShippingOrder.spanSearchLocationResult().click()
        this.common.textField("Estimated arrival time").click()
        this.createShippingOrder.btnNow().click()
        cy.wait(1000)
        this.common.textField("Estimated departure time").click()
    }

    changeVehicle(driver) {
        this.header.btnAtHeader("change vehicle").click()
        this.schedule.selectDriver(driver)
        cy.wait(1000)
        this.common.textField("change time").click()
        this.createShippingOrder.btnNow().click()
        this.common.btnDialogFooter("confirm").first().click()
        this.common.msgSuccess().should("be.visible")
    }
}

export default OnTheWay;