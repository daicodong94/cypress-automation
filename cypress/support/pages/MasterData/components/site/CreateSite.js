import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import SiteAdd from "./SiteAdd"
import SiteList from "./SiteList"

class CreateSite {
    constructor() {
        this.common = new CommonMasterData()
        // this.siteAdd = new SiteAdd()
        this.header = new HeaderMasterData()
    }

    ipInformation(infor) {
        return cy.xpath('//input[@placeholder="' + infor + '"]')
    }

    btnConfirmLocation() {
        return cy.xpath('//button//span[contains(text(),"Confirm Location")]')
    }

    btnSave() {
        return cy.xpath('//button//span[contains(text(),"Save")]')
    }

    selectValue(value) {
        return cy.xpath('//span[text()="' + value + '"]')
    }

    selectCustomerName(CustomerName) {
        return cy.xpath('(//li[contains(text(),"' + CustomerName + '")])[1]')
    }

    startAndEndTime(time) {
        return cy.xpath('//div[@class="relative w-full"]//div[contains(text(),"' + time + '")][1]')
    }

    selectHourAndMinutes(time, location) {
        return cy.xpath('(//div[@class="rounded-sm dropdown-container bg-white z-20 swing-in-top-fwd custom-time-dropdown fixed"]//div[contains(text(),"' + time + '")])[' + location + ']')
    }

    btnOK() {
        return cy.xpath('//div[contains(text(),"Ok")]')
    }

    ipTextareaInfor(value) {
        return cy.xpath('//textarea[@placeholder="' + value + '"]')
    }

    msgCreateSiteSuccess() {
        return cy.xpath('//p[text()="Create site successfully"]')
    }

    btnEditSite() {
        return cy.xpath('//div[contains(text(),"Edit")]')
    }

    btnConfirmLocationStatus() {
        return cy.xpath('//div[contains(text(),"Confirm Location")]//ancestor::button')
    }

    ipInformationOnSite(infor, value) {
        this.ipInformation(infor).type(value)
    }

    ipSiteTextareaInfor(value, SiteName) {
        this.ipTextareaInfor(value).type(SiteName)
    }

    ipCustomerName(infor, CustomerName) {
        this.ipInformation(infor).type(CustomerName)
        this.selectCustomerName(CustomerName).click()
    }

    selectInformation(infor, value) {
        this.ipInformation(infor).click()
        this.selectValue(value).trigger('mouseover').click()
    }

    selectStartTimeAndEndTime() {
        // Choose Start Time
        this.startAndEndTime("Start Time").click()
        this.selectHourAndMinutes("02", "1").click()
        this.selectHourAndMinutes("02", "2").click()
        this.btnOK().click()
        // Choose End Time
        this.startAndEndTime("End Time").click()
        this.selectHourAndMinutes("05", "1").click()
        this.selectHourAndMinutes("05", "2").click()
        this.btnOK().click()
    }

    ckbDayChecked(days) {
        return cy.xpath('//span[text()="' + days + '"]//preceding-sibling::span[@class="el-checkbox__input is-checked"]')
    }

    createNewSite(CustomerName, SiteCode, SiteName, SiteType, SiteTypeGroup, Title, FirstName, LastName, Email, Address, Province, District, SubDistrict, PostalCode) {
        this.header.btnCreate().click()
        this.ipCustomerName("Enter Customer name", CustomerName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Code", SiteCode)
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Site name", SiteName)
        cy.wait(1000)
        this.selectInformation("Enter Site type", SiteType)
        cy.wait(1000)
        this.selectInformation("Select Site type group", SiteTypeGroup)
        cy.wait(1000)
        this.ckbDayChecked("Tuesday").scrollIntoView().click()
        this.ckbDayChecked("Wednesday").scrollIntoView().click()
        this.ckbDayChecked("Thursday").scrollIntoView().click()
        this.ckbDayChecked("Friday").scrollIntoView().click()
        this.ckbDayChecked("Saturday").scrollIntoView().click()
        this.ckbDayChecked("Sunday").scrollIntoView().click()
        this.selectStartTimeAndEndTime()
        this.selectInformation("Select Title", Title)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s first name", FirstName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s last name", LastName)
        cy.wait(1000)
        cy.randomNumber(5).then((number) => {
            this.ipInformationOnSite("Enter Key Contact Person’s email", Email + number + '@gmail.com')
        })
        cy.wait(1000)
        cy.randomNumber(9).then((mobilePhone) => {
            this.ipInformationOnSite("Enter Mobile Phone no.", mobilePhone)
        })
        cy.wait(1000)
        cy.randomNumber(8).then((landlinePhone) => {
            this.ipInformationOnSite("Enter Landline phone no.", landlinePhone)
        })
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Address", Address)
        cy.wait(1000)
        // Select Province
        this.ipCustomerName("Select Province", Province)
        cy.wait(1000)
        // Select District
        this.selectInformation("Select District", District)
        cy.wait(1000)
        // Select Sub District
        this.selectInformation("Select Sub-District", SubDistrict)
        cy.wait(1000)
        // Select PostalCode
        this.selectInformation("Select Postal Code", PostalCode)
        cy.wait(1000)
        this.btnConfirmLocation().click()
        cy.wait(2000)
        this.btnSave().click()
        cy.wait(2000)
        // this.msgCreateSiteSuccess().should("be.visible")
    }

    createNewSiteEtoE(CustomerName, SiteCode, SiteName, SiteType, SiteTypeGroup, Title, FirstName, LastName, Email, Address, Province) {
        this.ipCustomerName("Enter Customer name", CustomerName)
        this.ipInformationOnSite("Enter Code", SiteCode)
        this.ipSiteTextareaInfor("Enter Site name", SiteName)
        this.selectInformation("Enter Site type", SiteType)
        this.selectInformation("Select Site type group", SiteTypeGroup)
        cy.wait(3000 / 2)
        this.ckbDayChecked("Tuesday").scrollIntoView().click({ force: true })
        this.ckbDayChecked("Wednesday").scrollIntoView().click({ force: true })
        this.ckbDayChecked("Thursday").scrollIntoView().click({ force: true })
        this.ckbDayChecked("Friday").scrollIntoView().click({ force: true })
        this.ckbDayChecked("Saturday").scrollIntoView().click({ force: true })
        this.ckbDayChecked("Sunday").scrollIntoView().click({ force: true })
        this.selectStartTimeAndEndTime()
        this.selectInformation("Select Title", Title)
        this.ipInformationOnSite("Enter Key Contact Person’s first name", FirstName)
        this.ipInformationOnSite("Enter Key Contact Person’s last name", LastName)
        cy.randomNumber(5).then((number) => {
            this.ipInformationOnSite("Enter Key Contact Person’s email", Email + number + '@gmail.com')
        })
        cy.randomNumber(9).then((mobilePhone) => {
            this.ipInformationOnSite("Enter Mobile Phone no.", mobilePhone)
        })
        cy.randomNumber(8).then((landlinePhone) => {
            this.ipInformationOnSite("Enter Landline phone no.", landlinePhone)
        })
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Address", Address)
        // Select Province
        this.ipCustomerName("Select Province", Province)
        // Select District 
        // this.ipInformation("Select District").click({ force: true })
        // cy.wait(1000)
        // this.common.selectRandomValue()
        var searchKeyDistrict = "ขามสะแกแสง"
        this.common.ipSelectBox("Select District").click({ force: true })
        this.common.liDropdownValue(searchKeyDistrict).click({ force: true })
        cy.wait(500)
        // Select Sub District
        this.ipInformation("Select Sub-District").click({ force: true })
        cy.wait(500)
        this.common.selectRandomValue()
        // Select PostalCode
        this.ipInformation("Select Postal Code").click({ force: true })
        cy.wait(500)
        this.common.selectRandomValue()
        cy.wait(500)
        this.btnConfirmLocation().click({ force: true })
        this.common.confirmCreateData("Save", "empty", "empty", "success")
        return new SiteList()
    }

    createSiteWithOutEmail(CustomerName, SiteCode, SiteName, SiteType, SiteTypeGroup, Title, FirstName, LastName, Address, Province, District, SubDistrict, PostalCode) {
        this.header.btnCreate().click()
        this.ipCustomerName("Enter Customer name", CustomerName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Code", SiteCode)
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Site name", SiteName)
        cy.wait(1000)
        this.selectInformation("Enter Site type", SiteType)
        cy.wait(1000)
        this.selectInformation("Select Site type group", SiteTypeGroup)
        cy.wait(1000)
        this.ckbDayChecked("Tuesday").scrollIntoView().click()
        this.ckbDayChecked("Wednesday").scrollIntoView().click()
        this.ckbDayChecked("Thursday").scrollIntoView().click()
        this.ckbDayChecked("Friday").scrollIntoView().click()
        this.ckbDayChecked("Saturday").scrollIntoView().click()
        this.ckbDayChecked("Sunday").scrollIntoView().click()
        this.selectStartTimeAndEndTime()
        this.selectInformation("Select Title", Title)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s first name", FirstName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s last name", LastName)
        cy.wait(1000)
        cy.wait(1000)
        cy.randomNumber(9).then((mobilePhone) => {
            this.ipInformationOnSite("Enter Mobile Phone no.", mobilePhone)
        })
        cy.wait(1000)
        cy.randomNumber(8).then((landlinePhone) => {
            this.ipInformationOnSite("Enter Landline phone no.", landlinePhone)
        })
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Address", Address)
        cy.wait(1000)
        // Select Province
        this.ipCustomerName("Select Province", Province)
        cy.wait(1000)
        // Select District
        this.selectInformation("Select District", District)
        cy.wait(1000)
        // Select Sub District
        this.selectInformation("Select Sub-District", SubDistrict)
        cy.wait(1000)
        // Select PostalCode
        this.selectInformation("Select Postal Code", PostalCode)
        cy.wait(1000)
        this.btnConfirmLocation().click()
        cy.wait(2000)
        this.btnSave().click()
        cy.wait(2000)
        // this.msgCreateSiteSuccess().should("be.visible")
    }

    addLocation(CustomerName, SiteCode, SiteName, SiteType, SiteTypeGroup, Title, FirstName, LastName, Email, Address) {
        this.header.btnCreate().click()
        this.ipCustomerName("Enter Customer name", CustomerName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Code", SiteCode)
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Site name", SiteName)
        cy.wait(1000)
        this.selectInformation("Enter Site type", SiteType)
        cy.wait(1000)
        this.selectInformation("Select Site type group", SiteTypeGroup)
        cy.wait(1000)
        this.ckbDayChecked("Tuesday").scrollIntoView().click()
        this.ckbDayChecked("Wednesday").scrollIntoView().click()
        this.ckbDayChecked("Thursday").scrollIntoView().click()
        this.ckbDayChecked("Friday").scrollIntoView().click()
        this.ckbDayChecked("Saturday").scrollIntoView().click()
        this.ckbDayChecked("Sunday").scrollIntoView().click()
        this.selectStartTimeAndEndTime()
        this.selectInformation("Select Title", Title)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s first name", FirstName)
        cy.wait(1000)
        this.ipInformationOnSite("Enter Key Contact Person’s last name", LastName)
        cy.wait(1000)
        cy.randomNumber(5).then((number) => {
            this.ipInformationOnSite("Enter Key Contact Person’s email", Email + number + '@gmail.com')
        })
        cy.wait(1000)
        cy.randomNumber(9).then((mobilePhone) => {
            this.ipInformationOnSite("Enter Mobile Phone no.", mobilePhone)
        })
        cy.wait(1000)
        cy.randomNumber(8).then((landlinePhone) => {
            this.ipInformationOnSite("Enter Landline phone no.", landlinePhone)
        })
        cy.wait(1000)
        this.ipSiteTextareaInfor("Enter Address", Address)
    }
}
export default CreateSite;