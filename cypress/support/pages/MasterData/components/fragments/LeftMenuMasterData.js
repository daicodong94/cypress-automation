import Driver from "../driver/Driver"
import SiteList from "../site/SiteList"
import SubcontractorList from "../subcontractor/SubcontractorList"
import VehicleList from "../vehicle/VehicleList"

class LeftMenuMasterData {

    lnkMenuMasterData() {
        return cy.xpath('//div[@class="el-submenu__title"]//span[text()="Master Data"]')
    }

    lnkSubMenuMasterData(subMenu) {
        return cy.xpath('//ul[@class="el-menu el-menu--inline"]//li[text()="' + subMenu + '"]')
    }

    navigateToSubPage(subMenu) {
        this.lnkSubMenuMasterData(subMenu, { timeout: 10000 }).should("be.visible").click()
        cy.wait(1000)
        if (subMenu == "Site") {
            return new SiteList()
        }
        if (subMenu == "Vehicle") {
            return new VehicleList()
        }
        if (subMenu == "Driver") {
            return new Driver()
        }
        if (subMenu == "Subcontractor") {
            return new SubcontractorList()
        }
        
    }


}
export default LeftMenuMasterData;