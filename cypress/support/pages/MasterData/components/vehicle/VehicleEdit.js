import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SiteAdd from "../site/SiteAdd";
import Subcontractor from "../subcontractor/SubcontractorList";
import AddVehicle from "./VehicleAdd";
import Vehicle from "./VehicleList";
import VehicleList from "./VehicleListdd";
import LeftMenu from "../../../TMS/components/fragments/LeftMenu";

class VehicleEdit {
    constructor() {
        this.vehicle = new Vehicle()
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
        this.siteAdd = new SiteAdd()
        this.vehicleList = new VehicleList()
        this.vehicleAdd = new AddVehicle()
        this.subcontractor = new Subcontractor()
    }

    breadcrumb() {
        return cy.xpath("//p[@class='text-sm mb-[14px]']")
    }

    specialCondition() {
        return cy.xpath("//div[@class='p-4']")
    }

    divVehicleID() {
        return cy.xpath("//div[@class='p-4 border border-divider bg-white radius-shadow_add']")
    }

    divStatus() {
        return cy.xpath("//div[@class='flex justify-between mb-[17px]']")
    }

    verifyDefaultUI() {
        this.vehicleAdd.verifyDefaultUI()
        this.breadcrumb().contains("Edit Vehicle")
        this.vehicleAdd.btnAssign("Site").should("be.visible")
        this.vehicleAdd.btnAssign("Subcontractor").should("be.visible")
        this.specialCondition().first().contains("Special Condition")
    }

    verifyDisplayTextboxes(id) {
        this.divVehicleID().contains(id)
        this.specialCondition().first().contains("Special Condition")
        this.divStatus().contains("Status")
        this.vehicleAdd.btnAssign("Site").should("be.visible")
        this.vehicleAdd.btnAssign("Subcontractor").should("be.visible")

        this.common.ipSelectBox("Enter Plate Number (ex. กข1234)").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Enter Vehicle Width").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Enter Vehicle Length").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Enter Vehicle Height").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Enter Max Weight").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Max Volume").scrollIntoView().should("be.exist")
        this.common.ipSelectBox("Select Registered Date").scrollIntoView().should("be.exist")
        this.common.ipSelectBox("Select Issued Date").scrollIntoView().should("be.exist")
        this.common.ipSelectBox("Select Expired Date").scrollIntoView().should("be.exist")
    }

    verifyDisplaySelectFields() {
        this.common.ipSelectBox("Select Vehicle Type").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Service Group").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Work Type").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Insurance").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Vehicle Brand").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Vehicle Model Year").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Fuel Type").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Provincal Sign").scrollIntoView().should("be.visible")
        this.common.ipSelectBox("Select Plate Color").scrollIntoView().should("be.visible")
    }

    removeData(field) {
        this.common.ipSelectBox(field).focus().clear()
    }

    //Compulsory third party insurance edit
    svgCompulsoryThirdPartyInsuranceIssuedDate() {
        return cy.xpath("(//div[text()='Compulsory Third Party Insurance']//parent::div//following-sibling::*[local-name()='svg'])[1]")
    }

    svgCompulsoryThirdPartyInsuranceExpiredDate() {
        return cy.xpath("(//div[text()='Compulsory Third Party Insurance']//parent::div//following-sibling::*[local-name()='svg'])[2]")
    }

    lblVehicleStatus() {
        return cy.xpath("//div[contains(text(),'Set Status')]//ancestor::div[@class='bg-white status-case-wrapper px-4 pt-4']//div//span")
    }

    verifyDisplayDefaultStatus() {
        this.lblVehicleStatus().then(($el1) => {
            var text1 = $el1.text().trim()
            cy.log("text1 is" + text1)
            this.common.btnAction("Edit").click()
            this.lblVehicleStatus().then(($el2) => {
                var text2 = $el2.text().trim()
                cy.log("text2 is" + text2)

                expect(text1).to.eql(text2)
            })
        })
    }

}

export default VehicleEdit;