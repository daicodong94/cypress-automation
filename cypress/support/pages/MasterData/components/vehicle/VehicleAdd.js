import LeftMenu from "../../../TMS/components/fragments/LeftMenu"
import CommonMasterData from "../common/CommonMasterData"
import HeaderMasterData from "../fragments/HeaderMasterData"
import SiteAdd from "../site/SiteAdd"
import Subcontractor from "../subcontractor/SubcontractorList"
import VehicleList from "./VehicleList"

class VehicleAdd {
    constructor() {
        this.vehicleList = new VehicleList()
        this.commonMD = new CommonMasterData()
        this.headerMD = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
        this.siteAdd = new SiteAdd()
        this.subcontractor = new Subcontractor()
    }

    btnConfirm(action) {
        return cy.xpath("//button//span[contains(text(), '" + action + "')]//parent::button")
    }

    tabInfor() {
        return cy.xpath("//div[@id='tab-information']//span[contains(text(),'Information')]")
    }

    tabDrivers() {
        return cy.xpath("//div[@id='tab-second']//p[text()='Drivers']")
    }

    //Add Vehicle
    verifyDefaultUI() {
        this.tabInfor().should("be.visible")
        this.tabDrivers().should("be.visible")
    }

    dtpIcon(name) {
        return cy.xpath("//div[text()='" + name + "']//following-sibling::*[local-name()='svg']")
    }

    verifySelectFields() {
        this.commonMD.ipSelectBox("Select Vehicle Type").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Vehicle Group").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Work Type").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Insurance").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Vehicle Brand").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Vehicle Model Year").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Fuel Type").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Provincal Sign").scrollIntoView().should("be.visible")
        this.commonMD.ipSelectBox("Select Plate Color").scrollIntoView().should("be.visible")
    }

    btnSearch() {
        return cy.xpath("//div[@class='flex w-full w-[480px]']//button")
    }

    verifyDefaultUIDriversTab() {
        this.tabDrivers().click({ force: true })
        this.headerMD.txtSearch().should("be.visible")
        this.btnSearch().should("be.visible")
        this.vehicleList.btnAction("Assign").should("be.visible")
        // this.commonMD.divNoData().should("be.visible")
        // this.commonMD.pageActive().should("have.text", "1")
    }

    divDate(date) {
        return cy.xpath("//div[text()='" + date + "']//following-sibling::div")
    }

    verifyDate() {
        cy.getCurrDate("hyphen").then((dateTime) => {
            this.divDate("Effective date:").should($el =>
                expect($el.text().trim()).to.equal(dateTime));
        })

        this.divDate("End date:").should(($date) => {
            const effDate = $date.text()
            expect(effDate).to.include("No end date")
        })
    }

    checkDataDefault() {
        this.commonMD.spanSelectedData().should("not.exist")
        this.verifyDate()
        this.divDate("Status").contains("Active")
    }

    getListVehicleModelYear() {
        var yearList = []
        var currYear = new Date().getFullYear()
        var maxYear = currYear + 15
        for (let index = 2010; index <= maxYear; index++) {
            yearList.push(index.toString());
        }
        return yearList
    }

    liValueDropdownList() {
        return cy.xpath("(//ul[@class='el-scrollbar__view el-select-dropdown__list'])[last()]//li")
    }

    checkDataServiceGroup(field, listData) {
        this.commonMD.ipSelectBox(field).scrollIntoView().click({ force: true })
        const expectedResult = listData
        const actualResult = []
        console.log(expectedResult)
        this.liValueDropdownList().each(($li) => {
            actualResult.push($li.text())
        }).then(() => {
            expect(expectedResult).to.include.members(actualResult)
            // if (expectedResult.length == actualResult.length){
            //     for (let i = 0; i < expectedResult.length; i++) {
            //         if (expectedResult[i] != actualResult[i]){
            //             console.log(expectedResult[i] + " -"  + actualResult[i])
            //         }
            //     }
            // }

        })
    }

    divLicensePlateLength() {
        return cy.xpath("//div[@class='text-sm text-secondary-45']")
    }

    countTextInputted() {
        this.divLicensePlateLength().should(($div) => {
            var text = $div.text()
            var formatText = text.trim().split('/')

            expect(formatText[0]).not.to.equal('0')
        })
    }

    inputTextToField(field, value) {
        this.commonMD.ipSelectBox(field).clear().type(value)
        this.countTextInputted()
    }

    selectRandomValue() {
        this.liValueDropdownList().its('length').then((len) => {
            this.liValueDropdownList().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    getCreatedData() {
        this.commonMD.openDetailsView()
    }

    dtpRegisteredDate() {
        return cy.xpath("//div[@class='el-picker-panel el-date-picker el-popper has-time tw tw-datepicker' and @x-placement]")
    }

    verifyRegisteredDatePickerDisplaying() {
        this.commonMD.ipSelectBox("Select Registered Date").scrollIntoView().click({ force: true })
        this.dtpRegisteredDate().should("be.visible")
    }

    inputDimension() {
        cy.randomNumber(1).then((width) => {
            this.commonMD.ipSelectBox("Enter Vehicle Width").clear().type(width)
            cy.randomNumber(1).then((length) => {
                this.commonMD.ipSelectBox("Enter Vehicle Length").clear().type(length)
                cy.randomNumber(1).then((height) => {
                    this.commonMD.ipSelectBox("Enter Vehicle Height").clear().type(height)
                    volume = width * length * height
                })
            })
        })
    }

    spanDatePickerYearMonth() {
        return cy.xpath("//span[@class='el-date-picker__header-label']")
    }

    verifyIssuedDateField(field) {
        var currentYear = new Date().getFullYear()
        var maxYear = currentYear + 544
        this.commonMD.ipSelectBox(field).scrollIntoView().click({ force: true })
        this.spanDatePickerYearMonth().contains(maxYear)
    }

    verifyDefaultStatusPopUp() {
        var expectRS
        this.commonMD.rdoGroup("Status").first().should("be.checked")
        cy.getCurrDate("slash").then((dateTime) => {
            expectRS = dateTime.split(" ").join("")
            this.commonMD.divEffectiveDataTime().should(($el) => {
                expect($el.text().trim().split(" ").join("")).contains(expectRS)
            })
        })
        this.commonMD.rdoGroup("End date / time").first().should("be.checked")
    }

    checkRadioStatusActive() {
        this.commonMD.rdoGroup("Status").first().check({ force: true })
        this.commonMD.verifyChecked("Status").first().should("have.attr", "aria-checked", "true")
    }

    blockInSideSetStatusPopUp() {
        return cy.xpath("//div[@class='el-dialog__body']//div[@class='col-span-12']")
    }

    checkRadioStatusInActive() {
        this.commonMD.rdoGroup("Status").last().check({ force: true })
        this.commonMD.verifyChecked("Status").last().should("have.attr", "aria-checked", "true")
        this.blockInSideSetStatusPopUp().should("have.length", 1)
    }

    divSelectDateTime() {
        return cy.xpath("//div[@class='mt-2']")
    }

    checkRadioEndDateTime() {
        this.commonMD.rdoGroup("End date / time").last().check({ force: true })
        this.divSelectDateTime().should("be.visible")
    }

    dtpSelectDateTime() {
        return cy.xpath("//div[@class='h-[40px] flex items-center border rounded-sm pr-3 border-neutral-5 text-sm text-disabled-25 cursor-pointer px-3 py-2 timer-active']")
    }

    verifySelectDateTimePickerDisplay() {
        this.commonMD.rdoGroup("End date / time").last().click({ force: true })
        this.divSelectDateTime().click({ force: true })
        this.dtpSelectDateTime().should("be.visible")
    }

    divQuitSettingStatusDialogNotDisPlaying() {
        return cy.xpath("//div[@class='el-message-box__wrapper' and contains(@style,'display: none')]")
    }

    verifyCancelSetStatus() {
        this.checkRadioStatusInActive()
        this.commonMD.btnConfirmSetStatus("Cancel").click({ force: true })
        this.divQuitSettingStatusDialogNotDisPlaying().should("not.exist")
    }

    //Assign Site
    lnkSelect() {
        return cy.xpath("//div[@class='text-sm text-primary-main font-medium cursor-pointer']")
    }

    btnAssign(name) {
        return cy.xpath("//div[@class='text-base font-medium' and text()='" + name + "']//following-sibling::button//span")
    }

    openSitePopUp() {
        this.btnAssign("Site").click({ force: true })
    }

    openSubcontractorPopUp() {
        this.btnAssign("Subcontractor").click({ force: true })
    }

    //page size
    ddlPagesize(popup) {
        return cy.xpath("//span[text()='" + popup + "']//ancestor::div[@role='dialog']//div[@class='flex justify-end mr-0 el-pagination']//input[@type='text']")
    }

    spanPageSize(size) {
        return cy.xpath('(//span[text()="' + size + '"])')
    }

    selectedValue() {
        return cy.xpath("//li[@class='el-select-dropdown__item selected']//span")
    }

    verifyTypeOfPageSize(popup) {
        this.ddlPagesize(popup).click({ force: true })
        this.spanPageSize("30/page").should("be.exist")
        this.spanPageSize("50/page").should("be.exist")
        this.spanPageSize("75/page").should("be.exist")
        this.spanPageSize("100/page").should("be.exist")
    }

    selectPageSize(popup, size) {
        this.ddlPagesize(popup).click({ force: true })
        cy.wait(1000)
        this.spanPageSize(size).last().click({ force: true })
        cy.wait(3000)
        this.selectedValue().first().should("have.text", size)
    }

    //go to page
    ipPageNumber() {
        return cy.xpath("//span[text()='Assign Site']//ancestor::div[@role='dialog']//div[@class='flex justify-end mr-0 el-pagination']//input[@type='number']")
    }

    pageActive() {
        return cy.xpath("//span[text()='Assign Site']//ancestor::div[@role='dialog']//div[@class='flex justify-end mr-0 el-pagination']//li[@class='number active']")
    }

    goToPageNumber(page) {
        this.ipPageNumber().clear()
        this.ipPageNumber().type(page)
        this.ipPageNumber().type('{enter}')
        cy.wait(2000)
        this.pageActive().should("have.text", page)
    }

    //search
    btnSearch() {
        return cy.xpath("//button[@class='el-button flex items-center el-button--primary el-button--mini']")
    }

    trimString(trackNumber) {
        var myArr = trackNumber.split("")
        return myArr
    }
    searchByCondition(condition) {
        cy.wait(1000)
        this.headerMD.txtSearch().first().clear({ force: true })
        this.headerMD.txtSearch().first().click({ force: true })
        // var characters = this.trimString(condition)
        // for (let index = 0; index < characters.length; index++) {
        //     this.headerMD.txtSearch().first().focus().type(characters[index], { force: true })
        //     cy.wait(200)
        // }
        // cy.wait(1000)
        this.headerMD.txtSearch().first().realType(condition, { force: true })
        cy.wait(2000)
        this.commonMD.btnSearchInPopUp().first().click({ force: true })
    }

    icClearSearchInputted() {
        return cy.xpath("//i[@class='el-input__icon el-icon-circle-close el-input__clear']")
    }

    clearSearchInputted() {
        this.headerMD.txtSearch().first().click({ force: true })
        this.icClearSearchInputted().click({ force: true })
    }

    lblSiteIDExpected() {
        return cy.xpath("(//table[@class='el-table__body']//span[contains(text(), 'ANL-S')])[1]")
    }

    lblSiteIDActual() {
        return cy.xpath("//div[@class='p-4 flex items-center justify-between border-t border-neutral-1']//div//div[contains(text(),'Site ID')]")
    }

    lblSiteCodeActual() {
        return cy.xpath("//div[@class='p-4 flex items-center justify-between border-t border-neutral-1']//div//div[contains(text(),'Site Code')]")
    }

    icDeleteSubContractor() {
        return cy.xpath("//*[local-name()='svg' and @class='cursor-pointer']//parent::div/div/div[@class='text-sm text-secondary-45' and contains(text(),'ID:')]")
    }

    iconDeleteSiteShouldBe() {
        this.icDeleteSubContractor().should("be.visible")
    }

    deleteAssignedSite() {
        this.icDeleteSubContractor().first().click({ force: true })
    }

    buttonAssignShouldBe() {
        this.btnAssign("Site").should("have.text", "Assign")
    }

    buttonReassignShouldBe() {
        this.btnAssign("Site").should("have.text", "Reassign")
    }

    //select and verify after select site
    selectAndVerifySite(button, condition) {
        if (button != "empty") {
            this.btnAssign(button).click({ force: true })
        }
        this.commonMD.searchByConditionV2(condition)
        cy.wait(2000)
        // this.lblSiteIDExpected().first().then(($el) => {
        //     var expectSiteID = $el.text().trim()
        //     cy.log("expectedSiteID is " + expectSiteID)

        this.lnkSelect().first().click({ force: true })
        cy.wait(2000)
        //     this.lblSiteIDActual().then(($el1) => {
        //         var actualSiteID = $el1.text().trim()
        //         cy.log("actualSiteID is " + actualSiteID)
        //         expect(actualSiteID).to.include(expectSiteID)
        //     })

        //     this.lblSiteCodeActual().then(($el2) => {
        //         var actualSiteCode = $el2.text().trim()
        //         cy.log("actualSiteCode is " + actualSiteCode)
        //         expect(actualSiteCode).to.include(condition)
        //     })
        // })

        this.iconDeleteSiteShouldBe()
        this.buttonReassignShouldBe()
    }

    btnCreateNewSite() {
        return cy.xpath("//button[@class='el-button base-icon-button el-button--primary el-button--mini']//span//div[contains(text(),'Create new Site')]")
    }

    clickButtonCreateNewSite() {
        this.btnCreateNewSite().click({ force: true })
        this.siteAdd.titleSiteAdd().should("be.visible")
        return new SiteAdd()
    }

    //select Subcontractor
    lblSubIDActual() {
        return cy.xpath("//div[@class='p-4 flex items-center justify-between border-t border-neutral-1']//div//div[contains(text(),'Subcontractor ID')]")
    }

    selectSubcontractor(button, condition) {
        if (button != "empty") {
            this.btnAssign(button).click({ force: true })
        }
        cy.wait(2000)
        this.searchByCondition(condition)
        this.lblSiteIDExpected().then(($el) => {
            var expectSubID = $el.text().trim()
            cy.log("expected Subcontractor ID is " + expectSubID)
            cy.wait(2000)
            this.lnkSelect().first().click({ force: true })
            cy.wait(1000)
            this.lblSubIDActual().then(($el1) => {
                var actualSubID = $el1.text().trim()
                cy.log("actual Sub ID is " + actualSubID)
                expect(actualSubID).to.include(expectSubID)
            })
        })

        this.iconDeleteSiteShouldBe()
    }

    btnCreateNewSubcontractor() {
        return cy.xpath("//button[@class='el-button base-icon-button el-button--primary el-button--mini']//span//div[contains(text(),'Create new Subcontractor')]")
    }

    clickButtonCreateNewSubcontractor() {
        this.btnCreateNewSubcontractor().click({ force: true })
        this.commonMD.btnConfirmHintPopup("Confirm", "1").click({ force: true })
        this.subcontractor.lblTitle().should("have.text", "Create Subcontractor")
        return new Subcontractor()
    }

    verifyUIAssignContractorScreenNoData() {
        this.btnCreateNewSubcontractor().should("be.visible")
        this.commonMD.icCloseDialog().should("be.visible")
    }

    verifyAfterDeleteSite() {
        this.deleteAssignedSite()
        this.buttonAssignShouldBe()
    }

    ///DRIVERS TAB

    //Assign driver
    driverInfor() {
        return cy.xpath("(//tr[@class='el-table__row'])[1]//td//span[@class='el-tooltip']")
    }

    chkSelectDriver() {
        return cy.xpath("(//tr[@class='el-table__row'])[1]//input[@type='checkbox']")
    }

    btnAssignDriver(button) {
        return cy.xpath("//div[@class='p-4 flex justify-end']//button//span[contains(text(),'" + button + "')]")
    }

    clickAssignButton() {
        this.vehicleList.btnAction("Assign").click({ force: true })
    }

    btnOccupied() {
        return cy.xpath("//input[@class='el-switch__input']")
    }

    occupiedOff() {
        return cy.xpath("//div[@class='el-switch el-tooltip custom-switch']")
    }

    occupiedOn() {
        return cy.xpath("//div[@class='el-switch el-tooltip custom-switch is-checked']")
    }

    verifyAssignDriver() {
        const expectedResult = []
        const actualResult = []
        this.driverInfor().each(($li) => {
            expectedResult.push($li.text().trim())
            console.log(expectedResult)
        }).then(() => {
            this.chkSelectDriver().first().click({ force: true })
            this.btnAssignDriver("Assign").click({ force: true })

            this.driverInfor().each(($li) => {
                actualResult.push($li.text().trim())
                console.log(actualResult)
            })

            expect(expectedResult).to.include.members(actualResult)

        })
    }

    trimString(trackNumber) {
        var myArr = trackNumber.split("")
        return myArr
    }
    searchDriver(condition) {
        this.headerMD.txtSearch().last().clear({ force: true })
        this.headerMD.txtSearch().last().click({ force: true })
        var characters = this.trimString(condition)
        for (let index = 0; index < characters.length; index++) {
            this.headerMD.txtSearch().last().focus().realType(characters[index], { force: true })
            cy.wait(100)
        }
        cy.wait(1000)
        this.btnSearch().last().click({ force: true })
        cy.wait(1000)
    }

    chkAll() {
        return cy.xpath("//div[@class='el-table__fixed-header-wrapper']//input")
    }

    selectAllData() {
        this.chkAll().first().click({ force: true })
    }

    //remove driver
    removeDriver() {
        this.chkSelectDriver().first().click({ force: true })
        this.vehicleList.btnAction("Remove").click({ force: true })
    }

    //assign driver
    assignDriver(occupied) {
        // if (driver != "empty") {
        //     this.verifyDefaultUIDriversTab()
        //     cy.wait(3000)
        //     this.clickAssignButton()
        //     cy.wait(1000)
        //     this.searchDriver(driver)
        //     this.verifyAssignDriver()
        //     cy.wait(2000)
        //     if (occupied == "yes") {
        //         this.btnOccupied().first().click({ force: true })
        //     }
        // }
        if (occupied == "yes") {
            this.tabDrivers().click({ force: true })
            this.btnOccupied().first().click({ force: true })
        }
    }

    confirmCreateVehicle(action, confirm, click) {
        if (action == "Save") {
            this.btnConfirm("Save").click({ force: true })
        } else {
            this.btnConfirm(action).first().click({ force: true })
            this.commonMD.btnConfirmHintPopupVer2(confirm).should("be.visible")
            if (click == "yes") {
                this.commonMD.btnConfirmHintPopupVer2(confirm).click({ force: true })
            }
        }
    }

    ddlSelect2(){
        return cy.xpath("(//div[@component='BaseSelectInput']//input[@placeholder='Select'])[2]")
    }

    createVehicle(site, subcontractor, occupied, action, confirm, click, verify) {
        //Infor
        this.commonMD.ipSelectBox("Select Vehicle Type").click({ force: true })
        // cy.wait(1000)
        this.selectRandomValue()
        this.commonMD.ipSelectBox("Select Vehicle Group").click({ force: true })
        this.selectRandomValue()
        this.commonMD.ipSelectBox("Select Work Type").click({ force: true })
        this.selectRandomValue()
        this.commonMD.ipSelectBox("Select Insurance").click({ force: true })
        // this.commonMD.ipSelectBox("Select Car Owner").click({ force: true })
        this.selectRandomValue()
        // this.commonMD.spanValue("Individual").last().click({ force: true })
        // this.commonMD.ipSelectBox("Select Title").click({ force: true })
        // this.selectRandomValue()
        // this.commonMD.ipSelectBox("Enter First Name").click().type("firstname")
        // this.commonMD.ipSelectBox("Enter Last Name").click().type("lastname")

        //Brand, model, and fuel type
        this.commonMD.ipSelectBox("Select Vehicle Brand").click({ force: true })
        this.commonMD.spanValue("คัมมิ่นส์").last().click({ force: true })
        this.commonMD.ipSelectBox("Select Vehicle Model Year").click({ force: true })
        // cy.wait(1000)
        this.selectRandomValue()
        this.commonMD.ipSelectBox("Select Fuel Type").click({ force: true })
        this.selectRandomValue()

        //assign Site
        if (site != "empty") {
            this.selectAndVerifySite("Site", site)
        }
        cy.wait(1000)
        //assign Subcontractor
        if (subcontractor != "empty") {
            this.selectSubcontractor("Subcontractor", subcontractor)
        }

        //License plate
        cy.randomStringAndNumber(6).then((license) => {
            this.commonMD.ipSelectBox("Enter Plate Number (ex. กข1234)").scrollIntoView().type(license)
        })
        this.commonMD.ipSelectBox("Select Provincal Sign").click({ force: true })
        // cy.wait(1000)
        this.selectRandomValue()
        this.commonMD.ipSelectBox("Select Plate Color").click({ force: true })
        this.selectRandomValue()
        this.dtpIcon("Select Registered Date").click({ force: true })
        cy.selectYear("thisYear", 0)
        cy.selectMonth("thisMonth")
        cy.selectDay("today")
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        //Dimension, Volume
        cy.randomNumber(2).then((width) => {
            if (width <= 1) {
                width = 2
            }
            this.commonMD.ipSelectBox("Enter Vehicle Width").scrollIntoView().type(width)
        })
        cy.randomNumber(2).then((length) => {
            if (length <= 1) {
                length = 2
            }
            this.commonMD.ipSelectBox("Enter Vehicle Length").type(length)
        })
        cy.randomNumber(2).then((height) => {
            if (height <= 1) {
                height = 2
            }
            this.commonMD.ipSelectBox("Enter Vehicle Height").type(height)
        })
        cy.randomNumber(2).then((max) => {
            if (max <= 1) {
                max = 2
            }
            this.commonMD.ipSelectBox("Enter Max Weight").type(max)
        })

        //Compulsory Third Party Insurance
        this.dtpIcon("Select Issued Date").scrollIntoView().click({ force: true })
        cy.selectYear("thisYear", 0)
        cy.selectMonth("thisMonth")
        cy.selectDay("today")
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        this.dtpIcon("Select Expired Date").scrollIntoView().click({ force: true })
        cy.selectYear(2024, 0)
        cy.selectMonth("thisMonth")
        cy.selectDay("today")
        this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        //Vehicle Insurance
        // cy.randomStringAndNumber(6).then((policy) => {
        //     this.commonMD.ipSelectBox("Enter Policy Number").first().scrollIntoView().type(policy)
        // })
        // this.commonMD.ipSelectBox("Select Vehicle Insurance Type").first().click({ force: true })
        // this.selectRandomValue()

        // this.dtpIcon("Select Effective Date").first().scrollIntoView().click({ force: true })
        // cy.selectYear("thisYear", 0)
        // cy.selectMonth("thisMonth")
        // cy.selectDay("today")
        // this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        // this.dtpIcon("Select End Date").first().scrollIntoView().click({ force: true })
        // cy.selectYear(2024, 0)
        // cy.selectMonth("thisMonth")
        // cy.selectDay("today")
        // this.commonMD.btnConfirmDatePicker("Set").click({ force: true })

        // this.commonMD.ipSelectBox("Select").first().click({ force: true })
        // this.selectRandomValue()
        // this.commonMD.ipSelectBox("Enter Insurance Coverage").first().scrollIntoView().type(234234)
        // this.commonMD.ipSelectBox("Select Insured Name Under").first().click({ force: true })
        // this.commonMD.spanValue("Company").last().click({ force: true })
        // this.commonMD.ipSelectBox("Enter Company Name").first().click({ force: true }).type("Company name Vehicle Insurance")
        // // this.selectRandomValue()

        // //Cargo Insurance
        // this.commonMD.ipSelectBox("Enter Policy Number").last().scrollIntoView().type(234235345)
        // this.commonMD.ipSelectBox("Enter Purchase From").click({ force: true })
        // this.commonMD.spanValue("All Now").last().click({ force: true })
        // // this.selectRandomValue()
        // this.dtpIcon("Select Effective Date").last().scrollIntoView().click({ force: true })
        // cy.selectYear("thisYear", 0)
        // cy.selectMonth("thisMonth")
        // cy.selectDay("today")
        // this.commonMD.btnConfirmDatePicker("Set").click({ force: true })
        // this.dtpIcon("Select End Date").last().scrollIntoView().click({ force: true })
        // cy.selectYear(2024, 0)
        // cy.selectMonth("thisMonth")
        // cy.selectDay("today")
        // this.commonMD.btnConfirmDatePicker("Set").click({ force: true })
        // cy.wait(500)
        // // this.commonMD.ipSelectBox("Select").last().click({ force: true })
        // this.ddlSelect2().click({ force: true })
        // this.selectRandomValue()
        // this.commonMD.ipSelectBox("Enter Insurance Coverage").last().scrollIntoView().type(566546)
        // this.commonMD.ipSelectBox("Select Insured Name Under").last().click({ force: true })
        // // this.selectRandomValue()
        // this.commonMD.spanValue("Company").last().click({ force: true })
        // this.commonMD.ipSelectBox("Enter Company Name").last().type("Company name")

        //assign driver
        this.assignDriver(occupied)

        //confirm
        this.commonMD.confirmCreateData(action, confirm, click, verify)

        return new VehicleList()
    }

}

export default VehicleAdd;