import CustomerEdit from "../../../support/pages/MasterData/components/customer/CustomerEdit"
import CustomerList from "../../../support/pages/MasterData/components/customer/CustomerList"
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"
import { Utility } from "../../../support/utility"
import Login from "../../../support/pages/TMS/components/login/Login"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier"
import CustomerAdd from "../../../support/pages/MasterData/components/customer/CustomerAdd"

describe("MasterData - Customer - Edit", () => {
    var data, objLogin, objLoginPRD
    var objCustomerEdit = new CustomerEdit(), objHome = new Homepage(), objCustomerList = new CustomerList(), objCustomerAdd = new CustomerAdd()
    var customerNameTH = "EtoECustomerTH"
    var id = "ANL-C000000000000429"
    var searchKeyProvince = "กาญจนบุรี", thirteenNumbers = "3253247567243"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
                objCustomerList = objHome.leftMenu.navigateToSubPage("Master Data", "Customer")
                cy.randomNumber(15).then((randomCustomerName) => {
                    customerNameTH += randomCustomerName
                    objCustomerAdd = objCustomerList.clickCreateButton()
                    objCustomerList = objCustomerAdd.createCustomer(customerNameTH, customerNameTH, "empty", "empty", "Save", "empty", "empty", "success")
                })
                objCustomerList.common.searchByCondition(customerNameTH)
                cy.wait(1000)
                objCustomerList.common.searchResultWithOutValue().dblclick({ force: true })
                objCustomerEdit = objCustomerList.clickEditButton()
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    // it('Create customer', () => {
    //     cy.randomNumber(15).then((randomCustomerName) => {
    //         customerNameTH += randomCustomerName
    //         objCustomerAdd = objCustomerList.clickCreateButton()
    //         objCustomerList = objCustomerAdd.createCustomer(customerNameTH, customerNameTH, "empty", "empty", "Save", "empty", "empty", "success")
    //     })
    // })

    it('TC_7	Check input 13 numbers into  "Company Tax ID" field \n', () => {
        //TC_7	Check input 13 numbers into  "Company Tax ID" field
        //cannot edit, cannot clear old data
        // objCustomerEdit.inputTextbox("Enter Company Tax ID", thirteenNumbers)
        // objCustomerEdit.common.btnAction("Save").click()
        // objCustomerEdit.common.msgSuccessShouldHaveText("Edit customer successfully")
        objCustomerEdit.customerAdd.ddlInformation("Enter Company Tax ID").click()
        // objCustomerEdit.ddlInformation("Enter Company Tax ID").type('{selectall}{backspace}')
        objCustomerEdit.customerAdd.ddlInformation("Enter Company Tax ID").type("12").clear()
    })
})