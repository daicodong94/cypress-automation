import CommonMasterData from "../MasterData/components/common/CommonMasterData";
import HeaderMasterData from "../MasterData/components/fragments/HeaderMasterData";
import CreateSite from "../MasterData/components/site/CreateSite";
import Abnormal from "../TMS/components/abnormal/Abnormal";
import ActivityTracking from "../TMS/components/activitytracking/ActivityTracking";
import Common from "../TMS/components/common/Common";
import Header from "../TMS/components/fragments/Header";
import LeftMenu from "../TMS/components/fragments/LeftMenu";
import DeliveryGood from "../TMS/components/Implement/DeliveryGood";
import Pickup from "../TMS/components/Implement/PickUp";
import DriverReceipt from "../TMS/components/receipt/DriverReceipt";
import ShippersReceipt from "../TMS/components/receipt/ShippersReceipt";
import Schedule from "../TMS/components/schedule/Schedule";
import CreateShippingOrder from "../TMS/components/shippingorder/CreateShippingOrder";
import ShippingOrder from "../TMS/components/shippingorder/ShippingOrder";
import DispatchOrderTracking from "../TMS/components/track/DispatchOrderTracking";
import ShippingOrderTracking from "../TMS/components/track/ShippingOrderTracking";

class E2E {
    constructor() {
        this.header = new Header()
        this.headerMD = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.commonMD = new CommonMasterData()
        this.pickUp = new Pickup()
        this.deliveryGood = new DeliveryGood()
        this.createSite = new CreateSite()
        this.shippingorder = new ShippingOrder()
        this.createshippingorder = new CreateShippingOrder()
        this.schedule = new Schedule()
        this.activityTracking = new ActivityTracking()
        this.shippingOrderTracking = new ShippingOrderTracking()
        this.dispatchOrderTracking = new DispatchOrderTracking()
        this.driverReceipt = new DriverReceipt()
        this.shipperReceipt = new ShippersReceipt()
        this.abnormal = new Abnormal()
    }

    spanSearchSuggestionDeliveryPlanPage(searchKey) {
        return cy.xpath("(//div[@class='select-style']//span[text()='" + searchKey + "'])[last()]")
    }

    shippingAndDispatchOrderTrackingStatus(status) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//span[text()= "' + status + '"]')
    }

    searchByCondition(searchField, condition) {
        // this.header.ipSearchBox(searchField).last().clear()
        this.header.ipSearchBox(searchField).last().click({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox(searchField).last().clear()
        this.header.ipSearchBox(searchField).last().focus().type(condition)
        this.header.ipSearchBox(searchField).last().type("{enter}")
        cy.wait(1000)
        this.spanSearchSuggestionDeliveryPlanPage(condition).should("be.visible", { timeout: 10000 })
        this.spanSearchSuggestionDeliveryPlanPage(condition).click({ force: true })
        cy.wait(1000)
        this.header.btnAtHeader("inquire").click({ force: true })
    }

    resultsSearch(result) {
        return cy.xpath('(//ul[@class="el-scrollbar__view el-select-dropdown__list"]//span[contains(text(),"' + result + '")])[last()]')
    }

    gotoActivityTrackingandOpenDetails(waybill, tabname, index) {
        this.leftMenu.navigateToPage("Activity Tracking")
        this.pickUp.tabShipment(tabname).click({ force: true })
        cy.wait(1000)
        this.pickUp.txtSearch(index).click({ force: true }).realType(waybill)
        cy.wait(1000)
        this.resultsSearch(waybill).click({ force: true })
        cy.wait(1000)
        this.pickUp.iconSearch(index).click({ force: true })
        cy.wait(1000)
        this.pickUp.shipmentNo(waybill).dblclick({ force: true })
    }


    pickUpAndDeliveryOrderSuccess(condition, filePath, orderStatus, split, changePackagesReceived) {
        this.leftMenu.navigateToSubPage("Implement", "Pick Up")
        this.common.searchByClickSuggestion("Dispatch Number", condition)
        this.common.openDetailView()
        cy.wait(1000)
        this.pickUp.wayBillNumber().invoke('text').then((wayBillNumber) => {
            var waybill = wayBillNumber.trim().substring(15)
            cy.log("waybillNumber = " + waybill)
            // Update shiment status to Shipment Arrival Pickup
            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
            cy.wait(5000)
            this.activityTracking.orderNumber().should("be.visible")
            this.activityTracking.jobNumber().should("be.visible")
            this.activityTracking.orderNumber().invoke('text').then((orderNum) => {
                var orderNumber = orderNum.trim()
                cy.log("orderNumber =" + orderNumber)
                this.activityTracking.jobNumber().invoke('text').then((jobNum) => {
                    var jobNumber = jobNum.trim()
                    cy.log("jobNumber =" + jobNumber)
                    this.pickUp.upDateShipmentStatus("Update Status for Driver", "Shipment_Arrival_Pickup")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    if (orderStatus == "split") {
                        this.shippingAndDispatchOrderTrackingStatus("order_processing").should("be.visible")
                    } else if (orderStatus == "normal") {
                        this.shippingAndDispatchOrderTrackingStatus("order_picking_up").should("be.visible")
                    }
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_picking_up").should("be.visible")
                    // Update Shipment to status Shipment Loading
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.pickUp.upDateShipmentStatus("Update Status for Driver", "Shipment_Loading")
                    // Verify Shipping Order Tracking Status
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    if (orderStatus == "split") {
                        this.shippingAndDispatchOrderTrackingStatus("order_processing").should("be.visible")
                    } else if (orderStatus == "normal") {
                        this.shippingAndDispatchOrderTrackingStatus("order_picking_up").should("be.visible")
                    }
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_picking_up").should("be.visible")
                    // Pick up Completed order
                    this.leftMenu.navigateToSubPage("Implement", "Pick Up")
                    this.common.searchByClickSuggestion("waybill number", waybill)
                    this.common.openDetailView()
                    this.pickUp.chooseCompletionTime(10)
                    this.pickUp.clickbtnFunctionPickup("pickup completed")
                    cy.wait(1000)
                    this.pickUp.confirmPickup("yes")
                    this.deliveryGood.msgDeliveryCompletedSuccess().should("be.visible")
                    // cy.wait(1000)
                    this.pickUp.btnconfirm().click({ force: true })
                    cy.wait(2000)
                    // this.pickUp.dispatchPickupStatus("SHIPMENT_ONTHEWAY").should("be.visible")
                    // Update Shipment status Shipment Arrival Destination
                    cy.reload()
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.pickUp.shipmentStatus("Shipment_Ontheway").should("be.visible")
                    this.pickUp.upDateShipmentStatus("Update Status for Driver", "Shipment_Arrival_Destination")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    if (orderStatus == "split") {
                        this.shippingAndDispatchOrderTrackingStatus("order_processing").should("be.visible")
                    } else if (orderStatus == "normal") {
                        this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    }
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    cy.wait(500)
                    this.shippingAndDispatchOrderTrackingStatus("job_arrival_destination").should("be.visible")
                    // Update Shipment status to Shipment Unloading
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.pickUp.upDateShipmentStatus("Update Status for Driver", "Shipment_Unloading")
                    // Verify Shipping Order Tracking Status
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    if (orderStatus == "split") {
                        this.shippingAndDispatchOrderTrackingStatus("order_processing").should("be.visible")
                    } else if (orderStatus == "normal") {
                        this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    }
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_handed_over_receiver").should("be.visible")
                    //15. Delivery Completed
                    this.leftMenu.navigateToSubPage("Implement", "Deliver Goods ")
                    this.common.searchByInputOrderNumber("please enter the tracking number", waybill)
                    this.deliveryGood.deliveryCompleted()

                    //new: 16. Go to Track > Dispatch Order Tracking > View Detail this order with status = job_delivered > Update Data Correction
                    cy.reload()
                    // this.leftMenu.btnCloseMenu().last().click({ force: true })
                    cy.wait(10000)
                    this.leftMenu.navigateToSubPage("Track", "Dispatch Order Tracking")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    cy.wait(500)
                    this.common.openDetailView()
                    this.dispatchOrderTracking.dataCorrection(changePackagesReceived)
                    this.dispatchOrderTracking.lblSubcontractor().then(($el) => {
                        const expectSub = $el.text().trim()
                        // this.dispatchOrderTracking.leftMenu.btnCloseMenu().last().click({ force: true })
                        cy.reload()
                        // verify data correction
                        this.driverReceipt = this.dispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        // this.driverReceipt.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        this.driverReceipt.common.searchByClickSuggestion("waybill number", waybill)
                        this.driverReceipt.common.openDetailView()
                        this.driverReceipt.navigateToMissionInforTab()
                        this.driverReceipt.lblCarrier().should("have.text", expectSub)
                        this.driverReceipt.navigateToPODInforTab()
                        this.driverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                        cy.get('@expectedLicense').then(expectedLicense => {
                            cy.get('@actualLicense').then(actualLicense => {
                                expect(expectedLicense).eq(actualLicense)
                                this.driverReceipt.leftMenu.btnCloseMenu().last().click({ force: true })
                            })
                        })
                    })
                    // Verify shipment status = Shipment_Delivered
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.pickUp.shipmentStatus("Shipment_Delivered").should("be.visible")
                    // Verify Shipping Order Tracking Status
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    cy.wait(2000)
                    if (orderStatus == "split") {
                        if (split == "inprogress") {
                            this.shippingAndDispatchOrderTrackingStatus("order_processing").should("be.visible")
                        } else if (split == "success") {
                            this.shippingAndDispatchOrderTrackingStatus("order_delivered").should("be.visible")
                        }
                    } else if (orderStatus == "normal") {
                        this.shippingAndDispatchOrderTrackingStatus("order_delivered").should("be.visible")
                    }
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    // Confirm Receipt
                    this.leftMenu.navigateToSubPage("Receipt", "Driver Receipt ")
                    this.common.searchByClickSuggestion("waybill number", waybill)
                    this.common.openDetailView()
                    cy.wait(1000)
                    this.common.addFileAttachments(filePath)
                    this.common.btnConfirmAgainHintPopup("Confirm POD").first().click({ force: true })
                    this.pickUp.confirmPickup("yes")
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    if (orderStatus == "normal") {
                        // Send Proof Delivery
                        this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                        this.common.openDetailView()
                        cy.wait(1000)
                        // this.common.addFileAttachments(filePath)
                        this.common.btnConfirmAgainHintPopup("proof of delivery ").click({ force: true })
                        this.pickUp.confirmPickup("yes")
                        this.shipperReceipt.confirmSendEmail("yes")
                        this.pickUp.msgSuccess("done successfully").should("be.visible")
                        this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                        // Verify Shipment status Finished
                        this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                        cy.wait(2000)
                        this.activityTracking.shipmentStatusFinished()
                        // Verify Order status finished
                        cy.go('back')
                        cy.go('back')
                        cy.wait(2000)
                        this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                        cy.wait(2000)
                        this.activityTracking.orderStatusFinished()
                        // Verify Job status finished
                        cy.go('back')
                        cy.go('back')
                        cy.wait(2000)
                        this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                        cy.wait(2000)
                        this.activityTracking.jobStatusFinished()
                    } else if (orderStatus == "split") {
                        if (split == "success") {
                            // Send Proof Delivery
                            this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                            this.common.openDetailView()
                            // this.common.addFileAttachments(filePath)
                            this.common.btnConfirmAgainHintPopup("proof of delivery ").click({ force: true })
                            this.pickUp.confirmPickup("yes")
                            this.shipperReceipt.confirmSendEmail("yes")
                            this.pickUp.msgSuccess("done successfully").should("be.visible")
                            this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                            // Verify Shipment status Finished
                            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                            cy.wait(2000)
                            this.activityTracking.shipmentSplitStatusFinished()
                            // Verify Order status finished
                            cy.go('back')
                            cy.go('back')
                            cy.wait(2000)
                            this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                            cy.wait(2000)
                            this.activityTracking.orderSplitStatusFinished()
                            // Verify Job status finished
                            cy.go('back')
                            cy.go('back')
                            cy.wait(2000)
                            this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                            cy.wait(2000)
                            this.activityTracking.jobSplitStatusFinished()
                        } else if (split == "inprogress") {
                            cy.clearAllCookies()
                            cy.reload()
                        }
                    }
                })
            })
        })
    }

    createOrderAndDirectSchedule(trackingNum, Customer, product, ShippingSite, DestinationSite, carrier, name, driver, consignment) {
        // this.leftMenu.navigateToPage("Shipping order")
        // this.shippingorder.navigateToCreateShippingOrderPage()
        this.createshippingorder.createNewShippingOrder("Direct Schedule", trackingNum, Customer, product, "BA", ShippingSite, DestinationSite, 20);
        cy.wait(3000)
        // this.leftMenu.navigateToPage("Schedule ")
        // this.common.searchByClickSuggestion("dispatch number", trackingNum)
        // this.common.openDetailView()
        // cy.wait(2000)
        this.pickUp.selectCarrier(carrier, name)
        this.common.radioBtnDifferentFromRequest().click()
        this.schedule.textAreaReason().type("Create schedule with different from requested")
        this.schedule.lnkAddVehicle().click()
        this.schedule.selectDriver(driver)
        cy.wait(5000)
        this.pickUp.selectConsignment(consignment)
        this.schedule.btnAction("Confirm and Dispatch").click()
        cy.wait(1000)
        this.common.msgSuccess().should("be.visible")
    }

    lblToDayDOB(today) {
        return cy.xpath("//td[@class='available default']//span[contains(.,'" + today + "')]")
    }

    btnClickToUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//button")
    }

    ipClickToUpload(typePhoto) {
        return cy.xpath("//div[contains(text(),'" + typePhoto + "')]/following-sibling::div//input[@class='invisible absolute']")
    }

    dtpDateOfBirth(typeDate) {
        return cy.xpath("//div[contains(text(),'" + typeDate + "')]/following-sibling::span//div//div[2]//div[1]")
    }

    lblToDay(today) {
        return cy.xpath("//td[@class='available today']//span[contains(.,'" + today + "')]")
    }

    lblMonthDOB() {
        return cy.xpath("//span[@class='el-date-picker__header-label']").last()
    }

    lblMonth(month) {
        return cy.xpath("//table[@class='el-month-table']//div//a[contains(text(),'" + month + "')]")
    }

    lblDayPrevMonth(day) {
        return cy.xpath("//td[@class='prev-month']//span[contains(.,'" + day + "')]")
    }

    btnAssign(name) {
        return cy.xpath("//div[@class='text-base font-medium' and text()='" + name + "']//following-sibling::button//span")
    }
    btnSearch() {
        return cy.xpath("//div[@class='flex w-full w-[480px]']//button")
    }
    lblSiteIDExpected() {
        return cy.xpath("//tr[@class='el-table__row']//span[contains(text(), 'ANL')]")
    }
    lnkSelect() {
        return cy.xpath("//div[@class='text-sm text-primary-main font-medium cursor-pointer']")
    }
    lblSubIDActual() {
        return cy.xpath("//div[@class='p-4 flex items-center justify-between border-t border-neutral-1']//div//div[contains(text(),'Subcontractor ID')]")
    }
    icDeleteSite() {
        return cy.xpath("//*[local-name()='svg' and @class='cursor-pointer']")
    }

    iconDeleteSiteShouldBe() {
        this.icDeleteSite().should("be.visible")
    }
    selectSubcontractor(button, condition) {
        if (button != "empty") {
            this.btnAssign(button).click({ force: true })
        }
        this.searchByCondition(condition)
        this.lblSiteIDExpected().first().then(($el) => {
            var expectSubID = $el.text().trim()
            cy.log("expected Subcontractor ID is " + expectSubID)

            this.lnkSelect().first().click({ force: true })

            this.lblSubIDActual().then(($el1) => {
                var actualSubID = $el1.text().trim()
                cy.log("actual Sub ID is " + actualSubID)
                expect(actualSubID).to.include(expectSubID)
            })
        })

        this.iconDeleteSiteShouldBe()
    }

    teperatureAbnormal(condition, filePath, changePackagesReceived) {
        this.leftMenu.navigateToSubPage("Implement", "Pick Up")
        this.common.searchByClickSuggestion("Dispatch Number", condition)
        this.common.openDetailView()
        cy.wait(2000)
        this.pickUp.wayBillNumber().invoke('text').then((wayBillNumber) => {
            var waybill = wayBillNumber.trim().substring(15)
            cy.log("waybillNumber = " + waybill)
            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
            cy.wait(5000)
            this.activityTracking.orderNumber().should("be.visible")
            this.activityTracking.jobNumber().should("be.visible")
            this.activityTracking.orderNumber().invoke('text').then((orderNum) => {
                var orderNumber = orderNum.trim()
                cy.log("orderNumber =" + orderNumber)
                this.activityTracking.jobNumber().invoke('text').then((jobNum) => {
                    var jobNumber = jobNum.trim()
                    cy.log("jobNumber =" + jobNumber)
                    // Update status to Shipment_Delivered with out of range teperature
                    this.activityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered", "yes")
                    // Processing Abnormal Order
                    this.leftMenu.navigateToPage("Abnormal ")
                    this.abnormal.confirmAndProcessingOrder(orderNumber, "delivery over temperature")
                    //new: 16. Go to Track > Dispatch Order Tracking > View Detail this order with status = job_delivered > Update Data Correction
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Dispatch Order Tracking")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    cy.wait(500)
                    this.common.openDetailView()
                    this.dispatchOrderTracking.dataCorrection(changePackagesReceived)
                    this.dispatchOrderTracking.lblSubcontractor().then(($el) => {
                        const expectSub = $el.text().trim()
                        // cy.reload()
                        // verify data correction
                        this.driverReceipt = this.dispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        this.driverReceipt.common.searchByClickSuggestion("waybill number", waybill)
                        this.driverReceipt.common.openDetailView()
                        this.driverReceipt.navigateToMissionInforTab()
                        this.driverReceipt.lblCarrier().should("have.text", expectSub)
                        this.driverReceipt.navigateToPODInforTab()
                        this.driverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                        cy.get('@expectedLicense').then(expectedLicense => {
                            cy.get('@actualLicense').then(actualLicense => {
                                expect(expectedLicense).eq(actualLicense)
                                // this.driverReceipt.leftMenu.btnCloseMenu().last().click({ force: true })
                            })
                        })
                    })
                    // Confirm Receipt
                    // this.leftMenu.navigateToSubPage("Receipt", "Driver Receipt ")
                    // this.common.searchByClickSuggestion("waybill number", waybill)
                    // this.common.openDetailView()
                    // cy.wait(1000)
                    this.driverReceipt.confirmPOD(filePath)
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    // Send Proof Delivery
                    this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                    this.common.openDetailView()
                    cy.wait(1000)
                    this.driverReceipt.proofOfDelivery()
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                    // Verify Shipment status Finished
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    cy.wait(2000)
                    this.activityTracking.shipmentStatusFinished()
                    // Verify Order status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                    cy.wait(2000)
                    this.activityTracking.orderStatusFinished()
                    // Verify Job status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                    cy.wait(2000)
                    this.activityTracking.jobStatusFinished()
                })
            })
        })
    }

    latePickUpAbnormal(condition, filePath, changePackagesReceived) {
        this.leftMenu.navigateToSubPage("Implement", "Pick Up")
        this.common.searchByClickSuggestion("Dispatch Number", condition)
        this.common.openDetailView()
        cy.wait(2000)
        this.pickUp.wayBillNumber().invoke('text').then((wayBillNumber) => {
            var waybill = wayBillNumber.trim().substring(15)
            cy.log("waybillNumber = " + waybill)
            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
            cy.wait(5000)
            this.activityTracking.orderNumber().should("be.visible")
            this.activityTracking.jobNumber().should("be.visible")
            this.activityTracking.orderNumber().invoke('text').then((orderNum) => {
                var orderNumber = orderNum.trim()
                cy.log("orderNumber =" + orderNumber)
                this.activityTracking.jobNumber().invoke('text').then((jobNum) => {
                    var jobNumber = jobNum.trim()
                    cy.log("jobNumber =" + jobNumber)
                    // Update status to Shipment_Delivered
                    this.activityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                    // Confirm and processing order Abnormal
                    this.leftMenu.navigateToPage("Abnormal ")
                    this.abnormal.confirmAndProcessingOrder(orderNumber, "pick up late")
                    //new: 16. Go to Track > Dispatch Order Tracking > View Detail this order with status = job_delivered > Update Data Correction
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Dispatch Order Tracking")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    cy.wait(500)
                    this.common.openDetailView()
                    this.dispatchOrderTracking.dataCorrection(changePackagesReceived)
                    this.dispatchOrderTracking.lblSubcontractor().then(($el) => {
                        const expectSub = $el.text().trim()
                        cy.reload()
                        // verify data correction
                        this.driverReceipt = this.dispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        this.driverReceipt.common.searchByClickSuggestion("waybill number", waybill)
                        this.driverReceipt.common.openDetailView()
                        this.driverReceipt.navigateToMissionInforTab()
                        this.driverReceipt.lblCarrier().should("have.text", expectSub)
                        this.driverReceipt.navigateToPODInforTab()
                        this.driverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                        cy.get('@expectedLicense').then(expectedLicense => {
                            cy.get('@actualLicense').then(actualLicense => {
                                expect(expectedLicense).eq(actualLicense)
                                // this.driverReceipt.leftMenu.btnCloseMenu().last().click({ force: true })
                            })
                        })
                    })
                    // Confirm Receipt
                    // this.leftMenu.navigateToSubPage("Receipt", "Driver Receipt ")
                    // this.common.searchByClickSuggestion("waybill number", waybill)
                    // this.common.openDetailView()
                    // cy.wait(1000)
                    this.driverReceipt.confirmPOD(filePath)
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    // Send Proof Delivery
                    this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                    this.common.openDetailView()
                    cy.wait(1000)
                    this.driverReceipt.proofOfDelivery()
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                    // Verify Shipment status Finished
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    cy.wait(2000)
                    this.activityTracking.shipmentStatusFinished()
                    // Verify Order status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                    cy.wait(2000)
                    this.activityTracking.orderStatusFinished()
                    // Verify Job status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                    cy.wait(2000)
                    this.activityTracking.jobStatusFinished()
                })
            })
        })
    }

    updateShipmentStatus(condition, filePath, changePackagesReceived) {
        this.leftMenu.navigateToSubPage("Implement", "Pick Up")
        this.common.searchByClickSuggestion("Dispatch Number", condition)
        this.common.openDetailView()
        cy.wait(1000)
        this.pickUp.wayBillNumber().invoke('text').then((wayBillNumber) => {
            var waybill = wayBillNumber.trim().substring(15)
            cy.log("waybillNumber = " + waybill)
            // Update shiment status to Shipment Arrival Pickup
            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
            cy.wait(5000)
            this.activityTracking.orderNumber().should("be.visible")
            this.activityTracking.jobNumber().should("be.visible")
            this.activityTracking.orderNumber().invoke('text').then((orderNum) => {
                var orderNumber = orderNum.trim()
                cy.log("orderNumber =" + orderNumber)
                this.activityTracking.jobNumber().invoke('text').then((jobNum) => {
                    var jobNumber = jobNum.trim()
                    cy.log("jobNumber =" + jobNumber)

                    // Update status to Shipment_Arrival_Pickup
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Arrival_Pickup")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_picking_up").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_picking_up").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update status order to Shipment_Pickup_Dock_Assigned
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Pickup_Dock_Assigned")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_picking_up").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_picking_up").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update Shipment to status Shipment_Loading
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Loading")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_picking_up").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_picking_up").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update Shipment Status to Shipment_Ontheway
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Ontheway")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_ontheway").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update order status to Shipment_Arrival_Destination
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Arrival_Destination")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    cy.wait(500)
                    this.shippingAndDispatchOrderTrackingStatus("job_arrival_destination").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update order status to Shipment_Deliver_Dock_Assigned
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Deliver_Dock_Assigned")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    cy.wait(500)
                    this.shippingAndDispatchOrderTrackingStatus("job_arrival_destination").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update Shipment status to Shipment_Unloading
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Unloading")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_ontheway").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_handed_over_receiver").should("be.visible")
                    this.leftMenu.closeTab()

                    // Update Shipment status to Shipment_Delivered
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    this.activityTracking.upDateOrderStatus("Update Status", "Shipment_Delivered")
                    // Verify Shipping Order Tracking Status
                    this.leftMenu.navigateToSubPage("Track", "Shipping Order Tracking ")
                    this.searchByCondition("shipping order", orderNumber)
                    this.shippingAndDispatchOrderTrackingStatus("order_delivered").should("be.visible")
                    this.leftMenu.closeTab()
                    // Verify Dispatch Order Tracking Status
                    this.leftMenu.navigateToSubPageActive("Dispatch Order Tracking ")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    this.leftMenu.closeTab()

                    //new: 16. Go to Track > Dispatch Order Tracking > View Detail this order with status = job_delivered > Update Data Correction
                    this.leftMenu.navigateToSubPage("Track", "Dispatch Order Tracking")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    cy.wait(500)
                    this.common.openDetailView()
                    this.dispatchOrderTracking.dataCorrection(changePackagesReceived)
                    this.dispatchOrderTracking.lblSubcontractor().then(($el) => {
                        const expectSub = $el.text().trim()
                        cy.reload()
                        // verify data correction
                        this.driverReceipt = this.dispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        this.driverReceipt.common.searchByClickSuggestion("waybill number", waybill)
                        this.driverReceipt.common.openDetailView()
                        this.driverReceipt.navigateToMissionInforTab()
                        this.driverReceipt.lblCarrier().should("have.text", expectSub)
                        this.driverReceipt.navigateToPODInforTab()
                        this.driverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                        cy.get('@expectedLicense').then(expectedLicense => {
                            cy.get('@actualLicense').then(actualLicense => {
                                expect(expectedLicense).eq(actualLicense)
                                // this.driverReceipt.leftMenu.btnCloseMenu().last().click({ force: true })
                            })
                        })
                    })
                    // Confirm Receipt
                    // this.leftMenu.navigateToSubPage("Receipt", "Driver Receipt ")
                    // this.common.searchByClickSuggestion("waybill number", waybill)
                    // this.common.openDetailView()
                    // cy.wait(1000)
                    this.driverReceipt.confirmPOD(filePath)
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    // Send Proof Delivery
                    this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                    this.common.openDetailView()
                    cy.wait(1000)
                    this.driverReceipt.proofOfDelivery()
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                    // Verify Shipment status Finished
                    this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                    cy.wait(2000)
                    this.activityTracking.shipmentStatusFinished()
                    // Verify Order status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                    cy.wait(2000)
                    this.activityTracking.orderStatusFinished()
                    // Verify Job status finished
                    cy.go('back')
                    cy.go('back')
                    cy.wait(2000)
                    this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                    cy.wait(2000)
                    this.activityTracking.jobStatusFinished()
                })
            })
        })
    }

    updateShipmentStatusToShipmentDelivered(condition, filePath, changePackagesReceived, orderStatus, split) {
        this.leftMenu.navigateToSubPage("Implement", "Pick Up")
        this.common.searchByClickSuggestion("Dispatch Number", condition)
        this.common.openDetailView()
        cy.wait(1000)
        this.pickUp.wayBillNumber().invoke('text').then((wayBillNumber) => {
            var waybill = wayBillNumber.trim().substring(15)
            cy.log("waybillNumber = " + waybill)
            // Update shiment status to Shipment Arrival Pickup
            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
            cy.wait(5000)
            this.activityTracking.orderNumber().should("be.visible")
            this.activityTracking.jobNumber().should("be.visible")
            this.activityTracking.orderNumber().invoke('text').then((orderNum) => {
                var orderNumber = orderNum.trim()
                cy.log("orderNumber =" + orderNumber)
                this.activityTracking.jobNumber().invoke('text').then((jobNum) => {
                    var jobNumber = jobNum.trim()
                    cy.log("jobNumber =" + jobNumber)
                    // Update status to Shipment_Delivered
                    this.activityTracking.updateShipmentStatusToShipmentDelivered("Update Status", "Shipment_Delivered")
                    //new: 16. Go to Track > Dispatch Order Tracking > View Detail this order with status = job_delivered > Update Data Correction
                    cy.reload()
                    this.leftMenu.navigateToSubPage("Track", "Dispatch Order Tracking")
                    this.searchByCondition("Job Number (R)", jobNumber)
                    this.shippingAndDispatchOrderTrackingStatus("job_delivered").should("be.visible")
                    cy.wait(500)
                    this.common.openDetailView()
                    this.dispatchOrderTracking.dataCorrection(changePackagesReceived)
                    this.dispatchOrderTracking.lblSubcontractor().then(($el) => {
                        const expectSub = $el.text().trim()
                        cy.reload()
                        // verify data correction
                        this.driverReceipt = this.dispatchOrderTracking.leftMenu.navigateToSubPage("Receipt", "Driver Receipt")
                        this.driverReceipt.common.searchByClickSuggestion("waybill number", waybill)
                        this.driverReceipt.common.openDetailView()
                        this.driverReceipt.navigateToMissionInforTab()
                        this.driverReceipt.lblCarrier().should("have.text", expectSub)
                        this.driverReceipt.navigateToPODInforTab()
                        this.driverReceipt.divPlateNumber().invoke('text').as('actualLicense')
                        cy.get('@expectedLicense').then(expectedLicense => {
                            cy.get('@actualLicense').then(actualLicense => {
                                expect(expectedLicense).eq(actualLicense)
                            })
                        })
                    })
                    // Confirm Receipt
                    cy.wait(200)
                    this.driverReceipt.confirmPOD(filePath)
                    this.pickUp.msgSuccess("done successfully").should("be.visible")
                    if (orderStatus == "normal") {
                        // Send Proof Delivery
                        this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                        this.common.openDetailView()
                        cy.wait(1000)
                        this.driverReceipt.proofOfDelivery()
                        this.pickUp.msgSuccess("done successfully").should("be.visible")
                        this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                        // Verify Shipment status Finished
                        this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                        cy.wait(2000)
                        this.activityTracking.shipmentStatusFinished()
                        // Verify Order status finished
                        cy.go('back')
                        cy.go('back')
                        cy.wait(2000)
                        this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                        cy.wait(2000)
                        this.activityTracking.orderStatusFinished()
                        // Verify Job status finished
                        cy.go('back')
                        cy.go('back')
                        cy.wait(2000)
                        this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                        cy.wait(2000)
                        this.activityTracking.jobStatusFinished()
                    } else if (orderStatus == "split") {
                        if (split == "success") {
                            // Send Proof Delivery
                            this.pickUp.dispatchPickupStatus("wait").should("be.visible")
                            this.common.openDetailView()
                            this.driverReceipt.proofOfDelivery()
                            this.pickUp.msgSuccess("done successfully").should("be.visible")
                            this.pickUp.dispatchPickupStatus("backed").should("be.visible")
                            // Verify Shipment status Finished
                            this.gotoActivityTrackingandOpenDetails(waybill, "tab-shipment", "3")
                            cy.wait(2000)
                            this.activityTracking.shipmentSplitStatusFinished()
                            // Verify Order status finished
                            cy.go('back')
                            cy.go('back')
                            cy.wait(2000)
                            this.gotoActivityTrackingandOpenDetails(orderNumber, "tab-order", "1")
                            cy.wait(2000)
                            this.activityTracking.orderSplitStatusFinished()
                            // Verify Job status finished
                            cy.go('back')
                            cy.go('back')
                            cy.wait(2000)
                            this.gotoActivityTrackingandOpenDetails(jobNumber, "tab-job", "2")
                            cy.wait(2000)
                            this.activityTracking.jobSplitStatusFinished()
                        } else if (split == "inprogress") {
                            cy.log("Run test with order B")
                        }
                    }
                })
            })
        })
    }
}
export default E2E;