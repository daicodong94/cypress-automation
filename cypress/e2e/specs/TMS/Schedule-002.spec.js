import Login from "../../../support/pages/TMS/components/login/Login"
import DeliveryPlan from "../../../support/pages/TMS/components/plan/DeliveryPlan"
import Schedule from "../../../support/pages/TMS/components/schedule/Schedule"
import Function from "../../../support/pages/TMS/components/setup/Function"
import CreateShippingOrder from "../../../support/pages/TMS/components/shippingorder/CreateShippingOrder"
import ShippingOrder from "../../../support/pages/TMS/components/shippingorder/ShippingOrder"
import { Utility } from "../../../support/utility"
import LoginSupplier from "../../../support/pages/TMS/components/login/LoginSupplier";
import Homepage from "../../../support/pages/TMS/components/homepage/Homepage"

describe("Schedule02", () => {
    var data, objLogin, objLoginPRD, objHome = new Homepage(), objShippingOrder = new ShippingOrder(),
        objCreateShippingOrder = new CreateShippingOrder(), objDeliveryPlan = new DeliveryPlan(),
        objFunction = new Function(), objSchedule = new Schedule()
    var trackNumber, trackNumber2, withdrawSchedule, carrier = "SYS_DEF_INDIVIDUAL", consignment = "Services for Carrier", driver = "Auto Driver",
        customer = "Automation Company", product = "Service for Shipper", temperature = "Chill",
        siteA = "SiteTestAOne", siteB = "SiteTestB", siteC = "SiteTestC", siteD = "SiteTestD"

    beforeEach("Login TMS systems", () => {
        cy.fixture("data_test_new_enhancement.json").then((loginData) => {
            data = loginData
            const url = new Utility().getBaseUrl()
            cy.visit(url)
            if (url == "https://tms-stg.allnowgroup.com/") {
                objLogin = new LoginSupplier()
                objHome = objLogin.loginWithUserandPassword(data.usernametms, data.password)
            } else if (url == "https://tms.allnowgroup.com") {
                cy.fixture("data_test.json").then((dataPRD) => {
                    data = dataPRD
                    objLoginPRD = new Login()
                    objHome = objLoginPRD.login(data.username, data.password)
                })
            }
        })
    })

    it("TC: Check search information by conditions", () => {
        cy.randomString(6).then((trackingNum) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum, customer, product, temperature, siteA, siteC, 7);
            trackNumber = trackingNum
        })
    })

    it("TC: Check keep function with orders have status Not dispatched- blank Carrier field", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("save").click({ force: true })
        objSchedule.common.msgError().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- no vehicle information", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.lnkAddVehicle().click({ force: true })
        objSchedule.btnAction("save").click({ force: true })
        objSchedule.common.msgWarning().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- blank Consignment Products/Items field", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.btnAction("save").click({ force: true })
        objSchedule.common.msgSuccess().should("be.visible")
    })

    it("TC: Check keep function with orders have status Not dispatched- input valid data", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(1000)
        objSchedule.common.openDetailView()
        cy.wait(1000)
        objSchedule.selectCarrier(carrier)
        cy.wait(1000)
        objSchedule.lnkAddVehicle().click({ force: true })
        objSchedule.selectDriver(driver)
        objSchedule.btnAction("save").click({ force: true })
        objSchedule.common.msgSuccess().should("be.visible")
    })

    it("TC: Check Re-plan order unsuccessfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.rePlan("cancel")
    })

    it("TC: Check Re-plan order successfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.rePlan("confirm")
        objDeliveryPlan = objSchedule.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
        objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", trackNumber, "second")
    })

    it("TC: Check Re-plan order successfully in case record transferred from Shipping Order", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", id, customer, product, temperature, siteB, siteC, 5);
            objShippingOrder.common.msgSuccess().should("be.visible")
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", id, "first")
            objDeliveryPlan.createADispatchOrder(id)
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", id, "second")
            objDeliveryPlan.confirmDispatch()
            objDeliveryPlan.common.msgSuccess().should("be.visible")
            objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            cy.wait(2000)
            objSchedule.rePlan("confirm")
            objDeliveryPlan = objSchedule.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", id, "second")
        })
    })

    it("TC: Confirm and dispatch", () => {
        cy.randomString(7).then((trackingNum2) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("direct schedule", trackingNum2, customer, product, temperature, siteB, siteD, 5);
            trackNumber2 = trackingNum2
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(2000)
            objSchedule.common.openDetailView()
            cy.wait(2000)
            objSchedule.header.searchResult(trackNumber2).should("be.visible")
        })
    })

    it("TC: Check Confirm and dispatch function with orders have status Not dispatched- blank Carrier field", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("confirm and dispatch").click({ force: true })
        objSchedule.common.msgError().should("be.visible")
    })

    //config
    it("Set up function - schedule offline carrier capacity information", () => {
        objFunction = objHome.leftMenu.navigateToSubPage("Set Up", "Function")
        objFunction.selectScheduleOffCarrier("must")
    })

    //bug
    it("TC: Check Confirm and dispatch function with orders have status Not dispatched- no vehicle information", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.btnAction("confirm and dispatch").click({ force: true })
        cy.wait(1000)
        objSchedule.common.btnConfirmHintPopUp("confirm").click({ force: true })
        objSchedule.common.msgError().should("be.visible")
    })

    it("TC: Check Confirm and schedule function with orders have status Not dispatched- input valid data", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectCarrier(carrier)
        objSchedule.lnkAddVehicle().click({ force: true })
        objSchedule.selectDriver(driver)
        // objSchedule.selectDriver("Hana driver11")
        cy.wait(5000)
        objSchedule.btnAction("confirm and dispatch").click({ force: true })
        objSchedule.common.msgSuccess().should("be.visible")
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
    })

    //depend on TC above
    it("TC: Check Notify again unsuccessfully with orders have status dispatched", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("notify again").click({ force: true })
        cy.wait(2000)
        objSchedule.sendEmailTab().should("be.visible")
        objSchedule.header.btnAtHeader("do not send").click({ force: true })
        cy.wait(2000)
        objSchedule.header.btnAtHeader("batch schedule").should("be.visible")
    })

    it("TC: Check Download the dispatch list function", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("notify again").click({ force: true })
        cy.wait(2000)
        objSchedule.sendEmailTab().should("be.visible")
        objSchedule.header.btnAtHeader("download the delivery note").click({ force: true })
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
    })

    it("TC: Check Confirm vehicle function with orders have status dispatched", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.selectDriver(driver)
        cy.wait(5000)
        objSchedule.header.btnAtHeader("confirm vehicle").click({ force: true })
        objSchedule.common.msgSuccess().should("be.visible")
    })

    it("TC: Check Download documents function", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("download documents").click({ force: true })
        cy.wait(2000)
        objSchedule.header.btnAtHeader("download the delivery note").click({ force: true })
        cy.task("countFiles", "cypress/downloads").then((count) => {
            if (count > 0) {
                return true
            } else if (count == 0) {
                return false
            }
        })
    })

    it("TC: Check Withdraw schedule function with orders have status dispatched", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.common.openDetailView()
        cy.wait(2000)
        objSchedule.btnAction("withdraw schedule").click({ force: true })
        objSchedule.common.btnConfirmHintPopUp("confirm").click({ force: true })
        objSchedule.common.msgSuccess().should("be.visible")
        objSchedule.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", trackNumber2)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
    })

    it("TC: Check Re-plan order successfully in case record transferred from Shipping Order", () => {
        cy.randomString(7).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("confirm", id, customer, product, temperature, siteB, siteC, 5);
            objShippingOrder.common.msgSuccess().should("be.visible")
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("waybill number", id, "first")
            objDeliveryPlan.createADispatchOrder(id)
            objDeliveryPlan = objShippingOrder.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", id, "second")
            objDeliveryPlan.confirmDispatch()
            objDeliveryPlan.common.msgSuccess().should("be.visible")
            objSchedule = objDeliveryPlan.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", id)
            cy.wait(2000)
            objSchedule.rePlan("confirm")
            objDeliveryPlan = objSchedule.leftMenu.navigateToSubPage("Plan", "Delivery Plan")
            objDeliveryPlan.searchByClickSuggestionAtDeliveryPlanPage("dispatch number", id, "second")
        })
    })

    it("Dispatch for withdraw schedule", () => {
        cy.randomString(8).then((id) => {
            objShippingOrder = objHome.leftMenu.navigateToPage("Shipping order")
            objCreateShippingOrder = objShippingOrder.navigateToCreateShippingOrderPage()
            objShippingOrder = objCreateShippingOrder.createNewShippingOrder("direct schedule", id, customer, product, temperature, siteB, siteD, 8);
            withdrawSchedule = id
            objSchedule = objShippingOrder.leftMenu.navigateToPage("Schedule")
            objSchedule.common.searchByClickSuggestion("dispatch number", withdrawSchedule)
            objSchedule.common.searchResultWithOutValue().should("be.visible")
            cy.wait(2000)
            objSchedule.common.openDetailView()
            cy.wait(2000)
            objSchedule.selectCarrier(carrier)
            objSchedule.lnkAddVehicle().click({ force: true })
            objSchedule.selectDriver(driver)
            // objSchedule.selectDriver("Hana driver11")
            cy.wait(5000)
            objSchedule.btnAction("confirm and dispatch").click({ force: true })
            objSchedule.common.msgSuccess().should("be.visible")
            objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        })
    })

    it("TC: Check after action withdraw schedule unsuccessfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", withdrawSchedule)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.withdrawSchedule("no")
    })

    it("TC: Check after action withdraw schedule successfully", () => {
        objSchedule = objHome.leftMenu.navigateToPage("Schedule")
        cy.wait(2000)
        objSchedule.common.searchByClickSuggestion("dispatch number", withdrawSchedule)
        objSchedule.dispatchScheduleStatus("job_accepted").should("be.visible")
        cy.wait(2000)
        objSchedule.withdrawSchedule("yes")
        objSchedule.leftMenu.navigateToPage("Schedule")
        objSchedule.common.searchByClickSuggestion("dispatch number", withdrawSchedule)
        objSchedule.dispatchScheduleStatus("job_created").should("be.visible")
    })

    //reset config
    it("Reset set up function - schedule offline carrier capacity information", () => {
        objFunction = objHome.leftMenu.navigateToSubPage("Set Up", "Function")
        objFunction.selectScheduleOffCarrier("no need")
    })
})