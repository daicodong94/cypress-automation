import LeftMenu from "../../../TMS/components/fragments/LeftMenu";
import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SiteAdd from "../site/SiteAdd";
import SiteList from "../site/SiteList";
import SubcontractorAdd from "./SubcontractorAdd";

class SubcontractorList {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        this.leftMenu = new LeftMenu()
        this.siteList = new SiteList()
        this.siteAdd = new SiteAdd()
    }

    lblTitle() {
        return cy.xpath("//p[text()='Create Subcontractor']")
    }

    titleSubcontractorList() {
        return cy.xpath('//span[text()="Subcontractor"]')
    }

    btnExport() {
        return cy.xpath('//div[contains(text(),"Export")]//ancestor::button')
    }

    btnImport() {
        return cy.xpath('//div[contains(text(),"Import")]//ancestor::button')
    }

    btnSearch() {
        return cy.xpath('//button[@class="el-button flex items-center el-button--primary el-button--mini"]')
    }

    dropdownSubcontractorStatus() {
        return cy.xpath('//div[contains(text(),"Subcontractor Status")]')
    }

    btnFilter() {
        return cy.xpath('//button//div[contains(text(),"Filter")]')
    }

    searchByCondition(condition) {
        this.header.txtSearch().clear({ force: true })
        this.header.txtSearch().type(condition)
        cy.wait(2000)
        this.btnSearch().click({ force: true })
    }

    clickBtnFilter() {
        this.btnFilter().click({ force: true })
    }

    searchingByValueOfDropdown(name, location, value) {
        this.siteList.dropdownFilter(name, location).click({ force: true })
        this.titleSearchFilter(value).click({ force: true })
        this.common.searchResultWithOutValue().should("be.visible")
    }

    titleSearchFilter(title) {
        return cy.xpath('//li//span[contains(text(),"' + title + '")]')
    }

    iconSetStatus() {
        return cy.xpath('(//span[@class="el-tooltip"]//*[local-name()="svg" and @fill="none"])[2]')
    }

    btnActionSetStatus(btnName) {
        return cy.xpath('//span[text()="' + btnName + '"]//ancestor::button')
    }

    searchResults(status) {
        return cy.xpath('//span[@class="el-tooltip"]//span[text()="' + status + '"]')
    }

    selectAndSetStatus(status, message, condition) {
        if (status == "Active") {
            this.siteList.rdBtnSetStatus("Inactive").click({ force: true })
            this.siteList.btnSetStatus().click({ force: true })
            cy.wait(2000)
            this.common.msgSuccess(message).should("be.visible")
            // this.scrollbar("left").scrollTo('right', {duration: 3000 })
            this.searchResults("Inactive").should("be.visible")
        } else if (status == "Inactive") {
            this.siteList.rdBtnSetStatus("Active").click({ force: true })
            this.siteList.btnSetStatus().click({ force: true })
            cy.wait(2000)
            this.common.msgSuccess(message).should("be.visible")
            // this.scrollbar("left").scrollTo('right', {duration: 3000 })
            this.searchResults("Active").should("be.visible")
        }
    }

    titleSubcontractorDetails() {
        return cy.xpath('//p[text()="Subcontractor Detail"]')
    }

    shouldUIPopupImportSubcontractor() {
        this.common.titlePopupImportFile("Import Subcontractor").should("be.visible")
        this.common.btnDownloadTemplate().should("be.visible")
        this.common.btnClicktoUpLoad().should("be.visible")
        this.common.btnCancel().should("be.visible")
        this.common.btnImportPopupAttachFileDisabled().should("be.disabled")
    }

    btnCreate() {
        return cy.xpath('//button//div[contains(text(),"Create")]')
    }

    titleHistoryLog() {
        return cy.xpath('//div//p[contains(text(),"History")]')
    }

    activeType(status) {
        return cy.xpath('(//span[contains(text(),"' + status + '")])[1]')
    }

    clickCreateSubcontractor() {
        this.common.btnAction("Create").click({ force: true })
        return new SubcontractorAdd()
    }
}

export default SubcontractorList;