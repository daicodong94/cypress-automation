import CommonMasterData from "../common/CommonMasterData";
import HeaderMasterData from "../fragments/HeaderMasterData";
import SiteList from "../site/SiteList";
import CustomerList from "./CustomerList";
class CustomerDetail {
    constructor() {
        this.common = new CommonMasterData()
        this.header = new HeaderMasterData()
        // this.siteList = new SiteList()
        this.customerList = new CustomerList()
    }

btnEditOrViewHisoryLog(index){ return cy.xpath("(//div[contains(@class, 'inline-flex items-center gap')])["+ index +"]")}


clickToBtnEditOrViewHistoryLog(button){
    if(button =="View"){
        this.btnEditOrViewHisoryLog("1").click()
    }else if( button == "View History Log"){
        this.btnEditOrViewHisoryLog("2").click()
    }
}
}

export default CustomerDetail