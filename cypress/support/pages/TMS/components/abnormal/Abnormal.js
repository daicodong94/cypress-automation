import Common from "../common/Common";
import Header from "../fragments/Header";
import LeftMenu from "../fragments/LeftMenu";
import Pickup from "../Implement/PickUp";

class Abnormal {
    constructor() {
        this.header = new Header()
        this.leftMenu = new LeftMenu()
        this.common = new Common()
        this.pickup = new Pickup()
    }

    deleteAbnormal(count, state, source) {
        this.header.btnAtHeader("more filter...").click({ force: true })
        this.common.searchBySelectValue("abnormal state", state)
        this.common.searchBySelectValue("source", source)
        this.common.selectMultiRecord(count)
        cy.wait(1000)
        this.header.btnAtHeader("delete").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    closeAbnormal(state, count) {
        // this.header.btnAtHeader("more...").click()
        this.common.searchBySelectValue("abnormal state", state)
        this.common.selectMultiRecord(count)
        cy.wait(1000)
        this.header.btnAtHeader("close").click()
        cy.wait(2000)
        this.common.btnConfirmHintPopUp("confirm").click()
    }

    reProcessAbnormal(state, count) {
        // this.header.btnAtHeader("more...").click()
        this.common.searchBySelectValue("abnormal state", state)
        this.common.selectMultiRecord(count)
        cy.wait(1000)
        this.header.btnAtHeader("reprocess").first().click()
    }

    inputWaybillnumber(trackNumber) {
        this.header.ipSearchBox("waybill number").click({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox("waybill number").focus().type(trackNumber)
        this.header.ipSearchBox("waybill number").type("{enter}")
        cy.wait(2000)
        this.header.spanSearchSuggestionDeliveryPlanPage(trackNumber).should("be.visible")
        this.header.spanSearchSuggestionDeliveryPlanPage(trackNumber).click({ force: true })
    }

    selectValue(field, value) {
        this.header.ipSearchBox(field).click()
        cy.wait(1000)
        this.header.spanDropdownValue(value).click()
    }

    ipGenerationTime() {
        return cy.xpath("//div[@class='el-date-editor common-input-style el-input el-input--mini el-input--prefix el-input--suffix el-date-editor--datetime']//input")
    }

    createAbnormal(trackNumber, exType, exLevel, time, action) {
        this.inputWaybillnumber(trackNumber)
        if (exType != "none") {
            this.selectValue("exception type", exType)
        }
        if (exLevel != "none") {
            this.selectValue("Select", exLevel)
        }
        if (time != "none") {
            this.ipGenerationTime().click()
            if (time == "now") {
                this.common.btnTimeNow().click()
            } else {
                this.common.inputTime(time)
            }
        }
        cy.wait(1000)
        this.header.btnAtHeader(action).click()
    }

    lblFieldDataAutoGenerate(label) {
        return cy.xpath("(//td[text()='" + label + "']//following-sibling::td)[1]")
    }

    ipEnterDataToFilter() {
        return cy.xpath("(//div[@class='search-candidate']//input)[last()]")
    }

    spanSearchFilterResult(condition) {
        return cy.xpath("//div[@class='user-name fl']//span[@class='search-highlight' and text()='" + condition + "']")
    }

    btnEditInside(btnName) {
        return cy.xpath("(//div[@class='tab-content h-full']//button//span[contains(text(),'" + btnName + "')])[2]")
    }

    editAbnormal(state, editField, condition, action) {
        this.common.searchBySelectValue("abnormal state", state)
        this.common.searchResultWithOutValue().first().dblclick({ force: true })
        cy.wait(1000)
        this.header.ipSearchBox(editField).last().click({ force: true })
        cy.wait(1000)
        this.ipEnterDataToFilter().type(condition, { force: true })
        this.spanSearchFilterResult(condition).click()
        cy.wait(2000)
        this.btnEditInside(action).click({ force: true })
        this.common.msgSuccess().should("be.visible")
    }

    ipDefinitionOfResponsibility(label) {
        return cy.xpath("(//td[text()='" + label + "']//following-sibling::td)[1]")
    }

    liSelectExceptionInformation() {
        return cy.xpath("(//div[@class='el-select-dropdown el-popper'])[last()]//li[1]")
    }

    fileManagement(filePath) {
        cy.xpath("(//button[@class='el-button el-button--text el-button--small'])[3]").click()
        cy.xpath("//div[@id='tab-fileManager']").click()
        cy.wait(1000)
        this.common.addFileAttachments(filePath)
    }

    abnormalState(state) {
        return cy.xpath('//table//tr[@class="el-table__row"]//td//div[text()= "' + state + '"]')
    }

    dropdownResponsiblePerson() {
        return cy.xpath('//input[@placeholder="Please enter the responsible person"]')
    }

    btnProcessingCompleted() {
        return cy.xpath('//button//span[contains(text(),"processing completed")]')
    }

    btnAddto() {
        return cy.xpath('//button//span[contains(text(),"+ add to")]')
    }

    liValueDropdownList2() {
        return cy.xpath('//ul//li[@class="search-candidate-item clearfix active"]')
    }

    selectRandomValue() {
        this.liValueDropdownList2().its('length').then((len) => {
            this.liValueDropdownList2().eq(Math.floor(Math.random() * ((len - 1) - 0 + 1)) + 0)
                .click({ force: true })
        })
    }

    confirmAndProcessingOrder(waybill, abnormal) {
        // confirm abnormal Order
        this.common.searchByClickSuggestion("order number", waybill)
        this.pickup.abnormalException(abnormal).should("be.visible")
        this.common.openDetailView()
        this.common.btnConfirmAgainHintPopup("Confirm").click()
        cy.wait(1000)
        this.common.msgSuccessShouldContainText("done successfully").should("be.visible")
        this.abnormalState("pending").should("be.visible")
        // Processing Order
        this.common.openDetailView()
        cy.wait(1000)
        this.btnAddto().click()
        this.dropdownResponsiblePerson().click()
        this.selectRandomValue()
        this.btnProcessingCompleted().click()
        this.common.msgSuccessShouldContainText("done successfully").should("be.visible")
        this.abnormalState("processed").should("be.visible")
    }
}

export default Abnormal;